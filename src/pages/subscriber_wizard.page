<apex:page controller="NewSubscriberWizard" id="subscriberPage">
    <apex:includeScript value="{!URLFOR($Resource.jquery)}"/>

    <script>
    var j$ = jQuery.noConflict();
    
    window.onload = function() {
        j$('[id$=discount-reason-other]').parent().parent().css('display', 'none');
        j$('#submit-button').addClass('btn').removeClass('btnDisabled'); //Ensure that the submit button is re-enabled if an error occurs on the page
    }
    
    /**
     *
     */
    function campaignInfo() {
    	var campaign = j$('[id$=new-campaign-list]').val();
    	if(campaign != '') {
    		j$('[id$=campaign-info-button]').click();
    	} else {
    		return false;
    	}
    }
    
    /**
     *
     */
    function formatMoney(value, c, d, t){
        var n = value, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };
    
    /**
     * change the payment amount field on the form to match the product amount field
     */
    function updateAmount() {
        // hide or display payment info depending on sub amount 
        changeFreePayment(j$('[id$=free-checkbox]')[0]);
        
        // identify the product amount 
        var product_amount = j$('[id$=sub-price]').val();
        product_amount = product_amount.replace(/,/g, '');
        
        // format the amount field 
        j$('[id$=amount]').val(formatMoney(product_amount, 2, '.', ','));
        
        //Remove the value for GST if > 0
        if (j$('[id$=GST-deduction]').val() > 0) {
        	product_amount -= j$('[id$=GST-deduction]').val(); 
        }
        
        // add surcharge fees depending on card type 
        addCreditCardSurcharges(product_amount);
        
        // 
        var reason = j$('[id$=discount-reason]').val();
        toggleOtherDiscountOption(reason);
    }
    
    /**
     * change the product amount field on the form to match the payment amount field
     */
    function updateProductAmount() {
        // identify the payment amount 
        var amount = j$('[id$=amount]').val();
        j$('[id$=sub-price]').val(amount);
        
        //Remove the value for GST if > 0
        if (j$('[id$=GST-deduction]').val() > 0) {
        	amount -= j$('[id$=GST-deduction]').val(); 
        }
        
        // add surcharge fees depending on card type 
        addCreditCardSurcharges(amount);
        
        var reason = j$('[id$=discount-reason]').val();
        toggleOtherDiscountOption(reason);
    }
    
    /**
     * update the total price to include credit card fees
     */
    function addCreditCardSurcharges(amount) {
        if (j$('[id$=free-checkbox]').prop('checked') == false) {
            if (j$('[id$=card_type]').val() == "AMEX") {
                amount = amount * 1.022;
            } else if (j$('[id$=card_type]').val() == "DINERS") {
                amount = amount * 1.011;
            }
        }
        j$('[id$=total-price]').text("$" + formatMoney(amount, 2, '.', ','));
    }
    
    /**
     * hide or show the payment section depending if product is free
     */
    function changeFreePayment(element) {
        if (element.checked == true) {
            j$('[id$=payment-section]').css('display', 'none');
        	j$('[id$=creditcard-section]').css('display', 'none');
        } else {
            j$('[id$=payment-section]').css('display', 'block');
        	j$('[id$=creditcard-section]').css('display', 'block');
        }
    }
    
    /**
     *
     */
    function changeAccountType(value) {
        if (value == "new") {
            j$('[id$=existing-account-section]').css('display', 'none');
            j$('[id$=new-account-section]').css('display', 'block');
        } else {
            j$('[id$=existing-account-section]').css('display', 'block');
            j$('[id$=new-account-section]').css('display', 'none');
        }
    }
    
    /**
     *
     */
    function showLoadingImage() {           
        j$('#loading-image').css('display', 'inline');
    }
    function showProductLoadingImage() {
    	j$('#loading-image-product').css('display', 'inline');
    }
    function hideLoadingImage() {
        j$('#loading-image').css('display', 'none');
    }
    
    /**
     * Really messy validation function
     */
    function validate() {
        if (j$('#submit-button').hasClass('btnDisabled') == false) {
            var validated = false;
            
            if (document.getElementById('subscriberPage:newSubscriberWizardForm:newSubscriberWizard:account-section:account-type-section-item:account-type:0').checked == true) {
                if (j$('[id$=free-checkbox]').prop('checked') == false) {
	                if (validatePayment() & validateProduct() & validateExistingAccount() & validateCreditCardDate()) {
	                    validated = true;
	                }
                } else {
                    if (validateProduct() & validateExistingAccount()) {
                        validated = true;
                    }
                }
            } else {
                if (j$('[id$=free-checkbox]').prop('checked') == false) {
	                if (validatePayment() & validateProduct() & validateNewAccount() & validateCreditCardDate()) {
	                   	validated = true;
	                }
                } else {
                    if (validateProduct() & validateNewAccount()) {
                        validated = true;
                    }
                }
            }
            
            // click the real submit button to send the form values to the server 
	        if (validated) {
	        	showLoadingImage();
		        j$('#submit-button').addClass('btnDisabled').removeClass('btn');
		        j$('[id$=submit-link]').click();
	        }
        }
    }
    
    function validateExistingAccount() {
        var account_lookup = j$('[id$=account-lookup]')[0];
        if (validateElement(account_lookup, '', 'validate-account-lookup')) {
            return true;
        }
        return false;
    }
    
    function validateNewAccount() {
        var firstname = j$('[id$=account-firstname]')[0];
        var lastname = j$('[id$=account-lastname]')[0];
        var email = j$('[id$=account-email]')[0];
        var phone = j$('[id$=account-phone]')[0];
        var birth = j$('[id$=account-year-birth]')[0];
        var street = j$('[id$=street]')[0];
        var city = j$('[id$=city]')[0];
       	var state = j$('[id$=state]')[0];
        var country = j$('[id$=country]')[0];
        var postcode = j$('[id$=postcode]')[0];
        
        if (validateElement(firstname, '', 'validate-firstname') & validateElement(lastname, '', 'validate-lastname') & validateElement(email, '', 'validate-email') & validateElement(phone, '', 'validate-phone') & validateElement(birth, '', 'validate-birth') & validateElement(street, '', 'validate-street') & validateElement(city, '', 'validate-city') & validateElement(state, '', 'validate-state') & validateElement(country, '', 'validate-country') & validateElement(postcode, '', 'validate-postcode') & validatePhoneNumber()) {
            return true;
        }
        return false;
    }
	
	function validatePhoneNumber() {
    	var phone = j$('[id$=account-phone]')[0].value;
    	var pattern = /^[0-9]?\d{1}[0-9]\d{8}$/g;
    	
    	if (!pattern.test(phone)){
    		//Display the error message
    		document.getElementById('validate-phone-format').style.display = 'inline';
    		return false;
    	}
    	else {
    		document.getElementById('validate-phone-format').style.display = 'none';
    		return true;
    	}
    }
    
    function validatePhoneNumber() {
    	var phone = j$('[id$=account-phone]')[0].value;
    	var pattern = /^[0-9]?\d{1}[0-9]\d{8}$/g;
    	
    	if (!pattern.test(phone)){
    		//Display the error message
    		document.getElementById('validate-phone-format').style.display = 'inline';
    		return false;
    	}
    	else {
    		document.getElementById('validate-phone-format').style.display = 'none';
    		return true;
    	}
    }
    
    function validateProduct() {
        var product_lookup = j$('[id$=product-lookup]')[0];
        if (validateElement(product_lookup, '', 'validate-product-lookup') & validateDiscount()) {
            return true;
        }
        return false;
    }
    
    function validateDiscount() {
    	var discount = j$('[id$=total-discount]').text();
    	var reason = j$('[id$=discount-reason]').val();
    	var other_reason = j$('[id$=discount-reason-other]').val();
    	
    	if (discount != '$0.00' & reason == '') {
    		j$('#validate-discount-reason').css('display', 'inline');
    		j$('[id$=discount-reason]').focus();
            return false;
        } else if (discount != '$0.00' & reason == 'Other' & other_reason == '') {
        	j$('#validate-discount-reason').css('display', 'none');
        	j$('#validate-other-discount-reason').css('display', 'inline');
        	j$('[id$=discount-reason-other]').focus();
            return false;
        } else {
            j$('#validate-discount-reason').css('display', 'none');
        	j$('#validate-other-discount-reason').css('display', 'none');
            return true;
        }
    }
    
    function validatePayment() {
        var amount = j$('[id$=amount]').val().replace('$', '').replace(/,/g, '');
        var validate_amount = amount.replace(/\./,'');
        if (validateDigits(validate_amount, 1, 'validate-amount-length', 'validate-amount')) {
            return true;
        }
        return false;
    }
    
    function validateCreditCardDate() {
    	var expiryMonth = j$('[id$=expiryMonth]').val();
    	var expiryYear = j$('[id$=expiryYear]').val();
    	
    	/* Check if the 'New Credit Card' radio button is ticked. If not, just return true */
    	if (j$('[id$=new-account]').prop('checked') == false) {
    		return true;
    	}
    	
    	/* If there are no credit card numbers etc, also just return true (prob different payment method!) */
    	if (j$('[id$=card_number]').val() == '') {
    		return true
    	}
    	
    	var now = new Date();
    	var currentMonth = now.getMonth()+1;
    	var currentYear = now.getFullYear();
    	
    	if (expiryMonth!=null && expiryYear!=null && (expiryYear < currentYear || (expiryYear == currentYear && expiryMonth < currentMonth))) {
    		j$('#validate-expiry-date').css('display', 'inline');
    		return false;
    	}
    	else {
    		j$('#validate-expiry-date').css('display', 'none');
    		return true;
    	}
    }
    
    /**
     * function for validating a single element to check that it only 
     * contains digits with a specified length
     *
     * @param value: the number to validate
     * @param length: the minimum required size of the number e.g. cc must contain atleast 15 digits
     * @param validation_length_id: the id of the error message for not reach the the required length
     * @param validation_id: the id of the error message for containing invalid digits
     */
    function validateDigits(value, length, validation_length_id, validation_id) {
        var original_size = value.length;
        var new_size = value.replace(/\D/g,'');
        if (original_size != new_size.length) {
            document.getElementById(validation_id).style.display = 'inline';
            document.getElementById(validation_length_id).style.display = 'none';
            return false;
        } else {
            if (value.length < length) {
                document.getElementById(validation_length_id).style.display = 'inline';
                document.getElementById(validation_id).style.display = 'none';
                return false;
            }
            document.getElementById(validation_length_id).style.display = 'none';
            document.getElementById(validation_id).style.display = 'none';
            return true;
        }
    }
    
    /**
     * function for validating a single element to check it does not match a specified value
     * 
     * @param element: the form element to perform the validation on
     * @param condition: the invalid value
     * @param validation_id: the id of the error message for matching the the condition
     */
    function validateElement(element, condition, validation_id) {
        if (element.value == condition) {
            document.getElementById(validation_id).style.display = 'inline';
            element.focus();
            return false;
        } else {
            document.getElementById(validation_id).style.display = 'none';
            return true;
        }
    }
    
    function enableSubmit() {
    }
    
    function toggleOtherDiscountOption(value) {
        if (value == 'Other') {
        	j$('[id$=discount-reason-other]').parent().parent().css('display', 'table-row');
        } else {
        	j$('[id$=discount-reason-other]').parent().parent().css('display', 'none');
        }
    }
    
    function init() {
        var form_wrapper = 'subscriberPage:newSubscriberWizardForm:newSubscriberWizard';
        // display the correct account type 
        if (document.getElementById(form_wrapper + ':account-section:account-type-section-item:account-type:0').checked == true) {
        	j$('[id$=existing-account-section]').css('display', 'block');
        	j$('[id$=new-account-section]').css('display', 'none');
        } else {
            j$('[id$=existing-account-section]').css('display', 'none');
        	j$('[id$=new-account-section]').css('display', 'block');
        }
        
        var reason = j$('[id$=discount-reason]').val();
        toggleOtherDiscountOption(reason);
        
        if (j$('[id$=free-checkbox]').prop('checked') == false) {
            j$('[id$=payment-section]').css('display', 'block');
            j$('[id$=creditcard-section]').css('display', 'block');
        }
        if (document.getElementById(form_wrapper + ':account-section:account-type-section-item:account-type:0').checked == true) {
            j$('[id$=new-account-section]').css('display', 'none');
        }
        j$('[id$=terms-and-conditions-checkbox]').prop('checked', false);
        hideLoadingImage();
    }
    </script>
    
    <!-- CSS Styling -->
    <style>
    textarea {
        margin: 2px;
        width: 148px;
        height: 34px;
    }
    .btnDisabled {
        float:left;
    }
    .btn {
        cursor:pointer;
        float:left;
    }
    .message .messageText a {
        margin: 0px;
        color: #015BA7;
        font-size: 100%;
    }
    [id=subscriberPage\:newSubscriberWizardForm\:newSubscriberWizard\:new-account-section], [id=subscriberPage\:newSubscriberWizardForm\:newSubscriberWizard\:payment-section], 
    [id=subscriberPage\:newSubscriberWizardForm\:newSubscriberWizard\:new-creditcard], [id=subscriberPage\:newSubscriberWizardForm\:newSubscriberWizard\:existing-creditcard],
    [id=subscriberPage\:newSubscriberWizardForm\:newSubscriberWizard\:creditcard-section] {
        display: none;
    }
    .pbBody .labelCol {
        vertical-align: middle !important;
    }
    .pbSubheader {
    	margin-top: 0px;
    }
    [id=subscriberPage\:newSubscriberWizardForm\:newSubscriberWizard\:campaign-section] .labelCol {
        vertical-align: top !important;
    }
    </style>
    
    <!-- HTML -->
    <apex:form id="newSubscriberWizardForm">
        <apex:inputHidden value="{!creditcard.go1__Token__c}" id="creditcard-token" />
        <apex:inputHidden value="{!gstDeduction}" id="GST-deduction" />
        <apex:pageBlock title="New Subscription Wizard" id="newSubscriberWizard">
            <apex:pageMessages id="messages" escape="false" />
  	
            <!-- Account Section -->
            <apex:pageBlockSection columns="1" id="account-section" showHeader="true" title="Account" rendered="{!showForm}" >
                 <apex:pageBlockSectionItem id="account-type-section-item" >
                    <apex:panelGroup >
                        <apex:selectRadio id="account-type" value="{!accountType}" onchange="changeAccountType(this.value);">
                            <apex:selectOption itemValue="existing" itemLabel="Existing Subscriber" id="existing-account"/>
                            <apex:selectOption itemValue="new" itemLabel="New Subscriber" id="new-account"/>
                        </apex:selectRadio>
                    </apex:panelGroup>
                 </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <!-- Existing Account showHeader="true" title="Existing Account" -->
            <apex:pageBlockSection columns="1" id="existing-account-section" rendered="{!showForm}">
                <apex:pageBlockSectionItem id="existing-account-section-item" >
                    <apex:outputLabel >Account: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:inputField id="account-lookup" value="{!product.Person_Account__c}" required="false">
                        	<apex:actionSupport event="onchange" action="{!loadAccount}" reRender="campaign-section,creditcard-section,GST-checkbox,GST-deduction,total-price" oncomplete="changeFreePayment(j$('[id$=free-checkbox]')[0]);" />
                        </apex:inputField>
                        <span id="validate-account-lookup" style="color:red;display:none;font-style: italic;"><apex:outputText value="* Account Required" /></span>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <!-- New Account showHeader="true" title="New Account" -->
            <apex:pageBlockSection columns="1" id="new-account-section" rendered="{!showForm}">
                <apex:pageBlockSectionItem id="new-firstname-section-item">
                    <apex:outputLabel >First Name: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:inputText value="{!account.FirstName}" required="false" id="account-firstname" />
                        <span id="validate-firstname" style="color:red;display:none;font-style: italic;"><apex:outputText value="* Firstname required" /></span>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem id="new-lastname-section-item">
                    <apex:outputLabel >Last Name: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:inputText value="{!account.LastName}" required="false" id="account-lastname" />
                        <span id="validate-lastname" style="color:red;display:none;font-style: italic;"><apex:outputText value="* Lastname required" /></span>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem id="new-email-section-item">
                    <apex:outputLabel >Email: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:inputText value="{!account.PersonEmail}" required="false" id="account-email" />
                        <span id="validate-email" style="color:red;display:none;font-style: italic;"><apex:outputText value="* Email required" /></span>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem id="new-phone-section-item">
                    <apex:outputLabel >Phone: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:inputText value="{!account.Phone}" required="false" id="account-phone" />
                        <span id="validate-phone" style="color:red;display:none;font-style: italic;"><apex:outputText value="* Phone required" /></span>
                        <span id="validate-phone-format" style="color:red;display:none;font-style: italic;"><apex:outputText value="* Phone must be valid 10 digits" /></span>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem id="new-dob-section-item">
                    <apex:outputLabel >Year of Birth: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:selectList value="{!account.Birth_Year__c}" size="1" required="false" id="account-year-birth">
                            <apex:selectOptions value="{!birthYears}" />
                        </apex:selectList>
                        <span id="validate-birth" style="color:red;display:none;font-style: italic;"><apex:outputText value="* Birth year required" /></span>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem id="new-street-section-item">
                    <apex:outputLabel >Street Address: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:inputField id="street" value="{!account.BillingStreet}" />
                    	<span id="validate-street" style="color:red;display:none;font-style: italic;"><apex:outputText value="* Street address required" /></span>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem id="new-city-section-item">
                    <apex:outputLabel >City: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:inputField id="city" value="{!account.BillingCity}" />
                    	<span id="validate-city" style="color:red;display:none;font-style: italic;"><apex:outputText value="* City required" /></span>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem id="new-state-section-item">
                    <apex:outputLabel >State: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:inputField id="state" value="{!account.BillingState}" />
                    	<span id="validate-state" style="color:red;display:none;font-style: italic;"><apex:outputText value="* State required" /></span>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
               <apex:pageBlockSectionItem id="new-country-section-item">
                    <apex:outputLabel >Country: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:inputField id="country" value="{!account.BillingCountry}" />
                    	<span id="validate-country" style="color:red;display:none;font-style: italic;"><apex:outputText value="* Country required" /></span>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem id="new-postcode-section-item">
                    <apex:outputLabel >Post Code: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:inputField id="postcode" value="{!account.BillingPostalCode}" />
                        <span id="validate-postcode" style="color:red;display:none;font-style: italic;"><apex:outputText value="* Postcode required" /></span>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <!-- Campaign Section -->
            <apex:pageBlockSection columns="1" id="campaign-section" showHeader="true" title="Campaign Offers" rendered="{!showForm}">
                <apex:pageBlockSectionItem id="new-campaign-list-section-item">
                    <apex:outputLabel >Campaign Offers: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:selectList id="new-campaign-list" required="false" value="{!campaignId}" size="1">
                            <apex:selectOptions value="{!newCampaigns}" />
                        </apex:selectList>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem id="campaign-info-button-section-item">
                    <apex:outputLabel ></apex:outputLabel>
                    <apex:panelGroup >
                    	<div class="btn" onclick="campaignInfo();">see campaign info</div>
                        <apex:commandLink id="campaign-info-button" action="{!campaignInfo}" style="display:none;" target="_blank"/>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <!-- Product Section -->
            <apex:pageBlockSection columns="1" id="product-section" showHeader="true" title="Product" rendered="{!showForm}">
                <apex:pageBlockSectionItem id="product-lookup-section-item" >
                    <apex:outputLabel >Product Lookup: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:inputField id="product-lookup" value="{!product.Subscription_Product__c}">
                            <apex:actionSupport event="onchange" action="{!retrieveProduct}" rerender="GST-deduction,product-section,payment-section-free,total-discount,total-price,recurring-payment,creditcard-section,auto-renewal-checkbox" onsubmit="showProductLoadingImage();" oncomplete="updateAmount();" />
                        </apex:inputField>
                        <img id="loading-image-product" src="{!$Resource.loading_gif}" style="display:none;width:15px;height:15px;padding-top:3px;padding-left:10px;" />
                        <span id="validate-product-lookup" style="color:red;display:none;font-style: italic;"><apex:outputText value="* Product required" /></span>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem id="product-price-section-item" >
                    <apex:outputLabel >Price: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:outputField id="product-price" value="{!subscription.Price__c}" />
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel >Duration: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:outputField value="{!subscription.Duration__c}" />&nbsp;<apex:outputField value="{!subscription.Duration_units__c}" />
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem id="discount-reason-section-item" >
                    <apex:outputLabel >Reason for Discount: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:inputField id="discount-reason" required="false" value="{!product.Discount_Reason__c}" onchange="toggleOtherDiscountOption(this.value);" /> 
                    	<span id="validate-discount-reason" style="color:red;display:none;font-style: italic;"><apex:outputText value="* Discount reason required" /></span>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem id="discount-reason-other-section-item">
                    <apex:outputLabel >Please specify discount reason: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:inputField id="discount-reason-other" required="false" value="{!product.Other_Discount_Reason__c}" />
                        <span id="validate-other-discount-reason" style="color:red;display:none;font-style: italic;"><apex:outputText value="* Other discount reason required" /></span>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem id="product-sub-price-section-item" >
                    <apex:outputLabel >Change Price: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:inputField required="false" id="sub-price" value="{!product.Amount__c}">
                            <apex:actionSupport event="onchange" action="{!changeDiscount}" reRender="GST-deduction,total-discount" oncomplete="updateAmount();" />
                        </apex:inputField>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel >Change Duration: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:inputField value="{!subscription.Duration__c}" id="sub-duration">
                            <apex:actionSupport event="onchange" action="{!changeDiscount}" reRender="total-discount" /> <!-- oncomplete="updateAmount();" -->
                        </apex:inputField>
                        <apex:inputField value="{!subscription.Duration_units__c}" id="sub-duration-units">
                            <apex:actionSupport event="onchange" action="{!changeDiscount}" reRender="total-discount" /> <!-- oncomplete="updateAmount();" -->
                        </apex:inputField>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <!-- Payment Section Free -->
            <apex:pageBlockSection columns="1" id="payment-section-free" showHeader="true" title="Payment" rendered="{!showForm}">
                <apex:pageBlockSectionItem id="payment-free-section-item" >
                    <apex:panelGroup >
                        <apex:outputLabel >Free Product: </apex:outputLabel>
                    </apex:panelGroup>
                    <apex:inputCheckbox id="free-checkbox" value="{!isFree}" selected="true" onchange="changeFreePayment(this)"/>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <!-- Payment Section -->
            <apex:pageBlockSection columns="1" id="payment-section" rendered="{!showForm}">
                <apex:pageBlockSectionItem id="payment-interval-section-item" >
                    <apex:outputLabel >Payment Type: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:selectList id="recurring-payment" required="false" size="1" value="{!subscription.Subscription_Type__c}">  <!-- onchange="enableSubmit()" -->
							<apex:selectOptions value="{!paymentTypes}" />
                        </apex:selectList>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem id="payment-amount-section-item" >
                    <apex:outputLabel >Amount: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:inputField required="false" id="amount" value="{!transaction.go1__Amount__c}">
                            <apex:actionSupport event="onchange" action="{!calculatePrice}" reRender="GST-deduction,total-price,total-discount" oncomplete="updateProductAmount();" />
                        </apex:inputField>
                        <span id="validate-amount" style="color:red;display:none;font-style: italic;"><apex:outputText value="* Invalid amount" /></span>
                        <span id="validate-amount-length" style="color:red;display:none;font-style: italic;"><apex:outputText value="* Amount requires a value" /></span>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem id="GST-exemption-section-item" >
                	<apex:outputLabel >GST Exempt: </apex:outputLabel>
                    <apex:panelGroup >
                    	<apex:inputCheckbox id="GST-checkbox" value="{!account.GST_Exempt__c}">
                    		<apex:actionSupport event="onchange" action="{!calculateGST}" reRender="GST-deduction,total-price,total-discount" oncomplete="updateAmount();" />
                    	</apex:inputCheckbox>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem id="creditcard-discount-section-item" >
                    <apex:outputLabel >Total Discount: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:outputField id="total-discount" value="{!product.Discount__c}" />
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection columns="1" id="creditcard-section" showHeader="true" title="Credit Card" rendered="{!showForm}">
            	<c:Go2Debit_PaymentOptions go2debit_paymentWrapper="{!payment}" go2debit_account="{!account}"></c:Go2Debit_PaymentOptions>
            </apex:pageBlockSection>

            <!-- Confirmation Section -->
            <apex:pageBlockSection columns="1" id="confirmation-section" showHeader="true" title="Confirmation" rendered="{!showForm}">
                <apex:pageBlockSectionItem id="creditcard-total-section-item" >
                    <apex:outputLabel >Total Amount Inc Surcharges: </apex:outputLabel>
                    <apex:panelGroup >
                        <apex:outputText id="total-price" value="{0,number,currency}" >
                        	<apex:param value="{!totalAmount}" />
                        </apex:outputText>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem id="confirmation-renewal-section-item" >
                    <apex:inputCheckbox id="auto-renewal-checkbox" value="{!product.Auto_Renewal__c}" onchange="enableSubmit()" selected="true" />
                    <apex:panelGroup >
                        <apex:outputLabel >I accept auto-renewal payments.</apex:outputLabel>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem id="submit-section-item" >
                    <apex:panelGroup id="submit-buttons">
                        <apex:commandLink value="Submit" action="{!submitForm}" id="submit-link" target="_top" oncomplete="init();" style="display:none" rerender="newSubscriberWizardForm" />
                        <div class="btn" id="submit-button" onclick="validate();" >Submit</div>
                        <img id="loading-image" src="{!$Resource.loading_gif}" style="display:none;width:15px;height:15px;padding-top:3px;padding-left:10px;" />
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
        </apex:pageBlock>
    </apex:form>
</apex:page>