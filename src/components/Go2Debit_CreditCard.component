<apex:component controller="Go2Debit_CreditCardController">
  <apex:attribute type="go1.PaymentWrapper" name="paymentWrapper" description="Payment" assignTo="{!payment}"></apex:attribute>
 
  <apex:pageBlockSection id="creditCardWrapper" columns="1">
    
    <apex:pageBlockSectionItem id="newCardTypeItem" >
      <apex:outputLabel >Card Type: </apex:outputLabel>
      <apex:panelGroup >
        <!-- <apex:selectList id="card_type-output" size="1" value="{!creditcard.go1__Card_Type__c}">
          <apex:selectOptions value="{!cardtypes}" />
        </apex:selectList> -->
        <apex:inputHidden value="{!creditcard.go1__Card_Type__c}" id="creditcard-type" />
        <img id="card-image" src="" style="display:none;width:34px;height:21px;" />
        <apex:outputText id="card-type-output" value="< Enter Card Number >" />
        <span id="validate-cardtype" class="form-error hidden"><apex:outputText value="* Select a card type" /></span>
        <span id="amex-alert" class="hidden surcharge-alert"><apex:outputText value=" (AMEX incurs a 2.2% surcharge fee)" /></span>
        <span id="diners-alert" class="hidden surcharge-alert"><apex:outputText value=" (Diners incurs a 1.1% surcharge fee)" /></span>
      </apex:panelGroup>
    </apex:pageBlockSectionItem>
    
    <apex:pageBlockSectionItem id="newCardNameItem" >
      <apex:outputLabel >Credit Card Name: </apex:outputLabel>
      <apex:panelGroup >
        <apex:inputText required="false" id="card-name" value="{!creditcard.go1__Card_Name__c}"></apex:inputText>
        <span id="validate-card-name" style="color:red;display:none;font-style: italic;"><apex:outputText value="* Card name required" /></span>
      </apex:panelGroup>
    </apex:pageBlockSectionItem>
    
    <apex:pageBlockSectionItem id="newCardNumberItem" >
      <apex:outputLabel >Card Number: </apex:outputLabel>
      <apex:panelGroup >
        <apex:inputField id="card_number" required="false" value="{!creditcard.go1__Token__c}" />
        <span id="validateCardNumberLength" class="form-error hidden"><apex:outputText value="* Card number requires at least 15 digits" /></span>
        <span id="validateCardNumber" class="form-error hidden"><apex:outputText value="* Card number contains invalid characters" /></span>
        <span id="validateCardType" class="form-error hidden"><apex:outputText value="* Card number does not match supported Credit Card Types" /></span>
        <span id="validateCardLengthLuhn" class="form-error hidden"><apex:outputText id="validateCardLengthLuhnText" value="* Card number requires ..." /></span>
        <span id="validateCardLuhn" class="form-error hidden"><apex:outputText id="validateCardLuhnText" value="* Card number is not legitimate - fails checksum validation" /></span>
      </apex:panelGroup>
    </apex:pageBlockSectionItem>
    
    <apex:pageBlockSectionItem id="newCcvItem" >
      <apex:outputLabel >CCV: </apex:outputLabel>
      <apex:panelGroup >
        <apex:inputText id="ccv" maxlength="5" size="3" />
        <span id="validateCcvLength" class="form-error hidden"><apex:outputText value="* CCV requires at least 3 digits" /></span>
        <span id="validateCcv" class="form-error hidden"><apex:outputText value="* CCV contains invalid characters" /></span>
      </apex:panelGroup>
    </apex:pageBlockSectionItem>
    
    <apex:pageBlockSectionItem id="newExpiryDateItem" >
      <apex:outputLabel >Expiry: </apex:outputLabel>
      <apex:panelGroup >
        <apex:selectList id="expiryMonth" size="1" value="{!monthValue}">
          <apex:selectOptions value="{!monthOptions}" />
        </apex:selectList>
        <apex:selectList id="expiryYear" size="1" value="{!yearValue}">
          <apex:selectOptions value="{!yearOptions}" />
        </apex:selectList>
        <span id="validate-expiry-date" style="color:red;display:none;font-style: italic;"><apex:outputText value="* Expiry Date cannot be in the past" /></span>
      </apex:panelGroup>
    </apex:pageBlockSectionItem>

  </apex:pageBlockSection>
    
  <script type="text/javascript">
  	__indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };
  
  	$.fn.validateCreditCard = function(callback, options) {
    var card, card_type, card_types, get_card_type, is_valid_length, is_valid_luhn, normalize, validate, validate_number, _i, _len, _ref, _ref1;
    card_types = [
      {
        name: 'AMEX',
        pattern: /^3[47]/,
        valid_length: [15]
      }, {
        name: 'diners_club_carte_blanche',
        pattern: /^30[0-5]/,
        valid_length: [14]
      }, {
        name: 'DINERS',
        pattern: /^36/,
        valid_length: [14]
      }, {
        name: 'jcb',
        pattern: /^35(2[89]|[3-8][0-9])/,
        valid_length: [16]
      }, {
        name: 'laser',
        pattern: /^(6304|670[69]|6771)/,
        valid_length: [16, 17, 18, 19]
      }, {
        name: 'visa_electron',
        pattern: /^(4026|417500|4508|4844|491(3|7))/,
        valid_length: [16]
      }, {
        name: 'Visa',
        pattern: /^4/,
        valid_length: [16]
      }, {
        name: 'MasterCard',
        pattern: /^5[1-5]/,
        valid_length: [16]
      }, {
        name: 'maestro',
        pattern: /^(5018|5020|5038|6304|6759|676[1-3])/,
        valid_length: [12, 13, 14, 15, 16, 17, 18, 19]
      }, {
        name: 'discover',
        pattern: /^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)/,
        valid_length: [16]
      }
    ];
    if (options == null) {
      options = {};
    }
    if ((_ref = options.accept) == null) {
      options.accept = (function() {
        var _i, _len, _results;
        _results = [];
        for (_i = 0, _len = card_types.length; _i < _len; _i++) {
          card = card_types[_i];
          _results.push(card.name);
        }
        return _results;
      })();
    }
    _ref1 = options.accept;
    for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
      card_type = _ref1[_i];
      if (__indexOf.call((function() {
        var _j, _len1, _results;
        _results = [];
        for (_j = 0, _len1 = card_types.length; _j < _len1; _j++) {
          card = card_types[_j];
          _results.push(card.name);
        }
        return _results;
      })(), card_type) < 0) {
        throw "Credit card type '" + card_type + "' is not supported";
      }
    }
    get_card_type = function(number) {
      var _j, _len1, _ref2;
      _ref2 = (function() {
        var _k, _len1, _ref2, _results;
        _results = [];
        for (_k = 0, _len1 = card_types.length; _k < _len1; _k++) {
          card = card_types[_k];
          if (_ref2 = card.name, __indexOf.call(options.accept, _ref2) >= 0) {
            _results.push(card);
          }
        }
        return _results;
      })();
      for (_j = 0, _len1 = _ref2.length; _j < _len1; _j++) {
        card_type = _ref2[_j];
        if (number.match(card_type.pattern)) {
          return card_type;
        }
      }
      return null;
    };
    is_valid_luhn = function(number) {
      var digit, n, sum, _j, _len1, _ref2;
      sum = 0;
      _ref2 = number.split('').reverse();
      for (n = _j = 0, _len1 = _ref2.length; _j < _len1; n = ++_j) {
        digit = _ref2[n];
        digit = +digit;
        if (n % 2) {
          digit *= 2;
          if (digit < 10) {
            sum += digit;
          } else {
            sum += digit - 9;
          }
        } else {
          sum += digit;
        }
      }
      return sum % 10 === 0;
    };
    is_valid_length = function(number, card_type) {
      var _ref2;
      return _ref2 = number.length, __indexOf.call(card_type.valid_length, _ref2) >= 0;
    };
    validate_number = function(number) {
      var length_valid, luhn_valid;
      card_type = get_card_type(number);
      luhn_valid = false;
      length_valid = false;
      if (card_type != null) {
        luhn_valid = is_valid_luhn(number);
        length_valid = is_valid_length(number, card_type);
      }      
      return callback({
        card_type: card_type,
        luhn_valid: luhn_valid,
        length_valid: length_valid
      });
    };
    validate = function() {
      var number;
      number = normalize($(this).val());
      return validate_number(number);
    };
    normalize = function(number) {
      return number.replace(/[ -]/g, '');
    };
    this.bind('input', function() {
      $(this).unbind('keyup');
      return validate.call(this);
    });
    this.bind('keyup', function() {
      return validate.call(this);
    });
    if (this.length !== 0) {
      validate.call(this);
   	}
   	return this;
  	}
  	
  	function showCardImage(cardType) {
  		if (cardType=='VISA') 
  			j$('#card-image').attr({src:"{!$Resource.VISA}"});
  		else if (cardType=='MASTERCARD')
  			j$('#card-image').attr({src:"{!$Resource.MASTERCARD}"}); 
  		else if (cardType=='AMEX')
  			j$('#card-image').attr({src:"{!$Resource.AMEX}"}); 
  		else if (cardType=='DINERS')
  			j$('#card-image').attr({src:"{!$Resource.DINERS}"}); 
        j$('#card-image').css('display', 'inline');
    }
  	
  	function triggerSurcharge(cardType){
        $('#amex-alert').addClass('hidden');
        $('#diners-alert').addClass('hidden');
            
        var amount = $('input[id$=sub-price]').val();
        
        //Remove the value for GST if > 0
        if (j$('[id$=GST-deduction]').val() > 0) {
        	amount -= j$('[id$=GST-deduction]').val(); 
        }
        
        if (cardType == 'AMEX') {
        	$('#amex-alert').removeClass('hidden');
        	var amount = amount * 1.022;
        } else if (cardType == 'DINERS') {
        	$('#diners-alert').removeClass('hidden');
        	var amount = amount * 1.011;
        }
        $('[id$=total-price]').text('$'+formatMoney(amount, 2, '.', ','));
  	}
  	
  	function validateCreditCardNum(cc) {
    	
   		$('input[id$=card_number]').validateCreditCard(function(result)
		{
			if (result.card_type == null) {
				$('[id$=validateCardType]').css('display', 'inline');
			}
			else {
    			$('[id$=card-type-output]').val(result.card_type.name);
    			$('[id$=card-type-output]').text(' ' + result.card_type.name);
      			$('[id$=creditcard-type]').val(result.card_type.name.toUpperCase());
      			
      			triggerSurcharge($('[id$=card-type-output]').val());
      			
      			$('[id$=validateCardType]').css('display', 'none');
      			
      			//Check that the card number doesnt have non-numeric chars
      			var original_size = $('[id$=card_number]').val().length;
      			var new_size = $('[id$=card_number]').val().replace(/\D/g,'');
      			if (original_size != new_size.length) {
      				$('[id$=validateCardNumber]').css('display', 'inline');
      			}
      			else{
      				$('[id$=validateCardNumber]').css('display', 'none');
      			}
      			
      			//Also check that the length of the card number is correct
      			if (result.length_valid == false){
      				$('[id$=validateCardLengthLuhnText]').text('* ' + result.card_type.name + ' Credit Cards requires a Credit Card number that is ' + result.card_type.valid_length + ' digits long');
      				$('[id$=validateCardLengthLuhn]').css('display', 'inline');
      			}
      			else {
      				$('[id$=validateCardLengthLuhn]').css('display', 'none');
      			}
      			
      			//Check that the Luhn validation ensures this is a valid card number
      			if (result.luhn_valid == false)
      				$('[id$=validateCardLuhn]').css('display', 'inline');
      			else
      				$('[id$=validateCardLuhn]').css('display', 'none');
      			
      			//Finally load in the Credit Card Type icon
      			showCardImage(result.card_type.name.toUpperCase());
      		}
		}, { accept: ['Visa', 'MasterCard', 'AMEX', 'DINERS'] });
		
        //return (cc != '' && validateDigits(cc, 15, 'validateCardNumberLength', 'validateCardNumber'));
        return null;
    }
    function validateCCV(ccv) {
        return (ccv != '' && validateDigits(ccv, 3, 'validateCcvLength', 'validateCcv'));
    }
    (function($) {
    	$('input[id$=card_number]').blur(function(context) {
            var number = $(this).val();
            $(this).val(number.replace(/ /g, ''));
            validateCreditCardNum($(this).val());
        });
        $('input[id$=ccv]').blur(function() {
            validateCCV($(this).val());
        });
        $('[id$=card-type-output]').change(function() {
        	triggerSurcharge($(this).val());
        });
    })(jQuery);
  </script>
  <style>
  .surcharge-alert {
  	font-size: 11px;
	font-style: italic;
  }
  </style>
</apex:component>