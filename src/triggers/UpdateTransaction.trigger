trigger UpdateTransaction on go1__Transaction__c (before insert, before update, after insert, after update) {
	try {
		if (Trigger.isBefore) {
			
			List<Id> accIds = new List<Id>();
			
			for (go1__Transaction__c trans : Trigger.New) {
				if (trans.go1__Code__c >= 10 && 
						(trans.go1__Status__c.contains('Success') || trans.go1__Status__c.contains('Manual') || trans.go1__Status__c.contains('Approved'))) {
					trans.go1__Code__c = 0;
				}
				
				// if Drupal sends transaction which says failure then make sure that its shown correctly in Salesforce.
				if (trans.go1__Code__c < 10 && trans.go1__Transaction__c == null)
				{
					trans.go1__Refundable__c = true;
				}
				if (trans.go1__Status__c == 'Failure') {
					trans.go1__Code__c = 100;
					trans.go1__Refundable__c = false;
				}
				
				if (trans.go1__Status__c == 'Success') {
					trans.go1__Status__c = 'Approved';
				}
				
				if (Trigger.new.size() <= 1 && trans.go1__Recurring_Instance__c != null) {
					Product_Instance__c instance = ProductInstanceHelper.loadProductInstanceByRecurringId(trans.go1__Recurring_Instance__c);
					trans.Subscription_Instance__c = instance.Id; 
					/* NOTE: The below if statement should not occur here - it should happen in the isAfter section
					if (Trigger.isInsert && trans.go1__Amount__c != null) {
						instance.Amount__c = instance.Amount__c + trans.go1__Amount__c;
						upsert instance;
					}*/
				}
				
				accIds.add(trans.go1__Account__c); //Get a collection of all the Account IDs used in these triggered transactions
			}
			
			//Setting the Account Email address on the transaction - moved from TransactionInvoiceSending trigger. Only do on transaction insert!
			if (accIds.size()>0 && Trigger.isInsert) {
				Map<Id, String> accEmails = new Map<Id, String>();
			
				//Get the PersonEmails for the necessary accounts	
				for (Account acc : [SELECT Id, PersonContactId, PersonEmail FROM Account WHERE Id IN :accIds]) {
					accEmails.put(acc.Id, acc.PersonEmail);
				}
				
				//For each transaction, find the appropriate Account and set the email address
				for (go1__Transaction__c trans : Trigger.new) {
					if (accEmails.containsKey(trans.go1__Account__c) && accEmails.get(trans.go1__Account__c)!=null)
						trans.Account_Email__c = accEmails.get(trans.go1__Account__c);
				}
			}
		}
		
		if (Trigger.new.size() <= 1 && Trigger.isAfter) {
			for (go1__Transaction__c trans : Trigger.New) {
				// check transaction was successful
				if (trans.go1__Code__c < 10) {
					ProductInstanceHelper helper = ProductInstanceHelper.loadProductHelper(trans.Subscription_Instance__c);
					Boolean updatePI = false; //Boolean to record whether we need to upsert the associated ProductInstance
					if (helper.instance != null && helper.instance.Subscription_Type__c == 'm') {
						//Check transaction is not a refund
						if (trans.go1__Transaction__c == null) {
							go1__Recurring_Instance__c recurring = helper.setupRecurringPayments();
							if (recurring.go1__Remaining_Attempts__c > 0) {
								upsert recurring;
								
								helper.instance.Recurring_Instance__c = recurring.Id;
								updatePI = true;
							}
						}
						//Only add to the Product Inst Amount if the transaction is sucessful, is inserted, has an amount and is linked to a recurring inst (i.e. not adding the first transaction against a Recurring Instance) OR is a refund
						if (Trigger.isInsert && trans.go1__Amount__c != null && (trans.go1__Recurring_Instance__c != null || trans.go1__Transaction__c != null)) {
							helper.instance.Amount__c = helper.instance.Amount__c + trans.go1__Amount__c;
							updatePI = true;
						}
					}
					
					//If the Product Instance needs to be upserted, do it here
					if (updatePI == true)
						upsert helper.instance;
				}
			}
		}
	}
	catch (Exception e) {
		// as this object is part of Go2Debit we do not want a business logic error causing update errors.
		System.debug(e);
	}
}