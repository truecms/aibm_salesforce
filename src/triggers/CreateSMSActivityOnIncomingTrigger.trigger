trigger CreateSMSActivityOnIncomingTrigger on smagicinteract__Incoming_SMS__c (after insert) 
{
	String subjectPrefix = 'SMS from number ';
	
	try 
	{
		Date todaysDate = System.today();
		List<Task> taskList = new List<Task>();
		List<Account> updateAccounts = new List<Account>();

		Id lastAccountManager = null;
		List<Task> lastSMSTask = [SELECT Id, ownerId FROM Task WHERE Subject LIKE :subjectPrefix + '%' ORDER BY createdDate desc LIMIT 1];
		if (lastSMSTask.size()>0)
			lastAccountManager = lastSMSTask[0].ownerId;

		for(smagicinteract__Incoming_SMS__c sms : Trigger.new)
		{
			Task newTask = new Task();

			Account account = [SELECT Id, OwnerId, PersonContactId, smagicinteract__SMSOptOut__pc 
								FROM Account WHERE Id =: sms.Account__c];
			System.debug(system.logginglevel.INFO, 'Account :' + account);

			//Get the Account Owner - if Sys Admin, assign to random Account Manager
			User userData = [SELECT Id, Name, Department FROM User WHERE Id =: account.OwnerId];
			System.debug(system.logginglevel.INFO, 'userData :' + userData);
			
			//Is this user one of the Acc Mgrs?
			if (TaskManager.managerMap.containsKey(userData.Id)) {
				newTask.OwnerId = account.OwnerId;
			}
			else {
				if (lastAccountManager != null)
					newTask.OwnerId = TaskManager.getNextManager(lastAccountManager);
				else
					newTask.OwnerId = TaskManager.getRandomManager();
			}
			lastAccountManager = newTask.OwnerId;
			
			newTask.Status = 'Not Started';
			newTask.Subject = subjectPrefix + sms.smagicinteract__Mobile_Number__c;
			newTask.Priority = 'Medium';
			newTask.Type = 'Text Inbound';
			newTask.WhoId = account.PersonContactId;
			newTask.ActivityDate = todaysDate;
			newTask.Description = sms.smagicinteract__SMS_Text__c;
			taskList.add(newTask);
			
			//We also want to check if the user is requesting to Opt Out of further SMS Messaging
			if (account.smagicinteract__SMSOptOut__pc == false) //If already opted out, do not bother checking further
			{
				List<smagicinteract__Optout_Settings__c> optOutSettings = [SELECT Id, smagicinteract__Optout_Field__c, smagicinteract__Keyword__c FROM smagicinteract__Optout_Settings__c];
				if (optOutSettings.size()>0)
				{
					List<String> keywords = optOutSettings[0].smagicinteract__Keyword__c.split(',');
					Set<String> textWords = new Set<String>();
					textWords.addAll(sms.smagicinteract__SMS_Text__c.toUpperCase().split('[!?(),;: &-]'));
					
					for (String keyword : keywords) {
						if (textWords.contains(keyword)) {
							//Set the OptOut value against the Account and exit
							account.smagicinteract__SMSOptOut__pc = true;
							updateAccounts.add(account);
							break;
						}
					} 
				}
			}
		}
		
		System.debug(system.logginglevel.INFO, 'taskList In incoming..:' + taskList);
		insert taskList;
		
		//If we've set any Accounts to be updated (Opt Out) then trigger the update DML
		if (updateAccounts.size()>0)
			update updateAccounts;
	}
	catch(Exception e){
		System.debug('DEBUG: Exception in create Task trigger:' + e);
	}
	
}