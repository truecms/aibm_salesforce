trigger PreferredCreditCard on go1__CreditCardAccount__c (before update, before insert, after update, after insert) {
	
	// set the new cc as the preferred card by default
	if (Trigger.New.size() <= 1 && Trigger.isBefore && Trigger.isInsert) {
		for (go1__CreditCardAccount__c creditcard : Trigger.New) {
			List<Account> accounts = [SELECT Id FROM Account WHERE Id = :creditcard.go1__Account__c];
			if (!accounts.isEmpty()) {
				AccountHelper helper = new AccountHelper(accounts);
			
				// uncheck the old preferred/active cc checkbox
				helper.removePreferredCreditCard();
			}
			// check this cc as the preferred card
			creditcard.Preferred_Active_Card__c = true;
		}
	}
	
	// update the cc as the preferred card if it has changed
	if (Trigger.New.size() <= 1 && Trigger.isBefore && Trigger.isUpdate) {
		for (go1__CreditCardAccount__c creditcard : Trigger.New) {
			
			// check that the preferred checkbox has been changed
			go1__CreditCardAccount__c oldCreditcard = Trigger.oldMap.get(creditcard.Id);
			if (creditcard.Preferred_Active_Card__c && !oldCreditcard.Preferred_Active_Card__c) {
				List<Account> accounts = [SELECT Id FROM Account WHERE Id = :creditcard.go1__Account__c];
				if (!accounts.isEmpty()) {
					AccountHelper helper = new AccountHelper(accounts);
				
					// uncheck the old preferred/active cc checkbox
					helper.removePreferredCreditCard();
				}
			}
		}
	}
	
	// update all the recurring instances once the new preferred creditcard has been saved into the database
	if (Trigger.New.size() <= 1 && Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {
		for (go1__CreditCardAccount__c creditcard : Trigger.New) {
			List<Account> accounts = [SELECT Id FROM Account WHERE Id = :creditcard.go1__Account__c];
			if (!accounts.isEmpty()) {
				AccountHelper helper = new AccountHelper(accounts);
			
				// update all the renewal instance that belong to the account
				helper.updateRecurringInstanceCreditCards();
			}
		}
	}
}