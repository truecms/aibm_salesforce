trigger UpdateCampaigns on Campaign (before insert, before update) {
	
	// set the long ID for the account
	for (Campaign campaign : Trigger.new) {
		campaign.Long_ID__c = String.valueOf(campaign.Id);
	}
}