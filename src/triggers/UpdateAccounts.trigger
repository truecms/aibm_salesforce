/**
 * This triggers handle actions to take when inserting/updating Accounts
 *
 * Actions:
 *  - update account lookup fields to correct product instances and transactions information.
 *
 */
trigger UpdateAccounts on Account (before insert, before update) {
	
	//If we need to send a notify 'call to action' email through this update, define the body text 
	String actionEmailBody = '';
	
	Map<Id, List<Account>> accountOwners = new Map<Id, List<Account>>(); //Map<Owner Id, List<Accounts with that OwnerId>>
	
	for (Account account : Trigger.new) {
		//BrightDay - stop Integrations from overwriting a manually chosen BD_Current_Role__c value unless its 'Member' (where the customer has closed their OneVue account)
		String previousRole = '';
		if (Trigger.OldMap != null)
			previousRole = Trigger.oldMap.get(account.Id).BD_Current_Role__c;
		if (Trigger.isUpdate 
			&& (previousRole == 'Platinum Client' || previousRole == 'Black') 
			&& (previousRole != account.BD_Current_Role__c)
			&& (UserInfo.getName() == 'System Admin')
			&& (account.BD_Current_Role__c != 'Member')) 
		{
			account.BD_Current_Role__c = previousRole;
			account.Force_Push__c = true;
		}
		
		//Get all Account Owners so that we can see if any are inactive and, if so, reallocate the Account
		List<Account> accs = null;
		if (accountOwners.containsKey(account.OwnerId)){
			accs = accountOwners.get(account.OwnerId);
		}
		else {
			accs = new List<Account>();
		}
		accs.add(account);
		accountOwners.put(account.OwnerId, accs);
		
		
		//Email definition that needs sending
		String emailDef = null;
		
		// Onboarding Email: Callout to ET REST Trigger point ONLY when the BD Is Verified has been checked AND they haven't recived it before (BD_Received_Welcome__c)
		if (Trigger.isUpdate && account.BD_Is_Verified__c == TRUE && account.Long_Id__c != null && account.BD_Received_Welcome__c != TRUE && 
				Trigger.OldMap.get(account.Id).BD_Send_Welcome__c == FALSE && account.BD_Send_Welcome__c == TRUE) {
					
			if (account.Knowledge_Level__c == 'Advanced')
				emailDef = 'Advanced Onboarding';
			else
				emailDef = 'Beginner Onboarding';
			account.BD_Received_Welcome__c = TRUE;
		}
		
		//If an email definition has been defined, we need to trigger the send
		if (emailDef != null && AIBMHelper.inWhiteListEmail(account.PersonEmail)) {
			
			//Send the Email
			ExactTarget_REST_Callouts.sendNotification(account.FirstName, account.LastName, account.PersonEmail, account.Subscriber_Key__c, emailDef);
		}
	}
	
	
	//Find any inactive Account Owners and reallocate their Accounts
	List<User> inactiveUsers = [SELECT Id, IsActive FROM User WHERE Id IN :accountOwners.keySet() AND IsActive = false];
	if (inactiveUsers != null && inactiveUsers.size() > 0){
		
		//Find the System admin user account
		User systemAdmin = [SELECT Id FROM User WHERE alias = 'sadm' LIMIT 1];
		
		for (User u : inactiveUsers){
			List<Account> inactiveAccs = accountOwners.get(u.Id);
			if (inactiveAccs != null && inactiveAccs.size() > 0){
				for (Account a : inactiveAccs){
					a.OwnerId = systemAdmin.Id;
				}
			}
		}
	}
	
	
	// update the reference fields for reporting
	if (Trigger.isUpdate) {
		AccountHelper helper;
		helper = new AccountHelper(Trigger.New);
		helper.LoadTransactionsInfo();
		helper.LoadSubscriptionInfo();
	}
	
	// @TODO: can this be moved to a separate class??
	List<AIBM_Settings__c> settings = AIBM_Settings__c.getAll().values();
	Boolean allowDuplicates = false;
    if (!settings.isEmpty()) {
    	allowDuplicates = Boolean.valueOf(settings[0].Allow_Duplicates_Test_Mode__c);
    }
	if (!allowDuplicates) {
		Map<String, Account> accountMap = new Map<String, Account>();
	    for (Account account : Trigger.new) {
			// don't treat an email address that isn't changing during an update as a duplicate.
	        if ((account.PersonEmail != null) && (Trigger.isInsert || (account.PersonEmail != Trigger.oldMap.get(account.Id).PersonEmail))) {
	            // ensure new account isn't also a duplicate  
	            if (accountMap.containsKey(account.PersonEmail)) {
	            	account.PersonEmail.addError('Another account has the same email address.');
	            } else {
	                accountMap.put(account.PersonEmail, account);
	            }
	        }
	    }
		
	    // find all the accounts in the database that have the same email address as the accounts being inserted or updated.
	    List<Account> duplicateAccounts = [SELECT PersonEmail FROM Account WHERE PersonEmail IN :accountMap.KeySet()];
	    for (Account account : duplicateAccounts) {
	    	Account newAccount = accountMap.get(account.PersonEmail);
	   		newAccount.PersonEmail.addError('An account with this email address already exists.');
	    }
	}
}