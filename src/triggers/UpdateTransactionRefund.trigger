trigger UpdateTransactionRefund on go1__Transaction__c (before insert) {
	for (go1__Transaction__c trans : Trigger.New) {
		// check if transaction is a refund
		Id id = (Id)trans.get('go1__Transaction__c');
		if (id != null) {
			// assign the subscription instance to the new refund object so that the transaction is displayed on the subscription
			go1__Transaction__c parentTransaction = [SELECT Id, Subscription_Instance__c FROM go1__Transaction__c WHERE Id = :id];
			trans.Subscription_Instance__c = parentTransaction.Subscription_Instance__c;
		}
	}
}