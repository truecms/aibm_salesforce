trigger NewSubscriptionProduct on Subscription_Product__c (before insert, before update) {
	
	if (Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
		for (Subscription_Product__c sub : Trigger.new) {
			// update the number of payments field if null
			if (sub.Number_of_Payments__c == null && sub.Subscription_Type__c == 'm' && sub.Duration_In_Months__c != null) {
				sub.Number_of_Payments__c = sub.Duration_In_Months__c;
			}
		}
	}
}