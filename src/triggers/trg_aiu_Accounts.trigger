/**
 * Trigger: After Insert, After Update on Account
 *
 * Detects changes in the BrightDay role field on the Account and creates new User Role History instances when these changes occur 
 * Also creates a new Opportunity where the Account has just registered for BD (whether Create or Update)
 *
 * Author: tailbyr
 * Copyright: EurekaReport.com.au
 */
trigger trg_aiu_Accounts on Account (after insert, after update) {
    
    List<User_Role_History__c> roleHistory = new List<User_Role_History__c>();
    List<Account> investmentAccs = null;
    Map<Id, Account> taskAccs = null;
    BrightDay_Settings__c settings = BrightDay_Settings__c.getInstance();
    
    List<Opportunity> opps = new List<Opportunity>();
    
    //Find the System Admin User Id
    User sysAdmin = [SELECT Id FROM User WHERE alias = 'sadm' LIMIT 1];
    
    Map<Id, Id> acctOwnerMapping = new Map<Id, Id>(); //Maps the account id to the owner id for updating Opportunities
    
    for (Account a : Trigger.new) {
        //See if there is a new value for the BrightDay Current Role field
        String currentRole = a.BD_Current_Role__c;
        String previousRole = '';
        Id previousAccountOwner = null;
        Date previousOVCreateDate = null;
        Date previousOVStatusChangeDate = null;
        if (Trigger.OldMap != null){
            previousRole = Trigger.oldMap.get(a.Id).BD_Current_Role__c;
            previousAccountOwner = Trigger.oldMap.get(a.Id).OwnerId;
            previousOVCreateDate = Trigger.oldMap.get(a.Id).OV_Created_Date__c;
            previousOVStatusChangeDate = Trigger.oldMap.get(a.Id).OV_Status_Change_Date__c;
        }
        
        //If current role is nothing and previous role has a value, set the current role in history to be 'none' (value required!)
        if ((currentRole == null || currentRole == '')) {
            if (previousRole != null && previousRole != '')
                currentRole = 'None';
            else
                continue; //Both current and previous roles are blank, don't need to store anything
        }
        //Only create the Opportunity if the Current Role is being initialised AND the Owner is being set OR changing from System Admin thru workflow (to get around the multi-workflow trigger fire issue)
        else if ((previousRole == null || previousRole == '') 
            && (currentRole == 'Premium Member' || currentRole == 'Member' || 
                    (currentRole == 'Premium Client' && a.OV_Created_Date__c == previousOVCreateDate && a.OV_Status_Change_Date__c == previousOVStatusChangeDate)
                )
            && (previousAccountOwner == null || a.OwnerId == previousAccountOwner || (a.OwnerId == sysAdmin.Id && previousAccountOwner != a.OwnerId))) { 
            
            //The Account has a current value and did not have a previous value - it needs a new Opportunity for following up!
            String businessType = '';
            if (trigger.isInsert)
                businessType = 'New Business';
            else
                businessType = 'Existing Business';
            
            OpportunityHelper oHelper = new OpportunityHelper(a, 'brightday registration');
            oHelper.setOpportunityValues(a.OwnerId, businessType, Date.today().addDays(30), '');
            
            opps.add(oHelper.opportunity);
        }
        
        //If an Account was inserted and Opportunities were assigned, the Account would then be allocated to a Sales User and the Opportunities will be assigned to SysAdmin - we need to update these to the same as the Account
        if (previousAccountOwner != null && previousAccountOwner != a.OwnerId) {
            acctOwnerMapping.put(a.Id, a.OwnerId);
        }
        
        
        if (previousRole == null) //If no previous role, just store a blank string
            previousRole = '';
        
        /* There is one specific scenario where the workflow that ticks the 'OV User Object Created' field and adds a date value to 'OV Created Date' 
           will cause the triggers to be re-evaluated and, due to SF's order of execution, will create 2 User_Role_History entries rather than 1.
           We can handle this by not creating a new role history when the 'OV Created Date' is updated as this is ONLY set by workflow, NOT Drupal. 
           Therefore the first trigger execution when the role field is updated will be the only action causing a User_Role_History to be created. 
           Same case for updating the Account Owner. Drupal NEVER changes the Account Owner and through the UI it can only be done in isolation
           i.e. Current Role cannot be changed at the same time.  
        */
        if (currentRole != previousRole 
            && (a.OV_Created_Date__c == null || a.OV_Created_Date__c == previousOVCreateDate)
            && (previousAccountOwner == null || a.OwnerId == previousAccountOwner)) {
                
            //Create a new User Role History instance
            User_Role_History__c urh = new User_Role_History__c();
            urh.Name = currentRole;
            urh.Previous_Role__c = previousRole;
            urh.Account__c = a.Id;
            urh.Date_Granted__c = Datetime.now();
            urh.Changed_By__c = UserInfo.getUserId(); //Find the user making the Account change
            if (UserInfo.getName() == 'System Admin')
                urh.Manual_Change__c = false;
            else
                urh.Manual_Change__c = true;
            roleHistory.add(urh);
        }
        
        //If BD_Current_Role is populated and the change is being made by the System User (Drupal), call the Investment Account manipulator
        if (a.Products_Raw__c != null && UserInfo.getName() == 'System Admin') {
            if (investmentAccs == null)
                investmentAccs = new List<Account>();
            
            investmentAccs.add(a);
        }
        
        //Now, check that if the Account is a 'Premium Client' with only a Direct investment account, they still have enough money to qualify for the role
        if (Trigger.isUpdate && a.BD_Current_Role__c == 'Premium Client' 
            && a.Total_Investment_Value__c < 180000 && Trigger.oldMap.get(a.id).Total_Investment_Value__c >= 180000) 
        {
            if (taskAccs == null)
                taskAccs = new Map<Id, Account>();
                
            taskAccs.put(a.Id, a);
        }
    }
    
    insert roleHistory;
    
    //If we have any Opportunities to insert, do this now
    //Below 2 lines were commented out on 5/10/2015 to trun off the automatic creation of opportunities for brightday.
   // if (opps.size() > 0)
       // insert opps; 
        
    //Also if there are Accounts that have newly been allocated a sales user and have Opps, reallocate the Opps too
    if (acctOwnerMapping.size() > 0) {
        List<Opportunity> updateOpps = [SELECT Id, OwnerId, AccountId FROM Opportunity WHERE AccountId IN :acctOwnerMapping.keySet()];
        for (Opportunity o : updateOpps) {
            if (acctOwnerMapping.containsKey(o.AccountId))
                o.OwnerId = acctOwnerMapping.get(o.AccountId);
        }
        
        update updateOpps;
    }
    
    
    //If our InvestmentAccs collection contains accounts, we will need to process the Investment Account product raw data
    if (investmentAccs != null && investmentAccs.size() > 0){
        InvestmentAccountHelper iah = new InvestmentAccountHelper(investmentAccs);
        iah.processAccountProducts();
    }
    
    //If our TaskAccs collection contains accounts, we will need to create tasks against these accounts for the sales team to follow up re downgrades
    if (taskAccs != null && taskAccs.size() > 0) {
        
        List<Investment_Account__c> nonDowngradeAccounts = [SELECT Id, Investment_Account_Type__c, Account__c 
                                                        FROM Investment_Account__c 
                                                        WHERE Account__c = :taskAccs.keySet() 
                                                        AND Investment_Account_Type__c != 'DIRECT'];

        //If the account appears in nonDowngradeAccounts, they have an account other than DIRECT and are therefore safe
        for (Investment_Account__c invAcc : nonDowngradeAccounts) {
                taskAccs.remove(invAcc.Account__c);
        }
        
        String roleChangeSubject = TaskManager.ROLE_CHANGE_SUBJECT;
        Database.executeBatch(new BDRoleChangeTasks(taskAccs.values(), roleChangeSubject)); //Use default batch size of 200
    }
}