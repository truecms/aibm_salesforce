trigger SuspendRecurringInstance on Product_Instance__c (after insert, after update) {
	
	Map<Id, Product_Instance__c> recurringInsts = new Map<Id, Product_Instance__c>();
	for (Product_Instance__c subscription : Trigger.New) {
		if (subscription.Recurring_Instance__c != null) {
			recurringInsts.put(subscription.Recurring_Instance__c, subscription);
		}
	}
	
	List<go1__Recurring_Instance__c> recurrings = [SELECT Id, Suspend_Start_Date__c, Suspend_End_date__c, go1__Next_Renewal__c, go1__Next_Attempt__c 
													FROM go1__Recurring_Instance__c 
													WHERE Id IN :recurringInsts.keySet()];
	
	for (go1__Recurring_Instance__c recurring : recurrings) {
		try {
			//Boolean to check if we're actually updating any fields on recurring inst - if not, don't trigger the recurring inst update!
			Boolean updateRI = false;
			
			//Get the Product Instance from the original map
			Product_Instance__c subscription = recurringInsts.get(recurring.Id);
			
			// when updating recurring object end date based on suspend suspend period
        	Integer oldSuspendDays = 0;
            if (recurring.Suspend_Start_Date__c != null && recurring.Suspend_End_Date__c != null) {
                oldSuspendDays = Date.valueOf(recurring.Suspend_Start_Date__c).daysBetween(Date.valueOf(recurring.Suspend_End_Date__c));
            }
               
            // modify the suspend date fields to be the same as the fields on the subscription object
            recurring.Suspend_Start_Date__c = (Datetime)subscription.Suspend_Subscription_Start__c;
            recurring.Suspend_End_Date__c = (Datetime)subscription.Suspend_Subscription_End__c;
                
            Integer newSuspendDays = 0;
            if (recurring.Suspend_Start_Date__c != null && recurring.Suspend_End_Date__c != null) {
                newSuspendDays = Date.valueOf(recurring.Suspend_Start_Date__c).daysBetween(Date.valueOf(recurring.Suspend_End_Date__c));
            }
            
            if (oldSuspendDays != 0 || newSuspendDays != 0) {
            	recurring.go1__Next_Attempt__c = recurring.go1__Next_Attempt__c.addDays(newSuspendDays - oldSuspendDays);
            	recurring.go1__Next_Renewal__c = recurring.go1__Next_Renewal__c.addDays(newSuspendDays - oldSuspendDays);
            	updateRI = true; //UPdating fields on RI, therefore RI DML update required
            }

            // check if payment occurs immediately
            if (Date.valueOf(recurring.go1__Next_Renewal__c).daysBetween(Date.today()) == 0) {
                // calculate difference between next renewal and next attempt
                Integer diff = Integer.valueOf(recurring.go1__Next_Attempt__c.getTime() - recurring.go1__Next_Renewal__c.getTime());
                    
                // update new renewal date
                recurring.go1__Next_Renewal__c = (Datetime)subscription.Start_Date__c;
                recurring.go1__Next_Attempt__c = recurring.go1__Next_Renewal__c.addDays(diff);
                
                updateRI = true; //Updating fields on RI, therefore RI DML update required
            }
            
            if (updateRI == true)
            	update recurring;
            	
		} catch (DMLException e) {
            System.debug(e);
        }
	}
}