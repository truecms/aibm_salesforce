/**
* tailbyr 20140516: deprecating and removing the code in this trigger as it was originally developed badly by Go1 for automated campaigns, 
* which was never released. 
*
* Intended use: if the Campaign.Only_New_Subscribers__c is selected, existing Accounts/Subscribers cannot be added to a Campaign. If it is not
* selected, then new and existing Accounts can be added to the Campaign. This is for the purpose of limiting special offers to only those
* subscribers who have just signed up. 
*
* Deprecated: providing the removal of this code has no knock-on effects, the trigger will be deactivated and removed in the next release
*/
trigger OnlyNewCampaignMembers on CampaignMember (after insert) {
	/*Campaign c = [select Id, name, Only_New_Subscribers__c, No_Paid_Subs__c, Excluded_Members__c from Campaign where Id = :Trigger.new[0].CampaignId limit 1];
   	
   	List<String> excludedEmails = new List<String>();
   	List<Lead> leadsToDelete = new List<Lead>();
   	List<CampaignMember> membersToDelete = new List<CampaignMember>();
   	List<CampaignMember> membersToInsert = new List<CampaignMember>();
   	List<Database.LeadConvert> leadsToConvert = new List<Database.LeadConvert>();
   	
   	Id[] ids = new Id[Trigger.new.size()];
   	for (CampaignMember cm : Trigger.new) {
   		if (cm.LeadId != null) {
   			ids.add(cm.LeadId);
   		}
   	}
   	
   	List<Lead> leads = [SELECT Id, Email FROM Lead WHERE Id IN :ids AND IsConverted = false];
   	String[] leadEmails = new String[leads.size()];
   	for (Integer i=0; i<leads.size(); i++) {
   		leadEmails[i] = leads[i].Email;
   	}
   	
   	List<Account> accounts = [SELECT Id, PersonEmail, PersonContactId, ER_Reporting_Sub_End_Date__c FROM Account WHERE PersonEmail IN :leadEmails];
   	Set<String> accountEmails = new Set<String>();
   	for (Account account : accounts) {
   		accountEmails.add(account.PersonEmail);
   	}
   	
   	for (Lead lead : leads) {
    	if (accountEmails.contains(lead.Email)) {
    		excludedEmails.add(lead.Email);
    		CampaignMember campaignMember = [SELECT Id FROM CampaignMember WHERE LeadId = :lead.Id AND CampaignId = :c.Id LIMIT 1];
    		membersToDelete.add(campaignMember);
    		leadsToDelete.add(lead);
    		
    		if (!c.Only_New_Subscribers__c) {
    			for (Account account : accounts) {
    				if (account.PersonEmail == lead.Email) {
    					if (!c.No_Paid_Subs__c || (c.No_Paid_Subs__c && account.ER_Reporting_Sub_End_Date__c == null)) {
	    					CampaignMember newCampaignMember = new CampaignMember(CampaignId=c.Id, ContactId=account.PersonContactId);
	    					membersToInsert.add(newCampaignMember);
	    				}
    				} 
    			}
    		}
		} else {
    		Database.LeadConvert leadConv = new database.LeadConvert();
    		leadConv.setLeadId(lead.Id);
    		leadConv.setDoNotCreateOpportunity(true);
    		leadConv.convertedStatus = 'Qualified';
    		leadsToConvert.add(leadConv);
    	}	
    }
	
	delete membersToDelete;
	delete leadsToDelete;
	insert membersToInsert;
	Database.LeadConvertResult[] lcr = Database.convertLead((Database.LeadConvert[])leadsToConvert);
	
	if (c.Only_New_Subscribers__c) {
		String allExcludedEmails = '';
		for (Integer i=0; i<excludedEmails.size(); i++) {
			if (i == excludedEmails.size() - 1) {
				allExcludedEmails += excludedEmails[i];
			} else {
				allExcludedEmails += excludedEmails[i] + ', ';
			}
		}
		if ((c.Excluded_Members__c != null && c.Excluded_Members__c != '') && allExcludedEmails != '') {
			c.Excluded_Members__c += ', ' + allExcludedEmails;
		} else {
			c.Excluded_Members__c = allExcludedEmails;
		}
		update c;
	}*/
}