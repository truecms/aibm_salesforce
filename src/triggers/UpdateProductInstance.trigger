/**
 * This triggers handle actions to take when inserting/updating ProductInstances
 *
 * Actions:
 *  - update account lookup fields to correct product instances (e.g. current ER subscription)
 *  -
 *
 */
trigger UpdateProductInstance on Product_Instance__c (before insert, before update, after insert, after update) {
	ProductInstanceHelper helper;
	
	// update the accounts based on new information loaded into product instances
	if (Trigger.isAfter){
		helper = new ProductInstanceHelper(Trigger.New);
		helper.UpdateAccounts();
		
		// seperate trigger to add campaign member once the product has been saved
		if (Trigger.New.size() <= 1 && Trigger.isInsert) {
			
			for (Product_Instance__c sub : Trigger.new) {
				// add a campaign member if the product is associated with a campaign
				if (sub.Campaign__c != null) {
					helper.addCampaignMember();
				}
			}
		}
		
	}
	
	// @todo - can we make this work when triggered on more then one item
	if (Trigger.new.size() <= 1 && Trigger.isBefore && Trigger.isUpdate){
		for (Product_Instance__c sub : Trigger.New) {
	        
	        helper = new ProductInstanceHelper(sub);
	        
	        //Check if we need to validate/update the Subscription stream - only if the subscription stream has changed in the update
	        if ((sub.Strean__c != Trigger.oldMap.get(sub.Id).Strean__c) || (sub.Subscription_Stream__c == null && sub.Strean__c != null)){
	        	helper.validateSubscriptionStream();
	        }
	        
	        Product_Instance__c oldSub = Trigger.oldMap.get(sub.Id);
			helper.updateSubscriptionDates(oldSub);
			
			// add a campaign member if the product is associated with a campaign that wasn't previously
			if (oldSub.Campaign__c == null && sub.Campaign__c != null) {
				helper.addCampaignMember();
			}
		}
	}
	
	// this was previously in a trigger called NewProductInstance
	if (Trigger.New.size() <= 1 && Trigger.isBefore && Trigger.isInsert) {
		
		String defaultCampaign = ProductInstanceHelper.DEFAULT_CAMPAIGN;
		List<Campaign> campaigns = [SELECT Id FROM Campaign WHERE Id =: defaultCampaign LIMIT 1];
		
		for (Product_Instance__c sub : Trigger.new) {
			helper = new ProductInstanceHelper(sub);
	        helper.validateSubscriptionStream();
	        // don't auto renew free subs
			helper.setAutoRenewal();
			helper.setCampaign();
			helper.setIsRenewal();
			helper.setPeriod();
			helper.setSuspendDays();
			
	    	sub = helper.instance;

			if (sub.Campaign__c == null && !campaigns.isEmpty())
			{
				// assign a default campaign
				sub.Campaign__c = defaultCampaign;
			}
			
			//Also set the Original Start and End dates - only on beforeInsert! They should not be updated any other time
			if (sub.Start_Date__c != null) {
				sub.Original_Start_Date__c = sub.Start_Date__c;
			}
			if (sub.End_Date__c != null) {
				sub.Original_End_Date__c = sub.End_Date__c;
			}
		}
	}
}