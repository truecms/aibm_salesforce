trigger trg_ai_Task on Task (after insert, after update) {
	
	Map<String, String> emailDirectionMap = new Map<String, String>();
	
	for (Task t : Trigger.New) {
		
		//If this is an update, we only want to run this code if the 'WhatId' has changed. Always runs for inserts.
		if ((Trigger.isUpdate && Trigger.OldMap != null && Trigger.oldMap.get(t.Id).WhatId != t.WhatId)
			|| Trigger.isInsert) {
		
			//Initially check that this is an Email task
			if (t.Subject.startsWith('Email:')){
			
				//Query the Description to see if it contains the word 'emailtosalesforce@'
				String descString = t.description;
				
				String matchPattern = 'emailtosalesforce@';
				
				if (descString != null && descString.contains(matchPattern)) {
					//This is an incoming email
					emailDirectionMap.put(t.WhatId, 'inbound');
				}
				else {
					emailDirectionMap.put(t.WhatId, 'outbound');
				}
			}
		}
	}
	
	//Fetch the Insurance Quotes affected by the task updates
	List<Insurance_Quote__c> insQuotes = [SELECT Id, Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id IN : emailDirectionMap.keySet()];
	
	//For each insurance quote, set the inbound or outbound direction set in the direction map
	for (Insurance_Quote__c quote : insQuotes) {
		String direction = emailDirectionMap.get(quote.Id);
		
		if (direction != null) {
			quote.Last_Activity_Direction__c = direction;
		}
	}
	
	//Update Insurance Quotes
	update insQuotes;
}