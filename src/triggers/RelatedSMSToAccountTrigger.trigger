trigger RelatedSMSToAccountTrigger on smagicinteract__Incoming_SMS__c (before insert) 
{
	String senderMobileNumber = null; //0412345678                              
    String senderMobileNumber2 = null; //61412345678                
    String senderMobileNumber3 = null; //04 1234 5678   
    String senderMobileNumber4 = null; //(04) 1234 5678
    String senderMobileNumber5 = null; //0412 345 678

    for (smagicinteract__Incoming_SMS__c sms : Trigger.new)
    {                     
    	senderMobileNumber = sms.smagicinteract__Mobile_Number__c;
    	senderMobileNumber2 = senderMobileNumber;
    	
    	//Check if the incoming number has the country code (61) in front of it - strip off if so
        if (senderMobileNumber.length() == 11 && senderMobileNumber.substring(0,2) == '61')
        {
        	senderMobileNumber = senderMobileNumber.substring(2,senderMobileNumber.length());   
        }   
                         
		// If number has no country/area code, or has already been stripped, add the leading zero
        if(senderMobileNumber.length() == 9)
        {                          
        	senderMobileNumber = '0'+senderMobileNumber;
        }            
		
		if(senderMobileNumber.length() == 10)
		{                                                          
        	senderMobileNumber3 = senderMobileNumber.substring(0,2) + ' ' + senderMobileNumber.substring(2,6) + ' ' + senderMobileNumber.substring(6,10);                       
        	senderMobileNumber4 = '(' + senderMobileNumber.substring(0,2) + ') ' + senderMobileNumber.substring(2,6) + ' ' + senderMobileNumber.substring(6,10);
        	senderMobileNumber5 = senderMobileNumber.substring(0,4) + ' ' + senderMobileNumber.substring(4,7) + ' ' + senderMobileNumber.substring(7, 10);
		}
        
       	String soslQuery = 'FIND \'' + senderMobileNumber + ' OR ' + senderMobileNumber2;
        if (senderMobileNumber3 != null && senderMobileNumber3.length() > 0)
        	soslQuery += ' OR ' + senderMobileNumber3;
        if (senderMobileNumber4 != null && senderMobileNumber4.length() > 0)
        	soslQuery += ' OR ' + senderMobileNumber4;
        if (senderMobileNumber5 != null && senderMobileNumber5.length() > 0)
        	soslQuery += ' OR ' + senderMobileNumber5;
        	
        soslQuery += '\' IN PHONE FIELDS RETURNING Account(Id, LastModifiedDate ORDER BY LastModifiedDate DESC)';
        	
        List<List<sObject>> accList = search.query(soslQuery);
        
        if(accList.size() > 0)
        {
        	List<Account> a = accList[0];
        	if (a.size() > 0)                
        		sms.Account__c = a[0].Id;
		}                                                              
    }         
}