trigger UpdateRecurringInstance on go1__Recurring_Instance__c (before insert, before update) {
	try {
		if (Trigger.New.size() <= 1) {
			
			for (go1__Recurring_Instance__c recurring : Trigger.new) {
				// if cancelled and remaining attempts != 0, set remaining attempts to 0
				if (recurring.go1__Cancelled__c == true && recurring.go1__Remaining_Attempts__c != 0) {
					// check that the product instance has actually cancelled and not just an error 
					List<Product_Instance__c> subscriptions = [SELECT Id, Status__c, End_Date__c FROM Product_Instance__c WHERE Recurring_Instance__c = :recurring.Id LIMIT 1];
					if (!subscriptions.isEmpty()) {
						if (subscriptions[0].Status__c == 'Cancelled' || subscriptions[0].End_Date__c < Date.Today()) {
							recurring.go1__Remaining_Attempts__c = 0;
						}
						
						List<go1__Transaction__c> transactions = [SELECT Id, go1__Code__c FROM go1__Transaction__c WHERE Subscription_Instance__c = :subscriptions[0].Id ORDER BY go1__Date__c DESC LIMIT 1];
						if ((!transactions.isEmpty() && transactions[0].go1__Code__c < 10) || transactions.isEmpty()) {
							recurring.go1__Remaining_Attempts__c = 0;
						}
					}
				}
			}
		}
	}
	catch (Exception e) {
		// as this object is part of Go2Debit we do not want a business logic error causing update errors.
		System.debug(e);
	}
}