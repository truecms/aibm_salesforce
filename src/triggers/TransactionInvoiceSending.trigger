trigger TransactionInvoiceSending on go1__Transaction__c (after insert)
{
	try {
		TransactionManager.sendInvoiceEmail(Trigger.newMap.keySet());
	}
	catch (Exception e) {
		// as this object is part of Go2Debit we do not want a business logic error causing update errors.
		System.debug(e);
	}
}