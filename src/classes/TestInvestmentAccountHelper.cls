/**
 * Testing the InvestmentAccountHelper class
 * Author: tailbyr
 * Copyright: eurekareport.com.au
 */
@isTest
public with sharing class TestInvestmentAccountHelper {

	public static TestMethod void testInvestmentAccountCreate(){
		//Create a test account
		List<Account> accs = TestAccountHelper.createAccounts(1);
    	Account account = accs[0];
    	
    	//Create the Investment Account JSON block to associate to the Products_Raw field
    	InvestmentAccount ia = createInvestmentAccount(1, 'SMSF', 'ACTIVE');
    		
    	Map<String, Object> accounts = new Map<String, Object>();
		accounts.put('1001', ia);
		
		//Put the Account map into another holder Map to mirror the structure we'll get from OneVue
		Map<String, Object> productsRaw = new Map<String, Object>();
		productsRaw.put('accounts', accounts);
    		
		account.Products_Raw__c = serializeMapToJSON(productsRaw);
    	account.BD_Current_Role__c = 'Client';
    	
    	//Run as the Drupal user - the only user who can make undates to these fields
    	System.runAs(TestLatePaperworkTasks.getDrupalUser()){
    		Test.startTest();
    		update account;
    		Test.stopTest();
    	}
    	
    	List<Investment_Account__c> invAccs = [SELECT Id, Name, Account_Number__c, Code__c, Investment_Account_Type__c, Status__c, AssetAllocation__c FROM Investment_Account__c WHERE Account__c = :account.Id];
    	System.assertEquals(invAccs.size(), 1);
    	System.assertEquals(invAccs[0].Name, 'Eureka Demo Account 1');
    	System.assertEquals(invAccs[0].Code__c, 'EU000001');
    	System.assertEquals(invAccs[0].Account_Number__c, '1001');
    	System.assertEquals(invAccs[0].Investment_Account_Type__c, 'SMSF');
    	System.assertEquals(invAccs[0].Status__c, 'ACTIVE');
    	System.assertEquals(invAccs[0].AssetAllocation__c, serializeAssetsToJSON());
	}

	public static TestMethod void testInvestmentAccountUpdate(){
		//Create a test account
		List<Account> accs = TestAccountHelper.createAccounts(1);
    	Account account = accs[0];
    	
    	//Initially check we don't have any investment accounts
    	List<Investment_Account__c> emptyInvAccs = [SELECT Id, Name, Account_Number__c, Code__c, Investment_Account_Type__c, Status__c FROM Investment_Account__c WHERE Account__c = :account.Id];
    	System.assertEquals(emptyInvAccs.size(), 0);
    	
    	//Create the Investment Account JSON block to associate to the Products_Raw field
    	InvestmentAccount ia1 = createInvestmentAccount(1, 'SMSF', 'ACTIVE');
    	InvestmentAccount ia2 = createInvestmentAccount(2, 'INDIVIDUAL', 'NEW');
    	InvestmentAccount ia3 = createInvestmentAccount(3, 'DIRECT', 'ACTIVE');
    		
    	Map<String, Object> accounts = new Map<String, Object>();
		accounts.put('1001', ia1);
		accounts.put('1002', ia2);
		accounts.put('1003', ia3);
		
		//Put the Account map into another holder Map to mirror the structure we'll get from OneVue
		Map<String, Object> productsRaw = new Map<String, Object>();
		productsRaw.put('accounts', accounts);
		
    	account.Products_Raw__c = serializeMapToJSON(productsRaw);
    	account.BD_Current_Role__c = 'Client';
    	
    	//Run as the Drupal user - the only user who can make undates to these fields
    	System.runAs(TestLatePaperworkTasks.getDrupalUser()){
    		
    		update account; //This Account update should initially create the 3 investment accounts.
    	
    		//Check that the Investment Accounts are created
    		List<Investment_Account__c> preInvAccs = [SELECT Id, Name, Account_Number__c, Code__c, Investment_Account_Type__c, Status__c FROM Investment_Account__c WHERE Account__c = :account.Id];
    		System.assertEquals(preInvAccs.size(), 3);
    	
    		ia1.code = 'EU12345';
    		ia2.post_code = '4321';
    		ia3.type = 'NONSENSE';
    		
    		//Also change the content of the assetAllocation block to check that is updated
    		ia3.property.value = '2525.00';
    		ia3.property.percent = '25.25';
    		
    		account.Products_Raw__c = serializeMapToJSON(productsRaw);
    	
    		Test.startTest();
    		update account;
    		Test.stopTest();
    	}
    	
    	List<Investment_Account__c> invAccs = [SELECT Id, Name, Account_Number__c, Code__c, Investment_Account_Type__c, Status__c, PostCode__c, AssetAllocation__c 
    											FROM Investment_Account__c WHERE Account__c = :account.Id];
    	
    	//Create JSON test string for the asset allocation, with the test change
    	propertyVal = 25.25;
    	String assetTestString = serializeAssetsToJSON();
    	
    	System.assertEquals(invAccs.size(), 3);
    	for (Investment_Account__c ia : invAccs) {
    		if (ia.Account_Number__c == '1001'){
    			System.assertEquals(ia.code__c, 'EU12345');
    		}
    		if (ia.Account_Number__c == '1002'){
    			System.assertEquals(ia.postCode__c, '4321');
    		}
    		if (ia.Account_Number__c == '1003'){
    			System.assertEquals(ia.Investment_Account_Type__c, 'NONSENSE');
    			System.assertEquals(ia.AssetAllocation__c, assetTestString);
    		}
    	}
	}
	
	public static TestMethod void testInvestmentAccountDelete(){
		//Create a test account
		List<Account> accs = TestAccountHelper.createAccounts(1);
    	Account account = accs[0];
    	
    	//Initially check we don't have any investment accounts
    	List<Investment_Account__c> emptyInvAccs = [SELECT Id FROM Investment_Account__c WHERE Account__c = :account.Id];
    	System.assertEquals(emptyInvAccs.size(), 0);
    	
    	//Create the Investment Account JSON block to associate to the Products_Raw field
    	InvestmentAccount ia1 = createInvestmentAccount(1, 'SMSF', 'ACTIVE');
    	InvestmentAccount ia2 = createInvestmentAccount(2, 'INDIVIDUAL', 'NEW');
    	InvestmentAccount ia3 = createInvestmentAccount(3, 'DIRECT', 'ACTIVE');
    		
		Map<String, Object> accounts = new Map<String, Object>();
		accounts.put('1001', ia1);
		accounts.put('1002', ia2);
		accounts.put('1003', ia3);
		
		//Put the Account map into another holder Map to mirror the structure we'll get from OneVue
		Map<String, Object> productsRaw = new Map<String, Object>();
		productsRaw.put('accounts', accounts);
		
    	account.Products_Raw__c = serializeMapToJSON(productsRaw);
    	account.BD_Current_Role__c = 'Client';
    	
    	//Run as the Drupal user - the only user who can make undates to these fields
    	System.runAs(TestLatePaperworkTasks.getDrupalUser()){
    		
    		update account; //This Account update should initially create the 3 investment accounts.
    	
    		//Check that the Investment Accounts are created
    		List<Investment_Account__c> preInvAccs = [SELECT Id FROM Investment_Account__c WHERE Account__c = :account.Id];
    		System.assertEquals(preInvAccs.size(), 3);
    	
    		//Remove the 3rd Investment Account and update the Products_Raw field
    		accounts.remove('1003');
    		account.Products_Raw__c = serializeMapToJSON(productsRaw);
    	
    		Test.startTest();
    		update account;
    		Test.stopTest();
    	}
    	
    	//Test that we only have the 2 correct Investment Accounts
    	List<Investment_Account__c> updateInvAccs = [SELECT Id, Account_Number__c, Investment_Account_Type__c FROM Investment_Account__c WHERE account__c = :account.Id];
    	System.assertEquals(updateInvAccs.size(), 2);
    	for (Investment_Account__c iac : updateInvAccs){
    		if (iac.Account_Number__c == '1001'){
    			System.assertEquals(iac.Investment_Account_Type__c, 'SMSF');
    		}
    		if (iac.Account_Number__c == '1002'){
    			System.assertEquals(iac.Investment_Account_Type__c, 'INDIVIDUAL');
    		}
    	}
	}
	
	public static TestMethod void testCreateAccountWithInvestments(){
		//Create the Account object and populate the Products_Raw data block, but don't insert!
		Account account = new Account(FirstName='Test', LastName='Tester', PersonEmail='test@test.com.test');
		InvestmentAccount ia1 = createInvestmentAccount(1, 'SMSF', 'ACTIVE');
    	InvestmentAccount ia2 = createInvestmentAccount(2, 'INDIVIDUAL', 'NEW');
    	
    	Map<String, Object> accounts = new Map<String, Object>();
		accounts.put('1001', ia1);
		accounts.put('1002', ia2);
		
		//Put the Account map into another holder Map to mirror the structure we'll get from OneVue
		Map<String, Object> productsRaw = new Map<String, Object>();
		productsRaw.put('accounts', accounts);
		
    	account.Products_Raw__c = serializeMapToJSON(productsRaw);
    	account.BD_Current_Role__c = 'Client';
    	
    	System.runAs(TestLatePaperworkTasks.getDrupalUser()){
    		Test.startTest();
    		insert account;
    		Test.stopTest();
    	}
    	
    	List<Investment_Account__c> invAccs = [SELECT Id, Account_Number__c, Investment_Account_Type__c, AssetAllocation__c FROM Investment_Account__c WHERE Account__c = :account.Id];
    	System.assertEquals(invAccs.size(), 2);
    	for (Investment_Account__c iac : invAccs){
    		if (iac.Account_Number__c == '1001'){
    			System.assertEquals(iac.Investment_Account_Type__c, 'SMSF');
    		}
    		if (iac.Account_Number__c == '1002'){
    			System.assertEquals(iac.Investment_Account_Type__c, 'INDIVIDUAL');
    		}
    		
    		System.assertEquals(iac.AssetAllocation__c, serializeAssetsToJSON());
    	}
	}
	
	public static TestMethod void testMultipleAccountsWithInvestments(){
		//Create multiple test accounts
		List<Account> accs = TestAccountHelper.createAccounts(6);
    	
    	//Get the Ids of our new accounts
    	Set<Id> accIds = new Set<Id>();
    	
    	for (Integer i = 0; i < accs.size(); i++){
    		Account a = accs[i];
    		accIds.add(a.Id);
    		
    		//Just create one Investment Account per account
    		InvestmentAccount ia1 = createInvestmentAccount(i, 'SMSF', 'ACTIVE');
    		
    		Map<String, Object> accounts = new Map<String, Object>();
    		accounts.put('100'+i, ia1);
    		
    		//Put the Account map into another holder Map to mirror the structure we'll get from OneVue
			Map<String, Object> productsRaw = new Map<String, Object>();
			productsRaw.put('accounts', accounts);
			String jsonInput = serializeMapToJSON(productsRaw);
			
			a.Products_Raw__c = jsonInput;
    		a.BD_Current_Role__c = 'Premium Client';
    	}
    	
		System.runAs(TestLatePaperworkTasks.getDrupalUser()){
    		Test.startTest();
    		update accs;
    		Test.stopTest();
    	}
    	
    	List<Investment_Account__c> insertedAccs = [SELECT Id, Account__c, Account_Number__c, Investment_Account_Type__c, AssetAllocation__c FROM Investment_Account__c WHERE Account__c IN :accIds];
    	
    	System.assertEquals(insertedAccs.size(), 6);
    	
    	//Check each account has an Investment Account attached
    	for (Investment_Account__c ia : insertedAccs){
    		System.assert(accIds.contains(ia.Account__c));
    		System.assertEquals(ia.AssetAllocation__c, serializeAssetsToJSON());
    		accIds.remove(ia.Account__c); //Remove the Account Id from the Set - then test that the set is empty at the end i.e. all accounts have a matching Investment Acc
    	}
    	System.assertEquals(accIds.size(), 0);
	}
	
	/**
	* Utility Methods
	*/
	static Decimal cashVal = 9.00;
	static Decimal equitiesVal = 10.00;
	static Decimal fixedInterestVal = 11.00;
	static Decimal intlEquitiesVal = 12.00;
	static Decimal intlFixedInterestVal = 13.00;
	static Decimal otherVal = 14.00;
	static Decimal propertyVal = 15.00;
	static Decimal propertyTrustsVal = 16.00;
	
	static String serializeCollectionToJSON(List<InvestmentAccount> invAccs){
		return JSON.serialize(invAccs);
	}
	
	static String serializeMapToJSON(Map<String, Object> invAccs){
		return JSON.serialize(invAccs);
	}
	
	static String serializeAssetsToJSON(){
		return '[{"value":' + cashVal*100 + ',"percentage":' + cashVal + ',"asset":"Cash"},' +
			   '{"value":' + equitiesVal*100 + ',"percentage":' + equitiesVal + ',"asset":"Equities"},' +
			   '{"value":' + fixedInterestVal*100 + ',"percentage":' + fixedInterestVal + ',"asset":"Fixed Interest"},' +
			   '{"value":' + intlEquitiesVal*100 + ',"percentage":' + intlEquitiesVal + ',"asset":"International Equities"},' +
			   '{"value":' + intlFixedInterestVal*100 + ',"percentage":' + intlFixedInterestVal + ',"asset":"International Fixed Interest"},' +
			   '{"value":' + otherVal*100 + ',"percentage":' + otherVal + ',"asset":"Other"},' +
			   '{"value":' + propertyVal*100 + ',"percentage":' + propertyVal + ',"asset":"Property"},' +
			   '{"value":' + propertyTrustsVal*100 + ',"percentage":' + propertyTrustsVal + ',"asset":"Property Trusts"}]';
	}
	
	static InvestmentAccount createInvestmentAccount(Integer i, String iaType, String status){
		InvestmentAccount ia = new InvestmentAccount(false, 
    		'123 Demo St', 
    		'EU00000'+i, 
    		'Demo User '+i, 
    		'AUS', 
    		'E1', 
    		'demo.user'+i+'@testbucket.net.unittest', 
    		1000+i, 
    		'2014-07-31T08:33:57.151+10:00', 
    		'Eureka Demo Account '+i, 
    		i, 
    		'0400123789', 
    		'3000', 
    		'Mr', 
    		'27A08EDF-6565-420A-A91E-A75662E99206', 
    		'VIC', 
    		status, 
    		'Melbourne', 
    		'0', 
    		iaType,
    		'2014-07-31T08:33:57.151+10:00',
    		'1500.00');
    		
		//Also setup the figures required for the asset allocation data
		ia.setupAssetAllocation(String.valueOf(cashVal),
							String.valueOf(cashVal*100), 
							String.valueOf(equitiesVal),
							String.valueOf(equitiesVal*100), 
							String.valueOf(fixedInterestVal),
							String.valueOf(fixedInterestVal*100), 
							String.valueOf(intlEquitiesVal),
							String.valueOf(intlEquitiesVal*100), 
							String.valueOf(intlFixedInterestVal),
							String.valueOf(intlFixedInterestVal*100), 
							String.valueOf(otherVal),
							String.valueOf(otherVal*100), 
							String.valueOf(propertyVal),
							String.valueOf(propertyVal*100), 
							String.valueOf(propertyTrustsVal),
							String.valueOf(propertyTrustsVal*100));
		
    	return ia;
	}
	
	
	
	/* Small inner class to represent an asset against the Investment Account
	*/
	class InvAsset{
		String percent {get; set;}
		String value {get; set;}
		
		InvAsset(String percent, String value){
			this.percent = percent;
			this.value = value;
		}
	}
	
	/* Inner class used to simulate creation of the Products JSON data dump coming from OneVue via Drupal
	*/
	class InvestmentAccount{
		Boolean persisted {get; set;}
		String address {get; set;}
		String code {get; set;}
		String contact_name {get; set;}
		String country {get; set;}
		String domain {get; set;}
		String email {get; set;}
		Integer account_id {get; set;}
		String insertDate {get; set;}
		String account_name {get; set;}
		Integer parentId {get; set;}
		String phone {get; set;}
		String post_code {get; set;}
		String salutation {get; set;}
		String sessionId {get; set;}
		String state {get; set;}
		String status {get; set;}
		String suburb {get; set;}
		String taskId {get; set;}
		String type {get; set;}
		String updateDate {get; set;}
		String value {get; set;}
		
		InvAsset cash {get; set;}
		InvAsset equities {get; set;}
		InvAsset fixed_interest {get; set;}
		InvAsset intl_interest {get; set;}
		InvAsset intl_fixed_interest {get; set;}
		InvAsset other {get; set;}
		InvAsset property {get; set;}
		InvAsset property_trusts {get; set;}
		
		InvestmentAccount(Boolean persisted,
							String address,
							String code,
							String contact_name,
							String country,
							String domain,
							String email,
							Integer account_id,
							String insertDate,
							String account_name,
							Integer parentId,
							String phone,
							String post_code,
							String salutation,
							String sessionId,
							String state,
							String status,
							String suburb,
							String taskId,
							String type,
							String updateDate,
							String totalValue){
			this.persisted = persisted;
			this.address = address;
			this.code = code;
			this.contact_name = contact_name;
			this.country = country;
			this.domain = domain;
			this.email = email;
			this.account_id = account_id;
			this.insertDate = insertDate;
			this.account_name = account_name;
			this.parentId = parentId;
			this.phone = phone;
			this.post_code = post_code;
			this.salutation = salutation;
			this.sessionId = sessionId;
			this.state = state;
			this.status = status;
			this.suburb = suburb;
			this.taskId = taskId;
			this.type = type;
			this.updateDate = updateDate;
			this.value = totalValue;
		}
		
		void setupAssetAllocation(String cashPercent,
							String cashValue,
							String equitiesPercent,
							String equitiesValue, 
							String fixedInterestPercent,
							String fixedInterestValue,
							String intlEquitiesPercent,
							String intlEquitiesValue,
							String intlFixedInterestPercent,
							String intlFixedInterestValue,
							String otherPercent,
							String otherValue, 
							String propertyPercent,
							String propertyValue, 
							String propertyTrustsPercent,
							String propertyTrustsValue){
								
			this.cash = new InvAsset(cashPercent, cashValue);
			this.equities = new InvAsset(equitiesPercent, equitiesValue);
			this.fixed_interest = new InvAsset(fixedInterestPercent, fixedInterestValue);
			this.intl_interest = new InvAsset(intlEquitiesPercent, intlEquitiesValue);
			this.intl_fixed_interest = new InvAsset(intlFixedInterestPercent, intlFixedInterestValue);
			this.other = new InvAsset(otherPercent, otherValue);
			this.property = new InvAsset(propertyPercent, propertyValue);
			this.property_trusts = new InvAsset(propertyTrustsPercent, propertyTrustsValue);					
		}
		
		String serializeToJSON(){
			return JSON.serialize(this);
		}
	}
}