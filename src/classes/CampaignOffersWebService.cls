/**
 * CampaignOffersWebService
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
@RestResource(urlMapping='/campaignoffers/*') 
global with sharing class CampaignOffersWebService {
	
	global class CampaignResponse {
		global String Response = null;
		global String Message = null;
		
		global String Parent_Campaign = null;
		global String Child_Campaign = null;
		global String Yearly_Sub = null;
		global String Monthly_Sub = null;
		global String Misc_Sub = null;
		
		global void setVariables(Campaign campaign, Campaign parentCampaign, Campaign childCampaign) {
			this.Response = 'Successful';
			this.Yearly_Sub = campaign.Yearly_Product__c;
			this.Monthly_Sub = campaign.Monthly_Product__c;
			this.Misc_Sub = campaign.Misc_Product__c;
			
			this.Parent_Campaign = null;
			if (parentCampaign != null) {
			 	this.Parent_Campaign = parentCampaign.Id;
			}
			
			this.Child_Campaign = null;
			if (childCampaign != null) {
				this.Child_Campaign = childCampaign.Id;
			}
		}
		
		global void setErrorMessage(String message) {
			this.Response = 'Error';
			this.Message = message;
		}
	}
	
	/**
	 * Save the fields into multiple objects
	 */
	@HttpPost 
	global static CampaignResponse doPost(String email) {
		AccountHelper accHelper = new AccountHelper();
		accHelper.account = acchelper.loadAccountByEmail(email); 

		CampaignResponse response = new CampaignResponse();
		String status = 'Successful';

		// retrieve the account that matches the email
		if (acchelper.account == null) {
			status = 'Error';
			response.setErrorMessage('There are no accounts with the email: ' + email + '!');
		}
		else {
			Campaign currentOffer = acchelper.getCurrentOffer(); 
			if (currentOffer != null) {
				response.setVariables(currentOffer, currentOffer, currentOffer);
			} else {
				List<Contact> contacts = [SELECT Id FROM Contact WHERE AccountId = :acchelper.account.Id];
				if (!contacts.isEmpty()) {
					List<CampaignMember> members = [SELECT Id, CampaignId FROM CampaignMember WHERE ContactId = :contacts[0].Id];
					List<Id> memberIds = new List<Id>();
					for (CampaignMember member : members) {
						memberIds.add(member.CampaignId);
					}
					if (!members.isEmpty() && !memberIds.isEmpty()) {
						// find the lightest campaign from all the campaigns that this account is a member of
						List<Campaign> parentCampaigns = [SELECT Id, Yearly_Product__c, Monthly_Product__c, Misc_Product__c FROM Campaign WHERE Id IN :memberIds AND EndDate >= :Date.today() ORDER BY Weight__c ASC LIMIT 1];
						if (!parentCampaigns.isEmpty()) {
							// I shouldn't really have to find child campaigns if the account doesn't have a current offer at the moment
							Integer duration = null;
							if (acchelper.account.ER_Reporting_Sub_End_Date__c != null) {
								duration = Date.today().daysBetween(acchelper.account.ER_Reporting_Sub_End_Date__c);
							}
							List<Campaign> childCampaigns = [SELECT Id, Yearly_Product__c, Monthly_Product__c, Misc_Product__c FROM Campaign WHERE ParentId = :parentCampaigns[0].Id AND Duration__c <= :duration AND EndDate >= :Date.today() ORDER BY Weight__c ASC LIMIT 1];
							if (!childCampaigns.isEmpty() && duration != null) {
								// this campaign has child campaigns that pass the account's criteria but for some reason is marked as current offer, (probably shoud be removed)
								response.setVariables(childCampaigns[0], parentCampaigns[0], childCampaigns[0]);
							} else {
								// this campaign doesn't have any child campaigns so the products attached to this campaign will be used
								response.setVariables(parentCampaigns[0], parentCampaigns[0], null);
							}
						}
					} else {
						// find the heaviest campaign to use as the defualt campaign
						List<Campaign> defaultCampaign = [SELECT Id, Yearly_Product__c, Monthly_Product__c, Misc_Product__c FROM Campaign WHERE EndDate >= :Date.today() ORDER BY Weight__c DESC LIMIT 1];
						if (!defaultCampaign.isEmpty()) {
							response.setVariables(defaultCampaign[0], defaultCampaign[0], null);
						} else {
							status = 'Error';
							response.setErrorMessage('There are no valid campaigns in the system!');
						}
					}
				}
			}
		}
		// return error response
		return response;
	}
}