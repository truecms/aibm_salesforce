/**
 * TestSubscriptionWizard 
 *
 *
 * @author: Ross Tailby
 * @license: http://www.eurekareport.com.au
 */
@isTest
private class TestSubscriptionWizard {
	
	/* Precreate Object Instances */
	/**********************************************************/
	
	static RecordType getAccountRecordType(){
		RecordType recordType = [ select Id, Name, DeveloperName from RecordType where SObjectType = 'Account' and IsPersonType = true limit 1 ];
		return recordType;
	}
	
	static Account accountInstance() {
		RecordType recordType = [ select Id, Name, DeveloperName from RecordType where SObjectType = 'Account' and IsPersonType = true limit 1 ];
  		
  		Account account = new Account(FirstName='Test', LastName='Tester', PersonEmail='test@test.com.test');
  		account.RecordTypeId = recordType.Id;
  		account.Sub_Status__c = '4';  // paid subscription
  		insert account;
    	
        return account;
	}
	
	static Product_Instance__c productInstance() {
		Account acc = accountInstance();
		Product_Instance__c productInstance = new Product_Instance__c(Amount__c=100.0, Person_Account__c=acc.Id);
		insert productInstance;
		return productInstance;
	}
	
	/* Test Methods */
	/**********************************************************/

	/* --- New Subscription Wizard Tests --- */
	
	// Test initial load
	static testMethod void testInitialController() {
		Account account = accountInstance();
		account.Phone = '12345678';
		update account;
		
		go1__CardTypes__c cardtypes = new go1__CardTypes__c(Name='Card Types');
		cardtypes.go1__Amex__c = true;
		cardtypes.go1__Diners__c = true;
		cardtypes.go1__MasterCard__c = true;
		cardtypes.go1__Visa__c = true;
		insert cardtypes;
		
		go1__CreditCardAccount__c creditcard = go1.PaymentHelper.savedTestCreditCard(account);
		Subscription_Stream__c stream = new Subscription_Stream__c(Name='Eureka Report');
		insert stream;
		Subscription_Product__c product = new Subscription_Product__c(name='prod',Subscription_Stream__c=stream.Id, Price__c=100.00);
		insert product;
		
		Campaign campaign = new Campaign(Name='Test Campaign', IsActive=true);
		insert campaign;
		
		System.debug(account.Id);
		//Get the PersonContactId of the account
		Account personAccount = [SELECT Id, PersonContactId FROM Account WHERE Id = :account.Id LIMIT 1];
		CampaignMember member = new CampaignMember(ContactId=personAccount.PersonContactId,CampaignId=campaign.Id);
		insert member;
		
		NewSubscriberWizard newSubscribeWizard = new NewSubscriberWizard();
		newSubscribeWizard.account = account;
		newSubscribeWizard.product.Person_Account__c = account.Id;
		newSubscribeWizard.product.Subscription_Product__c = product.Id;
		newSubscribeWizard.campaign = campaign;
		newSubscribeWizard.campaignId = campaign.Id;
		
		Account acc = newSubscribeWizard.getAccount();
		System.assertEquals(acc.Phone, '12345678');
		newSubscribeWizard.getProduct();
		newSubscribeWizard.getRecurring();
		
		newSubscribeWizard.retrieveProduct();
		System.assert(!newSubscribeWizard.isFree);
		System.assertEquals(newSubscribeWizard.totalAmount, 100.00);
		
		Subscription_Product__c sub = newSubscribeWizard.getSubscription();
		System.assertEquals(sub.Id, product.Id);
		
		newSubscribeWizard.getTransaction();
		newSubscribeWizard.getCampaign();
		
		List<SelectOption> options = newSubscribeWizard.getPaymentTypes();
		System.assert(options.size()>0);
		
		List<SelectOption> birthYears = newSubscribeWizard.getBirthYears();
		System.assert(birthYears.size()>10);
		
		List<SelectOption> campaigns = newSubscribeWizard.getNewCampaigns();
		System.assert(campaigns.size()>0);
		
		PageReference campaignInfo = newSubscribeWizard.campaignInfo();
		System.assertEquals(campaignInfo.getUrl(), '/' + campaign.Id);
	}
	
	// Test initial load when account has been specified in the url
	static testMethod void testInitialControllerWithAccount() {
		Account account = accountInstance();
		go1__CreditCardAccount__c creditcard = go1.PaymentHelper.savedTestCreditCard(account);
		
		NewSubscriberWizard newSubscribeWizard = new NewSubscriberWizard();
		newSubscribeWizard.setInitialAccountVariables(String.valueOf(account.Id));
		
		//Check that the newSubscriberWizard account has the acc id 
		System.assertEquals(newSubscribeWizard.product.Person_Account__c, account.Id);
	}
	
	// Test the code that submits the form
	static testMethod void testSubmitForm() {
		//Set up some AIBM Settings
		AIBM_Settings__c aibmSettings = new AIBM_Settings__c();
		aibmSettings.Name = 'Defaults';
		aibmSettings.ER_Minimum_Sub_Price__c = 217.50;
		insert aibmSettings;
		
		NewSubscriberWizard newSubscribeWizard = new NewSubscriberWizard();
		Account acc = new Account(FirstName='first',LastName='last', RecordTypeId=getAccountRecordType().Id, PersonEmail='test@test.com.test.new');
		newSubscribeWizard.account = acc;
		newSubscribeWizard.subscription = new Subscription_Product__c(Name='test');
		newSubscribeWizard.product = new Product_Instance__c();
		newSubscribeWizard.product.Amount__c = 100;
		newSubscribeWizard.trans.go1__Amount__c = 100;
		newSubscribeWizard.trans.go1__Account_ID__c = 'TestAcc';
		newSubscribeWizard.isFree = false;
		newSubscribeWizard.accountType = 'new';
		
		go1__Payment_Gateway__c gateway = new go1__Payment_Gateway__c(Name='Credit Card', go1__Gateway__c='Go2Debit', go1__Payment_Type__c='Credit Card');
		insert gateway;
		newSubscribeWizard.payment.creditcard = new go1__CreditCardAccount__c();
		newSubscribeWizard.payment.creditcard.go1__Payment_Gateway__c = gateway.Id;
		newSubscribeWizard.payment.creditcard.go1__Token__c = '4444333322221111';
		newSubscribeWizard.payment.creditcard.go1__Expiry__c = '10/20';
		newSubscribeWizard.payment.creditcard.go1__Card_Type__c = 'VISA';
		
		Test.startTest();
		newSubscribeWizard.submitForm();
		Test.stopTest();
		
		//Check that the submission was a success and that the acc is saved etc
		System.assert(!newSubscribeWizard.failed);
		
		List<Account> accList = [SELECT Id FROM Account WHERE FirstName = 'first' AND LastName = 'last'];
		System.assertEquals(accList.size(), 1);
		List<Product_Instance__c> insts = [SELECT Id FROM Product_Instance__c WHERE Person_Account__c = :accList[0].Id];
		System.assertEquals(insts.size(), 1);
	}
	
	// Test the code that submits the form
	static testMethod void testManualPaymentSubmitForm() {
		NewSubscriberWizard newSubscribeWizard = new NewSubscriberWizard();
		newSubscribeWizard.account = new Account(FirstName='first',LastName='last');
		newSubscribeWizard.subscription = new Subscription_Product__c(Name='test');
		newSubscribeWizard.product = new Product_Instance__c();
		newSubscribeWizard.product.Amount__c = 100;
		newSubscribeWizard.trans.go1__Amount__c = 100;
		
		go1__Payment_Gateway__c gateway = new go1__Payment_Gateway__c(Name='Credit Card', go1__Gateway__c='Go2Debit', go1__Payment_Type__c='Manual');
		insert gateway;
		
		Test.startTest();
		newSubscribeWizard.submitForm();
		Test.stopTest();
		
		//Manual payments not possible - check apex error message given
		System.assert(ApexPages.getMessages().size() == 1);
	}
	
	static testMethod void testSaveCreditCard() {
		NewSubscriberWizard newSubscribeWizard = new NewSubscriberWizard();
		go1__Payment_Gateway__c gateway = new go1__Payment_Gateway__c(Name='Credit Card', go1__Gateway__c='Go2Debit', go1__Payment_Type__c='Manual');
		insert gateway;
		Account acc = accountInstance();
		newSubscribeWizard.payment.creditcard = new go1__CreditCardAccount__c();
		newSubscribeWizard.payment.creditcard.go1__Payment_Gateway__c = gateway.Id;
		newSubscribeWizard.payment.creditcard.go1__Account__c = acc.Id;
		newSubscribeWizard.payment.creditcard.go1__Token__c = '4444333322221111';
		newSubscribeWizard.payment.creditcard.go1__Expiry__c = '10/20';
		newSubscribeWizard.payment.creditcard.go1__Card_Type__c = 'VISA';
		
		//Check we don't have a name
		System.assertEquals(newSubscribeWizard.payment.creditcard.name, null); 
		
		Test.startTest();
		newSubscribeWizard.saveCreditCardDetails();
		Test.stopTest();
		
		//We should now have a name!
		System.assert(newSubscribeWizard.payment.creditcard.name.length()>0);
	}
	
	// Test 
	static testMethod void testSaveAccount() {
		Subscription_Stream__c stream = new Subscription_Stream__c(Name='Eureka Report');
		insert stream;
		
		NewSubscriberWizard newSubscribeWizard = new NewSubscriberWizard();
		newSubscribeWizard.subscription = new Subscription_Product__c();
		newSubscribeWizard.subscription.Subscription_Stream__c = stream.Id;
		
		newSubscribeWizard.accountType = 'new';
		newSubscribeWizard.account.LastName = 'Tester';
		newSubscribeWizard.account.FirstName = 'Test';
		newSubscribeWizard.account.PersonEmail = 'test@test.com.test';
		newSubscribeWizard.saveAccount();
		
		List<Account> accounts = [SELECT Id FROM Account WHERE Id = :newSubscribeWizard.account.Id];
		// check that account has been saved to the database
		System.assertEquals(1, accounts.size());
	}
	
	static testMethod void testSaveProduct() {
		NewSubscriberWizard newSubscribeWizard = new NewSubscriberWizard();
		newSubscribeWizard.accountType = 'new';
		newSubscribeWizard.account = accountInstance();
		
		// day duration
		newSubscribeWizard.subscription = new Subscription_Product__c(Duration__c=1,Duration_units__c='month');
		newSubscribeWizard.saveProduct();
		
		// week duration
		Date endDate = NewSubscriberWizard.evaluateEndDate(Date.today(), 1.0, 'week');
		System.assertEquals(Date.today().addDays(7), endDate);
		
		// month duration
		endDate = NewSubscriberWizard.evaluateEndDate(Date.today(), 1.0, 'month');
		System.assertEquals(Date.today().addMonths(1), endDate);
		
		// year duration
		endDate = NewSubscriberWizard.evaluateEndDate(Date.today(), 1.0, 'year');
		System.assertEquals(Date.today().addYears(1), endDate);
	}
	
	static testMethod void testDiscountAmount() {
		NewSubscriberWizard newSubscribeWizard = new NewSubscriberWizard();
		Subscription_Stream__c stream = new Subscription_Stream__c();
		insert stream;
		Subscription_Product__c sub = new Subscription_Product__c(Duration__c=1,Duration_units__c='month',Price__c=100,Subscription_Stream__c=stream.Id);
		insert sub;
		
		newSubscribeWizard.subscription = sub;
		newSubscribeWizard.product.Amount__c = 50;
		newSubscribeWizard.changeDiscount();
		
		// discount should equal 50
		System.assertEquals(50, newSubscribeWizard.product.Discount__c);
	}
	
	// Test
	static testMethod void testAmexCreditCard() {
		Product_Instance__c productInstance = TestSubscriptionWizard.productInstance();
		
		NewSubscriberWizard newSubscribeWizard = new NewSubscriberWizard();
		newSubscribeWizard.product = productInstance;
		newSubscribeWizard.creditcard.go1__Card_Type__c = 'AMEX';
		newSubscribeWizard.trans.go1__Amount__c = 100.00;
		newSubscribeWizard.updateCreditCardSurcharges();
		
		//Check the Total Amount
		System.assertEquals(newSubscribeWizard.totalAmount,100.0*1.022);
	}
	
	// Test
	static testMethod void testDinersCreditCard() {
		Product_Instance__c productInstance = TestSubscriptionWizard.productInstance();
		
		NewSubscriberWizard newSubscribeWizard = new NewSubscriberWizard();
		newSubscribeWizard.product = productInstance;
		newSubscribeWizard.creditcard.go1__Card_Type__c = 'DINERS';
		newSubscribeWizard.trans.go1__Amount__c = 100.00;
		newSubscribeWizard.updateCreditCardSurcharges();
		
		//Check the Total Amount
		System.assertEquals(newSubscribeWizard.totalAmount,100.0*1.011);
	}
	
	
	// Test the code that submits a manual payment
	static testMethod void testSubmitManualPayment() {
		NewSubscriberWizard newSubscribeWizard = new NewSubscriberWizard();
		Account acc = new Account(FirstName='first',LastName='last', RecordTypeId=getAccountRecordType().Id, PersonEmail='test@test.com.test.new');
		newSubscribeWizard.account = acc;
		newSubscribeWizard.accountType = 'new'; //Set the account type as a new account to save
		newSubscribeWizard.subscription = new Subscription_Product__c(Name='test');
		newSubscribeWizard.product = new Product_Instance__c();
		newSubscribeWizard.product.Amount__c = 100;
		newSubscribeWizard.trans.go1__Amount__c = 100;
		newSubscribeWizard.isFree = false;
		
		go1__Payment_Gateway__c gateway = new go1__Payment_Gateway__c(Name='Manual Payment [Test]', go1__Gateway__c='Go2Debit', go1__Payment_Type__c='Manual');
		insert gateway;
		newSubscribeWizard.payment.creditcard = new go1__CreditCardAccount__c();
		newSubscribeWizard.payment.creditcard.go1__Payment_Gateway__c = gateway.Id;
		newSubscribeWizard.payment.creditcard.go1__Token__c = '4444333322221111';
		newSubscribeWizard.payment.creditcard.go1__Expiry__c = '10/20';
		newSubscribeWizard.payment.creditcard.go1__Card_Type__c = 'VISA';
		
		Test.startTest();
		newSubscribeWizard.submitForm();
		Test.stopTest();
				
		System.assert(!newSubscribeWizard.failed);
		
		//Check we have a transaction
		List<Account> savedAcc = [SELECT Id FROM Account WHERE FirstName = 'first' AND LastName='last'];
		
		//Find the latest transaction against the Account and check its a failure and for 100.12
		List<go1__Transaction__c> manualTrans = [SELECT Id, go1__Amount__c, go1__Code__c, Subscription_Instance__c 
					FROM go1__Transaction__c 
					WHERE go1__Account__c = :savedAcc[0].Id];
		System.assertEquals(manualTrans.size(), 1); //Check there is only one transaction on this account
		System.assertEquals(manualTrans[0].go1__Amount__c, 100.00); //Check the transaction is the one we want with the correct value
	}
	
	
	// Test throwing an error whilst communicating with the Payment Gateway
	static testMethod void testExceptionOnPayment() {
		NewSubscriberWizard newSubscribeWizard = new NewSubscriberWizard();
		newSubscribeWizard.account = new Account(FirstName='first',LastName='last');
		newSubscribeWizard.subscription = new Subscription_Product__c(Name='test');
		newSubscribeWizard.product = new Product_Instance__c();
		newSubscribeWizard.product.Amount__c = 100;
		newSubscribeWizard.trans.go1__Amount__c = 100;
		newSubscribeWizard.isFree = false;
		
		//Set the isExceptionTest boolean
		newSubscribeWizard.isExceptionTest = true;
		
		go1__Payment_Gateway__c gateway = new go1__Payment_Gateway__c(Name='Credit Card', go1__Gateway__c='Go2Debit', go1__Payment_Type__c='Credit Card');
		insert gateway;
		newSubscribeWizard.payment.creditcard = new go1__CreditCardAccount__c();
		newSubscribeWizard.payment.creditcard.go1__Payment_Gateway__c = gateway.Id;
		newSubscribeWizard.payment.creditcard.go1__Token__c = '4444333322221111';
		newSubscribeWizard.payment.creditcard.go1__Expiry__c = '10/20';
		newSubscribeWizard.payment.creditcard.go1__Card_Type__c = 'VISA';
		newSubscribeWizard.submitForm();
		
		System.debug('DEBUG: Status:' + newSubscribeWizard.status);
		Boolean result = newSubscribeWizard.status.contains('Exception');
		System.assert(result);
	}
	
	// Test the code = 10 successes handling
	static testMethod void testSubmitCode10Error() {
		NewSubscriberWizard newSubscribeWizard = new NewSubscriberWizard();
		newSubscribeWizard.account = accountInstance();
		newSubscribeWizard.accountType = 'new'; //Set the account type as a new account to save
		newSubscribeWizard.subscription = new Subscription_Product__c(Name='test');
		newSubscribeWizard.product = new Product_Instance__c();
		newSubscribeWizard.product.Amount__c = 100.10;
		newSubscribeWizard.trans.go1__Amount__c = 100.10;
		newSubscribeWizard.isFree = false;
		
		go1__Payment_Gateway__c gateway = new go1__Payment_Gateway__c(Name='Credit Card', go1__Gateway__c='Go2Debit', go1__Payment_Type__c='Credit Card');
		insert gateway;
		newSubscribeWizard.payment.creditcard = new go1__CreditCardAccount__c();
		newSubscribeWizard.payment.creditcard.go1__Payment_Gateway__c = gateway.Id;
		newSubscribeWizard.payment.creditcard.go1__Token__c = '4444333322221111';
		newSubscribeWizard.payment.creditcard.go1__Expiry__c = '10/20';
		newSubscribeWizard.payment.creditcard.go1__Card_Type__c = 'VISA';
		newSubscribeWizard.submitForm();
		
		System.assertEquals(newSubscribeWizard.failed, false);
	}
	
	
	// Test for creating a failed transaction
	static testMethod void testSubmitErrorTransaction() {
		//Set up some AIBM Settings
		AIBM_Settings__c aibmSettings = new AIBM_Settings__c();
		aibmSettings.Name = 'Defaults';
		aibmSettings.ER_Minimum_Sub_Price__c = 217.50;
		insert aibmSettings;
		
		NewSubscriberWizard newSubscribeWizard = new NewSubscriberWizard();
		Account acc = new Account(FirstName='first',LastName='last', RecordTypeId=getAccountRecordType().Id, PersonEmail='test@test.com.test.new');
		newSubscribeWizard.account = acc;
		newSubscribeWizard.accountType = 'new'; //Set the account type as a new account to save
		newSubscribeWizard.subscription = new Subscription_Product__c(Name='test');
		newSubscribeWizard.product = new Product_Instance__c();
		newSubscribeWizard.product.Amount__c = 100.59; //add a cents amount to force SecurePay to return an error
		newSubscribeWizard.trans.go1__Amount__c = 100.59;
		newSubscribeWizard.isFree = false;
		
		
		go1__Payment_Gateway__c gateway = new go1__Payment_Gateway__c(Name='Credit Card [Test]', go1__Gateway__c='Securepay', go1__Payment_Type__c='CreditCard');
		gateway.go1__Is_Test__c = true;
		gateway.go1__Active__c = true;
		gateway.go1__Username__c = 'EUK0022';
		gateway.go1__Password__c = '45SywtqJ';
		insert gateway;
		newSubscribeWizard.payment.creditcard = new go1__CreditCardAccount__c();
		newSubscribeWizard.payment.creditcard.go1__Payment_Gateway__c = gateway.Id;
		newSubscribeWizard.payment.creditcard.go1__Token__c = '4444333322221111';
		newSubscribeWizard.payment.creditcard.go1__Expiry__c = '10/20';
		newSubscribeWizard.payment.creditcard.go1__Card_Type__c = 'VISA';
		
		Test.startTest();
		newSubscribeWizard.submitForm();
		Test.stopTest();
		
		List<Account> savedAcc = [SELECT Id FROM Account WHERE FirstName = 'first' AND LastName='last'];
		
		//Find the latest transaction against the Account and check its a failure and for 100.12
		List<go1__Transaction__c> failedTrans = [SELECT Id, go1__Amount__c, go1__Code__c, Subscription_Instance__c 
					FROM go1__Transaction__c 
					WHERE go1__Account__c = :savedAcc[0].Id];
		System.assertEquals(failedTrans.size(), 1); //Check there is only one transaction on this account
		System.assertEquals(failedTrans[0].go1__Amount__c, 100.59); //Check the transaction is the one we want with the correct value
		System.assert(failedTrans[0].go1__Code__c>10); //Check this is a failed transaction
		System.assertEquals(failedTrans[0].Subscription_Instance__c, null); //Check no Product Instance was created against this failed trans
		
		//Check that the error message was correctly added and the form switched to EXISTING_ACCOUNT
		System.assertEquals(newSubscribeWizard.accountType, 'existing');
		System.assertEquals(newSubscribeWizard.product.Person_Account__c, savedAcc[0].Id);
	}
	
	
	/**************************************************************************************/
	
	// Test the order and group criteria for the campaign list
	// @TODO: move to campaign helper 
	static testMethod void testCampaignList() {
		Campaign activeCampaign = new Campaign(Name='Active Campaign', Affiliate_campaign_name__c='Active Campaign', IsActive=true, StartDate=Date.today().addMonths(-1), EndDate=Date.today().addMonths(1));
		insert activeCampaign;
		
		Campaign expiredCampaign = new Campaign(Name='Expired Campaign', Affiliate_campaign_name__c='Expired Campaign', IsActive=true, StartDate=Date.today().addMonths(-1), EndDate=Date.today().addDays(-7));
		insert expiredCampaign;
		
		NewSubscriberWizard newSubscriberWizard = new NewSubscriberWizard();
		List<SelectOption> options = newSubscriberWizard.getNewCampaigns();
		
		System.assertEquals(activeCampaign.Id, options[2].getValue());
		System.assertEquals(expiredCampaign.Id, options[4].getValue());
	}
	
	/**************************************************************************************/
	
	/* --- Subscription Renewal Tests --- */
	
	// Test multiple renewal dates
	static testMethod void testNextRenewalDates() {
		Date todayDate = Date.newInstance(2012, 11, 27);
		
		// today, once, pay now - next renewal date should remain the same
		Datetime todayOncePay = AIBMHelper.evaluateNextRenewal(todayDate, 'once', true);
		System.assertEquals(Date.newInstance(2012, 11, 27), todayOncePay);
		
		// today, monthly, pay now - next renewal date should be one month in the future from todayDate
		Datetime todayMonthlyPay = AIBMHelper.evaluateNextRenewal(todayDate, 'monthly', true);
		System.assertEquals(Date.newInstance(2012, 12, 27), todayMonthlyPay);
		
		// today, yearly, pay now - next renewal date should be one year in the future from todayDate 
		Datetime todayYearlyPay = AIBMHelper.evaluateNextRenewal(todayDate, 'yearly', true);
		System.assertEquals(Date.newInstance(2013, 11, 27), todayYearlyPay);
	}
	
	// Test multiple attempts remaining
	static testMethod void testRemainingNumberAttempts() {
		// expects 12 monthly payments
		Decimal twelveMonthlyPayments = AIBMHelper.evaluateNumberAttempts(1, 'year', 'monthly', false);
		System.assertEquals(12, twelveMonthlyPayments);
		
		// expects 11 monthly payments since the first month should not be included
		Decimal twelveMonthlyPaymentsNow = AIBMHelper.evaluateNumberAttempts(1, 'year', 'monthly', true);
		System.assertEquals(11, twelveMonthlyPaymentsNow);
		
		// expects only 1 payment out of the 12 months
		Decimal oneYearlyPayment = AIBMHelper.evaluateNumberAttempts(12, 'month', 'yearly', false);
		System.assertEquals(1, oneYearlyPayment);
		
		// expects 0 payments out of the 12 months since the first payment should not be included
		Decimal oneYearlyPaymentNow = AIBMHelper.evaluateNumberAttempts(12, 'month', 'yearly', true);
		System.assertEquals(0, oneYearlyPaymentNow);
	}
	
	/* --- Subscription Stacking Tests --- */
		
	// Test
	static testMethod void testSubscriptionDateStacking() {
		Account account = accountInstance();
		
		Subscription_Stream__c stream = new Subscription_Stream__c();
		insert stream;
		
		Subscription_Product__c prod = new Subscription_Product__c();
		prod.Name = 'product'; 
		prod.Duration__c = 2;
		prod.Duration_units__c = 'month';
		prod.Subscription_Stream__c = stream.Id;
		insert(prod);
		
		Product_Instance__c sub = new Product_Instance__c();
		sub.Person_Account__c = account.Id;
		sub.Subscription_Product__c = prod.Id;
		sub.End_Date__c = Date.today();
		
		insert(sub);
		
		//Create another Product Instance of the same subscription stream and check it stacks
		Product_Instance__c sub2 = new Product_Instance__c();
		sub2.Person_Account__c = account.Id;
		sub2.Subscription_Product__c = prod.Id;
		insert sub2;
		
		//System.assertEquals(Date.today().addDays(62), sub.End_Date__c);
		
		//Get back the sub and see its date
		Product_Instance__c pi1 = [SELECT Id, Start_Date__c, End_Date__c FROM Product_Instance__c WHERE Id = :sub.Id];
		System.debug('DEBUG: pi1 End Date:' + pi1.End_Date__c);
		System.assertEquals(pi1.Start_Date__c, Date.today());
		System.assertEquals(pi1.End_Date__c, Date.today().addMonths(2));
		
		Product_Instance__c pi2 = [SELECT Id, Start_Date__c, End_Date__c FROM Product_Instance__c WHERE Id = :sub2.Id];
		System.debug('DEBUG: pi2 End Date:' + pi2.End_Date__c);
		System.assertEquals(pi2.Start_Date__c, Date.today().addMonths(2).addDays(1));
		System.assertEquals(pi2.End_Date__c, Date.today().addMonths(2).addDays(1).addMonths(2));
	}
	
	
	/**************************************************************************************/
	
	/* --- Subscription Cancellation Tests --- */
	
	// Test
	static testMethod void testCancelSubscriptionError() {
		CancelSubscription cancelSubscription = new CancelSubscription();
		cancelSubscription.cancel();
		System.assertEquals(null, cancelSubscription.subscription);
	}
	
	// Test
	static testMethod void testCancelSubscriptionSuccessfulNow() {
		Account account = accountInstance();
		account.Phone = '12345678';
		update account;
		go1__CreditCardAccount__c creditcard = go1.PaymentHelper.savedTestCreditCard(account);
		
		go1__Recurring_Instance__c recurring = new go1__Recurring_Instance__c();
		recurring.go1__Account__c = account.Id;
		recurring.go1__Amount__c = 1;
		recurring.go1__Interval__c = 'Monthly';
		recurring.go1__Next_Renewal__c = Datetime.now();
		recurring.go1__Remaining_Attempts__c = 0;
		recurring.go1__Payment_Account_Type__c = 'Credit Card';
		recurring.go1__Credit_Card_Account__c = creditcard.Id;
		insert(recurring);
		
		Subscription_Stream__c stream = new Subscription_Stream__c();
		insert stream;
		
		Subscription_Product__c prod = new Subscription_Product__c();
		prod.Subscription_Stream__c = stream.Id;
		prod.Duration__c = 1;
		prod.Duration_units__c = 'month';
		prod.Subscription_Type__c = 'm';
		prod.Price__c = 100;
		insert(prod);
		
		Product_Instance__c cancelledSub = new Product_Instance__c();
		cancelledSub.Person_Account__c = account.Id;
		cancelledSub.Subscription_Product__c = prod.Id;
		cancelledSub.Start_Date__c = date.today();
		cancelledSub.Status__c = 'Cancelled';
		insert(cancelledSub);
		
		// Setup objects
		CancelSubscription cancelSubscription = new CancelSubscription();
		cancelSubscription.subscriptionId = cancelledSub.Id;
		cancelSubscription.loadObjects();
		
		// Cancel Subscription
		System.assertEquals(true, cancelSubscription.alreadyCancelled);
		
		Product_Instance__c Sub = new Product_Instance__c();
		sub.Person_Account__c = account.Id;
		sub.Subscription_Product__c = prod.Id;
		sub.Start_Date__c = date.today();
		sub.Recurring_Instance__c = recurring.Id;
		insert(sub);
		
		cancelSubscription = new CancelSubscription();
		cancelSubscription.subscriptionId = sub.Id;
		cancelSubscription.loadObjects();
		
		System.assertEquals(false, cancelSubscription.alreadyCancelled);
		
		// Cancel Subscription now
		cancelSubscription.cancel();
		
		System.assertEquals('Cancelled', cancelSubscription.subscription.Status__c);
		System.assertEquals(false, cancelSubscription.subscription.Auto_Renewal__c);
		System.assertEquals(true, cancelSubscription.recurring_instance.go1__Cancelled__c);
	}
	
	// Test
	static testMethod void testCancelSubscriptionSuccessfulLater() {
		Account account = accountInstance();
		account.Phone = '12345678';
		update account;
		go1__CreditCardAccount__c creditcard = go1.PaymentHelper.savedTestCreditCard(account);
		
		go1__Recurring_Instance__c recurring = new go1__Recurring_Instance__c();
		recurring.go1__Account__c = account.Id;
		recurring.go1__Amount__c = 1;
		recurring.go1__Interval__c = 'Monthly';
		recurring.go1__Next_Renewal__c = Datetime.now();
		recurring.go1__Remaining_Attempts__c = 0;
		recurring.go1__Payment_Account_Type__c = 'Credit Card';
		recurring.go1__Credit_Card_Account__c = creditcard.Id;
		insert(recurring);
		
		Subscription_Stream__c stream = new Subscription_Stream__c();
		insert stream;
		
		Subscription_Product__c prod = new Subscription_Product__c();
		prod.Subscription_Stream__c = stream.Id;
		prod.Duration__c = 1;
		prod.Duration_units__c = 'month';
		prod.Subscription_Type__c = 'm';
		prod.Price__c = 100;
		insert(prod);
		
		Product_Instance__c Sub = new Product_Instance__c();
		sub.Person_Account__c = account.Id;
		sub.Subscription_Product__c = prod.Id;
		sub.Start_Date__c = date.today();
		sub.Recurring_Instance__c = recurring.Id;
		insert(sub);
		
		// Setup objects
		CancelSubscription cancelSubscription = new CancelSubscription();
		cancelSubscription.subscriptionId = sub.Id;
		cancelSubscription.cancelType = 'later';
		cancelSubscription.loadObjects();
		
		// Cancel Subscription later
		cancelSubscription.cancel();
		
		Task reminder = [SELECT Id FROM Task WHERE WhatId = :sub.Id];
		System.assertNotEquals(null, reminder);
	}

}