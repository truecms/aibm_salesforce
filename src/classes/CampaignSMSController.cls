public with sharing class CampaignSMSController 
{
	public Boolean disableMe {get; set;}	
	public Boolean displayPopup {get; set;}
	public Boolean displayFinish {get; set;}
	
	public String campaignId {get; set;}
	public String campaignName {get; set;}
	public String senderId {get; set;}
	public String contactTemplateName {get; set;}
	
	public String contactTextContent {get; set;}
	public Integer totalContacts {get; set;}
	
	public List<SelectOption> senderIdDropDown {get; set;}
	public List<SelectOption> contactTemplateDropDown {get; set;}
	public List<StatusVar> statusFieldDropDown {get; set;}
	
	public List<CampaignMember> receiverList {get; set;}
	public Integer invalidReceivers {get; set;}
	
	public List<smagicinteract__smsMagic__c> smsToSend {get; set;}
	public String resultText {get; set;}
	
	public CampaignSMSController() {
		this.totalContacts = 0;
		this.invalidReceivers = 0;
		this.disableMe = true;
		this.displayFinish = false;
		lookupCampaignName();
		populateStatusFieldDropDown();
		populateContactTemplateDropDown();
		populateSenderIdDropDown();
	}
	
	public void lookupCampaignName() {
		this.campaignId = System.currentPageReference().getParameters().get('recordIds');
		Campaign chosenCampaign = [SELECT name FROM Campaign WHERE Id = :this.campaignId LIMIT 1];
		this.campaignName = chosenCampaign.Name;
	}
	
	//Populate the possible Campaign Member statuses, retrieved using the Schema describe methods
	public void populateStatusFieldDropDown() {
		this.statusFieldDropDown = new List<StatusVar>();
		
		Schema.DescribeFieldResult statusFieldResult = CampaignMember.Status.getDescribe();
		List<Schema.Picklistentry> statusPle = statusFieldResult.getPicklistValues();
		
		for (Schema.Picklistentry ple : statusPle) {
			this.statusFieldDropDown.add(new StatusVar(ple.getValue(), false));
		}
	}
	
	//Populate the selectOptions with all the available senderIds
	public void populateSenderIdDropDown() {
		if (this.senderIdDropDown == null)
			this.senderIdDropDown = new List<SelectOption>();
		else
			this.senderIdDropDown.clear(); // Clear any existing options
		
		List<smagicinteract__SMS_SenderId__c> senderIds = [SELECT smagicinteract__senderId__c
															FROM smagicinteract__SMS_SenderId__c];
															
		for (smagicinteract__SMS_SenderId__c senderId : senderIds) {
			senderIdDropDown.add(new SelectOption(senderId.smagicinteract__senderId__c, senderId.smagicinteract__senderId__c));
		}
	}
	
	//Populate the selectoptions with available appropriate SMS templates (where ObjectName = 'Contact')
	public void populateContactTemplateDropDown() {
		if (this.contactTemplateDropDown == null)
			this.contactTemplateDropDown = new List<SelectOption>();
		else
			this.contactTemplateDropDown.clear(); //Clear any existing values
		
		List<smagicinteract__SMS_Template__c> matchedTemplateText = [SELECT smagicinteract__Text__c, smagicinteract__Name__c 
										FROM smagicinteract__SMS_Template__c 
										WHERE smagicinteract__ObjectName__c = 'Contact'];
										
		for (smagicinteract__SMS_Template__c template : matchedTemplateText) {
			this.contactTemplateDropDown.add(new SelectOption(template.smagicinteract__Text__c, template.smagicinteract__Name__c));
		}
	}
	
	//Populate the contactTextContent with the text from the chosen template
	public void changeTemplateText() {
		this.contactTextContent = this.contactTemplateName;
	} 
	
	//This is the method that will query all the Campaign members that fit the query i.e. status and USER NOT opted out of SMS
	public PageReference getTotalCount() {
		
		//Get all the chosen filter options
		List<String> filters = new List<String>();
		for (StatusVar sv : statusFieldDropDown) {
			if (sv.selected == true)
				filters.add(sv.status);
		}
		
		//Query the Campaign Members for this campaign matching the filters selected through the checkboxes
		this.receiverList = [SELECT Id, Consolidated_Phone__c, Contact.Id, Campaign.Eureka_ID__c
							FROM CampaignMember
							WHERE Campaign.Id = :this.campaignId
							AND Contact.smagicinteract__SMSOptOut__c = false
							AND Status in :filters];
							
		//Remove the receivers without valid phone numbers - report on them too!
		Pattern phonePattern = Pattern.compile('^(\\()?(04|61)(\\))?\\s?4?\\s?\\d{2,4}(\\s|\\-)?\\d{2,4}\\s?\\d{0,4}$');
		this.invalidReceivers = 0;
		
		for (Integer i = 0; i < receiverList.size();) {
			CampaignMember receiver = receiverList[i];
			
			String consolPhoneTrim = receiver.Consolidated_Phone__c.replaceAll('/\\s+/g', '');
			consolPhoneTrim = consolPhoneTrim.replaceAll('/\\(+/g', '').replaceAll('/\\)+/g', '');

			Matcher phoneMatch = phonePattern.matcher(consolPhoneTrim); 
			if (!phoneMatch.matches() || consolPhoneTrim.length() < 10) {
				//Remove this CampaignMember from the receiver list and add to invalid count
				receiverList.remove(i);
				this.invalidReceivers++;
			}
			else {
				i++; //Only go to the next element if valid, otherwise this original element is removed and we want to consider the one put in its place
			}
		}
		
		this.totalContacts = this.receiverList.size();
		
		//If we have a positive value in the receiver list, enable the 'Run Campaign' button
		if (this.totalContacts > 0)
			this.disableMe = false;
		else
			this.disableMe = true;
		
		return null;
	}
	
	//Called on the 'Run Campaign' button - parses the template with lookup fields and loads them into a collection of SMS objects
	public void runCampaign() {
		List<Id> receiverIds = new List<Id>();
		for (CampaignMember cm : this.receiverList)
			receiverIds.add(cm.Contact.Id);
		
		//Render the Template text
		this.smsToSend = this.renderTemplateText(this.contactTextContent, receiverIds);
		
		//Before sending these, just pop-up a confirmation window with the first text to send
		if (smsToSend.size() > 0) {
			showPopup();
		}
	}
	
	
	private String parseRepsonse(String toParse) {
		DOM.Document response = new DOM.Document();
		try {
			response.load(toParse);
			DOM.XMLNode root = response.getRootElement();
			String text = '';
			
			if (root.getNodeType() == DOM.XMLNodeType.ELEMENT) {
				for (DOM.XMLNode child : root.getChildElements()) {
					if (child.getNodeType() == DOM.XMLNodeType.ELEMENT) {
						String name = child.getName();
						if (name == 'responseText') {
							text = child.getText();
						}
					}
				}
			}
			return text;
		}
		catch (System.Xmlexception e) {
			return e.getMessage(); //XML Parsing error
		}
	}
	
	
	//The Pop-Up confirmation has been accepted, now actually send the SMS messages!
	public void sendSMS() {
		String response = '';
		if (Test.isRunningTest()) //We don't want to send real SMS messages in Unit Tests so replace with a typical hardcoded success message
			response = '<?xml version=\"1.0\"?><response><vstatus>success</vstatus><responseText>,success | 61434838377  | 0200000028be2ae0 | 1</responseText></response>';
		else
			response = smagicinteract.ApexAPI.pushSMSCallout(this.smsToSend);
		System.debug('DEBUG: send response:' + response);
		
		//Check if successful - if not, do not save
		this.resultText = parseRepsonse(response);
		this.resultText = this.resultText.replaceAll(',', ''); //Remove unnecessary commas
		Boolean success = false;
		
		String[] splitResponse = resultText.split('\\|');
		if (splitResponse.size()>2) {
			if (splitResponse[0].trim() == 'Success')
				success = true;
					
			this.resultText = splitResponse[0].trim() 
								+ ' (' + splitResponse[1].trim() 
								+ ') : ' + splitResponse[2].trim();
		}
		else {
			success = true; //We cannot split the string so its best to save rather than lose the SMS messages
		}
		System.debug('DEBUG: final response text:' + resultText);
		//---------------------
		
		if (success == true) {
			insert this.smsToSend; //Save all of the outgoing SMS messages to SF	
			this.resultText = this.smsToSend.size() + ' SMS Messages sent successfully!';
		}
		
		//Now show the finish popup - will run the cancelSMS function on click to navigate the user back to the campaign page
		this.displayFinish = true;
	}
	
	public PageReference cancelSMS() {
		String retURL = System.currentPageReference().getParameters().get('retURL');
		PageReference ref = new PageReference(retUrl);
		ref.setRedirect(true);
		return ref;
	}
	
	
	private List<smagicinteract__smsMagic__c> renderTemplateText(String inputText, List<Id> receiverIds) {
		//Initialise variables to use in the 3 queries required to populate any potential template supplied by the User
		String extraFieldText = '';
		List<String> args;
		List<String> fields;
		String query = '';
		string userId = UserInfo.getUserId();
		string orgId = UserInfo.getOrganizationId();

		//Initialise the SMS Template Engine with the SMS Template text input
		smagicinteract.TemplateEngine TEngine = new smagicinteract.TemplateEngine(inputText);
		
		// render templatetext for loggedin user
		fields = TEngine.getFieldsFromSMSTextOfObjectType('$User');
		for (String x : fields) {
			if (x.equalsIgnoreCase('Name'))
				continue;
			
			if (!extraFieldText.contains(x))
				extraFieldText = extraFieldText + ', '+x; //Create a comma-separated list of the User fields used in the template
		}
		
		extraFieldText = String.escapeSingleQuotes(extraFieldText);

		args = new List<String>();
		args.add(extraFieldText);
		query = 'SELECT Id, Name, MobilePhone {0} FROM User WHERE id =:userId';
		query = String.format(query, args);
		
		User u = Database.query(query);
		TEngine.getFieldMap(u);
		TEngine.smsText = TEngine.getReplacedTextForObject(u, 1);


		// render templatetext for Organization
		fields = TEngine.getFieldsFromSMSTextOfObjectType('$Organization');
		extraFieldText = ''; //Reset extra fields to include
		for (String x : fields) {
			if (x.equalsIgnoreCase('Name'))
				continue;
			if (!extraFieldText.contains(x))
				extraFieldText = extraFieldText + ', '+x; //Create a comma-separated list of the Organisation fields used in the template
		}
		
		extraFieldText = String.escapeSingleQuotes(extraFieldText);
		args = new List<String>();
		args.add(extraFieldText);
		query = 'SELECT Id, Name {0} FROM Organization WHERE id = : orgId';
		query = String.format(query, args);
		
		Organization org = Database.query(query);
		TEngine.getFieldMap(org);
		TEngine.smsText = TEngine.getReplacedTextForObject(org, 1);
		
		
		// get field values for Campaign object fields in the template
		String objectType = 'Campaign';
		fields = TEngine.getFieldsFromSMSTextOfObjectType(objectType);
		
		extraFieldText = ''; //Reset extra fields to include
		for (String x : fields) {
			if (x.equalsIgnoreCase('Name'))
				continue;
			if (!extraFieldText.contains(x))
				extraFieldText = extraFieldText + ', '+x; //Create a comma-separated list of the Organisation fields used in the template
		}
		
		extraFieldText = String.escapeSingleQuotes(extraFieldText);
		args = new List<String>();
		args.add(extraFieldText);
		query = 'SELECT Id, Name {0} FROM Campaign WHERE id = : campaignId';
		query = String.format(query, args);
		
		Campaign campaign = Database.query(query);
		TEngine.getFieldMap(campaign);
		TEngine.smsText = TEngine.getReplacedTextForObject(campaign, 1);
		
		
		// get field values for Contact object fields in the template
		extraFieldText = ', Name, Consolidated_Phone__c, AccountId'; //Reset extra fields to include
		objectType = 'Contact';
		fields = TEngine.getFieldsFromSMSTextOfObjectType(objectType);
		
		for(string x: fields){
			if(x.equalsIgnoreCase('Name'))
				continue;
			if(!extraFieldText.contains(x))
				extraFieldText = extraFieldText + ', '+x;
		}
		
		//Also add any potentialy Account fields too --- Doesn't work with Account fields
		fields = TEngine.getFieldsFromSMSTextOfObjectType('Account');
		
		for(string x: fields){
			if(!extraFieldText.contains(x))
				extraFieldText = extraFieldText + ', Account.'+x;
		}
		//---------

		extraFieldText = String.escapeSingleQuotes(extraFieldText);
		args = new List<String>();
		args.add(extraFieldText);
		args.add(objectType);
		
		query ='SELECT id {0} FROM {1} WHERE id IN :receiverIds';
		query = String.format(query, args);
		
		List<sObject> sObjects = Database.query(query);
		TEngine.getFieldMap(sObjects[0]);
		
		//Create the SMS Text object array for returning and sending
		List<smagicinteract__smsMagic__c> smsToSend = new List<smagicinteract__smsMagic__c>();
		
		//There will be one of these sObject iterations per Customer receiving the text - use Bulk Send API
		for (sObject c :sObjects){
			String name = String.valueOf(c.get('Name'));
			String mobilePhone = String.valueOf(c.get('Consolidated_Phone__c'));
			String smsText = TEngine.getReplacedTextForObject(c, 0);
			
			smagicinteract__smsMagic__c sms = new smagicinteract__smsMagic__c();
			sms.smagicinteract__PhoneNumber__c = mobilePhone;
			sms.smagicinteract__SenderId__c = senderId;
			sms.smagicinteract__ObjectType__c = objectType;
			sms.smagicinteract__disableSMSOnTrigger__c = 1;
			sms.smagicinteract__external_field__c = smagicinteract.ApexAPI.generateUniqueKey();
			sms.smagicinteract__Name__c = name;
			sms.smagicinteract__SMSText__c = smsText;
			
			//We also need to set the Account and Campaign lookup fields so this SMS appears in the related lists
			sms.Account__c = Id.valueOf(String.valueOf(c.get('AccountId')));
			sms.smagicinteract__Campaign__c = this.campaignId;
			
			smsToSend.add(sms);
		}
		
		return smsToSend;
	}
	
	public void closePopup() {
		this.displayPopup = false;
	}
	
	public void showPopup() {
		this.displayPopup = true;
	}
	
	public class StatusVar
	{
		public String status {get; set;}
		public Boolean selected {get; set;}
		
		public StatusVar(String status, Boolean selected) {
			this.status = status;
			this.selected = selected;
		}
	}
	
}