public class RepeatedFreeTrialReport 
{
	public List<AccountFTWrapper> ftAccounts {get; set; }
	
	public Account filterAccount {get; set; }
	
	public boolean updateAccounts { get; set; }
	public Task newTask {get; set;}
	
	public Integer totalSize {get; set; }
	
	public final String PAID_SUB = 'Paid';
	public final String FREE_SUB = 'Free';
	public final String ALL_SUBS = '-- All --';
	
	public List<SelectOption> fromSubType {
		get{
			fromSubType = new List<SelectOption>();
			fromSubType.add(new SelectOption(ALL_SUBS, ALL_SUBS));
			fromSubType.add(new SelectOption(PAID_SUB, PAID_SUB));
			fromSubType.add(new SelectOption(FREE_SUB, FREE_SUB));
			return fromSubType;
		}
		
		set;
	}
	
	public List<SelectOption> activeFT {
		get{
			activeFT = new List<SelectOption>();
			activeFT.add(new SelectOption('1', 'Yes'));
			activeFT.add(new SelectOption('0', 'No'));
			return activeFT;
		}
		
		set;
	}
	
	public RepeatedFreeTrialReport()
	{
		totalSize = 0;
		initReportData();
		filterAccount = new Account(PersonDepartment=ALL_SUBS, OwnerId=userinfo.getUserId(), SicDesc='1');
		updateAccounts = false;
		newTask = new Task();
	}
	
	// Generate the SOQL Query needed to initialise the Accounts to be shown for this report
	public String generateActiveFTSOQLQuery(Date startDate, Date endDate)
	{
		String accQuery = 'SELECT firstname, lastname, PersonEmail, Owner.Name, '
								+ 'ER_Current_Active_Subscription__r.Start_Date__c, '
								+ 'ER_Current_Active_Subscription__r.End_Date__c, '
								+ 'Eureka_Status__c, '
								+ 'Eureka_Free_Trial_Status__c, '
								+ 'ER_Current_Active_Subscription__c, '
								+ 'ER_Entry_Free_Trial__c, '
								+ 'ER_Entry_Free_Trial__r.Start_Date__c, '
								+ 'ER_Entry_Free_Trial__r.End_Date__c, '
								+ '(SELECT Subject, ActivityDate, Status From Tasks WHERE Status = \'Not Started\' ORDER BY CreatedDate DESC) '
								+ 'FROM Account '
								+ 'WHERE ER_Current_Active_Subscription__r.Subscription_Type__c = \'f\' '
								+ 'AND ER_Reporting_Sub_Status__c <> \'4\' '
								+ 'AND ER_Reporting_Sub_Status__c <> \'8\' ';
		
		if (startDate!=null) {
			String formattedStartDate = String.valueOf(startDate);
			accQuery += ' AND ER_Current_Active_Subscription__r.Start_Date__c = ' + formattedStartDate.substringBefore(' ');
		}
		else {
			accQuery += ' AND ER_Current_Active_Subscription__r.Start_Date__c > 2013-12-01';
		}
			
		if (endDate!=null) {
			String formattedEndDate = String.valueOf(endDate);
			accQuery += ' AND ER_Current_Active_Subscription__r.End_Date__c = ' + formattedEndDate.substringBefore(' ');
		}
		else
		{
			String formattedEndDate = String.valueOf(Date.today());
			accQuery += ' AND ER_Current_Active_Subscription__r.End_Date__c > ' + formattedEndDate.substringBefore(' ');
		}
			
		System.debug('DEBUG: Active query:' + accQuery);
		return accQuery;
	}
	
	// Generate the SOQL Query needed to initialise the Accounts to be shown for this report
	public String generateExpiredFTSOQLQuery(Date startDate, Date endDate)
	{
		String accQuery = 'SELECT firstname, lastname, PersonEmail, Owner.Name, '
								+ 'ER_Current_Active_Subscription__r.Start_Date__c, '
								+ 'ER_Current_Active_Subscription__r.End_Date__c, '
								+ 'Eureka_Status__c, '
								+ 'Eureka_Free_Trial_Status__c, '
								+ 'ER_Current_Active_Subscription__c, '
								+ 'ER_Entry_Free_Trial__c, '
								+ 'ER_Entry_Free_Trial__r.Start_Date__c, '
								+ 'ER_Entry_Free_Trial__r.End_Date__c, '
								+ '(SELECT Subject, ActivityDate, Status From Tasks WHERE Status = \'Not Started\' ORDER BY CreatedDate DESC) '
								+ 'FROM Account '
								+ 'WHERE ER_Current_Active_Subscription__c = \'\' '
								+ 'AND Eureka_Free_Trial_Status__c > 1 '
								+ 'AND ER_Reporting_Sub_Type__c = \'f\''; //NB: The last sub the account had must be an FT
		
		if (startDate!=null) {
			String formattedStartDate = String.valueOf(startDate);
			accQuery += ' AND ER_Entry_Free_Trial__r.Start_Date__c = ' + formattedStartDate.substringBefore(' ');
		}
			
		if (endDate!=null) {
			String formattedEndDate = String.valueOf(endDate);
			accQuery += ' ER_Entry_Free_Trial__r.End_Date__c = ' + formattedEndDate.substringBefore(' ');
		}
		else {
			accQuery += ' AND ER_Entry_Free_Trial__r.End_Date__c > 2013-11-01';
		}
			
		System.debug('DEBUG: Expired query:' + accQuery);
		return accQuery;
	}
	
	
	// Initialise the Accounts to show in the report
	public void initReportData()
	{
		this.initReportData(null, null, ALL_SUBS, '1');
	}
	
	// Initialise the Accounts to show in the report
	public void initReportData(Date startDate, Date endDate, String prevSub, String active)
	{
		String accQuery = '';
		if(active=='0') //Show the inactive trials (with no current subscription) instead of the active ones!
		{
			accQuery = generateExpiredFTSOQLQuery(startDate, endDate);
		}
		else //Only show Accounts currently in their nth active FT
		{
			accQuery = generateActiveFTSOQLQuery(startDate, endDate);
		}
		
		//Run the query and find the accounts with current active FTs
		List<Account> accounts = Database.query(accQuery);
		System.debug('DEBUG: Query Results! : ' + accounts);
					
		List<Id> currentSubs = new List<Id>();
		Map<Id, Account> correctAccs = new Map<Id, Account>();
					
		for (Account a : accounts)
		{
			//Only carry on if the total number of Subs belonging to the Account is more than 1
			//TODO: IF WE CAN/COULD DO THIS IN THE ABOVE SOQL QUERY, MATTERS WOULD BE SIMPLIFIED...
			if (a.Eureka_Free_Trial_Status__c + a.Eureka_Status__c > 1)
			{
				currentSubs.add((active=='0') ? a.ER_Entry_Free_Trial__c : a.ER_Current_Active_Subscription__c);
				correctAccs.put(a.Id, a);
			}
		}
		
		
		List<Product_Instance__c> instances = [SELECT End_Date__c, Subscription_Type__c, Person_Account__c
											FROM Product_Instance__c
											WHERE Person_Account__c IN :correctAccs.keySet()
											AND Id NOT IN :currentSubs
											AND Subscription_Stream__c = 'Eureka Report'
											ORDER BY Person_Account__c, End_Date__c DESC
											]; 
											
		ftAccounts = new List<AccountFTWrapper>();
		
		Id pAcc = null;
			
		for (Product_Instance__c pi : instances)
		{
			if (pi.Person_Account__c != pAcc)
			{
				Account a = correctAccs.get(pi.Person_Account__c);
				
				//Ensure that the sub is a previous Sub, not future!	
				if (((a.ER_Current_Active_Subscription__r.End_Date__c > pi.End_Date__c) && active=='1')
					|| ((a.ER_Entry_Free_Trial__r.End_Date__c > pi.End_Date__c) && active=='0'))
				{
					//String subType = (pi.Subscription_Type__c == 'y' ) ? 'Annual' : ((pi.Subscription_Type__c == 'm') ? 'Monthly' : 'Free'); //If we need to be more granular in our description of the paid sub type
					String subType = (pi.Subscription_Type__c == 'f' ) ? FREE_SUB : PAID_SUB;
					
					pAcc = pi.Person_Account__c;
						
					if (prevSub==ALL_SUBS || (prevSub!=ALL_SUBS && subType==prevSub)) //Only add instances that fit our filtered Previous Subscription type
					{
						Integer noOfTasks = (a.tasks == null) ? 0 : a.tasks.size();
						if (noOfTasks>0) {
							Task latestTask = a.tasks[0]; //The first Task in the collection should be the latest due to the ORDER BY clause
							ftAccounts.add(new AccountFTWrapper(a, subType, noOfTasks, latestTask));
						}
						else {
							ftAccounts.add(new AccountFTWrapper(a, subType, noOfTasks));
						}
					}
				}
			}
		}
		
		totalSize = ftAccounts.size(); 
			
	}
	
	public void filterAccounts()
	{
		initReportData(filterAccount.FT_Start_Date__c, 
						filterAccount.FT_End_Date__c, 
						filterAccount.PersonDepartment, 
						filterAccount.SicDesc);
	}
	
	//Submit the chosen Account records to the Mass Assign Ownership (+ task create) class function
	public void assignOwner()
	{
		updateAccounts = true;
	}
	
	public void updateAccounts()
	{
		//Iterate round the selected Accounts, set their Owner ID and persist
		List<Account> accs = new List<Account>();
		for (AccountFTWrapper accWrap : ftAccounts) {
			if (accWrap.isSelected) {
				accWrap.acc.OwnerId = filterAccount.OwnerId;
				accs.add(accWrap.acc);
			}
		}
		update accs;
		
		//For each selected Account, create the relevant task IF the user has enetered one
		if (newTask.CallDisposition!=null && newTask.CallDisposition!='')
			createTasks(accs);
		
		//Create a success message to let the user know its done and the tasks are created
		ApexPages.Message msgSuccess = new ApexPages.Message(ApexPages.Severity.INFO, accs.size() + ' Accounts allocated successfully!');
        ApexPages.addmessage(msgSuccess);
		
		//Return to the standard report layout and refresh the results page
		updateAccounts = false;
		initReportData(filterAccount.FT_Start_Date__c, 
						filterAccount.FT_End_Date__c, 
						filterAccount.PersonDepartment,
						filterAccount.SicDesc);
	}
	
	private void createTasks(List<Account> selectedAccounts)
	{
    	//Check this - we want to create a task for each Account assigned to say "Call this Account". Can this be done through workflow rules?
    	List<Task> tasks = new List<Task>();
    	for (Account acc : selectedAccounts)
    	{
    		Task t = new Task(Subject=newTask.CallDisposition, ActivityDate=Date.Today(), WhatId=acc.Id);
    		t.WhoId = acc.PersonContactId;
    		t.OwnerId = acc.OwnerId;
    		tasks.add(t);
    	}
    	insert tasks;
    }
	
	public void cancel()
	{
		updateAccounts = false;
	}
	
	//-----------------------------------------------------------------
	// Unit Tests
	
	private static RecordType getRecordType()
	{
		// Find any person account record type
	    RecordType recordType = [ select Id, Name, DeveloperName from RecordType where SObjectType = 'Account' and IsPersonType = true limit 1 ];
	    return recordType;
	}
	
	private static Subscription_Product__c getSubProduct()
	{
		Subscription_Stream__c stream = new Subscription_Stream__c(Name='Eureka Report');
	  	insert stream;
	  	Subscription_Product__c product = new Subscription_Product__c();
	  	product.Subscription_Stream__c = stream.Id;
	  	product.Duration__c = 21;
	  	product.Duration_units__c = 'day';
	  	//product.Price__c = 0;
	  	//product.Subscription_Type__c = 'f';
	  	insert product;
	  	return product;
	}
	
	private static List<Account> createTestAccounts()
    {
    	RecordType personType = getRecordType();
    	List<Account> accs = new List<Account>();
    	Account acc1 = new Account(FirstName='MultipleFT', LastName='Test245', PersonEmail='expiredsub.test245@test.com.test', RecordTypeId=personType.Id);
    	Account acc2 = new Account(FirstName='MultipleFT', LastName='Test302', PersonEmail='activeuser.test302@test.com.test', RecordTypeId=personType.Id);
    	Account acc3 = new Account(FirstName='MultipleFT', LastName='Test957', PersonEmail='inactive.test957@test.com.test', RecordTypeId=personType.Id);
    	accs.add(acc1);
    	accs.add(acc2);
    	accs.add(acc3);
    	return accs;
    }
    
    private static Product_Instance__c createProductInstance(Account a, Subscription_Product__c subProduct, String subType, Date startDate, Date endDate, Decimal amount)
    {
    	Product_Instance__c instance = new Product_Instance__c();
	  	instance.Person_Account__c = a.Id;
	  	instance.Subscription_Product__c = subProduct.Id;
	  	instance.Subscription_Type__c = subType;
	  	instance.Amount__c = amount;
	  	instance.Start_Date__c = startDate;
	  	instance.End_Date__c = endDate;
	  	return instance;
    }
	
	public TestMethod static void testActiveFreeTrials(){
		//Create Test Data
		List<Account> accs = createTestAccounts();
		insert accs;
		
		//Get the Subscription Product for Free Trials
		Subscription_Product__c subProduct = getSubProduct();
		
		//Create an expired Free Trial for each Account
		List<Product_Instance__c> instances = new List<Product_Instance__c>();
		for (Account a : accs)
		{
			Product_Instance__c instance = createProductInstance(a, subProduct, 'f', Date.today().addMonths(-4), Date.today().addMonths(-3), 0.0);
	  		instances.add(instance);
		}	
		
		//Create an active Free Trial for each Account
		for (Account a : accs)
		{
			Product_Instance__c instance = createProductInstance(a, subProduct, 'f', null, null, 0.0);
	  		instances.add(instance);
		}
		
		//Then for the first account, add one more!		
	  	Product_Instance__c instance = createProductInstance(accs[0], subProduct, 'f', Date.today().addMonths(-2), Date.today().addMonths(-1), 0.0);
	  	instances.add(instance);
		
		insert instances;
		
		Test.startTest();
		RepeatedFreeTrialReport report = new RepeatedFreeTrialReport();
		Test.stopTest();
		
		System.assertEquals(accs.size(), report.ftAccounts.size());
		
		for (integer i = 0; i < report.ftAccounts.size(); i++){
			AccountFTWrapper ft = report.ftAccounts[i];
			System.assertEquals(ft.fromSubType, 'Free');
			System.assertEquals(ft.acc.Eureka_Status__c, 0); //None of the Accounts should report any paid subs
			if (i == 0) {
				System.assertEquals(ft.acc.Eureka_Free_Trial_Status__c, 3);
			}
			else {
				System.assertEquals(ft.acc.Eureka_Free_Trial_Status__c, 2);
			}
		}
		
	}
	
	public TestMethod static void testExpiredFreeTrials(){
		//Create Test Data
		List<Account> accs = createTestAccounts();
		insert accs;
		
		//Get the Subscription Product for Free Trials
		Subscription_Product__c subProduct = getSubProduct();
		
		//Create an expired Free Trial for each Account
		List<Product_Instance__c> instances = new List<Product_Instance__c>();
		for (Account a : accs)
		{
			Product_Instance__c instance = createProductInstance(a, subProduct, 'f', Date.today().addMonths(-9), Date.today().addMonths(-7), 0.0);
	  		instances.add(instance);
		}	
		
		insert instances;
		
		//Initially try the report and ensure that no Accounts appear as none have more than 1 FT
		RepeatedFreeTrialReport report = new RepeatedFreeTrialReport();
		System.assertEquals(0, report.ftAccounts.size());
		
		//Create an additional expired Free Trial for each Account
		for (Account a : accs)
		{
			Product_Instance__c instance = createProductInstance(a, subProduct, 'f', Date.today().addMonths(-2), Date.today().addMonths(-1), 0.0);
	  		instances.add(instance);
		}
		
		//Then for the first account, add one active sub and exsure that it is not considered!		
	  	Product_Instance__c instance = createProductInstance(accs[0], subProduct, 'f', null, null, 0.0);
	  	instances.add(instance);
		
		upsert instances;
		
		Test.startTest();
		RepeatedFreeTrialReport report2 = new RepeatedFreeTrialReport();
		report2.filterAccount.SicDesc = '0'; //Set the filter
		report2.filterAccount.PersonDepartment = report2.ALL_SUBS;
		report2.filterAccounts(); //Filter the results
		Test.stopTest();
		
		System.assertEquals(accs.size()-1, report2.ftAccounts.size());
		
		for (integer i = 0; i < report2.ftAccounts.size(); i++){
			AccountFTWrapper ft = report2.ftAccounts[i];
			System.assertEquals(ft.fromSubType, 'Free');
			System.assertEquals(ft.acc.Eureka_Status__c, 0); //None of the Accounts should report any paid subs
			System.assertEquals(ft.acc.Eureka_Free_Trial_Status__c, 2);
		}
	}
	
	public TestMethod static void testPreviousFreePaidFilter(){
		//Create Test Data
		List<Account> accs = createTestAccounts();
		insert accs;
		
		//Get the Subscription Product for Free Trials
		Subscription_Product__c subProduct = getSubProduct();
		
		//Create an expired Free Trial for each Account
		List<Product_Instance__c> instances = new List<Product_Instance__c>();
		for (Account a : accs)
		{
			Product_Instance__c instance = createProductInstance(a, subProduct, 'f', Date.today().addMonths(-9), Date.today().addMonths(-7), 0.0);
	  		instances.add(instance);
		}	
		
		insert instances;
		
		//Initially try the report and ensure that no Accounts appear as none have more than 1 FT
		RepeatedFreeTrialReport report = new RepeatedFreeTrialReport();
		System.assertEquals(0, report.ftAccounts.size());
		
		//Give two Accounts expired paid subs and one another expired FT
		Product_Instance__c instance0 = createProductInstance(accs[0], subProduct, 'y', Date.today().addMonths(-2), Date.today().addMonths(-1), 100.0);
		Product_Instance__c instance1 = createProductInstance(accs[1], subProduct, 'y', Date.today().addMonths(-2), Date.today().addMonths(-1), 100.0);
		Product_Instance__c instance2 = createProductInstance(accs[2], subProduct, 'f', Date.today().addMonths(-2), Date.today().addMonths(-1), 0.0);				
		instances.add(instance0);
		instances.add(instance1);
		instances.add(instance2);
		
		//Also give them all an active FT so that they will actually be picked up by the report
		for (Account a : accs)
		{
			Product_Instance__c instance = createProductInstance(a, subProduct, 'f', null, null, 0.0);
	  		instances.add(instance);
		}
		
		upsert instances;
		
		Test.startTest();
		RepeatedFreeTrialReport report2 = new RepeatedFreeTrialReport();
		report2.filterAccount.SicDesc = '1'; //Set the filter
		report2.filterAccount.PersonDepartment = report2.FREE_SUB;
		report2.filterAccounts(); //Filter the results
		
		List<AccountFTWrapper> freeSubs = report2.ftAccounts;
		
		report2.filterAccount.PersonDepartment = report2.PAID_SUB; //Now filter on paid subs only
		report2.filterAccounts();
		
		List<AccountFTWrapper> paidSubs = report2.ftAccounts;
		
		Test.stopTest();
		
		System.assertEquals(freeSubs.size(), 1);
		System.assertEquals(paidSubs.size(), 2);
		
		System.assertEquals(freeSubs[0].fromSubType, 'Free');
		System.assertEquals(freeSubs[0].acc.Eureka_Status__c, 0);
		System.assertEquals(freeSubs[0].acc.Eureka_Free_Trial_Status__c, 3);
		
		for (AccountFTWrapper ft : paidSubs){
			System.assertEquals(ft.fromSubType, 'Paid');
			System.assertEquals(ft.acc.Eureka_Status__c, 1); //None of the Accounts should report any paid subs
			System.assertEquals(ft.acc.Eureka_Free_Trial_Status__c, 2);
		}
	}
	
	public TestMethod static void testStartEndDateFilter(){
		//Create Test Data
		List<Account> accs = createTestAccounts();
		insert accs;
		
		//Get the Subscription Product for Free Trials
		Subscription_Product__c subProduct = getSubProduct();
		
		//Create an expired Free Trial for each Account
		List<Product_Instance__c> instances = new List<Product_Instance__c>();
		for (Account a : accs)
		{
			Product_Instance__c instance = createProductInstance(a, subProduct, 'f', Date.today().addMonths(-9), Date.today().addMonths(-7), 0.0);
	  		instances.add(instance);
		}	
		
		insert instances;
		
		//Initially try the report and ensure that no Accounts appear as none have more than 1 FT
		RepeatedFreeTrialReport report = new RepeatedFreeTrialReport();
		System.assertEquals(0, report.ftAccounts.size());
		
		//Give two Accounts FTs with the same start and end dates, and one with different
		Product_Instance__c instance0 = createProductInstance(accs[0], subProduct, 'f', Date.today().addMonths(-1), Date.today().addMonths(1), 0.0);
		Product_Instance__c instance1 = createProductInstance(accs[1], subProduct, 'f', Date.today().addMonths(-1), Date.today().addMonths(1), 0.0);
		Product_Instance__c instance2 = createProductInstance(accs[2], subProduct, 'f', Date.today().addMonths(-3), Date.today().addMonths(3), 0.0);				
		instances.add(instance0);
		instances.add(instance1);
		instances.add(instance2);
		
		upsert instances;
		
		Test.startTest();
		RepeatedFreeTrialReport report2 = new RepeatedFreeTrialReport();
		report2.filterAccount.SicDesc = '1'; //Set the filter
		report2.filterAccount.PersonDepartment = report2.ALL_SUBS;
		report2.filterAccount.FT_Start_Date__c = Date.today().addMonths(-1);
		report2.filterAccount.FT_End_Date__c = Date.today().addMonths(1);
		report2.filterAccounts(); //Filter the results
		
		List<AccountFTWrapper> freeSubs = report2.ftAccounts;
		
		Test.stopTest();
		
		System.assertEquals(freeSubs.size(), 2);
		
		for (AccountFTWrapper ft : freeSubs){
			System.assertEquals(ft.fromSubType, 'Free');
			System.assertEquals(ft.acc.Eureka_Status__c, 0);
			System.assertEquals(ft.acc.Eureka_Free_Trial_Status__c, 2);
		}		
	}
	
	public TestMethod static void testAllocateSelectedAccounts(){
		//Create Test Data
		List<Account> accs = createTestAccounts();
		insert accs;
		
		//Get the Subscription Product for Free Trials
		Subscription_Product__c subProduct = getSubProduct();
		
		//Create an expired Free Trial for each Account
		List<Product_Instance__c> instances = new List<Product_Instance__c>();
		for (Account a : accs)
		{
			Product_Instance__c instance = createProductInstance(a, subProduct, 'f', Date.today().addMonths(-9), Date.today().addMonths(-7), 0.0);
			Product_Instance__c instance0 = createProductInstance(a, subProduct, 'f', null, null, 0.0);
	  		instances.add(instance);
	  		instances.add(instance0);
		}	
		
		insert instances;
		
		//Just grab any user who we can use to become the Account Owner
    	User testuser = [SELECT Id FROM User WHERE IsActive = TRUE AND UserType = 'Standard' AND UserRoleId != '' AND alias NOT IN ('sadm', 'it') LIMIT 1];
    	
    	//Check that the Accounts currently DON'T belong to this user
    	for (Account a : accs) {
    		System.assertNotEquals(a.OwnerId, testUser.Id);
    	}
		
		Test.startTest();
		RepeatedFreeTrialReport report = new RepeatedFreeTrialReport();
		
		//Select all 3 accounts
		for (AccountFTWrapper ft : report.ftAccounts) {
			ft.isSelected = true;
		}
		
		//Set the OwnerId
		report.filterAccount.OwnerId = testUser.Id;
		report.updateAccounts();
				
		Test.stopTest();
		
		System.assertEquals(report.ftAccounts.size(), 3);
		
		//Check the Accounts now DO belong to our User!
		for (AccountFTWrapper ft : report.ftAccounts){
			System.assertEquals(ft.acc.OwnerId, testUser.Id);
		}
	}
	
}