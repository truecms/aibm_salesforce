/**
 * JobSchedulerWebService
 *
 * Can be used to remove and reschedule the scheduled Apex jopbs (e.g. auto renewals) from remote tools and software
 * This is particularly useful for calling through ANT from the Jenkins automated build tool.
 *
 * Created By: Ross Tailby
 * Copyright: www.eurekareports.com.au
 *
*/
global class JobSchedulerWebService {
	
		webService static void scheduleJobs() {
			Deploy.finalise();
		}
		
		webService static void removeJobs() {
			Deploy.start();
		}
		
		static TestMethod void testScheduleandRemoveJobs() {
			//Initially set up the Custom Setting required for this process
			TestProductInstanceHelper.createAllocationSettings();
			
			//Get the number of jobs that should be scheduled via the Deploy cls
			Integer deployableJobs = Deploy.schedules().size();
			
			//Remove jobs
			JobSchedulerWebService.removeJobs();
			
			//Check that there are now no jobs currently scheduled
			List<CronTrigger> removedTriggers = [SELECT Id, CronExpression, TimesTriggered, NextFireTime, PreviousFireTime, State FROM CronTrigger];
			Integer unscheduledCount = removedTriggers.size();
					
			//Schedule Jobs
			JobSchedulerWebService.scheduleJobs();
			
			//Check jobs have been scheduled
			List<CronTrigger> addedTriggers = [SELECT Id, CronExpression, TimesTriggered, NextFireTime, PreviousFireTime, State FROM CronTrigger];
			Integer scheduledCount = addedTriggers.size();
			System.assertEquals(scheduledCount, unscheduledCount + deployableJobs);
			
			//Finally remove again and check they are gone
			JobSchedulerWebService.removeJobs();
			
			List<CronTrigger> goneTriggers = [SELECT Id, CronExpression, TimesTriggered, NextFireTime, PreviousFireTime, State FROM CronTrigger];
			System.assertEquals(unscheduledCount, goneTriggers.size());
		}
}