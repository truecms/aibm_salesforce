/**
	Class, that contains useful methods for manipulating with Tasks
*/
public without sharing class TaskManager
{
	
	public static FINAL String CREDIT_CARD_EXPIRY_SUBJECT = 'The active Credit Card expires on ';
	public static FINAL String SUBSCRIPTION_EXPIRY_SUBJECT = 'Subscription was not renewed.';
	public static FINAL String NO_CREDIT_CARD_SUBJECT = 'No Credit Card stored on Account for annual AutoRenewal';
	public static FINAL String NO_CREDIT_CARD_SUBJECT_MONTHLY = 'No Credit Card stored on Account for processing monthly payment';
	public static FINAL String FAILED_MONTHLY_PAYMENT_SUBJECT = 'Monthly Recurring payment could not be made';
	public static FINAL String HOURLY_RECURRING_INST_MISSING_CARD_SUBJECT = 'Recurring Instance created, Credit Card is missing - payment will not occur';
	
	public static FINAL String EXPIRED_FREE_TRIAL_SUBJECT = 'Follow Up Call: FT expires in 10 days';
	public static FINAL String LATE_RENEWAL_SUBJECT = 'Late Renewal: Call to assist re-subscription';
	
	public static FINAL String LATE_PAPERWORK_SUBJECT = 'Late Paperwork: numDays days since User registered - no documentation returned';
	
	public static FINAL String ACTIVE_MEMBER_FOLLOWUP_SUBJECT = 'Follow Up Call: Active Member has not become a client after [xxx] days';
	public static FINAL String INACTIVE_MEMBER_FOLLOWUP_SUBJECT = 'Follow Up Call: Inactive Member has not become a client after [xxx] days';
	public static FINAL String IN_PROGRESS_APPLICATION_FOLLOWUP_SUBJECT = 'Follow Up Call: User started Transact application but has not submitted';
	
	public static FINAL String ROLE_CHANGE_SUBJECT = 'Downgrade: Premium Client not longer qualifies for role';
	
	public static FINAL String ALLOCATION_DISPOSITION = 'fair';
	
	public String salesUserQueue {get; set;}
	
	public TaskManager(String userQueue){
		this.salesUserQueue = userQueue;
		
	}
	
	/**
		Returns a list of managers, that are responsible for Accounts.
		It uses AccountManagers Queue to detect them. Hardcoded for threadsafety
	*/
	public static List<Id> managers
	{
		get
		{
			if (managers == null)
			{
				managers = new List<Id>();
				List<GroupMember> aMembers = [SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = 'AccountManagers'
					ORDER BY UserOrGroupId];
				for (GroupMember aMember: aMembers)
				{
					managers.add(aMember.UserOrGroupId);
				}
			}
			return managers;
		}
		set;
	}
	
	/**
		Returns a Map of managers, driven from the queried Account Manager list
		Used to find the next Account Manager in sequence for fair allocation of tasks
	*/
	public static Map<Id, Integer> managerMap
	{
		get
		{
			if (managerMap == null && managers != null)
			{
				managerMap = new Map<Id, Integer>();
				integer i = 0;
				for (Id manager : managers)
				{
					managerMap.put(manager, ++i);
				}
			}
			return managerMap;
		}
		set;
	}
	
	/**
		Returns a list of sales team members, that are selling to Accounts.
	*/
	public List<Id> salesTeamMembers
	{
		get
		{
			if (salesTeamMembers == null)
			{
				salesTeamMembers = new List<Id>();
				List<GroupMember> aMembers = [SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = :salesUserQueue
					ORDER BY UserOrGroupId];
				for (GroupMember aMember: aMembers)
				{
					salesTeamMembers.add(aMember.UserOrGroupId);
				}
			}
			return salesTeamMembers;
		}
		set;
	}
	
	/**
		Returns a Map of sales team members, driven from the queried Sales Team Member list
		Used to find the next Sales Team Member in sequence for fair allocation of tasks
	*/
	public Map<Id, Integer> salesTeamMemberMap
	{
		get
		{
			if (salesTeamMemberMap == null && salesTeamMembers != null)
			{
				salesTeamMemberMap = new Map<Id, Integer>();
				integer i = 0;
				for (Id member : salesTeamMembers)
				{
					salesTeamMemberMap.put(member, ++i);
				}
			}
			return salesTeamMemberMap;
		}
		set;
	}
	
	/**
		Get random manager from a queue.
	*/
	public static Id getRandomManager()
	{
		Integer aSize = managers.size();
		if (aSize == 0)
		{
			return null;
		}
		Decimal aRandom = Math.random();
		aRandom *= aSize;
		Integer anIndex = Integer.valueOf(aRandom.round(RoundingMode.DOWN));
		return managers.get(anIndex);
	}
	
	/** 
		Get next Account Manager in Sequence for fair task allocation
	*/
	public static Id getNextManager(String lastAccountManager)
	{
		if ((managerMap!=null && managerMap.size()>0) && lastAccountManager!=null) {
			
			Id nextAccountManager = null;
			
			if (managerMap.containsKey(lastAccountManager)) {
				integer mId = managerMap.get(lastAccountManager);
				if (mId >= managers.size())
					nextAccountManager = managers.get(0);
				else 
					nextAccountManager = managers.get(mId);
			}
			else
				nextAccountManager = getRandomManager();
				
			return nextAccountManager;
		}
		else
			return getRandomManager();
	}
	
	/**
		Get random sales team member from a queue.
	*/
	public Id getRandomSalesTeamMember()
	{
		Integer aSize = salesTeamMembers.size();
		if (aSize == 0)
		{
			return null;
		}
		Decimal aRandom = Math.random();
		aRandom *= aSize;
		Integer anIndex = Integer.valueOf(aRandom.round(RoundingMode.DOWN));
		return salesTeamMembers.get(anIndex);
	}
	
	/** 
		Get next Sales Team Member in Sequence for fair task allocation
	*/
	public Id getNextSalesTeamMember(String lastSalesTeamMember)
	{
		if ((salesTeamMemberMap!=null && salesTeamMemberMap.size()>0) && lastSalesTeamMember!=null) {
			
			Id nextSalesTeamMember = null;
			
			if (salesTeamMemberMap.containsKey(lastSalesTeamMember)) {
				integer mId = salesTeamMemberMap.get(lastSalesTeamMember);
				if (mId >= salesTeamMembers.size())
					nextSalesTeamMember = salesTeamMembers.get(0);
				else 
					nextSalesTeamMember = salesTeamMembers.get(mId);
			}
			else
				nextSalesTeamMember = getRandomSalesTeamMember();
				
			return nextSalesTeamMember;
		}
		else
			return getRandomSalesTeamMember();
	}
	
	/**
		Creates a task that will be fairly assigned to manager when a subscription was not renewed.
	*/
	public Task generateSubscriptionExpiredTask(Product_Instance__c theSubscription, Account theAccount)
	{
		return generateSubscriptionExpiredTask(theSubscription, theAccount, null);
	}
	
	/**
		Creates a task that will be fairly assigned to manager when a subscription was not renewed.
	*/
	public Task generateSubscriptionExpiredTask(Product_Instance__c theSubscription, Account theAccount, String accountManager)
	{
		Task aResult = new Task();
		aResult.WhatId = theSubscription.Id;
		aResult.WhoId = theAccount == null ? null : theAccount.PersonContactId;
		aResult.ActivityDate = Date.today();
		aResult.Subject = TaskManager.SUBSCRIPTION_EXPIRY_SUBJECT;
		
		
		//Get the lastAccountManager
		Id anOwner = null;
		if (accountManager == null)
		{
			anOwner = getNextSalesTeamMember(AutoRenewalSubscription.lastAccountManager); //This won't correctly allocate the Acc Mgr but we're doing this in the finish method of the batch execution
			
			//Also set a notifying value in the 'Call Result' field to identify the Tasks that need to be fairly allocated post-processing
			aResult.CallDisposition = TaskManager.ALLOCATION_DISPOSITION;
		}
		else
			anOwner = accountManager;
		
		if (anOwner != null)
		{
			aResult.OwnerId = anOwner;
		}
		
		return aResult;
	}
	
	/**
		Generates a task, that will be fairly assinged to manager 7 days before active credit card expires.
	*/
	public Task generateCreditCardExpiresTask(Product_Instance__c theSubscription, Account theAccount, Date theExpirationDate)
	{
		return generateCreditCardExpiresTask(theSubscription, theAccount, theExpirationDate, null);
	}
	
	/**
		Generates a task, that will be fairly assinged to manager 7 days before active credit card expires.
	*/
	public Task generateCreditCardExpiresTask(Product_Instance__c theSubscription, Account theAccount, Date theExpirationDate, String accountManager)
	{
		Task aResult = new Task();
		aResult.WhatId = theSubscription.Id;
		aResult.WhoId = theAccount == null ? null : theAccount.PersonContactId;
		aResult.ActivityDate = Date.today();
		aResult.Subject = TaskManager.CREDIT_CARD_EXPIRY_SUBJECT + theExpirationDate.format();
		
		//Get the lastAccountManager
		Id anOwner = null;
		if (accountManager == null)
		{
			anOwner = getNextSalesTeamMember(CreditCardExpiresBatch.lastAccountManager); //This won't correctly allocate the Acc Mgr but we're doing this in the finish method of the batch execution
			
			//Also set a notifying value in the 'Call Result' field to identify the Tasks that need to be fairly allocated post-processing
			aResult.CallDisposition = TaskManager.ALLOCATION_DISPOSITION;
		}
		else
			anOwner = accountManager;
		
		if (anOwner != null)
		{
			aResult.OwnerId = anOwner;
		}
		
		return aResult;
	}
	
	
	/**
		Generates a task for situations where auto-renewals are due and there are no stored cards against the Account
	*/
	public Task generateNoStoredCreditCardTask(Product_Instance__c theSubscription, Account theAccount)
	{
		return generateNoStoredCreditCardTask(theSubscription, theAccount, null);
	}
	
	/**
		Generates a task for situations where auto-renewals are due and there are no stored cards against the Account
	*/
	public Task generateNoStoredCreditCardTask(Product_Instance__c theSubscription, Account theAccount, String accountManager) 
	{
		Task aResult = new Task();
		aResult.WhatId = theSubscription.Id;
		aResult.WhoId = theAccount == null ? null : theAccount.PersonContactId;
		aResult.ActivityDate = Date.today();
		
		if(theSubscription.Subscription_Type__c == 'm')
			aResult.Subject = TaskManager.NO_CREDIT_CARD_SUBJECT_MONTHLY;
		else
			aResult.Subject = TaskManager.NO_CREDIT_CARD_SUBJECT;
		
		//Get the lastAccountManager
		Id anOwner = null;
		if (accountManager == null)
		{
			anOwner = getNextSalesTeamMember(NoCreditCardBatch.lastAccountManager); //This won't correctly allocate the Acc Mgr but we're doing this in the finish method of the batch execution
			
			//Also set a notifying value in the 'Call Result' field to identify the Tasks that need to be fairly allocated post-processing
			aResult.CallDisposition = TaskManager.ALLOCATION_DISPOSITION;
		}
		else
			anOwner = accountManager;
		
		if (anOwner != null)
		{
			aResult.OwnerId = anOwner;
		}
		
		return aResult;
	}
	
	/**
		Generates a task for situations where auto-renewals are due and there are no stored cards against the Account
	*/
	public Task generateFailedRecurringPaymentTask(Product_Instance__c theSubscription, Account theAccount, String accountManager, String subject) 
	{
		Task aResult = new Task();
		aResult.WhatId = theSubscription.Id;
		aResult.WhoId = theAccount == null ? null : theAccount.PersonContactId;
		aResult.ActivityDate = Date.today();
		
		if (subject == null || subject == '')
			aResult.Subject = TaskManager.FAILED_MONTHLY_PAYMENT_SUBJECT;
		else
			aResult.Subject = subject;
		
		//Get the lastAccountManager
		Id anOwner = null;
		if (accountManager == null)
		{
			anOwner = getNextSalesTeamMember(RecurringInstMissingCardTasks.lastAccountManager); //This won't correctly allocate the Acc Mgr but we're doing this in the finish method of the batch execution
			
			//Also set a notifying value in the 'Call Result' field to identify the Tasks that need to be fairly allocated post-processing
			aResult.CallDisposition = TaskManager.ALLOCATION_DISPOSITION;
		}
		else
			anOwner = accountManager;
		
		if (anOwner != null)
		{
			aResult.OwnerId = anOwner;
		}
		
		return aResult;
	}
	
	
	
	/**
		Generates a task for an Account which had an FT 7 days ago and has not yet subscribed
	*/
	public Task generateExpiredFreeTrialTask(Account theAccount)
	{
		return generateExpiredFreeTrialTask(theAccount, null);
	}
	
	/**
		Generates a task for an Account which had an FT 7 days ago and has not yet subscribed
	*/
	public Task generateExpiredFreeTrialTask(Account theAccount, String accountManager) 
	{
		Task aResult = new Task();
		aResult.WhatId = theAccount.Id;
		aResult.WhoId = theAccount == null ? null : theAccount.PersonContactId;
		aResult.ActivityDate = Date.today();
		
		aResult.Subject = TaskManager.EXPIRED_FREE_TRIAL_SUBJECT;
		
		//Get the lastAccountManager
		Id anOwner = null;
		if (accountManager == null)
		{
			anOwner = getRandomSalesTeamMember(); //This won't correctly allocate the Acc Mgr but we're doing this in the finish method of the batch execution, we can get a random one!
			
			//Also set a notifying value in the 'Call Result' field to identify the Tasks that need to be fairly allocated post-processing
			aResult.CallDisposition = TaskManager.ALLOCATION_DISPOSITION;
		}
		else
			anOwner = accountManager;
		
		if (anOwner != null)
		{
			aResult.OwnerId = anOwner;
		}
		
		return aResult;
	}
	
	/**
		Generates a task for an Account which had a Paid Sub 10 days ago and has not resubscribed (either no autorenew or monthly subs)
	*/
	public Task generateLateRenewalTask(Account theAccount)
	{
		return generateLateRenewalTask(theAccount, null);
	}
	
	/**
		Generates a task for an Account which had a Paid Sub 10 days ago and has not resubscribed (either no autorenew or monthly subs)
	*/
	public Task generateLateRenewalTask(Account theAccount, String accountManager) 
	{
		Task aResult = new Task();
		aResult.WhatId = theAccount.Id;
		aResult.WhoId = theAccount == null ? null : theAccount.PersonContactId;
		aResult.ActivityDate = Date.today();
		
		aResult.Subject = TaskManager.LATE_RENEWAL_SUBJECT;
		
		//Get the lastAccountManager
		Id anOwner = null;
		if (accountManager == null)
		{
			anOwner = getRandomSalesTeamMember(); //This won't correctly allocate the Acc Mgr but we're doing this in the finish method of the batch execution, we can get a random one!
			
			//Also set a notifying value in the 'Call Result' field to identify the Tasks that need to be fairly allocated post-processing
			aResult.CallDisposition = TaskManager.ALLOCATION_DISPOSITION;
		}
		else
			anOwner = accountManager;
		
		if (anOwner != null)
		{
			aResult.OwnerId = anOwner;
		}
		
		return aResult;
	}
	
	
	/**
		Generic Task generation method for Brightday tasks - includes the ability to set a reminder for the user to action the task
	*/
	public Task generateBDTask(Account theAccount, String accountManager, String subject, Datetime reminder) 
	{
		Task aResult = new Task();
		aResult.WhatId = theAccount.Id;
		aResult.WhoId = theAccount == null ? null : theAccount.PersonContactId;
		aResult.ActivityDate = Date.today();
		
		aResult.Subject = subject;
		
		if (reminder != null) {
			aResult.IsReminderSet = true;
			aResult.ReminderDateTime = reminder;
		}
		
		//Get the lastAccountManager
		Id anOwner = null;
		if (accountManager == null)
		{
			anOwner = getRandomSalesTeamMember(); //This won't correctly allocate the Acc Mgr but we're doing this in the finish method of the batch execution, we can get a random one!
			
			//Also set a notifying value in the 'Call Result' field to identify the Tasks that need to be fairly allocated post-processing
			aResult.CallDisposition = TaskManager.ALLOCATION_DISPOSITION;
		}
		else
			anOwner = accountManager;
		
		if (anOwner != null)
		{
			aResult.OwnerId = anOwner;
		}
		
		return aResult;
	}
	
	/**
		Generic Task generation method for BrightDay auto-created tasks
	*/
	public Task generateBDTask(Account theAccount, String subject, Datetime reminder) 
	{
		return generateBDTask(theAccount, null, subject, reminder);
	}
	
	/**
		Generic Task generation method for BrightDay auto-created tasks
	*/
	public Task generateBDTask(Account theAccount, String accountManager, String subject) 
	{
		return generateBDTask(theAccount, accountManager, subject, null);
	}
	
	/**
		Generic Task generation method for BrightDay auto-created tasks
	*/
	public Task generateBDTask(Account theAccount, String subject) 
	{
		return generateBDTask(theAccount, null, subject, null);
	}
}