/**
	Manager, that contains useful methods for Transaction control.
*/
public without sharing class TransactionManager
{
	/**
		Gets Org-wide email
	*/
	private static OrgWideEmailAddress fromObj
	{
		get
		{
			List<OrgWideEmailAddress> aList = [SELECT Id FROM OrgWideEmailAddress WHERE Address = 'enquiries@eurekareport.com.au'];
			if (aList.size() > 0)
			{
				fromObj = aList.get(0);
			} else
			{
				fromObj = new OrgWideEmailAddress();
			}
			return fromObj;
		}
		set;
	}
	
	/**
		Gets Org-wide email
	*/
	public static Id getOrgWideId()
	{
		return (fromObj == null || fromObj.Id == null) ? null : fromObj.Id;
	}
	
	OrgWideEmailAddress[] owea = [SELECT Id FROM OrgWideEmailAddress WHERE Address = 'doNotReply@blahblah.com'];
	
	/**
		Keeps the emal template for invoice sending.
	*/
	public static Id invoiceTemplate
	{
		get
		{
			if (invoiceTemplate == null)
			{
				List<EmailTemplate> aList = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Tax_Invoice'];
				if (aList.size() > 0)
				{
					invoiceTemplate = aList.get(0).Id;
				}
			}
			return invoiceTemplate;
		}
		set;
	}
	
	/**
		Checks the criteria for invoice sending.
	*/
	private static Boolean isReadyForInvoice(go1__Transaction__c theTransaction)
	{
		return theTransaction.go1__Code__c < 10 && theTransaction.go1__Amount__c >= 0;
	}
	
	/**
		Checks if transaction is ready for invoicing.
		It sets up email from Account object and sends an email on this address.
	*/
	public static Boolean sendInvoiceEmail(Id theTransactionId)
	{
		go1__Transaction__c aTransaction = getTransactions(new Set<Id>{theTransactionId}).get(theTransactionId);
		
		// if sandbox and email does not have correct domain
		if (AIBMHelper.inWhiteListEmail(aTransaction.go1__Account__r.PersonEmail))
		{
			if (isReadyForInvoice(aTransaction) && aTransaction.go1__Account__r.PersonContactId != null && aTransaction.go1__Account__r.PersonEmail != null)
			{
				aTransaction.Account_Email__c = aTransaction.go1__Account__r.PersonEmail;
				update aTransaction;
				sendEmail(new List<go1__Transaction__c>{aTransaction}, invoiceTemplate);
				return true;
			}
		}
		return false;
	}
	
	/**
		Checks if transactions are ready for invoicing.
		It sets up email from Account object and sends an email on this address.
	*/
	public static void sendInvoiceEmail(Set<Id> theTransactionIDs)
	{
		List<go1__Transaction__c> anUpdTransactions = new List<go1__Transaction__c>();
		List<go1__Transaction__c> aTransactions = getTransactions(theTransactionIDs).values();
		
		for (go1__Transaction__c aTrans: aTransactions)
		{
			if (aTrans.Send_Invoice__c == true && AIBMHelper.inWhiteListEmail(aTrans.go1__Account__r.PersonEmail))
			{
				if (isReadyForInvoice(aTrans) && aTrans.go1__Account__r.PersonContactId != null && aTrans.go1__Account__r.PersonEmail != null )
				{
					anUpdTransactions.add(aTrans);
					//aTrans.Account_Email__c = aTrans.go1__Account__r.PersonEmail; //Moved to UpdateTransaction (before) trigger
				}
			}
		}
		if (anUpdTransactions.size() > 0)
		{
			//update anUpdTransactions; //Moved to UpdateTransaction (before) trigger
			sendEmail(anUpdTransactions, invoiceTemplate);
		}
	}
	
	/**
		Generates invoice email and sends on transaction's email
	*/
	private static void sendEmail(List<go1__Transaction__c> theTransactions, Id theEmailTemplate)
	{
		List<Messaging.SingleEmailMessage> aMessageList = new List<Messaging.SingleEmailMessage>();
		for (go1__Transaction__c aTrans: theTransactions)
		{
			Messaging.SingleEmailMessage aMessage = new Messaging.SingleEmailMessage();
			aMessage.setTemplateId(theEmailTemplate);
			aMessage.setWhatId(aTrans.Id);
			if (getOrgWideId() == null)
			{
				aMessage.setSenderDisplayName('Eureka Report');
				aMessage.setReplyTo('enquiries@eurekareport.com.au');
			} else
			{
				aMessage.setOrgWideEmailAddressId(getOrgWideId());
			}
			aMessage.setTargetObjectId(aTrans.go1__Account__r.PersonContactId);
			aMessage.setSaveAsActivity(false);
			aMessageList.add(aMessage);
		}
		System.debug('***Sending: ' + aMessageList);
		Messaging.sendEmail(aMessageList);
	}
	
	/**
		Gets transaction's email.
	*/
	private static Map<Id, go1__Transaction__c> getTransactions(Set<Id> theTransId)
	{
		return new Map<Id, go1__Transaction__c>([SELECT Id, go1__Code__c, go1__Amount__c, Send_Invoice__c,
				  go1__Status__c, go1__Account__c, go1__Account__r.PersonEmail, go1__Account__r.PersonContactId
				  FROM go1__Transaction__c WHERE Id IN :theTransId]);
	}
}