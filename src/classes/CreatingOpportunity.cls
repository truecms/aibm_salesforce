/**
 *
 * Class to create BD opportunities through a nightly job.
 * Created on 10-09-2015
 * Author: Mithun Roy
 * Copyright: EurekaReport.com.au
 */
public class CreatingOpportunity {  
    // Opportunity to be created by the nightly job for various scenarios.
    public void CreateOpportunity() { 
        try {
            // Get the agent name from custom setting. This gives flexibility to change agents as required.
            List<Assignment__c> Ag  = [select RowId__c, Min__c, Max__c from Assignment__c];      
            List<Opportunity> oppList = new List<Opportunity>();  //List stores parameters for each opportunity to be created.
            //Query to get all the accounts who have started a one view application and reached till page3 in the last 2 days.
            List<Account> acctsApplicationStartedPage3 =    [SELECT Id, Name 
                                                            FROM Account 
                                                            WHERE OneVue_Application_Product__c != null 
                                                            AND OneVue_Application_Status__c IN('Not Started','in_progress','paperwork_sent','awaiting_paperwork') 
                                                            AND OV_Created_Date__c =  N_DAYS_AGO:2];  
            //For each account in the list, create opportunity and assign it to agents randomly.
            for (Account a : acctsApplicationStartedPage3) {
                Opportunity Opp = new Opportunity();
                Opp.Name = 'Application Started Page3'+' : '+a.Name;
                Opp.AccountID = a.Id;  
                Opp.CloseDate= System.Today() + 30; // Set this to current date + 30 days.
                Opp.StageName= '30 Day Trial';  
                Opp.Type = 'Existing'; 
                //Query the custom setting 'Assignment' and get the values.
                for(Assignment__c clist : Ag) {                
                    Decimal rand = Math.random();
                    if (rand >= clist.Min__c && rand <= clist.Max__c) {
                        Opp.OwnerId = clist.RowId__c;
                    }
                }
                oppList.Add(Opp);        
            }
            
            // Opportunity should be created for Customers who have just started a OneVue application.
            List<Account> acctsApplicationStarted =     [SELECT Id, Name 
                                                        FROM Account 
                                                        WHERE OneVue_Application_Product__c != null 
                                                        AND OV_Created_Date__c =  N_DAYS_AGO:2];
            //For each account in the list, create opportunity and assign it to agents randomly.
            for (Account a : acctsApplicationStarted) {
                Opportunity Opp = new Opportunity();
                Opp.Name = 'Application Started'+' : '+a.Name;
                Opp.AccountID = a.Id; 
                Opp.CloseDate= System.Today() + 30; // Set this to current date + 30 days.
                Opp.StageName= '30 Day Trial';  
                Opp.Type = 'Existing'; 
                for(Assignment__c blist : Ag) {                
                    Decimal rand = Math.random();
                    if (rand >= blist.Min__c && rand <= blist.Max__c) {
                        Opp.OwnerId = blist.RowId__c;
                    }
                }
                oppList.Add(Opp);                  
            }
            
            Assignment__c Ag1 = Assignment__c.getValues('Agent1');
                            
            // Opportunity should be created for Customers who have registered 10 days ago and signed up for a webinar.
            List<Account> acctsWebinarSignedUp =    [SELECT Id, Name 
                                                    FROM Account 
                                                    WHERE BD_Registration_Date__c = N_DAYS_AGO:10 
                                                    AND BD_Last_Login_Date__c > N_DAYS_AGO:10 
                                                    AND Webinar_Email__c != null];
            //For each account in the list, create opportunity.
            for (Account a : acctsWebinarSignedUp) {
                Opportunity Opp = new Opportunity();
                Opp.Name = 'Customer logged in, signed up for webinar'+' : '+a.Name;
                Opp.CloseDate= System.Today() + 30; // Set this to current date + 30 days.
                Opp.StageName= '30 Day Trial';  
                Opp.Type = 'Existing'; 
                Opp.OwnerId = Ag1.RowId__c;
                Opp.AccountID = a.Id;  
                oppList.Add(Opp);                  
            }            
            // Opportunity should be created for Customers who have registered 10 days ago but not signed up for a webinar.
            List<Account> acctsNonWebinarSignedUp =     [SELECT Id, Name 
                                                        FROM Account 
                                                        WHERE BD_Registration_Date__c = N_DAYS_AGO:10 
                                                        AND BD_Last_Login_Date__c > N_DAYS_AGO:10 
                                                        AND Webinar_Email__c= null];
            //For each account in the list, create opportunity.
            for (Account a : acctsNonWebinarSignedUp) {
                Opportunity Opp = new Opportunity();
                Opp.Name = 'Customer logged in, but not signed up for webinar'+' : '+a.Name;
                Opp.AccountID = a.Id; 
                Opp.CloseDate= System.Today() + 30; // Set this to current date + 30 days.
                Opp.StageName= '30 Day Trial';  
                Opp.Type = 'Existing'; 
                Opp.OwnerId = Ag1.RowId__c;
                oppList.Add(Opp);                  
            }            
            // Opportunity should be created for Customers who are not active members but have signed up for a webinar.
            List<Account> acctsCustomerLoggedIn =   [SELECT Id, Name 
                                                    FROM Account 
                                                    WHERE BD_Current_Role__c = null
                                                    AND Webinar_Email__c = true];
            //For each account in the list, create opportunity.
            for (Account a : acctsCustomerLoggedIn) {
                Opportunity Opp = new Opportunity();
                Opp.Name = 'Webinar Signup Inactive'+' : '+a.Name;
                Opp.AccountID = a.Id;  
                Opp.CloseDate= System.Today() + 30; // Set this to current date + 30 days.
                Opp.StageName= '30 Day Trial';  
                Opp.Type = 'Existing'; 
                Opp.OwnerId = Ag1.RowId__c;
                oppList.Add(Opp);                  
            }
             insert oppList; // Insert the opportunities.
                    
        } 
        catch(DmlException e) {
            ApexPages.addMessages(e);
            Messaging.SingleEmailMessage mail=new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {'tech.support@eurekareport.com.au'};
            mail.setToAddresses(toAddresses);
            mail.setReplyTo('tech.support@eurekareport.com.au');
            mail.setSenderDisplayName('Apex error message');
            mail.setSubject('Error from Org : ' + UserInfo.getOrganizationName());
            mail.setPlainTextBody('The following exception has occurred : ' + e.getMessage());          
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
}