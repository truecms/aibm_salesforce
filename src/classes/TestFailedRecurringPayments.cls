/**
 * Testing the FailedRecurringPayments class
 * Author: tailbyr
 * Copyright: eurekareport.com.au
 */
@isTest
private class TestFailedRecurringPayments {

	static Subscription_Product__c createSubs()
	{
		//Create subscription stream
    	Subscription_Stream__c stream = new Subscription_Stream__c(Name='Eureka Report');
	  	insert stream;
	  	Subscription_Product__c product = new Subscription_Product__c();
	  	product.Subscription_Stream__c = stream.Id;
	  	product.Duration__c = 1;
	  	product.Duration_units__c = 'month';
	  	product.Price__c = 100;
	  	insert product;
	  	return product;
	}
	
	static RecordType getRecordType()
	{
		// Find any person account record type
	    RecordType recordType = [ select Id, Name, DeveloperName from RecordType where SObjectType = 'Account' and IsPersonType = true limit 1 ];
	    return recordType;
	}
	
	static List<Account> createAccounts(Integer numAccounts, RecordType recordType) 
	{
		//Create the accounts for test
    	List<Account> accs = new List<Account>(); 
    	List<Id> accIds = new List<Id>();
    	
    	for (integer i = 0; i < numAccounts; i++) {
    		Account account = new Account(FirstName='Test' +i, LastName='Tester' +i, PersonEmail='test'+i+'@test.com.test');
	  		account.RecordTypeId = recordType.Id;
	  		account.Sub_Status__c = '4';  // paid subscription
	  		accs.add(account);
    	}
    	insert accs;
    	return accs;
	}


	// Check that the No credit card against account task has been created for that scenario, and no other tasks!  
    static testMethod void testFailedRecurringPaymentTaskCreated() 
    {
     	//Set up the custom settings for queue to use and whether to utilise assignment rules or not
		TestProductInstanceHelper.createAllocationSettings();
		     	
     	Subscription_Product__c product = createSubs();
     	RecordType recordType = getRecordType();
     
     	//Create just 1 account
    	List<Account> accountList = createAccounts(1, recordType);
    	Account account = accountList[0];
    	
    	
    	//Create Product Instance 
    	Product_Instance__c instance = new Product_Instance__c();
	  	instance.Person_Account__c = account.Id;
	  	instance.Subscription_Product__c = product.Id;
	  	instance.Subscription_Type__c = 'y';
	  	instance.Amount__c = 100;
	  	instance.Auto_Renewal__c = true;
    	
    	insert instance;    	
	  		
	  	//Create Recurring Instance
	  	go1__Recurring_Instance__c recur = new go1__Recurring_Instance__c();
	  	recur.go1__Account__c = account.Id;
	  	recur.go1__Amount__c = 38.50;
	  	recur.go1__Interval__c = 'Monthly';
	  	recur.go1__Remaining_Attempts__c = 10;
	  	recur.go1__Payment_Account_Type__c = 'Credit Card';
        recur.go1__Next_Attempt__c = Date.today().addDays(-1);
        recur.go1__Expired__c = true;
        recur.go1__Cancelled__c = true;
        
        insert recur;
        
        //Create failed transaction
        go1__Transaction__c trans = new go1__Transaction__c();
        trans.go1__Account__c = account.Id;
        trans.go1__Account_ID__c = account.Id;
        trans.go1__Amount__c = 38.50;
        trans.go1__Recurring_Instance__c = recur.Id;
        trans.Subscription_Instance__c = instance.Id;
        trans.go1__Status__c = 'Reserved Error';
        trans.go1__Code__c = 100;
        trans.go1__Payment_Name__c = 'TEST CREDIT CARD';
        
        insert trans;        
        
        Test.startTest();
        
		//Run the FailedRecurringPayment process to create the tasks
		Database.executeBatch(new FailedRecurringPaymentsTasks(), 1);
        Test.stopTest();
        
        //Check we have a task
        List<Task> failedPaymentTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
				where Subject = :TaskManager.FAILED_MONTHLY_PAYMENT_SUBJECT 
				AND createdDate = TODAY
				ORDER BY createdDate desc];
		System.assertEquals(failedPaymentTasks.size(), 1);
		System.assertEquals(failedPaymentTasks[0].WhatId, instance.Id);
		
		//Check we don't have any other tasks!
		List<Task> anyOtherTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
				where Subject != :TaskManager.FAILED_MONTHLY_PAYMENT_SUBJECT 
				AND createdDate = TODAY
				ORDER BY createdDate desc];
		System.assertEquals(anyOtherTasks.size(), 0);
    }
    
    
    // Create a number of the noCreditCard scenarios and check that all tasks are shared out equally across Account Managers
    static testMethod void testFailedRecurringPaymentTaskCreatedFairly()
    {
    	//Set up the custom settings for queue to use and whether to utilise assignment rules or not
		TestProductInstanceHelper.createAllocationSettings();
		    	
    	Subscription_Product__c product = createSubs();
     	RecordType recordType = getRecordType();
     
     	//Create 6 test accounts
    	List<Account> accountList = createAccounts(6, recordType);
    	List<Product_Instance__c> instances = new List<Product_Instance__c>();
    	List<go1__Recurring_Instance__c> recurs = new List<go1__Recurring_Instance__c>();
    	
    	for (Account acc : accountList)
    	{
    		Product_Instance__c instance = new Product_Instance__c();
	  		instance.Person_Account__c = acc.Id;
	  		instance.Subscription_Product__c = product.Id;
	  		instance.Subscription_Type__c = 'y';
	  		instance.Amount__c = 100;
	  		instance.Auto_Renewal__c = true;
	  		instances.add(instance);
	  		
	  		//Create Recurring Instance
	  		go1__Recurring_Instance__c recur = new go1__Recurring_Instance__c();
	  		recur.go1__Account__c = acc.Id;
	  		recur.go1__Amount__c = 38.50;
	  		recur.go1__Interval__c = 'Monthly';
	  		recur.go1__Remaining_Attempts__c = 10;
	  		recur.go1__Payment_Account_Type__c = 'Credit Card';
        	recur.go1__Next_Attempt__c = Datetime.now();
        	recur.go1__Expired__c = true;
        	recur.go1__Cancelled__c = true;
	        recurs.add(recur);
    	}
    	insert instances;
    	insert recurs;
    
    	//Ensure accounts, instances and recurs are all the same size
    	System.assertEquals(accountList.size(), instances.size(), recurs.size());
    
    	List<go1__Transaction__c> transList = new List<go1__Transaction__c>();
    	for (Integer i = 0; i < accountList.size(); i++)
    	{
    		//Create failed transaction
	        go1__Transaction__c trans = new go1__Transaction__c();
	        trans.go1__Account__c = accountList[i].Id;
	        trans.go1__Account_ID__c = accountList[i].Id;
	        trans.go1__Amount__c = 38.50;
	        trans.go1__Recurring_Instance__c = recurs[i].Id;
	        trans.Subscription_Instance__c = instances[i].Id;
	        trans.go1__Status__c = 'Reserved Error';
	        trans.go1__Code__c = 100;
	        trans.go1__Payment_Name__c = 'TEST CREDIT CARD';
	        
	        transList.add(trans);
    	}
    	insert transList;
    
    	Test.startTest();
    	
    	//Call the Monthly payments batch to create the failed transaction and cancel/expire the Recurring instance
        Database.executeBatch(new FailedRecurringPaymentsTasks(), instances.size());
    	Test.stopTest();
    	
    	//Check we have the right number of tasks
        List<Task> failedRecurringPaymentTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
				where Subject = :TaskManager.FAILED_MONTHLY_PAYMENT_SUBJECT 
				AND createdDate = TODAY
				ORDER BY createdDate desc];
		System.assertEquals(failedRecurringPaymentTasks.size(), instances.size());
		
		//Check that each account manager has the same number of these tasks
		checkAllocationOfTasks(failedRecurringPaymentTasks, instances.size());
    }
    
    // Create a number of the noCreditCard scenarios and check that if a task is initially allocated to a given acc mgr, new ones are allocated to the same person
    @IsTest(seealldata=true)
    static void testFailedRecurringPaymentTaskSameAccMgr()
    {
    	//Pick the first Account Manager for setting all the task ownership
    	GroupMember aMember = [SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = 'AccountManagers' ORDER BY UserOrGroupId LIMIT 1];
    	
    	//Create a TaskManager instance with the same user group
    	TaskManager taskMgr = new TaskManager('AccountManagers');
    	
    	//Also set the allocation rules so they are used
    	Boolean allocationRules = true;
    	
    	Subscription_Product__c product = createSubs();
     	RecordType recordType = getRecordType();
     
     	//Create 6 test accounts
    	List<Account> accountList = createAccounts(6, recordType);
    	List<Product_Instance__c> instances = new List<Product_Instance__c>();
    	List<go1__Recurring_Instance__c> recurs = new List<go1__Recurring_Instance__c>();
    	List<Task> tasks = new List<Task>();
    	
    	List<Id> accIds = new List<Id>();
    	
    	for (Account acc : accountList)
    	{
    		//Add accId to the list to ensure we only pick out the tasks created during this test
    		accIds.add(acc.Id);
    		
    		//Create a task against the account for a given acc mgr
    		Task newTask = new Task();
    		newTask.WhatId = acc.Id;
			newTask.ActivityDate = Date.today();
			newTask.Subject = 'Test task';
			newTask.OwnerId = aMember.UserOrGroupId;
			tasks.add(newTask);
    		
    		Product_Instance__c instance = new Product_Instance__c();
	  		instance.Person_Account__c = acc.Id;
	  		instance.Subscription_Product__c = product.Id;
	  		instance.Subscription_Type__c = 'y';
	  		instance.Amount__c = 100;
	  		instance.Auto_Renewal__c = true;
	  		instance.status__c = 'test product instance';
	  		instances.add(instance);
	  		
	  		//Create Recurring Instance
	  		go1__Recurring_Instance__c recur = new go1__Recurring_Instance__c();
	  		recur.go1__Account__c = acc.Id;
	  		recur.go1__Amount__c = 38.50;
	  		recur.go1__Interval__c = 'Monthly';
	  		recur.go1__Remaining_Attempts__c = 10;
	  		recur.go1__Payment_Account_Type__c = 'Credit Card';
        	recur.go1__Next_Attempt__c = Datetime.now();
        	recur.go1__Expired__c = true;
        	recur.go1__Cancelled__c = true;
	        recurs.add(recur);
    	}
    	insert instances;
    	insert recurs;
    	insert tasks;
	
    	//Ensure accounts, instances and recurs are all the same size
    	System.assertEquals(accountList.size(), instances.size(), recurs.size());
    	
    	List<go1__Transaction__c> transList = new List<go1__Transaction__c>();
    	for (Integer i = 0; i < accountList.size(); i++)
    	{
    		//Create failed transaction
	        go1__Transaction__c trans = new go1__Transaction__c();
	        trans.go1__Account__c = accountList[i].Id;
	        trans.go1__Account_ID__c = accountList[i].Id;
	        trans.go1__Amount__c = 38.50;
	        trans.go1__Recurring_Instance__c = recurs[i].Id;
	        trans.Subscription_Instance__c = instances[i].Id;
	        trans.go1__Status__c = 'Reserved Error';
	        trans.go1__Code__c = 100;
	        trans.go1__Payment_Name__c = 'TEST CREDIT CARD';
	        
	        transList.add(trans);
    	}
    	insert transList;
    	
    	//Get the PersonContactIds from the Accounts
    	List<Id> personAccs = new List<Id>();
    	List<Account> personAccDetails = [SELECT Id, PersonContactId FROM Account WHERE Id IN :accIds];
    	for (Account pc : personAccDetails){
    		personAccs.add(pc.PersonContactId);
    	}
    	
    	Test.startTest();
    	
    	//Get the Product Instances that the method needs to call
    	List<go1__Transaction__c> testTransactionList = [SELECT Subscription_Instance__c FROM go1__Transaction__c
										WHERE LastModifiedDate >= TODAY
										AND go1__Recurring_Instance__r.go1__Next_Attempt__c <= :Datetime.now()
										AND go1__Recurring_Instance__r.go1__Expired__c = true
										AND go1__Recurring_Instance__r.go1__Cancelled__c = true
										AND go1__Code__c > 10]; 
		List<Id> subs = new List<Id>();
		for (go1__Transaction__c trans : testTransactionList) {
			subs.add(trans.Subscription_Instance__c);
		}
		
		List<Product_Instance__c> piList = [SELECT Id, End_Date__c, Person_Account__c FROM Product_Instance__c 
										WHERE Id IN : subs
										AND Status__c = 'test product instance'];
										
		ProductInstanceHelper pih = new ProductInstanceHelper(piList);
    	pih.createFailedRecurringPaymentTasks(null, taskMgr, allocationRules);
    	
    	Test.stopTest();
    	
    	//Check we have the right number of tasks
        List<Task> failedRecurringPaymentTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId, WhoId 
        		FROM Task 
				where Subject = :TaskManager.FAILED_MONTHLY_PAYMENT_SUBJECT 
				AND createdDate = TODAY
				AND WhoId IN :personAccs
				ORDER BY createdDate desc];
		
		System.assertEquals(failedRecurringPaymentTasks.size(), instances.size());
		
		//Check each task created belongs to aMember
		for (Task t : failedRecurringPaymentTasks)
		{
			System.assertEquals(t.ownerId, aMember.UserOrGroupId);
		}
    }
    
    
    static void checkAllocationOfTasks(List<Task> failedPaymentTasks, Integer noOfProductInstances)
    {
    	List<GroupMember> aMembers = [SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = 'AccountManagers' ORDER BY UserOrGroupId];
    	
    	//Ensure that these tasks were allocated equally
		if (failedPaymentTasks!=null && failedPaymentTasks.size()>0){
			Map<Id, Integer> tasksPerManager = new Map<Id, Integer>();
			for (Task t : failedPaymentTasks) {
					
				//Count tasks per Account Manager
				if (tasksPerManager.containsKey(t.ownerId)){
					Integer count = tasksPerManager.get(t.ownerId);
					count++;
					tasksPerManager.put(t.ownerId, count);
				}
				else {
					tasksPerManager.put(t.ownerId, 1);
				}
			}
			
			//Work out how many tasks each Account Manager must have (not counting remainder)
			Integer noOfTasks = Math.floor(noOfProductInstances / aMembers.size()).intValue();
			List<Integer> counts = tasksPerManager.values();
			for (Integer val : counts){
				if (val == noOfTasks || val == noOfTasks+1) //+1 to account for the remainder where noOfTasks%noOfAccountManagers != 0
					System.assert(true);
				else
					System.assert(false);
			}
		}
		else
			System.assert(false);
    }
    
}