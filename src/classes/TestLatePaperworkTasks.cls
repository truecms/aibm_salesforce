/**
 * Testing the LatePaperworkTasks class
 * Author: tailbyr
 * Copyright: eurekareport.com.au
 */
@isTest
public with sharing class TestLatePaperworkTasks {

	//All calls in Days
	public static Integer firstCall = -7;
	public static Integer secondCall = -26;
	public static String firstMemberCall = '-45';

	/** 
	* Utility Methods 
	*/
	public static Allocation_Rules__c createAllocationSettings() 
	{
		Allocation_Rules__c allocationRules = new Allocation_Rules__c();
		allocationRules.Name = 'Default';
		
		allocationRules.Late_Paperwork_Queue__c = 'SalesTeamMembers';
		allocationRules.Late_Paperwork_Allocation_Rule__c = true;
		
		upsert allocationRules;
		return allocationRules;
	}
	
	public static BrightDay_Settings__c createBrightDaySettings()
	{
		BrightDay_Settings__c settings = BrightDay_Settings__c.getOrgDefaults();
		settings.First_No_Paperwork_Task__c = firstCall;
		settings.Second_No_Paperwork_Task__c = secondCall;
		settings.Member_Followup_Call_Day__c = firstMemberCall;
		settings.Refresh_brightday_Endpoint__c = 'testendpoint.brightday.com.au';
		
		upsert settings;
		return settings;
	}
	
	public static User getDrupalUser(){
		User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
		return drupalUser;
	}
	/*********************************************************/

	/**
	* Test Methods
	*/
	static TestMethod void testLatePaperworkFirstTaskCreated()
	{
		//Set up the custom settings for queue to use and whether to utilise assignment rules or not
		TestLatePaperworkTasks.createAllocationSettings();
		BrightDay_Settings__c settings = TestLatePaperworkTasks.createBrightDaySettings();
		
		//Create just 1 account
    	List<Account> accounts = TestAccountHelper.createAccounts(1);
    	Account account = accounts[0];
    	
    	//Set the values required for OV client created, then set the OV user created date back to 7 days ago (it will initially be set to today by the trigger)
    	System.runAs(getDrupalUser()){
	    	for (Account acc : accounts) {
	    		acc.BD_Current_Role__c = 'Premium Client';
		  		acc.OV_User_Object_Created__c = true;
		  		acc.Total_Investment_Value__c = 0.0;
		  		acc.OneVue_Application_Status__c = 'awaiting_paperwork';
	    	}
	    	update accounts;
	    	
	    	for (Account acc : accounts) {
	    		acc.OV_Status_Change_Date__c = Date.today().addDays(firstCall);
	    	}
	    	update accounts;
    	}
    	
    	Test.startTest();        
        Database.executeBatch(new LatePaperworkTasks(settings.First_No_Paperwork_Task__c.intValue()), 1);
        Test.stopTest();
        
        //Get the subject that should be used
        String subject = 'Late Paperwork: ' + math.abs(firstCall) + ' days since User registered - no documentation returned';
        
        //Check we have a task
        List<Task> latePaperworkTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
				where Subject = :subject
				AND createdDate = TODAY
				ORDER BY createdDate desc];
		System.assertEquals(latePaperworkTasks.size(), 1);
		System.assertEquals(latePaperworkTasks[0].WhatId, account.Id);
		
		//Check we don't have any other tasks!
		List<Task> anyOtherTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
				where Subject != :subject 
				AND createdDate = TODAY
				ORDER BY createdDate desc];
		System.assertEquals(anyOtherTasks.size(), 0);
	}

	static TestMethod void testLatePaperworkSecondTaskCreated()
	{
		//Set up the custom settings for queue to use and whether to utilise assignment rules or not
		TestLatePaperworkTasks.createAllocationSettings();
		BrightDay_Settings__c settings = TestLatePaperworkTasks.createBrightDaySettings();
		
		//Create just 1 account
    	List<Account> accounts = TestAccountHelper.createAccounts(1);
    	Account account = accounts[0];
    	
    	//Set the values required for OV client created, then set the OV user created date back to 7 days ago (it will initially be set to today by the trigger)
    	System.runAs(getDrupalUser()){
	    	for (Account acc : accounts) {
	    		acc.BD_Current_Role__c = 'Premium Client';
		  		acc.OV_User_Object_Created__c = true;
		  		acc.Total_Investment_Value__c = 0.0;
		  		acc.OneVue_Application_Status__c = 'awaiting_paperwork';
	    	}
	    	update accounts;
	    	
	    	for (Account acc : accounts) {
	    		acc.OV_Status_Change_Date__c = Date.today().addDays(secondCall);
	    	}
	    	update accounts;
    	}
    	
    	Test.startTest();        
        Database.executeBatch(new LatePaperworkTasks(settings.Second_No_Paperwork_Task__c.intValue()), 1);
        Test.stopTest();
        
        //Get the subject that should be used
        String subject = 'Late Paperwork: ' + math.abs(secondCall) + ' days since User registered - no documentation returned';
        
        //Check we have a task
        List<Task> latePaperworkTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
				where Subject = :subject
				AND createdDate = TODAY
				ORDER BY createdDate desc];
		System.assertEquals(latePaperworkTasks.size(), 1);
		System.assertEquals(latePaperworkTasks[0].WhatId, account.Id);
		
		//Check we don't have any other tasks!
		List<Task> anyOtherTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
				where Subject != :subject 
				AND createdDate = TODAY
				ORDER BY createdDate desc];
		System.assertEquals(anyOtherTasks.size(), 0);
	}

	//Create a number of Expired FT scenarios and ensure they are all shared equally across the Sales Team
	static TestMethod void testLatePaperworkFirstTasksCreatedFairly()
	{
		//Set up the custom settings for queue to use and whether to utilise assignment rules or not
		TestLatePaperworkTasks.createAllocationSettings();
		BrightDay_Settings__c settings = TestLatePaperworkTasks.createBrightDaySettings();
		
		Integer noOfAccs = 6;
		
		//Create noOfAccs test accounts
		List<Account> accountList = TestAccountHelper.createAccounts(noOfAccs);
		
		//Set the OV user created date back to 7 days ago
		System.runAs(getDrupalUser()){
	    	for (Account acc : accountList) {
	    		acc.BD_Current_Role__c = 'Premium Client';
		  		acc.OV_User_Object_Created__c = true;
		  		acc.Total_Investment_Value__c = 0.0;
		  		acc.OneVue_Application_Status__c = 'awaiting_paperwork';
	    	}
	    	update accountList;
	    	
	    	for (Account acc : accountList) {
	    		acc.OV_Status_Change_Date__c = Date.today().addDays(firstCall);
	    	}
	    	update accountList;
		}
		
		Test.startTest();
		Database.executeBatch(new LatePaperworkTasks(settings.First_No_Paperwork_Task__c.intValue()), noOfAccs);
		Test.stopTest();
		
		//Get the subject that should be used
        String subject = 'Late Paperwork: ' + math.abs(firstCall) + ' days since User registered - no documentation returned';
		
		//Initially check the right number of tasks were created
		List<Task> latePaperworkTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
				where Subject = :subject
				AND createdDate = TODAY
				ORDER BY createdDate desc];
		System.assertEquals(latePaperworkTasks.size(), noOfAccs);
		
		//Now check that each Sales User has the same number of these tasks
		TestExpiredFreeTrialTasks.checkAllocationOfTasks(latePaperworkTasks, noOfAccs, 'SalesTeamMembers');
	}
	
	//Test that an Account owned by a User in the SalesTeamMember group will have the tasks always allocated to them
	static TestMethod void testLatePaperworkFirstTaskWithAccOwner()
	{
		//Set up the custom settings for queue to use and whether to utilise assignment rules or not
		TestLatePaperworkTasks.createAllocationSettings();
		BrightDay_Settings__c settings = TestLatePaperworkTasks.createBrightDaySettings();
		
		//Pick the first Account Manager for setting all the task ownership
    	GroupMember aMember = [SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = 'SalesTeamMembers' ORDER BY UserOrGroupId LIMIT 1];
    	Integer noOfAccs = 6;
		
		//Create noOfAccs test accounts
		List<Account> accountList = TestAccountHelper.createAccounts(noOfAccs);
		
		System.runAs(getDrupalUser()){
			for (Account acc : accountList)
	    	{
	    		acc.OwnerId = aMember.UserOrGroupId;
	    		acc.BD_Current_Role__c = 'Premium Client';
		  		acc.OV_User_Object_Created__c = true;
		  		acc.Total_Investment_Value__c = 0.0;
		  		acc.OneVue_Application_Status__c = 'awaiting_paperwork';
	    	}
	    	update accountList;
	    	
	    	for (Account acc : accountList) {
	    		acc.OV_Status_Change_Date__c = Date.today().addDays(firstCall);
	    	}
	    	update accountList;
		}
		
		Test.startTest();
		Database.executeBatch(new LatePaperworkTasks(settings.First_No_Paperwork_Task__c.intValue()), noOfAccs);
		Test.stopTest();
		
		//Get the subject that should be used
        String subject = 'Late Paperwork: ' + math.abs(firstCall) + ' days since User registered - no documentation returned';
		
		//Check that the right number of tasks were created
		List<Task> latePaperworkTasks = [SELECT Id, OwnerId FROM Task 
									WHERE Subject = :subject
									AND createdDate = TODAY];
		System.assertEquals(latePaperworkTasks.size(), noOfAccs);
		
		//Check that all those tasks were assigned to our aMember retrievd earlier
		for (Task t : latePaperworkTasks)
		{
			System.assertEquals(t.OwnerId, aMember.UserOrGroupId);
		}
	}
	
	static TestMethod void testLatePaperworkFirstTaskNotCreated() 
	{
		Integer numAccs = 7;
		
		//Set up the custom settings for queue to use and whether to utilise assignment rules or not
		TestLatePaperworkTasks.createAllocationSettings();
		BrightDay_Settings__c settings = TestLatePaperworkTasks.createBrightDaySettings();
		
		//Create 3 data-scenario account
    	List<Account> accounts = TestAccountHelper.createAccounts(numAccs);
    	
    	//Set the values required for OV client created late paperwork tasks, then set non-compliant values individually to ensure tasks NOT created
    	System.runAs(getDrupalUser()){
	    	for (Account acc : accounts) {
	    		acc.BD_Current_Role__c = 'Premium Client';
		  		acc.OV_User_Object_Created__c = true;
		  		acc.Total_Investment_Value__c = 0.0;
		  		acc.OneVue_Application_Status__c = 'awaiting_paperwork';
	    	}
	    	update accounts;
	    	
	    	accounts[0].OV_Status_Change_Date__c = Date.today().addDays(firstCall - 1); //1 day different to firstCall
	    	accounts[1].OV_Status_Change_Date__c = Date.today().addDays(firstCall);
	    	accounts[1].Total_Investment_Value__c = 150000.00; // Total Investment Value > 0.00 => This now should create a task
	    	accounts[2].OV_Status_Change_Date__c = Date.today().addDays(firstCall);
	    	accounts[2].OV_User_Object_Created__c = false; //OV User Object NOT created
	    	accounts[3].OV_Status_Change_Date__c = Date.today().addDays(firstCall);
	    	accounts[3].BD_Current_Role__c = null; //Set BD Current Role == null
	    	accounts[4].OV_Status_Change_Date__c = Date.today().addDays(firstCall);
	    	
	    	//For the 5th account, create an Investment Account for it - should fit all other filter requirements => This now should create a task
	    	Investment_Account__c invAcc = new Investment_Account__c();
	    	invAcc.Account__c = accounts[4].Id;
	    	invAcc.Name = 'Test Investment Account';
	    	invAcc.Investment_Account_Type__c = 'SMSF';
	    	insert invAcc;
	    	
	    	//The 6th account will have a different OneVue application Status
	    	accounts[5].OneVue_Application_Status__c = 'in_progress';
	    	
	    	//Finally, the 6th account is the control - one that does fit all the requirements
	    	accounts[6].OV_Status_Change_Date__c = Date.today().addDays(firstCall);
	    	
	    	update accounts;
    	}
    	
    	Test.startTest();        
        Database.executeBatch(new LatePaperworkTasks(settings.First_No_Paperwork_Task__c.intValue()), numAccs);
        Test.stopTest();
        
        //Confirm that 3 tasks has been created
        List<Task> tasks = [SELECT Id, WhatId FROM Task];
        System.assertEquals(tasks.size(), 2); //Accounts 2 and 7 
        
        Task t1 = tasks[0];
        Task t2 = tasks[1];
        
        if (t1.WhatId == accounts[1].Id){
        	if (t2.WhatId == accounts[6].Id)
        		System.assert(true);
        	else
        		System.assert(false);
        }
        else if (t1.WhatId == accounts[6].Id) {
        	if (t2.WhatId == accounts[1].Id)
        		System.assert(true);
        	else
        		System.assert(false);
        }
        else
        	System.assert(false);
	}

}