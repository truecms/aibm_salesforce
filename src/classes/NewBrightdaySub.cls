/**
* Batchable class for renewing existing ER subs who have access to ER content through brightday 
*/
global with sharing class NewBrightdaySub implements Database.Batchable<Product_Instance__c> {

	public List<Product_Instance__c> instances {get; set;}
	public String renewalProductId {get; set;}
	public String renewalCampaignId {get; set;}

	public NewBrightdaySub(List<Product_Instance__c> instances) {
		this.instances = instances;
		
		//Get the Brightday free renew product from the Custom Setting
		List<AIBM_Settings__c> settings = AIBM_Settings__c.getAll().values();
		if (settings.size() > 0) {
			renewalProductId = String.valueOf(settings[0].Brightday_Renewal_Product__c);
			renewalCampaignId = String.valueOf(settings[0].Brightday_Renewal_Campaign__c);
		}
		else {
			renewalProductId = null;
			renewalCampaignId = null;
		}
	}

	public void autoRenew(){
		
		List<Product_Instance__c> renewalInstances = new List<Product_Instance__c>();
		List<Product_Instance__c> updatedInstances = new List<Product_Instance__c>();
		
		//Initially get the renewal Subscription Product
		Subscription_Product__c renewalSubscription = null;
		if (renewalProductId != null) {
			renewalSubscription = [SELECT Id, Price__c, Subscription_Type__c, Subscription_Stream__c, Duration_units__c, Duration__c 
								   FROM Subscription_Product__c 
								   WHERE Id = :renewalProductId 
								   LIMIT 1];
		}
		else {
			System.debug('DEBUG: no Renewal Subscription Product found - cannot process brightday renewals!');
		}
		
		if (renewalSubscription != null) {
			for (Product_Instance__c instance : this.instances) {
				
				ProductInstanceHelper piHelper = new ProductInstanceHelper(instance);
				
				Id currentActiveSub = instance.Person_Account__r.ER_Current_Active_Subscription__c;
				
				if (currentActiveSub != instance.Id ||
	            		piHelper.getFutureSubscriptions().size() > 0 || // this is for subscriptions that already have a subscription in the future so don't need to renew this one
	            		instance.End_Date__c < date.newInstance(2013, 3, 9) // this when we went live with the new system, anything before that should be expired.
	            ) {
	            	// if this is not the current subscription it means there has been renewal elsewhere
	            	// we expire the subscription so it doesn't show up again.
	            	instance.Status__c = piHelper.EXPIRED;
	            	instance.Auto_Renewal__c = false;
	            	updatedInstances.add(instance);
	        	}
	        	else {
					
					Product_Instance__c renewalProduct = instance.clone(false, true, false, false);
					renewalProduct.Start_Date__c = null;
					renewalProduct.End_Date__c = null;
					renewalProduct.Subscription_Product__c = renewalSubscription.Id;
					renewalProduct.Subscription_Type__c = renewalSubscription.Subscription_Type__c;
					
					piHelper.setInstancePeriod(renewalProduct, renewalSubscription); //@TODO: think about refactoring this, bad code (SOQL loop)
					            
					renewalProduct = this.addRenewalSubscription(renewalProduct, piHelper.PAID);
					renewalInstances.add(renewalProduct);
						
					//Cancel the original product
					instance.Status__c = piHelper.EXPIRED;
					updatedInstances.add(instance);
					
				}
			}
			
			update updatedInstances;
			insert renewalInstances;
		}
	}
	
	public Product_Instance__c addRenewalSubscription(Product_Instance__c renewalProduct, String status) {
        renewalProduct.Status__c = status;
        renewalProduct.Is_Renewal__c = false; //Don't want to autorenew this product
        renewalProduct.Campaign__c = this.renewalCampaignId;
        return renewalProduct;
    }

	global Iterable<Product_Instance__c> start(Database.batchableContext BC){
        List<Product_Instance__c> batchInstances = this.instances;
        return batchInstances;
    }
    
    /**
     *
     */
    global void execute(Database.BatchableContext BC, Product_Instance__c[] instances) {
        
        NewBrightdaySub helper = new NewBrightdaySub(instances);
        	
        helper.autoRenew();
    }
	
    /**
     *
     */
    global void finish(Database.BatchableContext BC){
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob WHERE Id =
            :BC.getJobId()];
            
       
    }

}