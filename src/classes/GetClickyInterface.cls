/**
 * GetClickyInterface
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 *
 * Add the code below to every website that needs tracking
 * It should be added to the login onclick event, using the email from the login input
 * 
  <script type="text/javascript">
    var clicky_custom = {};
    clicky_custom.session = {
      email: 'bob@jones.com'
    };
  </script>
  
  <!-- your hypothetical tracking code, which must go after clicky_custom -->
  <script src="http://static.getclicky.com/js" type="text/javascript"></script>
 *
 * Swap 'bob@jones.com' with 'document.getElementById('login_email_id').value;'
 */
public class GetClickyInterface {
  
  private Integer site_id = 66555345;
  private String sitekey = '3ee932bb74d50894';
  
  private static final String END_POINT = 'http://api.getclicky.com/api/stats/4?type=visitors-list';
  private ApexPages.StandardController controller;
  public static boolean isApexTest = false;
  
  private Map<String, String> pageGoals;
  private List<Goal> pageGoalsObjects = new List<Goal>();
  private Integer pageNumber = 0;
  private Integer pageSize = 0;
  private Integer totalPageNumber = 0;
  
  private Account account;
  private String url = '';
  private String username = '';
  private String email = '';
  private String last_visited = '';
  private String geolocation = '';
  private String visits_last_month = '';
  private Map<String, String> goals = new Map<String, String>();
  private String updated_time = '';
  
  public Account getCurrentAccount() {
    List<Account> accounts = [SELECT Id FROM Account WHERE Id = :controller.getId()];
    if (!accounts.isEmpty()) {
      Account account = [SELECT Id, name, PersonEmail FROM Account WHERE Id = :controller.getId()];
      return account;
    }
    return null;
  }
  
  public Integer getPageNumber() {
    return pageNumber;
  }
  public Integer getPageSize(){
    return pageSize;
  }
  public Boolean getPreviousButtonEnabled(){
    return !(pageNumber > 1);
  }
  public Boolean getNextButtonDisabled(){
    if (goals == null) return true;
    else
    return ((pageNumber * pageSize) >= goals.size());
  }
  public Integer getTotalPageNumber(){
    if (totalPageNumber == 0 && goals !=null){
      totalPageNumber = goals.size() / pageSize;
      Integer mod = goals.size() - (totalPageNumber * pageSize);
      if (mod > 0)
        totalPageNumber++;
    }
    return totalPageNumber;
  }
  
  public PageReference nextBtnClick() {
    BindData(pageNumber + 1);
    return null;
  }

  public PageReference previousBtnClick() {
    BindData(pageNumber - 1);
    return null;
  }
  public PageReference ViewData(){
    totalPageNumber = 0;
    BindData(1);
    return null;
  }
  private void BindData(Integer newPageIndex){
    System.debug(pageNumber);
    System.debug(pageSize);
    System.debug(totalPageNumber);
    try {
      pageGoals = new Map<String, String>();
      Transient Integer counter = 0;
      Transient Integer min = 0;
      Transient Integer max = 0;
      if (newPageIndex > pageNumber){
        min = pageNumber * pageSize;
        max = newPageIndex * pageSize;
      } else {
        max = newPageIndex * pageSize;
        min = max - pageSize;
      }
      Set<String> keys = goals.keySet();
      List<String> lists = new List<String>();
      for (String key : keys) {
        lists.add(key);
      }
      lists.sort();
      for(String k : lists){
        counter++;
        if (counter > min && counter <= max)
          pageGoals.put(k, goals.get(k));
        }
        pageNumber = newPageIndex;
        if (pageGoals == null || pageGoals.size() <= 0)
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'No data available.'));
    } catch(Exception ex) {
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
    }
    System.debug(pageGoals);
  }
  
  /**
   *
   */
  public GetClickyInterface(ApexPages.StandardController controller) {
    this.controller = controller;
    
    pageNumber = 0;
    totalPageNumber = 0;
    pageSize = 5;
    
    if(this.account == null) {
      this.account = this.getCurrentAccount();
    }
    //username = this.account.name;
    email = this.account.PersonEmail;
    
    //buildUrl('username', email, 'this-month', 'all');
    buildUrl('email', email, 'this-month', 'all'); // use the email to find the tracking information
    getStats();
    
    ViewData();
  }
  
  public void getStats() {
    if (!isApexTest) {
      XMLDOM result = this.invoke();
    
      List<XMLDom.Element> items = result.getElementsByTagName('item');
      if (items.size() > 0) {
        if (items.get(0).getValue('time_pretty') != null) { 
          this.last_visited = items.get(0).getValue('time_pretty');
        } else {
          this.last_visited = 'No current data';
        }
        setGoals(items);
        if (items.get(0).getValue('geolocation') != null) { 
          this.geolocation = items.get(0).getValue('geolocation');
        } else {
          this.geolocation = 'No current data';
        }
        if (String.valueOf(items.size()) != null) {
          this.visits_last_month = String.valueOf(items.size());
        } else {
          this.visits_last_month = 'No current data';
        }
      }
      this.updated_time = Datetime.now().format('d/MM/yy h:mm a');
    }
  }
  
  public String getUsername() {
    return username;
  }
  public String getEmail() {
    return email;
  }
  public String getLastVisited() {
    return last_visited;
  }
  public String getGeolocation() {
    return geolocation;
  }
  public String getVisitsLastMonth() {
    return visits_last_month;
  }
  /*
  public Map<String, String> getGoals() {
    return pageGoals;
  }
  */
  public List<Goal> getPageGoalsObjects() {
    List<Goal> goalList = new List<Goal>();
    
    Set<String> keySet = pageGoals.keySet();
    List<String> keyList = new List<String>();
    for (String key : keySet) {
      keyList.add(key);
    }
    keyList.sort();
    // TODO sort desc
    for (String key : keyList) {
      Goal goal = new Goal(key, pageGoals.get(key));
      goalList.add(goal);
    }
    System.debug(goalList);
    return goalList;
  }
  
  /**
   * TODO: if there are more than one goal in a single session, it only shows the last one. Need to fix this
   */
  public void setGoals(List<XMLDom.Element> items) {
    goals = new Map<String, String>();
    for (XMLDom.Element item : items) {
      if (item.getValue('goals') != null) {
        String item_date = item.getValue('time');
        List<XMLDom.Element> item_goals = item.getElementsByTagName('goal');
        for (XMLDom.Element goal : item_goals) {
          Datetime timestamp = Datetime.newInstance(0);
          timestamp = timestamp.addSeconds(integer.valueOf(item_date));
          goals.put(timestamp.format(), goal.getValue('goal'));
        }
      }
    }
  }
  
  public String getUpdatedTime() {
    return updated_time;
  }
  
  public PageReference updateStats() {
    getStats();
    return null;
  }
  
  public PageReference redirectGetClicky() {
    String username = 'go1'; // does this need to be changed to another username
    PageReference page = new PageReference('http://stats.go1.com.au/stats/visitors?site_id=' + site_id + '&custom[username]='+username);
    return page;
  }
  
  /**
   * TODO: use email instead of username
   * email: Related to the account object to identify the tracked user
   * time_frame: today, yesterday, X-days-ago, last-X-days, YYYY-MM, this-month, last-month, this-week, last-week...
   * result_limit: Any number between 1 and 1000 or 'all' for no limit
   */
  public void buildUrl(String type, String value, String time_frame, String result_limit) {
    //email = Encodingutil.urlEncode(email, 'UTF-8');
    time_frame = Encodingutil.urlEncode(time_frame, 'UTF-8');
    result_limit = Encodingutil.urlEncode(result_limit, 'UTF-8');
    this.url = END_POINT + '&site_id=' + site_id + '&sitekey=' + sitekey + '&custom[' + type + ']='+ value +'&date='+ time_frame +'&limit='+ result_limit +'&output=xml';
  }
  
  public virtual HttpRequest createRequest() {
    HttpRequest request = new HttpRequest();
    request.setEndPoint(url);
    request.setMethod('GET');
    return request;
  }
  
  public virtual XMLDom invoke() {
    HttpRequest request = createRequest();
    request.setHeader('Content-Type', 'text/xml; charset=utf-8');
    System.debug('request=' + request.getHeader('Content-Type'));
    System.debug('request=' + request.getBody());

    Http h = new Http();
    HttpResponse response = h.send(request);
    System.debug('response=' + response.toString());
    if (response.getStatusCode() != 200) {
      System.debug(response.getStatusCode());
    }
    System.debug('response=' + response.getBody());
    
    XMLDom xml = new XMLDom();
    xml.parseFromString(response.getBody());
    return xml;
  }
  
  public class Goal {
    public String key { public get; set; }
    public String value { public get; set; }
    
    public Goal(String key, String value) {
      this.key = key;
      this.value = value; 
    }
  }
}