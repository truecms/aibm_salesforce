/**
* Batchable scheduled job that finds all the accounts that are shortly due to expire but do not have Credit Card information
* Author: tailbyr
* Copyright: EurekaReport.com.au
*/
global class NoCreditCardBatch implements Schedulable, Database.Batchable<Product_Instance__c>, Database.Stateful 
{
	public static String lastAccountManager { get; set; }
	
	public TaskManager tm {get; set;}
	
	public String noCreditCardQueue {get; set;}
	public Boolean noCreditCardAllocationRules {get; set;}
	
	//Constructor - get the lastAccountManager given one of these 'No Credit Card' tasks
	public NoCreditCardBatch()
	{
		// Get the Account Manager/Owner of the last No Credit Card task so that we can assign the tasks equally
		List<Task> lastNoCreditCardTask = [SELECT Id, ownerId FROM Task WHERE Subject = :TaskManager.NO_CREDIT_CARD_SUBJECT ORDER BY createdDate desc LIMIT 1];
		if (lastNoCreditCardTask.size()>0)
			lastAccountManager = lastNoCreditCardTask[0].ownerId;
			
		//Get the customer settings allocation rule and queue for this Expired FT automated task creator
		Allocation_Rules__c allocationRules = Allocation_Rules__c.getInstance('Default');
		noCreditCardAllocationRules = allocationRules.Credit_Card_Allocation_Rule__c;
		noCreditCardQueue = allocationRules.Credit_Card_Queue__c;
		
		tm = new TaskManager(noCreditCardQueue); //Instigate the TaskManager and tell it which User Queue to use from Custom Settings
	}
	
	//Schedulable override method: execute - called when the scheduled job is kicked off
	global void execute(SchedulableContext ctx)
	{
		Database.executeBatch(new NoCreditCardBatch(), 1);
	}
	
	//Batchable override method: start - queries the records to iterate over
	global Iterable<Product_Instance__c> start(Database.batchableContext BC)
	{
		//Get the annual subscriptions that will be expiring in 7 days time
		List<Product_Instance__c> piList = [SELECT Id, End_Date__c, Person_Account__c FROM Product_Instance__c WHERE Auto_Renewal__c = true
				  AND End_Date__c = :Date.today().addDays(7)];
		
		//Also get the monthly subs that have a recurring payment due in 7 days time (shouldn't ever be that many)
		List<Product_Instance__c> riList = [SELECT Id, End_Date__c, Person_Account__c FROM Product_Instance__c 
				WHERE Recurring_Instance__r.go1__Next_Attempt__c = :Date.today().addDays(7) 
				AND Recurring_Instance__r.go1__Expired__c != true 
				AND Recurring_Instance__r.go1__Cancelled__c != true];
		
		piList.addAll(riList);
		
		return piList;
	}
	
	global void execute(Database.BatchableContext BC, List<Product_Instance__c> instances)
	{
		ProductInstanceHelper piHelper = new ProductInstanceHelper(instances);
		piHelper.createNoCreditCardTasks(tm, noCreditCardAllocationRules);
	}
	
	//Batchable override method: finish - called when all batches are processed; allocates all fair tasks created in a fair manner across Acc mgrs
	global void finish(Database.BatchableContext BC)
	{
		//Now all the tasks have been created, grab all created tasks and reallocate fairly - CC expiry should ALWAYS be allocated fairly
		List<Task> allNoCCTasksToday = [SELECT Id, ownerId FROM Task 
				where Subject = :TaskManager.NO_CREDIT_CARD_SUBJECT 
				AND createdDate = TODAY
				AND CallDisposition = :TaskManager.ALLOCATION_DISPOSITION];
				
		System.debug('DEBUG: allNOCreditCardTasksToday:' + allNoCCTasksToday.size());		
		
		for (Task t : allNoCCTasksToday) 
		{
			String nextAccountManager = tm.getNextSalesTeamMember(NoCreditCardBatch.lastAccountManager);
			t.OwnerId = nextAccountManager;
			NoCreditCardBatch.lastAccountManager = nextAccountManager;	
		}
		
		update allNoCCTasksToday;
	}
	
	/*
		Method used to create the scheduling of this Batch Job. This is used by the 'TestScheduledJobs' function which enables the scheduled jobs to be 
		setup and removed programmatically (also via API for Jenkins build job)
	*/
	global static void setupSchedule(String jobName, String scheduledTime) { 
		NoCreditCardBatch scheduled_task = new NoCreditCardBatch();
		String payment_id = System.schedule(jobName, scheduledTime, scheduled_task);
	}
}