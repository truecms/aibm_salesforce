public class DeactivateAccount 
{
	private final Account deactivateAcc;
	public Note wtfNote {get; set; }
	
	public Boolean showDone {get; set; }
	
	public DeactivateAccount(ApexPages.StandardController stdController) 
	{
		this.deactivateAcc = (Account)stdController.getRecord();
		this.wtfNote = new Note(ParentId=this.deactivateAcc.Id, Title='Account Deactivated', IsPrivate=false);
		this.showDone = false;
	}
	
	public void submitForm() {
		try {
			insert wtfNote;			
		} catch (Exception e) {
			System.debug(e.getMessage());
		}
		
		//Set all the fields needed to deactivate an account (passing Account so this method can be used externally too, and getting Account back so we can control when its saved)
		Account cancelledAcc = cancelAccount(this.deactivateAcc);
		update cancelledAcc;
		
		//And cancel all of the subscriptions against the account
		cancelSubscriptions(); 		
		
		//Finally set the 'showDone' boolean to true so that the window gets updated
		this.showDone = true;
	}
	
	public static Account cancelAccount(Account cancelAcc)
	{
		//Also update the email and newsletter fields
		Account fullDeactivateAcc = [SELECT PersonEmail, Publication__c, WE_Briefing__c, Webinar_Email__c, //Morning__c, 
									//KGB__c, Afternoon__c, Climate_Spectator_Daily_Update__c, Technology_Spectator_Emails__c, Sponsored_Content__c,  
									PersonHasOptedOutOfEmail, Phone, Merged_Into_Account__c
									FROM Account
									WHERE Id = :cancelAcc.Id LIMIT 1];
																		
		String stopEmail = fullDeactivateAcc.PersonEmail;
		fullDeactivateAcc.PersonEmail = stopEmail + '.DoNotUse';
		
		//We now need to check if this new 'DoNotUse' email address is used already! Maybe in chained merge calls?
		List<sObject> usedCount = Database.query('SELECT Id FROM Account WHERE PersonEmail LIKE \'' + stopEmail + '.DoNotUse' + '%\'');
		if (usedCount.size() > 0)
			fullDeactivateAcc.PersonEmail = stopEmail + '.DoNotUse' + usedCount.size();
		
		fullDeactivateAcc.Publication__c = false;
		fullDeactivateAcc.WE_Briefing__c = false;
		fullDeactivateAcc.Webinar_Email__c = false;
		//fullDeactivateAcc.Morning__c = false;
		//fullDeactivateAcc.KGB__c = false;
		//fullDeactivateAcc.Afternoon__c = false;
		//fullDeactivateAcc.Climate_Spectator_Daily_Update__c = false;
		//fullDeactivateAcc.Technology_Spectator_Emails__c = false;
		//fullDeactivateAcc.Sponsored_Content__c = false;
		
		//Also tick the 'EMail Opt Out' checkbox for a clear indicator that the subscriber should not get any further email
		fullDeactivateAcc.PersonHasOptedOutOfEmail = true;
		
		//And blank out the phone field
		fullDeactivateAcc.Phone = '-';
		
		return fullDeactivateAcc;
	}
	
	private void cancelSubscriptions()
	{
		List<Product_Instance__c> subs = [SELECT End_Date__c, Id, Person_Account__c, Status__c 
			FROM Product_Instance__c 
			WHERE Person_Account__c = :deactivateAcc.Id];
			
		for (Product_Instance__c sub : subs)
		{
			sub.End_Date__c = Date.today().addDays(-1);
			sub.Status__c = 'Cancelled';
		}
		
		update subs;
		
		//Also check for any Recurring Instances and set their remaining attempts to 0 and check the 'Cancelled' box
		List<go1__Recurring_Instance__c> recurs = [SELECT Id, go1__Remaining_Attempts__c, go1__Reattempts__c, go1__Cancelled__c
			FROM go1__Recurring_Instance__c 
			WHERE go1__Account__c = :deactivateAcc.Id];
			
		for (go1__Recurring_Instance__c r : recurs)
		{
			r.go1__Remaining_Attempts__c = 0;
			r.go1__Reattempts__c = 0;
			r.go1__Cancelled__c = true;
		}
		
		update recurs;
	}
}