/**
 * Testing the LateRenewalTasks class
 * Author: tailbyr
 * Copyright: eurekareport.com.au
 */
@isTest
public with sharing class TestLateRenewalTasks 
{
    /** Utility Methods
    */
    static Subscription_Product__c createSubs()
    {
        //Create subscription stream
        Subscription_Stream__c stream = new Subscription_Stream__c(Name='Eureka Report');
        insert stream;
        Subscription_Product__c product = new Subscription_Product__c();
        product.Subscription_Stream__c = stream.Id;
        product.Duration__c = 1;
        product.Duration_units__c = 'month';
        product.Price__c = 0;
        insert product;
        return product;
    }
    
    static RecordType getRecordType()
    {
        // Find any person account record type
        RecordType recordType = [ select Id, Name, DeveloperName from RecordType where SObjectType = 'Account' and IsPersonType = true limit 1 ];
        return recordType;
    }
    
    static List<Account> createAccounts(Integer numAccounts) 
    {
        //Set up the product to be subscribed to
        Subscription_Product__c sub = createSubs();
        
        //Create the accounts for test
        List<Account> accs = new List<Account>(); 
        List<Id> accIds = new List<Id>();
        
        for (integer i = 0; i < numAccounts; i++) {
            Account account = new Account(FirstName='Test' +i, LastName='Tester' +i, PersonEmail='test'+i+'@test.com.test');
            account.RecordTypeId = getRecordType().Id;
            account.AccountNumber = 'UnitTestAccount';
            accs.add(account);
        }
        
        insert accs;
        
        //Now create the expired FTs for these Accounts
        createExpiredSubs(accs, sub);
        
        return accs;
    }
    
    static void createExpiredSubs(List<Account> accs, Subscription_Product__c sub)
    {
        //For each Account, create an expired paid sub that finished 10 days ago (for odd nums create a yearly, for even a monthly to check both work!)
        List<Product_Instance__c> insts = new List<Product_Instance__c>();
        for (Integer i = 0; i < accs.size(); i++)
        {
            Product_Instance__c pi = new Product_Instance__c();
            pi.Person_Account__c = accs[i].Id;
            pi.Subscription_Product__c = sub.Id;
            pi.Start_Date__c = Date.today().addDays(-128);
            pi.End_Date__c = Date.today().addDays(-1);
            
            if (Math.Mod(i, 2) == 0) {
                pi.Subscription_Type__c = 'y';
                pi.Auto_Renewal__c = false;
            }
            else {
                pi.Subscription_Type__c = 'm';
            }
                
            insts.add(pi);
        }
        insert insts;
        
        //Update the dates so they are correctly expired!
        for (Product_Instance__c pi : insts)
        {
            pi.Start_Date__c = Date.today().addDays(-128);
            pi.End_Date__c = Date.today().addDays(-1);
        }
        update insts;
    }
    
    static void checkAllocationOfTasks(List<Task> tasks, Integer noOfAccounts)
    {
        List<GroupMember> aMembers = [SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = 'AccountManagers' ORDER BY UserOrGroupId];
        
        //Ensure that these tasks were allocated equally
        if (tasks!=null && tasks.size()>0){
            Map<Id, Integer> tasksPerManager = new Map<Id, Integer>();
            for (Task t : tasks) {
                    
                //Count tasks per Account Manager
                if (tasksPerManager.containsKey(t.ownerId)){
                    Integer count = tasksPerManager.get(t.ownerId);
                    count++;
                    tasksPerManager.put(t.ownerId, count);
                }
                else {
                    tasksPerManager.put(t.ownerId, 1);
                }
            }
            
            //Work out how many tasks each Account Manager must have (not counting remainder)
            Integer noOfTasks = Math.floor(noOfAccounts / aMembers.size()).intValue();
            List<Integer> counts = tasksPerManager.values();
            for (Integer val : counts){
                /*if (val == noOfTasks || val == noOfTasks+1) //+1 to account for the remainder where noOfTasks%noOfAccountManagers != 0
                    System.assert(true);
                else
                    System.assert(false);*/
            }
        }
        else
            System.assert(false);
    }
    /*********************************************************/
    
    /**
    * Test Methods
    */
    static TestMethod void testLateRenewalTaskCreated()
    {
        //Set up the custom settings for queue to use and whether to utilise assignment rules or not
        TestProductInstanceHelper.createAllocationSettings();
        
        //Create just 1 account
        List<Account> accountList = createAccounts(1);
        Account account = accountList[0];
        
        Test.startTest();
        //Call the ExpiredFreeTrial batch to initiate the process
        Database.executeBatch(new LateRenewalTasks(), 1);
        Test.stopTest();
        
        //Check we have a task
        List<Task> lateRenewalTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
                where Subject = :TaskManager.LATE_RENEWAL_SUBJECT 
                AND createdDate = TODAY
                ORDER BY createdDate desc];
        System.assertEquals(lateRenewalTasks.size(), 1);
        System.assertEquals(lateRenewalTasks[0].WhatId, account.Id);
        
        //Check we don't have any other tasks!
        List<Task> anyOtherTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
                where Subject != :TaskManager.LATE_RENEWAL_SUBJECT 
                AND createdDate = TODAY
                ORDER BY createdDate desc];
        System.assertEquals(anyOtherTasks.size(), 0);
    }
    
    //Create a number of Late Renewal scenarios and ensure they are all shared equally across the Account Managers
    static TestMethod void testLateRenewalTasksCreatedFairly()
    {
        //Set up the custom settings for queue to use and whether to utilise assignment rules or not
        TestProductInstanceHelper.createAllocationSettings();
        
        Integer noOfAccs = 6;
        
        //Create noOfAccs test accounts
        List<Account> accountList = createAccounts(noOfAccs);
        
        Test.startTest();
        Database.executeBatch(new LateRenewalTasks(), noOfAccs);
        Test.stopTest();
        
        //Initially check the right number of tasks were created
        List<Task> lateRenewalTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
                where Subject = :TaskManager.LATE_RENEWAL_SUBJECT 
                AND createdDate = TODAY
                ORDER BY createdDate desc];
        System.assertEquals(lateRenewalTasks.size(), noOfAccs);
        
        //Now check that each Sales User has the same number of these tasks
        checkAllocationOfTasks(lateRenewalTasks, noOfAccs);
    }
    
    //Test that an Account owned by a User in the AccountManager group will have the tasks always allocated to them
    static TestMethod void testLateRenewalWithAccOwner()
    {
        //Set up the custom settings for queue to use and whether to utilise assignment rules or not
        TestProductInstanceHelper.createAllocationSettings();
        
        //Pick the first Account Manager for setting all the task ownership
        GroupMember aMember = [SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = 'AccountManagers' ORDER BY UserOrGroupId LIMIT 1];
        Integer noOfAccs = 6;
        
        //Create noOfAccs test accounts
        List<Account> accountList = createAccounts(noOfAccs);
        
        for (Account acc : accountList)
        {
            acc.OwnerId = aMember.UserOrGroupId;
        }
        update accountList;
        
        Test.startTest();
        Database.executeBatch(new LateRenewalTasks(), noOfAccs);
        Test.stopTest();
        
        //Check that the right number of tasks were created
        List<Task> lateRenewalTasks = [SELECT Id, OwnerId FROM Task 
                                    WHERE Subject = :TaskManager.LATE_RENEWAL_SUBJECT
                                    AND createdDate = TODAY];
        System.assertEquals(lateRenewalTasks.size(), noOfAccs);
        
        //Check that all those tasks were assigned to our aMember retrievd earlier
        for (Task t : lateRenewalTasks)
        {
           //System.assertEquals(t.OwnerId, aMember.UserOrGroupId);
        }
    }
}