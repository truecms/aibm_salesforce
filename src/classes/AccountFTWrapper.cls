public class AccountFTWrapper 
{
	public Account acc {get; set;}
	public String fromSubType {get; set;}
	public Boolean isSelected {get; set;}
	
	public Integer associatedTasks {get; set; }
	public Task latestTask {get; set; }
	
	public AccountFTWrapper(Account a, String fromSubType)
	{
		this.acc = a;
		this.fromSubType = fromSubType;
	}
	
	public AccountFTWrapper(Account a, String fromSubType, Integer associatedTasks)
	{
		this(a, fromSubType);
		this.associatedTasks = associatedTasks;
	}
	
	public AccountFTWrapper(Account a, String fromSubType, Integer associatedTasks, Task latestTask)
	{
		this(a, fromSubType, associatedTasks);
		this.latestTask = latestTask;
	}
}