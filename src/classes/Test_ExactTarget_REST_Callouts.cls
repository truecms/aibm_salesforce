@IsTest
global class Test_ExactTarget_REST_Callouts implements HTTPCalloutMock {
	
	String messages {get; set;}
	Boolean hasErrors {get; set;}
	String httpStatus {get; set;}
	Integer httpStatusCode {get; set;}
	
	String accessToken {get; set;}
	Integer expiresIn {get; set;}
	
	//Our Default constructor - used for the send email functionality
	Test_ExactTarget_REST_Callouts(String msg, Boolean hasErrors, String httpStatus, Integer code){
		this.messages = msg;
		this.hasErrors = hasErrors;
		this.httpStatus = httpStatus;
		this.httpStatusCode = code;
	}
	
	//Additional constructor - used for testing the ET authentication mechanism
	Test_ExactTarget_REST_Callouts(String accessToken, Integer expiresIn, String httpStatus, Integer code){
		this(null, false, httpStatus, code);
		this.accessToken = accessToken;
		this.expiresIn = expiresIn;
	}
	
	global HttpResponse respond(HttpRequest req) {
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
    	res.setStatus(this.httpStatus);
    	res.setStatusCode(this.httpStatusCode);
    	
    	//The final response object which will be serialized and sent back as the repsonse body
    	Map<String, Object> httpResponse = new Map<String, Object>();
    	
    	if (accessToken != null) {
    		//Testing the authentication functionality
    		httpResponse.put('accessToken', this.accessToken);
    		httpResponse.put('expiresIn', this.expiresIn);
    	}
    	else {
    		//Testing the Email send functionality
    		Map<String, Object> responses = new Map<String, Object>();
    		responses.put('hasErrors', this.hasErrors);
			responses.put('messages', this.messages);
			responses.put('recipientSendId', '1234');
    	
    		List<Object> respObj = new List<Object>();
    		respObj.add(responses);
    		
    		httpResponse.put('requestId', '10001');
    		httpResponse.put('responses', respObj);
    	}
    	
    	res.setBody(JSON.serialize(httpResponse));
    	
    	return res;
	}
	
	public static Account createAccountOnly(Integer i){
		Account account = new Account(FirstName='Test'+i, LastName='Tester'+i, PersonEmail='test'+i+'@test.com.test');
  		account.Sub_Status__c = '4';  // paid subscription
  		insert account;
    	
        return account;
	}
	
	public static void populateCustomSettings(){
		//Populate the Org Defaults for the Custom Setting required
		ExactTarget_Variables__c testVars = ExactTarget_Variables__c.getOrgDefaults();
		if (testVars == null) testVars = new ExactTarget_Variables__c();
		testVars.Auth_Endpoint__c = 'testauth.exacttarget.com';
		testVars.ClientId__c = 'testId';
		testVars.ClientSecret__c = 'testSecret';
		upsert testVars;
	}
	
	public static TestMethod void testSuccessfulETCallout() {
		Test.startTest();
		
		Test.setMock(HTTPCalloutMock.class, new Test_ExactTarget_REST_Callouts('(Queued)', false, 'OK', 200));
		Account acc = createAccountOnly(1);
		ExactTarget_REST_Callouts.sendNotification(acc.FirstName, acc.LastName, acc.PersonEmail, 'ER_'+acc.Long_Id__c, 'Welcome Video');
				
		Test.stopTest();
		
		Map<String, Object> responseMap = ExactTarget_REST_Callouts.response;
		
		List<Object> httpObj = (List<Object>)responseMap.get('responses');
		Map<String, Object> httpMap = (Map<String, Object>)httpObj.get(0);
		System.assertEquals(httpMap.get('hasErrors'), false);
		System.assertEquals(httpMap.get('messages'), '(Queued)');
	}
	
	public static TestMethod void testFailedETCallout() {
		Test.startTest();
		
		Test.setMock(HTTPCalloutMock.class, new Test_ExactTarget_REST_Callouts('(InvalidOrMissingToAddress)', true, 'OK', 200));
		Account acc = createAccountOnly(1);
		ExactTarget_REST_Callouts.sendNotification(acc.FirstName, acc.LastName, acc.PersonEmail, 'ER_'+acc.Long_Id__c, 'Welcome Video');
				
		Test.stopTest();
		
		Map<String, Object> responseMap = ExactTarget_REST_Callouts.response;
		
		List<Object> httpObj = (List<Object>)responseMap.get('responses');
		Map<String, Object> httpMap = (Map<String, Object>)httpObj.get(0);
		System.assertEquals(httpMap.get('hasErrors'), true);
		System.assertEquals(httpMap.get('messages'), '(InvalidOrMissingToAddress)');
	}
	
	public static TestMethod void testAuthenticateETCallout() {
		
		populateCustomSettings();
		
		Test.startTest();
		Test.setMock(HTTPCalloutMock.class, new Test_ExactTarget_REST_Callouts('access1234', 3600, 'OK', 200));
		String token = ExactTarget_REST_Callouts.authenticate();
		Test.stopTest();
		
		System.assertEquals(token, 'access1234');
	}
	
	public static TestMethod void testExpiredSession() {
		populateCustomSettings();
		
		Test.startTest();
		
		Test.setMock(HTTPCalloutMock.class, new Test_ExactTarget_REST_Callouts('Not Authorized', true, 'Unauthorized', 401));
		Account acc = createAccountOnly(1);
		ExactTarget_REST_Callouts.sendNotification(acc.FirstName, acc.LastName, acc.PersonEmail, 'ER_'+acc.Long_Id__c, 'Welcome Video');
				
		Test.stopTest();
		
		Map<String, Object> responseMap = ExactTarget_REST_Callouts.response;
		
		List<Object> httpObj = (List<Object>)responseMap.get('responses');
		Map<String, Object> httpMap = (Map<String, Object>)httpObj.get(0);
		System.assertEquals(httpMap.get('hasErrors'), true);
		System.assertEquals(httpMap.get('messages'), 'Not Authorized');
	}
	
	static TestMethod void InsertValidatedAccountSendMail(){
  		Test.startTest();
  		Test.setMock(HTTPCalloutMock.class, new Test_ExactTarget_REST_Callouts('(Queued)', false, 'OK', 200));
  		Account validAccount = new Account(FirstName='Test'+1, LastName='Tester'+1, PersonEmail='test'+1+'@test.com.test');
  		validAccount.BD_Is_Verified__c = true;
  		validAccount.BD_Send_Welcome__c = true;
  		insert validAccount; //Inserts never trigger the email - should still not set the Received Welcome flag
  		
  		Account invalidAccount = new Account(FirstName='Test'+2, LastName='Tester'+2, PersonEmail='test'+2+'@test.com.test');
  		invalidAccount.BD_Is_Verified__c = false;
  		invalidAccount.BD_Send_Welcome__c = true;
  		insert invalidAccount;
  		Test.stopTest();
  		
  		Account validTest = [SELECT BD_Received_Welcome__c FROM Account WHERE LastName = 'Tester1' LIMIT 1];
  		Account invalidTest = [SELECT BD_Received_Welcome__c FROM Account WHERE LastName = 'Tester2' LIMIT 1];
  		
  		System.assertEquals(validTest.BD_Received_Welcome__c, false);
  		System.assertEquals(invalidTest.BD_Received_Welcome__c, false);
  	}
  	
  	static TestMethod void UpdateValidatedAccountSendEmail(){
  		Account validAccount = new Account(FirstName='Test'+1, LastName='Tester'+1, PersonEmail='test'+1+'@test.com.test');
  		validAccount.BD_Is_Verified__c = false;
  		validAccount.BD_Send_Welcome__c = false;
  		insert validAccount;
  		
  		Account invalidAccount = new Account(FirstName='Test'+2, LastName='Tester'+2, PersonEmail='test'+2+'@test.com.test');
  		invalidAccount.BD_Is_Verified__c = false;
  		invalidAccount.BD_Send_Welcome__c = false;
  		insert invalidAccount;
  		
  		//Start as validated and then perform the update to set isVerified from True to False - no email sent
  		Account validToInvalidAccount = new Account(FirstName='Test'+4, LastName='Tester'+4, PersonEmail='test'+4+'@test.com.test');
  		validToInvalidAccount.BD_Is_Verified__c = true;
  		validToInvalidAccount.BD_Send_Welcome__c = false;
  		insert validToInvalidAccount;
  		
  		//Start as verified and Received Welcome = true, set Received Welcome = false - no email sent
  		Account receivedToUnreceivedAccount = new Account(FirstName='Test'+5, LastName='Tester'+5, PersonEmail='test'+5+'@test.com.test');
  		receivedToUnreceivedAccount.BD_Is_Verified__c = true;
  		receivedToUnreceivedAccount.BD_Send_Welcome__c = true;
  		insert receivedToUnreceivedAccount;
  		
  		Test.startTest();
  		Test.setMock(HTTPCalloutMock.class, new Test_ExactTarget_REST_Callouts('(Queued)', false, 'OK', 200));
  		
  		//Starts as validated AND the received welcome flag has already been set but then gets unset - sends email
  		Account prevalidAccount = new Account(FirstName='Test'+3, LastName='Tester'+3, PersonEmail='test'+3+'@test.com.test');
  		prevalidAccount.BD_Is_Verified__c = true;
  		prevalidAccount.BD_Received_Welcome__c = true;
  		prevalidAccount.BD_Send_Welcome__c = false;
  		insert prevalidAccount;
  		
  		validAccount.BD_Is_Verified__c = true;
  		validAccount.BD_Send_Welcome__c = true;
  		update validAccount;
  		
  		invalidAccount.BD_Is_Verified__c = false;
  		invalidAccount.BD_Send_Welcome__c = true;
  		update invalidAccount;
  		
  		prevalidAccount.BD_Is_Verified__c = true;
  		prevalidAccount.BD_Send_Welcome__c = true;
  		prevalidAccount.BD_Received_Welcome__c = false;
  		update prevalidAccount;
  		
  		validToInvalidAccount.BD_Is_Verified__c = false;
  		validToInvalidAccount.BD_Send_Welcome__c = true;
  		update validToInvalidAccount;
  		
  		receivedToUnreceivedAccount.BD_Is_Verified__c = true;
  		receivedToUnreceivedAccount.BD_Send_Welcome__c = false;
  		update receivedToUnreceivedAccount;
  		
  		Test.stopTest();
  		
  		List<Account> accounts = [SELECT Id, LastName, BD_Received_Welcome__c FROM Account];
  		for (Account a : accounts) {
  			if (a.LastName == 'Tester1') {
  				System.assertEquals(a.BD_Received_Welcome__c, true);
  			}
  			if (a.LastName == 'Tester2') {
  				System.assertEquals(a.BD_Received_Welcome__c, false);
  			}
  			if (a.LastName == 'Tester3') {
  				System.assertEquals(a.BD_Received_Welcome__c, true);
  			}
  			if (a.LastName == 'Tester4') {
  				System.assertEquals(a.BD_Received_Welcome__c, false);
  			}
  			if (a.LastName == 'Tester5') {
  				System.assertEquals(a.BD_Received_Welcome__c, false);
  			}
  		}
  	}
}