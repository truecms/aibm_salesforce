@IsTest
public with sharing class TestCreateSMSActivityOnIncomingTrigger 
{
	static TestMethod void testTaskInsertedForAccountManager()
	{
		Account con = new Account();
        con.LastName = 'Test Contact 1';
        con.PersonEmail = 'test@test.com.test';
        con.PersonMobilePhone = '61434838377';
        con.OwnerId = TaskManager.managers[0];
        insert con;
        
        Test.setFixedSearchResults(new Id[]{con.Id});
        
        smagicinteract__Incoming_SMS__c incomingSMS = new smagicinteract__Incoming_SMS__c();
    	incomingSMS.smagicinteract__external_field__c = 'test';
    	incomingSMS.smagicinteract__Inbound_Number__c = '999888777';
    	incomingSMS.smagicinteract__Mobile_Number__c = '61434838377';
    	incomingSMS.smagicinteract__SMS_Text__c = 'Task SMS Test 1';
    	insert incomingSMS;

		Account insAcc = [SELECT Id, OwnerId, PersonContactId FROM Account WHERE Id = :con.Id LIMIT 1];

		//Initially check that the SMS was created against the correct account
    	List<smagicinteract__Incoming_SMS__c> incomingList = [SELECT Id, Account__c, smagicinteract__Mobile_Number__c FROM smagicinteract__Incoming_SMS__c WHERE smagicinteract__Mobile_Number__c =: incomingSMS.smagicinteract__Mobile_Number__c];
    	System.assertEquals(incomingList[0].Account__c,insAcc.Id);
    	
    	//Test that a Task has been created from the SMS
    	List<Task> createdTask = [SELECT Id, OwnerId FROM Task WHERE WhoId = :insAcc.PersonContactId];
    	System.assertEquals(createdTask.size(),1);
    	System.assertEquals(createdTask[0].OwnerId, con.OwnerId);
	}
	
	static TestMethod void testTaskInsertedForNonAccountManager()
	{
		User user = [SELECT Id FROM User WHERE Id NOT IN :TaskManager.managers LIMIT 1];
		
		Account con = new Account();
        con.LastName = 'Test Contact 2';
        con.PersonEmail = 'test@test.com.test';
        con.PersonMobilePhone = '61434838377';
        con.OwnerId = user.id;
        insert con;
    
    	Test.setFixedSearchResults(new Id[]{con.Id});
        
        smagicinteract__Incoming_SMS__c incomingSMS = new smagicinteract__Incoming_SMS__c();
    	incomingSMS.smagicinteract__external_field__c = 'test';
    	incomingSMS.smagicinteract__Inbound_Number__c = '999888777';
    	incomingSMS.smagicinteract__Mobile_Number__c = '61434838377';
    	incomingSMS.smagicinteract__SMS_Text__c = 'Task SMS Test 2';
    	insert incomingSMS;

		Account insAcc = [SELECT Id, OwnerId, PersonContactId FROM Account WHERE Id = :con.Id LIMIT 1];

		//Initially check that the SMS was created against the correct account
    	List<smagicinteract__Incoming_SMS__c> incomingList = [SELECT Id, Account__c, smagicinteract__Mobile_Number__c FROM smagicinteract__Incoming_SMS__c WHERE smagicinteract__Mobile_Number__c =: incomingSMS.smagicinteract__Mobile_Number__c];
    	System.assertEquals(incomingList[0].Account__c,insAcc.Id);
    	
    	//Test that a Task has been created from the SMS
    	List<Task> createdTask = [SELECT Id, OwnerId FROM Task WHERE WhoId = :insAcc.PersonContactId];
    	System.assertEquals(createdTask.size(),1);
    	System.assert(TaskManager.managerMap.containsKey(createdTask[0].ownerId));
    	
    	//Now create another and check that it is assigned fairly i.e. not to the same Acc Mgr
    	smagicinteract__Incoming_SMS__c secondIncomingSMS = new smagicinteract__Incoming_SMS__c();
    	secondIncomingSMS.smagicinteract__external_field__c = 'test2';
    	secondIncomingSMS.smagicinteract__Inbound_Number__c = '999888777';
    	secondIncomingSMS.smagicinteract__Mobile_Number__c = '61434838377';
    	secondIncomingSMS.smagicinteract__SMS_Text__c = 'Task SMS Test 3';
    	insert secondIncomingSMS;
    	
    	List<smagicinteract__Incoming_SMS__c> secondIncomingList = [SELECT Id, Account__c, smagicinteract__Mobile_Number__c FROM smagicinteract__Incoming_SMS__c WHERE smagicinteract__external_field__c =: secondIncomingSMS.smagicinteract__external_field__c];
    	System.assertEquals(secondIncomingList[0].Account__c,insAcc.Id);
    	
    	//Test that a Task has been created from the SMS
    	List<Task> secondCreatedTask = [SELECT Id, OwnerId FROM Task WHERE WhoId = :insAcc.PersonContactId];
    	System.assertEquals(secondCreatedTask.size(),2);
    	System.assert(TaskManager.managerMap.containsKey(secondCreatedTask[0].ownerId)); //Effectively same assertion as above 
    	System.assert(TaskManager.managerMap.containsKey(secondCreatedTask[1].ownerId));
    	
    	if (TaskManager.managers.size()>1) {
    		System.assertNotEquals(secondCreatedTask[1].ownerId, secondCreatedTask[0].OwnerId); //Ensure the two created tasks have different Assignees
    	}
	}
	
	static TestMethod void testMultipleTextsReceived()
	{
		User user = [SELECT Id FROM User WHERE Id NOT IN :TaskManager.managers LIMIT 1];
		
		Account con = new Account();
        con.LastName = 'Test Contact 3';
        con.PersonEmail = 'test@test.com.test';
        con.PersonMobilePhone = '61434838377';
        con.OwnerId = user.id;
        insert con;
        
        Test.setFixedSearchResults(new Id[]{con.Id});
        
        Integer numSMSMessages = 10;
        
        List<smagicinteract__Incoming_SMS__c> smsInserts = new List<smagicinteract__Incoming_SMS__c>();
        for (integer i = 0; i < numSMSMessages; i++)
        {
        	smagicinteract__Incoming_SMS__c incomingSMS = new smagicinteract__Incoming_SMS__c();
    		incomingSMS.smagicinteract__external_field__c = 'test'+i;
    		incomingSMS.smagicinteract__Inbound_Number__c = '999888777'+i;
    		incomingSMS.smagicinteract__Mobile_Number__c = '61434838377';
    		incomingSMS.smagicinteract__SMS_Text__c = 'Task SMS Test '+i;
    		smsInserts.add(incomingSMS);
        }
        insert smsInserts;
        
     	//Check we have the right number of SMS messages attached to the account, the right number of Tasks, and the correct Acc Mgr allocation
     	Account insAcc = [SELECT Id, OwnerId, PersonContactId FROM Account WHERE Id = :con.Id LIMIT 1];
     		   
     	List<smagicinteract__Incoming_SMS__c> incomingList = [SELECT Id, Account__c, smagicinteract__Mobile_Number__c FROM smagicinteract__Incoming_SMS__c WHERE smagicinteract__Mobile_Number__c =: con.PersonMobilePhone];
    	System.assertEquals(incomingList.size(), numSMSMessages);
    	for (smagicinteract__Incoming_SMS__c sms : incomingList)
    	{
    		System.assertEquals(sms.Account__c,insAcc.Id); //Correct num of SMSs on the Account
    	}
    	
    	List<Task> createdTasks = [SELECT Id, OwnerId FROM Task WHERE WhoId = :insAcc.PersonContactId];
    	System.assertEquals(createdTasks.size(), numSMSMessages); //Correct num of created tasks
    	
    	Map<Id, Integer> taskAllocation = new Map<Id, Integer>();
    	for (Task t : createdTasks)
    	{
    		if (taskAllocation.containsKey(t.OwnerId))
    			taskAllocation.put(t.OwnerId, taskAllocation.get(t.OwnerId)+1);
    		else
    			taskAllocation.put(t.OwnerId, 1);
    	}
    	List<Integer> taskVals = taskAllocation.values();
    	
    	Integer noOfTasks = Math.floor(numSMSMessages / TaskManager.managers.size()).intValue();
    	for (Integer t : taskVals)
    	{
    		if (t == noOfTasks || t == noOfTasks+1)
    			System.assert(true); //Correct fair allocation of tasks 
    		else
    			System.assert(false); //Tasks not allocated fairly
    	}
	}
	
	static TestMethod void testAutomatedOptOutMessage()
	{
		smagicinteract__Optout_Settings__c optOut = new smagicinteract__Optout_Settings__c(smagicinteract__Optout_Field__c='smsoptout__pc', smagicinteract__Keyword__c='STOP,OUT');
		insert optOut;
		
		Account con = new Account();
        con.LastName = 'Test Contact 1';
        con.PersonEmail = 'test@test.com.test';
        con.PersonMobilePhone = '61434838377';
        con.OwnerId = TaskManager.managers[0];
        con.smagicinteract__SMSOptOut__pc = false;
        insert con;
        
        Test.setFixedSearchResults(new Id[]{con.Id});
        
        smagicinteract__Incoming_SMS__c incomingSMS = new smagicinteract__Incoming_SMS__c();
    	incomingSMS.smagicinteract__external_field__c = 'test';
    	incomingSMS.smagicinteract__Inbound_Number__c = '999888777';
    	incomingSMS.smagicinteract__Mobile_Number__c = '61434838377';
    	incomingSMS.smagicinteract__SMS_Text__c = 'Task SMS Test 4: stop sending messages';
    	insert incomingSMS;

		//Initially check that the SMS was created against the correct account
    	List<smagicinteract__Incoming_SMS__c> incomingList = [SELECT Id, Account__c, smagicinteract__Mobile_Number__c FROM smagicinteract__Incoming_SMS__c WHERE smagicinteract__Mobile_Number__c =: incomingSMS.smagicinteract__Mobile_Number__c];
    	System.assertEquals(incomingList[0].Account__c,con.Id);
    	
    	Account insAcc = [SELECT Id, OwnerId, PersonContactId, smagicinteract__SMSOptOut__pc FROM Account WHERE Id = :con.Id LIMIT 1];
    	
    	//Check that this account is now marked as being opted out of SMS communications
    	System.assertEquals(insAcc.smagicinteract__SMSOptOut__pc, true);
	}
}