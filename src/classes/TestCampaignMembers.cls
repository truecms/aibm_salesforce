/**
 * TestCampaignMembers 
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
@isTest
private class TestCampaignMembers {

	/**
	 * Check that the second campaign member is not stored since there is already an account with the same email
	 *
	 * DEPRECATED - moved to a TestCampaignOffersAndMembers
	 */
	/*
    static testMethod void onlyNewCampaignMembers() {
    	Account account = new Account(LastName='Test',PersonEmail='test@lead.com');
    	insert account;
    	
    	Campaign campaign = new Campaign(Name='Test Campaign',Only_New_Subscribers__c=true);
    	insert campaign;
    	
    	Contact contact = new Contact(FirstName='Contact',LastName='Test',Email='test@contact.com');
    	insert contact;
    	CampaignMember cm = new CampaignMember(CampaignId=campaign.Id, ContactId=contact.Id);
    	insert cm;
    	
    	Lead lead = new Lead(FirstName='Lead',LastName='Test',Email='test@lead.com');
    	insert lead;
    	CampaignMember cm1 = new CampaignMember(CampaignId=campaign.Id, LeadId=lead.Id);
    	insert cm1;
    	
    	campaign = [SELECT Id, Excluded_Members__c FROM Campaign WHERE Id = :campaign.Id];
    	System.assertEquals('test@lead.com', campaign.Excluded_Members__c);
    }
    */
    
    /**
     * Check that the campaign member is still added to the campaign regardless of the already same account
     *
	 * DEPRECATED - moved to a TestCampaignOffersAndMembers
     */
    /*
    static testMethod void onlyNewCampaignMembersTest1() {
    	Campaign campaign = new Campaign(Name='Test Campaign');
    	insert campaign;
    	
    	Account account = new Account(FirstName='Test',LastName='Tester1',PersonEmail='test@test1.com');
    	insert account;
    	Lead lead = new Lead(FirstName='Test',LastName='Tester1',Email='test@test1.com');
    	insert lead;	
    	CampaignMember cm = new CampaignMember(CampaignId=campaign.Id, LeadId=lead.Id);
    	insert cm;
    	
    	campaign = [SELECT Id, Excluded_Members__c FROM Campaign WHERE Id = :campaign.Id];
    	System.assertEquals(null, campaign.Excluded_Members__c);
    }
    */
}