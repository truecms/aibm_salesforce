/**
 * UnitTest: 
 *
 * Tests the functionality of the trg_aiu_Accounts trigger 
 *
 * Author: tailbyr
 * Copyright: EurekaReport.com.au
 */
@IsTest
public without sharing class Test_aiu_AccountTrigger {

    static Account createAccount(String eoRole) {
        return createAccount(eoRole, 1);
    }
    
    static Account createAccount(String eoRole, Integer i) {
        Account a = new Account();
        a.FirstName = 'test'+i;
        a.LastName = 'tester'+i;
        a.PersonEmail = 'test' + i + '@testbucket.net';
        a.Phone = '12345678';
        if (eoRole != null && eoRole != '')
            a.BD_Current_Role__c = eoRole;
        
        return a;
    }

    static List<User_Role_History__c> getRoleHistory() {
        return [SELECT Id, Name, Account__c, Changed_By__c, Previous_Role__c, Manual_Change__c
                                                FROM User_Role_History__c];
    }


    static TestMethod void createAccountNoRole() {
        Account testAcc = createAccount('');
        
        Test.startTest();
        insert testAcc;
        Test.stopTest();
        
        List<User_Role_History__c> hList = getRoleHistory();
        System.assertEquals(hList.size(), 0);
    }
    
    static TestMethod void createAccountRole() {
        User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
        
        Account testAcc = createAccount('Client');
        
        //Need to run the creates and updates for certain roles as the Drupal user to avoid getting the validation error
        System.runAs(drupalUser){
            Test.startTest();
            insert testAcc;
            Test.stopTest();
        }
        
        List<User_Role_History__c> hList = getRoleHistory();
        System.assertEquals(hList.size(), 1);
        System.assertEquals(hList[0].Previous_Role__c, null);
        System.assertEquals(hList[0].Name, 'Client');
        System.assertEquals(hList[0].Account__c, testAcc.Id);
    }
    
    static TestMethod void updateAccountNoRole() {
        Account testAcc = createAccount('');
        insert testAcc;
        
        Test.startTest();
        testAcc.Phone = '999888777666';
        update testAcc;
        Test.stopTest();
        
        List<User_Role_History__c> hList = getRoleHistory();
        System.assertEquals(hList.size(), 0);
    }
    
    static TestMethod void updateAccountAddRole() {
        Account testAcc = createAccount('');
        insert testAcc;
        
        User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
        
        System.runAs(drupalUser) {
            Test.startTest();
            testAcc.BD_Current_Role__c = 'Client';
            update testAcc;
            Test.stopTest();
        }
        
        List<User_Role_History__c> hList = getRoleHistory();
        System.assertEquals(hList.size(), 1);
        System.assertEquals(hList[0].Name, 'Client');
        System.assertEquals(hList[0].Previous_Role__c, null);
    }
    
    static TestMethod void updateAccountNoRoleChange() {
        User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
        
        Account testAcc = null;
        System.runAs(drupalUser) {
            testAcc = createAccount('Member');
            insert testAcc;
                
            Test.startTest();
            testAcc.Phone = '123456123';
            update testAcc;
            Test.stopTest();
        }
        
        List<User_Role_History__c> hList = getRoleHistory();
        System.assertEquals(hList.size(), 1);
        System.assertEquals(hList[0].Previous_Role__c, null);
        System.assertEquals(hList[0].Name, 'Member');
        System.assertEquals(hList[0].Account__c, testAcc.Id);
    }
    
    static TestMethod void updateAccountRoleChange() {
        User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
        
        Account testAcc = null;
        System.runAs(drupalUser) {
            testAcc = createAccount('Member');
            insert testAcc;
        
            Test.startTest();
            testAcc.BD_Current_Role__c = 'Black';
            update testAcc;
            Test.stopTest();
        }
        
        List<User_Role_History__c> hList = getRoleHistory();
        System.assertEquals(hList.size(), 2);
        System.assertEquals(hList[0].Previous_Role__c, null);
        System.assertEquals(hList[0].Name, 'Member');
        System.assertEquals(hList[0].Account__c, testAcc.Id);
        
        System.assertEquals(hList[1].Previous_Role__c, 'Member');
        System.assertEquals(hList[1].Name, 'Black');
        System.assertEquals(hList[1].Account__c, testAcc.Id);
    }
    
    static TestMethod void updateAccountRoleChangeAsDrupalUser() {
        User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin']; //Get reference to the user Drupal will insert changes under
        
        System.runAs(drupalUser) {
            Account testAcc = createAccount('Client');
            insert testAcc;
        
            Test.startTest();
            testAcc.BD_Current_Role__c = 'Premium Client';
            update testAcc;
            Test.stopTest();
        }
        
        List<User_Role_History__c> hList = getRoleHistory();
        System.assertEquals(hList.size(), 2);
        
        System.assertEquals(hList[1].Previous_Role__c, 'Client');
        System.assertEquals(hList[1].Name, 'Premium Client');
        System.assertEquals(hList[1].Manual_Change__c, FALSE);
    }
    
    static TestMethod void updateAccountRoleChangeAsNamedUser() {
        User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin']; 
        
        Account testAcc = null;
        System.runAs(drupalUser) {
            testAcc = createAccount('Premium Client');
            insert testAcc;
        }
        
        List<User> namedUser = [SELECT Id FROM User WHERE Name != 'System Admin' and IsActive = TRUE and UserType = 'Standard' and UserRoleId != '']; //Get reference to one of the named sales users
        System.assert(namedUser.size() > 0);
        
        Test.startTest();
        System.runAs(namedUser[0]) {
            testAcc.BD_Current_Role__c = 'Platinum Client';
            update testAcc;
        }
        Test.stopTest();
        
        List<User_Role_History__c> hList = getRoleHistory();
        System.assertEquals(hList.size(), 2);
        
        System.assertEquals(hList[1].Previous_Role__c, 'Premium Client');
        System.assertEquals(hList[1].Name, 'Platinum Client');
        System.assertEquals(hList[1].Manual_Change__c, TRUE);
    }
    
    static TestMethod void updateAccountRemoveRole() {
        User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
        
        System.runAs(drupalUser) {
            Account testAcc = createAccount('Premium Client');
            insert testAcc;
        
            Test.startTest();
            testAcc.BD_Current_Role__c = null;
            update testAcc;
            Test.stopTest();
        }
        
        List<User_Role_History__c> hList = getRoleHistory();
        System.assertEquals(hList.size(), 2);
        
        System.assertEquals(hList[1].Previous_Role__c, 'Premium Client');
        System.assertEquals(hList[1].Name, 'None');
    }
    
    static TestMethod void createAccountWithOpportunity() {
        //Find System Admin user
        User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
        
        List<Account> accs = new List<Account>();
        
        Test.startTest();
        //Create Account with Premium Member Role
        Account pmAcc = createAccount('Premium Member', 1);
        accs.add(pmAcc);
        
        //Create Account with Member role
        Account mAcc = createAccount('Member', 2);
        accs.add(mAcc);
        
        //Create Account with Premium Client role
        Account pcAcc = createAccount('Premium Client', 3);
        accs.add(pcAcc);
        
        insert accs;
        
        Test.stopTest();
        
        //Check both Accounts have an Opportunity against them with the same Owner as the Account
        List<Opportunity> pmOpps = [SELECT Id, OwnerId, Account.OwnerId FROM Opportunity WHERE AccountId = :pmAcc.Id];
        List<Opportunity> mOpps = [SELECT Id, OwnerId, Account.OwnerId FROM Opportunity WHERE AccountId = :mAcc.Id];
        List<Opportunity> pcOpps = [SELECT Id, OwnerId, Account.OwnerId FROM Opportunity WHERE AccountId = :pcAcc.Id];
        
        //System.assertEquals(pmOpps.size(), 1);
        //System.assertEquals(mOpps.size(), 1);
        //System.assertEquals(pcOpps.size(), 1);
        
       // System.assertEquals(pmOpps[0].OwnerId, pmOpps[0].Account.OwnerId);
       // System.assertEquals(mOpps[0].OwnerId, mOpps[0].Account.OwnerId);
       // System.assertEquals(pcOpps[0].OwnerId, pcOpps[0].Account.OwnerId);
    }
    
    static TestMethod void createAccountWithoutOpportunity() {
        //Find System Admin User
        User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
        
        List<Account> accs = new List<Account>();
        
        Test.startTest();
        //Create Account with no Role
        Account noAcc = createAccount(null, 1);
        accs.add(noAcc);
        
        //Create Account with the Client role
        Account cAcc = createAccount('Client', 2);
        accs.add(cAcc);
        
        insert accs;
        
        Test.stopTest();
        
        //Check neither Account has any Opportunities
        List<Opportunity> opps = [SELECT Id FROM Opportunity];
        System.assertequals(opps.size(), 0);
    }
    
    static TestMethod void updateAccountCreateOpportunity() {
        //Find System Admin User
        User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
        
        List<Account> accs = new List<Account>();
        
        //Create 3 Accounts with no role
        Account acc1 = createAccount(null, 1);
        Account acc2 = createAccount(null, 2);
        Account acc3 = createAccount(null, 3);
        accs.add(acc1);
        accs.add(acc2);
        accs.add(acc3);
        
        insert accs;
        
        System.runAs(drupalUser) {
            Test.startTest();
            //Update each Account, setting one to have Premium Member role and one to have Member role
            acc1.BD_Current_Role__c = 'Premium Member';
            acc2.BD_Current_Role__c = 'Member';
            acc3.BD_Current_Role__c = 'Premium Client';
            update accs;
        
            Test.stopTest();
        }
        
        //Check that both Accounts have Opportunities against them and that the Opp Owner is the same as the Account Owner  
       // List<Opportunity> acc1Opps = [SELECT Id, OwnerId, Account.OwnerId FROM Opportunity WHERE AccountId = :acc1.Id];
      //  List<Opportunity> acc2Opps = [SELECT Id, OwnerId, Account.OwnerId FROM Opportunity WHERE AccountId = :acc2.Id];
      //  List<Opportunity> acc3Opps = [SELECT Id, OwnerId, Account.OwnerId FROM Opportunity WHERE AccountId = :acc3.Id];
        
       // System.assertEquals(acc1Opps.size(), 1);
     //   System.assertEquals(acc2Opps.size(), 1);
      //  System.assertEquals(acc3Opps.size(), 1);
      //  System.assertEquals(acc1Opps[0].OwnerId, acc1Opps[0].Account.OwnerId);
     //   System.assertEquals(acc2Opps[0].OwnerId, acc2Opps[0].Account.OwnerId);
      //  System.assertEquals(acc3Opps[0].OwnerId, acc3Opps[0].Account.OwnerId);
    }
    
    static TestMethod void updateAccountNoNewOpportunity() {
        //Find System Admin User
        User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
        
        List<Account> accs = new List<Account>();
        
        //Create an Account with no role
        Account acc1 = createAccount(null, 1);
        insert acc1;
        
        System.runAs(drupalUser) {
            Test.startTest();
            //Update Account, setting to have Premium Member role
            acc1.BD_Current_Role__c = 'Premium Member';
            update acc1;
            
            //Ensure that the Opportunity has been created
           // List<Opportunity> acc1Opps = [SELECT Id, OwnerId, Account.OwnerId FROM Opportunity WHERE AccountId = :acc1.Id];
          //  System.assertEquals(acc1Opps.size(), 1);
          //  System.assertEquals(acc1Opps[0].OwnerId, acc1Opps[0].Account.OwnerId);
            
            //Update again to upgrade from Premium Member to Premium Client
            acc1.BD_Current_Role__c = 'Premium Client';
            update acc1;
        
            Test.stopTest();
        }
        
        //Check that the Account does not have any additional Opportunities created against it - only the one from before   
      //  List<Opportunity> acc1NoNewOpps = [SELECT Id, OwnerId, Account.OwnerId FROM Opportunity WHERE AccountId = :acc1.Id];
      //  System.assertEquals(acc1NoNewOpps.size(), 1);
    }
    
    static TestMethod void updateAccountNoOpportunity() {
        //Find System Admin User
        User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
        
        List<Account> accs = new List<Account>();
        
        System.runAs(drupalUser) {
            //Create 2 Accounts, one with the Client role and one with the Platinum Client role
            Account clientAcc = createAccount('Client', 1);
            Account premClientAcc = createAccount('Platinum Client', 2);
            accs.add(clientAcc);
            accs.add(premClientAcc);
        
            insert accs;
        
            Test.startTest();
            //Update both accounts, changing the role to Member in both cases
            clientAcc.BD_Current_Role__c = 'Member';
            premClientAcc.BD_Current_Role__c = 'Member';
            update accs;
        
            Test.stopTest();
        }
        
        //Check neither Account have any Opportunities
        List<Opportunity> opps = [SELECT Id FROM Opportunity];
        System.assertEquals(opps.size(), 0);
    }
    
    static TestMethod void updateAccountInactiveOwner() {
        //Find System Admin User
        User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
        
        //Find a User to act as the Account Owner
        User oldAccountOwner = [SELECT Id, IsActive FROM User WHERE IsActive = TRUE AND UserType = 'Standard' AND UserRoleId != '' AND alias NOT IN ('sadm', 'it', 'gkhre') LIMIT 1];
        
        //Grab a second 'runAs' user to execute the inactivation of the owner user
        User poweredUser = [SELECT Id FROM User WHERE IsActive = TRUE AND UserType = 'Standard' AND UserRoleId != '' AND alias NOT IN ('sadm', 'it') AND Id != :oldAccountOwner.Id LIMIT 1];
        
        //Create an Account against the chosen user
        Account a = createAccount('', 1);
        a.OwnerId = oldAccountOwner.Id;
        insert a;
        
        //Check the owner is correct
        Account preTestAcc = [SELECT Id, OwnerId FROM Account WHERE Id = :a.Id];
        System.assertEquals(preTestAcc.OwnerId, oldAccountOwner.Id);
        
        //Deactivate the User account
        System.runAs(poweredUser){
            oldAccountOwner.IsActive = false;
            update oldAccountOwner;
        }
        
        Test.startTest();
        System.runAs(drupalUser){
            //Update Account, set the Current Role = 'Premium Member'
            a.BD_Current_Role__c = 'Premium Member';
            update a;
        }
        Test.stopTest();
        
        //Test that the Account has been reallocated
        Account testAcc = [SELECT Id, OwnerId FROM Account WHERE Id = :a.Id];
        System.assertNotEquals(testAcc.OwnerId, oldAccountOwner.Id);
        
        //Test that an Opportunity has been created against the Account for the current new Owner
      //  List<Opportunity> testOpps = [SELECT Id, OwnerId FROM Opportunity WHERE AccountId = :testAcc.Id];
      //  System.assertEquals(testOpps.size(), 1);
    }

}