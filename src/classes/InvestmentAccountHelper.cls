/**
 * InvestmentAccountHelper: wrapper class for Investment Accounts.
 *
 * Contains all the additional methods for manipulating Investment Account custom object instances
 *
 * @author: Ross Tailby
 * @license: http://www.eurekareport.com.au
 */
public with sharing class InvestmentAccountHelper {

	public List<Account> accounts {get; set;}
	public List<Id> accountIds {get; set;}
	
	public Map<String, Investment_Account__c> invAccMap {get; set;}
	
	/* Definition of all the Asset Allocation option types we're currently using */
	public static List<String[]> assetArray = new List<String[]>();
	static {
		assetArray.add(new String[]{'cash', 'Cash'});
		assetArray.add(new String[]{'equities', 'Equities'});
		assetArray.add(new String[]{'fixed_interest', 'Fixed Interest'});
		assetArray.add(new String[]{'intl_interest', 'International Equities'});
		assetArray.add(new String[]{'intl_fixed_interest', 'International Fixed Interest'});
		assetArray.add(new String[]{'other', 'Other'});
		assetArray.add(new String[]{'property', 'Property'});
		assetArray.add(new String[]{'property_trusts', 'Property Trusts'});
	}
	
	public InvestmentAccountHelper(List<Account> accs) {
		this.accounts = accs;
		this.accountIds = new List<Id>();
		for (Account a : accs) {
			accountIds.add(a.Id);
		}
		
		getAllInvestmentAccounts();
	}
	
	public void getAllInvestmentAccounts(){
		List<Investment_Account__c> invAccs = [SELECT Id, Name, Account_Number__c, 
												  Investment_Account_Type__c, 
												  Account__c,
												  Address__c,
												  Code__c,
												  Contact_Name__c,
												  Country__c,
												  Email__c,
												  Phone__c,
												  Postcode__c,
												  State__c,
												  Status__c,
												  Suburb__c,
												  AssetAllocation__c,
												  Total_Value__c
												  FROM Investment_Account__c
												  WHERE Account__r.Id IN :this.accountIds];
		
		this.invAccMap = new Map<String, Investment_Account__c>(); 
		for (Investment_Account__c ia : invAccs){
			this.invAccMap.put(String.valueOf(ia.Account_Number__c), ia);
		}
	}
	
	public void processAccountProducts(){
		
		List<Investment_Account__c> upsertInvestments = new List<Investment_Account__c>();
		List<Investment_Account__c> deleteInvestments = new List<Investment_Account__c>();
		
		for (Account acc : this.accounts) {
			
			//No need to process anything other than deletes if the Products_Raw text is blank
			if (acc.Products_Raw__c != null && acc.Products_Raw__c != '') {
				
				Map<String, Object> iaNew = getJSONMap(acc.Products_Raw__c);
				
				if (iaNew != null && iaNew.containsKey('accounts')) {
					Map<String, Object> iaList = (Map<String, Object>)iaNew.get('accounts');
					
					for (Object ia : iaList.values()) {
						Map<String, Object> iaMap = (Map<String, Object>)ia;
						
						Investment_Account__c uInvest = null;
						Boolean needsUpsert = false;
						
						//If the Investment Account type are not supplied, move onto the next Account (required field)
						if (((String)iaMap.get('type') == null || (String)iaMap.get('type') == '')
							&& ((String)iaMap.get('account_type') == null || (String)iaMap.get('account_type') == '')){
							continue;
						}
						
						
						Object accId = iaMap.get('account_id');
						
						if (accId == null)
							continue; //This JSON entry does not have an Account ID - therefore should be a supplimentary entry picked up below
						else if (accId != null && this.invAccMap.containsKey(String.valueOf(accId))){
							//The Investment Account already exists - we're doing an update
							uInvest = this.invAccMap.get(String.valueOf(accId));
							
							//The Id is unique so, once found, we should remove the Investment Account from the invAccMap
							this.invAccMap.remove(String.valueOf(accId));
						}
						else {
							//The Investment Account does NOT exist - this is an insert
							uInvest = new Investment_Account__c();
							needsUpsert = true;
						}
						
						/* Also check if we have another entry in the map that corresponds to this account_id -> supplimentary values to add to our Map*/
						if (accId != null && iaList.containsKey(String.valueOf(accId))){
							iaMap.putAll((Map<String, Object>)iaList.get(String.valueOf(accId)));
						}
						/* --- */
						
						//Only update the object (and individual fields) if the value has changed
						if (uInvest.Name != (String)iaMap.get('account_name')){ 
							uInvest.Name = (String)iaMap.get('account_name');
							needsUpsert = true;
						}
						if (uInvest.Account__c != acc.Id){
							uInvest.Account__c = acc.Id;
							needsUpsert = true;
						}
						if (uInvest.Account_Number__c == null || (uInvest.Account_Number__c != String.valueOf((Integer)iaMap.get('account_id')))){
							uInvest.Account_Number__c = String.valueOf((Integer)iaMap.get('account_id'));
							
							//We need a value for the Account Number so we can map correctly - if not supplied in JSON, use the account ID instead
							if (uInvest.Account_Number__c == null || uInvest.Account_Number__c == '')
								uInvest.Account_Number__c = acc.Id;
								
							needsUpsert = true;
						}
						
						if ((String)iaMap.get('type') == null) {
							if (uInvest.Investment_Account_Type__c != (String)iaMap.get('account_type')){
								uInvest.Investment_Account_Type__c = (String)iaMap.get('account_type');
								needsUpsert = true;
							}
						}
						else {
							if (uInvest.Investment_Account_Type__c != (String)iaMap.get('type')){
								uInvest.Investment_Account_Type__c = (String)iaMap.get('type');
								needsUpsert = true;
							}
						}
						
						if (uInvest.Address__c != (String)iaMap.get('address')){
							uInvest.Address__c = (String)iaMap.get('address');
							needsUpsert = true;
						}
						
						if (uInvest.Code__c != (String)iaMap.get('code')){
							uInvest.Code__c = (String)iaMap.get('code');
							needsUpsert = true;
						}
						
						String contactName = (((String)iaMap.get('salutation') != '') ? (String)iaMap.get('salutation')+' ' : '') + (String)iaMap.get('contact_name');
						
						if (uInvest.Contact_Name__c != contactName){
							uInvest.Contact_Name__c = contactName;
							needsUpsert = true;
						}
						
						if (uInvest.Country__c != (String)iaMap.get('country')){
							uInvest.Country__c = (String)iaMap.get('country');
							needsUpsert = true;
						}
						
						if (uInvest.Email__c != (String)iaMap.get('email')){
							uInvest.Email__c = (String)iaMap.get('email');
							needsUpsert = true;
						}
						
						if(uInvest.Phone__c != (String)iaMap.get('phone')){
							uInvest.Phone__c = (String)iaMap.get('phone');
							needsUpsert = true;
						}
						
						if (uInvest.Postcode__c != (String)iaMap.get('post_code')){
							uInvest.Postcode__c = (String)iaMap.get('post_code');
							needsUpsert = true;
						}
						
						if (uInvest.State__c != (String)iaMap.get('state')){
							uInvest.State__c = (String)iaMap.get('state');
							needsUpsert = true;
						}
						
						if (uInvest.Status__c != (String)iaMap.get('status')){
							uInvest.Status__c = (String)iaMap.get('status');
							needsUpsert = true;
						}
						
						if (uInvest.Suburb__c != (String)iaMap.get('suburb')){
							uInvest.Suburb__c = (String)iaMap.get('suburb');
							needsUpsert = true;
						}
						
						if (iaMap.containsKey('value')) {
                            System.debug('>>> iaMap: ' + iaMap);
                            System.debug('>>> iaMap.get("value"): ' + iaMap.get('value'));
                            System.debug('>>> uInvest.Total_Value__c: ' + uInvest.Total_Value__c);
                            if (uInvest.Total_Value__c != Decimal.valueOf((String)iaMap.get('value'))){
								uInvest.Total_Value__c = Decimal.valueOf((String)iaMap.get('value'));
								needsUpsert = true;
							}
						}
						else if (uInvest.Total_Value__c != null){
							uInvest.Total_Value__c = null;
							needsUpsert = true;
						}
						
						//After configuring the Investment Account, now set up the Asset Allocation textarea for this IA, only if the serialised JSON has changed!
						List<AssetAllocation> assets = createAssetAllocation(uInvest, iaMap);
						String assetAllocate = JSON.serialize(assets);
						if (assetAllocate != uInvest.AssetAllocation__c){
							uInvest.AssetAllocation__c = assetAllocate;
							needsUpsert = true;
						}
						
						//only upsert if needsUpsert has been set to true
						if (needsUpsert)
							upsertInvestments.add(uInvest);
					}
				}
			}
		}
				
		//If we have some Investment Accounts to upsert, upsert them!
		if (upsertInvestments.size() > 0) 
			upsert upsertInvestments;
			
		//Now delete any Investment Accounts left in the Investment Account Map
		for (Investment_Account__c deleteIa : this.invAccMap.values()){
			deleteInvestments.add(deleteIa);
		}
		
		if (deleteInvestments.size() > 0)
			delete deleteInvestments;
	}

	
	/* Pull out details of the Asset Allocation and create the JSON asset string for the Investment Account */
	public List<AssetAllocation> createAssetAllocation(Investment_Account__c uInvest, Map<String, Object> iaMap) {
		
		//AssetAllocation aa = new AssetAllocation();
		List<AssetAllocation> assets = new List<AssetAllocation>();
		
		//Go through each Investment Option and, if there is a positive >0 figure, create an option for it
		for (String[] assetOption : assetArray){
			if (iaMap.containsKey(assetOption[0])){
				AssetAllocation aa = getAssetFromMap(iaMap, assetOption[0], assetOption[1]);
				
				if (aa != null)
					assets.add(aa);
			}
		}
		
		return assets;
	}
	
	/* Generically create the AssetAllocation instance by passing the correct data */
	public AssetAllocation getAssetFromMap(Map<String, Object> iaMap, String jsonAsset, String vfAsset) {
		
		AssetAllocation aa = null;
		Map<String, Object> assetMap = (Map<String, Object>)iaMap.get(jsonAsset);
		if (assetMap.size() > 0 && decimal.valueOf((String)assetMap.get('percent'))>0.00) {
				
			aa = new AssetAllocation(vfAsset, 
									decimal.valueOf((String)assetMap.get('percent')), 
									decimal.valueOf((String)assetMap.get('value')));
		}
		
		return aa;
	}
	
	/* JSON manipulation methods */
	public static List<Object> getJSONList(String products){
		return (List<Object>) JSON.deserializeUntyped(products);
	}

	public static Map<String, Object> getJSONMap(String products){
		Map<String, Object> jsonMap = null;
		try {
			jsonMap = (Map<String, Object>) JSON.deserializeUntyped(products);
		}
		catch(JSONException jse) {
			System.debug('DEBUG: There was an error with the JSON format. Exception: ' + jse + ' Line: ' + jse.getLineNumber() 
            				+ ' StackTrace:' + jse.getStackTraceString());
		}
		return jsonMap;
	}
	
	/**
	* Asset Allocation class - holds the type of asset, its percentage of the current investment total and the actual dollar value
	*/
	public class AssetAllocation {
		
		public String asset {get; set;}
		public Decimal percentage {get; set;}
		public Decimal value {get; set;}
		
		public AssetAllocation(String asset, Decimal percentage, Decimal value){
			this.asset = asset;
			this.percentage = percentage;
			this.value = value;
		}
	}

}