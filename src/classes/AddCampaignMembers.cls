/**
 * AddCampaignMember
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 * @note: We should be able to remove this code as the auto-allocate to Campaign functionality is not used and this does not get called
 */
global class AddCampaignMembers implements Schedulable {
	
	global void execute(SchedulableContext ctx) {
		//PaymentWrapper scriptBatch = new PaymentWrapper();
		//ID batchprocessid = Database.executeBatch(scriptBatch, 1); // execute one at a time
  		runTask();
	}
	
	@future(callout=true) 
	public static void runTask() {
  		List<Account> accounts = new List<Account>();
  		List<CampaignMember> members = [SELECT Id, CampaignId, ContactId, Next_Offer_Date__c FROM CampaignMember WHERE Next_Offer_Date__c <= :date.today() LIMIT 10];
  		for (CampaignMember member : members) {
  			CampaignMemberHelper helper = new CampaignMemberHelper(member);
  			Account account = helper.account;
  			Product_Instance__c instance = helper.getLastProductInstance();
  			
  			if (instance != null) {
	  			Integer remainingDays = member.Next_Offer_Date__c.daysBetween(instance.End_Date__c);
	  			
	  			// searches for the first campaign offer that is inside the remaining days field
	  			Campaign currentOffer = helper.getCurrentBestCampaignOffer(remainingDays);
	  			if (currentOffer != null) {
	  				// these campaigns should always be children of another campaign
	  				/*account.Campaign_Offer__c = currentOffer.Id; //SF-864 This field is not required */
	  				accounts.add(account);
	  			}
	  			
	  			// find the next offer after the current campaign offer
	  			Campaign nextOffer = helper.getNextBestCampaignOffer(remainingDays);
  				if (nextOffer == null) {
  					// set the next offer date to the last subscription end date
	  				member.Next_Offer_Date__c = instance.End_Date__c;
  				} else {
  					Date offerDate = instance.End_Date__c.addDays(0 - (Integer)nextOffer.Duration__c);
  					member.Next_Offer_Date__c = offerDate;
  				}
  			}
  		}
  		// save the new campaign offer into the member and account objects
  		update accounts;
  		update members;
  	}
  	
  	// start the background process.
	global static void start(String job_name, String scheduled_time) {
		//'0 0 * * * ?' calls function every hour
		AddCampaignMembers scheduled_task = new AddCampaignMembers();
		String payment_id = System.schedule(job_name, scheduled_time, scheduled_task);
	}
}