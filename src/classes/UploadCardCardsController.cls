/**
 * TestAccountHelper 
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
 global with sharing class UploadCardCardsController implements Database.Batchable<UploadCardCardsController.CreditCard>, Database.AllowsCallouts {

	public class UploadCardException extends Exception {}

	global class CreditCard {
		public String CardName { get; set; }
		public String CardNumber { get; set; }
		public String ExpiryMonth { get; set; }
		public String ExpiryYear { get; set; }
		public String Expiry { get; set; }
		public String CCV { get; set; }
		public String Token { get; set; }
		public String Gateway { get; set; }
		public String LuhnTest { get; set; }
		
		public CreditCard() {}
	}

	public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    String[] filelines = new String[]{};
    public List<CreditCard> cards;
   
   	public String paymentGateway {get;set;}
   
   	public UploadCardCardsController() {
   		paymentGateway = 'a0k900000058l4i';   // Live
		//paymentGateway = 'a0kO0000001NAvh';   // Test
   	}
   
    public Pagereference ReadFile()
    {
        nameFile=contentFile.toString();
        filelines = nameFile.split('\n');
        cards = new List<CreditCard>();
        for (Integer i=1;i<filelines.size();i++)
        {
            String[] inputvalues = new String[]{};
            inputvalues = filelines[i].split(',');
           
            CreditCard c = new CreditCard();
            c.Token = inputvalues[0];
            c.CardName = inputvalues[1];
            c.CardNumber = inputvalues[2];
			c.ExpiryMonth = inputvalues[3];
			c.ExpiryYear = '20' + inputvalues[4];
			c.Expiry = '00/00';
			c.Expiry = go1.PaymentHelper.setMonth(c.Expiry, Integer.valueOf(c.ExpiryMonth));
			c.Expiry = go1.PaymentHelper.setYear(c.Expiry, Integer.valueOf(c.ExpiryYear)); 
			c.CCV = inputvalues[5];

			if (go1.PaymentHelper.CCValidation(c.CardNumber)) {
				c.LuhnTest = 'PASS';
			}
			else {
				c.LuhnTest = 'FAILED';
			}
			
			c.Gateway = paymentGateway;
			
            cards.add(c);
        }
        
        ID batchprocessid = Database.executeBatch(this, 1); // execute one at a time
        
        return null;
    }
   
    public List<CreditCard> getuploadedCreditCards()
    {
    	List<CreditCard> displayCards = new List<CreditCard>(); 
        if (cards!= NULL)
            if (cards.size() > 0 && cards.size() < 1000) {
                return cards;
            }
            else if (cards.size() >= 1000) {
            	Integer num = 0;
            	for (CreditCard c : cards) {
            		if (num >= 1000) {
						break;
					}
					displayCards.add(c);
					num++;
            	}
            	return displayCards;
            }
            else
                return null;
        else
            return null;
    }
    
    public void uploadCard(UploadCardCardsController.CreditCard c) {
		List<go1__CreditCardAccount__c> cards = [SELECT Id, Name, go1__Expiry__c, go1__Token__c FROM go1__CreditCardAccount__c WHERE go1__Token__c = :c.Token LIMIT 1];
		if (!cards.isEmpty()) {
			go1__CreditCardAccount__c card = cards[0];
			
			card.go1__Card_Name__c = c.CardName;
			card.go1__Expiry__c = c.Expiry;
			card.go1__Payment_Gateway__c = c.Gateway;
			
			go1.Paymethod method = go1.Paymethod.loadPaymentMethod(card.go1__Payment_Gateway__c);
		 	method.vault(c.CardNumber, card.go1__Expiry__c, card.go1__Token__c);
		 	
			update card;
		}
		else {
			throw new UploadCardException(c.Token);
		}
	}
    
    /**
	 * The BATCHABLE interface means we can parse a bunch of objects and run any updates on them.
	 */
	global Iterable<UploadCardCardsController.CreditCard> start(Database.BatchableContext BC){
		return cards;
	}
	
	global void execute(Database.BatchableContext BC, UploadCardCardsController.CreditCard[] creditcards) {
		for (UploadCardCardsController.CreditCard c : creditcards) {
			uploadCard(c);
		}
	}
	 
	global void finish(Database.BatchableContext BC){
		AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
	    	TotalJobItems, CreatedBy.Email
	    	FROM AsyncApexJob WHERE Id =
	    	:BC.getJobId()];
	}
	
	@istest
	static void testUploadCreditCards() {
		go1__Payment_Gateway__c gateway = new go1__Payment_Gateway__c();
		insert gateway;		
		Account account = TestAccountHelper.createAccount();
		go1__CreditCardAccount__c card = new go1__CreditCardAccount__c();
		card.go1__Account__c = account.Id;
		card.go1__Card_Name__c = 'Test';
		card.go1__Expiry__c = '01/1999';
		card.go1__Token__c = 'TOKEN';
		card.go1__Payment_Gateway__c = gateway.Id;
		card.go1__Merchant_ID__c = '1';
		insert card;
		
		CreditCard c = new CreditCard();
		c.CardName = '1234567890123456';
		c.ExpiryMonth = '2';
		c.ExpiryYear = '2014';
		c.Expiry = '2/2014';
		c.CCV = '123';
		c.Token = 'TOKEN';
		c.Gateway = gateway.Id;
		
		List<CreditCard> creditcards = new List<CreditCard>();
		creditcards.add(c); 
		
		UploadCardCardsController uploader = new UploadCardCardsController();
		uploader.cards = creditcards;
		ID batchprocessid = Database.executeBatch(uploader, 1); // execute one at a time
	}
	
	@istest
	static void testUploadFile() {
		go1__Payment_Gateway__c gateway = new go1__Payment_Gateway__c();
		insert gateway;		
		Account account = TestAccountHelper.createAccount();
		go1__CreditCardAccount__c card = new go1__CreditCardAccount__c();
		card.go1__Account__c = account.Id;
		card.go1__Card_Name__c = 'Test';
		card.go1__Expiry__c = '01/1999';
		card.go1__Token__c = 'TOKEN';
		card.go1__Payment_Gateway__c = gateway.Id;
		card.go1__Merchant_ID__c = '1';
		insert card;
		
		UploadCardCardsController controller = new UploadCardCardsController();
		
		Blob bodyBlob = Blob.valueOf('Token,CC Name,CreditCards,CC Exp MM,CC Exp YYYY, CCV\nTOKEN,CardName,1234567890123456,6,14,709');
    	controller.contentFile = bodyBlob;
		controller.paymentGateway = gateway.Id;
   		controller.ReadFile();

	}
	
}