/**
 * Controller: Investment Account Visualisation 
 *
 * Retrieves all Investment Account/Tax Entity/Investment Option data structure for the Account
 * and loads into the visualisation component 
 *
 * Author: tailbyr
 * Copyright: EurekaReport.com.au
 */
public with sharing class InvestmentAccountVisualisation {
	
	public Account parentAccount {get; set;}
	public List<VisualInvestmentAccount> visualInvestments {get; set;}
	
	public Datetime lastUpdated {get; set;}
	public Boolean refreshPage {get; set;}
	
	public InvestmentAccountVisualisation(ApexPages.StandardController stdController) {
		if (!Test.isRunningTest())
			stdController.addFields(new List<String>{'Last_Refresh_Datetime__c'});
		this.parentAccount = (Account)stdController.getRecord();
		refreshPage = false;
		
		populateDataStructure();
	}

	public void populateDataStructure() {
		List<Investment_Account__c> investAccountStructure = [SELECT Name, Investment_Account_Type__c, Account_Number__c, Code__c, 
							Status__c, Contact_Name__c, Email__c, Phone__c, Address__c, Suburb__c, State__c, 
							Postcode__c, Country__c, AssetAllocation__c, Total_Value__c, LastModifiedDate, 
							(SELECT Name, Entity_Type__c, Investment_Account__c FROM Tax_Entities__r),
							(SELECT Name, Option_Type__c, Investment_Products__c, Investment_Account__c, Tax_Entity__c, Percentage__c, Value__c FROM Investment_Options__r)
							FROM Investment_Account__c 
							WHERE Account__c = :this.parentAccount.Id];
				
		//Because we can't go two steps down the object hierarchy, we need to try to populate the Investment Options underneath Tax Entities
		visualInvestments = new List<VisualInvestmentAccount>();
		
		Map<Id, VisualTaxEntity> taxEntityMap = new Map<Id, VisualTaxEntity>();
		for (Investment_Account__c tempAccount : investAccountStructure) {
			
			VisualInvestmentAccount via = new VisualInvestmentAccount();
			via.investmentAccount = tempAccount;
			
			//If we have a last refreshed date/time against the account, initialise the lastUpdated to that
			if (parentAccount.Last_Refresh_Datetime__c != null && (lastUpdated == null || parentAccount.Last_Refresh_Datetime__c > lastUpdated))
				lastUpdated = parentAccount.Last_Refresh_Datetime__c;
			
			//The data may have been automatically refreshed since last manual refresh. Therefore, if this is the latest Investment_Account updated date, set it as the lastUpdated date/time
			if (lastUpdated == null || tempAccount.LastModifiedDate > lastUpdated)
				lastUpdated = tempAccount.LastModifiedDate;
				
			for (Tax_Entity__c tx : tempAccount.Tax_Entities__r) {
				VisualTaxEntity vte = new VisualTaxEntity();
				vte.taxEntity = tx;
				via.taxEntities.add(vte);
				taxEntityMap.put(tx.Id, vte);
			}
		
			//Loop around the Investment options and add to the Tax Entity where populated	
			for (Investment_Option__c io : tempAccount.Investment_Options__r) {
				if (io.Tax_Entity__c != null && taxEntityMap.containsKey(io.Tax_Entity__c)) {
					(taxEntityMap.get(io.Tax_Entity__c)).investmentOptions.add(io);
				}
				else {
					//Add to Investment Options object directly on the Investment Account
					via.investmentOptions.add(io);
				}
			}
			
			via.populateAssetAllocation();
			via.getPieData();
			
			visualInvestments.add(via);
		}
		
		//Finally, get any Investment Options that were linked to the Tax Entity and NOT the Investment Account
		List<Investment_Option__c> taxOnlyOptions = [SELECT Id, Name, Option_Type__c, 
													Investment_Products__c, 
													Investment_Account__c, 
													Tax_Entity__c,
													Value__c,
													Percentage__c
													FROM Investment_Option__c
													WHERE Investment_Account__c = NULL
													AND Tax_Entity__c IN :taxEntityMap.keySet()];
		
		for (Investment_Option__c option : taxOnlyOptions){
			//Shouldn't really need to check tax entity as it was in the query, but just to be safe...
			if (option.Tax_Entity__c != null && taxEntityMap.containsKey(option.Tax_Entity__c)) {
				(taxEntityMap.get(option.Tax_Entity__c)).investmentOptions.add(option);
			}
		}
		
	}
	
	public PageReference refreshDataStructure() {
		//When clicking the 'Refresh' button, it should execute the same process as the RefreshBrightDayData class
		this.refreshPage = true;
		return null;
	}
	
	public class VisualInvestmentAccount {
		public Investment_Account__c investmentAccount {get; set;}
		public List<VisualTaxEntity> taxEntities {get; set;}
		public List<Investment_Option__c> investmentOptions {get; set;}
		
		//Populate a collection of custom Asset Allocation objects created from the AssetAllocation JSON 
		public List<InvestmentAccountHelper.AssetAllocation> visualAllocationList {get; set;}
		
		//Graph visualisation of the asset allocation
		public List<PieWedgeData> pieData {get; set;}
		
		public VisualInvestmentAccount(){
			this.taxEntities = new List<VisualTaxEntity>();
			this.investmentOptions = new List<Investment_Option__c>();
		}
		
		public void populateAssetAllocation(){
			if (this.investmentAccount.AssetAllocation__c != null){
				
				this.visualAllocationList
						= (List<InvestmentAccountHelper.AssetAllocation>)JSON.deserialize(investmentAccount.AssetAllocation__c, 
																						  List<InvestmentAccountHelper.AssetAllocation>.class);
			}
		}
		
		//Populating the Pie Chart data
		public List<PieWedgeData> getPieData(){
			
			if (this.visualAllocationList != null && this.visualAllocationList.size()> 0){
				this.pieData = new List<PieWedgeData>();
				
				for (InvestmentAccountHelper.AssetAllocation vaa : visualAllocationList) {
					this.pieData.add(new PieWedgeData(vaa.asset, (vaa.percentage*100).setScale(2)));
				}
				
				return this.pieData;
			}
			else
				return null;
		}
	}
	
	public class VisualTaxEntity {
		public Tax_Entity__c taxEntity {get; set;}
		public List<Investment_Option__c> investmentOptions {get; set;}
		
		public VisualTaxEntity() {
			this.investmentOptions = new List<Investment_Option__c>();
		}
	}
	
	public class PieWedgeData {
		public String name {get; set;}
		public Decimal data {get; set;}
		
		public PieWedgeData(String name, Decimal data) {
			this.name = name;
			this.data = data;
		}
	}
}