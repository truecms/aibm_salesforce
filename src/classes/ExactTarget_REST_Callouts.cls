/**
* Future Callout Class to call directly into ExactTarget to trigger email sends. 
* Uses connection variables held in ExactTarget_Variables Custom Setting to authenticate and then
* calls the messageKey corresponding to the email in ExactTarget
*
* [Ths is not currently being used as triggered email sends have been Deprecated]
*/
public with sharing class ExactTarget_REST_Callouts {

	//Hold the JSON response to the callout as a class variable for Unit Test and external verification (not otherwise referenced)
	public static Map<String, Object> response {get; set;}

	/**
	* ExactTarget REST oAuth authentication : should always be called from a @future method
	*/
	public static String authenticate() {
		String token = '';
		
		HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
		
		ExactTarget_Variables__c etVars = ExactTarget_Variables__c.getOrgDefaults();
		String clientId = etVars.clientId__c;
        String clientSecret = etVars.clientSecret__c;
        String authEndpoint = etVars.Auth_Endpoint__c;
        
        req.setEndpoint(authEndpoint);
        req.setMethod('POST');
        req.setHeader('content-type', 'application/json');
        req.setHeader('Accept', 'application/json');
        req.setBody('{"clientId" : "' + clientId + '", "clientSecret" : "' + clientSecret + '"}');
        
        try {
            System.debug('DEBUG: Attempting to authorize...HttpRequest:' + req + ' Body:' + req.getBody());
            res = http.send(req);
            System.debug('DEBUG: Auth returned! Got response:' + res + ' Body:' + res.getBody());
        } catch(System.CalloutException e) {
            System.debug('Authorization Callout error: '+ e);
            System.debug(res.toString());
        }

        //Get the Authorization response and JSON parse
        Map<String, Object> authResponse = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
        if(String.valueOf(authResponse.get('ReturnCode')) != '10' && String.valueOf(authResponse.get('ReturnCode')) != '1'){
            token = String.valueOf(authResponse.get('accessToken'));
        }
        else {
            System.debug('DEBUG: something was wrong with the authorization JSON format..:' + authResponse);
        }
		
		return token;
	}

	@future (callout=true)
    public static void sendNotification(String firstName, String lastName, String email, String subscriberKey, String message) {
		
		HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        
        ExactTarget_Variables__c etVars = ExactTarget_Variables__c.getOrgDefaults();
        
        //Get the session token from Custom Setting - if its no longer valid we can call the authenticate method
        String token = etVars.Auth_Token__c;
        
		String fromName = etVars.From_Name__c;
		String fromAddress = etVars.From_Address__c;
		String messageEndpoint = etVars.Messaging_Endpoint__c;
		
		String messageKey = '';
		if (message == 'Beginner Onboarding')
			messageKey = etVars.Beginner_Onboarding_Email_Key__c;
		else if (message == 'Advanced Onboarding')
			messageKey = etVars.Advanced_Onboarding_Email_Key__c;

        HttpRequest newReq = new HttpRequest();
        HttpResponse newRes = new HttpResponse();
        newReq.setEndpoint(messageEndpoint + '/key:' + messageKey + '/send');
        newReq.setHeader('content-type', 'application/json');
        newReq.setHeader('Accept', 'application/json');
        newReq.setHeader('Authorization', 'Bearer ' + token);
        newReq.setMethod('POST');
        
        newReq.setBody('{"From": {"Address": "' + fromAddress + '","Name": "' + fromName + '"},"To": {"Address": "' + email + '","SubscriberKey": "' + subscriberKey + '","ContactAttributes": {"SubscriberAttributes": {"First Name": "' + firstName + '","Last Name": "' + lastName + '"}}}}');

        try {
            System.debug('DEBUG: Attempting to send... HttpRequest:' + newReq + ' Body:' + newReq.getBody());
            newRes = http.send(newReq);
            System.debug('DEBUG: sent! Got response:' + newRes + ' Response Body:' + newRes.getBody());
            
            //If the session token has expired, we'll get a 401 response code, so need to re-authenticate and try again
            if (newRes.getStatusCode() == 401) {
	            System.debug('DEBUG: our token is no longer valid, better re-authorize and try again');
	            token = ExactTarget_REST_Callouts.authenticate();
	            
	            //Get the newly set token and add to the HttpRequest header
	            etVars.Auth_Token__c = token;
	            newReq.setHeader('Authorization', 'Bearer ' + token);
	            newRes = http.send(newReq);
	            
	            System.debug('DEBUG: re-sent! Got response:' + newRes + ' Response Body:' + newRes.getBody());
            	            	
            	//We've refreshed the token so persist it to the Custom Setting (this HAS to be done after all Callouts)
            	update etVars;
	        }
	        else {
	            System.debug('DEBUG: Message sent, session token still valid. All is well..:' + newRes.getBody());
	        }
	        
	        //Check the Http Response - if the request failed, create a SF email to say the Welcome Video Email was not sent
	        if (newRes.getBody().length()>0){
	        	Boolean hasError = false;
	        	
	        	response = (Map<String, Object>) JSON.deserializeUntyped(newRes.getBody());
	        	
	        	List<Object> responseList = (List<Object>)response.get('responses');
	        	
	        	if (responseList == null || responseList.size() < 1){
	        		hasError = true;
	        	}
	        	else {
	        		Map<String, Object> responseMap = (Map<String, Object>)responseList[0];
	        		hasError = (Boolean)responseMap.get('hasErrors');
	        	}
	        	
        		if(hasError){
		            sendErrorEmail('ERROR: ExactTarget Callout failed with error from ExactTarget', 'REST Callout returned code ' + newRes.getStatus() + ' (' + newRes.getStatusCode() + ')', newRes.getBody());
        		}
	        }
            
        } catch(System.CalloutException e) {
            System.debug('Callout error: '+ e);
            System.debug(newRes.toString());
            sendErrorEmail('ERROR: ExactTarget Callout exception when sending Welcome Email', e);
        }

    }
    
    public static void sendErrorEmail(String subject, String exceptionMessage, String stacktrace) 
    {
    	//---- Email error to dev team
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        String[] toAddresses = new String[] {'tech.support@eurekareport.com.au'}; //TEST USE ONLY - use Custom Setting to hold email addresses
        mail.setToAddresses(toAddresses); 
        mail.setSubject(subject); 
            
        string s = 'Exception: ' + exceptionMessage 
           	+ '<br />StackTrace:' + stacktrace; 
        mail.setHtmlBody(s); 
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
        //-------------------------------------
    }
    
    public static void sendErrorEmail(String subject, Exception e)
    {
    	sendErrorEmail(subject, e.getMessage(), e.getStackTraceString());
    }

}