/**
* Batch job that runs nightly to pull all Accounts that have not signed up for a Onevue account after being registered on the BD site for x days
* A task for the Sales Team to follow up will be created for each of these customers 
* Author: tailbyr
* Copyright: EurekaReport.com.au
*/
public with sharing virtual class BDMemberTasks implements Database.Batchable<Account>, Database.Stateful {

    public static String lastSalesTeamMember { get; set; }
    
    public TaskManager tm {get; set;}
    
    public String memberFollowUpQueue {get; set;}
    public Boolean memberFollowUpRules {get; set;}
    
    public Integer followUpDay {get; set;}
    public Boolean active {get; set;}
    
    public String subject {get; set;}

    //Constructor - get the lastSalesTeamMember given one of the 'Member FollowUp' tasks
    public BDMemberTasks(Integer followUpDay, Boolean active, String subject) {
        this.followUpDay = followUpDay;
        this.active = active;
        this.subject = subject;
        
        lastSalesTeamMember = AccountBDHelper.getLastTaskOwner(this.subject);
            
        //Get the customer settings allocation rule and queue for this Expired FT automated task creator
        Allocation_Rules__c allocationRules = Allocation_Rules__c.getInstance('Default');
        memberFollowUpRules = allocationRules.Member_FollowUp_Allocation_Rule__c;
        memberFollowUpQueue = allocationRules.Member_FollowUp_Queue__c;
        
        tm = new TaskManager(memberFollowUpQueue);
    }
    
    //Batchable override method: start - queries the records to iterate over
    public virtual Iterable<Account> start(Database.batchableContext BC) {
        
        Date memberAfterDate = System.Today().addDays(followUpDay); //Not transitioned to Client+ status after 'followUpDay' days
        
        List<Account> memberFollowUpAccounts = [SELECT id, Name, OwnerId, PersonContactId, BD_Last_Login_Date__c, BD_Registration_Date__c
                                            FROM Account 
                                            WHERE (BD_Current_Role__c = 'Member'
                                            OR BD_Current_Role__c = 'Premium Member')
                                            AND OV_User_Object_Created__c = False
                                            AND BD_Registration_Date__c = :memberAfterDate
                                            AND Account.Id IN (SELECT AccountId FROM Opportunity WHERE IsClosed = FALSE)]; //Don't create if no Open Opps
            
        List<Account> finalAccounts = new List<Account>();
            
        for (Integer i = 0; i < memberFollowUpAccounts.size(); i++) {
            if (this.active == true && memberFollowUpAccounts[i].BD_Registration_Date__c < memberFollowUpAccounts[i].BD_Last_Login_Date__c) {
                finalAccounts.add(memberFollowUpAccounts[i]);
            }
            
            if (this.active == false 
                    && (memberFollowUpAccounts[i].BD_Registration_Date__c >= memberFollowUpAccounts[i].BD_Last_Login_Date__c
                        || memberFollowUpAccounts[i].BD_Last_Login_Date__c == null))
            {
                finalAccounts.add(memberFollowUpAccounts[i]);       
            }
        }   
                                    
        return finalAccounts;
    }
    
    //Batchable override method: execute
    public void execute(Database.BatchableContext BC, List<Account> instances) {
        AccountBDHelper accHelper = new AccountBDHelper(instances);
        accHelper.createBrightDayTasks(memberFollowUpRules, tm, this.subject);
    }
    
    //Batchable override method: finish - called when all batches are processed; allocates all fair tasks created in a fair manner across Acc mgrs
    public void finish(Database.BatchableContext BC) {
        
        AccountBDHelper.fairlyAllocateTasks(this.subject, this.tm, lastSalesTeamMember);
    }
}