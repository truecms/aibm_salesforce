/**
	Class, that allows to send invoice information.
*/
public with sharing class TransactionInvoiceController
{
	/**
		Transaction Id.
	*/
	private Id transId;
	
	public TransactionInvoiceController(Apexpages.Standardcontroller theController)
	{
		transId = theController.getId();
	}
	
	/**
		Sends the invoice email and redirects user back to transaction.
		It stays on the same page on crash.
	*/
	public PageReference doRedirect()
	{
		if (TransactionManager.sendInvoiceEmail(transId))
		{
			return new PageReference('/' + transId);
		} else
		{
			Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, 'Transaction doesn\'t follow the requirements. Please check it.'));
		}
		return null;
	}
}