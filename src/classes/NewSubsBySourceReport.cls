public with sharing class NewSubsBySourceReport 
{
	public static final String INV_TRENDS = 'Affiliate - Investment Trends';
	public static final String BUS_SPEC = 'Business Spectator';
	
	public List<DataRow> results {get; set;}
	public Integer totalSubs {get; set;}
	
	public Product_Instance__c dateInstance {get; set;}
	public String campaignType {get; set;}
	
	public NewSubsBySourceReport(){
		
		dateInstance = new Product_Instance__c();
		
		//Set default values
		this.campaignType = BUS_SPEC;
		
		Date defaultDate = Date.today();
		this.dateInstance.Start_Date__c = Date.newInstance(defaultDate.addMonths(-1).year(), defaultDate.addMonths(-1).month(), 1);
		this.dateInstance.End_Date__c = Date.newInstance(defaultDate.year(), defaultDate.month(), 1);
		
		initData();
	}
	
	public void initData(){
		results = new List<DataRow>();
		
		//Get the Campaign lotus nums
		List<Campaign> chosenCampaigns = [SELECT Eureka_ID__c, Type, Id FROM Campaign WHERE Type = :campaignType];
		List<String> campaignNums = new List<String>();
		
		for (Campaign c : chosenCampaigns) {
			campaignNums.add(c.Eureka_ID__c);
		}
		
		//Use the Campaign Type and dates to get the list
		List<AggregateResult> subs = queryResults(campaignNums, dateInstance);
		
		campaignNums = null;		
		Map<Id, DataRow> matchedRow = new Map<Id, DataRow>();
													
		for (AggregateResult ar : subs) {
			Datetime firstPaymentCorrect = (Datetime)ar.get('first_payment');
			Date firstPayCorrectDate = Date.newInstance(firstPaymentCorrect.year(), firstPaymentCorrect.month(), firstPaymentCorrect.day());
		 	DataRow dr = new DataRow((String)ar.get('email'), firstPayCorrectDate, (Decimal)ar.get('amount'));
		 	matchedRow.put((Id)ar.get('go1__Account__c'), dr);
		}
		
		//Now get the other data values required for the output
		List<Account> accs = [SELECT Id, 
									Eureka_Id__c, 
									ER_Reporting_FT_Entry_Campaign__c, 
									ER_Reporting_First_Paid_Sub_Campaign__c, 
									ER_Reporting_Current_Sub_Campaign__c,
									ER_Entry_Free_Trial__r.Start_Date__c								
									FROM Account
									WHERE Id in :matchedRow.keySet()];
		
		for (Account a : accs) {
			DataRow dr = matchedRow.get(a.Id);
			if (dr != null) {
				dr.campaign = a.ER_Reporting_Current_Sub_Campaign__c;
				dr.ftEntryCampaign = a.ER_Reporting_FT_Entry_Campaign__c;
				dr.subscribedCampaign = a.ER_Reporting_First_Paid_Sub_Campaign__c;
				dr.subStart = a.ER_Entry_Free_Trial__r.Start_Date__c;
				dr.accountId = a.Id;
				
				if (a.Eureka_Id__c != null)
					dr.subId = a.Eureka_Id__c;
				else
					dr.subId = a.Id;
					
				results.add(dr);
			}
		}
		
		this.totalSubs = results.size();
		matchedRow = null;
	}
	
	
	/**
	* Main queries to find the Accounts/Subs that need to be reported on
	*/
	private List<AggregateResult> queryResults(List<String> campaignNums, Product_Instance__c dateInstance){
		
		Datetime startDate = Datetime.newInstance(dateInstance.Start_Date__c.year(), dateInstance.Start_Date__c.month(), dateInstance.Start_Date__c.day(), 0, 0, 0);
		Datetime endDate = Datetime.newInstance(dateInstance.End_Date__c.year(), dateInstance.End_Date__c.month(), dateInstance.End_Date__c.day(), 23, 59, 59);
		
		List<AggregateResult> subs = [SELECT go1__Account__c, go1__Account__r.PersonEmail email, min(go1__date__c) first_payment, min(go1__Amount__c) amount 
									FROM go1__Transaction__c 
									WHERE go1__Account__r.ER_Reporting_Sub_Status__c = '4' 
									AND go1__Account__r.ER_Reporting_FT_Entry_Campaign__c IN :campaignNums 
									AND go1__amount__c > 0 
									AND go1__status__c in ('Approved','Success','Successful Transaction','Successfully added manual transaction')
									AND Subscription_Instance__r.Is_Renewal__c != TRUE
									GROUP BY go1__Account__r.PersonEmail, go1__Account__c 
									HAVING min(go1__Date__c) > :startDate
									AND min(go1__Date__c) < :endDate];
		
		return subs;
	}
	
	public List<SelectOption> getSources() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption(INV_TRENDS, INV_TRENDS));
		options.add(new SelectOption(BUS_SPEC, BUS_SPEC));
		return options;
	}
	
	
	public class DataRow
	{
		public Id accountId {get; set;}
		public String subId {get; set;}
		
		public String email {get; set;}
		public Date firstPayment {get; set;}
		public Decimal amount {get; set;}
		
		public Date subStart {get; set;}
		public String subscribedCampaign {get; set;}
		public String campaign {get; set;}
		public String ftEntryCampaign {get; set;}
		
		DataRow(){}
		
		DataRow(String email, Date firstPayment, Decimal amount) {
			this.email = email;
			this.firstPayment = firstPayment; 
			this.amount = amount;
		}
	}
}