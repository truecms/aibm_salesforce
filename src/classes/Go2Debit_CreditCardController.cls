/**
 * Go2Debit_CreditCardController - Controller for credit card component. 
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
public with sharing class Go2Debit_CreditCardController {
	
	// credit card object
	public go1__CreditCardAccount__c creditcard { get; public set; }
	
	// payment details
	public go1.PaymentWrapper payment {
		get;
		set { 
			payment = value;
			payment.creditcard = this.creditcard;
		}
	}
	
	public List<go1__CardTypes__c> card_types { get; public set; }
	
	public String monthValue;
	public String yearValue;
	
	public Go2Debit_CreditCardController() {
		this.creditcard = new go1__CreditCardAccount__c();
		card_types = go1__CardTypes__c.getAll().values();
	}
	
	/**
	 * TODO: insert the card types externally
	 */
	public List<SelectOption> getCardTypes() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('', '-Select Card-'));
		if (!this.card_types.isEmpty()) {
			if (this.card_types[0].go1__MasterCard__c == true) {
				options.add(new SelectOption('MASTERCARD', 'MasterCard'));
			}
			if (this.card_types[0].go1__Visa__c == true) {
				options.add(new SelectOption('VISA', 'Visa'));
			}
			if (this.card_types[0].go1__Amex__c == true) {
				options.add(new SelectOption('AMEX', 'AMEX'));
			}
			if (this.card_types[0].go1__Diners__c == true) {
				options.add(new SelectOption('DINERS', 'Diners'));
			}
		}
		return options;
	}
	
	// get list of months
	public List<SelectOption> getMonthOptions() {
		List<String> months = new List<String> {'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'};
		List<SelectOption> options = new List<SelectOption>();
		for (Integer i = 1; i <= 12; i++) {
			options.add(new SelectOption(i.format(), months[i-1]));
		}
		return options;
	}
	
	// get list of years, assume only interested in future years
	public List<SelectOption> getYearOptions() {
		DateTime d = System.now();
		Integer year = (Integer)d.year();
		List<SelectOption> options = new List<SelectOption>();
		for (Integer i = 0; i < 20; i++) {
			String year_str = string.valueOf(year + i);
			options.add(new SelectOption(year_str, year_str));
		}
		return options;
	}
	public String getMonthValue() {
		return String.valueOf(go1.PaymentHelper.getMonth(creditcard.go1__Expiry__c));
	}
	public void setMonthValue(String monthValue) {
		creditcard.go1__Expiry__c = go1.PaymentHelper.setMonth(creditcard.go1__Expiry__c, Integer.valueOf(monthValue));
	}
	public String getYearValue() {
		return String.valueOf(go1.PaymentHelper.getYear(creditcard.go1__Expiry__c));
	}
	public void setYearValue(String yearValue) {
		creditcard.go1__Expiry__c = go1.PaymentHelper.setYear(creditcard.go1__Expiry__c, Integer.valueOf(yearValue));
	}
}