/**
 * Testing the RecurringInstMissingCardTasks class
 * Author: tailbyr
 * Copyright: eurekareport.com.au
 */
@isTest
public class TestRecurringInstanceNoCreditCardTrigger 
{
	static Subscription_Product__c createSubs()
	{
		//Create subscription stream
    	Subscription_Stream__c stream = new Subscription_Stream__c(Name='Eureka Report');
	  	insert stream;
	  	Subscription_Product__c product = new Subscription_Product__c();
	  	product.Subscription_Stream__c = stream.Id;
	  	product.Duration__c = 1;
	  	product.Duration_units__c = 'month';
	  	product.Price__c = 100;
	  	insert product;
	  	return product;
	}
	
	static RecordType getRecordType()
	{
		// Find any person account record type
	    RecordType recordType = [ select Id, Name, DeveloperName from RecordType where SObjectType = 'Account' and IsPersonType = true limit 1 ];
	    return recordType;
	}
	
	static List<Account> createAccounts(Integer numAccounts, RecordType recordType) 
	{
		//Create the accounts for test
    	List<Account> accs = new List<Account>(); 
    	List<Id> accIds = new List<Id>();
    	
    	for (integer i = 0; i < numAccounts; i++) {
    		Account account = new Account(FirstName='Test' +i, LastName='Tester' +i, PersonEmail='test'+i+'@test.com.test');
	  		account.RecordTypeId = recordType.Id;
	  		account.Sub_Status__c = '4';  // paid subscription
	  		accs.add(account);
    	}
    	insert accs;
    	return accs;
	}


	// Check that the No credit card against account task has been created for that scenario, and no other tasks!  
    static testMethod void testFailedRecurringPaymentTaskCreated() 
    {
     	//Set up the custom settings for queue to use and whether to utilise assignment rules or not
		TestProductInstanceHelper.createAllocationSettings();
		
		Subscription_Product__c product = createSubs();
     	RecordType recordType = getRecordType();
     
     	//Create just 1 account
    	List<Account> accountList = createAccounts(1, recordType);
    	Account account = accountList[0];
    	
    	//Create Recurring Instance
	  	go1__Recurring_Instance__c recur = new go1__Recurring_Instance__c();
	  	recur.go1__Account__c = account.Id;
	  	recur.go1__Amount__c = 38.50;
	  	recur.go1__Interval__c = 'Monthly';
	  	recur.go1__Remaining_Attempts__c = 10;
	  	recur.go1__Payment_Account_Type__c = 'Credit Card';
        recur.go1__Next_Attempt__c = Date.today().addDays(-1);
        
        insert recur;
    	
    	//Create Product Instance 
    	Product_Instance__c instance = new Product_Instance__c();
	  	instance.Person_Account__c = account.Id;
	  	instance.Subscription_Product__c = product.Id;
	  	instance.Subscription_Type__c = 'm';
	  	instance.Amount__c = 100;
	  	instance.Recurring_Instance__c = recur.Id;
    	
    	insert instance;        
        
        Test.startTest();
        
		//Run the Recurring Inst Mssing Card process to create the tasks
		Database.executeBatch(new RecurringInstMissingCardTasks(), 1);
        Test.stopTest();
  		
  		//Now check another task has not been created 
  		List<Task> ccTask = [SELECT Id, Subject FROM Task];
  		System.assertEquals(1, ccTask.size());
  		System.assertEquals(ccTask[0].Subject, TaskManager.HOURLY_RECURRING_INST_MISSING_CARD_SUBJECT);
  		
  		//If we run it a second time, it should NOT create an extra task
  		Database.executeBatch(new RecurringInstMissingCardTasks(), 1);
  		
  		//Now check another task has not been created 
  		List<Task> updateTask = [SELECT Id, Subject FROM Task];
  		System.assertEquals(1, updateTask.size());
	}
	
	public TestMethod static void TestFailedTaskNotCreatedWithCC()
	{
		//Set up the custom settings for queue to use and whether to utilise assignment rules or not
		TestProductInstanceHelper.createAllocationSettings();
				
		Subscription_Product__c product = createSubs();
     	RecordType recordType = getRecordType();
     
     	//Create just 1 account
    	List<Account> accountList = createAccounts(1, recordType);
    	Account account = accountList[0];
    	
    	go1__CreditCardAccount__c creditcard = go1.PaymentHelper.savedTestCreditCard(account);
    	
    	//Create Recurring Instance
	  	go1__Recurring_Instance__c recur = new go1__Recurring_Instance__c();
	  	recur.go1__Account__c = account.Id;
	  	recur.go1__Amount__c = 38.50;
	  	recur.go1__Interval__c = 'Monthly';
	  	recur.go1__Remaining_Attempts__c = 10;
	  	recur.go1__Payment_Account_Type__c = 'Credit Card';
        recur.go1__Credit_Card_Account__c=creditcard.Id;
        
        insert recur;
    	
    	//Create Product Instance 
    	Product_Instance__c instance = new Product_Instance__c();
	  	instance.Person_Account__c = account.Id;
	  	instance.Subscription_Product__c = product.Id;
	  	instance.Subscription_Type__c = 'm';
	  	instance.Amount__c = 100;
	  	instance.Recurring_Instance__c = recur.Id;
    	
    	insert instance;
        
        Test.startTest();
		
		//Run the Recurring Inst Mssing Card process to create the tasks
		Database.executeBatch(new RecurringInstMissingCardTasks(), 1);
		Test.stopTest();
  		
  		//Make sure that no tasks have been created
  		List<Task> updateTask = [SELECT Id, Subject FROM Task];
  		System.assertEquals(0, updateTask.size());
	}
	
	public TestMethod static void testNoFailedPaymentTaskCreatedOnAnnualSub()
	{
		//Set up the custom settings for queue to use and whether to utilise assignment rules or not
		TestProductInstanceHelper.createAllocationSettings();
				
		Subscription_Product__c product = createSubs();
     	RecordType recordType = getRecordType();
     
     	//Create just 1 account
    	List<Account> accountList = createAccounts(1, recordType);
    	Account account = accountList[0];
  		
  		//Create Product Instance 
    	Product_Instance__c instance = new Product_Instance__c();
	  	instance.Person_Account__c = account.Id;
	  	instance.Subscription_Product__c = product.Id;
	  	instance.Subscription_Type__c = 'y';
	  	instance.Amount__c = 100;
    	
    	insert instance;
  		
		Test.startTest();
		
  		//Run the Recurring Inst Mssing Card process to create the tasks
		Database.executeBatch(new RecurringInstMissingCardTasks(), 1);
  		Test.stopTest();
  		
  		//Check no task is created!
  		List<Task> insertTask = [SELECT Id FROM Task];
  		System.assertEquals(insertTask.size(), 0);
  		
	}
}