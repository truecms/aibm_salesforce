public class Deploy {
	
	public static final String AutoRenewal = 'AutoRenewal';
	public static final String MonthlyPayments = 'MonthlyPayments';
	public static final String CreditCardExpiry = 'CreditCardExpiry';
	public static final String AccountActiveStatus = 'AccountActiveStatus';
	
	public static final String AutoRenewalBackup = 'AutoRenewalBackup'; //Additional run of the AutoRenewal job to ensure it processes each day
	
	//tailbyr 20131014: adding the 'No credit card' taak creator to the scheduled jobs list
	public static final String noCreditCard = 'NoCreditCards';
	
	//tailbyr 20131203: ensuring that recurring payment errors through Go1 have tasks created and are not just lost
	public static final String failedRecurringPayment = 'FailedRecurringPayment';
	
	//tailbyr 20131217: hourly job to find any Recurring Instances created in last hour without linked credit card details
	public static final String hourlyRecurringNoCard = 'RecurringPaymentNoCard';
	
	//tailbyr 20140207: Duplicate Subs Report checking no doubled (or more) Product Instances have been created for an Account
	//public static final String duplicateSubsReportSchedule = 'DuplicateSubsReport'; //removed 20141201: manually run the report to free up processing time
	
	//tailbyr 20140407: Expired Free Trial task creator for the sales team
	public static final String expiredFTs = 'ExpiredFreeTrialTasks';
	
	//tailbyr 20140408: Late Renewal Task Creator for Customer Services
	public static final String lateRenewals = 'LateRenewalsTasks';
	
	//BrightDay Scheduled Jobs
	public static final String latePaperworkFirstCall = 'LatePaperworkFirstCall';
	//public static final String latePaperworkSecondCall = 'LatePaperworkSecondCall'; //Removed 20141204: latePaperworkFirstCall runs this too
	public static final String memberFollowUpCall = 'MemberFollowUpCall';
	public static final String oppsPerUserBatch = 'OppsPerUserBatch';

	public static final String salesResultAggregateSchedule = 'SalesResultAggregator';
	
	public List<DeploySchedule> jobs { get; set; }
	public String runJob { get; set; }
	
	public class DeploySchedule {
		
		public String ScheduleName { get; set; }
		public String ScheduleTime { get; set; }
		public String ScheduleStatus { get; set; }
		public Datetime ScheduleNextDate { get; set; }
		
		public DeploySchedule(String str, String tm) {
			ScheduleName = str;
			ScheduleTime = tm;
			updateStatus();
		}
		
		public void updateStatus() {
			list<CronTrigger> cron = new list<CronTrigger>([select Id, State, NextFireTime, OwnerId, CronExpression from CronTrigger WHERE CronExpression = :this.ScheduleTime LIMIT 1]);
			if (!cron.isEmpty()) {
				ScheduleStatus = cron[0].State;
				Datetime gmtScheduleNextDate = cron[0].NextFireTime;
				TimeZone tz = UserInfo.getTimeZone();
				ScheduleNextDate = gmtScheduleNextDate.AddSeconds(tz.getOffset(gmtScheduleNextDate)/1000);
			} else {
				ScheduleStatus = 'OFF';
			}
		}
	}
	
	public Deploy() {
		jobs = Deploy.schedules();
		
	}
	
	/*
	 * List of schedules that need to be setup on deployment.
	 */ 
	public static List<DeploySchedule> schedules() {
		List<DeploySchedule> deploySchedule = new List<DeploySchedule>();
		DeploySchedule.add(new DeploySchedule(Deploy.AutoRenewal, '0 15 3 * * ?'));
		DeploySchedule.add(new DeploySchedule(Deploy.AutoRenewalBackup, '0 10 6 * * ?'));
		DeploySchedule.add(new DeploySchedule(Deploy.MonthlyPayments, '0 0 4 * * ?'));
		DeploySchedule.add(new DeploySchedule(Deploy.CreditCardExpiry, '0 0 7 * * ?'));
		DeploySchedule.add(new DeploySchedule(Deploy.AccountActiveStatus, '0 0 2 * * ?'));
		DeploySchedule.add(new DeploySchedule(Deploy.NoCreditCard, '0 0 5 * * ?'));
		DeploySchedule.add(new DeploySchedule(Deploy.failedRecurringPayment, '0 0 6 * * ?'));
		DeploySchedule.add(new DeploySchedule(Deploy.hourlyRecurringNoCard, '0 0 12 * * ?'));
		//DeploySchedule.add(new DeploySchedule(Deploy.duplicateSubsReportSchedule, '0 0 9 * * ?'));
		DeploySchedule.add(new DeploySchedule(Deploy.expiredFTs, '0 30 6 * * ?'));
		DeploySchedule.add(new DeploySchedule(Deploy.lateRenewals, '0 30 5 * * ?'));
		DeploySchedule.add(new DeploySchedule(Deploy.latePaperworkFirstCall, '0 10 8 * * ?'));
		//DeploySchedule.add(new DeploySchedule(Deploy.latePaperworkSecondCall, '0 30 8 * * ?'));
		DeploySchedule.add(new DeploySchedule(Deploy.memberFollowUpCall, '0 30 11 * * ?'));
		DeploySchedule.add(new DeploySchedule(Deploy.oppsPerUserBatch, '0 0 1 * * ?'));
		DeploySchedule.add(new DeploySchedule(Deploy.salesResultAggregateSchedule, '0 0 23 ? * SUN'));
		return deploySchedule;
	}
	
	public static void start() {
		// remove the scheduled items
		list<CronTrigger> cron = new list<CronTrigger>([select Id, State, OwnerId, CronExpression from CronTrigger WHERE State = 'WAITING']);
		System.debug(cron);
		
		List<DeploySchedule> deploySchedule = Deploy.schedules();
		for (CronTrigger c : cron) {
			for (DeploySchedule schedule : DeploySchedule) {
				if (schedule.ScheduleTime == c.CronExpression) {
					System.abortJob(c.Id);
				}
			}
		}
	}
	
	public static void finalise() {
		// add the scheduled items again.
		
		List<DeploySchedule> deploySchedule = Deploy.schedules();
		for (DeploySchedule schedule : DeploySchedule) {
			if (schedule.ScheduleName == Deploy.AutoRenewal) {
				AutoRenewalSubscription.start('Auto Renewals', '0 15 3 * * ?');
			}
			else if (schedule.ScheduleName == Deploy.AutoRenewalBackup) {
				AutoRenewalSubscription.start('Auto Renewals Backup', '0 10 6 * * ?');
			}
			else if (schedule.ScheduleName == Deploy.MonthlyPayments) {
				go1.SchedulePayment.start('Monthly Payments', '0 0 4 * * ?');
			}
			else if (schedule.ScheduleName == Deploy.CreditCardExpiry) {
				CreditCardExpiresBatch scheduled_task = new CreditCardExpiresBatch();
				String payment_id = System.schedule('ExpiredCreditCards', '0 0 7 * * ?', scheduled_task);
			}
			else if (schedule.ScheduleName == Deploy.AccountActiveStatus) {
				AccountSchedule.start('Account Active Status', '0 0 2 * * ?');
			}
			else if (schedule.ScheduleName == Deploy.noCreditCard) {
				NoCreditCardBatch.setupSchedule('No Credit Cards', '0 0 5 * * ?');
			}
			else if (schedule.ScheduleName == Deploy.failedRecurringPayment) {
				FailedRecurringPaymentsTasks.setupSchedule('FailedRecurringPayment', '0 0 6 * * ?');
			}
			else if (schedule.ScheduleName == Deploy.hourlyRecurringNoCard) {
				RecurringInstMissingCardTasks.setupSchedule('RecurringPaymentNoCard', '0 0 12 * * ?');
			}
			/*else if (schedule.ScheduleName == Deploy.duplicateSubsReportSchedule) {
				DuplicateSubsReport.setupSchedule(Deploy.duplicateSubsReportSchedule, '0 0 9 * * ?');
			}*/
			else if (schedule.ScheduleName == Deploy.expiredFTs) {
				ExpiredFreeTrialTasks.setupSchedule(Deploy.expiredFTs, '0 30 6 * * ?');
			}
			else if (schedule.ScheduleName == Deploy.lateRenewals) {
				LateRenewalTasks.setupSchedule(Deploy.lateRenewals, '0 30 5 * * ?');
			}
			else if (schedule.ScheduleName == Deploy.latePaperworkFirstCall) {
				LatePaperworkFirstTask.setupSchedule(Deploy.latePaperworkFirstCall, '0 10 8 * * ?');
			}
			/*else if (schedule.ScheduleName == Deploy.latePaperworkSecondCall) {
				LatePaperworkSecondTask.setupSchedule(Deploy.latePaperworkSecondCall, '0 30 8 * * ?');
			}*/
			else if (schedule.ScheduleName == Deploy.memberFollowUpCall) {
				BDFirstMemberTask.setupSchedule(Deploy.memberFollowUpCall, '0 30 11 * * ?');
			}
			else if (schedule.ScheduleName == Deploy.oppsPerUserBatch) {
				OppPerUserSchedule.setupSchedule(Deploy.oppsPerUserBatch, '0 0 1 * * ?');
			}
			else if (schedule.ScheduleName == Deploy.salesResultAggregateSchedule) {
                SalesResultAggregator.setupSchedule(Deploy.salesResultAggregateSchedule, '0 0 23 ? * SUN');
            }
		}
	}
	
	public PageReference runScheduler() {
		SchedulableContext ctx;
		if (runJob == Deploy.AutoRenewal) {
			AutoRenewalSubscription scheduled_task = new AutoRenewalSubscription();
			scheduled_task.execute(ctx);
		}
		else if (runJob == Deploy.AutoRenewalBackup) {
			AutoRenewalSubscription scheduled_task = new AutoRenewalSubscription();
			scheduled_task.execute(ctx);
		}
		else if (runJob == Deploy.MonthlyPayments) {
			//@todo: change the visibility
			go1.SchedulePayment schedulePayment = new go1.SchedulePayment();
			schedulePayment.execute(ctx);
		}
		else if (runJob == Deploy.CreditCardExpiry) {
			CreditCardExpiresBatch scheduled_task = new CreditCardExpiresBatch();
			scheduled_task.execute(ctx);
		} 
		else if (runJob == Deploy.AccountActiveStatus) {
			AccountSchedule scheduled_task = new AccountSchedule();
			scheduled_task.execute(ctx);
		}
		else if (runJob == Deploy.noCreditCard) {
			NoCreditCardBatch scheduled_task = new NoCreditCardBatch();
			scheduled_task.execute(ctx);
		}
		else if (runJob == Deploy.failedRecurringPayment) {
			FailedRecurringPaymentsTasks scheduled_task = new FailedRecurringPaymentsTasks();
			scheduled_task.execute(ctx);
		}
		else if (runJob == Deploy.hourlyRecurringNoCard) {
			RecurringInstMissingCardTasks scheduled_task = new RecurringInstMissingCardTasks();
			scheduled_task.execute(ctx);
		}
		/*else if (runJob == Deploy.duplicateSubsReportSchedule) {
			DuplicateSubsReport scheduled_task = new DuplicateSubsReport();
			scheduled_task.execute(ctx);
		}*/
		else if (runJob == Deploy.expiredFTs) {
			ExpiredFreeTrialTasks scheduled_task = new ExpiredFreeTrialTasks();
			scheduled_task.execute(ctx);
		}
		else if (runJob == Deploy.lateRenewals) {
			LateRenewalTasks scheduled_task = new LateRenewalTasks();
			scheduled_task.execute(ctx);
		}
		else if (runJob == Deploy.latePaperworkFirstCall) {
			LatePaperworkFirstTask scheduled_task = new LatePaperworkFirstTask();
			scheduled_task.execute(ctx);
		}
		/*else if (runJob == Deploy.latePaperworkSecondCall) {
			LatePaperworkSecondTask scheduled_task = new LatePaperworkSecondTask();
			scheduled_task.execute(ctx);
		}*/
		else if (runJob == Deploy.memberFollowUpCall) {
			BDFirstMemberTask scheduled_task = new BDFirstMemberTask();
			scheduled_task.execute(ctx);
		}
		else if (runJob == Deploy.oppsPerUserBatch) {
			OppPerUserSchedule scheduled_task = new OppPerUserSchedule();
			scheduled_task.execute(ctx);
		}
		else if (runJob == Deploy.salesResultAggregateSchedule) {
            SalesResultAggregator scheduled_task = new SalesResultAggregator();
            scheduled_task.execute(ctx);
        }
		
		ApexPages.Message msgErr = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Schedule \'' + runJob + '\' has run successfully!');
		ApexPages.addmessage(msgErr);
		return null;
	}
	
	public PageReference setup() {
		finalise();
		jobs = Deploy.schedules();
		return null;
	}
	
	public PageReference remove() {
		start();
		jobs = Deploy.schedules();
		return null;
	}
}