/**
 * TestGetClicky
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
@isTest
private class TestGetClicky {
  
  static testMethod void testGetClickyDataTable() {
    Integer pageNumber = 1;
    Integer pageSize = 1;
    Integer totalPageNumber = 1;
    Map<String, String> pageGoals = new Map<String, String>();
    
    Account account = new Account();
    account.name = 'test';
    account.PersonEmail = 'email@noemail.com';
    GetClickyInterface.isApexTest = true;
    GetClickyInterface getclicky = new GetClickyInterface(new ApexPages.StandardController(account));
    getclicky.getPageNumber();
    getclicky.getPageSize();
    getclicky.getTotalPageNumber();
    //getclicky.getGoals();
    getclicky.getPageGoalsObjects();
    getclicky.getNextButtonDisabled();
    getclicky.getPreviousButtonEnabled();
    getclicky.nextBtnClick();
    getclicky.previousBtnClick();
  }
  
  /**
   * d
   */
  static TestMethod void TestGetClickyConstructor() {
    String username = 'test';
    String email = 'email@noemail.com';
    String last_visited = '1/1/2012';
    String geolocation = 'Brisbane';
    String visits_last_month = '48';
    Map<String, String> pageGoals = new Map<String, String>();
    String updated_time = '1/1/2012';
    
    Account account = new Account();
    account.name = 'test';
    account.PersonEmail = 'email@noemail.com';
    GetClickyInterface.isApexTest = true;
    GetClickyInterface getclicky = new GetClickyInterface(new ApexPages.StandardController(account));
    
    getclicky.getUsername();
    getclicky.getEmail();
    getclicky.getLastVisited();
    getclicky.getGeolocation();
    getclicky.getVisitsLastMonth();
    //getclicky.getGoals();
    getclicky.getPageGoalsObjects();
    getclicky.getUpdatedTime();
    
    getclicky.updateStats();
    getclicky.redirectGetClicky();
    
    List<XMLDom.Element> items = new List<XMLDom.Element>();
    getclicky.setGoals(items);
  }

}