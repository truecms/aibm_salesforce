/**
 * TransactionRecords
 *
 * Localised code taken from the Go2Debit payments module. 
 * Installed directly into SF to enable maniupulation and avoid Go1 namespace.
 *
 * @author: GO1 Pty Ltd
 * @updatedBy: tailbyr
 * @license: http://www.go1.com.au/tos
 */
public class TransactionRecords {
	ApexPages.StandardController controller;
	
	public Boolean isApexTest = false;
	public String refundId {get; set;}
	
	private List<go1__Transaction__c> transactions;
	private List<go1__Transaction__c> pageTransactions;
	private Integer pageNumber;
	private Integer pageSize;
	private Integer totalPageNumber;
	private Decimal total;
  	
  	List<go1__PaymentTypes__c> paymentTypes;
  
  	// getters
  	public Integer getStartPageNumber() {
  		return (pageNumber - 1) * pageSize + 1;
  	}
  	public Integer getEndPageNumber() {
  		return (pageNumber - 1) * pageSize + pageTransactions.size();
  	}
  	public Integer getTotalSize() {
  		return transactions.size();
  	}
  	public Decimal getTotal() {
  		return total;
  	}
  	public Integer getPageNumber() {
    	return pageNumber;
  	}
  	public List<go1__Transaction__c> getTransactions(){
    	return pageTransactions;
  	}
  	public Integer getPageSize(){
    	return pageSize;
  	}
  	public Boolean getPreviousButtonDisabled(){
    	return !(pageNumber > 1);
  	}
  	public Boolean getNextButtonDisabled(){
	    if (transactions == null) 
	    	return true;
	    else
	    	return ((pageNumber * pageSize) >= transactions.size());
  	}
  	public Integer getTotalPageNumber(){
	    if (totalPageNumber == 0 && transactions !=null){
	    	totalPageNumber = transactions.size() / pageSize;
	      	Integer mod = transactions.size() - (totalPageNumber * pageSize);
	    	if (mod > 0)
	        	totalPageNumber++;
	    }
	    return totalPageNumber;
  	}
  
  	/**
   	 * constructor
   	 */
  	public TransactionRecords(ApexPages.StandardController controller){
	  	this.controller = controller;
	  	this.paymentTypes = go1__PaymentTypes__c.getAll().values();
	  	
	  	// initialise the default values
	  	this.total = 0.00;
	    this.pageNumber = 0;
	    this.totalPageNumber = 0;
	    this.pageSize = 10;
	    
	    // display the data
	    ViewData();
  	}
  
  	/**
   	 * Change the current transaction list to reflect options such as page number 
   	 */
  	private void BindData(Integer newPageIndex){
    	try {
    		if (transactions == null) {
        		transactions = [SELECT Id, Name, 
        						go1__Description__c, 
        						go1__Code__c, 
        						go1__Amount__c, 
        						go1__Status__c, 
        						go1__Payment_Name__c, 
        						go1__Date__c, 
        						go1__Refunded__c, 
        						go1__Refundable__c 
        						FROM go1__Transaction__c 
        						WHERE go1__Account__c = :controller.getId() 
        						ORDER BY go1__Date__c DESC 
        						LIMIT 1000];
        		total = 0;
        		for (go1__Transaction__c trans : transactions) {
          			// only add the amount to total if the transaction was successful
          			if (integer.valueOf(trans.go1__Code__c) < 10) {
      	    			total += trans.go1__Amount__c;
          			}
        		}
      		}
		  	pageTransactions = new List<go1__Transaction__c>();
		  	Transient Integer counter = 0;
		  	Transient Integer min = 0;
	  		Transient Integer max = 0;
	     	if (newPageIndex > pageNumber){
	        	min = pageNumber * pageSize;
	        	max = newPageIndex * pageSize;
	      	} else {
				max = newPageIndex * pageSize;
				min = max - pageSize;
		  	}
	  		for(go1__Transaction__c t : transactions){
				counter++;
				if (counter > min && counter <= max)
		  			pageTransactions.add(t);
			}
			pageNumber = newPageIndex;
			if (pageTransactions == null || pageTransactions.size() <= 0) {
		  		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Data not available for this view.'));
    		}
    	} catch(Exception ex) {
	  		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
		}
	}
  
  	/**
   	 * ajax call to refresh list and load first page
   	 */
  	public PageReference ViewData(){
	    transactions = null;
	    totalPageNumber = 0;
	    BindData(1);
	    return null;
  	}
  
  	/**
   	 * ajax call to request all transactions that belong to the next page
   	 */
  	public PageReference nextBtnClick() {
		BindData(pageNumber + 1);
		return null;
  	}
  
  	/**
   	 * ajax call to request all transactions that belong to the previous page
	 */
  	public PageReference previousBtnClick() {
		BindData(pageNumber - 1);
		return null;	
	}
  
  	/**
   	 * ajax call to refund a transaction and refresh the current page to display changes
   	 */
  	public PageReference refund() {
	  	PageReference page = new PageReference('/apex/go1__refund?id='+refundId);
		page.setRedirect(true);
		return page;
  	}
}