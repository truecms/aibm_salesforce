/**
 * RefreshBrightDayData
 *
 * Gives the Sales Team the ability to get the very latest OV data to reflect in the Account record
 * Used before calling or if they receive an inbound call 
 *
 * Author: tailbyr
 * Copyright: EurekaReport.com.au
 */
public class RefreshBrightDayData {
	
	public Account refreshBDAccount { get; set; }
	
	public boolean ack { get; set; }
	public boolean redirect { get; set; }
	
	public RefreshBrightDayData(ApexPages.StandardController stdController) {
		if (!Test.isRunningTest())
			stdController.addFields(new List<String>{'PersonEmail','Last_Refresh_Datetime__c','Long_ID__c'});
		this.refreshBDAccount = (Account)stdController.getRecord();
		
		this.ack = false; //Current negative acknowledgement : populated from HTTPResponse and defines whether to update Account or not 
		this.redirect = false; //Triggers the redirect back to the Account page, this should always be set to True by the end of the class execution
	}
	
	public void calloutDataRefresh(){
		httpCallout();
		
		updateAccount();
	}
	
	public void httpCallout() {
		//Callout
		try {
			//Get the endpoint from the Custom Setting
			BrightDay_Settings__c bdSettings = BrightDay_Settings__c.getOrgDefaults();
			String endpoint = bdSettings.Refresh_brightday_Endpoint__c;
			
			if (endpoint != null) {
			    HttpRequest req = new HttpRequest();
			    req.setEndpoint(endpoint + this.refreshBDAccount.Long_ID__c);
			    req.setMethod('GET');
			    
			    Http http = new Http();
			    HTTPResponse res = http.send(req);
			    
			    if (res.getStatusCode() == 200) //@TODO: work out from the HTTPResponse what we actully need for a successful ack - might be the statuscode PLUS the "true" in the body
			    	this.ack = true; 
			}
		}
		catch(Exception e) {
			System.debug('Callout error: '+ e);
            System.debug('StackTrace' + e.getStackTraceString());
		}
		
		this.redirect = true; //Regardless of outcome, we don't want to be stuck on this page
	}
	
	public void updateAccount(){
		if (this.ack == true) {
			//Also update the Account last_refresh_datetime so this can be reflected in the visualisation
			refreshBDAccount.Last_Refresh_Datetime__c = System.now();
			update refreshBDAccount;
		}
	}
	
}