/**
 * Controller extension for the Kissmetrics Person Details VF widget
 * Sets the rendering of the widget so it does not auto-load on page load
 *
 * Author: tailbyr
 * Copyright: EurekaReport.com.au
 */
public with sharing class KissmetricsPersonDetails {

	public Boolean showKM {get; set;}
	
	public KissmetricsPersonDetails(ApexPages.StandardController stdController) {
		this.showKM = false;
	}
	
	public PageReference getKMDetails() {
		this.showKM = true;
		return null;
	}
	
	public static TestMethod void testShowHideKissMetrics(){
		List<Account> accounts = TestAccountHelper.createAccounts(1);
		Account a = accounts[0];
		
		Test.startTest();
		ApexPages.Standardcontroller sc = new ApexPages.StandardController(a);
		KissmetricsPersonDetails kpd = new KissmetricsPersonDetails(sc);
		
		System.assert(!kpd.showKM); //Not visible
		
		kpd.getKMDetails();
		Test.stopTest();
		
		System.assert(kpd.showKM); //Visible
	}
}