/**
* Report to monitor the performance of the Sales year over the calendar year
* Shows statistics for New, Winbacks and LateRenewals for each member of the sales team, tracking against given sales targets
*
* Author: tailbyr
* Copyright: EurekaReport.com.au
*/
public with sharing class SalesPerformanceReport {

	public List<SalesResultWrapper> salesWrapperList { get; set; } 
	
	public Boolean result { get; set; }
	public Boolean displayTargets { get; set; }
	public static Decimal weeklyTargets { get; set; }
	
	public Integer year { get; set; }
	
	public String[] callResultOptions { get; set; }
	public Map<String, Integer> callResultOptionLookup { get; set; }
	
	static{
		AIBM_Settings__c aibmSettings = AIBM_Settings__c.getInstance('Defaults');
		weeklyTargets = aibmSettings.Weekly_Target__c;
	}

	public SalesPerformanceReport() {
		//Default year
		this.year = Date.today().year();
		
		populateSalesResults();
	}

	// Query the Sales Results for the given year integer to populate the sales results collection
	public void populateSalesResults() {
		//Create start of year and end of year dates
		Date startOfYear = Date.newInstance(this.year, 1, 1);
		Date endOfYear = Date.newInstance(this.year, 12, 31);
		
		List<Sales_Result__c> salesResults = [SELECT Id, Week_Ending__c, Actual_Days_Worked__c, Winback__c, Late_Renewal__c, New__c, Total__c, Average_Value__c, 
												Owner.Name, OwnerId, Call_Result__c
												FROM Sales_Result__c
												WHERE Week_Ending__c >= :startOfYear
												AND Week_Ending__c <= :endOfYear
												ORDER BY Owner.Name, Week_Ending__c desc];
		
		this.salesWrapperList = new List<SalesResultWrapper>();
		
		//Populate the call results options from the possible values in the Drop Down
		Schema.Describefieldresult callResultField = Task.Call_Result__c.getDescribe();
		List<Schema.PicklistEntry> callResultVals = callResultField.getPicklistValues();
		this.callResultOptions = new String[]{};
		this.callResultOptionLookup = new Map<String, Integer>();
		
		for (Integer i = 0; i < callResultVals.size(); i++) {
			this.callResultOptions.add(callResultVals[i].getValue());
			this.callResultOptionLookup.put(callResultVals[i].getValue(), i);
		}
		
		//Add the 'No Call Result' option
		this.callResultOptions.add('No Call Result');
		this.callResultOptionLookup.put('No Call Result', callResultOptions.size()-1); //References last position in callResultOptions array
		
		//Hold the value of the owner of the previous salesResult so we know if its the first row of a new Owner (displayed differently)
		Id priorOwner = null;
		
		//Iterate round the Sales Results, create the wrapper objects and assign the correct Call Results
		if (salesResults != null && salesResults.size() > 0) {
			for (Sales_Result__c res : salesResults) {
				
				SalesResultWrapper wrap = new SalesResultWrapper(res);
				
				//Initialise the call result integers - if there are no values for the Sales Result, fill it with zeroes
				wrap.callResultsInt = new Integer[this.callResultOptionLookup.size()];
				
				//Get the Call Results from the JSON field
				if (res.Call_Result__c != null && res.Call_Result__c != '') {
					Map<String, Object> callResults = (Map<String, Object>)JSON.deserializeUntyped(res.Call_Result__c);
					
					for (String key : callResults.keySet()){
						Integer pos = this.callResultOptionLookup.get(key);
						wrap.callResultsInt[pos] = (Integer)callResults.get(key);
					}
					
				}
				
				//Iterate round the created Int array and zero any null values
				for (Integer j = 0; j < wrap.callResultsInt.size(); j++) {
					if (wrap.callResultsInt[j] == null)
						wrap.callResultsInt[j] = 0;
				}
				
				
				if (priorOwner != res.OwnerId) {
					wrap.firstRow = true;
					priorOwner = res.OwnerId;
				}
				else
					wrap.firstRow = false;
				
				this.salesWrapperList.add(wrap);
				
			}
			result = true;
		}
		else
			result = false;
			
	}
	
	public void showTargets(){
		this.displayTargets = true;
	}
	
	public void hideTargets(){
		this.displayTargets = false;
	}
	
	//Save a new value of the Weekly Sales Target to the Custom Setting 
	public void saveTargets(){
		//Save the Weekly Target value to its Custom Setting
		AIBM_Settings__c aibmSettings = AIBM_Settings__c.getInstance('Defaults');
		aibmSettings.Weekly_Target__c = weeklyTargets;
		update aibmSettings;
	}
	
	//Save the Actual Days Worked updated values
	public void saveActualDaysWorked(){
		
		List<Sales_Result__c> saveResults = new List<Sales_Result__c>();
		Boolean containsErrors = false; //For validation - only save if no rows contains error values (e.g. alpha characters)
		
		for (SalesResultWrapper srw : this.salesWrapperList) {
			Sales_Result__c res = srw.result;
			
			//Check that the value entered is a valid number. If not, display the error message and break out from saving
			Pattern isnumbers = Pattern.Compile('^[0-9]+$');
			Matcher daysWorkedMatch = isnumbers.matcher(srw.daysWorked);
			
			if(!daysWorkedMatch.Matches()){
			    //has characters other than numbers
			    srw.daysWorkedValidationStyle = 'color:red;display:inline;font-style: italic;';
			    containsErrors = true;
			}
			else {
				res.Actual_Days_worked__c = Decimal.valueOf(srw.daysWorked);
				saveResults.add(res);
				srw.daysWorkedValidationStyle = 'color:red;display:none;font-style: italic;';
			}
		}
		
		if (containsErrors == false) {
			update saveResults;
			saveResults = null;
		}
		else {
			//Create an Error message for the top of the page so users know there was an error and nothing saved
			ApexPages.Message msgErr = new ApexPages.Message(ApexPages.Severity.ERROR, 'Entries could not be saved. Invalid value in Actual Days Worked. Please scroll down to view invalid rows.');
            ApexPages.addmessage(msgErr);
		}
	}
	
	//Populate the dropdown list for year data
	public List<SelectOption> getYearOptions() {
	    List<SelectOption> options = new List<SelectOption>();
	    Date todayYear = Date.today();
	    for (Integer i = -4; i < 1; i++) {
	    	Date yStart = todayYear.addYears(i);
	    	options.add(new SelectOption(String.valueOf(yStart.year()), String.valueOf(yStart.year())));
	    }
	    return options;
	}
	
	//Download the report into a CSV for use in other reporting
	public PageReference downloadReport() {		
		PageReference exportPage = new PageReference('/apex/Sales_Performance_Report_Export');
		exportPage.setRedirect(false);
		return exportPage;
	}
	
	
	
	/******
	* Wrapper class on the Sales Result object
	* Adds extra fields on the calculated weekly target and the % to target
	*/
	public class SalesResultWrapper {
		public Sales_Result__c result { get; set; }
		public String daysWorked { get; set; }
		public String daysWorkedValidationStyle { get; set; }
		
		public boolean firstRow { get; set; } //Indicates the first row of a Salespersons stats
		
		public Integer[] callResultsInt { get; set; }
		public Integer callResultsTotal { 
			get {
				Integer callResultsTotal = 0;
				if (callResultsInt != null) {
					for (Integer i : callResultsInt)
						callResultsTotal += i;
				}
				return callResultsTotal;
			} 
		}
		
		public Decimal weeklyTarget {
			get {
				return (SalesPerformanceReport.weeklyTargets/5) * this.result.Actual_Days_Worked__c;
			}
			
			set;
		}
		
		public Decimal percentToTarget { 
			get {
				if (this.result.Total__c == null || this.result.Total__c == 0 
					|| this.weeklyTarget == null || this.weeklyTarget == 0)
					return 0.0;
				else
					return (this.result.Total__c / this.weeklyTarget)*100;
			}
			
			set; 
		}
		
		public String getStyle() {
			if (this.percentToTarget < 100.00 && this.percentToTarget > 0)
				return 'background:#FFC7CE; text-align:center;';
			else if (this.percentToTarget >= 100.00)
				return 'background:#C6EFCE; font-weight:bold; text-align:center;';
			else return 'text-align:center;';
		}
		
		public String getFirstRowStyle() {
			if (this.firstRow == true)
				return 'border-top: 1px solid black;height:20px;vertical-align:bottom; border-left: 1px dotted black; border-right: 1px dotted black;';
			else
				return 'border-left: 1px dotted black; border-right: 1px dotted black;';
		}
		
		public SalesResultWrapper(Sales_Result__c result) {
			this.result = result;
			this.daysWorked = String.valueOf(result.Actual_Days_Worked__c);
			this.daysWorkedValidationStyle = 'color:red;display:none;font-style: italic;';
		}
	} 

}