/**
 * UnitTest: 
 *
 * Tests the functionality of the InvestmentAccountVisualisation VF Controller 
 *
 * Author: tailbyr
 * Copyright: EurekaReport.com.au
 */
@IsTest
public with sharing class TestInvestmentAccountVisualisation {

	private static Account createAccount() {
		Account a = new Account();
		a.FirstName = 'test';
		a.LastName = 'tester';
		a.PersonEmail = 'test@testbucket.net';
		a.Phone = '12345678';
		a.BD_Current_Role__c = 'Client';
		return a;
	}
	
	private static Investment_Account__c createInvestmentAccount(Account a, String accountType, String name, String accountNumber) {
		Investment_Account__c invAcc = new Investment_Account__c();
		invAcc.Account__c = a.Id;
		invAcc.Investment_Account_Type__c = accountType;
		invAcc.Name = name;
		invAcc.Account_Number__c = accountNumber;
		invAcc.AssetAllocation__c = createAssetAllocation();
		return invAcc;
	}
	
	private static String createAssetAllocation(){
		return '[{"value":900.00,"percentage":9.00,"asset":"Cash"},{"value":1000.00,"percentage":10.00,"asset":"Equities"},{"value":1100.00,"percentage":11.00,"asset":"Fixed Interest"},{"value":1200.00,"percentage":12.00,"asset":"International Equities"},{"value":1300.00,"percentage":13.00,"asset":"International Fixed Interest"},{"value":1400.00,"percentage":14.00,"asset":"Other"},{"value":2525.00,"percentage":25.25,"asset":"Property"},{"value":1600.00,"percentage":16.00,"asset":"Property Trusts"}]';
	}
	
	private static Tax_Entity__c createTaxEntity(Account a, Investment_Account__c ia, String name, String entityType) {
		Tax_Entity__c tx = new Tax_Entity__c();
		tx.Name = name;
		tx.Entity_Type__c = entityType;
		tx.Account__c = a.Id;
		tx.Investment_Account__c = ia.Id;
		return tx;
	}
	
	private static Investment_Option__c createInvestmentOption(Account a, Investment_Account__c invAcc, Tax_Entity__c tx, String name, String optionType) {
		Investment_Option__c invOpt = new Investment_Option__c();
		invOpt.Name = name;
		invOpt.Account__c = a.Id;
		if (tx != null)
			invOpt.Tax_Entity__c = tx.Id;
		if (invAcc != null)
			invOpt.Investment_Account__c = invAcc.Id;
		invOpt.Option_Type__c = optionType;
		return invOpt;
	}
	

	static TestMethod void testInvestmentAccountStructure() {
		//Create a reasonably complex hierarchy of Investment Account/Tax Entity/Investment Options objects and check they are all retrived correctly
		Account a = createAccount();
		insert a;
		
		/** Create Investment accounts **/
		List<Investment_Account__c> invAccs = new List<Investment_Account__c>();
		for (Integer i = 0; i < 5; i++) {
			Investment_Account__c invAcc = createInvestmentAccount(a, 'SMSF', 'Test SMSF'+i, '000'+i);
			invAccs.add(invAcc);
		}
		//Blank out the Products_Raw field for one account to check this is also handled
		invAccs[4].AssetAllocation__c = null;
		insert invAccs;
		
		/** Create Tax Entities **/
		List<Tax_Entity__c> taxEntities = new List<Tax_Entity__c>();
		//3 tax entities for Investment Account 1
		for (Integer i = 0; i < 3; i++) {
			Tax_Entity__c tx = createTaxEntity(a, invAccs[0], 'SMSF 1 Entity '+i, 'Company');
			taxEntities.add(tx);	
		}
		
		//2 tax entities for Investment Account 2
		for (Integer i = 0; i < 2; i++) {
			Tax_Entity__c tx = createTaxEntity(a, invAccs[1], 'SMSF 2 Entity '+i, 'Company');
			taxEntities.add(tx);	
		}
		
		//1 tax entities for Investment Account 3
		Tax_Entity__c tx = createTaxEntity(a, invAccs[2], 'SMSF 3 Entity 1', 'Company');
		taxEntities.add(tx);	
		
		insert taxEntities;
		
		/** Create Investment Options **/
		List<Investment_Account__c> testInvest = [SELECT Id, Name, 
												(SELECT Id, Name FROM Tax_Entities__r)
												FROM Investment_Account__c
												WHERE Account__c = :a.Id];
		
		List<Investment_Option__c> invOptions = new List<Investment_Option__c>();
		for (Investment_Account__c ia : testInvest) {
			
			//Add an Investment Option under the tax entity without a link to the Investment Account
			for (Tax_Entity__c t : ia.Tax_Entities__r) {
				invOptions.add(createInvestmentOption(a, null, t, 'SMSF Option for TE:' + t.Name, 'Shares'));
			}
			
			//Also add an Investment Option that links to both the Tax Entity and Investment Account
			for (Tax_Entity__c t : ia.Tax_Entities__r) {
				invOptions.add(createInvestmentOption(a, ia, t, 'SMSF Option for full link:' + t.Name + ' ' + ia.Name, 'Bonds'));
			}
			
			//For those Investment Accounts with no Tax Entities, add 4 Investment Options directly underneath
			if (ia.Tax_Entities__r.size() < 1) {
				for (Integer i = 0; i < 4; i++){
					invOptions.add(createInvestmentOption(a, ia, null, 'SMSF Option for IA:' + ia.Name, 'Term Deposits'));
				}
			}
		}
			
		insert invOptions;
		
		//Now update the Last Refresh Date on the Account to ensure that process is working (not testing Inv Accs last modified date - system timestamping)
		Datetime testLastRefresh = System.now().addMinutes(30);
		a.Last_Refresh_Datetime__c = testLastRefresh;
		update a;
		
		/** Test the Visualisation **/
		Test.startTest();
		ApexPages.Standardcontroller sc = new ApexPages.StandardController(a);
		InvestmentAccountVisualisation iav = new InvestmentAccountVisualisation(sc);
		Test.stopTest();
		
		System.assertEquals(iav.visualInvestments.size(), 5);
		List<InvestmentAccountVisualisation.VisualInvestmentAccount> testInvAccs = iav.visualInvestments;
		
		//Test that the Visualisation last refresh datetime is our testLastRefresh
		System.assertEquals(iav.lastUpdated, testLastRefresh);
		
		//Test correct number of Tax Entities
		System.assertEquals(testInvAccs[0].taxEntities.size(), 3);
		System.assertEquals(testInvAccs[1].taxEntities.size(), 2);
		System.assertEquals(testInvAccs[2].taxEntities.size(), 1);
		System.assertEquals(testInvAccs[3].taxEntities.size(), 0);
		System.assertEquals(testInvAccs[4].taxEntities.size(), 0);
		
		//Test correct Investment options linked under the Tax Entities
		for (InvestmentAccountVisualisation.VisualTaxEntity tempTx : testInvAccs[0].taxEntities) {
			System.assertEquals(tempTx.investmentOptions.size(), 2);
		}
		
		//Test correct number of Investment options linked directly to accounts
		System.assertEquals(testInvAccs[0].investmentOptions.size(), 0);
		System.assertEquals(testInvAccs[1].investmentOptions.size(), 0);
		System.assertEquals(testInvAccs[2].investmentOptions.size(), 0);
		System.assertEquals(testInvAccs[3].investmentOptions.size(), 4);
		System.assertEquals(testInvAccs[4].investmentOptions.size(), 4);
		
		//Test the population of the Asset Allocation data and the Pie Chart data
		System.assertEquals(testInvAccs[0].visualAllocationList.size(), 8);
		System.assertEquals(testInvAccs[0].pieData.size(), 8);
		
		//Test the last Investment Account has no asset allocation data
		System.assert(testInvAccs[4].visualAllocationList == null);
		System.assert(testInvAccs[4].pieData == null);
	}

}