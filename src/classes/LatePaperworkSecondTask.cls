/**
* Batch job that runs nightly to pull all Accounts that have signed up to OneVue platform but not returned their paperwork within SECOND NO PAPERWORK TASK days.
* A task for the Sales Team to follow up will be created for each of these customers 
* Author: tailbyr
* Copyright: EurekaReport.com.au
*/
//DEPRECATED
public with sharing class LatePaperworkSecondTask {// implements Schedulable {

    /*public static void setupSchedule(String jobName, String scheduledTime) { 
		LatePaperworkSecondTask scheduled_task = new LatePaperworkSecondTask();
		String payment_id = System.schedule(jobName, scheduledTime, scheduled_task);
	}

	//Schedulable override method: execute - called when the scheduled job is kicked off
	public void execute(SchedulableContext ctx) {
		Database.executeBatch(new LatePaperworkTasks(getNumDays()), 1);
	}
	
	public static Integer getNumDays(){
		BrightDay_Settings__c settings = BrightDay_Settings__c.getInstance();
		
		Integer numDays = 0;
		if (settings.Second_No_Paperwork_Task__c == null) numDays = -26; //Default 2nd task trigger day
		else numDays = settings.Second_No_Paperwork_Task__c.intValue();
		
		return numDays;
	}*/
}