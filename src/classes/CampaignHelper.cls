/**
 * CampaignHelper is a wrapper class for the Campaign object in Salesforce.
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
public with sharing class CampaignHelper {

	Campaign campaign { get; set; }
	Account account { get; set; }
	
	// store previous queried campaigns by default so that it won't appear in other lists
	public Boolean savePreviousCampaigns = true;
	public List<Id> previousIds;
	
	// filter variables by default allow any time of user
	public Boolean allowFilters = false;
	// only one filter can be applied to an account so set all as false initially
	public Boolean newEmailAddress = false;
	public Boolean currentFreeTrial = false;
	public Boolean currentSubscriber = false;
	public Boolean exFreeTrial = false;
	public Boolean exSubscriber = false;
	
	public CampaignHelper() {
		previousIds = new List<Id>();
	}
	
	/**
	 * Another constructor for campaign methods that revolve around a single account
	 */
	public CampaignHelper(Id accountId, Boolean allowFilters) {
		List<Account> accounts = [SELECT Id FROM Account WHERE Id = :accountId LIMIT 1];
		if (!accounts.isEmpty()) {
			this.account = accounts[0];
		}
		// initialise empty list for previous queries
		previousIds = new List<Id>();
		
		// set which campaign filter the account fits into
		this.allowFilters = allowFilters;
		setCampaignFilters();
	}
	
	/**
	 * Identify what type of filter applies to the current account and filter those campaigns in queries
	 * e.g. if account is currently subscribed to free trial, any campaign with the current free trial ticked will be shown
	 */
	public void setCampaignFilters() {
		if (this.account != null) {
			List<Product_Instance__c> currentSubs = [SELECT Id, Subscription_Type__c FROM Product_Instance__c WHERE Person_Account__c =:account.Id AND Start_Date__c <= :Date.today() AND End_Date__c >= :Date.today() LIMIT 1];
			if (!currentSubs.isEmpty()) {
				if (currentSubs[0].Subscription_Type__c != 'f') {
					currentSubscriber = true; //current subscriber
				} else {
					currentFreeTrial = true; //current free trial
				}
			} else {
				List<Product_Instance__c> pastSubs = [SELECT Id, Subscription_Type__c FROM Product_Instance__c WHERE Person_Account__c =:account.Id AND End_Date__c < :Date.today() LIMIT 1];
				if (!pastSubs.isEmpty()) {
					if (pastSubs[0].Subscription_Type__c != 'f') {
						exSubscriber = true; //ex subscriber
					} else {
						exFreeTrial = true; //ex free trial
					}
				} else {
					// If account exists but does not contain any subscriptions, set to newEmailAddress
					newEmailAddress = true; //new email address	
				}
			}
		} else {
			newEmailAddress = true; //new email address
		}
	}
	
	/**
	 * Inputs a list of campaigns and returns only the campaigns that match the filter variables
	 * If none of the filters have been ticked, make the campaign available to anyone by default
	 */
	public List<Campaign> filterCampaigns(List<Campaign> unfiltered) {
		// initialise empty filter list
		List<Campaign> filteredCampaigns = new List<Campaign>();
		for (Campaign campaign : unfiltered) {
			// compare all the filter options for any matches
			if ((campaign.New_Email_Address__c && this.newEmailAddress) ||
				(campaign.Current_Free_Trial__c && this.currentFreeTrial) ||
				(campaign.Current_Subscriber__c && this.currentSubscriber) ||
				(campaign.Ex_Free_Trialer__c && this.exFreeTrial) ||
				(campaign.Ex_Subscriber__c && this.exSubscriber) ||
				(!campaign.New_Email_Address__c && !campaign.Current_Free_Trial__c && !campaign.Current_Subscriber__c && !campaign.Ex_Free_Trialer__c && !campaign.Ex_Subscriber__c)) 
			{
				// add the campaign to the filtered list
				filteredCampaigns.add(campaign);
			}
		}
		return filteredCampaigns;
	}
	
	/**
	 *
	 */
	public static Campaign getCampaignByLotusCode(String code) {
		List<Campaign> campaigns = [SELECT Id, Name FROM Campaign WHERE Eureka_Code__c = :code LIMIT 1];
		if (!campaigns.isEmpty()) {
			return campaigns[0];
		}
		return null;
	}
	
	/**
	 * Lists all campaigns where today's date is between the campaign's start and end date, supplying the 'Order By' field string for dynamic sorting
	 */
	public List<Campaign> getCurrentCampaigns(String orderBy, String direction) {
		return getCurrentCampaigns(new String[]{orderBy + ' ' + direction});
	}
	
	/**
	* Lists all campaigns where today's date is between the campaign's start and end date, supplying a collection of OrderBy crtieria for sorting by multiple fields
	*/
	public List<Campaign> getCurrentCampaigns(String[] orderBy) {
		List<Campaign> campaigns = new List<Campaign>();
		
		/* 20130625 tailbyr: replacing the inline SOQL with dynamic SOQL so that we can set the order by field at runtime
		campaigns = [
			SELECT Id, Name, Affiliate_campaign_name__c, New_Email_Address__c, Current_Free_Trial__c, Current_Subscriber__c, Ex_Free_Trialer__c, Ex_Subscriber__c  
			FROM Campaign 
			WHERE StartDate <= :Date.today() AND 
				EndDate >= :Date.today() AND 
				IsActive = true AND 
				Id NOT IN :previousIds 
			ORDER BY StartDate DESC
		];*/
		if (orderBy==null || orderBy.size()<1)
			orderBy = new String[]{'StartDate desc'};
		String soqlQuery = 'SELECT Id, Name, Affiliate_campaign_name__c, New_Email_Address__c, Current_Free_Trial__c, Current_Subscriber__c, Ex_Free_Trialer__c, Ex_Subscriber__c '  
			+ 'FROM Campaign ' 
			+ 'WHERE StartDate <= ' + DateTime.now().format('yyyy-MM-dd') + ' AND ' 
			+	'EndDate >= ' + DateTime.now().format('yyyy-MM-dd') + ' AND ' 
			+	'IsActive = true ';
			
		//Only apply previousIds filter if there are previousIds to filter on!
		if (previousIds.size()>0)
			soqlQuery += ' AND Id NOT IN :previousIds';
		
		if (orderBy != null && orderBy.size() > 0) {
			soqlQuery += ' ORDER BY ' + orderBy[0];
			if (orderBy.size()>1){
				for (integer i = 1; i<orderBy.size(); i++){
					soqlQuery += ', ' + orderBy[i]; 
				}
			}
		}
		campaigns = Database.query(soqlQuery);
				
		if (allowFilters) {
			campaigns = filterCampaigns(campaigns);
		}
		// store the campaign ids so it won't be shown in other queries
		if (savePreviousCampaigns) {
			previousIds.addAll(getCampaignIds(campaigns));
		}
		return campaigns;
	}
	
	/**
	 * Lists all campaigns where today's date is between the campaign's start and end date
	 */
	public List<Campaign> getCurrentCampaigns() {
		return this.getCurrentCampaigns(new String[]{'StartDate DESC'});
	}
	
	/**
	 * Lists all campaigns where today's date is after the campaign's end date, supplying the 'Order By' field string for dynamic sorting
	 */
	public List<Campaign> getExpiredCampaigns(String orderBy, String direction) {
		return getExpiredCampaigns(new String[]{orderBy + ' ' + direction});
	}
	
	/**
	 * Lists all campaigns where today's date is after the campaign's end date, supplying the 'Order By' field string for dynamic sorting
	 */
	public List<Campaign> getExpiredCampaigns(String[] orderBy) {
		List<Campaign> campaigns = new List<Campaign>();
		/* 20130625 tailbyr: replacing the inline SOQL with dynamic SOQL so that we can set the order by field at runtime
		campaigns = [
			SELECT Id, Name, Affiliate_campaign_name__c, New_Email_Address__c, Current_Free_Trial__c, Current_Subscriber__c, Ex_Free_Trialer__c, Ex_Subscriber__c 
			FROM Campaign 
			WHERE EndDate < :Date.today() AND 
				IsActive = true AND 
				Id NOT IN :previousIds 
			ORDER BY StartDate DESC
		];*/
		
		if (orderBy==null || orderBy.size()<1)
			orderBy = new String[]{'StartDate desc'};
		String soqlQuery = 'SELECT Id, Name, Affiliate_campaign_name__c, New_Email_Address__c, Current_Free_Trial__c, Current_Subscriber__c, Ex_Free_Trialer__c, Ex_Subscriber__c ' 
			+ 'FROM Campaign ' 
			+ 'WHERE EndDate < ' + DateTime.now().format('yyyy-MM-dd') + ' AND ' 
			+	'IsActive = true ';
			
		if (previousIds!=null || previousIds.size()>0)
			soqlQuery += 'AND Id NOT IN :previousIds '; 
			
		if (orderBy != null && orderBy.size() > 0) {
			soqlQuery += ' ORDER BY ' + orderBy[0];
			if (orderBy.size()>1){
				for (integer i = 1; i<orderBy.size(); i++){
					soqlQuery += ', ' + orderBy[i]; 
				}
			}
		}
		campaigns = Database.query(soqlQuery);
		
		if (allowFilters) {
			campaigns = filterCampaigns(campaigns);
		}
		// store the campaign ids so it won't be shown in other queries
		if (savePreviousCampaigns) {
			previousIds.addAll(getCampaignIds(campaigns));
		}
		return campaigns;
	}
	
	/**
	 * Lists all campaigns where today's date is after the campaign's end date
	 */
	public List<Campaign> getExpiredCampaigns() {
		return this.getExpiredCampaigns(new String[]{'StartDate DESC'});
	}
	
	/**
	 * List all campaigns where the account has been affiliated with it and start and end date is in between today
	 */
	public List<Campaign> getCurrentAccountCampaigns() {
		List<Campaign> campaigns = new List<Campaign>();
        // query for all the campaigns that this account is a member of
        List<Contact> contacts = [SELECT Id FROM Contact WHERE AccountId = :this.account.Id LIMIT 1];
        if (!contacts.isEmpty()) {
            campaigns = [
            	SELECT Id, Name, Affiliate_campaign_name__c, New_Email_Address__c, Current_Free_Trial__c, Current_Subscriber__c, Ex_Free_Trialer__c, Ex_Subscriber__c 
            	FROM Campaign 
            	WHERE IsActive = true AND
            		Id NOT IN :previousIds AND 
            		Id IN (
            			SELECT CampaignId 
            			FROM CampaignMember 
            			WHERE ContactId = :contacts[0].Id
            		) 
            	ORDER BY StartDate DESC
            ];
            
            if (allowFilters) {
				campaigns = filterCampaigns(campaigns);
			}
            // store the campaign ids so it won't be shown in other queries
			if (savePreviousCampaigns) {
				previousIds.addAll(getCampaignIds(campaigns));
			}
        }
        return campaigns;
	}
	
	/**
	 * List all campaigns where the account has been affiliated with it and end is in the past
	 */
	public List<Campaign> getExpiredAccountCampaigns() {
		List<Campaign> campaigns = new List<Campaign>();
        // query for all the campaigns that this account is a member of
    	campaigns = [
        	SELECT Id, Name, Affiliate_campaign_name__c, New_Email_Address__c, Current_Free_Trial__c, Current_Subscriber__c, Ex_Free_Trialer__c, Ex_Subscriber__c 
        	FROM Campaign 
        	WHERE IsActive = true AND 
        		Id NOT IN :previousIds AND 
        		Id IN (
        			SELECT Campaign__c 
        			FROM Product_Instance__c 
        			WHERE Person_Account__c = :this.account.Id AND 
        				Campaign__c != null AND 
        				End_Date__c < :Date.today()
        		)
        	ORDER BY StartDate DESC
        ];
        
        if (allowFilters) {
			campaigns = filterCampaigns(campaigns);
		}
        // store the campaign ids so it won't be shown in other queries
		if (savePreviousCampaigns) {
			previousIds.addAll(getCampaignIds(campaigns));
		}
        return campaigns;
	}
	
	/**
     * Retrieve a list of ids from the list of campaigns in the parameter
     */
    public List<Id> getCampaignIds(List<Campaign> campaignList) {
    	List<Id> ids = new List<Id>();
    	for (Campaign campaign : campaignList) {
    		ids.add(campaign.Id);
    	}
    	return ids;
    }
	
	/**
     * Retrieve a group of campaign options to use with a select list 
     */
    public List<SelectOption> getCampaignSelectOptions(String groupName, List<Campaign> campaignList) {
    	List<SelectOption> options = new List<SelectOption>();
    	if (!campaignList.isEmpty()) {
        	options.add(new SelectOption('group', groupName, true));
	        for (Campaign campaign : campaignList) {
	        	if (campaign.Affiliate_campaign_name__c != null) {
	            	options.add(new SelectOption(campaign.Id, campaign.Affiliate_campaign_name__c));
	        	} else {
	        		options.add(new SelectOption(campaign.Id, campaign.Name));
	        	}
	        }
        }
        return options;
    }
	
	/************************************************************************************************/
	/* Test Units */
	
	static RecordType getRecordType()
	{
		// Find any person account record type
	    RecordType recordType = [ select Id, Name, DeveloperName from RecordType where SObjectType = 'Account' and IsPersonType = true limit 1 ];
	    return recordType;
	}
    
    static Account getTestAccount() {
    	Account account = new Account(FirstName='Test', LastName='Tester', RecordTypeId='01290000000NnFN', PersonEmail='test@test.com.test');
    	insert account;
    	return account;
    }
	
	@istest
	static void testCampaignByCode() {
		Campaign campaign = new Campaign();
  		campaign.Name = 'campaign';
  		campaign.Eureka_Code__c = 'LOTUSCODE';
  		insert campaign;
  		
  		Campaign c = CampaignHelper.getCampaignByLotusCode(campaign.Eureka_Code__c);
  		
  		System.assertEquals(c.Id, campaign.Id);
	}
	
	@istest
	static void testGetCampaignIds() {
		Campaign campaign = new Campaign(Name='campaign');
		insert campaign;
		
		CampaignHelper helper = new CampaignHelper();
		List<Id> ids = helper.getCampaignIds(new List<Campaign>{campaign});
		
		// checks that the list contains the campaign id
		System.assertEquals(campaign.Id, ids[0]);
	}
	
	@istest
	static void testGetCurrentCampaigns() {
		Campaign campaign = new Campaign(Name='campaign',StartDate=Date.today().addDays(-7),EndDate=Date.today().addDays(7),IsActive=true);
		insert campaign;
		
		CampaignHelper helper = new CampaignHelper();
		List<Campaign> current = helper.getCurrentCampaigns();
		
		// checks that campaign in the list is current
		System.assertEquals(campaign.Id, current[0].Id);
	}
	
	@istest
	static void testGetCurrentCampaignsSorted() {
		Campaign a_campaign = new Campaign(Name='A campaign',StartDate=Date.today().addDays(-7),EndDate=Date.today().addDays(7),IsActive=true);
		insert a_campaign;
		Campaign b_campaign = new Campaign(Name='B campaign',StartDate=Date.today().addDays(-7),EndDate=Date.today().addDays(7),IsActive=true);
		insert b_campaign;
		
		CampaignHelper helper = new CampaignHelper();
		List<Campaign> current = helper.getCurrentCampaigns('Name', 'desc');
		
		// checks that campaign in the list is current and ordered correctly
		System.assertEquals(a_campaign.Id, current[1].Id);
		System.assertEquals(b_campaign.Id, current[0].Id);
	}
	
	/* Where no Order By args are given, all campaign results should be sorted by startdate desc */
	@istest
	static void testGetCurrentCampaignsSortedByDefault() {
		Campaign a_campaign = new Campaign(Name='A campaign',StartDate=Date.today().addDays(-7),EndDate=Date.today().addDays(7),IsActive=true);
		insert a_campaign;
		Campaign b_campaign = new Campaign(Name='B campaign',StartDate=Date.today().addDays(-1),EndDate=Date.today().addDays(7),IsActive=true);
		insert b_campaign;
		
		CampaignHelper helper = new CampaignHelper();
		List<Campaign> current = helper.getCurrentCampaigns(new String[]{});
		
		// checks that campaign in the list is current and ordered correctly
		System.assertEquals(a_campaign.Id, current[1].Id);
		System.assertEquals(b_campaign.Id, current[0].Id);
	}
	
	/* Where multiple orderby params are given, should be sorted in that order */
	@istest
	static void testGetCurrentCampaignsSortedByMultipleParams() {
		Campaign a_campaign = new Campaign(Name='A campaign',StartDate=Date.today().addDays(-7),EndDate=Date.today().addDays(7),IsActive=true);
		insert a_campaign;
		Campaign b_campaign = new Campaign(Name='B campaign',StartDate=Date.today().addDays(-1),EndDate=Date.today().addDays(7),IsActive=true);
		insert b_campaign;
		Campaign c_campaign = new Campaign(Name='C campaign',StartDate=Date.today().addDays(-1),EndDate=Date.today().addDays(7),IsActive=true);
		insert c_campaign;
		
		CampaignHelper helper = new CampaignHelper();
		List<Campaign> current = helper.getCurrentCampaigns(new String[]{'StartDate desc', 'Name asc'});
		
		// checks that campaign in the list is current and ordered correctly
		System.assertEquals(b_campaign.Id, current[0].Id);
		System.assertEquals(c_campaign.Id, current[1].Id);
		System.assertEquals(a_campaign.Id, current[2].Id);
	}
	
	@istest
	static void testGetExpiredCampaigns() {
		Campaign campaign = new Campaign(Name='campaign',StartDate=Date.today().addDays(-7),EndDate=Date.today().addDays(-1),IsActive=true,Current_Subscriber__c=true);
		insert campaign;
		Account account = getTestAccount();
		Product_Instance__c product = new Product_Instance__c(Campaign__c=campaign.Id,Person_Account__c=account.Id,Subscription_Type__c='y',Start_Date__c=Date.today(),End_Date__c=Date.today().addDays(7));
		insert product;
		
		CampaignHelper helper = new CampaignHelper(account.Id, true);
		List<Campaign> expired = helper.getExpiredCampaigns();
		
		// checks that campaign in the list is expired
		System.assertEquals(campaign.Id, expired[0].Id);
	}
	
	/* Where no Order By args are given, all expired campaign results should be sorted by startdate desc */
	@istest
	static void testGetExpiredCampaignsSortedByDefault() {
		Campaign a_campaign = new Campaign(Name='A campaign',StartDate=Date.today().addDays(-14),EndDate=Date.today().addDays(-7),IsActive=true);
		insert a_campaign;
		Campaign b_campaign = new Campaign(Name='B campaign',StartDate=Date.today().addDays(-7),EndDate=Date.today().addDays(-1),IsActive=true);
		insert b_campaign;
		
		CampaignHelper helper = new CampaignHelper();
		List<Campaign> current = helper.getExpiredCampaigns(new String[]{});
		
		// checks that campaign in the list is current and ordered correctly
		System.assertEquals(b_campaign.Id, current[0].Id);
		System.assertEquals(a_campaign.Id, current[1].Id);
	}
	
	/* Where multiple orderby params are given, expired campaigns should be sorted in that order */
	@istest
	static void testGetExpiredCampaignsSortedByMultipleParams() {
		Campaign a_campaign = new Campaign(Name='A campaign',StartDate=Date.today().addDays(-14),EndDate=Date.today().addDays(-7),IsActive=true);
		insert a_campaign;
		Campaign b_campaign = new Campaign(Name='B campaign',StartDate=Date.today().addDays(-7),EndDate=Date.today().addDays(-1),IsActive=true);
		insert b_campaign;
		Campaign c_campaign = new Campaign(Name='C campaign',StartDate=Date.today().addDays(-7),EndDate=Date.today().addDays(-1),IsActive=true);
		insert c_campaign;
		
		CampaignHelper helper = new CampaignHelper();
		List<Campaign> current = helper.getExpiredCampaigns(new String[]{'StartDate desc', 'Name asc'});
		
		// checks that campaign in the list is current and ordered correctly
		System.assertEquals(b_campaign.Id, current[0].Id);
		System.assertEquals(c_campaign.Id, current[1].Id);
		System.assertEquals(a_campaign.Id, current[2].Id);
	}
	
	@istest
	static void testGetExpiredCampaignsSorted() {
		Campaign a_campaign = new Campaign(Name='A campaign',StartDate=Date.today().addDays(-14),EndDate=Date.today().addDays(-7),IsActive=true);
		insert a_campaign;
		Campaign b_campaign = new Campaign(Name='B campaign',StartDate=Date.today().addDays(-14),EndDate=Date.today().addDays(-7),IsActive=true);
		insert b_campaign;
		
		CampaignHelper helper = new CampaignHelper();
		List<Campaign> current = helper.getExpiredCampaigns('Name', 'desc');
		
		// checks that campaign in the list is current and ordered correctly
		System.assertEquals(a_campaign.Id, current[1].Id);
		System.assertEquals(b_campaign.Id, current[0].Id);
	}
	
	@istest
	static void testGetCurrentAccountCampaigns() {
		Campaign campaign = new Campaign(Name='campaign',StartDate=Date.today().addDays(-7),EndDate=Date.today().addDays(7),IsActive=true,Current_Subscriber__c=true);
		insert campaign;
		Account account = getTestAccount();
		Product_Instance__c product = new Product_Instance__c(Campaign__c=campaign.Id,Person_Account__c=account.Id,Subscription_Type__c='y',Start_Date__c=Date.today(),End_Date__c=Date.today().addDays(7));
		insert product;
		
		CampaignHelper helper = new CampaignHelper(account.Id, true);
		List<Campaign> currentAccount = helper.getCurrentAccountCampaigns();
		
		// checks that campaign in the list is current and is assigned to one of the account's product instances
		System.assertEquals(campaign.Id, currentAccount[0].Id);
	}
	
	@istest
	static void testGetExpiredAccountCampaigns() {
		Campaign campaign = new Campaign(Name='campaign',StartDate=Date.today().addDays(-7),EndDate=Date.today().addDays(-1),IsActive=true,Ex_Subscriber__c=true);
		insert campaign;
		Account account = getTestAccount();
		Product_Instance__c product = new Product_Instance__c(Campaign__c=campaign.Id,Person_Account__c=account.Id,Subscription_Type__c='y',Start_Date__c=Date.today().addDays(-7),End_Date__c=Date.today().addDays(-1));
		insert product;
		
		CampaignHelper helper = new CampaignHelper(account.Id, true);
		List<Campaign> expiredAccount = helper.getExpiredAccountCampaigns();
		
		// checks that campaign in the list is expired and is assigned to one of the account's product instances
		System.assertEquals(campaign.Id, expiredAccount[0].Id);
	}
	
	@istest
	static void testSetCampaignFiltersForCurrentSubscriber() {
		Account account = getTestAccount();
		Product_Instance__c product = new Product_Instance__c(Person_Account__c=account.Id,Subscription_Type__c='y',Start_Date__c=Date.today(),End_Date__c=Date.today().addDays(7));
		insert product;
		
		CampaignHelper helper = new CampaignHelper(account.Id, true);
		
		// checks that the account attached current on a paid subscription
		System.assertEquals(true, helper.currentSubscriber);
	}
	
	@istest
	static void testSetCampaignFiltersForExSubscriber() {
		Account account = getTestAccount();
		Product_Instance__c product = new Product_Instance__c(Person_Account__c=account.Id,Subscription_Type__c='y',Start_Date__c=Date.today().addDays(-7),End_Date__c=Date.today().addDays(-1));
		insert product;
		
		CampaignHelper helper = new CampaignHelper(account.Id, true);
		
		// checks that the account attached previously had a paid subscription
		System.assertEquals(true, helper.exSubscriber);
	}
	
	@istest
	static void testSetCampaignFiltersForCurrentFreeTrials() {
		Account account = getTestAccount();
		Product_Instance__c product = new Product_Instance__c(Person_Account__c=account.Id,Subscription_Type__c='f',Start_Date__c=Date.today(),End_Date__c=Date.today().addDays(7));
		insert product;
		
		CampaignHelper helper = new CampaignHelper(account.Id, true);
		
		// checks that the account attached is currently on a free trial
		System.assertEquals(true, helper.currentFreeTrial);
	}
	
	@istest
	static void testSetCampaignFiltersForExFreeTrials() {
		Account account = getTestAccount();
		Product_Instance__c product = new Product_Instance__c(Person_Account__c=account.Id,Subscription_Type__c='f',Start_Date__c=Date.today().addDays(-7),End_Date__c=Date.today().addDays(-1));
		insert product;
		
		CampaignHelper helper = new CampaignHelper(account.Id, true);
		
		// checks that the account attached previously had a free trial
		System.assertEquals(true, helper.exFreeTrial);
	}
	
	@istest 
	static void testSetCampaignFiltersForNewEmailAddress() {
		CampaignHelper helper = new CampaignHelper();
		helper.setCampaignFilters();
		
		// checks that there is no existing account
		System.assertEquals(true, helper.newEmailAddress);
	}
	
	@istest
	static void testSetCampaignFiltersForAccountWithNoSubs() {
		Account account = getTestAccount();
		
		CampaignHelper helper = new CampaignHelper(account.Id, true);
		
		// checks that the account attached previously had a free trial
		System.assertEquals(true, helper.newEmailAddress);
	}
}