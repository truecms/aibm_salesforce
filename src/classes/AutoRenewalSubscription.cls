/**
 * AutoRenewalSubscription
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
global class AutoRenewalSubscription implements Schedulable {
	
	public static String lastAccountManager { get; set; }
	
	public String autoRenewExpiryQueue { get; set; }
	public Boolean autoRenewAllocationRules { get; set; }
	
	public AutoRenewalSubscription() 
	{
		// Get the Account Manager/Owner of the last Credit Card Expiry task so that we can assign the tasks equally
		List<Task> lastCCExpiryTask = [SELECT Id, ownerId FROM Task WHERE Subject LIKE :TaskManager.SUBSCRIPTION_EXPIRY_SUBJECT + '%' ORDER BY createdDate desc LIMIT 1];
		if (lastCCExpiryTask.size()>0)
			lastAccountManager = lastCCExpiryTask[0].ownerId;
			
		//Get the customer settings allocation rule and queue for this automated task creator
		Allocation_Rules__c allocationRules = Allocation_Rules__c.getInstance('Default');
		autoRenewAllocationRules = allocationRules.Not_Renewed_Allocation_Rule__c;
		autoRenewExpiryQueue = allocationRules.Not_Renewed_Queue__c;
	}
	
	global void execute(SchedulableContext ctx) {
		ProductInstanceHelper helper = new ProductInstanceHelper();
		List<AIBM_Settings__c> settings = AIBM_Settings__c.getAll().values();
		
		Boolean autorenewal = true;
		if (!settings.isEmpty()) {
			autorenewal = Boolean.valueOf(settings[0].Auto_Renewal__c);
		}
		
		/* For brightday, if the Account is a Premium Client, Platinum or Black and due for renewal, we want to give them a free renewed sub */
        List<Product_Instance__c> brightdayRenewals = helper.getBrightDayExpiries();
        NewBrightdaySub bdSubBatch = new NewBrightdaySub(brightdayRenewals);
        ID bdBatchId = Database.executeBatch(bdSubBatch, 10); //Run with batches of 10, execute methods should be made to handle multiples
        /* */
		
		if (autorenewal) {
			List<Product_Instance__c> subscriptions = helper.getAutoRenewals();
			
			ProductInstanceHelper scriptBatch = new ProductInstanceHelper(subscriptions);
			scriptBatch.setTaskAllocationRules(autoRenewAllocationRules);
			scriptBatch.setTaskMgr(new TaskManager(autoRenewExpiryQueue));
			ID batchprocessid = Database.executeBatch(scriptBatch, 1); // execute one at a time
		}
    	
    	// delete old log entries so they don't consume all our storage space.
		Datetime d = system.today().addDays(-30);
		List<go1__PaymentLog__c> log = [SELECT Id FROM go1__PaymentLog__c WHERE go1__Time__c <= :d LIMIT 1000];
		delete log;
	}
	
	
	/**
	 *
	 */
	global static void start(String jobName, String scheduledTime) {
		//'0 0 * * * ?' calls function every hour
  		AutoRenewalSubscription renewalSchedule = new AutoRenewalSubscription();
  		String job_id = System.schedule(jobName, scheduledTime, renewalSchedule);
	}
	
    /**
	 *
	 */
	global static void startSchedules() {
		AutoRenewalSubscription.start('Auto Renewals', '0 0 3 * * ?');
		go1.SchedulePayment.start('Monthly Payments', '0 0 4 * * ?');
	}
}