/**
 * The NewSubscriberWizard - is a form that combines all the actions that
 * customer service staff require to signup new subscribers in the one form.
 *
 * Creates records for:
 *   - Account
 *   - CampaignMember
 *   - CreditCard
 *   - Transaction
 *   - Recurring payment schedule.
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
public class NewSubscriberWizard {
    
    final String NEW_ACCOUNT = 'new';
    final String EXISTING_ACCOUNT = 'existing';
    
    
    public String status = '';
    public Boolean failed = false;
    public Boolean showForm { get; public set; }
    
    // objects
    public Account account { get; public set; }
    public Product_Instance__c product { get; public set; }
    public Subscription_Product__c subscription { get; public set; }
    public Campaign campaign { get; public set; }
    
    // PaymentWrapper is a wrapper for the creditcard/transaction/recurring objects
    // we should really just have the one object here.
    public go1.PaymentWrapper payment  { get; set; }
	public go1__CreditCardAccount__c creditcard {
		get {
			return payment.creditcard;
		}
		set {
			payment.creditcard = value;
		}
	}
    public go1__Transaction__c trans { get; public set; }
    public go1__Recurring_Instance__c recurring { get; public set; }
    
	// -------------------------------------------------------------
	// Variables we may be able to remove
	public Boolean isExceptionTest = false;
	public Boolean isCalloutExceptionTest = false;
    public List<go1__Go2DebitSettings__c> settings { get; public set; }
    public Boolean payNow = false;    
    // -------------------------------------------------------------
    
    //Custom Settings (for minimum subscription price)
    public AIBM_Settings__c aibmSettings {get; public set; }
    
    // variables
    public String accountType { get; public set; }
    
    public String campaignId { get; public set; }
    public String duration { get; public set; }
    
    public Boolean isFree { get; public set; }
    public Decimal totalAmount { get; public set; }
    
    public Decimal gstDeduction { get; public set; }
    public Boolean originallyGSTExempt { get; set; }
    
    public class PaymentException extends Exception {}
    
    /**
     * Constructor
     */
    public NewSubscriberWizard() {
    	
    	payment = new go1.PaymentWrapper();
		payment.creditcard = new go1__CreditCardAccount__c();
    	
        // initialise all the objects and classes
        account = new Account();
        product = new Product_Instance__c();
        //subscription = new Subscription_Product__c();
        campaign = new Campaign();
		
        trans = new go1__Transaction__c();
        recurring = new go1__Recurring_Instance__c();

		// -------------------------------------------------------------
		// Variables we may be able to remove
        this.payNow = this.isPayNow();
        settings = go1__Go2DebitSettings__c.getAll().values();
        //this.creditcard = new go1__CreditCardAccount__c();
		// -------------------------------------------------------------        

		//Initialise the settings
		aibmSettings = AIBM_Settings__c.getInstance('Defaults');

        // initialise all the variables
        this.showForm = true;
        this.accountType = EXISTING_ACCOUNT;
        this.campaignId = '';
        this.isFree = true;
        
        this.product.Amount__c = 0;
        this.trans.go1__Amount__c = 0;
        this.totalAmount = 0;
        this.product.Discount__c = 0;
        this.gstDeduction = 0;
        
        
        if (ApexPages.currentPage() != null) {
        	String account_parameter = ApexPages.currentPage().getParameters().get('account');
        	if (account_parameter != null) {
        		setInitialAccountVariables(account_parameter);
        	}
        }
    }
    
    /**
     * Set variable to match the account if the url includes an account id
     */
    public void setInitialAccountVariables(String account_parameter) {
    	if (account_parameter != null) {
            // check the account exists
            Account[] accounts = [SELECT Id FROM Account WHERE Id = :account_parameter];
            if (!accounts.isEmpty()) {
                this.product.Person_Account__c = account_parameter;
                loadAccount();
            }
        }
    }
    
    /**************************************************/
    
    public Account getAccount() {
        return this.account;
    }
    
    public Product_Instance__c getProduct() {
        return this.product;
    }
    
    public Subscription_Product__c getSubscription() {
        return this.subscription;
    }
    
    public Campaign getCampaign() {
        return this.campaign;
    }

    public go1__Transaction__c getTransaction() {
        return this.trans;
    }
    
    public go1__Recurring_Instance__c getRecurring() {
        return this.recurring;
    }
    
    /***************************************************/
    /* List Methods */
    
    // return a list of payment intervals
    public List<SelectOption> getPaymentTypes() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('m', 'Monthly'));
        options.add(new SelectOption('y', 'Yearly'));
        return options;
    }
    
    // return a list of birth years 
    public List<SelectOption> getBirthYears() {
        DateTime d = System.now();
        Integer year = (Integer)d.year();
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--Select Birth Year--'));
        for (Integer i = 0; i < 100; i++) {
            String year_str = string.valueOf(year - i);
            options.add(new SelectOption(year_str, year_str));
        }
        return options;
    }
    
    // return a list of campaigns sorted by four different groups
    public List<SelectOption> getNewCampaigns() {
        CampaignHelper campaignHelper;
        if (this.product.Person_Account__c != null) {
            campaignHelper = new CampaignHelper(this.product.Person_Account__c, true);
        } else {
            campaignHelper = new CampaignHelper();
            campaignHelper.allowFilters = true;
        	campaignHelper.setCampaignFilters();
        }
        
        List<Campaign> campaigns = new List<Campaign>();
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'None'));
        
        // query all the current campaigns for the selected user
        if (this.product.Person_Account__c != null) {
	        campaigns = campaignHelper.getCurrentAccountCampaigns();
	        options.addAll(campaignHelper.getCampaignSelectOptions('--Active/Current Member Campaigns--', campaigns));
        }
        
        // query all campaigns that the selected user used to be a campaign
        if (this.product.Person_Account__c != null) {
	        campaigns = campaignHelper.getExpiredAccountCampaigns();
	        options.addAll(campaignHelper.getCampaignSelectOptions('--Previous Member Campaigns--', campaigns));
        }
        
        // query all the active campaigns that is not in the previous two lists
        campaigns = campaignHelper.getCurrentCampaigns(new String[]{'StartDate desc', 'Affiliate_campaign_name__c asc'});
        options.addAll(campaignHelper.getCampaignSelectOptions('--All Current Campaigns--', campaigns));

        //query all past campaigns
        campaigns = campaignHelper.getExpiredCampaigns(new String[]{'StartDate desc', 'Affiliate_campaign_name__c asc'});
        options.addAll(campaignHelper.getCampaignSelectOptions('--Recent Campaigns--', campaigns));

        return options;
    }
    
    /***************************************************/
    /* AJAX & Redirect Methods */
    
    /**
     * Fired when the search button is clicked
     */
    public PageReference campaignInfo() {
    	if (this.campaignId != null && this.campaignId != '') {
	        PageReference page = new PageReference('/'+this.campaignId);
	        page.setRedirect(true);
	        return page;
    	} else {
    		return null;
    	}
    }
    
    /**
     * AJAX method for changing the discount amount
     */
    public PageReference changeDiscount() {
        // update the discount field
        updateDiscount();
        calculateGST(); //If GST exempt, the GST amount will need to be recalculated
        return null;
    }
    
    /**
     * AJAX method for calculating creditcard fees
     */
    public PageReference calculatePrice() {
        if (trans.go1__Amount__c != null) {
        	// remove any commas from the amount
            if (String.valueOf(this.totalAmount).contains(',')) {
                this.totalAmount = Double.valueOf(String.valueOf(this.totalAmount).replaceAll(',', ''));
            }
            
            // update the discount field
            this.product.Amount__c = this.trans.go1__Amount__c;
            updateDiscount();
            calculateGST(this.trans.go1__Amount__c); //Also check if GST needs to be recalculated on the new value
        } else {
            this.totalAmount = 0;
        }
        return null;
    }
    
    public PageReference calculateGST() {
    	return this.calculateGST(null);
    }
    
    public PageReference calculateGST(Decimal amount) {
    	//If we have GST Exemption and its a yearly payment, remove 1/11th of the price after discount, PROVIDING THE VALUE DOES NOT DROP BELOW MIN THRESHOLD (50% of market value)
        if (this.account.GST_Exempt__c == true && this.subscription.Subscription_Type__c == 'y') {
        	amount = (amount == null) ? this.product.Amount__c : amount; 
         	Decimal gstTotal = (1.0/11.0) * amount;
           	this.gstDeduction = gstTotal;
        }
        else
        	this.gstDeduction = 0.0;
        return null;
    }
    
    /**
     * AJAX method for query any campaign offers that are attached to the current person account
     */
    public PageReference loadAccount() {
    	
    	if (this.product.Person_Account__c != null) {
    		AccountHelper helper = AccountHelper.loadAccountHelper(this.product.Person_Account__c);
    		if (helper.account != null) {
            	account = helper.account;
    		}
        }
        calculateGST();
        //Set the original value of the GST exemption so we can test whether we need to update the Account or not
        originallyGSTExempt = account.gst_exempt__c;
        return null;
    }
    
    /**
     * AJAX method for retriving info about the subscription
     */
    public PageReference retrieveProduct() {
        Subscription_Product__c[] subscriptions = [ Select Id, Price__c, Duration__c, Duration_units__c, Promotional_Description__c, Subscription_Type__c, Subscription_Stream__c From Subscription_Product__c Where Id = :product.Subscription_Product__c LIMIT 1];
        if (!subscriptions.isEmpty()) {
            this.subscription = subscriptions[0];
            if (this.subscription.Price__c == null || this.subscription.Price__c == 0) {
                this.subscription.Price__c = 0;
                this.isFree = true;
            } else {
                this.isFree = false;
            }
            if (this.subscription.Subscription_Type__c == 'f') {
				this.product.Auto_Renewal__c = false;
            } else {
            	this.product.Auto_Renewal__c = true;
            }
            this.product.Amount__c = this.subscription.Price__c;
            this.totalAmount = this.subscription.Price__c;
            
            if (this.product.Amount__c != null && this.product.Amount__c != 0) {
                updateDiscount();
                calculateGST();
            }
        }
        return null;
    }
    
    /***************************************************/
    /* Main Process */
    
    /**
     * The main submit method for calling all the method components
     */
    public PageReference submitForm() {
    	Boolean signupProcessed = false;
    	ApexPages.Severity errorStatus = ApexPages.Severity.WARNING;
    	
    	//Can we check if the wizard already has an error status and failed boolean? If so, don't check the below
    	if (this.failed == null || !this.failed || this.status == null || this.status.length() < 1) 
    	{
    		// check that new account does not have an existing email, return error message if true
        	List<Account> sameEmailAccounts = [SELECT Id FROM Account WHERE PersonEmail = :this.Account.PersonEmail];
        	if (accountType == NEW_ACCOUNT && !sameEmailAccounts.isEmpty()) {
	        	ApexPages.Message msgErr = new ApexPages.Message(ApexPages.Severity.WARNING, 'There is already a subscriber with the email \''+ String.valueOf(this.Account.PersonEmail) +'\'. Please use a different email or search for an existing account!');
            	ApexPages.addmessage(msgErr);
            	return null;
        	}
    	}
        // check if the subscription is $0.00, return error message if true
        if (this.isFree && this.product.Amount__c > 0) {
            ApexPages.Message msgErr = new ApexPages.Message(ApexPages.Severity.WARNING, 'Selected product requires payment!');
            ApexPages.addmessage(msgErr);
            return null;
        }
        // If the user has managed to click submit without the onChange event firing on the product, throw an error and get them to reselect the product
		if (this.subscription == null) {
			ApexPages.Message msgErr = new ApexPages.Message(ApexPages.Severity.WARNING, 'The Product has not been loaded. Please reselect the Subscription Product in the Product Lookup field and resubmit.');
            ApexPages.addmessage(msgErr);
            return null;
		}
        
        // include the surcharge prices into the total amount
        this.creditcard = this.payment.creditcard;
        updateCreditCardSurcharges();
        
        this.showForm = true;
        // ensure all form sections are successful 
        try {
            Boolean save = false;
            go1__Payment_Gateway__c gateway = null;
            if (this.isFree) {
            	save = true;
            }
            else {
            	System.debug('DEBUG: CreditCard:' + this.payment.creditcard);
            	gateway = go1.Paymethod.loadGateway(payment.creditcard.go1__Payment_Gateway__c);
            	if (gateway.go1__Payment_Type__c == 'Manual') {
            		payment.setDefaultGateway(gateway);
            		this.trans = payment.processPayment(account, this.totalAmount, 'AUD');
            		this.trans.go1__Payment_Name__c = gateway.name;
            		this.trans.go1__Account__c = this.account.id;
            		this.trans.go1__Account_ID__c = gateway.name;
            		this.trans.go1__Payment_Name__c = gateway.name;
            		this.trans.Payment_Method__c = gateway.name;
            		save = true;
            	}
            	else {
            		 
            		if (this.payment.existing_creditcard != null) {
						this.creditcard = this.payment.existing_creditcard;            				
            		}
            		
            		if (this.creditcard == null && gateway.go1__Payment_Type__c == 'CreditCard' && go1.PaymentHelper.CCValidation(this.payment.creditcard.go1__Token__c)) {
            			this.status = 'Credit Card is Invaild.';
            			this.failed = true;
            		}
            		else if (this.totalAmount < aibmSettings.ER_Minimum_Sub_Price__c && this.subscription.Subscription_Type__c == 'y') {
            			//Throw error - price cannot go this low through GST
           				ApexPages.Message msgErr = new ApexPages.Message(ApexPages.Severity.WARNING, 'The price of this subscription cannot go below the minimum value of $' + aibmSettings.ER_Minimum_Sub_Price__c + '. We cannot offer GST Exemption below this value.');
           				ApexPages.addmessage(msgErr);
           				return null;
            		}
            		else {
            			this.trans = payment.processPayment(this.creditcard, this.totalAmount, 'AUD');
            			
            			//To facilitate testing, we need to 'manually' fail transactions that SHOULD fail under Unit Testing (due to hardcoding within 'Securepay.cls' to always return true in unit testing)
            			if (Test.isRunningTest())
            			{
            				String strAmountVal = this.totalAmount.toPlainString().substringAfter('.');
            				Integer intAmountVal = 0;
            				if (strAmountVal!=null && strAmountVal!='')
            					intAmountVal = Integer.valueOf(strAmountVal);
            				if (intAmountVal > 10){
            					//Set a generic error so the 'fail' functionality can be tested
            					this.trans.go1__Code__c = 100;
            					this.trans.go1__Status__c = 'Error';
            				} 
            			}
            			
            			//SPECIAL CASES: if trans code >= 10 and status contains success, manual or approved
            			if (this.trans.go1__Code__c >= 10 && 
							(this.trans.go1__Status__c.contains('Success') || this.trans.go1__Status__c.contains('Manual') || this.trans.go1__Status__c.contains('Approved')))
						{
							this.trans.go1__Code__c = 0;
						}
            			
            			//Simulate an Exception error to ensure comliant functionality
            			if (isExceptionTest)
            				throw new PaymentException('Test Exception: Problem in submission of Transaction to Payment Gateway');
            			
            			if (this.payment.isTransactionSuccess(this.trans)) { 
		            		if (this.creditcard.Id == null) {
		            			// create the credit card before creating a recurring object so that it can reference it
		                		this.creditcard = saveCreditCardDetails();
		            		}
		            		save = true;
		                	this.failed = false;
		            	} else {
		            		save = true; //tailbyr: SF-761: we want to record ALL transactions in SF, even failures
		            		this.failed = true;
		            	}
            		}
            		
            		this.trans.go1__Account_ID__c = this.creditcard.go1__Token__c;
            		this.trans.go1__Payment_Name__c = this.creditcard.name;
            		
            		this.status = trans.go1__Status__c + '.';
            		if (gateway.go1__Is_Test__c == true && !payment.isTransactionSuccess(this.trans)) {
            			this.status = 'USING TEST GATEWAY:<br/>We expect an error here if the value is not whole dollars $X.00. This is testing that we can correctly handle a failure from the payment gateway. To test a successful payment, change the product or amount so that it has whole dollars.<br/><br/>Gateway error message: ' + this.status; 
            		}
				}
			}
            
  			if (save) {
                // retrieve or create new account depending on the user's preference
                saveAccount();
                
                //If we get this far, we've saved the Account. However, if we've hit an error, report the Account save on the resubmission form (SF-837)
                if (this.failed && accountType == NEW_ACCOUNT){
                	this.status += '<br>Your new Account details have been saved but, as an error occurred, no subscription was created. Please correct any errors and resubmit to create the new Subscription';
                	accountType = EXISTING_ACCOUNT; //Set to existing as the Account is now created
                	this.product.Person_Account__c = this.account.Id; //And select the account we've just created
                }
                
				// create a new product instance only if the payment was successul!
				if (!this.failed)
					saveProduct();
                
                if (!this.isFree) {
                	this.creditcard.go1__Account__c = this.account.Id;
                	
                	if (this.creditcard.go1__Merchant_ID__c == null)
                		this.creditcard.go1__Merchant_ID__c = '1'; //Seems to be a default value
                	
                	//Only save the credit card if the transaction went through successfully 
                	if (gateway != null && gateway.go1__Payment_Type__c == 'CreditCard' && !this.failed) {
						upsert this.creditcard;
                	}
                	
                	//If the payment name on the Transaction has not been set, set it now
                	if (this.trans.go1__Payment_Name__c == null)
                		this.trans.go1__Payment_Name__c = this.account.name;
                	
                	// store the transaction to the database
                	this.trans.Subscription_Instance__c = this.product.Id;
                	this.trans.go1__Account__c = this.account.id;
                	
                	// SF-831 - we need to ensure unencrypted CC numbers not stored! A new sub with a failed trans will not have generated a CC token so will have this. Use Reference instead
                	if (!(this.payment.isTransactionSuccess(this.trans)) && this.creditcard.Id == null) {
                		if (this.trans.go1__Reference__c == null || this.trans.go1__Reference__c == '')
                			this.trans.go1__Account_ID__c = this.trans.go1__Reference_Id__c;
                		else
                			this.trans.go1__Account_ID__c = this.trans.go1__Reference__c;
                	}
                	
                	insert this.trans;
					// recurring object is saved with the transaction if required
                }
                
                //Signup was only processed when payment didn't fail
                if (!this.failed)
                	signupProcessed = true;
            }
            
            if (!this.failed) {
	        	errorStatus = ApexPages.Severity.CONFIRM;
     	        this.status = 'Successful Subscription! View subscription <a href="/'+ this.product.Id +'">here</a> or click <a href="/apex/subscriber_wizard">here</a> to create another subscription.';
     	        this.showForm = false;
	        }
	        
        } catch (Calloutexception ce) {
        	System.debug('DEBUG: There was an error with the subscription wizard. Exception: ' + ce);
        	errorStatus = ApexPages.Severity.ERROR;
        	
        	this.status = this.status + ' The was an error in the communication to SecurePay and a response was not received back. <br/> YOUR TRANSACTION MAY HAVE BEEN SUCCESSFUL! <br/>Please check in SecurePay before reprocessing this transaction.';
        	
        	// Email error to dev team
        	sendErrorEmail('Callout Exception to SecurePay from New Subscription Wizard', ce);
        	
        } catch (Exception e) {
            System.debug('DEBUG: There was an error with the subscription wizard. Exception: ' + e + ' Line: ' + e.getLineNumber() 
            				+ ' StackTrace:' + e.getStackTraceString());
            errorStatus = ApexPages.Severity.ERROR;
            
            this.status = this.status + ' There was an error with the subscription wizard. Exception: ' + e;
            
            // Email error to dev team
            sendErrorEmail('General Exception hit in New Subscription Wizard', e);
        }
        
		ApexPages.Message msgErr = new ApexPages.Message(errorStatus, this.status);
		ApexPages.addmessage(msgErr);

        return null;
    }
    
    /**
    * Send an email to SF Development with the Exception encountered in NSW so more information can be traced
    */
    public void sendErrorEmail(String subject, Exception e)
    {
    	//---- Email error to dev team
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        //String[] toAddresses = new String[] {a.CreatedBy.Email}; 
        String[] toAddresses = new String[] {'ross.tailby@eurekareport.com.au'}; //TEST USE ONLY - use Customer Setting to hold email addresses
        mail.setToAddresses(toAddresses); 
        mail.setSubject(subject); 
            
        string s = 'Exception: ' + e 
           	+ '<br />StackTrace:' + e.getStackTraceString(); 
        mail.setHtmlBody(s); 
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
        //-------------------------------------
    }
    
    /**
     * Retrieves the correct account or creates a new one if needed
     */
    public void saveAccount() {
        if (accountType == NEW_ACCOUNT) {
            // if new account, create and store in database
            try {
                // make the account a person account type
                AccountHelper helper = new AccountHelper(this.account);
                helper.setDefaultEmailPreferences(this.subscription);
                helper.account.RecordTypeId = '01290000000NnFN';
                
                upsert helper.account;
                
                //Load account back into the Account object for reference access to the fields
                Account[] accounts = [SELECT Id, Name, FirstName, LastName, BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode, PersonEmail, RecordTypeId, Phone, IsPersonAccount, Birth_Year__c, GST_Exempt__c 
                						FROM Account WHERE Id = :this.account.id LIMIT 1];
            	if (!accounts.isEmpty()) {
                	this.account = accounts[0];
            	}
                
            } catch (DMLException e) {
            	System.debug('DEBUG: Error saving account:' + e.getMessage());
                errorHandler(e, 'Failed creating a new account!');
            }
        } else {
        	// account already loaded, but want to save the GST Exemption status
        	if (originallyGSTExempt != this.account.GST_Exempt__c) {
        		update this.account;
        	}
        }
    }
    
    /**
     * Creates a new product instance from the values in the form
     */
    public void saveProduct() {
        // create Product Instance Object
        try {
            // assign an account to the product if new account
            if (accountType == NEW_ACCOUNT) {
                this.product.Person_Account__c = this.account.Id;
            }
            
            // change the default duration
            ProductInstanceHelper helper = new ProductInstanceHelper(this.product);
            /*helper.setInstancePeriod(this.product, this.subscription); //This will happen in the trigger - Don't need to do it here!
            this.product.Start_Date__c = helper.instance.Start_Date__c;
            this.product.End_Date__c = helper.instance.End_Date__c;
            */
            
            //Check to see if the auto-renewal has been ticked or not..
            boolean autoRenewal = this.product.Auto_Renewal__c;
            
            // assign a campaign object to the product if a campaign has been selected
            if (this.campaignId != null && this.campaignId != '') {
                this.product.Campaign__c = this.campaignId;
            }
            upsert this.product;
            
            //The PI trigger will always tick AutoRenew if not a free product. So if trying to setup a non-free product with no autorenew, update the PI post-trigger to reflect this
            /* Attempting to remove this to avoid reprocessing all the triggers on Product Instance
            if (autoRenewal == false && this.product.Subscription_Type__c != 'f') {
            	this.product.Auto_Renewal__c = autoRenewal;
            	update this.product;
            }*/
            
        } catch (DMLException e) {
            errorHandler(e, 'Failed subscribing to product!');
        }
    }
   
    /***************************************************/
    /* Helper Methods */
    
    public void errorHandler(Exception e, String message) {
    	this.status = message;
        this.failed = true;
        throw e;
    }
    
    /**
     * Calculate the end date 
     */
    public static Date evaluateEndDate(Date startDate, Decimal num, String period) {
        // change the default duration
        Date endDate = startDate;
        if (period == 'day') {
            endDate = startDate.addDays(Integer.valueOf(num));
        } else if (period == 'week') {
            endDate = startDate.addDays(Integer.valueOf(num * 7));
        } else if (period == 'month') {
            endDate = startDate.addMonths(Integer.valueOf(num));
        } else if (period == 'year') {
            endDate = startDate.addYears(Integer.valueOf(num));
        }
        return endDate;
    }
    
 
    /**
     * Returns true if payment for subscription should be made immediately 
     */
    public Boolean isPayNow() {
        List<Product_Instance__c> previous =
        	[
        		SELECT Id, End_Date__c
        		FROM Product_Instance__c
        		WHERE Person_Account__c = :this.account.Id AND Strean__c = :this.subscription.Subscription_Stream__c
        		ORDER BY End_Date__c DESC
        	];
        
        if (previous.isEmpty()) {
            return true;
        } else if (previous[0].End_Date__c <= Date.today()) {
            return true;
        }
        return false;
    }
    
    /**
     * Adds a surcharge to the total amount depending on the card type
     */
    public void updateCreditCardSurcharges() {
    	// change the price if needed
    	Decimal surcharge = AIBMHelper.getCreditCardSurcharge(this.creditcard.go1__Card_Type__c);
    	this.totalAmount = AIBMHelper.getAmountWithSurcharge(this.product.Amount__c - this.gstDeduction, surcharge); //Also remove GST if necessary
    }
    
    /**
     *
     */
    public void updateRecurredAmounts() {
    	// change the price if needed
        Decimal numberOfAttempts = AIBMHelper.evaluateNumberAttempts(this.subscription.Duration__c, this.subscription.Duration_units__c, this.subscription.Subscription_Type__c, false);
        Decimal surcharge =	AIBMHelper.getCreditCardSurcharge(this.creditcard.go1__Card_Type__c);
        //this.trans.go1__Amount__c = (trans.go1__Amount__c * (1+surcharge))/numberOfAttempts;
        this.totalAmount = (AIBMHelper.getAmountWithSurcharge(this.product.Amount__c, surcharge))/numberOfAttempts;
    }
    
    /**
     * Calculates the discount using the original sub info with the updated sub info,
     * takes into account the price differences as well as the duration differences
     */
    public void updateDiscount() {
        // check that a subscription product has been selected to compare the new amount against
        if (this.subscription != null) {
            Subscription_Product__c original_sub = [SELECT Duration__c, Duration_units__c FROM Subscription_Product__c WHERE Id = :this.subscription.Id];
            
            // calculate the duration into the discount if not null or 0
            if (original_sub.Duration__c != null && original_sub.Duration__c != 0) {
                // calculate the number of monthly attempts for the original and current sub
                Decimal product_attempts = AIBMHelper.evaluateNumberAttempts(this.subscription.Duration__c, this.subscription.Duration_units__c, 'monthly', false);
                Decimal sub_attempts = AIBMHelper.evaluateNumberAttempts(original_sub.Duration__c, original_sub.Duration_units__c, 'monthly', false);
                 
                if (sub_attempts == 0 && product_attempts == 0) {
                    // the discount is the differences between the original and current price
                    this.product.Discount__c = this.subscription.Price__c - this.product.Amount__c;
                } else if (sub_attempts == 0 && product_attempts != 0) {
                    // the discount is multiples of the product attempts
                    this.product.Discount__c = (this.subscription.Price__c * product_attempts) - this.product.Amount__c;
                } else {
                    // the discount takes into account the ratio of the two sub attempts
                    this.product.Discount__c = (this.subscription.Price__c * (product_attempts/sub_attempts)) - this.product.Amount__c;
                }
            } else {
                // the discount is the negative current price
                this.product.Discount__c = (this.subscription.Price__c * 0) - this.product.Amount__c;
            }
        }
    }
    
    /**
	 * Saves a credit card instance to SecurePay
	 */ 
	public go1__CreditCardAccount__c saveCreditCardDetails() {
		go1__CreditCardAccount__c credit_card_item;
		if (payment.creditcard != null) {
			try {
				credit_card_item = payment.creditcard;
	
				credit_card_item.go1__Merchant_Id__c = '1'; // @deprecated
				credit_card_item.go1__Account__c = this.account.Id;
	     		credit_card_item.name = go1.PaymentHelper.sanatizeCreditCard(credit_card_item.go1__Token__c) + ' ' + credit_card_item.go1__Card_Type__c;
	     			
	     		String uniqueToken = go1.PaymentHelper.uniqueToken(credit_card_item.go1__Token__c, 12, 'SF-');
	    	 	// here we save to external system
	    	 	go1.Paymethod method = go1.Paymethod.loadPaymentMethod(credit_card_item.go1__Payment_Gateway__c);
	    	 	method.vault(credit_card_item.go1__Token__c, credit_card_item.go1__Expiry__c, uniqueToken);
	
				credit_card_item.go1__Token__c = method.getToken();
			}
			catch (Exception e) {
				sendErrorEmail('Exception hit in New Subscription Wizard: timeout while saving Credit Card details into SecurePay', e);
			}
		}
		return credit_card_item;
	}
}