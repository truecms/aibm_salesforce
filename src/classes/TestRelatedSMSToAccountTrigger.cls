@IsTest
public with sharing class TestRelatedSMSToAccountTrigger 
{
	static testMethod void triggerForFirstMobileFormat() 
	{
		Account con = new Account();
        con.LastName = 'Test Contact 1';
        con.PersonEmail = 'test@test.com.test';
        con.PersonMobilePhone = '61434838377';
        insert con;

		Test.setFixedSearchResults(new Id[]{con.Id});

       	smagicinteract__Incoming_SMS__c incomingSMS = new smagicinteract__Incoming_SMS__c();
    	incomingSMS.smagicinteract__external_field__c = 'test';
    	incomingSMS.smagicinteract__Inbound_Number__c = '987654321';
    	incomingSMS.smagicinteract__Mobile_Number__c = '61434838377';
    	incomingSMS.smagicinteract__SMS_Text__c = 'test SMS 1';

    	insert incomingSMS;

    	List<smagicinteract__Incoming_SMS__c> incomingList = [SELECT Id, Account__c, smagicinteract__Mobile_Number__c FROM smagicinteract__Incoming_SMS__c WHERE smagicinteract__Mobile_Number__c =: incomingSMS.smagicinteract__Mobile_Number__c];

    	system.assertEquals(incomingList[0].Account__c,con.Id);
    }

	static testMethod void triggerForSecondMobileFormat()
	{
        Account con = new Account();
        con.LastName = 'Test Contact 2';
        con.PersonEmail = 'test@test.com.test';
        con.PersonMobilePhone = '0434838377';
        insert con;

		Test.setFixedSearchResults(new Id[]{con.Id});

       	smagicinteract__Incoming_SMS__c incomingSMS = new smagicinteract__Incoming_SMS__c();
    	incomingSMS.smagicinteract__external_field__c = 'test';
    	incomingSMS.smagicinteract__Inbound_Number__c = '987654321';
    	incomingSMS.smagicinteract__Mobile_Number__c = '61434838377';
    	incomingSMS.smagicinteract__SMS_Text__c = 'test SMS 2';

    	insert incomingSMS;

    	List<smagicinteract__Incoming_SMS__c> incomingList = [SELECT Id, Account__c, smagicinteract__Mobile_Number__c FROM smagicinteract__Incoming_SMS__c WHERE smagicinteract__Mobile_Number__c =: incomingSMS.smagicinteract__Mobile_Number__c];

    	system.assertEquals(incomingList[0].Account__c,con.Id);
    }
    
    static testMethod void triggerForThirdMobileFormat()
	{
        Account con = new Account();
        con.LastName = 'Test Contact 3';
        con.PersonEmail = 'test@test.com.test';
        con.PersonMobilePhone = '04 3483 8377';
        insert con;

		Test.setFixedSearchResults(new Id[]{con.Id});

       	smagicinteract__Incoming_SMS__c incomingSMS = new smagicinteract__Incoming_SMS__c();
    	incomingSMS.smagicinteract__external_field__c = 'test';
    	incomingSMS.smagicinteract__Inbound_Number__c = '987654321';
    	incomingSMS.smagicinteract__Mobile_Number__c = '61434838377';
    	incomingSMS.smagicinteract__SMS_Text__c = 'test SMS 3';

    	insert incomingSMS;

    	List<smagicinteract__Incoming_SMS__c> incomingList = [SELECT Id, Account__c, smagicinteract__Mobile_Number__c FROM smagicinteract__Incoming_SMS__c WHERE smagicinteract__Mobile_Number__c =: incomingSMS.smagicinteract__Mobile_Number__c];

    	system.assertEquals(incomingList[0].Account__c,con.Id);
    }
    
    static testMethod void triggerForFourthMobileFormat()
	{
        Account con = new Account();
        con.LastName = 'Test Contact 4';
        con.PersonEmail = 'test@test.com.test';
        con.PersonMobilePhone = '(04) 3483 8377';
        insert con;

		Test.setFixedSearchResults(new Id[]{con.Id});

       	smagicinteract__Incoming_SMS__c incomingSMS = new smagicinteract__Incoming_SMS__c();
    	incomingSMS.smagicinteract__external_field__c = 'test';
    	incomingSMS.smagicinteract__Inbound_Number__c = '987654321';
    	incomingSMS.smagicinteract__Mobile_Number__c = '61434838377';
    	incomingSMS.smagicinteract__SMS_Text__c = 'test SMS 4';

    	insert incomingSMS;

    	List<smagicinteract__Incoming_SMS__c> incomingList = [SELECT Id, Account__c, smagicinteract__Mobile_Number__c FROM smagicinteract__Incoming_SMS__c WHERE smagicinteract__Mobile_Number__c =: incomingSMS.smagicinteract__Mobile_Number__c];

    	system.assertEquals(incomingList[0].Account__c,con.Id);
    }
    
    static testMethod void triggerForFifthMobileFormat()
	{
        Account con = new Account();
        con.LastName = 'Test Contact 5';
        con.PersonEmail = 'test@test.com.test';
        con.PersonMobilePhone = '0434 838 377';
        insert con;

		Test.setFixedSearchResults(new Id[]{con.Id});

       	smagicinteract__Incoming_SMS__c incomingSMS = new smagicinteract__Incoming_SMS__c();
    	incomingSMS.smagicinteract__external_field__c = 'test';
    	incomingSMS.smagicinteract__Inbound_Number__c = '987654321';
    	incomingSMS.smagicinteract__Mobile_Number__c = '61434838377';
    	incomingSMS.smagicinteract__SMS_Text__c = 'test SMS 5';

    	insert incomingSMS;

    	List<smagicinteract__Incoming_SMS__c> incomingList = [SELECT Id, Account__c, smagicinteract__Mobile_Number__c FROM smagicinteract__Incoming_SMS__c WHERE smagicinteract__Mobile_Number__c =: incomingSMS.smagicinteract__Mobile_Number__c];

    	system.assertEquals(incomingList[0].Account__c,con.Id);
    }
    
    static testMethod void triggerRandomUnmatchedMobileFormat()
	{
        Account con = new Account();
        con.LastName = 'Test Contact 6';
        con.PersonEmail = 'test@test.com.test';
        con.PersonMobilePhone = '043 4 83 8 3 (77)';
        insert con;

       	smagicinteract__Incoming_SMS__c incomingSMS = new smagicinteract__Incoming_SMS__c();
    	incomingSMS.smagicinteract__external_field__c = 'test';
    	incomingSMS.smagicinteract__Inbound_Number__c = '987654321';
    	incomingSMS.smagicinteract__Mobile_Number__c = '61434838377';
    	incomingSMS.smagicinteract__SMS_Text__c = 'test SMS 6';

    	insert incomingSMS;

    	List<smagicinteract__Incoming_SMS__c> incomingList = [SELECT Id, Account__c, smagicinteract__Mobile_Number__c FROM smagicinteract__Incoming_SMS__c WHERE smagicinteract__Mobile_Number__c =: incomingSMS.smagicinteract__Mobile_Number__c];

    	system.assertEquals(incomingList[0].Account__c,null);
    }
    
    static TestMethod void triggerForWithoutCountryCode()
    {
    	Account con = new Account();
        con.LastName = 'Test Contact 6';
        con.PersonEmail = 'test@test.com.test';
        con.PersonMobilePhone = '0434 838 377';
        insert con;

		Test.setFixedSearchResults(new Id[]{con.Id});

       	smagicinteract__Incoming_SMS__c incomingSMS = new smagicinteract__Incoming_SMS__c();
    	incomingSMS.smagicinteract__external_field__c = 'test';
    	incomingSMS.smagicinteract__Inbound_Number__c = '987654321';
    	incomingSMS.smagicinteract__Mobile_Number__c = '0434838377';
    	incomingSMS.smagicinteract__SMS_Text__c = 'test SMS 6';

    	insert incomingSMS;

    	List<smagicinteract__Incoming_SMS__c> incomingList = [SELECT Id, Account__c, smagicinteract__Mobile_Number__c FROM smagicinteract__Incoming_SMS__c WHERE smagicinteract__Mobile_Number__c =: incomingSMS.smagicinteract__Mobile_Number__c];

    	system.assertEquals(incomingList[0].Account__c,con.Id);
    }
    
    static TestMethod void triggerForOnlyMobileNumber()
    {
    	Account con = new Account();
        con.LastName = 'Test Contact 7';
        con.PersonEmail = 'test@test.com.test';
        con.PersonMobilePhone = '0434 838 377';
        insert con;

		Test.setFixedSearchResults(new Id[]{con.Id});

       	smagicinteract__Incoming_SMS__c incomingSMS = new smagicinteract__Incoming_SMS__c();
    	incomingSMS.smagicinteract__external_field__c = 'test';
    	incomingSMS.smagicinteract__Inbound_Number__c = '987654321';
    	incomingSMS.smagicinteract__Mobile_Number__c = '434838377';
    	incomingSMS.smagicinteract__SMS_Text__c = 'test SMS 7';

    	insert incomingSMS;

    	List<smagicinteract__Incoming_SMS__c> incomingList = [SELECT Id, Account__c, smagicinteract__Mobile_Number__c FROM smagicinteract__Incoming_SMS__c WHERE smagicinteract__Mobile_Number__c =: incomingSMS.smagicinteract__Mobile_Number__c];

    	system.assertEquals(incomingList[0].Account__c,con.Id);
    }
}