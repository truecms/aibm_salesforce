/**
 * Testing the REST Webservice: CampaignMemberCreateService
 * Author: tailbyr
 * Copyright: brightday.com.au
 */
@isTest
public with sharing class TestCampaignMemberCreateService {

	static Campaign createCampaign(){
		Campaign campaign = new Campaign(Name='Test Campaign',StartDate=Date.today().addDays(-7),EndDate=Date.today().addDays(7),IsActive=true);
		insert campaign;
		
		return campaign;
	}

	static TestMethod void testCreateCampaignMemberREST() {
		//Create Account
		Account acc = TestAccountHelper.createAccountOnly(1);
		
		//Create Campaign
		Campaign cam = createCampaign();
		
		//Create the REST req/res objects 
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		
		req.requestURI = '/services/apexrest/setCampaign';
		req.httpMethod = 'GET';
		req.addParameter('emailAddress', acc.PersonEmail);
		req.addParameter('campaignId', cam.Id);
		RestContext.request = req;
		RestContext.response = res;
		
		Test.startTest();
		String result = CampaignMemberCreateService.createCampaignMember();
		Test.stopTest();
		
		//Test REST repsonse
		System.assertEquals(result, 'Success');
		
		//Check we have the correct CampaignMember object
		Account resultAcc = [SELECT PersonContactId FROM Account WHERE Id = :acc.Id LIMIT 1];
		List<CampaignMember> member = [SELECT ContactId, CampaignId FROM CampaignMember WHERE ContactId = :resultAcc.PersonContactId];
		
		System.assertEquals(member.size(), 1);
		System.assertEquals(member[0].CampaignId, cam.Id);
	}
	
	static TestMethod void testAddNewCampaignMemberREST() {
		//Create Account
		Account acc = TestAccountHelper.createAccountOnly(1);
		
		//Create the REST req/res objects 
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		
		req.requestURI = '/services/apexrest/setCampaign';
		req.httpMethod = 'GET';
		req.addParameter('emailAddress', acc.PersonEmail);
		req.addParameter('source', 'UnitTest');
		RestContext.request = req;
		RestContext.response = res;
		
		Test.startTest();
		String result = CampaignMemberCreateService.createCampaignMember();
		Test.stopTest();
		
		//Test REST repsonse
		System.assertEquals(result, 'Success');
		
		//Get the Account values back and check the BD Reporting fields
		Account resultAcc = [SELECT Id, AccountSource
							FROM Account WHERE Id = :acc.Id LIMIT 1];
		
		System.assertEquals(resultAcc.AccountSource, 'UnitTest');
	}
	
	static TestMethod void testMissingData(){
		//Create the REST req/res objects 
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		
		req.requestURI = '/services/apexrest/setCampaign';
		req.httpMethod = 'GET';
		
		//Adding blank values for both email and campaign - email validation is first
		req.addParameter('emailAddress', '');
		req.addParameter('campaignId', '');
		RestContext.request = req;
		RestContext.response = res;
		
		Test.startTest();
		String noEmailResult = CampaignMemberCreateService.createCampaignMember();
		System.assertEquals(noEmailResult, 'ERROR: no email address supplied');
		
		req.addParameter('emailAddress', 'test entry');
		RestContext.request = req;
		
		String noCampaignResult = CampaignMemberCreateService.createCampaignMember();
		System.assertEquals(noCampaignResult, 'ERROR: no campaign id or source supplied');
		Test.stopTest();
	}
	
	static TestMethod void testIncorrectEmailAddress() {
		//Create Campaign
		Campaign cam = createCampaign();
		
		//Create the REST req/res objects 
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		
		req.requestURI = '/services/apexrest/setCampaign';
		req.httpMethod = 'GET';
		
		//Supply an Email address that does not exist 
		req.addParameter('emailAddress', 'doesnotexist@testbucket.net');
		req.addParameter('campaignId', cam.Id);
		RestContext.request = req;
		RestContext.response = res;
		
		Test.startTest();
		String result = CampaignMemberCreateService.createCampaignMember();
		Test.stopTest();
		
		//Test REST repsonse
		System.assertEquals(result, 'ERROR: no matching Account could be found for email address:doesnotexist@testbucket.net');
	}
	
	static TestMethod void testIncorrectCampaign(){
		//Create Account
		Account acc = TestAccountHelper.createAccountOnly(1);
		
		//Create the REST req/res objects 
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		
		req.requestURI = '/services/apexrest/setCampaign';
		req.httpMethod = 'GET';
		req.addParameter('emailAddress', acc.PersonEmail);
		
		//Supply a Campaign ID that does not exist
		req.addParameter('campaignId', '123456789');
		RestContext.request = req;
		RestContext.response = res;
		
		Test.startTest();
		String result = CampaignMemberCreateService.createCampaignMember();
		Test.stopTest();
		
		//Test REST repsonse
		System.assertEquals(result, 'ERROR: no matching Campaign could be found with ID:123456789');
	}

}