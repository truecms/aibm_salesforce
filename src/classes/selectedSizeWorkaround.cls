public class selectedSizeWorkaround {

    ApexPages.StandardSetController setCon;

	public Task newTask {get; set;}
	
	public boolean uploadFile {get; set; }
	public transient string nameFile { get; set; }
    public transient Blob contentFile { get; set; }
    
    public List<String> customers { get; set; }
    public integer noDetailRows { get; set; }
    private transient String[] filelines = new String[]{};
    
    public List<String> invalidEmails { get; set; }
    public boolean invalidEmail { get; set; }
    
    public selectedSizeWorkaround(ApexPages.StandardSetController controller) {
        if (!Test.isRunningTest()) {
        	controller.addFields(new List<String>{'OwnerId', 'PersonContactId'});
        }
        setCon = controller;
        newTask = new Task();
        invalidEmail = false;
        
        //If we have no selected records, offer the upload function!
        if (getMySelectedSize() < 1)
        {
        	uploadFile = true;
        }
        else
        {
        	uploadFile = false;
        }
    }

    public integer getMySelectedSize() {
        return setCon.getSelected().size();
    }
    public integer getMyRecordsSize() {
        return setCon.getRecords().size();
    }
    public PageReference save(){
    	PageReference pr = setCon.save();
    	//Only create the tasks if there is a Task Subject specified
    	if (newTask.CallDisposition!=null)
    		this.createTasks();
    	return pr;
    }
    private void createTasks(){
    	//Check this - we want to create a task for each Account assigned to say "Call this Account". Can this be done through workflow rules?
    	List<Account> selectAccounts = (List<Account>)setCon.getSelected();
    	List<Task> tasks = new List<Task>();
    	for (Account acc : selectAccounts)
    	{
    		Task t = new Task(Subject=newTask.CallDisposition, ActivityDate=Date.Today(), WhatId=acc.Id);
    		t.WhoId = acc.PersonContactId;
    		t.OwnerId = acc.OwnerId;
    		tasks.add(t);
    	}
    	insert tasks;
    }
    
    
    
    public PageReference readFile() {
    	if (nameFile != null) {
            nameFile=contentFile.toString();
            
            //20130625 tailbyr: handle scenario where no line breaks are in the file - sensible error message
            if (!nameFile.contains('\n') && !nameFile.contains('\r')) {
                ApexPages.Message msgErr = new ApexPages.Message(ApexPages.Severity.ERROR, 'Import file does not contain any line breaks! Please check your file format and ensure that each Customer entry is given on a new line and columns are separated by commas.');
                ApexPages.addmessage(msgErr);
                return null;
            }
            
            this.noDetailRows = 0; //Initialise the blank rows count
            customers = new List<String>(); //Initialise the Account collection
            invalidEmails = new List<String>(); //Initialise the invalid addresses too
            
            //Also initialise the Email pattern matcher so we don't try to insert bad emails!
            Pattern p = Pattern.compile( '([a-zA-Z0-9_\\-\\.]+)@(((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3}))');
            
            if (nameFile.contains('\r'))
				filelines = nameFile.split('\r');
			else	        
	        	filelines = nameFile.split('\n');
            
            for (Integer i=0;i<filelines.size();i++)
            {
            	String[] inputvalues = new String[]{};
                inputvalues = filelines[i].split(',');
                
                //If the row is totally blank, just ignore it and carry on
                if (filelines[i].length() < 2 || inputvalues.size()<1) {
                	noDetailRows++;
                	continue;
                }
                
                String emailValue = inputvalues[0].trim();
                
                //Add a noDetailRow if the email address is blank
                if (emailValue.length()<1) {
                	noDetailRows++;
                }
                
                //Check whether the first line contains a header or not
                if ((i == 0 && p.matcher(emailValue).matches())
                		|| (i > 0)) 
                {
                    //Check that the email entry is valid
                    Matcher emailMatch = p.matcher(emailValue); 
                    if (emailMatch.matches()){
                    	
                       	//Add to the collection of Accounts we're changing
                       	customers.add(emailValue);
                    }
                    else {
                    	//This email address is invalid! Report it...
                    	invalidEmails.add(emailValue);
                    }
                }
            }
            
            if (customers.size() > 0) {
            	
            	//Grab the Accounts from the emails
            	List<Account> accs = [SELECT Id, PersonEmail, FirstName, LastName, Owner.Name, PersonContactId FROM Account WHERE PersonEmail in :customers];
            	setCon.setSelected(accs);
            	
            	//Now we need to check if there were any accounts in the csv that did not retrieve Accounts from SF
            	if (accs.size() != customers.size()) //Don't bother if both are the same size! 
            	{
            		Set<String> setCustomers = new Set<String>();
            		setCustomers.addAll(customers);
            		for (Account a : accs) {
            			if (setCustomers.contains(a.PersonEmail))
            				setCustomers.remove(a.PersonEmail);
            		}
            		
            		//Whatever is left in the setCustomers object has not been returned, therefore add to invalidEmails list
            		invalidEmails.addAll(setCustomers);
            	}
            	
            	//Hide the upload section and show the allocator!
            	uploadFile = false;
            }
            
            //Show the invalid email section if applicable
            if (invalidEmails.size() > 0)
            	invalidEmail = true;
              
        }
        return null;
    }
    
    
    //*****************************************
    //Test Methods
    
    static RecordType getRecordType()
	{
		// Find any person account record type
	    RecordType recordType = [ select Id, Name, DeveloperName from RecordType where SObjectType = 'Account' and IsPersonType = true limit 1 ];
	    return recordType;
	}
    
    private static List<Account> createTestAccounts()
    {
    	RecordType personType = getRecordType();
    	List<Account> accs = new List<Account>();
    	Account acc1 = new Account(FirstName='ExpiredSub', LastName='Test245', PersonEmail='expiredsub.test245@test.com.test', RecordTypeId=personType.Id);
    	Account acc2 = new Account(FirstName='ActiveUser', LastName='Test302', PersonEmail='activeuser.test302@test.com.test', RecordTypeId=personType.Id);
    	Account acc3 = new Account(FirstName='Inactive', LastName='Test957', PersonEmail='inactive.test957@test.com.test', RecordTypeId=personType.Id);
    	accs.add(acc1);
    	accs.add(acc2);
    	accs.add(acc3);
    	return accs;
    }
    
    public static TestMethod void testReadFile()
    {
    	//Need to create and insert these Accounts
    	List<Account> accs = createTestAccounts();
    	insert accs;
    	
    	String csv = 'Email\n';
    	csv += 'expiredsub.test245@test.com.test\n';
    	csv += 'activeuser.test302@test.com.test\n';
    	csv += 'inactive.test957@test.com.test\n';
    	csv += 'inactive, test957@test.com.test\n'; //Also add an invalid email address to show that this is picked up and handled
    	csv += 'abc123@test.net\n'; //Also add a valid email which does not belong to an account and check that is marked as invalid
    	
    	//Create a basic instance of the controller with an empty collection of Accounts
    	ApexPages.Standardsetcontroller ssc = new ApexPages.Standardsetcontroller(new List<Account>());
    	selectedSizeWorkaround ssw = new selectedSizeWorkaround(ssc);
    	ssw.nameFile = '';
    	ssw.contentFile = Blob.valueOf(csv);
    	
    	Test.startTest();
    	ssw.readFile();
    	Test.stopTest();
    	
    	//Check that we have 3 accounts in the selectedSet
    	System.assertEquals(ssw.getMySelectedSize(), 3);
    	System.assert(ssw.invalidEmail);
    	System.assertEquals(ssw.invalidEmails.size(), 2);
    }
    
    public static TestMethod void testAssignOwners()
    {
    	List<Account> accs = createTestAccounts();
    	insert accs;
    	    	
    	//Just grab any user who we can use to become the Account Owner
    	User testuser = [SELECT Id FROM User WHERE IsActive = TRUE AND UserType = 'Standard' AND UserRoleId != '' AND alias NOT IN ('sadm', 'it') LIMIT 1];
    	    	
    	for (Account a : accs)
    	{
    		a.OwnerId = testUser.Id;
    	}
    	
    	//Set these accounts as the selected accounts for the SetController
    	ApexPages.Standardsetcontroller ssc = new ApexPages.Standardsetcontroller(accs);
    	ssc.setSelected(accs); //Ensure the selected accounts are set as Selected
    	selectedSizeWorkaround ssw = new selectedSizeWorkaround(ssc);
    	
    	ssw.newTask.CallDisposition = 'Unit Test assignOwners';
    	
    	//Now call the save method and check that the owners are correct and the tasks are created
    	Test.startTest();
    	ssw.save();
    	Test.stopTest();
    	
    	//Get our accounts and check their owners and tasks
    	List<Account> testAccounts = [SELECT Id, OwnerId FROM Account WHERE OwnerId = :testUser.Id LIMIT 10];
    	System.assertEquals(testAccounts.size(), 3);
    	
    	List<Id> accountIds = new List<Id>();
    	for (Account a : testAccounts) { accountIds.add(a.Id); }
    	List<Task> testTasks = [SELECT Id FROM Task WHERE WhatId IN :accountIds];
    	System.assertEquals(testTasks.size(), 3);
    }
    
}