@isTest
private class TestTransactionManager
{

	static testMethod void testMain()
	{
		RecordType anRC = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Account' AND IsPersonType = true LIMIT 1 ];
		Account anAccount = new Account(FirstName = 'Frst Name', LastName = 'Lst Name', PersonEmail = 'real@testbucket.net', RecordTypeId = anRC.Id);
		insert anAccount;
		
		//aTrans1 is a success
		go1__Transaction__c aTrans1 = new go1__Transaction__c(go1__Account__c = anAccount.Id, go1__Account_ID__c = anAccount.Id, 
				  go1__Amount__c = 100, go1__Status__c = 'Success', go1__Payment_Name__c = '123', Send_Invoice__c = true, go1__Code__c = 8);
		//aTrans2 is a failure
		go1__Transaction__c aTrans2 = new go1__Transaction__c(go1__Account__c = anAccount.Id, go1__Account_ID__c = anAccount.Id, 
				  go1__Amount__c = 100, go1__Status__c = 'Fail', go1__Payment_Name__c = '123', Send_Invoice__c = false, go1__Code__c = 11);
		List<go1__Transaction__c> aMainList = new List<go1__Transaction__c>{aTrans1, aTrans2};
		insert aMainList;
				
		Apexpages.Standardcontroller aStdCont = new Apexpages.Standardcontroller(aTrans2);
		TransactionInvoiceController aController = new TransactionInvoiceController(aStdCont);
		PageReference pr2 = aController.doRedirect();
		
		//Check that the redirect has produced the error
		System.assertEquals(null, pr2);
		System.assert(ApexPages.getMessages().size() == 1);
        System.assert(ApexPages.getMessages().get(0).getDetail() == 'Transaction doesn\'t follow the requirements. Please check it.');
		
		aStdCont = new Apexpages.Standardcontroller(aTrans1);
		aController = new TransactionInvoiceController(aStdCont);
		PageReference pr1 = aController.doRedirect();
		
		//Check that this redirect has given the successful pageref with transId at end
		System.assertEquals('/'+aTrans1.Id, pr1.getUrl());
	}
	
	static testMethod void testIncorrectEmailAddress()
	{
		RecordType anRC = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Account' AND IsPersonType = true LIMIT 1 ];
		Account anAccount = new Account(FirstName = 'Frst Name', LastName = 'Lst Name', PersonEmail = 'test@test.em', RecordTypeId = anRC.Id);
		insert anAccount;
		
		go1__Transaction__c aTrans1 = new go1__Transaction__c(go1__Account__c = anAccount.Id, go1__Account_ID__c = anAccount.Id, 
				  go1__Amount__c = 100, go1__Status__c = 'Success', go1__Payment_Name__c = '123', Send_Invoice__c = true, go1__Code__c = 8);
		insert aTrans1;
		
		Boolean result = TransactionManager.sendInvoiceEmail(aTrans1.Id);
		
		// different test depending on environment - should pass in product and fail elsewhere
		if (Userinfo.getOrganizationId() == AIBMHelper.PRODUCTION_ORG) {
			System.assertEquals(true, result);
		}
		else {
			System.assertEquals(false, result);
		}
	}
	
	static testMethod void testCorrectEmailAddress()
	{
		RecordType anRC = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Account' AND IsPersonType = true LIMIT 1 ];
		Account anAccount = new Account(FirstName = 'Frst Name', LastName = 'Lst Name', PersonEmail = 'eurekareport@eurekareport.com.au', RecordTypeId = anRC.Id);
		insert anAccount;
		
		go1__Transaction__c aTrans1 = new go1__Transaction__c(go1__Account__c = anAccount.Id, go1__Account_ID__c = anAccount.Id, 
				  go1__Amount__c = 100, go1__Status__c = 'Success', go1__Payment_Name__c = '123', Send_Invoice__c = true, go1__Code__c = 8);
		insert aTrans1;
		
		Boolean result = TransactionManager.sendInvoiceEmail(aTrans1.Id);
		System.assertEquals(true, result);
	}
}