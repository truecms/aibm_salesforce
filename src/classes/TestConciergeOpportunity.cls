/**
 *
 * Test Class for Concierge Forms Integration with Salesforce for creating opportunities using Email Services.
 * Created on 17-11-2015
 * Author: Mithun Roy
 * Copyright: EurekaReport.com.au
 */
@isTest(SeeAllData=true)
public class TestConciergeOpportunity {
@isTest
static void testEmail() {
    // create a new email and envelope object
    Messaging.InboundEmail email = new Messaging.InboundEmail() ;
    Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

    // setup the data for the email
    email.subject = 'Create Concierge Opportunity';
    email.fromname = 'Xyz Abc';
    env.fromAddress = 'noreply@brightday.com.au';
    email.plainTextBody = 'A new lead has been captured from your Concierge Form...'+'\n'+'\n'+                                         
                                 'Here is the data submitted from your form:'+'\n'+'\n'+
                                 'name : Mithun Roy'+'\n'+
                                 'email : abc@eurekareport.com.au'+'\n'+
                                 'phone_number: 0470410145'+'\n'+                                  
                                 'preferred_date_of_call : 2015/11/17'+'\n'+
                                 'preferred_time_of_call : Morning'+'\n';   

    // call the email service class and test it with the data in the testMethod
    CreateConciergeOpportunity emailProcess = new CreateConciergeOpportunity();
    emailProcess.handleInboundEmail(email, env);

    // query for the contact the email service created
    Opportunity Opp = [select id, Name,Best_time_to_call__c from Opportunity
    where CreatedDate = TODAY and StageName= '30 Day Trial' and Name = 'Mithun Roy : Hot Phone Call Opt In (Concierge Form)'];
    //Assert the values
    System.assertEquals(Opp.Name,'Mithun Roy : Hot Phone Call Opt In (Concierge Form)');
    //System.assertEquals(Opp.Preferred_Date_to_Call__c,'2015/11/17');
    System.assertEquals(Opp.Best_time_to_call__c,'Morning');
  
  }
}