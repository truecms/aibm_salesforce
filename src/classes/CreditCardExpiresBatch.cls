/**
	Batchable job, that creates tasks for accounts with credit cards, that expires before the renewal happen.
*/
global class CreditCardExpiresBatch implements Schedulable, Database.Batchable<Product_Instance__c>, Database.Stateful
{
	public static String lastAccountManager { get; set; }
	
	public TaskManager tm {get; set;}
	
	public String ccExpiryQueue {get; set;}
	public Boolean ccExpiryAllocationRules {get; set;}
		
	public CreditCardExpiresBatch() 
	{
		// Get the Account Manager/Owner of the last Credit Card Expiry task so that we can assign the tasks equally
		List<Task> lastCCExpiryTask = [SELECT Id, ownerId FROM Task WHERE Subject LIKE :TaskManager.CREDIT_CARD_EXPIRY_SUBJECT + '%' ORDER BY createdDate desc LIMIT 1];
		if (lastCCExpiryTask.size()>0)
			lastAccountManager = lastCCExpiryTask[0].ownerId;
			
		//Get the customer settings allocation rule and queue for this automated task creator
		Allocation_Rules__c allocationRules = Allocation_Rules__c.getInstance('Default');
		ccExpiryAllocationRules = allocationRules.Credit_Card_Allocation_Rule__c;
		ccExpiryQueue = allocationRules.Credit_Card_Queue__c;
		
		tm = new TaskManager(ccExpiryQueue); //Instigate the TaskManager and tell it which User Queue to use from Custom Settings
	}
	
	global void execute(SchedulableContext ctx)
	{
		Database.executeBatch(new CreditCardExpiresBatch(), 1);
	}
	
	global Iterable<Product_Instance__c> start(Database.batchableContext BC)
	{
		List<Product_Instance__c> aList = [SELECT Id, End_Date__c, Person_Account__c FROM Product_Instance__c WHERE Auto_Renewal__c = true
				  AND End_Date__c = :Date.today().addDays(1)]; //Changed to 1 : SF-862
		System.debug('DEBUG:aList - ' + aList);
		return aList;
	}

	global void execute(Database.BatchableContext BC, List<Product_Instance__c> instances)
	{
		ProductInstanceHelper aHelper = new ProductInstanceHelper(instances);
		aHelper.createExpirationTasks(tm, ccExpiryAllocationRules);
	}
	
	global void finish(Database.BatchableContext BC)
	{
		//Now all the tasks have been created, grab all created tasks and reallocate fairly - CC expiry should ALWAYS be allocated fairly
		List<Task> allCCTasksToday = [SELECT Id, ownerId FROM Task 
				where Subject LIKE :TaskManager.CREDIT_CARD_EXPIRY_SUBJECT + '%' 
				AND createdDate = TODAY
				AND CallDisposition = :TaskManager.ALLOCATION_DISPOSITION];
				
		System.debug('DEBUG: allCCTasksToday:' + allCCTasksToday.size());		
		
		for (Task t : allCCTasksToday) 
		{
			String nextAccountManager = tm.getNextSalesTeamMember(CreditCardExpiresBatch.lastAccountManager);
			t.OwnerId = nextAccountManager;
			CreditCardExpiresBatch.lastAccountManager = nextAccountManager;	
		}
		
		update allCCTasksToday;
	}
	
	global static void setupSchedule() { 
		CreditCardExpiresBatch scheduled_task = new CreditCardExpiresBatch();
		String payment_id = System.schedule('Expired Credit Cards', '0 0 * * * ?', scheduled_task);
	}
}