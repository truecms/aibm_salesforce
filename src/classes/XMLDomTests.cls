@isTest
private class XMLDomTests {
  
  	public static testmethod void test0() { xmldom d;  
 		// truncated xml
 		d = new xmldom('<books><book author="Manoj" date="1999" >My Book</book><book author="Ron" >Your '); 
        system.assert( d.root != null);
        List<xmldom.Element> keys  = d.getElementsByTagName('bad');
        system.assert( d.toXmlString() != null );
        XMLdom.Element p = d.root;
        p.removeChild(p.firstChild());
        p = d.getElementByTagName('bad');
 		system.assert( p == null );
 	} 
  	
  	public static testmethod void test1() { 
        xmldom d;        
        d = new xmldom('<book  author="Manoj" >My Book</book>');
        d.dumpAll(); 
        XMLdom.Element e = d.getElementsByTagName('book')[0];
        system.assert( e.getAttribute('author') =='Manoj' ); 
        
        d = new xmldom(); d.parseFromString('<book  author="Manoj" >My Book</book>');
        d.dumpAll(); 
         
        d = new xmldom('<books><book>My Book</book></books>');
        d.dumpAll(); 
        //system.debug( d.getElements() ); 
        system.debug ( d.getElementsByTagName('book')[0].nodeValue );
        system.assert ( d.getElementsByTagName('book')[0].nodeValue == 'My Book' );
        
        d = new xmldom('<books><book author="Manoj" date="1999" >My Book</book><book author="Ron" >Your Book</book></books>'); 
        d.dumpAll();
        system.debug ( d.getElementsByTagName('book') );
        for(XMLdom.Element ee:d.getElementsByTagName('book')) { system.debug( 'Author is ' + ee.getAttribute('author')); }
        
        string testErrorResponse = 
         '<?xml version="1.0" encoding="UTF-8"?>'+ 
         '<Error><Code>NoSuchKey</Code><Message>The specified key does not exist.</Message><Key>test-key</Key><RequestId>49DDFSFDFSDfBD</RequestId>'+
         '<HostId>PB4hNZsosdfz</HostId></Error>';
    
        d = new xmldom(testErrorResponse); 
        d.root.dumpAll();
        
        // uses namespaces
         string testACLResponse = 
     '<?xml version="1.0" encoding="UTF-8"?>'+ 
     '<AccessControlPolicy xmlns="http://s3.amazonaws.com/doc/2006-03-01/"><Owner><ID>owner_id</ID><DisplayName>vdddddds</DisplayName></Owner>'+
     '<AccessControlList><Grant><Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="CanonicalUser" foo="bar" ><ID>owner_id</ID>'+
     '<DisplayName>vne</DisplayName></Grantee><Permission>FULL_CONTROL</Permission></Grant></AccessControlList></AccessControlPolicy>';
        d = new xmldom(testACLResponse); 
        d.dumpAll(); 
        system.debug ('has child '+ d.root.hasChildNodes()) ; 
        
        system.assert(  d.root.isEqualNode(d.root) ,' is equal node');
        system.assert( d.root.textContent() == '' );
         
        d.getElementsByTagName('Grantee')[0].dump(); 
        
        system.assert( d.getElementsByTagName('Grantee')[0].hasAttributes() );
        
            
        
    }
    

    public static testmethod void test3() { 
         string testNotification = 
         '<?xml version="1.0" encoding="UTF-8"?>' +
            '<bookstore><book><title lang="eng">Harry Potter</title><price>29.99</price>' +
            '</book><book><title lang="eng">Learning XML</title><price>39.95</price></book></bookstore>';

        xmldom d;
        d = new xmldom(testNotification);  
        list<xmldom.element> tmp ; 

        tmp =  d.root.getElementsByTagName('book');  // matching by name
        system.assertEquals( 2, tmp.size() ); 
        d.dumpList( tmp );
        system.debug( d.root.toXmlString() ) ;
        XMLdom.Element a = d.ownerDocument();
        XMLdom.Element f = a.firstChild();
        XMLdom.Element c = f.ownerDocument(); 
        system.assert( a.isSameNode( c ) );
        XMLdom.Element b = a.cloneNode();
        system.assert( ! a.isSameNode(f) ); 
        
        a = new XMLdom.Element(); 
        system.assertEquals( a.firstChild(), null, ' must be null' );
        
        system.assertEquals( a.getElementByTagName('bad'), null);       
     }

    public static testmethod void test4() { 
    
        String feed =   '<?xml version="1.0" encoding="UTF-8"?>' +
            '<bookstore><book><title lang="eng">Harry Potter</title><entry>v</entry><price>29.99</price>' +
            '</book><book><title lang="eng">Learning XML</title><price>39.95</price></book></bookstore>';

        xmldom d = new xmldom(feed);  
        list<xmldom.element> tmp ;  
        d.dumpAll();
        XMLdom.Element e2 = d.ownerDocument().getElementByTagName('book');
        e2.dumpAll();   
        system.assertEquals('book', e2.nodeName );         
        system.assertEquals( e2.getValue('title'), 'Harry Potter');
        
        
        XMLdom.Element[] el = d.ownerDocument().getElementsByTagName('link');
        for(XMLdom.Element ee:el) { 
            system.debug( ee.path() );
            ee.dump(); 
        }
        
        e2 = d.ownerDocument().getElementByTagName('entry');
        System.assertEquals('v', e2.nodeValue);
        
        e2 = d.ownerDocument().getElementsByPath('/bookstore/book/title')[0];
        e2.dump();
        
        /* 
         * experimental path based patern matching, sort of like xpath, 
         * but simpler, just matches a path() string with 
         * the pattern supplied
         */
        // children of entry
        el= d.ownerDocument().getElementsByPath('/bookstore/book/.*');
        d.dumpList(el);
        system.assertEquals( 5, el.size() );
        
        // just the entry node
        el= d.ownerDocument().getElementsByPath('/bookstore/book');
        system.assertEquals( 2, el.size() );
        
        // entry and children
        el= d.ownerDocument().getElementsByPath('/.*/entry.*');
        system.assertEquals( 1, el.size() );
        
    }        

    /* google data elements have a prefix that is parsed differently, make sure we can 
     * deliver that intact 
     */
    public static testmethod void testWithPrefix() { 
        
        String feed = '<?xml version=\'1.0\' encoding=\'UTF-8\'?><feed xmlns=\'http://www.w3.org/2005/Atom\' xmlns:openSearch=\'http://a9.com/-/spec/opensearchrss/1.0/\' xmlns:gCal=\'http://schemas.google.com/gCal/2005\' xmlns:gd=\'http://schemas.google.com/g/2005\'><id>http://www.google.com/calendar/feeds/default/allcalendars/full</id><updated>2008-06-11T17:28:38.768Z</updated><title type=\'text\'>Nick Tran\'s Calendar List</title><link rel=\'http://schemas.google.com/g/2005#feed\' type=\'application/atom+xml\' href=\'http://www.google.com/calendar/feeds/default/allcalendars/full\'/><link rel=\'http://schemas.google.com/g/2005#post\' type=\'application/atom+xml\' href=\'http://www.google.com/calendar/feeds/default/allcalendars/full\'/><link rel=\'self\' type=\'application/atom+xml\' href=\'http://www.google.com/calendar/feeds/default/allcalendars/full\'/><author><name>Nick Tran</name><email>sforcedemos@gmail.com</email></author><generator version=\'1.0\' uri=\'http://www.google.com/calendar\'>Google Calendar</generator><openSearch:startIndex>1</openSearch:startIndex>' + 
        '<entry><id>http://www.google.com/calendar/feeds/default/allcalendars/full/sforcedemos%40gmail.com</id><published>2008-06-11T17:28:38.789Z</published><updated>2008-06-11T14:24:25.000Z</updated><title type=\'text\'>force.com Team Demo</title><link rel=\'alternate\' type=\'application/atom+xml\' href=\'http://www.google.com/calendar/feeds/sforcedemos%40gmail.com/private/full\'/><link rel=\'http://schemas.google.com/acl/2007#accessControlList\' type=\'application/atom+xml\' href=\'http://www.google.com/calendar/feeds/sforcedemos%40gmail.com/acl/full\'/><link rel=\'self\' type=\'application/atom+xml\' href=\'http://www.google.com/calendar/feeds/default/allcalendars/full/sforcedemos%40gmail.com\'/><link rel=\'edit\' type=\'application/atom+xml\' href=\'http://www.google.com/calendar/feeds/default/allcalendars/full/sforcedemos%40gmail.com\'/><author><name>Nick Tran</name><email>sforcedemos@gmail.com</email></author><gCal:timezone value=\'America/Los_Angeles\'/><gCal:hidden value=\'false\'/>'+ 
        '<gCal:color value=\'#2952A3\'/><gCal:selected value=\'true\'/><gCal:accesslevel value=\'owner\'/></entry></feed>';
        
        xmldom d = new xmldom(feed);  
        list<xmldom.element> tmp ;  
        d.dumpAll();
        XMLdom.Element e2 = d.ownerDocument().getElementByTagName('entry');
        e2.dumpAll();   
        //system.assert( d.getElementByTagName('gCal:selected') != null , ' missing gcal');
    }
}