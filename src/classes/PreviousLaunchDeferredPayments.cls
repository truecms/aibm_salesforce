/**
 * PreviousLaunchDeferredPayments 
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * DEPRECATED: This report is no longer required - superceeded by the new DeferredPaymentsReport implementation
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
 public class PreviousLaunchDeferredPayments extends DeferredPaymentsReportBase {
	
	public Date launchDate { get; set; }
	public String launchDateFormatted { get; set; }
	
	/**
	 * Initialises the initial variables
	 */
	public PreviousLaunchDeferredPayments() {
		// set the launch date in the url
		if (ApexPages.currentPage().getParameters().get('launch') != null) {
			this.launchDate = Date.parse(ApexPages.currentPage().getParameters().get('launch'));
			this.launchDateFormatted = this.launchDate.format();
		} else {
			this.launchDate = Date.newInstance(2013,03,10);
			this.launchDateFormatted = this.launchDate.format();
		}
		
		setDefaultParamters();
		initData();
	}
	
	/**
	 *
	 */
	public override void historicalData(Integer pastMonths) {
		/*for (Integer i=0; i < pastMonths; i++) {
			List<SObject> monthlyResult = new List<SObject>();
			List<SObject> yearlyResult = new List<SObject>();
	   		try {
	   			// set all the subscription instances first before calculating deferred payment for each sub
	   			for (DeferredPaymentsReport.DataRow row : this.table.values()) {
	   				row.values.add(0);
	   				row.dateLinks.add(Datetime.newInstance(this.dates[i], Time.newInstance(0, 0, 0, 0)).format('d/M/y'));
	   			}
	   			
	   			//
	   			if (this.streamName == '' || this.streamName == null) {
	   				monthlyResult = [SELECT Subscription_Product__r.Subscription_Stream__r.Name, SUM(Deferred_Payment__c) Deferred_Payment FROM Product_Instance__c WHERE Start_Date__c < :this.launchDate AND Start_Date__c <= :Date.today() AND ((Start_Date__c < :this.dates[i+1] AND End_Date__c >= :this.dates[i+1]) OR (Start_Date__c >= :this.dates[i] AND End_Date__c < :this.dates[i+1])) AND Subscription_Type__c = 'm' AND Deferred_Payment__c != 0 AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :this.dates[i] AND go1__Date__c < :this.dates[i+1] AND go1__Refunded__c = true) GROUP BY Subscription_Product__r.Subscription_Stream__r.Name];
	   				yearlyResult = [SELECT Subscription_Product__r.Subscription_Stream__r.Name, SUM(Deferred_Payment__c) Deferred_Payment FROM Product_Instance__c WHERE Start_Date__c < :this.launchDate AND ((Start_Date__c < :this.dates[i+1] AND End_Date__c >= :this.dates[i+1]) OR (Start_Date__c >= :this.dates[i] AND End_Date__c < :this.dates[i+1])) AND Subscription_Type__c = 'y' AND Deferred_Payment__c != 0 AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :this.dates[i] AND go1__Date__c < :this.dates[i+1] AND go1__Refunded__c = true) GROUP BY Subscription_Product__r.Subscription_Stream__r.Name];
	   			} else {
	   				monthlyResult = [SELECT Subscription_Product__r.Name, SUM(Deferred_Payment__c) Deferred_Payment FROM Product_Instance__c WHERE Start_Date__c < :this.launchDate AND Start_Date__c <= :Date.today() AND ((Start_Date__c < :this.dates[i+1] AND End_Date__c >= :this.dates[i+1]) OR (Start_Date__c >= :this.dates[i] AND End_Date__c < :this.dates[i+1])) AND Subscription_Type__c = 'm' AND Deferred_Payment__c != 0 AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :this.dates[i] AND go1__Date__c < :this.dates[i+1] AND go1__Refunded__c = true) GROUP BY Subscription_Product__r.Name];
	   				yearlyResult = [SELECT Subscription_Product__r.Name, SUM(Deferred_Payment__c) Deferred_Payment FROM Product_Instance__c WHERE Start_Date__c < :this.launchDate AND ((Start_Date__c < :this.dates[i+1] AND End_Date__c >= :this.dates[i+1]) OR (Start_Date__c >= :this.dates[i] AND End_Date__c < :this.dates[i+1])) AND Subscription_Type__c = 'y' AND Deferred_Payment__c != 0 AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :this.dates[i] AND go1__Date__c < :this.dates[i+1] AND go1__Refunded__c = true) GROUP BY Subscription_Product__r.Name];
	   			}
	   			
	   			//
	   			for (SObject sub : monthlyResult) {
	   				if (this.table.get(String.valueOf(sub.get('Name'))) != null && (Decimal)sub.get('Deferred_Payment') != null) {
	   					this.table.get(String.valueOf(sub.get('Name'))).values[i] += (Decimal)sub.get('Deferred_Payment');
	   				}
	   			}
	   			for (SObject sub : yearlyResult) {
	   				if (this.table.get(String.valueOf(sub.get('Name'))) != null && (Decimal)sub.get('Deferred_Payment') != null) {
	   					this.table.get(String.valueOf(sub.get('Name'))).values[i] += (Decimal)sub.get('Deferred_Payment');
	   				}
	   			}
	   		} catch (Exception e) {
	   			System.debug(e);
	   		}
		}*/
	}
	
	/**
	 *
	 */
	public override void futureData(Integer futureMonths) {
		/*for (Integer i=futureMonths; i < this.numberOfMonths; i++) {
			List<SObject> result = new List<SObject>();
	   		try {
	   			// set all the subscription instances first before calculating deferred payment for each sub
	   			for (DeferredPaymentsReport.DataRow row : this.table.values()) {
	   				row.values.add(0);
	   				row.dateLinks.add(Datetime.newInstance(this.dates[i], Time.newInstance(0, 0, 0, 0)).format('d/M/y'));
	   			}
	   			
	   			if (this.streamName == '' || this.streamName == null) {
	   				result = [SELECT Subscription_Product__r.Subscription_Stream__r.Name, SUM(Deferred_Payment__c) Deferred_Payment FROM Product_Instance__c WHERE Start_Date__c < :this.launchDate AND ((Start_Date__c < :this.dates[i+1] AND End_Date__c >= :this.dates[i+1]) OR (Start_Date__c >= :this.dates[i] AND End_Date__c < :this.dates[i+1])) AND Subscription_Type__c = 'y' AND Deferred_Payment__c != 0 AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :this.dates[i] AND go1__Date__c < :this.dates[i+1] AND go1__Refunded__c = true) GROUP BY Subscription_Product__r.Subscription_Stream__r.Name];
	   			} else {
	   				result = [SELECT Subscription_Product__r.Name, SUM(Deferred_Payment__c) Deferred_Payment FROM Product_Instance__c WHERE Start_Date__c < :this.launchDate AND ((Start_Date__c < :this.dates[i+1] AND End_Date__c >= :this.dates[i+1]) OR (Start_Date__c >= :this.dates[i] AND End_Date__c < :this.dates[i+1])) AND Subscription_Type__c = 'y' AND Deferred_Payment__c != 0 AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :this.dates[i] AND go1__Date__c < :this.dates[i+1] AND go1__Refunded__c = true) GROUP BY Subscription_Product__r.Name];
	   			}
	   			for (SObject sub : result) {
	   				if (this.table.get(String.valueOf(sub.get('Name'))) != null && (Decimal)sub.get('Deferred_Payment') != null) {
	   					this.table.get(String.valueOf(sub.get('Name'))).values[i] += (Decimal)sub.get('Deferred_Payment');
	   				}
	   			}
	   		} catch (Exception e) {
	   			System.debug(e);
	   		}
		}*/
	}
	
	/**
	 *
	 */
	public override void getAccruals() {
		Date endDate = this.dates[numberOfMonths];
		System.debug(endDate);
		System.debug(this.streamName);
		
		// find the total amount of all unstarted subscriptions
		List<SObject> remainingUnstartedYearlyPayments = new List<SObject>();
		List<SObject> remainingUnstartedMonthlyPayments = new List<SObject>();
		// find the remaining payments from all started subscriptions
		List<Product_Instance__c> remainingYearlyProducts = new List<Product_Instance__c>();
		List<Product_Instance__c> remainingMonthlyProducts = new List<Product_Instance__c>();
		
		if (this.streamName == '' || this.streamName == null) {
			remainingUnstartedYearlyPayments = [SELECT SUM(Amount__c) Remaining_Payment FROM Product_Instance__c WHERE Start_Date__c >= :this.launchDate AND Start_Date__c >= :endDate AND End_Date__c >= :endDate AND Amount__c != 0 AND Subscription_Type__c = 'y' AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :endDate AND go1__Refunded__c = true)];
			remainingUnstartedMonthlyPayments = [SELECT SUM(Amount__c) Remaining_Payment FROM Product_Instance__c WHERE Start_Date__c >= :this.launchDate AND End_Date__c <= :Date.today() AND Start_Date__c >= :endDate AND End_Date__c >= :endDate AND Amount__c != 0 AND Subscription_Type__c = 'm' AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :endDate AND go1__Refunded__c = true)];
			remainingYearlyProducts = [SELECT Id, Deferred_Payment__c, End_Date__c FROM Product_Instance__c WHERE Start_Date__c >= :this.launchDate AND Deferred_Payment__c > 0 AND Start_Date__c < :endDate AND End_Date__c >= :endDate AND Subscription_Type__c = 'y' AND Deferred_Payment__c != 0 AND Status__c != 'Cancelled'];
			remainingMonthlyProducts = [SELECT Id, Deferred_Payment__c, End_Date__c FROM Product_Instance__c WHERE Start_Date__c >= :this.launchDate AND Deferred_Payment__c > 0 AND Start_Date__c < :Date.today() AND Start_Date__c < :endDate AND End_Date__c >= :endDate AND Subscription_Type__c = 'm' AND Deferred_Payment__c != 0 AND Status__c != 'Cancelled'];
		} else {
			remainingUnstartedYearlyPayments = [SELECT SUM(Amount__c) Remaining_Payment FROM Product_Instance__c WHERE Start_Date__c >= :this.launchDate AND Start_Date__c >= :endDate AND End_Date__c >= :endDate AND Amount__c != 0 AND Subscription_Type__c = 'y' AND Subscription_Product__r.Subscription_Stream__r.Name = :this.streamName AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :endDate AND go1__Refunded__c = true)];
			remainingUnstartedMonthlyPayments = [SELECT SUM(Amount__c) Remaining_Payment FROM Product_Instance__c WHERE Start_Date__c >= :this.launchDate AND End_Date__c <= :Date.today() AND Start_Date__c >= :endDate AND End_Date__c >= :endDate AND Amount__c != 0 AND Subscription_Type__c = 'm' AND Subscription_Product__r.Subscription_Stream__r.Name = :this.streamName AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :endDate AND go1__Refunded__c = true)];
			remainingYearlyProducts = [SELECT Id, Deferred_Payment__c, End_Date__c FROM Product_Instance__c WHERE Start_Date__c >= :this.launchDate AND Deferred_Payment__c > 0 AND Start_Date__c < :endDate AND End_Date__c >= :endDate AND Subscription_Type__c = 'y' AND Deferred_Payment__c != 0 AND Status__c != 'Cancelled' AND Subscription_Product__r.Subscription_Stream__r.Name = :this.streamName];
			remainingMonthlyProducts = [SELECT Id, Deferred_Payment__c, End_Date__c FROM Product_Instance__c WHERE Start_Date__c >= :this.launchDate AND Deferred_Payment__c > 0 AND Start_Date__c < :Date.today() AND Start_Date__c < :endDate AND End_Date__c >= :endDate AND Subscription_Type__c = 'm' AND Deferred_Payment__c != 0 AND Status__c != 'Cancelled' AND Subscription_Product__r.Subscription_Stream__r.Name = :this.streamName];
		}
		
		List<Product_Instance__c> remainingProducts = new List<Product_Instance__c>();
		remainingProducts.addAll(remainingYearlyProducts);
		remainingProducts.addAll(remainingMonthlyProducts);
		
		Decimal remainingTotal = 0;
		for (Product_Instance__c product : remainingProducts) {
			Integer remainingMonths = endDate.daysBetween(product.End_Date__c) / 30;
			remainingTotal += product.Deferred_Payment__c * remainingMonths;
		}
		if (!remainingUnstartedYearlyPayments.isEmpty() && (Decimal)remainingUnstartedYearlyPayments[0].get('Remaining_Payment') != null) {
			remainingTotal += (Decimal)remainingUnstartedYearlyPayments[0].get('Remaining_Payment');
		}
		if (!remainingUnstartedMonthlyPayments.isEmpty() && (Decimal)remainingUnstartedMonthlyPayments[0].get('Remaining_Payment') != null) {
			remainingTotal += (Decimal)remainingUnstartedMonthlyPayments[0].get('Remaining_Payment');
		}
		
		accrualData = new List<Decimal>();
		for (Integer i=0; i<numberOfMonths; i++) {
			Decimal accrual = 0;
			for (Integer j=i+1; j<numberOfMonths; j++) {
				accrual += this.totalsData[j];
			}
			accrualData.add(accrual + remainingTotal);
		}
	}
}