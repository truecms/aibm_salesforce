/**
* REST Webservice : Create CampaignMember when supplied with a valid Account email address and Campaign ID
* For use with brightday KissMetrics campaign landing pages.
* Author: tailbyr
* Copyright: brightday.com.au
*/
@RestResource(urlMapping='/setCampaign/*')
global with sharing class CampaignMemberCreateService {

	@HttpGet
	global static String createCampaignMember() {
		
		//Get the email address, campaign ID and/or source from the RestContext
		String email = RestContext.request.params.get('emailAddress');
		String campaignId = RestContext.request.params.get('campaignId');
		String source = RestContext.request.params.get('source');
		
		//Find the Account(Contact)
		if (email == null || email.length() < 1)
			return 'ERROR: no email address supplied';
		
		//Ensure we have either a Campaign or a Source
		if ((campaignId == null || campaignId.length() < 1) && (source == null || source.length() < 1))
			return 'ERROR: no campaign id or source supplied';	
		
		//Retrieve the Account and Campaign from input data
		List<Account> account = [SELECT Id, PersonContactId
								FROM Account WHERE PersonEmail = :email];
		
		if (account == null || account.size() < 1) {
			return 'ERROR: no matching Account could be found for email address:' + email;
		}
		else {
			Boolean updateAccount = false;
			
			//If a source string has been supplied, set the Source field on the Account
			if (source != null && source.length() < 41) {
				account[0].AccountSource = source;
				updateAccount = true;
			}
			
			//Also, if a campaign id has been given, link the Campaign and Account too
			if (campaignId != null && campaignId.length() > 0) {
				
				List<Campaign> campaign = [SELECT Id, Name, Eureka_ID__c FROM Campaign WHERE Id = :campaignId];
				if (campaign == null || campaign.size() < 1) {
					return 'ERROR: no matching Campaign could be found with ID:' + campaignId;
				}
				else {
					//Now create the new CampaignMember, joining the Account and Campaign
					CampaignMember cm = new CampaignMember();
					cm.CampaignId = campaign[0].Id;
					cm.ContactId = account[0].PersonContactId;
					cm.Status = 'Responded';
					insert cm;
				}
			}
			
			//If we've updated the Contact, perform the DML
			if (updateAccount)
				update account[0];
			
			return 'Success';
		}
	}

}