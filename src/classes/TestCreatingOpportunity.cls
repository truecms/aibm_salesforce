@isTest
public class TestCreatingOpportunity {
@isTest
    static void TestCreatingOpportunity() {
        List<Account> accts = new List<Account>();
        for (Integer i=0;i<2;i++) {
            Account a0 = new Account(Name='OptyAccount' + i, 
                                    OneVue_Application_Product__c = 'DIRECT',
                                    OneVue_Application_Status__c = 'Not Started',
                                    OV_Created_Date__c = (System.now() - 2).date());
            accts.add(a0);            
            Account a1 = new Account(Name='OptyAccount' + i, 
                                    OneVue_Application_Product__c = 'DIRECT',
                                    OV_Created_Date__c = (System.now() - 2).date());            
            accts.add(a1);            
            Account a2 = new Account(Name='OptyAccount' + i, 
                                    BD_Registration_Date__c = (System.now() - 10).date(),
                                    BD_Last_Login_Date__c = (System.now() - 8).date(),
                                    Webinar_Email__c = true);
            accts.add(a2);            
            Account a3 = new Account(Name='OptyAccount' + i, 
                                    BD_Registration_Date__c = (System.now() - 10).date(),
                                    BD_Last_Login_Date__c = (System.now() - 8).date(),
                                    Webinar_Email__c = false);
            accts.add(a3);            
            Account a4 = new Account(Name='OptyAccount' + i, 
                                    BD_Current_Role__c  = null,
                                    Webinar_Email__c = true);
            accts.add(a4);            
            Account a5 = new Account(Name='OptyAccount1' + i, 
                                    OneVue_Application_Product__c = 'DIRECT',
                                    OneVue_Application_Status__c = 'Not Started',
                                    OV_Created_Date__c = (System.now() - 3).date());
            accts.add(a5);            
            Account a6 = new Account(Name='OptyAccount2' + i, 
                                    BD_Current_Role__c  = 'Member',
                                    Webinar_Email__c = true);
            accts.add(a6);        
        }
        insert accts;
        try {
            Assignment__c Agent1 = new Assignment__c();
            Agent1.Name = 'Agent1';
            Agent1.RowId__c = '00590000003NgMu';
            insert Agent1;
            Assignment__c Agent2 = new Assignment__c();
            Agent2.Name = 'Agent2';
            Agent2.RowId__c = '00590000004DqaY';
            insert Agent2;
            Assignment__c Agent3 = new Assignment__c();
            Agent3.Name = 'Agent3';
            Agent3.RowId__c = '00590000003KxkI';
            insert Agent3;
            // For each account just inserted, add opportunities
            CreatingOpportunity Opp = new  CreatingOpportunity();
            Opp.CreateOpportunity();
            for (Opportunity o:[SELECT Name FROM Opportunity WHERE CreatedDate = TODAY and Name = 'Application Started Page3 : OptyAccount0']) {
                System.assertEquals(o.Name, 'Application Started Page3 : OptyAccount0');
            } 
             for (Opportunity o:[SELECT Name FROM Opportunity WHERE CreatedDate = TODAY and Name = 'Application Started : OptyAccount0']) {
                System.assertEquals(o.Name, 'Application Started : OptyAccount0');
            } 
            for (Opportunity o:[SELECT Name FROM Opportunity WHERE CreatedDate = TODAY and Name = 'Customer logged in, signed up for webinar : OptyAccount0']) {
                System.assertEquals(o.Name, 'Customer logged in, signed up for webinar : OptyAccount0');
            }
            for (Opportunity o:[SELECT Name FROM Opportunity WHERE CreatedDate = TODAY and Name = 'Customer logged in, but not signed up for webinar : OptyAccount0']) {
                System.assertEquals(o.Name, 'Customer logged in, but not signed up for webinar : OptyAccount0');
            } 
            for (Opportunity o:[SELECT Name FROM Opportunity WHERE CreatedDate = TODAY and Name = 'Webinar Signup Inactive : OptyAccount0']) {
                System.assertEquals(o.Name, 'Webinar Signup Inactive : OptyAccount0');
            }  
            for (Opportunity o:[SELECT Name FROM Opportunity WHERE CreatedDate = TODAY and Name = 'Application Started Page3 : OptyAccount10']) {
                System.assertNotEquals(o.Name, 'Webinar Signup Inactive : OptyAccount0');
            }
            for (Opportunity o:[SELECT Name FROM Opportunity WHERE CreatedDate = TODAY and Name = 'Webinar Signup Inactive : OptyAccount20']) {
                System.assertNotEquals(o.Name, 'Webinar Signup Inactive : OptyAccount0');
            }
        }
        catch (System.NullPointerException e) {
                System.debug('The following exception has occurred: ' + e.getMessage());
        }           
    }
}