/**
* Controller for massupdatetaskowners
* Loads all tasks for the logged in user and all sub-ordinates and shows them so they can be reallocated as necessary
* Author: tailbyr
* Copyright: EurekaReport.com.au
*/
public class selectedTaskUpdate {
	
	private integer counter=0;  //keeps track of the offset
    private integer list_size=20; //sets the page size or number of rows
    public integer total_size; //used to show user the total size of the list
        
	public Set<String> taskNames {get;set;}
	public Boolean hasSelectedTasks {get;set;}
        
    public List<TaskWrapperCls> taskList;
    public List<Task> chosenTasks {get; set;}
    public List<String> stuffTask {get; set;}
    
    public Task ownerTask {get; set;}
    
 	public String sortBy { get; set; } // The string containing the column to filter by
	public String sortDir { get; set; } // The string containing the direction (ASC, DESC)
        
    public string selectedPage { 
    	get;
    	
    	set { selectedPage=value; }
    }
    
    public selectedTaskUpdate() {
    	total_size = 0; //Initiate to 0 at first as we haven't found any records yet, then set properly in getTaskList()
        selectedPage='0';
        ownerTask = new Task(ActivityDate=null, OwnerId=userinfo.getUserId(), Status='Not Started');
    }
    
    public String constructTaskQuery(boolean count)
    {
    	String taskQuery = 'SELECT ';

		if (count)
			taskQuery += 'COUNT() ';
		else
			taskQuery += 'Id, Subject, Status, Description, Owner.Name, WhoId, Who.Name, WhatId, What.Name, ActivityDate, CreatedDate ';

		taskQuery += 'FROM Task '
            	   + 'WHERE OwnerId = \'' + ownerTask.OwnerId + '\'';
            					
        if (ownerTask.ActivityDate!=null) {
         	String formattedActivityDate = String.valueOf(ownerTask.ActivityDate);
           	taskQuery += ' AND ActivityDate = ' + formattedActivityDate.substringBefore(' ');
        }
        if (ownerTask.Subject!=null)
          	taskQuery += ' AND Subject LIKE \'' + ownerTask.Subject + '%\' ';
        if (ownerTask.WhoId!=null)
          	taskQuery += ' AND WhoId = \'' + ownerTask.WhoId + '\'';
        if (ownerTask.Status!=null)
        	taskQuery += ' AND Status = \'' + ownerTask.Status + '\'';
            					
        if (!count)
        	taskQuery += ' ORDER BY ' + sortBy + ' ' + sortDir + ' LIMIT ' + list_size + ' OFFSET ' + counter;
        else
        	taskQuery += ' LIMIT 500'; //Set arbitrary limit to avoid 'Too many query rows' API limit error
        
        return taskQuery;
    }
    
    public TaskWrapperCls[] getTaskList() 
    {
    	//Initialise the sorting variables if they haven't been set already
    	if (sortBy == null)
    		sortBy = 'Id';
    	if (sortDir == null)
    		sortDir = 'ASC';
    	
        if (selectedPage != '0') 
        	counter = list_size*integer.valueOf(selectedPage)-list_size;
        	
        try 
        { 
        	//We need to run a COUNT query to see how many rows we have to paginate
        	total_size = Database.countQuery(this.constructTaskQuery(true));
        	
        	taskList = new List<TaskWrapperCls>();
        	taskNames = new Set<String>();
        	
        	//Call the query construct method to get the query for the fields we require for tasks
        	String taskQuery = this.constructTaskQuery(false);
        	
        	List<Task> resultTasks = Database.Query(taskQuery); //Pull out to separate list to avoid for loop soql 200 row govenor limit
        	for (Task t : resultTasks) 
            {
            	taskList.add(new TaskWrapperCls(t));						
            }
            
            return taskList;
        } 
        catch (QueryException e) {                            
        	ApexPages.addMessages(e);                   
            return null;
        }        
    }
    
    public void displaySelectedTasks()
    {
    	if (chosenTasks == null)
    		chosenTasks = new List<Task>();
    	else
    		chosenTasks.clear();
    		
        hasSelectedTasks = false;
        for(TaskWrapperCls cWrapper : taskList){
        	if(cWrapper.isSelected){
            	hasSelectedTasks = true;
                chosenTasks.add(cWrapper.cTask);
            }
		}
	}
	
	public void displayAllTasks()
	{
		if (chosenTasks == null)
    		chosenTasks = new List<Task>();
    	else
    		chosenTasks.clear();
    
    	selectedPage = '0'; //Set the selectedPage and counter to zero so there is no offset in the SOQL query
		counter = 0;
		list_size = 500; //Maximum number of records to update
		this.getTaskList();
		for (TaskWrapperCls cWrapper : taskList) {
			chosenTasks.add(cWrapper.cTask);
		}
		
		//Set the OwnerTask Status to None so that the user is not forced to update, after the field is used in the filter query
    	ownerTask.Status = '-- None --';
		
		//If there are tasks that need to be updated, set the hasSelectedTasks so that the update page displays
		if (chosenTasks.size() > 0)
			hasSelectedTasks = true;
	}
	
	public void filterTasks()
	{
		this.getTaskList();
	}
	
	public PageReference persistChange()
	{
		//Set the chosen owner as the OwnerId for the chosen tasks
		for (Task t : chosenTasks) {
			t.OwnerId = ownerTask.OwnerId;
			
			if (ownerTask.Status != '-- None --' && ownerTask.Status != null)
				t.Status = ownerTask.Status;
		}
		try {
			update chosenTasks;
		}
		catch (DMLException e) {
			System.debug('DEBUG:'+e);
			ApexPages.addMEssages(e);
			return null;
		}
		return (new PageReference('/007')); //Task List View
	}
	
	public PageReference abandon() 
	{
		return (new PageReference('/007')); //Task List View
	}
	
	public PageReference empty() {
		return null;
	}
	
	public Component.Apex.pageBlockButtons getMyCommandButtons() 
    {
        //the reRender attribute is a set NOT a string
        Set<string> theSet = new Set<string>();
        theSet.add('myPanel');
        theSet.add('myButtons');
                
        integer totalPages;
        if (math.mod(total_size, list_size) > 0) {
            totalPages = total_size/list_size + 1;
        } else {
            totalPages = (total_size/list_size);
        }
        
        integer currentPage;        
        if (selectedPage == '0') {
            currentPage = counter/list_size + 1;
        } else {
            currentPage = integer.valueOf(selectedPage);
        }
     
        Component.Apex.pageBlockButtons pbButtons = new Component.Apex.pageBlockButtons();        
        pbButtons.location = 'top';
        pbButtons.id = 'myPBButtons';
        
        Component.Apex.outputPanel opPanel = new Component.Apex.outputPanel();
        opPanel.id = 'myButtons';
                                
        //the Previous button will alway be displayed
        Component.Apex.commandButton b1 = new Component.Apex.commandButton();
        b1.expressions.action = '{!Previous}';
        b1.title = 'Previous';
        b1.value = 'Previous';
        b1.expressions.disabled = '{!disablePrevious}';        
        b1.reRender = theSet;

        opPanel.childComponents.add(b1);        
                        
        for (integer i=0;i<totalPages;i++) 
        {
            Component.Apex.commandButton btn = new Component.Apex.commandButton();
            
            if (i+1==1) {
                btn.title = 'First Page';
                btn.value = 'First Page';
                btn.rendered = true;                                        
            } else if (i+1==totalPages) {
                btn.title = 'Last Page';
                btn.value = 'Last Page';
                btn.rendered = true;                            
            } else {
                btn.title = 'Page ' + string.valueOf(i+1) + ' ';
                btn.value = ' ' + string.valueOf(i+1) + ' ';
                btn.rendered = false;             
            }
            
            if (   (i+1 <= 5 && currentPage < 5)
                || (i+1 >= totalPages-4 && currentPage > totalPages-4)
                || (i+1 >= currentPage-2 && i+1 <= currentPage+2))
            {
                btn.rendered = true;
            }
                                     
            if (i+1==currentPage) {
                btn.disabled = true; 
                btn.style = 'color:blue;';
            }  
            
            btn.onclick = 'queryByPage(\''+string.valueOf(i+1)+'\');return false;';
                 
            opPanel.childComponents.add(btn);
            
            if (i+1 == 1 || i+1 == totalPages-1) { //put text after page 1 and before last page
                Component.Apex.outputText text = new Component.Apex.outputText();
                text.value = '...';        
                opPanel.childComponents.add(text);
            } 
             
        }
        
        //the Next button will alway be displayed
        Component.Apex.commandButton b2 = new Component.Apex.commandButton();
        b2.expressions.action = '{!Next}';
        b2.title = 'Next';
        b2.value = 'Next';
        b2.expressions.disabled = '{!disableNext}';        
        b2.reRender = theSet;
        opPanel.childComponents.add(b2);
                
        //Also load the 'Show Selected Tasks' button - always show
        Component.APex.commandButton bTask = new Component.Apex.commandButton();
        bTask.expressions.action = '{!displaySelectedTasks}';
        bTask.title = 'Update Selected Tasks';
        bTask.value = 'Update Selected Tasks';
        opPanel.childComponents.add(bTask);
        
        //Also load the 'Show Selected Tasks' button - always show
        Component.APex.commandButton allTask = new Component.Apex.commandButton();
        allTask.expressions.action = '{!displayAllTasks}';
        allTask.title = 'Update All Tasks';
        allTask.value = 'Update All Tasks';
        opPanel.childComponents.add(allTask);
                
        //add all buttons as children of the outputPanel                
        pbButtons.childComponents.add(opPanel);  
  
        return pbButtons;

    }    
    
    public PageReference refreshGrid() { //user clicked a page number        
        system.debug('**** ' + selectedPage);
        return null;
    }
    
    public PageReference Previous() { //user clicked previous button
        selectedPage = '0';
        counter -= list_size;
        return null;
    }

    public PageReference Next() { //user clicked next button
        selectedPage = '0';
        counter += list_size;
        return null;
    }

    public PageReference End() { //user clicked end
        selectedPage = '0';
        counter = total_size - math.mod(total_size, list_size);
        return null;
    }
    
    public Boolean getDisablePrevious() { //this will disable the previous and beginning buttons
        if (counter>0) 
        	return false; 
        else 
        	return true;
    }

    public Boolean getDisableNext() { //this will disable the next and end buttons
        if (counter + list_size < total_size) 
        	return false; 
        else 
        	return true;
    }

    public Integer getTotal_size() {
        return total_size;
    }
    
    public Integer getPageNumber() {
        return counter/list_size + 1;
    }

    public Integer getTotalPages() {
        if (math.mod(total_size, list_size) > 0) {
            return total_size/list_size + 1;
        } else {
            return (total_size/list_size);
        }
    }
    
        
	//*****************************************
    //Test Methods
    
    //Test - populating the Task Mass-Update with all filter scenarios
    public TestMethod static void testTaskFilterScenarios()
    {
    	//Find any user account to get the Assignee of the test tasks
    	User u = [SELECT Id FROM User WHERE IsActive = TRUE AND UserType = 'Standard' AND UserRoleId != '' AND alias NOT IN ('sadm', 'it') LIMIT 1];
    	
    	//Create test tasks
    	List<Task> tasks = new List<Task>();
    	Task t1 = new Task(OwnerId=u.Id, Subject='Test task 1', Status='Not Started', ActivityDate=Date.Today());
    	Task t2 = new Task(OwnerId=u.Id, Subject='Test task 2', Status='In Progress', ActivityDate=Date.Today());
    	Task t3 = new Task(OwnerId=u.Id, Subject='Test task 3', Status='Completed', ActivityDate=Date.Today().addDays(3));
    	Task t4 = new Task(OwnerId=u.Id, Subject='Test task 4', Status='In Progress', ActivityDate=Date.Today().addDays(-4));
    	tasks.add(t1);
    	tasks.add(t2);
    	tasks.add(t3);
    	tasks.add(t4);
    	insert tasks;
    	    	
    	//Construct the selectedTaskUpdate and set the filter OwnerId (normally done through logged in user)
    	selectedTaskUpdate stu = new selectedTaskUpdate();
    	stu.ownerTask.OwnerId = u.Id;
    	stu.ownerTask.Status = null;
    	
    	Test.startTest();
    	
    	//Execute the getTaskList() method to get all tasks using only default filter
    	TaskWrapperCls[] testTasks1 = stu.getTaskList();
    	System.assertEquals(testTasks1.size(), 4);
    	
    	//Set some filter criteria and query again
    	stu.ownerTask.Subject = 'Test task 1';
    	TaskWrapperCls[] testTasks2 = stu.getTaskList();
    	System.assertEquals(testTasks2.size(), 1);
    	
    	stu.ownerTask.Subject = '';
    	stu.ownerTask.Status = 'In Progress';
    	TaskWrapperCls[] testTasks3 = stu.getTaskList();
    	System.assertEquals(testTasks3.size(), 2);
    	
    	stu.ownerTask.Status = null;
    	stu.ownerTask.ActivityDate = Date.today();
    	TaskWrapperCls[] testTasks4 = stu.getTaskList();
    	System.assertEquals(testTasks4.size(), 2);
    	
    	Test.stopTest();
    }
    
    //Test - select certain Tasks into the chosen task collection from setting the isChecked flags
    public TestMethod static void testIsCheckedFlags()
    {
    	//Find any user account to get the Assignee of the test tasks
    	User u = [SELECT Id FROM User WHERE IsActive = TRUE AND UserType = 'Standard' AND UserRoleId != '' AND alias NOT IN ('sadm', 'it') LIMIT 1];
    	
    	//Create test tasks
    	List<Task> tasks = new List<Task>();
    	Task t1 = new Task(OwnerId=u.Id, Subject='Test task 1', Status='Not Started', ActivityDate=Date.Today());
    	Task t2 = new Task(OwnerId=u.Id, Subject='Test task 2', Status='In Progress', ActivityDate=Date.Today());
    	Task t3 = new Task(OwnerId=u.Id, Subject='Test task 3', Status='Completed', ActivityDate=Date.Today().addDays(3));
    	Task t4 = new Task(OwnerId=u.Id, Subject='Test task 4', Status='In Progress', ActivityDate=Date.Today().addDays(-4));
    	tasks.add(t1);
    	tasks.add(t2);
    	tasks.add(t3);
    	tasks.add(t4);
    	insert tasks;
    	    	
    	//Construct the selectedTaskUpdate and set the filter OwnerId (normally done through logged in user)
    	selectedTaskUpdate stu = new selectedTaskUpdate();
    	stu.ownerTask.OwnerId = u.Id;
    	stu.ownerTask.Status = null;
    	
    	Test.startTest();
    	
    	//Get all the Tasks available and select certain ones
    	TaskWrapperCls[] twc = stu.getTaskList(); //Populate task list
    	System.assertEquals(twc.size(), 4);
    	stu.taskList[0].isSelected = true;
    	stu.taskList[2].isSelected = true;
    	stu.taskList[1].isSelected = false;
    	stu.taskList[3].isSelected = false;
    	
    	stu.displaySelectedTasks();
    	
    	Test.stopTest();
    	
    	//Check 'chosenTasks' now has two entries
    	System.assertEquals(stu.chosenTasks.size(), 2);
    }
    
    //Test - Update the Owner of a collection of selected Tasks
    public TestMethod static void testUpdateSelectedTasksOwner()
    {
    	//Find any user account to get the Assignee of the test tasks
    	List<User> u = [SELECT Id FROM User WHERE IsActive = TRUE AND UserType = 'Standard' AND UserRoleId != '' AND alias NOT IN ('sadm', 'it') LIMIT 2];
    	System.assertEquals(u.size(), 2); //Make sure we got 2 subs!
    	
    	//Create test tasks
    	List<Task> tasks = new List<Task>();
    	Task t1 = new Task(OwnerId=u[0].Id, Subject='Test task 1', Status='Not Started', ActivityDate=Date.Today());
    	Task t2 = new Task(OwnerId=u[0].Id, Subject='Test task 2', Status='In Progress', ActivityDate=Date.Today());
    	Task t3 = new Task(OwnerId=u[0].Id, Subject='Test task 3', Status='Completed', ActivityDate=Date.Today().addDays(3));
    	Task t4 = new Task(OwnerId=u[0].Id, Subject='Test task 4', Status='In Progress', ActivityDate=Date.Today().addDays(-4));
    	tasks.add(t1);
    	tasks.add(t2);
    	tasks.add(t3);
    	tasks.add(t4);
    	insert tasks;
    	    	
    	//Construct the selectedTaskUpdate and set the filter OwnerId (normally done through logged in user)
    	selectedTaskUpdate stu = new selectedTaskUpdate();
    	stu.ownerTask.OwnerId = u[0].Id;
    	stu.ownerTask.Status = null;
    	
    	Test.startTest();
    	
    	//Get all the Tasks available and select certain ones
    	TaskWrapperCls[] twc = stu.getTaskList(); //Populate task list
    	System.assertEquals(twc.size(), 4);
    	stu.taskList[0].isSelected = true;
    	stu.taskList[2].isSelected = true;
    	stu.taskList[1].isSelected = false;
    	stu.taskList[3].isSelected = false;
    	
    	stu.displaySelectedTasks();
    	
    	//Set the new Owner ID to assign the tasks to
    	stu.ownerTask.ownerId = u[1].Id;
    	PageReference prf = stu.persistChange();
    	
    	Test.stopTest();
    	
    	//Query for tasks assigned to each of the two users originally found
    	List<Task> user1Tasks = [SELECT Id FROM Task WHERE OwnerId = :u[0].Id];
    	List<Task> user2Tasks = [SELECT Id FROM Task WHERE OwnerId = :u[1].Id];
    	System.assertEquals(user1Tasks.size(), 2);
    	System.assertEquals(user2Tasks.size(), 2);
    	
    	System.assertEquals(prf.getURL(), '/007');
    }
    
    //Test - Update the Owner and Status of all filtered Tasks
    public TestMethod static void testUpdateAllTasksOwner()
    {
    	//Find any user account to get the Assignee of the test tasks
    	List<User> u = [SELECT Id FROM User WHERE IsActive = TRUE AND UserType = 'Standard' AND UserRoleId != '' AND alias NOT IN ('sadm', 'it') LIMIT 2];
    	System.assertEquals(u.size(), 2); //Make sure we got 2 users!
    	
    	//Create test tasks
    	List<Task> tasks = new List<Task>();
    	Task t1 = new Task(OwnerId=u[0].Id, Subject='Test task 1', Status='Not Started', ActivityDate=Date.Today());
    	Task t2 = new Task(OwnerId=u[0].Id, Subject='Test task 2', Status='In Progress', ActivityDate=Date.Today());
    	Task t3 = new Task(OwnerId=u[0].Id, Subject='Test task 3', Status='Completed', ActivityDate=Date.Today().addDays(3));
    	Task t4 = new Task(OwnerId=u[0].Id, Subject='Test task 4', Status='In Progress', ActivityDate=Date.Today().addDays(-4));
    	tasks.add(t1);
    	tasks.add(t2);
    	tasks.add(t3);
    	tasks.add(t4);
    	insert tasks;
    	    	
    	//Construct the selectedTaskUpdate and set the filter OwnerId (normally done through logged in user)
    	selectedTaskUpdate stu = new selectedTaskUpdate();
    	stu.ownerTask.OwnerId = u[0].Id;
    	stu.ownerTask.Status = null;
    	
    	Test.startTest();
    	
    	//Get all the Tasks available and select certain ones
    	TaskWrapperCls[] twc = stu.getTaskList(); //Populate task list
    	System.assertEquals(twc.size(), 4);
    	
    	stu.displayAllTasks();
    	
    	//Set the new Owner ID to assign the tasks to
    	stu.ownerTask.ownerId = u[1].Id;
    	stu.ownerTask.status = 'Completed';
    	PageReference prf = stu.persistChange();
    	
    	Test.stopTest();
    	
    	//Query for tasks assigned to each of the two users originally found
    	List<Task> user1Tasks = [SELECT Id, Status FROM Task WHERE OwnerId = :u[0].Id];
    	List<Task> user2Tasks = [SELECT Id, Status FROM Task WHERE OwnerId = :u[1].Id];
    	System.assertEquals(user1Tasks.size(), 0);
    	System.assertEquals(user2Tasks.size(), 4);
    	
    	//Also check that all 4 statuses were updated to 'Complete'
    	for (Task t : user2Tasks) {
    		System.assertEquals(t.Status, 'Completed');
    	}
    	
    	System.assertEquals(prf.getURL(), '/007');
    }
    
    //Test - check cancel goes to the correct page
    public TestMethod static void testCancelPage()
    {
    	selectedTaskUpdate stu = new selectedTaskUpdate();
    	
    	Test.startTest();
    	PageReference prf = stu.abandon();
    	Test.stopTest();
    	
    	System.assertEquals(prf.getURL(), '/007');
    }
    
    //Test - the pagination buttons?
	public TestMethod static void testPaginationButtons()
	{
		//Add a large number of test tasks so we can paginate between them
		User u = [SELECT Id FROM User WHERE IsActive = TRUE AND UserType = 'Standard' AND UserRoleId != '' AND alias NOT IN ('sadm', 'it') LIMIT 1];
		List<Task> tasks = new List<Task>();
		
		for (integer i = 0; i < 100; i++)
		{
			Task t = new Task(OwnerId=u.Id, Subject='Test task '+i, Status='Not Started', ActivityDate=Date.Today());
			tasks.add(t);
		}
		insert tasks;
		
		selectedTaskUpdate stu = new selectedTaskUpdate();
		stu.ownerTask.OwnerId = u.Id;
    	stu.ownerTask.Status = null;
		
		Test.startTest();
		
		TaskWrapperCls[] twc = stu.getTaskList(); //Populate task list
		
		//Activate all our pagination
		stu.getMyCommandButtons();
		stu.refreshGrid();
		
		//Check default counter value
		System.assertEquals(stu.counter, 0);
		
		stu.Next();
		System.assertEquals(stu.counter, 20);
		
		stu.Next();
		System.assertEquals(stu.counter, 40);
		
		stu.Previous();
		System.assertEquals(stu.counter, 20);
		
		stu.End();
		System.assertEquals(stu.counter, 100);
		
		System.assertEquals(stu.getDisableNext(), true);
		System.assertEquals(stu.getDisablePrevious(), false);
		System.assertEquals(stu.getTotal_size(), 100);
		System.assertEquals(stu.getPageNumber(), 6);
		System.assertEquals(stu.getTotalPages(), 5);
		
		Test.stopTest();
	}
}