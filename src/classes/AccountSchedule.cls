/**
 * AccountSchedule is a class that sets up a schedulable job to update account fields
 * that depend on time related events e.g. start/end of current subscription.
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
global class AccountSchedule implements Schedulable {
	
	global void execute(SchedulableContext ctx) {
		AccountHelper helper = new AccountHelper();
		
		// list of active and inactive - kept separate so that i know what has already changed
		List<Account> accounts = new List<Account>();
		List<Account> active = helper.getActiveAccounts();
		List<Account> inactive = helper.getInactiveAccounts();
		
		// merge the two list since the trigger will do it's own checking
		accounts.addAll(active);
		accounts.addAll(inactive);
		
		AccountHelper scriptBatch = new AccountHelper(accounts);
		ID batchprocessid = Database.executeBatch(scriptBatch, 1); // execute one at a time
	}
	
	/**
	 *
	 */
	global static void start(String jobName, String scheduledTime) {
		//'0 0 * * * ?' calls function every hour
  		AccountSchedule schedule = new AccountSchedule();
  		String job_id = System.schedule(jobName, scheduledTime, schedule);
	}
	
    /**
	 *
	 */
	global static void startDefaultTime() {
		AccountSchedule.start('UpdateActiveAccounts', '0 0 1 * * ?');
	}
	
	/********************************************************************/
}