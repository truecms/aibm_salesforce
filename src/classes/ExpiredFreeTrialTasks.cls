/**
* Batch job that runs nightly to pull all Accounts that are not subscribers and will finish their FT in 10 days time
* A task for the Sales Team to follow up will be created for each of these customers 
* Author: tailbyr
* Copyright: EurekaReport.com.au
*/
public with sharing class ExpiredFreeTrialTasks implements Schedulable, Database.Batchable<Account>, Database.Stateful
{
	public static String lastSalesTeamMember { get; set; }
	
	public TaskManager tm {get; set;}
	
	public String exFTAllocationQueue {get; set;}
	public Boolean exFTAllocationRules {get; set;}
	
	//Constructor - get the lastSalesTeamMember given one of the 'Expired FT' tasks
	public ExpiredFreeTrialTasks()
	{
		// Get the Salesperson/Owner of the last Expired FT task so that we can assign the tasks equally
		List<Task> lastExpiredFreeTrialTask = [SELECT Id, ownerId FROM Task WHERE Subject = :TaskManager.EXPIRED_FREE_TRIAL_SUBJECT ORDER BY createdDate desc LIMIT 1]; 
		if (lastExpiredFreeTrialTask.size()>0)
			lastSalesTeamMember = lastExpiredFreeTrialTask[0].ownerId;
			
		//Get the customer settings allocation rule and queue for this Expired FT automated task creator
		Allocation_Rules__c allocationRules = Allocation_Rules__c.getInstance('Default');
		exFTAllocationRules = allocationRules.Expired_FT_Allocation_Rule__c;
		exFTAllocationQueue = allocationRules.Expired_FT_Queue__c;
		
		tm = new TaskManager(exFTAllocationQueue); //Instigate the TaskManager and tell it which User Queue to use from Custom Settings
	}
	
	//Schedulable override method: execute - called when the scheduled job is kicked off
	public void execute(SchedulableContext ctx)
	{
		Database.executeBatch(new ExpiredFreeTrialTasks(), 1);
	}
	
	//Batchable override method: start - queries the records to iterate over
	public Iterable<Account> start(Database.batchableContext BC) 
	{
		Date expiredFTDate = System.Today().addDays(10); //SF-878 Active FTs due to expire in 10 days
		List<Account> expiredFTAccounts = [SELECT id, OwnerId, PersonContactId 
											FROM Account 
											WHERE ER_Reporting_Sub_Type__c = 'f'
											AND ER_Reporting_Sub_Status__c = '3'
											AND ER_Reporting_Sub_End_Date__c = :expiredFTDate];
		
		return expiredFTAccounts;
	}
	
	//Batchable override method: execute
	public void execute(Database.BatchableContext BC, List<Account> instances)
	{
		AccountHelper aHelper = new AccountHelper(instances);
		aHelper.createExpiredFTTasks(exFTAllocationRules, tm);
	}
	
	//Batchable override method: finish - called when all batches are processed; allocates all fair tasks created in a fair manner across Acc mgrs
	public void finish(Database.BatchableContext BC)
	{
		//Now all the tasks have been created, grab all created tasks and reallocate fairly where an alternative assignment rule has not been set
		List<Task> allExpiredFTTasksToday = [SELECT Id, ownerId FROM Task 
				where Subject = :TaskManager.EXPIRED_FREE_TRIAL_SUBJECT
				AND createdDate = TODAY
				AND CallDisposition = :TaskManager.ALLOCATION_DISPOSITION];
				
		System.debug('DEBUG: allExpiredFTTasksToday:' + allExpiredFTTasksToday.size());		
		
		for (Task t : allExpiredFTTasksToday) 
		{
			String nextSalesTeamMember = tm.getNextSalesTeamMember(ExpiredFreeTrialTasks.lastSalesTeamMember);
			t.OwnerId = nextSalesTeamMember;
			ExpiredFreeTrialTasks.lastSalesTeamMember = nextSalesTeamMember;	
		}
		
		update allExpiredFTTasksToday;
	}
	
	/*
		Method used to create the scheduling of this Batch Job. This is used by the 'TestScheduledJobs' function which enables the scheduled jobs to be 
		setup and removed programmatically (also via API for Jenkins build job)
	*/
	public static void setupSchedule(String jobName, String scheduledTime) { 
		ExpiredFreeTrialTasks scheduled_task = new ExpiredFreeTrialTasks();
		String payment_id = System.schedule(jobName, scheduledTime, scheduled_task);
	}
}