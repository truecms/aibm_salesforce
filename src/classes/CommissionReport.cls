public with sharing class CommissionReport {

	public List<DataRow> results {get; set;}
		
	public List<User> users {get; set;}
	public Product_Instance__c dateInstance {get; set;}
	public go1__Transaction__c startDate {get; set;}
	public go1__Transaction__c endDate {get; set;}
	
	public List<SummaryRow> summaryResults {get; set;}
	
	public CommissionReport(){		
		users = [SELECT Id FROM User WHERE Alias NOT IN ('sadm', 'it') and IsActive = true];
		this.dateInstance = new Product_Instance__c();
		this.startDate = new go1__Transaction__c();
		this.endDate = new go1__Transaction__c();
		
		//Default start and end dates
		Datetime defaultDate = Datetime.now();
		this.dateInstance.Start_Date__c = Date.newInstance(defaultDate.addMonths(-1).year(), defaultDate.addMonths(-1).month(), 1);
		this.dateInstance.End_Date__c = this.dateInstance.Start_Date__c.addDays(Date.daysInMonth(this.dateInstance.Start_Date__c.year(), this.dateInstance.Start_Date__c.month()) - 1);
		
		initData();
	}
	
	public void initData(){
		this.results = new List<DataRow>();
		
		this.startDate.go1__Date__c = Datetime.newInstance(this.dateInstance.Start_Date__c.year(), this.dateInstance.Start_Date__c.month(), this.dateInstance.Start_Date__c.day(), 0, 0, 0);
		this.endDate.go1__Date__c = Datetime.newInstance(this.dateInstance.End_Date__c.year(), this.dateInstance.End_Date__c.month(), this.dateInstance.End_Date__c.day(), 23, 59, 59);		
		
		List<go1__Transaction__c> transList = [SELECT Id, 
												createdBy.Name, 
												go1__Date__c, 
												createdDate,
												Name, 
												go1__Account__r.PersonEmail, 
												go1__Amount__c, 
												Subscription_Instance__r.Start_Date__c,
												Campaign__c,
												go1__Account__c,
												Subscription_Instance__c
												FROM go1__Transaction__c
												WHERE go1__Status__c IN ('Approved','Success','Successful Transaction','Successfully added manual transaction')
												AND Subscription_Instance__r.Status__c != 'Cancelled'
												AND go1__Refunded__c != TRUE
												AND go1__Amount__c > 0
												AND createdDate > :this.startDate.go1__Date__c
												AND createdDate < :this.endDate.go1__Date__c
												AND createdById IN :users];
							
		Map<Id, List<DataRow>> dataLookup = new Map<Id, List<DataRow>>();
							
		for (go1__Transaction__c tr : transList) {
			DataRow dr = new DataRow(tr);
			this.results.add(dr);
			
			if (dataLookup.containsKey(tr.go1__Account__c)){
				List<DataRow> tempRows = dataLookup.get(tr.go1__Account__c);
				tempRows.add(dr);
				dataLookup.put(tr.go1__Account__c, tempRows);
				tempRows = null;
			}
			else {
				List<DataRow> tempRow = new List<DataRow>();
				tempRow.add(dr);
				dataLookup.put(tr.go1__Account__c, tempRow);
				tempRow = null;
			}
		}
		
		List<AggregateResult> latestPayments = [SELECT go1__Account__c, max(go1__Date__c) last_payment_date
												FROM go1__Transaction__c
												WHERE go1__Status__c IN ('Approved','Success','Successful Transaction','Successfully added manual transaction')
												AND Subscription_Instance__r.Status__c != 'Cancelled'
												AND go1__Refunded__c != TRUE
												AND go1__Amount__c > 0
												AND go1__Account__c IN :dataLookup.keySet()
												GROUP BY go1__Account__c];
												
		for (AggregateResult ar : latestPayments) {
			Id accountId = (Id)ar.get('go1__Account__c');
			if (dataLookup.containsKey(accountId)){
				List<DataRow> tempDrs = dataLookup.get(accountId);
				for (DataRow tempDr : tempDrs) {
					tempDr.lastPaymentDate = (Datetime)ar.get('last_payment_date');
				}
				dataLookup.put(accountId, tempDrs);
				tempDrs = null;
			}
		}
		
		
		List<Product_Instance__c> insts = [SELECT Person_Account__c, End_Date__c,
								(SELECT Name FROM Transactions__r WHERE go1__refunded__c != TRUE AND go1__Amount__c > 0) translist
											FROM Product_Instance__c
											WHERE Person_Account__c IN :dataLookup.keySet()
											AND Subscription_Stream__c = 'Eureka Report'
											AND Subscription_Type__c IN ('m', 'y')
											AND Status__c != 'Cancelled'
											ORDER BY Person_Account__c, End_Date__c DESC];
											
		Date prevSubEnd = null;
		Id tempAccId = null;
		Integer counter = 0;
		for (Product_Instance__c p : insts) {
			List<go1__Transaction__c> trans = p.Transactions__r;
			if (trans.size()<1) //No valid transactions against this account, ignore the Product Instance
				continue;
			
			if (p.Person_Account__c != tempAccId) {
				tempAccId = p.Person_Account__c;
				prevSubEnd = null; //Reset lastSubEnd so we don't set it to a previous acc value
				counter = 0;
			}
			else 
				counter++;
			
			if (prevSubEnd == null) {
				prevSubEnd = p.End_Date__c;
			}
			
			//Set up the previous sub end date - only if there is more than 1 sub, otherwise this value should be NULL
			if (counter == 1 && p.End_Date__c < prevSubEnd) {
				List<DataRow> tempDrs = dataLookup.get(p.Person_Account__c);
				for (DataRow tempDr : tempDrs) {
					tempDr.previousSubEnd = p.End_Date__c;
				}
				dataLookup.put(p.Person_Account__c, tempDrs);
				tempDrs = null;
			}
				
		}
		
		//Now our results are fully populated, apply the custom sort for ease of reading the Output
		results.sort();
		
		//Loop round all of our final DataRows and populate the Summary Map matrix (Is there a more efficient way for this?)
		Map<String, SummaryRow> summaryMap = new Map<String, SummaryRow>(); 
		for (DataRow dr : results) {
			SummaryRow sr = null;
			
			if (summaryMap.containsKey(dr.trans.CreatedBy.Name)){
				sr = summaryMap.get(dr.trans.CreatedBy.Name);
			}
			else {
				sr = new SummaryRow(dr.trans.CreatedBy.Name);
			}
			
			if (dr.transactionType == 'New') {
				sr.newCount++;
				sr.newSumValue += dr.trans.go1__Amount__c;
			}
			else if (dr.transactionType == 'Winback') {
				sr.winbackCount++;
				sr.winbackSumValue += dr.trans.go1__Amount__c;
			}
			else if (dr.transactionType == 'Late Renewal') {
				sr.lateRenewalCount++;
				sr.lateRenewalSumValue += dr.trans.go1__Amount__c;
			}
			else if (dr.transactionType == 'Renewal') {
				sr.renewalCount++;
				sr.renewalSumValue += dr.trans.go1__Amount__c;
			}
			
			summaryMap.put(sr.salesperson, sr);
		}
		this.summaryResults = summaryMap.values();
		
	}
	
	//Downloads the data from the Commission report into an Excel template
	public PageReference downloadReport() {
		PageReference exportPage = new PageReference('/apex/Commission_report_export');
		exportPage.setRedirect(false);
		return exportPage;
	}
	
	public Class SummaryRow {
		public String salesperson {get; set;}
		public Integer newCount {get; set;}
		public Decimal newSumValue {get; set;}
		public Integer winbackCount {get; set;}
		public Decimal winbackSumValue {get; set;}
		public Integer lateRenewalCount {get; set;}
		public Decimal lateRenewalSumValue {get; set;}
		public Integer renewalCount {get; set;}
		public Decimal renewalSumValue {get; set;}
		
		public Integer totalCount {
			get {
				return (this.newCount + this.winbackCount + this.lateRenewalCount + this.renewalCount);
			}
			
			set;
		}
		
		public Decimal totalSumValue {
			get{
				return (this.newSumValue + this.winbackSumValue + this.lateRenewalSumValue + this.renewalSumValue);
			} 
			
			set;
		}
		
		public SummaryRow(String salesperson) {
			this.salesperson = salesperson;
			this.newCount = 0;
			this.newSumValue = 0.00;
			this.winbackCount = 0;
			this.winbackSumValue = 0.00;
			this.lateRenewalCount = 0;
			this.lateRenewalSumValue = 0.00;
			this.renewalCount = 0;
			this.renewalSumValue = 0.00;
		}
	}
	
	public Class DataRow implements Comparable{
		
		public go1__Transaction__c trans {get; set;}
		public Datetime lastPaymentDate {get; set;}
		public Date lastPayment {
			get {
				if (this.lastPaymentDate == null)
					return null;
				else
					return Date.newInstance(this.lastPaymentDate.year(), this.lastPaymentDate.month(), this.lastPaymentDate.day());
			}
			set;}
			
		public Date previousSubEnd {get; set;}
		
		public Date paymentDateDisplay {
			get {
				if (this.trans != null && this.trans.go1__Date__c != null)
					return Date.newInstance(this.trans.go1__Date__c.year(), this.trans.go1__Date__c.month(), this.trans.go1__Date__c.day());
				else
					return null;
			}
			
			set;
		}
		
		public Date createdDateDisplay {
			get {
				if (this.trans != null)
					return Date.newInstance(this.trans.createdDate.year(), this.trans.createdDate.month(), this.trans.createdDate.day());
				else
					return null;
			}
			
			set;
		}
		
		public Integer daysLapsed {
			get {
				if (previousSubEnd == null || this.trans.Subscription_Instance__r.Start_Date__c == null)
					return null;
				else
					return previousSubEnd.daysBetween(this.trans.Subscription_Instance__r.Start_Date__c);
			}
			 
			set;}
		
		public String transactionType {
			get {
				if (this.daysLapsed == null)
					return 'New';
				else if (this.daysLapsed >= 1 && this.daysLapsed <= 30)
					return 'Late Renewal';
				else if (this.daysLapsed > 30)
					return 'Winback';
				else
					return 'Renewal';
			} 
			
			set;}
		
		public DataRow(go1__Transaction__c trans) {
			this.trans = trans;
		}
		
		//Concrete implementation of the Interface Comparable compareTo method stub
		public Integer compareTo(Object compareTo) {
			DataRow tempDr = (DataRow)compareTo;
			if (this.trans.CreatedBy.Name == tempDr.trans.CreatedBy.Name) {
				if (this.transactionType == tempDr.transactionType)
					return 0;
				else if (this.transactionType > tempDr.transactionType)
					return 1;
				else 
					return -1;
			}
			else if (this.trans.CreatedBy.Name > tempDr.trans.CreatedBy.Name) return 1;
			else return -1;
		}
		
	}

}