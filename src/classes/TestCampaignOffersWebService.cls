/**
 * TestCampaignOffersWebService
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
@isTest 
public class TestCampaignOffersWebService {
	
	/**
	 * Tests the six possible outcomes from the webservice e.g. no account, no campaign, no valid child campaigns...
	 */
	 /*
	static testMethod void testWebService() {
		// insert a test account to allow the web service to query it
       	Account account = new Account(FirstName='Test',LastName='Tester',PersonEmail='test@tester.com');
       	insert account;
       	Subscription_Stream__c stream = new Subscription_Stream__c(Name='Test Stream');
		insert stream;
		Subscription_Product__c product = new Subscription_Product__c(Name='Test Subscription',Subscription_Stream__c=stream.Id);
		insert product;
		Product_Instance__c sub = new Product_Instance__c(Person_Account__c=account.Id,Subscription_Product__c=product.Id,Start_Date__c=Date.today(),End_Date__c=Date.today().addDays(7));
		insert sub;
       	
       	// test the first error scenario where no account contains the email parameter
        String expectedResponse1 = '{Response:\'Error\', Message:\'There are no accounts with the email: test1@tester.com!\'}';
        String testResponse1 = CampaignOffersWebService.doPost('test1@tester.com');
        System.assertEquals(expectedResponse1, testResponse1);
       	
       	// test the second error scenario where there are no valid campaigns
        String expectedResponse2 = '{Response:\'Error\', Message:\'There are no valid campaigns in the system!\'}';
		String testResponse2 = CampaignOffersWebService.doPost('test@tester.com');
		System.assertEquals(expectedResponse2, testResponse2);
		
		Campaign campaign = new Campaign(Name='Test Campaign', EndDate=Date.today().addDays(7));
       	insert campaign;
       	
       	// test the default successful response
        String expectedResponse3 = '{Response:\'Successful\', Parent_Campaign:' + campaign.Id + ', Campaign:' + null + ', Yearly_Product:' + null + ', Monthly_Product:' + null + ', Misc_Product:' + null + '}';
		String testResponse3 = CampaignOffersWebService.doPost('test@tester.com');
		System.assertEquals(expectedResponse3, testResponse3);
       	
       	Contact contact = [SELECT Id FROM Contact WHERE AccountId = :account.Id];
       	CampaignMember member = new CampaignMember(CampaignId=campaign.Id,ContactId=contact.Id);
       	insert member;
		
		// test the second successful scenario where there is a campaign associated with an account but does not contain any child campaigns
		String expectedResponse4 = '{Response:\'Successful\', Parent_Campaign:' + campaign.Id + ', Campaign:' + null + ', Yearly_Product:' + null + ', Monthly_Product:' + null + ', Misc_Product:' + null + '}';
		String testResponse4 = CampaignOffersWebService.doPost('test@tester.com');
		System.assertEquals(expectedResponse4, testResponse4);
		
		Campaign childCampaign = new Campaign(Name='Test Child Campaign',ParentId=campaign.Id,EndDate=Date.today().addDays(4));
		insert childCampaign;
		
		// test the forth successful scenario where account is associated with a campaign that contains a child campaign but is not valid
		String expectedResponse5 = '{Response:\'Successful\', Parent_Campaign:' + campaign.Id + ', Campaign:' + null + ', Yearly_Product:' + null + ', Monthly_Product:' + null + ', Misc_Product:' + null + '}';
		String testResponse5 = CampaignOffersWebService.doPost('test@tester.com');
		System.assertEquals(expectedResponse5, testResponse5);
		
		childCampaign.Duration__c = 3;
		update childCampaign;
		
		// test the forth successful scenario where account is associated with a campaign that contains a valid child campaign
		String expectedResponse6 = '{Response:\'Successful\', Parent_Campaign:' + campaign.Id + ', Campaign:' + childCampaign.Id + ', Yearly_Product:' + null + ', Monthly_Product:' + null + ', Misc_Product:' + null + '}';
		String testResponse6 = CampaignOffersWebService.doPost('test@tester.com');
		//System.assertEquals(expectedResponse6, testResponse6);
	}
	*/
	/**
	 *
	 */
	 /*
	static testMethod void testScheduledTask() {
		Campaign campaign = new Campaign(Name='Test Campaign');
		insert campaign;
		Campaign offer1 = new Campaign(Name='Campaign Offer 1',ParentId=campaign.Id,Duration__c=22);
		insert offer1;
		Campaign offer2 = new Campaign(Name='Campaign Offer 2',ParentId=campaign.Id,Duration__c=10);
		insert offer2;
		
		Account account = new Account(FirstName='Test',LastName='Tester',Campaign_Offer__c=null);
		insert account;
		
		Subscription_Stream__c stream = new Subscription_Stream__c(Name='Test Stream');
		insert stream;
		Subscription_Product__c product = new Subscription_Product__c(Name='Test Subscription',Subscription_Stream__c=stream.Id);
		insert product;
		Product_Instance__c sub = new Product_Instance__c(Person_Account__c=account.Id,Subscription_Product__c=product.Id,Start_Date__c=Date.today(),End_Date__c=Date.today().addMonths(1));
		insert sub;
		
		List<Contact> contacts = [SELECT Id FROM Contact WHERE AccountId = :account.Id LIMIT 1];
		
		CampaignMember member = new CampaignMember(ContactId=contacts[0].Id,CampaignId=campaign.Id,Next_Offer_Date__c=Date.today().addDays(7));
		insert member;
		
		AddCampaignMembers.runTask();
		Account testAccount1 = [SELECT Id, Eureka_Status_Subscription_End_Date__c, Campaign_Offer__c FROM Account WHERE Id = :account.Id];
		CampaignMember testMember1 = [SELECT Id, Next_Offer_Date__c FROM CampaignMember WHERE Id = :member.Id];
		
		
		// Uncommetted because method is a future method
		//System.assertEquals(offer1.Id, testAccount1.Campaign_Offer__c);
		//System.assertEquals(testAccount1.Eureka_Status_Subscription_End_Date__c - 10, testMember1.Next_Offer_Date__c);
		
		AddCampaignMembers.runTask();
		Account testAccount2 = [SELECT Id, Eureka_Status_Subscription_End_Date__c, Campaign_Offer__c FROM Account WHERE Id = :account.Id];
		//CampaignMember testMember2 = [SELECT Id, Next_Offer_Date__c FROM CampaignMember WHERE Id = :member.Id];
		
		// Uncommetted because method is a future method
		//System.assertEquals(offer2.Id, testAccount2.Campaign_Offer__c);
		//System.assertEquals(testAccount2.Eureka_Status_Subscription_End_Date__c, testMember2.Next_Offer_Date__c);
		
		Test.startTest();
		AddCampaignMembers.start('Test Task', '0 0 * * * ?');
		Test.stopTest();
		
		// Test the delete campaign member trigger
		Campaign newCampaign = new Campaign(Name='New Campaign');
		insert newCampaign;
		CampaignMember newMember = new CampaignMember(ContactId=contacts[0].Id,CampaignId=newCampaign.Id,Next_Offer_Date__c=Date.today().addDays(7));
		insert newMember;
		
		delete newMember;
	}
	*/
}