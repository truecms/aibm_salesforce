/**
 * TestAccountHelper 
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
 @isTest
public class TestAccountHelper {
	
	static RecordType getRecordType()
	{
		// Find any person account record type
	    RecordType recordType = [ select Id, Name, DeveloperName from RecordType where SObjectType = 'Account' and IsPersonType = true limit 1 ];
	    return recordType;
	}
	
	public static Account createAccountOnly() {
		return createAccountOnly(1);	
	}
	
	public static Account createAccountOnly(Integer i){
		Account account = new Account(FirstName='Test'+i, LastName='Tester'+i, PersonEmail='test'+i+'@test.com.test');
  		account.Sub_Status__c = '4';  // paid subscription
  		insert account;
    	
        return account;
	}
	
	public static List<Account> createAccounts(Integer numAccounts){
		List<Account> accounts = new List<Account>();
		
		for (integer i = 0; i < numAccounts; i++) {
    		Account account = new Account(FirstName='Test' +i, LastName='Tester' +i, PersonEmail='test'+i+'@test.com.test');
	  		account.RecordTypeId = getRecordType().Id;
	  		account.AccountNumber = 'UnitTestAccount';
	  		accounts.add(account);
    	}
    	
    	insert accounts;
    	
    	return accounts;
	}
	
	public static Account createAccount() {
		return createAccount(1, true);
	}
	
	public static Account createAccount(Integer i, Boolean transactions) {
		String stream = 'Eureka Report';
  		
  		Subscription_Stream__c subStream = new Subscription_Stream__c(Name=stream);
  		insert subStream;
  		Subscription_Product__c product = new Subscription_Product__c(Subscription_Stream__c=subStream.Id);
  		insert product;
  		
  		Account account = new Account(FirstName='Test'+i, LastName='Tester'+i, PersonEmail='test'+i+'@test.com.test');
  		insert account;
  		
  		List<Product_Instance__c> instances = new List<Product_Instance__c>(); 
  		
  		Product_Instance__c instance1 = new Product_Instance__c();
  		instance1.Subscription_Product__c = product.Id;
  		instance1.Person_Account__c = account.Id;
  		instances.add(instance1);
  		
  		Product_Instance__c instance2 = new Product_Instance__c();
  		instance2.Subscription_Product__c = product.Id;
  		instance2.Person_Account__c = account.Id;
  		instances.add(instance2);
		
		Product_Instance__c instance3 = new Product_Instance__c();
		instance3.Subscription_Product__c = product.Id;
  		instance3.Person_Account__c = account.Id;
  		instance3.Subscription_Type__c = 'm';
  		instances.add(instance3);
		
  		Product_Instance__c instance4 = new Product_Instance__c();
		instance4.Subscription_Product__c = product.Id;
  		instance4.Person_Account__c = account.Id;
  		instances.add(instance4);
  		
  		insert instances;
  		
  		instance1.Start_Date__c = date.valueOf('2005-12-01');
  		instance1.End_Date__c = date.valueOf('2006-12-01');
  		instance2.Start_Date__c = date.valueOf('2006-12-01');
  		instance2.End_Date__c = date.valueOf('2007-12-01');
  		instance3.Start_Date__c = date.today();
  		instance3.End_Date__c = date.today().addDays(1);
  		instance4.Start_Date__c = date.valueOf('2020-12-01');
  		instance4.End_Date__c = date.valueOf('2021-12-01');
  		
  		update instances;
  		
  		//Indication of whether to include Transactions in the Unit Test data - adds a lot of managed pkg SOQL queries to the execution!
  		if (transactions == true)
			createAccountTransactions(account, instance1, instance2);
  		
  		return account;
	}
	
	public static void createAccountTransactions(Account a, Product_Instance__c instance1, Product_Instance__c instance2) {
		go1__CreditCardAccount__c creditcard = go1.PaymentHelper.savedTestCreditCard(a);
		
		List<go1__Transaction__c> transactions = new List<go1__Transaction__c>(); 
  		go1__Transaction__c trans1 = TestAccountHelper.savedTestTransaction(a, creditcard);
 		go1__Transaction__c trans2 = TestAccountHelper.savedTestTransaction(a, creditcard);
  		
  		if (instance1 != null)
  			trans1.Subscription_Instance__c = instance1.Id;
  			
  		if (instance2 != null)
  			trans2.Subscription_Instance__c = instance2.Id;
  			
  		transactions.add(trans1);
  		transactions.add(trans2);
  		
		insert transactions;
	}
	
	/*
	 * Replace this with method from go2debit package
	 */
	public static go1__Transaction__c savedTestTransaction(Account person_account, go1__CreditCardAccount__c creditcard) {
    	go1__Transaction__c trans = new go1__Transaction__c();
    	trans.go1__Account__c = person_account.Id;
    	trans.go1__Amount__c = 1.00;
    	trans.go1__Account_ID__c = creditcard.go1__Token__c;
    	trans.go1__Payment_Name__c = creditcard.Name;
    	trans.go1__Reference_Id__c = String.valueOf(Integer.valueOf(Math.random()*1000)) + String.valueOf(Integer.valueOf(Math.random()*1000));
    	trans.go1__Reference__c = '635008';
    	trans.go1__Code__c = 0;
    	trans.go1__Description__c = 'Test Transaction';
    	trans.go1__Refundable__c = true;
    	trans.go1__Status__c = 'Successful Transaction';
    	
    	//insert trans;
    	return trans;
    }
	
	/**
	 *
	 */
  	static testMethod void testGetSubscriptions() {
  		String stream = 'Eureka Report';
		Account account = TestAccountHelper.createAccount();
  		AccountHelper helper = new AccountHelper(account);
  		
  		test.startTest();
  		List<Product_Instance__c> instances = helper.getSubscriptions(stream);
  		System.assertEquals(4, instances.size());
  		test.stopTest();
  	}
  	
	/**
	 *
	 */
  	static testMethod void testUpdateAccount() {
  		String stream = 'Eureka Report';
		List<Account> accounts = new List<Account>();
		Account account = TestAccountHelper.createAccount();
		accounts.add(account);
		
  		AccountHelper helper = new AccountHelper(accounts);
  		 		
	  	test.startTest();
  		helper.LoadTransactionsInfo();
		helper.LoadSubscriptionInfo();
		test.stopTest();
		
		Product_Instance__c instance = [SELECT Id, Campaign__c FROM Product_Instance__c WHERE Start_Date__c = :Date.today() LIMIT 1]; 
  		System.assertEquals(helper.account.ER_Current_Active_Subscription__c, instance.Id);
  		
  		//instance = [SELECT Id, Campaign__c FROM Product_Instance__c WHERE Person_Account__c = :account.Id AND Subscription_Stream__c = 'Eureka Report' AND Start_Date__c <= :Date.today() AND End_Date__c >= :Date.today() ORDER BY Start_Date__c LIMIT 1];
  		//System.assertEquals(helper.account.Eureka_Current_Campaign__c, instance.Campaign__c);
  		
  		instance = [SELECT Id FROM Product_Instance__c WHERE Person_Account__c = :account.Id AND Subscription_Stream__c = 'Eureka Report' AND Subscription_Type__c != 'f' ORDER BY Start_Date__c LIMIT 1];
  		System.assertEquals(account.Eureka_Subscribed_Campaign__c, instance.Id);
  		
  		account = [SELECT Id, ER_Current_Active_Subscription__c FROM Account WHERE Id = :account.Id];
  		System.assert(account.ER_Current_Active_Subscription__c!=null);
  	}
  	
  	/**
  	 * Testing update multiple accounts - all should be manipulated through the trigger
  	 */
  	static testMethod void testUpdateAccounts() {
  		Integer numAccounts = 2;
  		
   		List<Account> accounts = new List<Account>();
  		for (Integer i = 0; i < numAccounts; i++) {
  			Account account = TestAccountHelper.createAccount(i, false);
  			accounts.add(account);
  		}
  		Test.startTest();
  		upsert(accounts);
  		Test.stopTest();
  		
  		// UnitTesting! We should only have these accounts in the DB
  		List<Product_Instance__c> accInsts = [SELECT Id, 
  													Campaign__c, 
  													Person_Account__c, 
  													Start_Date__c, 
  													End_Date__c,
  													Subscription_Type__c,
  													Person_Account__r.ER_Current_Active_Subscription__c,
  													Person_Account__r.Eureka_Subscribed_Campaign__c
  													FROM Product_Instance__c 
  													WHERE Subscription_Stream__c = 'Eureka Report'  
  													ORDER BY Start_Date__c];
  		
  		Set<Id> updatedAccounts = new Set<Id>();
  		
  		for (Product_Instance__c prodInst : accInsts) {
  			System.assert(prodInst.Person_Account__r.ER_Current_Active_Subscription__c!=null);
  			
  			if (prodInst.Start_Date__c == Date.today())
  				System.assertEquals(prodInst.Person_Account__r.ER_Current_Active_Subscription__c, prodInst.Id);
  				
  			if (prodInst.Subscription_Type__c != 'f')
  				System.assertEquals(prodInst.Person_Account__r.Eureka_Subscribed_Campaign__c, prodInst.Id);
  				
  			updatedAccounts.add(prodInst.Person_Account__c);		
  		}
  		
  		System.assertEquals(updatedAccounts.size(), numAccounts);
  	}
  	
  	static testMethod void testAccountHelper() {
  	  	Account account = TestAccountHelper.createAccount();
  		Test.startTest();
  		AccountHelper helper = new AccountHelper(account);
  		List<go1__Transaction__c> trans = helper.getTransactions();
  		System.assertEquals(2, trans.size());
  		
  		List<Product_Instance__c> instances = helper.getSubscriptions('Eureka Report');
  		System.assertEquals(4, instances.size());
  		Test.stopTest();
  	}
  	
  	static testMethod void testDefaultEmails() {
  		Account account = TestAccountHelper.createAccount();
  		Test.startTest();
  		AccountHelper helper = new AccountHelper(account);
  		
  		Subscription_Stream__c subStream = new Subscription_Stream__c(Name='Eureka Report');
  		insert subStream;
  		Subscription_Product__c product = new Subscription_Product__c(Subscription_Stream__c=subStream.Id);
  		insert product;
  		helper.setDefaultEmailPreferences(product);
  		
  		Test.stopTest();
  		
  		System.assertEquals(true, helper.account.Webinar_Email__c);
  		System.assertEquals(true, helper.account.WE_Briefing__c);
  		System.assertEquals(true, helper.account.Publication__c);
  		
  		/*subStream = new Subscription_Stream__c(Name='Business Spectator');
  		insert subStream;
  		product = new Subscription_Product__c(Subscription_Stream__c=subStream.Id);
  		insert product;
  		helper.setDefaultEmailPreferences(product);
  		
  		System.assertEquals(true, helper.account.Morning_Briefing__c);
  		System.assertEquals(true, helper.account.KGB_Dossier__c);
  		System.assertEquals(true, helper.account.Afternoon_Update__c);*/
  	}
  	
  	static testMethod void testDefaultCC() {
  		Account account = new Account(LastName='test', PersonEmail='test@test.com.test');
  		insert account;
  		go1__CreditCardAccount__c creditcard = go1.PaymentHelper.savedTestCreditCard(account);
  		
  		Test.startTest();
  		AccountHelper helper = new AccountHelper(account);
  		go1__CreditCardAccount__c defaultCC = helper.getDefaultCreditCard();
  		go1__CreditCardAccount__c preferredCC = helper.getPreferredCreditCard();
  		Test.stopTest();
  		
  		System.assertEquals(creditcard.Id, defaultCC.Id);
  		System.assertEquals(creditcard.Id, preferredCC.Id);
  	}
  	
  	/**
  	 * Test a credit card is automatically assigned as the preferred cc when created
  	 * and updates all the objects that reference the credit card object
  	 */
  	static testMethod void testNewPreferredCC() {
  		Subscription_Stream__c stream = new Subscription_Stream__c(Name='Eureka Report');
  		insert stream;
  		Subscription_Product__c product = new Subscription_Product__c();
  		product.Subscription_Stream__c = stream.Id;
  		product.Duration__c = 1;
  		product.Duration_units__c = 'year';
  		product.Subscription_Type__c = 'y';
  		product.Price__c = 100;
  		insert product;
  		
  		Account account = new Account(LastName='test', PersonEmail='test@test.com.test');
  		insert account;
  		go1__CreditCardAccount__c oldCreditcard = go1.PaymentHelper.savedTestCreditCard(account);
  		
  		Product_Instance__c instance = new Product_Instance__c();
  		instance.Person_Account__c = account.Id;
  		instance.Subscription_Product__c = product.Id;
  		instance.Subscription_Type__c = 'y';
  		instance.Amount__c = 100;
  		instance.Start_Date__c = date.today();
  		instance.Auto_Renewal__c = true;
  		insert instance;
  		
  		ProductInstanceHelper productHelper = new ProductInstanceHelper(instance);
    	go1__Recurring_Instance__c recurring = productHelper.setupRecurringPayments();
  		insert recurring;
  		
  		AccountHelper helper = new AccountHelper(account);
  		System.assertEquals(oldCreditcard.Id, helper.getPreferredCreditCard().Id);
  		System.assertEquals(oldCreditcard.Id, recurring.go1__Credit_Card_Account__c);
  		
  		Test.startTest();
  		go1__CreditCardAccount__c newCreditcard = go1.PaymentHelper.savedTestCreditCard(account);
  		Test.stopTest();
  		
  		recurring = [SELECT go1__Credit_Card_Account__c FROM go1__Recurring_Instance__c WHERE Id = :recurring.Id];
  		
  		System.assertEquals(newCreditcard.Id, helper.getPreferredCreditCard().Id);
  		System.assertEquals(newCreditcard.Id, recurring.go1__Credit_Card_Account__c);
  	}
  	
  	/**
  	 * Test that account helper loads and stores all the accounts in a list from the 
  	 * account emails
  	 */
  	static testMethod void testLoadAccountEmails() {
  		Account account = createAccount();
  		AccountHelper helper = new AccountHelper();
  		
  		Test.startTest();
  		helper.loadAccountsByEmail(new List<String>{account.PersonEmail});
  		Test.stopTest();
  		
  		System.assertEquals(account.Id, helper.accounts[0].Id);
  	}
  	
  	/**
  	 * Test that all the accounts and child objects are deleted successfully
  	 */
  	static testMethod void testDeleteAccounts() {
  		Account account = createAccount();
  		AccountHelper helper = new AccountHelper(new List<Account>{account});
  		System.assertEquals(1, helper.accounts.size());
  		
  		List<Product_Instance__c> subs = [SELECT Id FROM Product_Instance__c WHERE Person_Account__c = :account.Id];
  		System.assertEquals(4, subs.size());
  		List<go1__Transaction__c> trans = [SELECT Id FROM go1__Transaction__c WHERE go1__Account__c = :account.Id];
  		System.assertEquals(2, trans.size());
  		List<go1__CreditCardAccount__c> cards = [SELECT Id FROM go1__CreditCardAccount__c WHERE go1__Account__c = :account.Id];
  		System.assertEquals(1, cards.size());
  		
  		Test.startTest();
  		helper.deleteAccounts();
  		Test.stopTest();
  		
  		System.assertEquals(0, helper.accounts.size());
  		
  		subs = [SELECT Id FROM Product_Instance__c];
  		System.assertEquals(0, subs.size());
  		trans = [SELECT Id FROM go1__Transaction__c];
  		System.assertEquals(0, trans.size());
  		cards = [SELECT Id FROM go1__CreditCardAccount__c];
  		System.assertEquals(0, cards.size());
  	}
  	
  	/** 
  	 * Test that an error occurs when insert/updating accounts with duplicate emails
  	 */
  	static testMethod void testDuplicateAccounts() {
  		Account account1 = new Account(FirstName='Test1', LastName='Tester', PersonEmail='test1@tester.com');
  		insert account1;
  		Account account2 = new Account(FirstName='Test2', LastName='Tester', PersonEmail='test1@tester.com');
  		
  		try {
  			insert account2;
  			System.assert(false);
  		} catch (DMLException e) {
  			System.assert(e.getNumDml() == 1);
	        System.assert(e.getDmlIndex(0) == 0);
	        System.assert(e.getDmlFields(0).size() == 1);
	        System.assert(e.getDmlMessage(0).indexOf('An account with this email address already exists.') > -1);
  		}
  	}
  	
  	static testMethod void AccountIsNew() {
  		Account account = new Account(FirstName='Test0',LastName='Tester0',PersonEmail='test0@tester.com');
  		
  		AccountHelper helper = new AccountHelper(account);
  		System.assertEquals(true, helper.isNewAccount());
	}
	
	static testMethod void AccountHasCurrentFreeTrial() {
		Subscription_Stream__c stream = new Subscription_Stream__c(Name='Eureka Report');
		insert stream;
		Subscription_Product__c product = new Subscription_Product__c(Name='Test Product', Subscription_Stream__c=stream.Id, Subscription_Type__c='f');
		insert product;
		Campaign campaign = new Campaign(Name='Test');
		insert campaign;
		
		Account account = new Account(FirstName='Test0',LastName='Tester0',PersonEmail='test0@tester.com');
		insert account; 
		
		AccountHelper helper = new AccountHelper(account);
		System.assertEquals(false, helper.hasCurrentEurekaFT());
		
		Product_Instance__c instance = new Product_Instance__c();
  		instance.Person_Account__c = account.Id;
  		instance.Subscription_Product__c = product.Id;
  		instance.Subscription_Type__c = 'f';
  		instance.Amount__c = 0;
  		insert instance;
  		instance.Start_Date__c = date.today().addDays(-10);
  		instance.End_Date__c = date.today().addDays(21);
  		instance.Auto_Renewal__c = true;
  		upsert instance;
		
		helper = new AccountHelper(account);
  		System.assertEquals(true, helper.hasCurrentEurekaFT());
	}
	
	static testMethod void AccountHasPaidSub() {
		Subscription_Stream__c stream = new Subscription_Stream__c(Name='Eureka Report');
		insert stream;
		Subscription_Product__c product = new Subscription_Product__c(Name='Test Product', Subscription_Stream__c=stream.Id, Subscription_Type__c='y');
		insert product;
		Campaign campaign = new Campaign(Name='Test');
		insert campaign;
		
		Account account = new Account(FirstName='Test0',LastName='Tester0',PersonEmail='test0@tester.com');
		insert account; 
		
		AccountHelper helper = new AccountHelper(account);
		System.assertEquals(false, helper.hasCurrentEurekaPaid());
		
		Product_Instance__c instance = new Product_Instance__c();
  		instance.Person_Account__c = account.Id;
  		instance.Subscription_Product__c = product.Id;
  		instance.Subscription_Type__c = 'y';
  		instance.Amount__c = 100;
  		insert instance;
  		instance.Start_Date__c = date.today().addDays(-10);
  		instance.End_Date__c = date.today().addDays(21);
  		instance.Auto_Renewal__c = true;
  		upsert instance;
		
		helper = new AccountHelper(account);
  		System.assertEquals(true, helper.hasCurrentEurekaPaid());
	}
	
	static testMethod void AccountHasPreviousPaidSub() {
		Subscription_Stream__c stream = new Subscription_Stream__c(Name='Eureka Report');
		insert stream;
		Subscription_Product__c product = new Subscription_Product__c(Name='Test Product', Subscription_Stream__c=stream.Id, Subscription_Type__c='y');
		insert product;
		Campaign campaign = new Campaign(Name='Test');
		insert campaign;
		
		Account account = new Account(FirstName='Test0',LastName='Tester0',PersonEmail='test0@tester.com');
		insert account; 
		
		AccountHelper helper = new AccountHelper(account);
		System.assertEquals(false, helper.hasPreviousEurekaPaid());
		
		Product_Instance__c instance = new Product_Instance__c();
  		instance.Person_Account__c = account.Id;
  		instance.Subscription_Product__c = product.Id;
  		instance.Subscription_Type__c = 'y';
  		instance.Amount__c = 100;
  		insert instance;
  		instance.Start_Date__c = date.today().addDays(-100);
  		instance.End_Date__c = date.today().addDays(-50);
  		instance.Auto_Renewal__c = true;
		upsert instance;
		
		helper = new AccountHelper(account);
		System.assertEquals(true, helper.hasPreviousEurekaPaid());
	}
	
	static testMethod void AccountHasPreviousFreeSub() {
		Subscription_Stream__c stream = new Subscription_Stream__c(Name='Eureka Report');
		insert stream;
		Subscription_Product__c product = new Subscription_Product__c(Name='Test ', Subscription_Stream__c=stream.Id, Subscription_Type__c='f');
		insert product;
		Campaign campaign = new Campaign(Name='Test');
		insert campaign;
		
		Account account = new Account(FirstName='Test0',LastName='Tester0',PersonEmail='test0@tester.com');
		insert account; 
		
		AccountHelper helper = new AccountHelper(account);
		System.assertEquals(false, helper.hasPreviousEurekaFT());
		
		Product_Instance__c instance = new Product_Instance__c();
  		instance.Person_Account__c = account.Id;
  		instance.Subscription_Product__c = product.Id;
  		instance.Subscription_Type__c = 'f';
  		instance.Amount__c = 0;
  		insert instance;
  		instance.Start_Date__c = date.today().addDays(-100);
  		instance.End_Date__c = date.today().addDays(-50);
  		instance.Auto_Renewal__c = true;
  		upsert instance;
		
		helper = new AccountHelper(account);
		System.assertEquals(true, helper.hasPreviousEurekaFT());
	}
	
	/*
	 * Testing the BrightDay element of the UpdateAccount BIU trigger
	 */
	static TestMethod void UpdateAccountDowngradeRoleSysAdmin() {
  		User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
  		Account a = new Account(FirstName='Test0',LastName='Tester0',PersonEmail='test0@tester.com');
  		
  		System.runAs(drupalUser) {
  			a.BD_Current_Role__c = 'Black';
  			insert a;
  		
  			Test.startTest();
  			a.BD_Current_Role__c = 'Client';
  			update a;
  			Test.stopTest();
  		}
  		
  		Account testAcc = [SELECT Id, BD_Current_Role__c, Force_Push__c FROM Account WHERE Id = :a.Id LIMIT 1];
  		System.assertEquals(testAcc.BD_Current_Role__c, 'Black');
  	}
  	
  	static TestMethod void UpdateAccountDowngradeToMemberSysAdmin() {
  		User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
  		Account a = new Account(FirstName='Test0',LastName='Tester0',PersonEmail='test0@tester.com');
  		
  		System.runAs(drupalUser) {
  			a.BD_Current_Role__c = 'Black';
  			insert a;
  			
  			//Check we have the correct values to start with
  			Account preTestAcc = [SELECT Id, BD_Current_Role__c, Force_Push__c FROM Account WHERE Id = :a.Id LIMIT 1];
  			System.assertEquals(preTestAcc.BD_Current_Role__c, 'Black');
  			System.assertEquals(preTestAcc.Force_Push__c, FALSE); 
  		
  			Test.startTest();
  			a.BD_Current_Role__c = 'Member';
  			update a;
  			Test.stopTest();
  		}
  		
  		Account testAcc = [SELECT Id, BD_Current_Role__c, Force_Push__c FROM Account WHERE Id = :a.Id LIMIT 1];
  		System.assertEquals(testAcc.BD_Current_Role__c, 'Member');
  		System.assertEquals(testAcc.Force_Push__c, FALSE);
  	}
  	
  	static TestMethod void UpdateAccountDowngradeRoleNonAdmin() {
  		User normalUser = [SELECT Id FROM User WHERE Name != 'System Admin' AND IsActive=true and UserType = 'Standard' and UserRoleId != '' LIMIT 1];
  		Account a = new Account(FirstName='Test0',LastName='Tester0',PersonEmail='test0@tester.com');
  		a.BD_Current_Role__c = 'Black';
  		insert a;
  		
  		System.runAs(normalUser) {
  			Test.startTest();
  			a.BD_Current_Role__c = 'Premium Client';
  			update a;
  			Test.stopTest();
  		}
  		
  		Account testAcc = [SELECT Id, BD_Current_Role__c, Force_Push__c FROM Account WHERE Id = :a.Id LIMIT 1];
  		System.assertEquals(testAcc.BD_Current_Role__c, 'Premium Client');
  		System.assertEquals(testACc.Force_Push__c, FALSE);
  	}
  	
  	static TestMethod void UpdateAccountNoRoleChange() {
  		User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
  		User normalUser = [SELECT Id FROM User WHERE Name != 'System Admin' AND IsActive=true and UserType = 'Standard' and UserRoleId != '' LIMIT 1];
  		Account a = new Account(FirstName='Test0',LastName='Tester0',PersonEmail='test0@tester.com');
  		a.BD_Current_Role__c = 'Member'; //Update only possible while Member, NOT client and up!
  		insert a;
  		
  		//Just make sure we are adding a new Account with a role already
  		Account preTest = [SELECT Id, BD_Current_Role__c, Force_Push__c FROM Account WHERE Id = :a.Id LIMIT 1];
  		System.assertEquals(preTest.BD_Current_Role__c, 'Member');
  		System.assertEquals(preTest.Force_Push__c, FALSE);
  		
  		Test.startTest();
  		
  		System.runAs(normalUser) {
  			a.FirstName='Test1';
  			update a;
  		}
  		
  		Account testAcc = [SELECT Id, BD_Current_Role__c, Force_Push__c FROM Account WHERE Id = :a.Id LIMIT 1];
  		System.assertEquals(testAcc.BD_Current_Role__c, 'Member');
  		System.assertEquals(testAcc.Force_Push__c, FALSE);
  		
  		System.runAs(drupalUser) {
  			a.FirstName='Test2';
  			update a;
  		}
  		
  		Test.stopTest();
  		
  		Account testAcc2 = [SELECT Id, BD_Current_Role__c, Force_Push__c FROM Account WHERE Id = :a.Id LIMIT 1];
  		System.assertEquals(testAcc2.BD_Current_Role__c, 'Member');  
  		System.assertEquals(testAcc2.Force_Push__c, FALSE);		
  	}
  	/* BrightDay tests end */
}