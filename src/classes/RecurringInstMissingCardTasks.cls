/**
* Batch to run hourly and create tasks for each Recurring Instance created within the last hour that does not have an attached Credit Card
* Author: tailbyr
* Copyright: EurekaReport.com.au
*/
global with sharing class RecurringInstMissingCardTasks implements Schedulable, Database.Batchable<Product_Instance__c>, Database.Stateful 
{

	public static String lastAccountManager { get; set; }
	
	public TaskManager tm {get; set;}
	
	public String creditCardQueue {get; set;}
	public Boolean creditCardAllocationRules {get; set;}
		
	//Constructor - get the lastAccountManager given one of the 'Hourly missing credit card' tasks
	public RecurringInstMissingCardTasks()
	{
		// Get the Account Manager/Owner of the last Hourly Missing Card task so that we can assign the tasks equally
		List<Task> lastHourlyNoCCPaymentTask = [SELECT Id, ownerId FROM Task WHERE Subject = :TaskManager.HOURLY_RECURRING_INST_MISSING_CARD_SUBJECT ORDER BY createdDate desc LIMIT 1];
		if (lastHourlyNoCCPaymentTask.size()>0)
			lastAccountManager = lastHourlyNoCCPaymentTask[0].ownerId;
			
		//Get the customer settings allocation rule and queue for this Expired FT automated task creator
		Allocation_Rules__c allocationRules = Allocation_Rules__c.getInstance('Default');
		creditCardAllocationRules = allocationRules.Credit_Card_Allocation_Rule__c;
		creditCardQueue = allocationRules.Credit_Card_Queue__c;
		
		tm = new TaskManager(creditCardQueue); //Instigate the TaskManager and tell it which User Queue to use from Custom Settings
	}
	
	//Schedulable override method: execute - called when the scheduled job is kicked off
	global void execute(SchedulableContext ctx)
	{
		Database.executeBatch(new RecurringInstMissingCardTasks(), 1);
	}
	
	//Batchable override method: start - queries the records to iterate over
	global Iterable<Product_Instance__c> start(Database.batchableContext BC) {
		
		Datetime lastHour = System.now().addHours(-1).addMinutes(-5); //Take 1hr 5 mins so there is an overlap and we don't miss any if the job doesn't kick off dead on the hour
		List<Product_Instance__c> piList = [SELECT Id, End_Date__c, Person_Account__c, Person_Account__r.PersonContactId 
											FROM Product_Instance__c 
											WHERE Recurring_Instance__c != null 
                                    		AND (Recurring_Instance__r.go1__Credit_Card_Account__c = null
											OR Recurring_Instance__r.go1__Credit_Card_Account__c = '')
											AND createdDate > :lastHour
											AND Subscription_Type__c = 'm'];
		
		//Remove any of these in the List that already have an appropriate task against them (from overlap)
		Map<Id, Product_Instance__c> accContactIds = new Map<Id, Product_Instance__c>();
		for (Product_Instance__c pi : piList) {
			accContactIds.put(pi.Person_Account__r.PersonContactId, pi);
		}
		
		List<Task> recurringInstNoCC = [SELECT Id, WhoId FROM Task 
										WHERE WhoId IN :accContactIds.keySet()
										AND Subject = :TaskManager.HOURLY_RECURRING_INST_MISSING_CARD_SUBJECT
										AND createdDate > :lastHour];
		
		for (Task t : recurringInstNoCC){
			if (accContactIds.containsKey(t.WhoId))
				accContactIds.remove(t.WhoId);
		}
		
		return accContactIds.values();
	}
	
	global void execute(Database.BatchableContext BC, List<Product_Instance__c> instances)
	{
		ProductInstanceHelper piHelper = new ProductInstanceHelper(instances);
		piHelper.createFailedRecurringPaymentTasks(TaskManager.HOURLY_RECURRING_INST_MISSING_CARD_SUBJECT, tm, creditCardAllocationRules);
	}


	//Batchable override method: finish - called when all batches are processed; allocates all fair tasks created in a fair manner across Acc mgrs
	global void finish(Database.BatchableContext BC)
	{
		//Now all the tasks have been created, grab all created tasks and reallocate fairly
		Datetime lastHour = System.now().addHours(-1);
		List<Task> allMissingCCTasksHour = [SELECT Id, ownerId FROM Task 
				where Subject = :TaskManager.HOURLY_RECURRING_INST_MISSING_CARD_SUBJECT 
				AND createdDate > :lastHour
				AND CallDisposition = :TaskManager.ALLOCATION_DISPOSITION];
				
		System.debug('DEBUG: allMissingCCTasksHour:' + allMissingCCTasksHour.size());		
		
		for (Task t : allMissingCCTasksHour) 
		{
			String nextAccountManager = tm.getNextSalesTeamMember(RecurringInstMissingCardTasks.lastAccountManager);
			t.OwnerId = nextAccountManager;
			RecurringInstMissingCardTasks.lastAccountManager = nextAccountManager;	
		}
		
		update allMissingCCTasksHour;
	}
	
	/*
		Method used to create the scheduling of this Batch Job. This is used by the 'TestScheduledJobs' function which enables the scheduled jobs to be 
		setup and removed programmatically (also via API for Jenkins build job)
	*/
	global static void setupSchedule(String jobName, String scheduledTime) { 
		RecurringInstMissingCardTasks scheduled_task = new RecurringInstMissingCardTasks();
		String payment_id = System.schedule(jobName, scheduledTime, scheduled_task);
	}

}