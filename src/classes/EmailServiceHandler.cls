/*
 *
 * Class serves as an Email Service Handler.
 * Created on 6-11-2015
 * Author: Mithun Roy
 * Copyright: EurekaReport.com.au
 */
global class EmailServiceHandler implements Messaging.InboundEmailHandler {
      // Method to parse Inbound Email received by the Email Service.
      global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
          Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
          return result;
      }
  }