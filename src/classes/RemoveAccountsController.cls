/**
 * RemoveAccountsController - is a form to remove account emails and their associated objects
 * via an uploaded csv file. 
 * 
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 * 
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
public with sharing class RemoveAccountsController {
	
	/**
	 *
	 */
	public class DeletedAccount {
		
		public Account account { get; set; }
		public Boolean toDelete { get; set; }
		
		public DeletedAccount(Account account, Boolean toDelete) {
			this.account = account;
			this.toDelete = toDelete;
		}
	}
	
	public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    String[] filelines = new String[]{};
	public List<DeletedAccount> deletedAccounts{get;set;}
	
	/**
	 *
	 */
	public RemoveAccountsController() {}
	
	/**
	 * Querys all the accounts that had an email in the imported csv file
	 */
	public Pagereference ReadFile() {
        // convert the csv file into a list of emails
        if (contentFile == null) {
        	return null;
        }
        nameFile=contentFile.toString();
        filelines = nameFile.split('\n');
        List<String> emails = new List<String>();
        for (Integer i=1;i<filelines.size();i++) {
            emails.add(filelines[i].trim());
        }
        
        // find all the accounts that contain one of the emails in the above list
        List<Account> accounts = [SELECT Id, Name, PersonEmail FROM Account WHERE PersonEmail IN :emails];
        if (!accounts.isEmpty()) {
	        deletedAccounts = new List<DeletedAccount>();
	        for (Account account : accounts) {
	        	deletedAccounts.add(new DeletedAccount(account, true));
	        }
        } else {
    		deletedAccounts = null;
    	}
        
        // display message for the number of accounts found
        String deleteMessage = 'Found '+accounts.size()+' accounts';
    	if (accounts.size() == 1) {
    		deleteMessage = 'Found '+accounts.size()+' account';
    	}
    	ApexPages.Message msgErr = new ApexPages.Message(ApexPages.Severity.CONFIRM, deleteMessage);
        ApexPages.addmessage(msgErr);
        return null;
    }
    
    /**
     * Deletes all the accounts in the list that was checked
     */
    public Pagereference RemoveAccounts() {
    	List<Account> accounts = new List<Account>();
    	List<Id> ids = new List<Id>();
    	
    	// checks which accounts were ticked to be deleted
    	for (DeletedAccount delAccount : deletedAccounts) {
    		if (delAccount.toDelete) {
    			accounts.add(delAccount.account);
    		} else {
    			ids.add(delAccount.account.Id);
    		}
    	}
    	
    	// Delete the account list, display message for number of accounts deleted
    	String deleteMessage = 'Deleted 0 accounts';
    	if (!accounts.isEmpty()) {
    		AccountHelper helper = new AccountHelper(accounts);
    		Integer deleteCount = helper.deleteAccounts();
    		
    		deleteMessage = 'Deleted '+deleteCount+' accounts';
    		if (deleteCount == 1) {
    			deleteMessage = 'Deleted 1 account';
    		}
    	}
    	ApexPages.Message msgErr = new ApexPages.Message(ApexPages.Severity.CONFIRM, deleteMessage);
        ApexPages.addmessage(msgErr);
    	
    	// refresh deleted list
    	deletedAccounts.clear();
    	List<Account> remainingAccounts = [SELECT Id, Name, PersonEmail FROM Account WHERE Id IN :ids];
    	if (!remainingAccounts.isEmpty()) {
	    	for (Account account : remainingAccounts) {
	    		deletedAccounts.add(new DeletedAccount(account, false));
	    	}
    	} else {
    		deletedAccounts = null;
    	}
    	return null;
    }
    
    @istest
	static void testRemoveAccounts() {
		Account account = new Account(FirstName='Test',LastName='Tester',PersonEmail='test@tester.com');
		insert account;
		
		List<Account> accounts = [SELECT Id FROM Account WHERE Id = :account.Id];
		System.assertEquals(1, accounts.size());
		
		RemoveAccountsController uploader = new RemoveAccountsController();
		uploader.deletedAccounts = new List<DeletedAccount>{new DeletedAccount(account, true)};
		uploader.RemoveAccounts();
		
		accounts = [SELECT Id FROM Account WHERE Id = :account.Id];
		System.assertEquals(0, accounts.size());
	}
	
	@istest
	static void testUploadFile() {
		Account account = new Account(FirstName='Test',LastName='Tester',PersonEmail='test@tester.com');
		insert account;
		
		RemoveAccountsController controller = new RemoveAccountsController();
		System.assertEquals(null, controller.deletedAccounts);
		
		Blob bodyBlob = Blob.valueOf('Email\ntest@tester.com');
    	controller.contentFile = bodyBlob;
   		controller.ReadFile();
   		
   		System.assertEquals(1, controller.deletedAccounts.size());
	}
}