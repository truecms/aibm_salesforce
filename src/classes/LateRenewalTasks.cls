/**
* Batch job that runs nightly to pull all Accounts that were not on auto-renewal (or were monthly subs) that expired 10 days ago and have not yet renewed.
* Process will create a task each night for these and assign it to the Customer Support representative
* Author: tailbyr
* Copyright: EurekaReport.com.au
*/
public with sharing class LateRenewalTasks implements Schedulable, Database.Batchable<Account>, Database.Stateful
{
    public static String lastAccountManager { get; set; }
    
    public TaskManager tm {get; set;}
    
    public String lateRenewalQueue {get; set;}
    public Boolean lateRenewalAllocationRules {get; set;}
    
    
    //Constructor - get the lastAccountManager given one of the 'Late Renewal' tasks
    public LateRenewalTasks()
    {
        // Get the Owner of the last Late Renewal task so that we can assign the tasks equally
        List<Task> lastLateRenewalTask = [SELECT Id, ownerId FROM Task WHERE Subject = :TaskManager.LATE_RENEWAL_SUBJECT ORDER BY createdDate desc LIMIT 1]; 
        if (lastLateRenewalTask.size()>0)
            lastAccountManager = lastLateRenewalTask[0].ownerId;
            
        //Get the customer settings allocation rule and queue for this Expired FT automated task creator
        Allocation_Rules__c allocationRules = Allocation_Rules__c.getInstance('Default');
        lateRenewalAllocationRules = allocationRules.Late_Renewal_Allocation_Rule__c;
        lateRenewalQueue = allocationRules.Late_Renewal_Queue__c;
        
        tm = new TaskManager(lateRenewalQueue); //Instigate the TaskManager and tell it which User Queue to use from Custom Settings
    }
    
    //Schedulable override method: execute - called when the scheduled job is kicked off
    public void execute(SchedulableContext ctx)
    {
        Database.executeBatch(new LateRenewalTasks(), 1);
    }
    
    //Batchable override method: start - queries the records to iterate over
    public Iterable<Account> start(Database.batchableContext BC) 
    {
        Date expiredSubDate = System.Today().addDays(-1); //TODO: we could set the 10 days after expiry value in a custom setting so that it can be changed on the fly if required
        List<Account> lateRenewalAccounts = [SELECT id, OwnerId, PersonContactId 
                                            FROM Account 
                                            WHERE (ER_Reporting_Sub_Type__c = 'm'
                                                    OR (ER_Reporting_Sub_Type__c = 'y' AND ER_Last_Paid_Subscription__r.Auto_Renewal__c = FALSE) 
                                            )
                                            AND ER_Reporting_Sub_Status__c = '8'
                                            AND ER_Reporting_Sub_End_Date__c = :expiredSubDate];
                                            
        return lateRenewalAccounts;
    }
    
    //Batchable override method: execute
    public void execute(Database.BatchableContext BC, List<Account> instances)
    {
        AccountHelper aHelper = new AccountHelper(instances);
        aHelper.createLateRenewalTasks(lateRenewalAllocationRules, tm);
    }
    
    //Batchable override method: finish - called when all batches are processed; allocates all fair tasks created in a fair manner across Acc mgrs
    public void finish(Database.BatchableContext BC)
    {
        //Now all the tasks have been created, grab all created tasks and reallocate fairly where an alternative assignment rule has not been set
        List<Task> allLateRenewalTasksToday = [SELECT Id, ownerId FROM Task 
                where Subject = :TaskManager.LATE_RENEWAL_SUBJECT 
                AND createdDate = TODAY
                AND CallDisposition = :TaskManager.ALLOCATION_DISPOSITION];
                
        System.debug('DEBUG: allLateRenewalTasksToday:' + allLateRenewalTasksToday.size());     
        
        for (Task t : allLateRenewalTasksToday) 
        {
            String nextAccountManager = tm.getNextSalesTeamMember(LateRenewalTasks.lastAccountManager);
            t.OwnerId = nextAccountManager;
            LateRenewalTasks.lastAccountManager = nextAccountManager;   
        }
        
        update allLateRenewalTasksToday;
    }
    
    /*
        Method used to create the scheduling of this Batch Job. This is used by the 'TestScheduledJobs' function which enables the scheduled jobs to be 
        setup and removed programmatically (also via API for Jenkins build job)
    */
    public static void setupSchedule(String jobName, String scheduledTime) { 
        LateRenewalTasks scheduled_task = new LateRenewalTasks();
        String payment_id = System.schedule(jobName, scheduledTime, scheduled_task);
    }
}