global with sharing class PopulateTestData {

	/*
	* Create all the test data objects and persist
	*/
	public void run() {
		/* Delete all (any) test data - each method checks you are on a dev/test sandbox first! */
		deleteAllSubscriptionProducts();
		deleteAllCampaigns();
		deleteAllAccounts();
		
		/* Create Custom Settings instances */
		createCustomSettings();
		
		/* Create Payment Gateways */
		createPaymentGateways();
		
		/* Create Subscription Streams */
		createSubscriptionStreams();
		
		/* Create Subscription Products */
		createSubscriptionProducts();
		
		/* Create New Test Campaigns */
		createTestCampaigns();
		
		/* Create a test Person Account! */
		createTestAccount(); 
	}
	
	
	/* Create a WebService call for the above 'Run' method so it can be called from Automation */
	webService static Boolean runSetup() {
		PopulateTestData ptd = new PopulateTestData();
		ptd.run();
		return true;
	}
	
	
	/* Test whether we are currently operating on a sandbox or not */
	public Boolean isSandbox() {
		if (URL.getSalesforceBaseUrl().getHost().left(2).equalsignorecase('cs') ||
				URL.getSalesforceBaseUrl().getHost().left(4).equalsignorecase('c.cs'))
			return true;
		else
			return false;
	}
	
	public void createCustomSettings() {
		//Check that there isn't already a Settings record in each case
		List<AIBM_Settings__c> existingAIBMSettings = [SELECT Id FROM AIBM_Settings__c WHERE Name = 'Defaults'];
		if (existingAIBMSettings.size()>0) {
			System.debug('Customer Setting \'AIBM Settings\' already has an entry in this instance of SF');
		}
		else {
			AIBM_Settings__c aibmSettings = new AIBM_Settings__c(Name = 'Defaults', Allow_Duplicates_Test_Mode__c = false, Auto_Renewal__c = true, Auto_Renew_Test_Mode__c = false);
			insert aibmSettings;
		}
		
		List<go1__CardTypes__c> existingCardTypes = [SELECT Id FROM go1__CardTypes__c WHERE Name = 'CardTypes'];
		if (existingCardTypes.size()>0) {
			System.debug('Custom Setting \'Card Types\' already has an entry in this instance of SF');
		}
		else {
			go1__CardTypes__c cardTypes = new go1__CardTypes__c(Name = 'CardTypes', go1__Amex__c = true, go1__Diners__c = true, go1__MasterCard__c = true, go1__Visa__c = true);
			insert cardTypes;	
		}
		
		List<go1__Go2DebitSettings__c> existingGo2Debit = [SELECT Id FROM go1__Go2DebitSettings__c WHERE Name = 'Go2DebitSettings'];
		if (existingGo2Debit.size()>0) {
			System.debug('Custom Setting \'Go2DebitSettings\' already has an entry in this instance of SF');
		}
		else {
			go1__Go2DebitSettings__c go2Debit = new go1__Go2DebitSettings__c(Name = 'Go2DebitSettings', 
				go1__Account_ID__c = 1, 
				go1__Allow_Currency__c = false, 
				go1__Allow_Recurring__c = true, 
				go1__Bank_Account_Merchant_ID__c = null, 
				go1__Credit_Card_Merchant_ID__c = 1, 
				go1__Enable_Scheduled_Payments__c = false, 
				go1__Failed_Attempts_Allowed__c = 1, 
				go1__Failed_Interval_Time_Limit__c = 1, 
				go1__Items_to_queue_per_recurring_batch__c = 200, 
				go1__Save_Detailed_Payments__c = true);
			insert go2Debit;
		}
	}
	
	public void createPaymentGateways() {
		//Get all (if any) payment gateways and check we're not doubling up
		List<go1__Payment_Gateway__c> existingPaymentGateways = [SELECT Id, Name FROM go1__Payment_Gateway__c];
		Set<String> gatewayNames = new Set<String>();
		for (go1__Payment_Gateway__c gateway : existingPaymentGateways) {
			gatewayNames.add(gateway.Name);
		}
		
		//Create collection of gateways for single DML
		List<go1__Payment_Gateway__c> allGateways = new List<go1__Payment_Gateway__c>();
		
		if (!gatewayNames.contains('Credit Card [Test]')) {
			go1__Payment_Gateway__c ccGateway = new go1__Payment_Gateway__c(Name='Credit Card [Test]', 
				go1__Gateway__c='Securepay',
				go1__Username__c='EUK0022',
				go1__Password__c='45SywtqJ',
				go1__Refunds__c=true,
				go1__Active__c=true,
				go1__Is_Test__c=true,
				go1__Is_Default__c=true,
				go1__Payment_Type__c='CreditCard');
			allGateways.add(ccGateway);
		}
		if (!gatewayNames.contains('Manual - Cheque')) {
			go1__Payment_Gateway__c mcGateway = new go1__Payment_Gateway__c(Name='Manual - Cheque',
				go1__Gateway__c='Manual',
				go1__Username__c='',
				go1__Password__c='',
				go1__Refunds__c=true,
				go1__Active__c=true,
				go1__Is_Test__c=true,
				go1__Is_Default__c=false,
				go1__Payment_Type__c='Manual');
			allGateways.add(mcGateway);
		}
		if (!gatewayNames.contains('Manual - Other')) {
			go1__Payment_Gateway__c moGateway = new go1__Payment_Gateway__c(Name='Manual - Other',
				go1__Gateway__c='Manual',
				go1__Username__c='',
				go1__Password__c='',
				go1__Refunds__c=true,
				go1__Active__c=true,
				go1__Is_Test__c=true,
				go1__Is_Default__c=false,
				go1__Payment_Type__c='Manual');
			allGateways.add(moGateway);
		}
		if (!gatewayNames.contains('Manual - EFT')) {
			go1__Payment_Gateway__c meftGateway = new go1__Payment_Gateway__c(Name='Manual - EFT',
				go1__Gateway__c='Manual',
				go1__Username__c='',
				go1__Password__c='',
				go1__Refunds__c=true,
				go1__Active__c=true,
				go1__Is_Test__c=true,
				go1__Is_Default__c=false,
				go1__Payment_Type__c='Manual');
			allGateways.add(meftGateway);
		}
		
		insert allGateways;
	}
	
	public void createSubscriptionStreams() {
		//Get all sub streams to avoid recreating same stream
		List<Subscription_Stream__c> existingSubscriptionStreams = [SELECT Id, Name FROM Subscription_Stream__c];
		Set<String> streamNames = new Set<String>();
		for (Subscription_Stream__c stream : existingSubscriptionStreams) {
			streamNames.add(stream.Name);
		}
		
		//Create collection for persisting all streams in one DML
		List<Subscription_Stream__c> allStreams = new List<Subscription_Stream__c>();
		
		if (!streamNames.contains('Business Spectator')) {
			Subscription_Stream__c bsStream = new Subscription_Stream__c(Name='Business Spectator');
			allStreams.add(bsStream);
		}
		if (!streamNames.contains('Eureka Live Markets')) {
			Subscription_Stream__c elmStream = new Subscription_Stream__c(Name='Eureka Live Markets');
			allStreams.add(elmStream);
		}
		if (!streamNames.contains('Eureka Report')) {
			Subscription_Stream__c erStream = new Subscription_Stream__c(Name='Eureka Report');
			allStreams.add(erStream);
		}
		if (!streamNames.contains('Eureka Small Caps')) {
			Subscription_Stream__c escStream = new Subscription_Stream__c(Name='Eureka Small Caps');
			allStreams.add(escStream);
		}
		if (!streamNames.contains('Save Our Super')) {
			Subscription_Stream__c sosStream = new Subscription_Stream__c(Name='Save Our Super');
			allStreams.add(sosStream);
		}
		
		insert allStreams;
	}

	public void deleteAllSubscriptionProducts() {
		//First check that we're on a sandbox
		if (isSandbox()) {
			Subscription_Product__c[] products = [SELECT Id FROM Subscription_Product__c];
			try {
				System.debug('DEBUG: Deleteing all subscription products...');
				delete products;
				System.debug('DEBUG: all subscription products deleted');
			}
			catch (DmlException dmle) {
				for (Integer i = 0; i < dmle.getNumDml(); i++) {
        			System.debug(dmle.getDmlMessage(i)); 
    			}
			}
		}
	}

	public void createSubscriptionProducts() {
		List<Subscription_Stream__c> existingSubscriptionStreams = [SELECT Id, Name FROM Subscription_Stream__c];
		Map<String, Id> streams = new Map<String, Id>();
		for (Subscription_Stream__c stream : existingSubscriptionStreams) {
			streams.put(stream.Name, stream.Id);
		}
		
		System.debug('DEBUG:No of existing streams:' + existingSubscriptionStreams.size());
		System.debug('DEBUG:Streams:' + streams);
		
		List<Subscription_Product__c> subProducts = new List<Subscription_Product__c>();
		
		//Create free BS product
		Subscription_Product__c freeBusinessSpectator = new Subscription_Product__c(Name='BS Annual Subscription', 
				Subscription_Stream__c=streams.get('Business Spectator'),
				Subscription_Type__c='f',
				Duration__c=1.0,
				Duration_units__c='year',
				Number_of_Payments__c=0.0);
		subProducts.add(freeBusinessSpectator);
		
		//Create free ER product
		Subscription_Product__c freeEurekaReport = new Subscription_Product__c(Name='ER Free Trial 3 month',
				Title__c='ER Free Trial', 
				Subscription_Stream__c=streams.get('Eureka Report'),
				Subscription_Type__c='f',
				Duration__c=3.0,
				Duration_units__c='month',
				Number_of_Payments__c=0.0,
				Price__c=0.0);
		subProducts.add(freeEurekaReport);
		
		//Create monthly ER product
		Subscription_Product__c monthlyEurekaReport = new Subscription_Product__c(Name='ER Monthly subscription',
				Title__c='ER Monthly subscription', 
				Subscription_Stream__c=streams.get('Eureka Report'),
				Subscription_Type__c='m',
				Duration__c=12.0,
				Duration_units__c='month',
				Number_of_Payments__c=12.0,
				Price__c=36.50);
		subProducts.add(monthlyEurekaReport);
		
		//Create yearly ER product
		Subscription_Product__c yearlyEurekaReport = new Subscription_Product__c(Name='ER Yearly subscription',
				Title__c='ER Yearly subscription', 
				Subscription_Stream__c=streams.get('Eureka Report'),
				Subscription_Type__c='y',
				Duration__c=1.0,
				Duration_units__c='year',
				Number_of_Payments__c=1.0,
				Price__c=385.00);
		subProducts.add(yearlyEurekaReport);
		
		//Create complimentary ER product
		Subscription_Product__c compEurekaReport = new Subscription_Product__c(Name='ER Complimentary',
				Title__c='ER Complimentary', 
				Subscription_Stream__c=streams.get('Eureka Report'),
				Subscription_Type__c='c',
				Duration__c=12.0,
				Duration_units__c='year',
				Number_of_Payments__c=0.0,
				Price__c=0.00);
		subProducts.add(compEurekaReport);
		
		//Create Live Markets product
		Subscription_Product__c liveMarkets = new Subscription_Product__c(Name='ER - Live Markets - 3 month comp',
				Title__c='ER - Live Markets : 3 months free', 
				Subscription_Stream__c=streams.get('Eureka Live Markets'),
				Subscription_Type__c='c',
				Duration__c=3.0,
				Duration_units__c='month',
				Number_of_Payments__c=0.0,
				Price__c=0.00,
				Promotional_Description__c='Get access to all of Eureka Reports : Live Markets articles for three months free simply by signing up.');
		subProducts.add(liveMarkets);
		
		insert subProducts;
	}

	public void deleteAllCampaigns() {
		//First check that we're on a sandbox
		if (isSandbox()) {
			CampaignMember[] members = [SELECT Id FROM CampaignMember]; 
			Campaign[] campaigns = [SELECT Id FROM Campaign];
			try {
				delete members;
				delete campaigns;
			}
			catch (DmlException dmle) {
				for (Integer i = 0; i < dmle.getNumDml(); i++) {
        			System.debug(dmle.getDmlMessage(i)); 
    			}
			}
		}
	}

	public void createTestCampaigns() {
		List<Campaign> newCampaigns = new List<Campaign>();
		Campaign testCampaign1 = new Campaign(Name='A: Campaign Test',
				isActive=true,
				type='Email Marketing',
				StartDate=Date.today().addDays(-1),
				EndDate=Date.today().addDays(364));
		newCampaigns.add(testCampaign1);
		Campaign testCampaign2 = new Campaign(Name='B: Campaign Test',
				isActive=true,
				type='Organic',
				StartDate=Date.today().addDays(-1),
				EndDate=Date.today().addDays(364));
		newCampaigns.add(testCampaign2);
		Campaign testCampaign3 = new Campaign(Name='C: Campaign Test',
				isActive=true,
				type='Social',
				StartDate=Date.today().addDays(-1),
				EndDate=Date.today().addDays(364));
		newCampaigns.add(testCampaign3);
		
		insert newCampaigns;	
	}

	public void deleteAllAccounts() {
		System.debug('DEBUG: Deleting all Accounts now... is Sandbox?: ' + isSandbox());
		//First check that we're on a sandbox
		if (isSandbox()) {
			System.debug('DEBUG: We are on a sandbox');
			go1__Transaction__c[] trans = [SELECT Id FROM go1__Transaction__c];
			Product_Instance__c[] products = [SELECT Id FROM Product_Instance__c];
			go1__Recurring_Instance__c[] recurs = [SELECT Id FROM go1__Recurring_Instance__c];
			go1__CreditCardAccount__c[] cards = [SELECT Id FROM go1__CreditCardAccount__c]; 
			Case[] cases = [SELECT Id FROM Case];
			Task[] tasks = [SELECT Id FROM Task];
			Account[] accs = [SELECT Id FROM Account];
			try {
				System.debug('DEBUG: trying to get rid of stuff');
				delete trans;
				delete products;
				delete recurs;
				delete cards;
				delete cases;
				delete tasks;
				delete accs;
				System.debug('DEBUG: stuff has been got rid of');
			}
			catch (DmlException dmle) {
				for (Integer i = 0; i < dmle.getNumDml(); i++) {
        			System.debug(dmle.getDmlMessage(i)); 
    			}
			}
		}
	}

	public void createTestAccount() {
		//Get the RecordType
		List<RecordType> personAccount = [SELECT Id FROM RecordType WHERE DeveloperName = 'PersonAccount'];
		
		if (personAccount.size()>0) {
			try {
				Account newAcc = new Account(FirstName='Test',
					LastName='Account',
					RecordType=personAccount[0],
					PersonEmail='TestAccount@testbucket.net.test',
					Phone='123456789',
					Birth_Year__c='2000',
					BillingStreet='10 Test Street',
					BillingCity='Melbourne',
					BillingState='VIC',
					BillingCountry='Australia',
					BillingPostalCode='3000');
				insert newAcc;
			} catch (Exception e) {
				System.debug('DEBUG: Exception:' + e);
			}
		}
	}
	
	/*************************************************************************
	* TestCases
	**************************************************************************/

	@IsTest
	public static void testCreateAccount(){
		//Check that the account does not exist before creation
		List<Account> prevAccs = [SELECT Id FROM Account WHERE PersonEmail = 'TestAccount@testbucket.net.test'];
		System.assertEquals(prevAccs.size(), 0);
		
		Test.startTest();
		PopulateTestData ptd = new PopulateTestData();
		ptd.createTestAccount();
		Test.stopTest();
		
		//Now check that the account is available 
		List<Account> newAccs = [SELECT Id FROM Account WHERE PersonEmail = 'TestAccount@testbucket.net.test'];
		System.assertEquals(newAccs.size(),1);
		
		//Also check that the delete account functionality works too!
		
	}
	
	@IsTest
	public static void testDeleteAccounts(){
		PopulateTestData ptd = new PopulateTestData();
		ptd.createTestAccount();
		
		//Check we have an account in the system
		List<Account> newAccs = [SELECT Id FROM Account WHERE PersonEmail = 'TestAccount@testbucket.net.test'];
		System.assertEquals(newAccs.size(),1);
		
		Test.startTest();
		ptd.deleteAllAccounts();
		Test.stopTest();
		
		//Check all accounts are now gone
		if (ptd.isSandbox()) {
			List<Account> goneAccs = [SELECT Id FROM Account];
			System.assertEquals(goneAccs.size(),0);
		}
	}
	
	@IsTest
	public static void testCreateCampaigns() {
		//Check that these Campaigns haven't already been created
		List<Campaign> prevCampaigns = [SELECT Id FROM Campaign WHERE Name like '%Campaign Test'];
		System.assertEquals(prevCampaigns.size(), 0);
		
		Test.startTest();
		PopulateTestData ptd = new PopulateTestData();
		ptd.createTestCampaigns();
		Test.stopTest();
		
		List<Campaign> newCampaigns = [SELECT Id FROM Campaign WHERE Name like '%Campaign Test'];
		System.assertEquals(newCampaigns.size(), 3);
	}
	
	@IsTest
	public static void testDeleteAllCampaigns() {
		PopulateTestData ptd = new PopulateTestData();
		ptd.createTestCampaigns();
		
		//Check the campaigns have been created 
		List<Campaign> newCampaigns = [SELECT Id FROM Campaign WHERE Name like '%Campaign Test'];
		System.assertEquals(newCampaigns.size(), 3);
		
		Test.startTest();
		ptd.deleteAllCampaigns();
		Test.stopTest();
		
		//Check that all campaigns have now been removed
		if (ptd.isSandbox()) {
			List<Campaign> goneCampaigns = [SELECT Id FROM Campaign];
			System.assertEquals(goneCampaigns.size(), 0);
		}
	}
	
	@IsTest
	public static void testCreateSubscriptionProductsAndStreams() {
		//Just check for one of the products to ensure it is actually creating
		List<Subscription_Product__c> prevSub = [SELECT Id FROM Subscription_Product__c WHERE Name = 'ER Yearly subscription'];
		System.assertEquals(prevSub.size(), 0);
		List<Subscription_Stream__c> prevStreams = [SELECT Id FROM Subscription_Stream__c];
		System.assertEquals(prevStreams.size(), 0);
		
		Test.startTest();
		//Create both so that the sub products have access to subscription streams
		PopulateTestData ptd = new PopulateTestData();
		ptd.createSubscriptionStreams();
		ptd.createSubscriptionProducts();
		
		List<Subscription_Stream__c> newStreams = [SELECT Id FROM Subscription_Stream__c];
		System.assertEquals(newStreams.size(), 5);
		List<Subscription_Product__c> newSub = [SELECT Id FROM Subscription_Product__c WHERE Name = 'ER Yearly subscription'];
		System.assertEquals(newSub.size(), 1);
		
		//Call the subscription stream creator again and check that they are not all created again
		ptd.createSubscriptionStreams();
		Test.stopTest();
		
		List<Subscription_Stream__c> recreateStreams = [SELECT Id FROM Subscription_Stream__c];
		System.assertEquals(recreateStreams.size(), 5);
		
	}
	
	@IsTest
	public static void testDeleteSubscriptionStreams() {
		PopulateTestData ptd = new PopulateTestData();
		ptd.createSubscriptionStreams();
		ptd.createSubscriptionProducts();
		
		//Check we have some subscription products
		List<Subscription_Product__c> newSub = [SELECT Id FROM Subscription_Product__c];
		System.assert(newSub.size() > 0);
		
		Test.startTest();
		ptd.deleteAllSubscriptionProducts();
		Test.stopTest();
		
		//Check that all sub products are now gone
		if (ptd.isSandbox()) {
			List<Subscription_Product__c> goneSub = [SELECT Id FROM Subscription_Product__c];
			System.assertEquals(goneSub.size(), 0);
		}
	}
	
	@IsTest
	public static void testCreatePaymentGateways() {
		//Check we aren't starting with payment gateways
		List<go1__Payment_Gateway__c> prevGateways = [SELECT Id FROM go1__Payment_Gateway__c];
		System.assertEquals(prevGateways.size(), 0);
		
		Test.startTest();
		PopulateTestData ptd = new PopulateTestData();
		ptd.createPaymentGateways();		
		
		List<go1__Payment_Gateway__c> newGateways = [SELECT Id FROM go1__Payment_Gateway__c];
		System.assertEquals(newGateways.size(), 4);
		
		//Call the payment gateway creator again and check that they are not all created again
		ptd.createPaymentGateways();
		Test.stopTest();
		
		List<go1__Payment_Gateway__c> recreateGateways = [SELECT Id FROM go1__Payment_Gateway__c];
		System.assertEquals(recreateGateways.size(), 4);
	}
	
	@IsTest
	public static void testCreateCustomSettings() {
		Test.startTest();
		PopulateTestData ptd = new PopulateTestData();
		ptd.createCustomSettings();
		Test.stopTest();
		
		List<AIBM_Settings__c> existingAIBMSettings = [SELECT Id FROM AIBM_Settings__c WHERE Name = 'Defaults'];
		System.assertEquals(existingAIBMSettings.size(), 1);
		List<go1__CardTypes__c> existingCardTypes = [SELECT Id FROM go1__CardTypes__c WHERE Name = 'CardTypes'];
		System.assertEquals(existingCardTypes.size(), 1);
		List<go1__Go2DebitSettings__c> existingGo2Debit = [SELECT Id FROM go1__Go2DebitSettings__c WHERE Name = 'Go2DebitSettings'];
		System.assertEquals(existingGo2Debit.size(), 1);
	}
	
	@IsTest
	public static void testRun() {
		Test.startTest();
		PopulateTestData ptd = new PopulateTestData();
		ptd.run();
		Test.stopTest();
		
		//Check ALL test data is present
		List<AIBM_Settings__c> existingAIBMSettings = [SELECT Id FROM AIBM_Settings__c WHERE Name = 'Defaults'];
		System.assertEquals(existingAIBMSettings.size(), 1);
		List<go1__Payment_Gateway__c> newGateways = [SELECT Id FROM go1__Payment_Gateway__c];
		System.assertEquals(newGateways.size(), 4);
		List<Subscription_Stream__c> newStreams = [SELECT Id FROM Subscription_Stream__c];
		System.assertEquals(newStreams.size(), 5);
		List<Subscription_Product__c> newSub = [SELECT Id FROM Subscription_Product__c WHERE Name = 'ER Yearly subscription'];
		System.assertEquals(newSub.size(), 1);
		List<Campaign> newCampaigns = [SELECT Id FROM Campaign WHERE Name like '%Campaign Test'];
		System.assertEquals(newCampaigns.size(), 3);
		List<Account> newAccs = [SELECT Id FROM Account WHERE PersonEmail = 'TestAccount@testbucket.net.test'];
		System.assertEquals(newAccs.size(),1);
	}
	
}