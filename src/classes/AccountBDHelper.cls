/**
 * AccountBDHelper is a wrapper class for the Account object in Salesforce with
 * customised functionality developed for the BrightDay / OneVue linked investment platform.
 * 
 * Accounts are treated in a different way to the existing Eureka Report functionality - this class
 * implements account manipulation for our new platform
 *
 * @author: Ross Tailby
 * @license: http://www.eurekareport.com.au
 */
global with sharing class AccountBDHelper {

    public Account account {get; set;}
    
    public List<Account> accounts {get; set;}
    
    public AccountBDHelper(Account a) {
        this.account = a;
    }
    
    public AccountBDHelper(List<Account> accs) {
        if (accs.size() > 0){
            this.account = accs[0];
        }
        this.accounts = accs;
    }

	/**
	* Find the last Sales Person / Account Manager that a task of a certain type was assigned to
	*/
	public static String getLastTaskOwner(String subject){
		List<Task> latePaperworkTask = [SELECT Id, ownerId FROM Task WHERE Subject = :subject ORDER BY createdDate desc LIMIT 1]; 
		if (latePaperworkTask.size()>0)
			return latePaperworkTask[0].ownerId;
		else
			return null;
	}

    /**
    * Creates a map holding each account by Id and the last task allocated to that Account
    */
    public Map<Id, Task> getLastAccountTask() {
        //Query for any tasks associated to the accounts being considered. If task associated, assign Acc Mgr to same as this prev task
        List<Id> accs = new List<Id>();
        for (Account account : this.accounts)
        {
            accs.add(account.Id);
        }
        List<Task> preExistingTasks = [SELECT Id, OwnerId, AccountId FROM Task where AccountId IN :accs AND Owner.IsActive=true ORDER BY createdDate ASC];
        Map<Id, Task> prevAccountTasks = new Map<Id, Task>();
        for (Task t : preExistingTasks)
        {
            prevAccountTasks.put(t.AccountId, t);
        }
        return prevAccountTasks;
    }

    /**
    * Processes the Accounts that require tasks in relation to a triggered scenario of the BrightDay client communications journey
    */
    public void createBrightDayTasks(Boolean bdAllocationRule, TaskManager tm, String subject) {
        List<Task> aNewTasks = new List<Task>();
        
        Map<Id, Task> previousTaskPerAcc = this.getLastAccountTask();
        
        for (Account account : this.accounts)
        {
            this.account = account;
            
            Task newTask = null;
                    
            //First task allocation rule - try to assign to the Account Owner if it is a User in the Account Management list
            Id accountOwner = this.account.OwnerId;
            if (tm.salesTeamMemberMap.containsKey(accountOwner) && bdAllocationRule) //We can use SalesTeamMemberMap as its being dynamically constructed from the Custom setting retrived in the initator
            {
                newTask = tm.generateBDTask(this.account, accountOwner, subject);
            }
            else if (previousTaskPerAcc.containsKey(this.account.Id) && bdAllocationRule) //Check if the account already has tasks against it. If so, assign new task to their owner
            {
                newTask = tm.generateBDTask(this.account, previousTaskPerAcc.get(this.account.Id).OwnerId, subject);
            }
            else 
            {
                //Just get the task created with the fairly allocation i.e. next Acc Mgr in line
                newTask = tm.generateBDTask(this.account, subject);
            }
            aNewTasks.add(newTask);
        }
        insert aNewTasks;
    }
    
    /**
    * To be used in Batchable 'finish' methods - takes the tasks of a certain type created today and allocates them in a
    * round robin fashion to the appropriate members of the selected queue
    */
    public static void fairlyAllocateTasks(String subject, TaskManager tm, String lastSalesTeamMember) {
    	String tempSalesTeamMember = lastSalesTeamMember;
    	
    	List<Task> allSubjectTasksToday = [SELECT Id, ownerId FROM Task 
                where Subject = :subject
                AND createdDate = TODAY
                AND CallDisposition = :TaskManager.ALLOCATION_DISPOSITION];
                
        System.debug('DEBUG: allSubjectTasksToday:' + allSubjectTasksToday.size());       
        
        for (Task t : allSubjectTasksToday) 
        {
            String nextSalesTeamMember = tm.getNextSalesTeamMember(tempSalesTeamMember);
            t.OwnerId = nextSalesTeamMember;
            tempSalesTeamMember = nextSalesTeamMember;
        }
        
        update allSubjectTasksToday;
    }
}