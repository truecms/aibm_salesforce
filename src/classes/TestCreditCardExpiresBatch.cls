@isTest
private class TestCreditCardExpiresBatch
{
	
	static TestMethod void checkFairAllocationOfTasks(){
		
		//Set up the custom settings for queue to use and whether to utilise assignment rules or not
		TestProductInstanceHelper.createAllocationSettings();
		
		integer noOfProductInstances = 10; //Set the number of PIs to create
		
		//Find current Account Managers to test each is allocated a task
    	List<GroupMember> aMembers = [SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = 'AccountManagers' ORDER BY UserOrGroupId];
    	
    	//Create subscription stream
    	Subscription_Stream__c stream = new Subscription_Stream__c(Name='Eureka Report');
	  	insert stream;
	  	Subscription_Product__c product = new Subscription_Product__c();
	  	product.Subscription_Stream__c = stream.Id;
	  	product.Duration__c = 1;
	  	product.Duration_units__c = 'month';
	  	product.Price__c = 100;
	  	insert product;
	  	//----------------------
	  		
	  	//Create campaign
	  	Campaign campaign = new Campaign(Name='Test Campaign');
	  	insert campaign;
	  	//----------------------
	  		
	  	// Find any person account record type
	    RecordType recordType = [ select Id, Name, DeveloperName from RecordType where SObjectType = 'Account' and IsPersonType = true limit 1 ];
	  	//----------------------
    	
    	
    	//Create the accounts for test
    	List<Account> accs = new List<Account>(); 
    	List<Id> accIds = new List<Id>();
    	
    	for (integer i = 0; i < noOfProductInstances; i++) {
    		Account account = new Account(FirstName='Test' +i, LastName='Tester' +i, PersonEmail='test'+i+'@test.com.test');
	  		account.RecordTypeId = recordType.Id;
	  		account.Sub_Status__c = '4';  // paid subscription
	  		accs.add(account);
    	}
    	insert accs;
    	//----------------------
    	
    	//Now create the Product Instances, one per account created
    	List<Product_Instance__c> instances = new List<Product_Instance__c>();
    	List<go1__CreditCardAccount__c> creditCards = new List<go1__CreditCardAccount__c>();
    	
    	for (Account acc : accs) {
    		accIds.add(acc.Id); //Add the account ids now that they are persisted
    		
	  		Product_Instance__c instance = new Product_Instance__c();
	  		instance.Person_Account__c = acc.Id;
	  		instance.Subscription_Product__c = product.Id;
	  		instance.Subscription_Type__c = 'y';
	  		instance.Amount__c = 100;
	  		instance.Auto_Renewal__c = true;
	  		instance.Campaign__c = campaign.Id;
	  		instance.End_Date__c = Date.today().addDays(1);
	  		
		  	Datetime expiryDateTime = Datetime.now().addDays(-32); //Ensure card expires more than one month ago for expiry month/year rounding
			String expiryDate = expiryDateTime.format('M/yyyy');
		    go1__CreditCardAccount__c creditcard = go1.PaymentHelper.savedTestCreditCard(acc);
		  	creditcard.go1__Card_Type__c = 'VISA';
		  	creditcard.go1__Expiry__c = expiryDate;
		  	creditCards.add(creditcard);
		  		
	  		instances.add(instance);
    	}
    	
    	insert instances;
    	update creditCards;
	  		
	  	//Update the start and end dates of the PI so it will be picked up by the expiry process
	  	List<Product_Instance__c> insts = [SELECT Id, Start_Date__c, End_Date__c FROM Product_Instance__c WHERE Person_Account__c in :accIds];
	  	for (Product_Instance__c inst : insts) {
	  		inst.Start_Date__c = Date.today().addDays(-358);
	  		inst.End_Date__c = Date.today().addDays(1);
	  	}
	  	update insts;
    	
    	Test.startTest();
    	Database.executeBatch(new CreditCardExpiresBatch(), noOfProductInstances);
	  	Test.stopTest();
	  	
	  	
	  	//Get all of the credit card expiry tasks that were created today
	  	List<Task> allCCTasksToday = [SELECT Id, ownerId, CallDisposition, createdDate FROM Task 
				where Subject LIKE :TaskManager.CREDIT_CARD_EXPIRY_SUBJECT + '%' 
				AND createdDate = TODAY
				ORDER BY createdDate desc];
				
		//Ensure that these tasks were allocated equally
		if (allCCTasksToday!=null && allCCTasksToday.size()>0){
			Map<Id, Integer> tasksPerManager = new Map<Id, Integer>();
			for (Task t : allCCTasksToday) {
				System.debug('DEBUG: taskId:' + t.Id + ' Account Mgr:' + t.ownerId 
					+ ' Call Result:' + t.CallDisposition + ' Created Date:' + t.createdDate);
					
				//Count tasks per Account Manager
				if (tasksPerManager.containsKey(t.ownerId)){
					Integer count = tasksPerManager.get(t.ownerId);
					count++;
					tasksPerManager.put(t.ownerId, count);
				}
				else {
					tasksPerManager.put(t.ownerId, 1);
				}
			}
			
			System.debug('DEBUG: AccMgr map:' + tasksPerManager);
			
			//Work out how many tasks each Account Manager must have (not counting remainder)
			Integer noOfTasks = Math.floor(noOfProductInstances / aMembers.size()).intValue();
			List<Integer> counts = tasksPerManager.values();
			for (Integer val : counts){
				if (val == noOfTasks || val == noOfTasks+1) //+1 to account for the remainder where noOfTasks%noOfAccountManagers != 0
					System.assert(true);
				else
					System.assert(false);
			}
		}
		else
			System.assert(false);
		
	}
	
	
}