/**
 * AquisitionAndRetentionReport
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
public with sharing class AquisitionAndRetentionReport {
	
	public List<DataRow> table { get; set; }
	public DataRow total { get; set; }
	public List<String> dateLabels { get; set; }
	
	public final String streamName = 'Eureka Report';
	public Integer weeks { get; set; }
	
	public List<SelectOption> getWeekOptions() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('10','10'));
		options.add(new SelectOption('15','15'));
		options.add(new SelectOption('20','20'));
		return options;
	}
	
	/**
	 *
	 */
	public AquisitionAndRetentionReport() {
		this.weeks = 10;
		initData();
	}
	
	public void initData() {
		this.table = new List<DataRow>();
		this.dateLabels = getDateLabels();
		buildDataTable();
		getTotalData();
	}
	
	/**
	 *
	 */
	public void buildDataTable() {
		for (Integer i=0; i<dateLabels.size()-1; i++) {
			DataRow row = new DataRow(dateLabels[i]);
			Date endDate = Date.parse(dateLabels[i]);
			Date startDate = Date.parse(dateLabels[i+1]);
			
			// count the number of free trials that started in that week
			row.free = getFreeTrialsData(startDate, endDate);
			
			// count the number of free trials that started in that week and have turned into subs
			row.subs = getSubsData(startDate, endDate);
			
			// calculate conversion value
			row.calculatePercent();
			this.table.add(row);
		}
	}
	
	/**
	 * subscribers ending in that week
	 */
	public Integer getFreeTrialsData(Date startDate, Date nextDate) {
		Integer results = [SELECT count() FROM Product_Instance__c WHERE Subscription_Product__r.Subscription_Stream__r.Name = :streamName AND Subscription_Type__c = 'f' AND Start_Date__c >= :startDate AND Start_Date__c < :nextDate];
		return results;
	}
	
	/**
	 * subscribers ending in that week and not selected to auto renew
	 */
	public Integer getSubsData(Date startDate, Date nextDate) {
		Integer results = [SELECT count() FROM Product_Instance__c WHERE Subscription_Product__r.Subscription_Stream__r.Name = :streamName AND Subscription_Type__c = 'f' AND Start_Date__c >= :startDate AND Start_Date__c < :nextDate AND Person_Account__r.ER_Reporting_First_Payment__c > :startDate];
		return results;
	}
	
	/**
	 * calculates the total for each year
	 */
	public void getTotalData() {
		total = new DataRow('Total');
		
		for (DataRow row : table) {
			total.free += row.free;
			total.subs += row.subs;
			total.calculatePercent();
		}
	}
	
	/**
	 *
	 */
	public List<String> getDateLabels() {
		List<String> dateLabels = new List<String>();
		
		Date startDate = Date.today();
		Date startOfWeek = startDate.toStartOfWeek();
		dateLabels.add(startOfWeek.format());
		
		// repeat the following statement for each week in a year
		for (Integer i=0; i<this.weeks; i++) {
			// add a week to the current end date
			startOfWeek = startOfWeek.addDays(-7);
			// add string label to list
			dateLabels.add(startOfWeek.format());
		}
		return dateLabels;
	}
	
	
	/****************************************************************/
	/**
	 * Repres
	 */
	public class DataRow {
		
		public String label { get; set; }
		public Integer free { get; set; }
		public Integer subs { get; set; }
		public Decimal percent { get; set; }
		
		public dataRow(String label) {
			this.label = label;
			this.free = 0;
			this.subs = 0;
			this.percent = 0;
		}
		
		public void calculatePercent() {
			if (this.free != 0) {
				this.percent = (Decimal.valueOf(this.subs)/Decimal.valueOf(this.free)) * 100;
			}
		}
	}
}