/**
 * TestMonthlyRecurringPayments 
 *
 * Tests various functionality with the recurring payments that are generated for 
 * monthly subscriptions 
 *
 * @author: Ross Tailby
 * @license: http://www.eurekareports.com.au
 */
@isTest
private class TestMonthlyRecurringPayments {

	public static Product_Instance__c createRecurringInstance() {
  		Subscription_Stream__c stream = new Subscription_Stream__c(Name='Eureka Report');
  		insert stream;
  		Subscription_Product__c product = new Subscription_Product__c();
  		product.Subscription_Stream__c = stream.Id;
  		product.Duration__c = 12;
  		product.Duration_units__c = 'month';
  		product.Price__c = 100;
  		insert product;
  		
  		Campaign campaign = new Campaign(Name='Test Campaign');
  		insert campaign;
  		
  		// Find any person account record type
        RecordType recordType = [ select Id, Name, DeveloperName from RecordType where SObjectType = 'Account' and IsPersonType = true limit 1 ];
  		
  		Account account = new Account(FirstName='Test', LastName='Tester', PersonEmail='test@test.com.test');
  		account.RecordTypeId = recordType.Id;
  		account.Sub_Status__c = '4';  // paid subscription
  		insert account;
  		Product_Instance__c instance = new Product_Instance__c();
  		instance.Person_Account__c = account.Id;
  		instance.Subscription_Product__c = product.Id;
  		instance.Subscription_Type__c = 'm';
  		instance.Amount__c = 100;
  		instance.Campaign__c = campaign.Id;
  		insert instance;
  		
  		go1__Recurring_Instance__c recurringInst = new go1__Recurring_Instance__c(go1__Account__c=account.Id, go1__Amount__c=38.00, go1__Interval__c='Monthly', go1__Remaining_Attempts__c=10);
  		insert recurringInst;
  		
  		instance.Recurring_Instance__c = recurringInst.Id;
  		update instance;
  		
  		go1__CreditCardAccount__c creditcard = go1.PaymentHelper.savedTestCreditCard(account);
  		creditcard.go1__Card_Type__c = 'VISA';
  		update creditcard;  		
  		
  		return instance;
	}


	static TestMethod void testSetRemainingAttemptsToZero() {
		//Create a new monthly recurring instance
		Product_Instance__c testSub = createRecurringInstance();
		
		//Will this testSub automatically get a recurring instance generated for it?
		List<Product_Instance__c> riSub = [SELECT Id, Recurring_Instance__c FROM Product_Instance__c WHERE Id = :testSub.Id];
		
		Test.startTest();
		
		if (riSub.size()>0) {
			//Check the number of remaining attempts is > 0
			go1__Recurring_Instance__c ri = [SELECT go1__Remaining_Attempts__c FROM go1__Recurring_Instance__c WHERE Id = :riSub.get(0).Recurring_Instance__c];
			System.debug('DEBUG: Remaining attempts:' + ri.go1__Remaining_Attempts__c);
			System.assert(ri.go1__Remaining_Attempts__c>0);
				
			//Cancel the instance and check that the remaining attempts is updated to zero
			ri.go1__Cancelled__c = true;
			Product_Instance__c cancelPI = riSub.get(0);
			cancelPI.End_Date__c = Date.today().addDays(-10);
			cancelPI.Status__c = 'Cancelled';
			update cancelPI;
			update ri;
			
			go1__Recurring_Instance__c updateRi = [SELECT go1__Remaining_Attempts__c FROM go1__Recurring_Instance__c WHERE Id = :riSub.get(0).Recurring_Instance__c];
			System.debug('DEBUG: Remaining attempts:' + updateRi.go1__Remaining_Attempts__c);
			System.assert(updateRi.go1__Remaining_Attempts__c==0);
		}

		Test.stopTest();
	}
	
	static TestMethod void testCancelRecurringThoughErrorAndDoNoZero() {
		//Create a new monthly recurring instance
		Product_Instance__c testSub = createRecurringInstance();
		
		//Will this testSub automatically get a recurring instance generated for it?
		List<Product_Instance__c> riSub = [SELECT Id, Recurring_Instance__c FROM Product_Instance__c WHERE Id = :testSub.Id];
		
		Test.startTest();
		
		//Cancel/Expire recurring instance through a failed transaction
		go1__Transaction__c failTrans = new go1__Transaction__c(go1__Amount__c=38.00, go1__Currency__c='AUD', go1__Account__c=testSub.Person_Account__c, go1__Status__c='Reserved Error', go1__Code__c=100, go1__Recurring_Instance__c=riSub.get(0).Recurring_Instance__c, go1__Account_ID__c=testSub.Person_Account__c, go1__Payment_Name__c='Test User');
		insert failTrans;
		
		//Check if the recurring instance has been cancelled
		go1__Recurring_Instance__c cancelledRi = [SELECT go1__Remaining_Attempts__c, go1__Cancelled__c, go1__Expired__c 
							FROM go1__Recurring_Instance__c WHERE Id = :riSub.get(0).Recurring_Instance__c];
							
		//The Recurring Instance is cancelled but the Product Instance is still active
		cancelledRi.go1__Cancelled__c = true;
		cancelledRi.go1__Expired__c = true;
		update cancelledRi;
		
		Test.stopTest();
		
		//Check that the remaining attempts is still > 0
		go1__Recurring_Instance__c updatedRi = [SELECT go1__Remaining_Attempts__c, go1__Cancelled__c, go1__Expired__c 
							FROM go1__Recurring_Instance__c WHERE Id = :riSub.get(0).Recurring_Instance__c];
		System.debug('DEBUG: updatedRI remaining attempts:' + updatedRI.go1__Remaining_Attempts__c);
		System.assert(updatedRI.go1__Remaining_Attempts__c>0);
		
		//To complete this test, delete the failed transaction, update the Recurring Inst again and check the remaining attempts go to zero
		delete failTrans;
		update updatedRi;
		
		go1__Recurring_Instance__c finalRi = [SELECT go1__Remaining_Attempts__c, go1__Cancelled__c, go1__Expired__c 
							FROM go1__Recurring_Instance__c WHERE Id = :riSub.get(0).Recurring_Instance__c];
		System.debug('DEBUG: finalRI remaining attempts:' + finalRI.go1__Remaining_Attempts__c);
		System.assert(finalRI.go1__Remaining_Attempts__c==0);
	}

}