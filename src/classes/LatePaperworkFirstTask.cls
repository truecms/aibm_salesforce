/**
* Batch job that runs nightly to pull all Accounts that have signed up to OneVue platform but 
* not returned their paperwork within either FIRST NO PAPERWORK TASK days or SECOND NO PAPERWORK TASK days
* A task for the Sales Team to follow up will be created for each of these customers 
* Author: tailbyr
* Copyright: EurekaReport.com.au
*/
public with sharing class LatePaperworkFirstTask implements Schedulable{

	/*
		Method used to create the scheduling of this Batch Job. This is used by the 'TestScheduledJobs' function which enables the scheduled jobs to be 
		setup and removed programmatically (also via API for Jenkins build job)
	*/
	public static void setupSchedule(String jobName, String scheduledTime) { 
		LatePaperworkFirstTask scheduled_task = new LatePaperworkFirstTask();
		String payment_id = System.schedule(jobName, scheduledTime, scheduled_task);
	}

	//Schedulable override method: execute - called when the scheduled job is kicked off
	public void execute(SchedulableContext ctx) {
		Integer[] numDays = getNumDays();
		
		if (numDays[0] != 0)
			Database.executeBatch(new LatePaperworkTasks(numDays[0]), 1);
		
		if (numDays[1] != 0)
			Database.executeBatch(new LatePaperworkTasks(numDays[1]), 1);
	}

	public static Integer[] getNumDays(){
		BrightDay_Settings__c settings = BrightDay_Settings__c.getInstance();
		
		Integer[] numDays = new Integer[2];
		if (settings.First_No_Paperwork_Task__c == null) numDays[0] = 0; //If no value, use Zero and do not trigger that job
		else numDays[0] = settings.First_No_Paperwork_Task__c.intValue();
		
		if (settings.Second_No_Paperwork_Task__c == null) numDays[1] = 0;
		else numDays[1] = settings.Second_No_Paperwork_Task__c.intValue();
		
		return numDays;
	}

}