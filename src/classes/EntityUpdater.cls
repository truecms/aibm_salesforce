/**
 * EntityUpdater
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
global class EntityUpdater implements Database.Batchable<sObject>{
	global final String Query;
	global final String Entity;
	global final String Field;
	global final String Value;

	global EntityUpdater(String q, String f, String v){
		Query=q; Field=f;Value=v;
	}

	global Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope){
		/*
		if (Field != '') {
			for(Sobject s : scope){
				s.put(Field,Value); 
			}
		}
		*/
		update scope;
	}

	global void finish(Database.BatchableContext BC){

	}

}