/**
* Batch job that runs nightly to pull all Accounts that have started the OneVue transact signup process but not finished and clicked subject. 
* Therefore, the account has a OneVue Application Status = 'in_progress'
* A task for the Sales Team to follow up will be created for each of these customers
* 
* This class is a child class of BDMemberTasks as it follows very similar functionality and sales user queue
* 
* Author: tailbyr
* Copyright: EurekaReport.com.au
*/
public with sharing class BDInProgressTasks extends BDMemberTasks {

	public BDInProgressTasks(Integer followUpDay, Boolean active, String subject){
		super(followUpDay, active, subject);
	}
	
	//Batchable override method: should find all of the Accounts that need a task created against
	public override Iterable<Account> start(Database.batchableContext BC) {
		
		Date statusLength = System.Today().addDays(followUpDay); //How many days in the "in_progress" status should an account be left
		
		//Finding all accounts that have been 'in_progress' for x number of days
		List<Account> accountsInProgress = [SELECT id, Name, OwnerId, PersonContactId, BD_Last_Login_Date__c, BD_Registration_Date__c,
        									OneVue_Application_Status__c
        									FROM Account 
        									WHERE OneVue_Application_Status__c = 'in_progress'
        									AND OV_Status_Change_Date__c = :statusLength];
		
        return accountsInProgress;
	}

}