/**
 * DeferredPaymentsReport
 *
 * Produces the Deferred Payments Report for the current month with a selector to choose to view previous months reports. 
 * Can also download the report to reproduce the original Excel spreadsheet 
 *
 * Author: tailbyr
 * Copyright: EurekaReport.com.au
 */
global class DeferredPaymentsReport implements Database.Batchable<DeferredPaymentsReport.DataRow>, Database.Stateful {
	
	public static final Integer TOTAL_MONTHS = 50; //The maximum number of months to propagate deferred payments
	public static final Integer MAX_RECORD_PER_PAGE = 50; //If changing the recordsPerPage, this is the default max to return to 
	
	public Integer recordsPerPage = 50; //Default number of records to show on any one page to avoid maxing out VF memory limit
	private Integer counter = 0;  //Keeps track of the offset
	
	public Integer numberOfMonths { get; set; } //The number of month columns to display
	public List<Integer> monthCount {get; set;} //Month counter to render the dynamic columns correctly
	public Date[] monthHeaders { get; set; } //The headers for the deferred month payment columns - size should match the numberOfMonths value
	
	public List<DataRow> results {get; set;}
	public Integer totalResults {get; set;}
	public Integer visibleResults {get; set;}
	
	public Boolean showData {get; set;} //Indicates whether to show the trans/sub/account columns
	public Boolean showFinancials {get; set;} //Indicates whether to show the financial deferred payment columns
	public Boolean showOnlyInvalid {get; set;} //Shows only rows where the 'validation' column != 0.00
	
	public Boolean displayPopUp {get; set;} //Displays the 'report sent to email' confirmation popup
	
	public Decimal totalYearly {get; set;}
	public Decimal totalMonthly {get; set;}
	
	public String sortBy { get; set; } // The string containing the column to filter by
	public String sortDir { get; set; } // The string containing the direction (ASC, DESC)
	
	public Product_Instance__c dateRange {get; set;}	
	
	public string currentPage { 
    	get;
    	
    	set { currentPage=value; }
    } //The current page number the user is viewing
    
    public Integer getCounter() {
    	return this.counter;
    }
	
	public DeferredPaymentsReport() {
		this.dateRange = new Product_Instance__c();
		this.dateRange.Start_Date__c = Date.today();
		this.dateRange.End_Date__c = Date.today();
		
		this.numberOfMonths = 36; //Default - 36 months deferred revenue displayed
		this.currentPage = '0'; //Starting page
		this.visibleResults = 0;
		showData = false;
		showFinancials = true;
		showOnlyInvalid = false;
	}
	
	public void initData() {
		try {
			if(this.recordsPerPage != MAX_RECORD_PER_PAGE)
				this.recordsPerPage = MAX_RECORD_PER_PAGE;
			
			//Initialise month counter
			initialiseMonthCounter();
			
			//Initialise the sortby and sortDir values if null
			if (sortBy == null || sortBy == '') {
				sortBy = 'go1__Reference__c';
			}
			if (sortDir == null || sortDir == '') {
				sortDir = 'ASC';
			}
			
			//Also initialise the column headers for the deferred payment columns
			generateMonthHeaders();
			
			//Initialise the total monthly, yearly and result count figures
			totalYearly = 0;
			totalMonthly = 0;
			totalResults = 0;
			
			Datetime[] periodRange = getPeriod(); //Working from the start and end ates now rather than month by month
				
			//Get the total number of results from the full count query
			this.totalResults = Database.countQuery(this.createCountString(periodRange[0], periodRange[1]));
			
			//Get the total monthly and yearly values from the sumarised query
			List<AggregateResult> sumResults = Database.query(this.createSumString(periodRange[0], periodRange[1]));
			for (AggregateResult ar : sumResults) {
				if (ar.get('Subscription_Type__c') == 'm') {
					this.totalMonthly = (Decimal)ar.get('expr1');
				}
				else if (ar.get('Subscription_Type__c') == 'y') {
					this.totalYearly = (Decimal)ar.get('expr1');
				}
			}
			
			//Generate the result set 
			queryTransactions();
			
		}
		catch(Exception e) {
			System.debug('DEBUG: Exception thrown:' + e.getMessage() + '. StackTrace:' + e.getStackTraceString());
		}
	}
	
	//Get the periodStart and periodEnd values for retrieving the correct transactions
	private Datetime[] getPeriod() {
		Datetime periodStart = Datetime.newInstance(this.dateRange.Start_Date__c.year(), this.dateRange.Start_Date__c.month(), this.dateRange.Start_Date__c.day(), 0, 0, 0);
		Datetime periodEnd = Datetime.newInstance(this.dateRange.End_Date__c.year(), this.dateRange.End_Date__c.month(), this.dateRange.End_Date__c.day(), 23, 59, 59);
		
		return new Datetime[]{periodStart, periodEnd};
	}
	
	//Method to actually query for the matching transactions
	public void queryTransactions() {
		
		//Initialise the results collection - blanking if already has contents
		this.results = new List<DataRow>();
		
		Datetime[] periodRange = getPeriod();
		
		//If a specific page has been selected (e.g. numbered page or first/last), set the correct counter offset for the query
		if (this.currentPage != '0') 
        	counter = recordsPerPage * integer.valueOf(this.currentPage) - recordsPerPage;
		
		//We want to find all of the invalid trans, not just those in the paginated results
		if (showOnlyInvalid == true)
			this.recordsPerPage = 5000;
			
		//Reworked query to use dynamic sorting
		String query = this.createQueryString(periodRange[0], periodRange[1]);
		List<go1__Transaction__c> transactions = Database.query(query); 
				
		List<Id> accIds = new List<Id>(); //Collection to hold the Acc Ids for retrieving the last sub start and end
			
		for (go1__Transaction__c trans : transactions) {
			DataRow dr = new DataRow(trans);
			dr.calculateDates(this.dateRange.Start_Date__c, this.dateRange.End_Date__c);
			results.add(dr);
			
			accIds.add(dr.trans.go1__Account__c); 
		}
		
		//Round the totals to 2 D.P.
		this.totalMonthly = this.totalMonthly.setScale(2, RoundingMode.HALF_UP);
		this.totalYearly = this.totalYearly.setScale(2, RoundingMode.HALF_UP);
			
		//Query for the latest transaction end dates for each Account
		List<AggregateResult> lastSubTrans = [SELECT 
												go1__Account__c,
												max(Subscription_Instance__r.Start_Date__c),
												max(Subscription_Instance__r.End_Date__c)
												FROM go1__Transaction__c
												WHERE go1__Status__c IN ('Approved','Success','Successful Transaction','Successfully added manual tran')
												AND go1__Account__c IN :accIds
												GROUP BY go1__Account__c
												];
			
		Map<String, Date[]> lastDates = new Map<String, Date[]>();
		for (AggregateResult ar : lastSubTrans) {
			lastDates.put(String.valueOf(ar.get('go1__Account__c')), new Date[]{Date.valueOf(ar.get('expr0')),Date.valueOf(ar.get('expr1'))});
		}
		
		//for (DataRow dr : results) {
		for (Integer i = 0; i < results.size(); i++) {
			Date[] dates = lastDates.get(results[i].trans.go1__Account__c);
			if (dates != null && dates.size() > 1) {
				results[i].lastProdStart = dates[0];
				results[i].lastProdEnd = dates[1];
			}
			results[i].generateDeferredValues(numberOfMonths, monthHeaders);
		}
		
		this.visibleResults = this.totalResults; //Populate the visible results variable - used in the pagination when filtering the full resultset
		
		//Finally, if the user has selected the show only invalid rows checkbox, filter out those rows deemed valid
		if (showOnlyInvalid == true) {
			this.recordsPerPage = MAX_RECORD_PER_PAGE;
			showInvalidRowsOnly();
		}
	}
	
	
	
	//Creates the resultset count 
	private String createCountString(Datetime periodStart, Datetime periodEnd){
		String countValue = 'SELECT count() ';
		
		return countValue + createQueryFromString(periodStart, periodEnd);
	}
	
	//Creates the total monthly and yearly figures
	private String createSumString(Datetime periodStart, Datetime periodEnd){
		String countValue = 'SELECT Subscription_Instance__r.Subscription_Type__c, count(Id), sum(go1__Amount__c) ';
		
		return countValue + createQueryFromString(periodStart, periodEnd)
							+ ' AND ((Subscription_Instance__r.Subscription_Type__c = \'y\') OR (Subscription_Instance__r.Subscription_Type__c = \'m\' AND go1__Amount__c < 60 AND go1__Amount__c > -60))' 
							+ ' GROUP BY Subscription_Instance__r.Subscription_Type__c';
	}
	
	//Creates the full string for querying fields for the period passing in args
	private String createQueryString(Datetime periodStart, Datetime periodEnd){
		
		String queryFields = 'SELECT '
				+ 'go1__Account__r.Eureka_ID__c,'
				+ 'go1__Account__r.Long_ID__c, '
				+ 'go1__Account__r.PersonEmail, '
				+ 'go1__Reference__c, ' //Merchant_Ref
				+ 'go1__Date__c, '
				+ 'go1__Account__r.ER_Reporting_Sub_Status__c, '
				+ 'go1__Account__r.ER_Reporting_Sub_Start_Date__c, '
				+ 'Subscription_Instance__r.Is_Renewal__c, '
				+ 'go1__Status__c, '
				+ 'Subscription_Instance__r.Name, '
				+ 'Name, '
				+ 'Subscription_Instance__r.Start_Date__c, '
				+ 'Subscription_Instance__r.End_Date__c, '
				+ 'Subscription_Instance__r.Subscription_Type__c, '
				+ 'Subscription_Instance__r.Original_Start_Date__c, '
				+ 'Subscription_Instance__r.Original_End_Date__c, '
				+ 'go1__Refunded__c, '
				+ 'go1__Amount__c ';
		
		queryfields += createQueryFromString(periodStart, periodEnd) 
				+ ' ORDER BY ' + sortBy + ' ' + sortDir 
				+ ' LIMIT ' + recordsPerPage;
				
		if (recordsPerPage != 5000 && counter != 0) 
			queryFields += ' OFFSET ' + counter; //Don't want to include the offset if we're downloading all records in Batch
		
		return queryFields;
	}
	
	private String createQueryFromString(Datetime periodStart, Datetime periodEnd){
		
		String formattedPeriodStart = periodStart.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
		String formattedPeriodEnd = periodEnd.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
		
		return 'FROM '
				+ 'go1__Transaction__c '
				+ 'WHERE '
				+ 'go1__Status__c IN (\'Approved\',\'Success\',\'Successful Transaction\',\'Successfully added manual transaction\') '
				+ 'AND go1__Date__c >= ' + formattedPeriodStart + ' AND go1__Date__c <= ' + formattedPeriodEnd + ' '
				+ 'AND Subscription_Instance__r.Subscription_Type__c IN (\'m\',\'y\') ';
	}
	
	
	//Downloads the data from the Deferred Payment report into an Excel template
	public void downloadReport() {
		
		//Use a Batch Interface - means we don't need to worry about the Apex timeout limits on creating a huge file
		DeferredPaymentsReport dpr = new DeferredPaymentsReport();
		
		//Set all data fields to visible for the report
		dpr.showData = true;
		dpr.showFinancials = true;
		
		//We need ALL the rows and deferred months, although this is not renderable for the standard VF page (shouldn't be more than 5000 records in any query!)
		dpr.recordsPerPage = 5000; 
		dpr.counter = 0;
		dpr.currentPage = '0';
		dpr.numberOfMonths = TOTAL_MONTHS;
		dpr.totalMonthly = this.totalMonthly;
		dpr.totalYearly = this.totalYearly;
		dpr.dateRange.Start_Date__c = this.dateRange.Start_Date__c;
		dpr.dateRange.End_Date__c = this.dateRange.End_Date__c; 
		dpr.initialiseMonthCounter();
		dpr.generateMonthHeaders();
		
		dpr.sortBy = 'go1__Reference__c';
		dpr.sortDir = 'ASC';
		Database.executeBatch(dpr, 500);
		
		//Display the confirmation pop-up to inform the user the report is being sent to their email address
		displayPopUp = true;
	}
	
	public void closePopup(){
		displayPopUp = false;
	}
	
	/* Batchable implementation - allow us to download reports where more than 2k rows are returned (Apex long execution prevents direct download)
	*/
	String[] lines;
	
	global Iterable<DeferredPaymentsReport.DataRow> start(Database.BatchableContext bc){
		this.lines = new String[0];
		String line = '"Subid","emailaddress","reference","payment_date","substatus","substartdate","isrenewal","pay_status","subscription_instance","transaction_number","last_prod_start","last_prod_end","sub_start","sub_end","sub_type","original_start_date","original_end_date","refunded","amount","subscription_months","accrued_months","months_left","Monthly Allocation","Total P &amp; L Allocation","Deferred Revenue","Adjusted Start Date","Adjusted Start Date","Adjusted End Date","validation"';
		for (Integer m : monthCount){
			Datetime tempMonthHeader = Datetime.newInstance(this.monthHeaders[m].year(), this.monthHeaders[m].month(), this.monthHeaders[m].day(), 0, 0, 0);
			line += ',"' + tempMonthHeader.format('MMM-yy') + '"';
		}
		lines.add(line);
		
		this.queryTransactions();
		Iterable<DeferredPaymentsReport.DataRow> drit = (Iterable<DeferredPaymentsReport.DataRow>)this.results;
		return drit;
	}
	
	global void execute(Database.BatchableContext bc, List<DeferredPaymentsReport.DataRow> dataRows){
		//Iterate round the supplied data rows and add to the lines collection
		for (DeferredPaymentsReport.DataRow dr : dataRows) {
			String line = '"' + dr.SubId + '","' + dr.trans.go1__Account__r.PersonEmail + '","' + dr.trans.go1__Reference__c + '","' + dr.paymentDateDisplay + '","' + dr.subStatus + '","' + dr.trans.go1__Account__r.ER_Reporting_Sub_Start_Date__c + '","' + dr.trans.Subscription_Instance__r.Is_Renewal__c + '","' + dr.trans.go1__Status__c + '","' + dr.trans.Subscription_Instance__r.Name + '","' + dr.trans.Name + '","' + dr.lastProdStart + '","' + dr.lastProdEnd + '","' + dr.trans.Subscription_Instance__r.Start_Date__c + '","' + dr.trans.Subscription_Instance__r.End_Date__c + '","' + dr.sub_type + '","' + dr.originalStartDate + '","' + dr.originalEndDate + '","' + dr.trans.go1__Refunded__c + '","' + dr.trans.go1__Amount__c + '","' + dr.subscriptionMonths + '","' + dr.accruedMonths + '","' + dr.monthsLeft + '","' + dr.monthlyAllocation.setScale(2, RoundingMode.HALF_UP) + '","' + dr.totalPLAllocation.setScale(2, RoundingMode.HALF_UP) + '","' + dr.deferredRevenue.setScale(2, RoundingMode.HALF_UP) + '","' + dr.adjustedStartDate + '","' + dr.adjustedStartDate1 + '","' + dr.adjustedEndDate+ '","' + dr.validation + '"' ;
			
			for (Integer m : monthCount) {
				line += ',"' + dr.monthlyDeferred[m] + '"';
			}		
			lines.add(line);
		}
	}
	
	global void finish(Database.BatchableContext bc) {
		
		//Create the final result Blob
		Blob attach = Blob.valueOf(String.join(lines, '\n'));
		lines = null; 
		
		//Get the email address of the logged-in user
		String toEmail = UserInfo.getUserEmail();
		
		//Email the resulting file to logged in user as an attachment
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        String[] toAddresses = new String[] {toEmail}; 
        mail.setToAddresses(toAddresses); 
        mail.setSubject('Deferred Payment Report: downloaded content'); 
            
        //Attach the finalResult to the email
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        efa.setFileName('DeferredPaymentReport.csv');
        efa.setBody(attach);
        
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            
        string s = 'Your report is attached to this email'; 
        mail.setHtmlBody(s); 
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
        //-------------------------------------
	}
	
	/* --------- */
	
	
	// Filter out the Rows that have validated successfully
	public void showInvalidRowsOnly() {
		
		//If ticked, remove the valid rows from the result set
		for (Integer i = 0; i < this.results.size(); i++) {
			DataRow dr = this.results[i];
			if (dr.validation==0.00) {
				results.remove(i);
				i--; //We've removed i so we need to step back to consider the element taking its place
			}
		}
		
		this.visibleResults = results.size();
	}
	
	public PageReference empty() {
		queryTransactions();
		return null;
	}
	
	//Initialise the month counter collection
	private void initialiseMonthCounter() {
		this.monthCount = new List<Integer>();
		for (Integer i = 0; i < numberOfMonths; i++) {
			monthCount.add(i);
		}
	}
	
	//Create the headers for all possible monthly deferred revenue columns
	public void generateMonthHeaders() {
		
		this.monthHeaders = new Date[TOTAL_MONTHS];
		
		//Find the Accounting Month (month of the date mid-way between the start and end dates)
		Integer daysDiff = this.dateRange.Start_Date__c.daysBetween(this.dateRange.End_Date__c);
		Date midMonth = this.dateRange.Start_Date__c.addDays(daysDiff/2);
		
		Date dateHead = Date.newInstance(midMonth.year(), midMonth.month(), 1);
		this.monthHeaders[0] = dateHead;
			
		for (Integer i = 1; i < TOTAL_MONTHS; i++) {
			this.monthHeaders[i] = dateHead.addMonths(i);
		}
	}
	
	// return a list of 2 years into the future
	public List<SelectOption> getNumberOfMonthsOptions() {
		List<SelectOption> options = new List<SelectOption>();
		for (Integer i = 1; i < 51; i++) {
			String option = string.valueOf(i);
			options.add(new SelectOption(option, option));
		}
		return options;
	}
	
	
	
	/*
	* Pagination buttons section
	*/
	
	public Component.Apex.pageBlockButtons getMyCommandButtons() 
    {
        //the reRender attribute is a set NOT a string
        Set<string> theSet = new Set<string>();
        theSet.add('report-table');
        theSet.add('myButtons');
        theSet.add('pageNumPanel');
                
        integer totalPages;
        if (math.mod(this.visibleResults, recordsPerPage) > 0) {
            totalPages = this.visibleResults/recordsPerPage + 1;
        } else {
            totalPages = (this.visibleResults/recordsPerPage);
        }
        
        integer currentPage;        
        if (this.currentPage == '0') {
            currentPage = counter/recordsPerPage + 1;
        } else {
            currentPage = integer.valueOf(this.currentPage);
        }
        
        Component.Apex.pageBlockButtons pbButtons = new Component.Apex.pageBlockButtons();        
        pbButtons.location = 'top';
        pbButtons.id = 'myPBButtons';
        
        Component.Apex.outputPanel opPanel = new Component.Apex.outputPanel();
        opPanel.id = 'myButtons';
                                
        //the Previous button will alway be displayed
        Component.Apex.commandButton b1 = new Component.Apex.commandButton();
        b1.expressions.action = '{!Previous}';
        b1.title = 'Previous';
        b1.value = 'Previous';
        b1.expressions.disabled = '{!disablePrevious}';        
        b1.reRender = theSet;

        opPanel.childComponents.add(b1);        
                        
        for (integer i=0;i<totalPages;i++) 
        {
            Component.Apex.commandButton btn = new Component.Apex.commandButton();
            
            if (i+1==1) {
                btn.title = 'First Page';
                btn.value = 'First Page';
                btn.rendered = true;                                        
            } else if (i+1==totalPages) {
                btn.title = 'Last Page';
                btn.value = 'Last Page';
                btn.rendered = true;                            
            } else {
                btn.title = 'Page ' + string.valueOf(i+1) + ' ';
                btn.value = ' ' + string.valueOf(i+1) + ' ';
                btn.rendered = false;             
            }
            
            if (   (i+1 <= 5 && currentPage < 5)
                || (i+1 >= totalPages-4 && currentPage > totalPages-4)
                || (i+1 >= currentPage-2 && i+1 <= currentPage+2))
            {
                btn.rendered = true;
            }
                                     
            if (i+1==currentPage) {
                btn.disabled = true; 
                btn.style = 'color:blue;';
            }  
            
            btn.onclick = 'queryByPage(\''+string.valueOf(i+1)+'\');return false;';
                 
            opPanel.childComponents.add(btn);
            
            if (i+1 == 1 || i+1 == totalPages-1) { //put text after page 1 and before last page
                Component.Apex.outputText text = new Component.Apex.outputText();
                text.value = '...';        
                opPanel.childComponents.add(text);
            } 
             
        }
        
        //the Next button will alway be displayed
        Component.Apex.commandButton b2 = new Component.Apex.commandButton();
        b2.expressions.action = '{!Next}';
        b2.title = 'Next';
        b2.value = 'Next';
        b2.expressions.disabled = '{!disableNext}';        
        b2.reRender = theSet;
        opPanel.childComponents.add(b2);
                
        //add all buttons as children of the outputPanel                
        pbButtons.childComponents.add(opPanel);  
  
        return pbButtons;
    }
	
	public PageReference refreshGrid() { //user clicked a page number        
        system.debug('**** ' + this.currentPage);
        queryTransactions();
        return null;
    }
    
    public PageReference Previous() { //user clicked previous button
        this.currentPage = '0';
        counter -= recordsPerPage;
        queryTransactions();
        return null;
    }

    public PageReference Next() { //user clicked next button
        this.currentPage = '0';
        counter += recordsPerPage;
        queryTransactions(); 
        return null;
    }

    public PageReference End() { //user clicked end
        this.currentPage = '0';
        counter = this.visibleResults - math.mod(this.visibleResults, recordsPerPage);
        queryTransactions(); 
        return null;
    }
    
    public Boolean getDisablePrevious() { //this will disable the previous and beginning buttons
        if (counter>0) 
        	return false; 
        else 
        	return true;
    }

    public Boolean getDisableNext() { //this will disable the next and end buttons
        if (counter + recordsPerPage < this.visibleResults) 
        	return false; 
        else 
        	return true;
    }

    public Integer getPageNumber() {
        return counter/recordsPerPage + 1;
    }

    public Integer getTotalPages() {
        if (math.mod(this.visibleResults, recordsPerPage) > 0) {
            return this.visibleResults/recordsPerPage + 1;
        } else {
            return (this.visibleResults/recordsPerPage);
        }
    }
	
	// Pagination ends....
	
	
	
	
	/**
	* Deferred Report row object
	* Holds the field values (calculated and directly queried) for each transaction in the report for the month in question
	*/
	global class DataRow {
		
		public go1__Transaction__c trans {get; set;}
		
		public String subId {
			get{
				if (this.trans.go1__Account__c != null) {
					if (this.trans.go1__Account__r.Eureka_ID__c != null)
						subId = this.trans.go1__Account__r.Eureka_ID__c;
					else
						subId = this.trans.go1__Account__r.Long_ID__c;
				}
				else
					subId = null;
				
				return subId;
			}
			
			set;
		}
		
		public String subStatus {
			get{
				if (this.trans.go1__Account__c != null) {
					String subStatus = this.trans.go1__Account__r.ER_Reporting_Sub_Status__c;
					if (subStatus == '2'){
						return 'comp';
					}
					else if (subStatus == '3'){
						return 'active FT';
					}
					else if (subStatus == '4'){
						return 'active sub';
					}
					else if (subStatus == '8'){
						return 'expired';
					}
					else
						return 'unknown';
				}
				else
					return 'unknown';				
			} 
			
			set;
		}
		
		
		public Date lastProdStart {get; set;} //Created from separate aggregate query
		public Date lastProdEnd {get; set;} //Created from separate aggregate query
		
		public String sub_type {
			get{
				if (this.trans.Subscription_Instance__c != null) {
					if (this.trans.Subscription_Instance__r.Subscription_Type__c == 'y')
						return 'Yearly';
					else if (this.trans.Subscription_Instance__r.Subscription_Type__c == 'm' && this.trans.go1__Amount__c < 60 && this.trans.go1__Amount__c > -60)
						return 'Monthly';
					else
						return 'Other';
				}
				else
					return 'Other';
			}
			
			set;
		}
		
		public Date originalStartDate {get; set;} //Created when start or end dates change - ever happen? Find example?
		public Date originalEndDate {get; set;} //Created when start or end dates change - ever happen? Find example?
		
		public Integer subscriptionMonths {get; set;}
		public Integer accruedMonths {get; set;}
		public Integer monthsLeft {get; set;}
		public Decimal monthlyAllocation {get; set;}
		public Decimal totalPLAllocation {get; set;}
		public Decimal deferredRevenue {get; set;}
		
		public Date adjustedStartDate {get; set;}
		public Date adjustedStartDate1 {get; set;}
		public Date adjustedEndDate {get; set;}
		
		public String[] monthlyDeferred {get; set;} //Use String so we can represent blank cells in the VF report page
		public Decimal validation {get; set;}
		
		public Date paymentDateDisplay { //Display the correct Transaction date allowing for timezone
			get {
				if (this.trans != null && this.trans.go1__Date__c != null)
					return Date.newInstance(this.trans.go1__Date__c.year(), this.trans.go1__Date__c.month(), this.trans.go1__Date__c.day());
				else
					return null;
			}
			
			set;
		}
		
		/* Constructor */
		public DataRow(go1__Transaction__c trans) {
			this.trans = trans;
			if (this.trans.Subscription_Instance__r.Original_Start_Date__c == null)
				this.originalStartDate = this.trans.Subscription_Instance__r.Start_Date__c;
			else
				this.originalStartDate = this.trans.Subscription_Instance__r.Original_Start_Date__c;
				
			if (this.trans.Subscription_Instance__r.Original_End_Date__c == null)
				this.originalEndDate = this.trans.Subscription_Instance__r.End_Date__c;
			else
				this.originalEndDate = this.trans.Subscription_Instance__r.Original_End_Date__c;
		}
		
		/* Method to calculate special date fields 
		 * PeriodStart = the first day of the month for finding the number of months passed and left 
		 */
		public void calculateDates(Date periodStart, Date periodEnd){
			
			/* Subscription Months */
			if (this.originalEndDate > this.originalStartDate)
				this.subscriptionMonths = this.originalStartDate.monthsBetween(this.originalEndDate);
			else
				this.subscriptionMonths = 0;
			
			//As Start and End Dates can be around any date period, find the midpoint between these dates, get that month and adjust
			Integer daysDiff = periodStart.daysBetween(periodEnd);
			Date midMonth = periodStart.addDays(daysDiff/2);
			
			Integer deferredEndAddition = 0;
			Integer deferredStartAddition = 0;
			
			//Populate the adjusted start and end dates
			if (this.originalStartDate > this.originalEndDate)
				this.adjustedStartDate = this.originalEndDate;
			else
				this.adjustedStartDate = this.originalStartDate;
			
			if (this.sub_type == 'Monthly')
				this.adjustedEndDate = Date.newInstance(this.trans.go1__Date__c.year(), midMonth.month(), this.trans.go1__Date__c.day());
			else {
				if (this.adjustedStartDate > periodEnd) //Future sub totally outside the considered accounting period (Check this first to avoid matching any of the other logic scenarios)
					deferredEndAddition = 1;
				else if (this.adjustedStartDate.month() < midMonth.month() && this.adjustedStartDate >= periodStart)
					deferredEndAddition = 0;
				else if (this.adjustedStartDate.month() == periodStart.month() && this.adjustedStartDate < periodStart) //The adjusted start date is ealier but this is still the first month of the subscription
					deferredEndAddition = 0;
				else if (this.adjustedStartDate.month() == midMonth.month() || (this.adjustedStartDate.month() < midMonth.month() && this.adjustedStartDate < periodStart))
					deferredEndAddition = 1;
				else if (this.adjustedStartDate.month() > midMonth.month() && this.adjustedStartDate > midMonth) {
					deferredStartAddition = -1;
					deferredEndAddition = 2;
				}
				else if (this.adjustedStartDate.month() > midMonth.month() && this.adjustedStartDate <= midMonth)
					deferredEndAddition = 1;
			
				this.adjustedEndDate = Date.newInstance(this.adjustedStartDate.year(),
														this.adjustedStartDate.month() + this.subscriptionMonths - deferredEndAddition,
														1);
			}
			
			if (this.adjustedStartDate > this.adjustedEndDate)
				this.adjustedStartDate1 = this.adjustedEndDate.addMonths(deferredStartAddition);
			else
				this.adjustedStartDate1 = this.adjustedStartDate.addMonths(deferredStartAddition);
				
			//Find the adjusted months inbetween (useful for discount calculations)
			Integer discountMonthsBetw = midMonth.monthsBetween(adjustedEndDate) + 1;
				
			
			/* Accrued Months */
			if (this.originalEndDate <= this.originalStartDate)
				this.accruedMonths = 0;
			else {
				
				//If we have a discount, use the number of months between the midMonth and the adjusted end of the subscription
				if (this.trans.go1__Amount__c < 0){
					this.accruedMonths = subscriptionMonths - discountMonthsBetw + 1;
					if (this.accruedMonths < 0)
						this.accruedMonths = 0;
					else if (this.accruedMonths >= 12)
						this.accruedMonths = 12;
				}
				else {
					//Number of months between the start date of the sub and the periodStart used for the first deferred payment month
					Integer monthsBetw = this.originalStartDate.monthsBetween(periodStart);
					if (monthsBetw < 0)
						this.accruedMonths = 0;
					else if (monthsBetw >= 12)
						this.accruedMonths = 12;
					else
						this.accruedMonths = monthsBetw + 1;
				}
			}
			
			/* Months Left */
			this.monthsLeft = subscriptionMonths - this.accruedMonths; //Subscription Months - Accrued Months
			
			/* Monthly Allocation */
			if (this.sub_type == 'Monthly') {
				this.monthlyAllocation = this.trans.go1__Amount__c;
			}
			else if (this.originalEndDate <= this.originalStartDate || subscriptionMonths == 0) {
				this.monthlyAllocation = this.trans.go1__Amount__c;
			}
			else if (this.trans.go1__Amount__c < 0) {
				
				Integer remainMonth = 0;
				if (this.originalEndDate > this.originalStartDate) {
					//Number of months between the start date of the sub and the first of the chosen months
					//Integer monthsBetw = this.originalStartDate.monthsBetween(periodStart);
					Integer monthsBetw = subscriptionMonths - discountMonthsBetw;
					if (monthsBetw < 0)
						remainMonth = 0;
					else if (monthsBetw >= 12)
						remainMonth = 12;
					else
						remainMonth = monthsBetw;
				}
				remainMonth = subscriptionMonths - remainMonth;
				
				if (remainMonth == 0)
					this.monthlyAllocation = this.trans.go1__Amount__c;
				else 
					this.monthlyAllocation = this.trans.go1__Amount__c / remainMonth;
			}
			else {
				//For non-recurring payments (currently not "monthly") - transaction amount divided by the number of subscription months 
				this.monthlyAllocation = this.trans.go1__Amount__c / subscriptionMonths;
			}
			
			/* Total PL Allocation */
			if (this.trans.go1__Amount__c < 0 || this.originalEndDate <= this.originalStartDate) {
				this.totalPLAllocation = 0;
			}
			else {
				Decimal firstValue = 0;
				if (this.sub_type == 'Monthly')
					firstValue = this.trans.go1__Amount__c;
				else if (this.trans.go1__Amount__c < 0) {
					firstValue = this.trans.go1__Amount__c / this.monthsLeft;
				}
				else if (subscriptionMonths == 0) {
					firstValue = 0;
				}
				else {
					firstValue = this.trans.go1__Amount__c / subscriptionMonths;
				}
				
				this.totalPLAllocation = firstValue * this.accruedMonths;
			}
			
			/* Deferred Revenue */
			if (this.sub_type == 'Monthly' || this.originalEndDate <= this.originalStartDate)
				this.deferredRevenue = 0;
			else {
				this.deferredRevenue = this.monthlyAllocation * this.monthsLeft;
			}
		}
		
		// Based on the selected number of months, generate the deferred values
		public void generateDeferredValues(Integer numMonths, Date[] monthHeaders) {
			
			Decimal deferredSum = 0; //Holds the running total of deferred values for the validation value
			this.monthlyDeferred = new String[numMonths];
			
			for (Integer i = 0; i < TOTAL_MONTHS; i++)
			{
				if ((this.adjustedStartDate1.toStartOfMonth() <= monthHeaders[i].toStartOfMonth()) 
						&& (this.adjustedEndDate.toStartOfMonth() >= monthHeaders[i].toStartOfMonth())) {
					deferredSum += this.monthlyAllocation;
					
					if (i < numMonths) {
						monthlyDeferred[i] = String.valueOf(this.monthlyAllocation.setScale(2, RoundingMode.HALF_UP));
					}
				}
				else if (i < numMonths) {
					monthlyDeferred[i] = '';
				}
				else if (this.adjustedEndDate.toStartOfMonth() > monthHeaders[i].toStartOfMonth())
					continue; //a future sub - we haven't started deferring revenue yet, but its in a range beyond whats visible in the VF view
				else
					break; //we have no more deferred payments and we're not displaying any more vals or blanks, we can quit the loop
			}
			
			//Finally set the validation decimal as the sum of these deferred values - the total trans amount
			this.validation = (deferredSum - this.trans.go1__Amount__c).setScale(2, RoundingMode.HALF_UP);
		}
		
	}
}