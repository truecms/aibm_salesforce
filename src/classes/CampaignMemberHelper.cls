/**
 * CampaignMemberHelper is a wrapper class for the CampaignMember object in
 * Salesforce.
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
public with sharing class CampaignMemberHelper {
	
	public CampaignMember member { get; set; }
	public Account account { get; set; }
	
	/**
	 * Constructor for when already initially using a campaign member
	 */
	public CampaignMemberHelper(CampaignMember member) {
		this.member = member;
		this.account = getAccountFromCampaignMember();
	}
	
	/**
	 * Constructor for when starting off with an account
	 */
	public CampaignMemberHelper(Account account) {
		this.account = account;
		this.member = getCampaignMemberFromAccount(account.Id);
	}
	
	/**
	 * Return the campaign object for the current campaign member, it should never return null
	 */
	public Campaign getCampaignFromMember() {
		List<Campaign> campaigns = [SELECT Id, Duration__c FROM Campaign WHERE Id = :this.member.CampaignId LIMIT 1];
		if (!campaigns.isEmpty()) {
			return campaigns[0];
		}
		return null;
	}
	
	/**
	 * Returns a campaign member using the account id and campaign id
	 */
	public CampaignMember getCampaignMemberFromAccount(Id accountId) {
  		// find the single member that is associated with the contact and the supplied campaign id 
  		// [Removed Campaign ID filter - this is called in the constructor to find this.member but uses this.member in the query! Would never return anything]
  		List<CampaignMember> members = [
  			SELECT Id, CampaignId, Next_Offer_Date__c, Status, Response_Type__c 
  			FROM CampaignMember 
  			WHERE //CampaignId = :this.member.CampaignId AND 
  				ContactId IN (
  					SELECT Id 
  					FROM Contact 
  					WHERE AccountId = :accountId
  				)
  			ORDER BY createdDate desc
  			LIMIT 1
  		];
  		
  		if (!members.isEmpty()) {
  			return members[0];
  		}
  		return null;
	}
	
	/**
	 * Returns the account that belongs to the campaign member
	 */
	public Account getAccountFromCampaignMember() {
  		List<Account> accounts = [
  			SELECT Id, ER_Reporting_Sub_End_Date__c
  			FROM Account 
  			WHERE Id IN (
  				SELECT AccountId 
  				FROM Contact 
  				WHERE Id = :this.member.ContactId
  			) 
  			LIMIT 1
  		];
  		
	  	if (!accounts.isEmpty()) {
	  		this.account = accounts[0];
	  		return accounts[0];
	  	}
  		return null;
	}
	
	/**
	 * Returns the best campaign offer from a particular parent using the remaining days of a subscription to rate
	 * The lowest duration without exceeding the remaining days variable will be assigned as the best current offer
	 */
	public Campaign getCurrentBestCampaignOffer(Integer remainingDays) {
		List<Campaign> offers = [
			SELECT Id 
			FROM Campaign 
			WHERE ParentId = :this.member.CampaignId AND 
				Duration__c >= :remainingDays 
			ORDER BY Duration__c ASC 
			LIMIT 1
		];
		
		if (!offers.isEmpty()) {
			// return the best current offer
			return offers[0];
		}
		return null;
	}
	
	/**
	 * Returns the best campaign offer from the current member campaign using the remaining days of a subscription to rate
	 * The highest duration inside the remaining days variable will be assigned as the next best current offer
	 */
	public Campaign getNextBestCampaignOffer(Integer remainingDays) {
		List<Campaign> offers = [
			SELECT Id, ParentId, Duration__c 
			FROM Campaign 
			WHERE ParentId = :this.member.CampaignId AND 
				Duration__c < :remainingDays 
			ORDER BY Duration__c DESC LIMIT 1
		];
		
  		if (!offers.isEmpty()) {
  			// this campaign should always be a child of another campaign
  			if (offers[0].Duration__c == null) {
  				offers[0].Duration__c = 0;
  			}
  			return offers[0];
  		}
  		return null;
	}
	
	/**
	 * Returns a list of all the campaigns this member belongs to
	 * Otherwise null if no campaigns
	 */
	public List<Campaign> getAllParentCampaigns() {
	  	List<Campaign> campaigns = [
	  		SELECT Id 
	  		FROM Campaign 
	  		WHERE Id IN (
	  			SELECT CampaignId 
	  			FROM CampaignMember 
	  			WHERE ContactId = :this.member.ContactId
	  		) 
	  		ORDER BY Weight__c ASC 
	  		LIMIT 1
	  	];
	  	
	  	return campaigns;
	}
	
	/**
	 * Returns the last product instance for identifying the next campaign offer date
	 */
	public Product_Instance__c getLastProductInstance() {
  		// this has to be here encase the member was created from a lead, not a contact
  		if (this.account != null) {
	  		List<Product_Instance__c> instances = [
	  			SELECT Id, End_Date__c 
	  			FROM Product_Instance__c 
	  			WHERE Person_Account__c = :this.account.Id 
	  			ORDER BY End_Date__c DESC 
	  			LIMIT 1
	  		];
	  		
	  		if (!instances.isEmpty()) {
	  			return instances[0];
	  		}
  		}
  		return null;
	}
	
	/**
	 * @deprecated
	 */
	public List<Campaign> getBestParentCampaign() {
		return this.getAllParentCampaigns();
	}

}