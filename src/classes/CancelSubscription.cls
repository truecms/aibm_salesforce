/**
 * CancelSubscription
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
public class CancelSubscription {
	
	// object variables
	public Product_Instance__c subscription { get; public set; }
	public go1__Recurring_Instance__c recurring_instance { get; public set; }
	public Subscription_Product__c product { get; public set; }
	
	// field variables
	public String cancelType { get; public set; }
	public String subscriptionId { get; public set; }
	public String cancelReason { get; public set; }
	public Boolean alreadyCancelled { get; public set; }
	public Boolean alreadyEnded { get; public set; }
	
	// final variables
	public final String NOW = 'now';
	public final String LATER ='later';
	public final String CANCELLED = 'Cancelled';
	
	/**
	 *
	 */
	public CancelSubscription() {
		// set the query parameter to a variable
		this.subscriptionId = ApexPages.currentPage().getParameters().get('id');
		
		// default variables
		this.cancelType = NOW;
		this.alreadyCancelled = false;
		this.alreadyEnded = false;
		
		// initialise the objects
		loadObjects();
	}
	
	/**
	 *
	 */
	public void loadObjects() {
		// load the subscription object
		if (this.subscriptionId != null) {
			this.subscription = [SELECT Person_Account__c, Start_Date__c, End_Date__c, Status__c, Auto_Renewal__c, Is_Renewal__c, Recurring_Instance__c, Subscription_Product__c, Subscription_Type__c FROM Product_Instance__c WHERE Id = :this.subscriptionId];
		}
		
		// load the recurring object
		if (this.subscription.Recurring_Instance__c != null) {
			this.recurring_instance = [SELECT Id, go1__Cancelled__c, go1__Remaining_Attempts__c, go1__Next_Renewal__c, go1__Interval__c FROM go1__Recurring_Instance__c WHERE Id = :this.subscription.Recurring_Instance__c LIMIT 1];
		}
		
		// load the subscription product
		if (this.subscription != null && this.subscription.Subscription_Product__c != null) {
			this.product = [SELECT Id, Name FROM Subscription_Product__c WHERE Id = :this.subscription.Subscription_Product__c LIMIT 1];
		}
		
		if (this.subscription.Status__c == CANCELLED) {
			this.alreadyCancelled = true;
		}
		
		if (this.subscription.End_Date__c < Date.today()) {
			this.alreadyEnded = true;
		}
	}
	
	/**
	 *
	 */
	public void cancelNow() {
		// cancel the recurring payments if available
		if (this.recurring_instance != null) {
			this.recurring_instance.go1__Cancelled__c = true;
			update this.recurring_instance;
		}
		
		// add a note to the subscription's account page for references and reporting
		Note reason = new Note();
		reason.ParentId = subscription.Person_Account__c;
		reason.Title = 'Cancelled Subscription: \''+ this.product.Name +'\'';
		reason.Body = this.cancelReason;
		insert reason;
		
		// cancel and stop subscription renewals
		this.subscription.Status__c = CANCELLED;
		this.subscription.Auto_Renewal__c = false;
		
		// reset the suspension dates (temporary solution until the suspension functionality is resolved)
		this.subscription.Suspend_Subscription_End__c = null;
		this.subscription.Suspend_Subscription_Start__c = null;
		
		// change the end date to today
		this.subscription.End_Date__c = date.today().addDays(-1);
		
		// for the benefit of the Deferred Payment Report, set the Original End Date if the end date is <= start date and its a Yearly sub
		if (this.subscription.End_Date__c <= this.subscription.Start_Date__c && this.subscription.Subscription_Type__c == 'y')
			this.subscription.Original_End_Date__c = this.subscription.End_Date__c;
	}
	
	/**
	 * Sets a reminder for when subscription is to be cancelled
	 * Also modifies recurring instance to ensure if not manually cancelled when specified, 
	 * it won't continually charge the account
	 */
	public Boolean cancelLater(Date cancelDate) {
		// create task
		Task reminder = new Task();
		reminder.ActivityDate = cancelDate;
		reminder.Description = 'Cancellation Reason: ' + this.cancelReason;
		reminder.Subject = 'Subscription Cancellation Reminder: \'' + this.product.Name +'\'';
		reminder.WhatId = this.subscription.Id;
		insert reminder;
		
		// if subscription references recurring object, update the remaining attempts to match cancel date
		// TODO: potentially problematic if remaining attempts is less than 0
		if (this.recurring_instance != null && this.recurring_instance.go1__Remaining_Attempts__c != 0) {
			
			//Get the current Product Instance Subscription End Date - check the subscription is not being lengthened!
			Product_Instance__c current_instance = [SELECT End_Date__c, Start_Date__c, Subscription_Product__r.Duration_In_Months__c 
													FROM Product_Instance__c WHERE Id = :this.subscription.Id LIMIT 1];
			
			//Check the cancellation is not later than the full length of the sub!
			Date startDate = current_instance.Start_Date__c;
			Integer durationMonths = current_instance.Subscription_Product__r.Duration_In_Months__c.intValue();
			
			if (cancelDate <= startDate.addMonths(durationMonths)) {
				
				// simululate the recurring process to find out how many future payments to remove
				Decimal remaining = this.recurring_instance.go1__Remaining_Attempts__c;
				Datetime renewalDate = this.recurring_instance.go1__Next_Renewal__c;
				
				Date correctRenewalDate = renewalDate.date();
				Integer originalDayInMonth = correctRenewalDate.day();
				
				Integer paymentsToAdd = 0; //If the cancelDate is subsequently made later than the first cancellation
				
				while (cancelDate > correctRenewalDate) {
					// reduce the remaining payments by one each time an interval is added (each month)
					if (this.recurring_instance.go1__Interval__c == 'Daily') {
						correctRenewalDate = correctRenewalDate.addDays(1);
					} else if (this.recurring_instance.go1__Interval__c == 'Weekly') {
						correctRenewalDate = correctRenewalDate.addDays(7);
					} else if (this.recurring_instance.go1__Interval__c == 'Monthly') {
						correctRenewalDate = correctRenewalDate.addMonths(1);
						
						//SF-824 need to ensure we maintain the correct day in month if its 30+					
						if (correctRenewalDate.day() < originalDayInMonth && Date.daysInMonth(correctRenewalDate.year(), correctRenewalDate.month()) >= originalDayInMonth) {
							correctRenewalDate = correctRenewalDate.addDays(originalDayInMonth-correctRenewalDate.day());
						}
						
					} else if (this.recurring_instance.go1__Interval__c == 'Yearly'){
						correctRenewalDate = correctRenewalDate.addYears(1);
					} else {
						break;
					}
					remaining--;
					paymentsToAdd++;
				}
				
				//Ensure we don't subtract a negative value from the remaining attempts! Would only happen if canceldate greater than full sub length end date - which we check for below!
				if (remaining < 0)
					remaining = 0;
				
				//Check if the subscription length was shortened or lengthened by the cancellation (up to the maximum of the sub length)
				if (cancelDate > current_instance.End_Date__c)
					this.recurring_instance.go1__Remaining_Attempts__c = paymentsToAdd;
				else
					this.recurring_instance.go1__Remaining_Attempts__c = this.recurring_instance.go1__Remaining_Attempts__c - remaining;
					
				update this.recurring_instance;
				
			}
			else {
				//ERROR: the cancellation is later than the largest possible end date for this subscription - should be extended properly!
				ApexPages.Message msgErr = new ApexPages.Message(ApexPages.Severity.WARNING, 'Cancellation date is later than the latest end date possible for this Subscription: ' + startDate.addMonths(durationMonths));
            	ApexPages.addmessage(msgErr);
            	return false;
			}
		}
		return true;
	} 
	
	/**
	 *
	 */
	public PageReference cancel() {
		if (this.subscription != null) {
			boolean success = true;
			try {
				if (cancelType == NOW) {
					cancelNow();
				} else if (cancelType == LATER) {
					success = cancelLater(this.subscription.End_Date__c);
				}
				
				if (success)
					update this.subscription;
			} catch (DMLException e) {
				System.debug(e);
			}
			
			// redirect back to the subscription page
			// TODO: redirect to refund page
			if (success)
			{
				PageReference page = new PageReference('/'+this.subscriptionId);
				page.setRedirect(true);
				return page;
			}
			else
				return null;
		} else {
			ApexPages.Message msgErr = new ApexPages.Message(ApexPages.Severity.WARNING, 'Invalid product instance!');
            ApexPages.addmessage(msgErr);
            return null;
		}
	}
	
	/**************************************************************************************/
	
	// Test
	@isTest 
	static void testCancelSubscriptionError() {
		CancelSubscription cancelSubscription = new CancelSubscription();
		cancelSubscription.cancel();
		System.assertEquals(null, cancelSubscription.subscription);
	}
	
	// Test
	@isTest 
	static void testCancelSubscriptionSuccessfulNow() {
		Account account = TestAccountHelper.createAccountOnly();
		go1__CreditCardAccount__c creditcard = go1.PaymentHelper.savedTestCreditCard(account);
		
		go1__Recurring_Instance__c recurring = new go1__Recurring_Instance__c();
		recurring.go1__Account__c = account.Id;
		recurring.go1__Amount__c = 1;
		recurring.go1__Interval__c = 'Monthly';
		recurring.go1__Next_Renewal__c = Datetime.now();
		recurring.go1__Remaining_Attempts__c = 0;
		recurring.go1__Payment_Account_Type__c = 'Credit Card';
		recurring.go1__Credit_Card_Account__c = creditcard.Id;
		insert(recurring);
		
		Subscription_Stream__c stream = new Subscription_Stream__c();
		insert stream;
		
		Subscription_Product__c prod = new Subscription_Product__c();
		prod.Subscription_Stream__c = stream.Id;
		prod.Duration__c = 1;
		prod.Duration_units__c = 'month';
		prod.Subscription_Type__c = 'm';
		prod.Price__c = 100;
		insert(prod);
		
		Product_Instance__c cancelledSub = new Product_Instance__c();
		cancelledSub.Person_Account__c = account.Id;
		cancelledSub.Subscription_Product__c = prod.Id;
		cancelledSub.Start_Date__c = date.today();
		cancelledSub.Status__c = 'Cancelled';
		insert(cancelledSub);
		
		// Setup objects
		CancelSubscription cancelSubscription = new CancelSubscription();
		cancelSubscription.subscriptionId = cancelledSub.Id;
		cancelSubscription.loadObjects();
		
		// Cancel Subscription
		System.assertEquals(true, cancelSubscription.alreadyCancelled);
		
		Product_Instance__c Sub = new Product_Instance__c();
		sub.Person_Account__c = account.Id;
		sub.Subscription_Product__c = prod.Id;
		sub.Start_Date__c = date.today();
		sub.Recurring_Instance__c = recurring.Id;
		insert(sub);
		
		cancelSubscription = new CancelSubscription();
		cancelSubscription.subscriptionId = sub.Id;
		cancelSubscription.loadObjects();
		
		System.assertEquals(false, cancelSubscription.alreadyCancelled);
		
		// Cancel Subscription now
		cancelSubscription.cancel();
		
		System.assertEquals('Cancelled', cancelSubscription.subscription.Status__c);
		System.assertEquals(false, cancelSubscription.subscription.Auto_Renewal__c);
		System.assertEquals(true, cancelSubscription.recurring_instance.go1__Cancelled__c);
	}
	
	// Test
	@isTest 
	static void testCancelSubscriptionSuccessfulLater() {
		Account account = TestAccountHelper.createAccountOnly();
		go1__CreditCardAccount__c creditcard = go1.PaymentHelper.savedTestCreditCard(account);
		
		go1__Recurring_Instance__c recurring = new go1__Recurring_Instance__c();
		recurring.go1__Account__c = account.Id;
		recurring.go1__Amount__c = 1;
		recurring.go1__Interval__c = 'Monthly';
		recurring.go1__Next_Renewal__c = Datetime.now().addMonths(1);
		recurring.go1__Remaining_Attempts__c = 6;
		recurring.go1__Payment_Account_Type__c = 'Credit Card';
		recurring.go1__Credit_Card_Account__c = creditcard.Id;
		insert(recurring);
		
		Subscription_Stream__c stream = new Subscription_Stream__c();
		insert stream;
		
		Subscription_Product__c prod = new Subscription_Product__c();
		prod.Subscription_Stream__c = stream.Id;
		prod.Duration__c = 7;
		prod.Duration_units__c = 'month';
		prod.Subscription_Type__c = 'm';
		prod.Price__c = 100;
		insert(prod);
		
		Product_Instance__c Sub = new Product_Instance__c();
		sub.Person_Account__c = account.Id;
		sub.Subscription_Product__c = prod.Id;
		sub.Start_Date__c = date.today();
		sub.Recurring_Instance__c = recurring.Id;
		insert(sub);
		
		// Setup objects
		CancelSubscription cancelSubscription = new CancelSubscription();
		cancelSubscription.subscriptionId = sub.Id;
		cancelSubscription.cancelType = 'later';
		cancelSubscription.loadObjects();
		
		System.assertEquals(6, recurring.go1__Remaining_Attempts__c);
		
		// Cancel Subscription later
		// set cancel date 3 months in the future, so should expect 2 more recurring payments before cancelled
		Date cancelDate = Date.today().addMonths(3);	
		
		//Adjust the cancelDate so that it aligns with the next renewal date so the remaining attempts align etc - only if the canceldate.day is larger than the next renewal day!
		Integer dayAdjustment = cancelDate.day() - recurring.go1__Next_Renewal__c.day();
		if (dayAdjustment>0)
			cancelDate = cancelDate.addDays(-dayAdjustment);
		
		cancelSubscription.cancelLater(cancelDate);
		
		Task reminder = [SELECT Id FROM Task WHERE WhatId = :sub.Id];
		System.assertNotEquals(null, reminder);
		
		recurring = [SELECT Id, go1__Remaining_Attempts__c, go1__Next_Renewal__c FROM go1__Recurring_Instance__c WHERE Id = :recurring.Id];
		System.assertEquals(2, recurring.go1__Remaining_Attempts__c);
		
		cancelSubscription.subscription.End_Date__c = cancelDate;
		update cancelSubscription.subscription; //To update the End date of the subscription based on the last cancel
		
		//Now make a second cancellation, but later than the first and check that the recurring instance increases correctly
		cancelDate = Date.today().addMonths(5);
		dayAdjustment = cancelDate.day() - recurring.go1__Next_Renewal__c.day();
		if (dayAdjustment>0)
			cancelDate = cancelDate.addDays(-dayAdjustment);
		cancelSubscription.cancelLater(cancelDate);
		
		go1__Recurring_Instance__c newRecurring = [SELECT Id, go1__Remaining_Attempts__c FROM go1__Recurring_Instance__c WHERE Id = :recurring.Id];
		System.assertEquals(4, newRecurring.go1__Remaining_Attempts__c);
	}
	
	// Test
	@isTest 
	static void testCancelSubscriptionLaterMethod() {
		Account account = TestAccountHelper.createAccountOnly();
		go1__CreditCardAccount__c creditcard = go1.PaymentHelper.savedTestCreditCard(account);
		
		go1__Recurring_Instance__c recurring = new go1__Recurring_Instance__c();
		recurring.go1__Account__c = account.Id;
		recurring.go1__Amount__c = 1;
		recurring.go1__Interval__c = 'Monthly';
		recurring.go1__Next_Renewal__c = Datetime.now().addMonths(1);
		recurring.go1__Remaining_Attempts__c = 9;
		recurring.go1__Payment_Account_Type__c = 'Credit Card';
		recurring.go1__Credit_Card_Account__c = creditcard.Id;
		insert(recurring);
		
		Subscription_Stream__c stream = new Subscription_Stream__c();
		insert stream;
		
		Subscription_Product__c prod = new Subscription_Product__c();
		prod.Subscription_Stream__c = stream.Id;
		prod.Duration__c = 10;
		prod.Duration_units__c = 'month';
		prod.Subscription_Type__c = 'm';
		prod.Price__c = 100;
		insert(prod);
		
		Product_Instance__c Sub = new Product_Instance__c();
		sub.Person_Account__c = account.Id;
		sub.Subscription_Product__c = prod.Id;
		sub.Start_Date__c = date.today();
		sub.Recurring_Instance__c = recurring.Id;
		insert(sub);
		
		// Setup objects
		CancelSubscription cancelSubscription = new CancelSubscription();
		cancelSubscription.subscriptionId = sub.Id;
		cancelSubscription.cancelType = 'later';
		cancelSubscription.loadObjects();
		
		//Set the end date of the product instance so it is picked up by the cancel method and aligned to the same day as the next renewal attempt! 
		cancelSubscription.subscription.End_Date__c = cancelSubscription.subscription.End_Date__c.addMonths(-4);
		Integer dayAdjustment = cancelSubscription.subscription.End_Date__c.day() - recurring.go1__Next_Renewal__c.day();
		if (dayAdjustment>0)
			cancelSubscription.subscription.End_Date__c = cancelSubscription.subscription.End_Date__c.addDays(-dayAdjustment);
		
		System.assertEquals(9, recurring.go1__Remaining_Attempts__c);
		
		// Cancel Subscription later
		// set cancel date 3 months in the future, so should expect 2 more recurring payments before cancelled  
		cancelSubscription.cancel();
		
		Task reminder = [SELECT Id FROM Task WHERE WhatId = :sub.Id];
		System.assertNotEquals(null, reminder);
		
		recurring = [SELECT Id, go1__Remaining_Attempts__c FROM go1__Recurring_Instance__c WHERE Id = :recurring.Id];
		System.assertEquals(5, recurring.go1__Remaining_Attempts__c);
		
		//Now make a second cancellation, but later than the first and check that the recurring instance increases correctly
		cancelSubscription.subscription.End_Date__c = cancelSubscription.subscription.End_Date__c.addMonths(3);
		cancelSubscription.cancel();
		
		go1__Recurring_Instance__c newRecurring = [SELECT Id, go1__Remaining_Attempts__c FROM go1__Recurring_Instance__c WHERE Id = :recurring.Id];
		System.assertEquals(8, newRecurring.go1__Remaining_Attempts__c);
	}
	
	// Test
	@isTest 
	static void testCancelSubscriptionOnThirtyFirst() {
		Account account = go1.PaymentHelper.savedTestAccount();
    	account.Phone = '12345678';
    	update account;
		go1__CreditCardAccount__c creditcard = go1.PaymentHelper.savedTestCreditCard(account);
		
		go1__Recurring_Instance__c recurring = new go1__Recurring_Instance__c();
		recurring.go1__Account__c = account.Id;
		recurring.go1__Amount__c = 1;
		recurring.go1__Interval__c = 'Monthly';
		//recurring.go1__Next_Renewal__c = Datetime.now().addMonths(1);
		recurring.go1__Next_Renewal__c = Date.newInstance(2013,10,31);
		recurring.go1__Remaining_Attempts__c = 6;
		recurring.go1__Payment_Account_Type__c = 'Credit Card';
		recurring.go1__Credit_Card_Account__c = creditcard.Id;
		insert(recurring);
		
		Subscription_Stream__c stream = new Subscription_Stream__c();
		insert stream;
		
		Subscription_Product__c prod = new Subscription_Product__c();
		prod.Subscription_Stream__c = stream.Id;
		prod.Duration__c = 3;
		prod.Duration_units__c = 'month';
		prod.Subscription_Type__c = 'm';
		prod.Price__c = 100;
		insert(prod);
		
		Product_Instance__c Sub = new Product_Instance__c();
		sub.Person_Account__c = account.Id;
		sub.Subscription_Product__c = prod.Id;
		sub.Start_Date__c = date.today();
		sub.Recurring_Instance__c = recurring.Id;
		insert(sub);
		
		// Setup objects
		CancelSubscription cancelSubscription = new CancelSubscription();
		cancelSubscription.subscriptionId = sub.Id;
		cancelSubscription.cancelType = 'later';
		cancelSubscription.loadObjects();
		
		System.assertEquals(6, recurring.go1__Remaining_Attempts__c);
		
		// Cancel Subscription later
		Date cancelDate = Date.newInstance(2014,1,31);	
		
		cancelSubscription.cancelLater(cancelDate);
		
		Task reminder = [SELECT Id FROM Task WHERE WhatId = :sub.Id];
		System.assertNotEquals(null, reminder);
		
		recurring = [SELECT Id, go1__Remaining_Attempts__c FROM go1__Recurring_Instance__c WHERE Id = :recurring.Id];
		System.assertEquals(3, recurring.go1__Remaining_Attempts__c);
	}
	
	@IsTest
	static void testCancelWithDateLaterThanProductDuration()
	{
		Account account = go1.PaymentHelper.savedTestAccount();
    	account.Phone = '12345678';
    	update account;
		go1__CreditCardAccount__c creditcard = go1.PaymentHelper.savedTestCreditCard(account);
		
		go1__Recurring_Instance__c recurring = new go1__Recurring_Instance__c();
		recurring.go1__Account__c = account.Id;
		recurring.go1__Amount__c = 1;
		recurring.go1__Interval__c = 'Monthly';
		recurring.go1__Next_Renewal__c = Datetime.now().addMonths(1);
		recurring.go1__Remaining_Attempts__c = 9;
		recurring.go1__Payment_Account_Type__c = 'Credit Card';
		recurring.go1__Credit_Card_Account__c = creditcard.Id;
		insert(recurring);
		
		Subscription_Stream__c stream = new Subscription_Stream__c();
		insert stream;
		
		Subscription_Product__c prod = new Subscription_Product__c();
		prod.Subscription_Stream__c = stream.Id;
		prod.Duration__c = 10;
		prod.Duration_units__c = 'month';
		prod.Subscription_Type__c = 'm';
		prod.Price__c = 100;
		insert(prod);
		
		Product_Instance__c Sub = new Product_Instance__c();
		sub.Person_Account__c = account.Id;
		sub.Subscription_Product__c = prod.Id;
		sub.Start_Date__c = date.today();
		sub.Recurring_Instance__c = recurring.Id;
		insert(sub);
		
		// Setup objects
		CancelSubscription cancelSubscription = new CancelSubscription();
		cancelSubscription.subscriptionId = sub.Id;
		cancelSubscription.cancelType = 'later';
		cancelSubscription.loadObjects();
		
		cancelSubscription.subscription.End_Date__c = cancelSubscription.subscription.End_Date__c.addMonths(4);
		
		System.assertEquals(9, recurring.go1__Remaining_Attempts__c);
		
		// Cancel Subscription later
		// added 4 months to the end date of the sub through this cancel method - should fail with message  
		PageReference pr = cancelSubscription.cancel();
		System.assertEquals(pr, null);
				
		recurring = [SELECT Id, go1__Remaining_Attempts__c FROM go1__Recurring_Instance__c WHERE Id = :recurring.Id];
		System.assertEquals(9, recurring.go1__Remaining_Attempts__c);
		
		//Check we have an error message in the ApexMessages object too
		System.assert(ApexPages.getMessages().size() == 1);
	}
	
}