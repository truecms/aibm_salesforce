/**
 * CampaignResults
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
public class CampaignResults {
	
	public List<DataRow> table { get; set; }
	
	public CampaignResults() {
		table = new List<DataRow>();
		getCampaignData();
	}
	
	//
	public void getCampaignData() {
		// query the top 8 most recent campaigns
		Campaign[] campaigns = [SELECT Id, Name, NumberSent, StartDate FROM Campaign WHERE StartDate <= :Date.today() AND IsActive = true ORDER BY StartDate DESC LIMIT 8];
		
		for (Campaign campaign : campaigns) {
			getSubscriptionData(campaign);
		}
	}
	
	//
	public void getSubscriptionData(Campaign campaign) {
		Decimal totalPercentage = 0;
		Decimal freePercentage = 0;
		
		// return the number of subs that were generated from the same campaign
		Integer totalSubCount = [SELECT count() FROM Product_Instance__c WHERE Campaign__c = :campaign.Id];
		if (totalSubCount > 0 && campaign.NumberSent > 0) {
			totalPercentage = (Decimal.valueOf(totalSubCount)/campaign.NumberSent) * 100;
		} else if (totalSubCount > 0 && (campaign.NumberSent == 0 || campaign.NumberSent == null)) {
			totalPercentage = 100;
		}
		
		// return the number of subs that were generated from the same campaign
		Integer freeSubCount = [SELECT count() FROM Product_Instance__c WHERE Subscription_Type__c = 'f' AND Campaign__c = :campaign.Id];
		if (freeSubCount > 0 && campaign.NumberSent > 0) {
			freePercentage = (Decimal.valueOf(freeSubCount)/campaign.NumberSent) * 100;
		} else if (totalSubCount > 0 && (campaign.NumberSent == 0 || campaign.NumberSent == null)) {
			freePercentage = 100;
		}
		
		// add entity to chart
		DataRow row = new DataRow(campaign.Name, campaign.StartDate.format(), totalSubCount, freeSubCount);
		System.debug(row);
		this.table.add(row);
	}
	
	/***********************************************/
	/**
	 *
	 */
	public class DataRow {
		
		public String label { get; set; }
		public String dateLabel { get; set; }
		public Integer Subs { get; set; }
		public Integer Free { get; set; }
		
		public dataRow(String label, String dateLabel, Integer value1, Integer value2) {
			this.label = label;
			this.dateLabel = dateLabel;
			this.Subs = value1;
			this.Free = value2;
		}
	}
	
}