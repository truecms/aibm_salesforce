/**
 * MonthlyRecurringPayments 
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
global with sharing class MonthlyRecurringPayments implements Database.Batchable<Product_Instance__c>, Database.AllowsCallouts {

	public List<Product_Instance__c> instances { get; set; }

	public MonthlyRecurringPayments(List<Product_Instance__c> instances) {
		this.instances = instances;
	}
	
	public static List<Product_Instance__c> getMonthlies() {
		List<Product_Instance__c> subscriptions = 
		[
			SELECT Id, Name, End_Date__c, Person_Account__r.Id, Person_Account__r.Name,
				   Subscription_Product__r.Id, Subscription_Product__r.Name, Subscription_Type__c
			FROM Product_Instance__c
			WHERE Subscription_Type__c = 'm' AND
				  Subscription_Stream__c = 'Eureka Report' AND
				  Recurring_Instance__c <> '' AND
				  (Status__c = '4' OR
				  (Status__c = '' AND Person_Account__r.Sub_Status__c != '8'))  // old subs have no status, use the one on the account instead
				  //End_Date__c <  2013-04-09 AND
				  //End_Date__c >= 2013-03-09 AND
				  
			ORDER BY End_Date__c ASC
		];
		return subscriptions;
	}

    /**
     * The BATCHABLE interface means we can parse a bunch of objects and run any updates on them.
     *
     *
     */ 
    global Iterable<Product_Instance__c> start(Database.batchableContext BC){
        List<Product_Instance__c> batchInstances = this.instances;
        return batchInstances;
    }
    
    /**
     *
     */
    global void execute(Database.BatchableContext BC, Product_Instance__c[] instances) {
        if (instances.size() == 1) {
        	
        	ProductInstanceHelper helper = new ProductInstanceHelper(instances[0]);
        	helper.instance = helper.loadProductInstance();
        	// has recurring schedule already?
        	if (false && helper.hasRecurringSchedule()) {
        		/*
        		if (helper.isRewewalDue()) {
        		
        		}
        		*/
        	}
        	else {
        		// get the number of payments 
        		go1__Recurring_Instance__c recurring = helper.setupRecurringPayments();
       			if (recurring.go1__Next_Renewal__c < date.newInstance(2013, 3, 9)) {
       				recurring.go1__Next_Renewal__c = helper.instance.Start_Date__c.addMonths(Integer.valueOf(recurring.go1__Number_Processed__c));
       				recurring.go1__Next_Attempt__c = recurring.go1__Next_Renewal__c;
       			}
       			upsert recurring;
       			/*
       			helper.instance.Recurring_Instance__c = recurring.Id;
				helper.instance.End_Date__c = helper.instance.Start_Date__c.addDays(365);
				//helper.instance.Subscription_Renewal_Date_c = helper.instance.End_Date__c;
       			upsert helper.instance;
       			*/
       			
       			List<go1__Transaction__c> trans = helper.loadTransactions();
       			if (trans != null) {
       				for (go1__Transaction__c t : trans) {
       					t.go1__Recurring_Instance__c = recurring.Id;
       				}
       				upsert trans;
       			}
        	}
        } else {
            // we can't handle processing multiple at once - just let the system break and someone will hopefully find this is the reason.
        }
    }
	
    /**
     *
     */
    global void finish(Database.BatchableContext BC){
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob WHERE Id =
            :BC.getJobId()];
    }

	@isTest
	static void testSetupMonthlyRecurring() {
		List<Product_Instance__c> instances = MonthlyRecurringPayments.getMonthlies();
		
		Subscription_Stream__c stream = new Subscription_Stream__c(Name='Eureka Report');
		insert stream;
		
		Subscription_Product__c prod = new Subscription_Product__c(Name='test'); 
		prod.Number_of_Payments__c = 12;
		prod.Duration__c = 12;
		prod.Duration_units__c = 'month';
		prod.Subscription_Stream__c = stream.Id;
		insert prod;

		Account account = TestAccountHelper.createAccount();
		
		Product_Instance__c instance = new Product_Instance__c();
    	instance.Person_Account__c = account.Id;
		instance.Amount__c = 10;
		instance.Subscription_Product__c = prod.Id;
		instance.Subscription_Type__c = 'Monthly';
		instance.Subscription_Stream__c = 'Eureka Report';
		insert instance;
		
		instances.add(instance);
		MonthlyRecurringPayments payments = new MonthlyRecurringPayments(instances);
		Test.startTest();
	  	ID batchprocessid = Database.executeBatch(payments, 1);
		Test.stopTest();
	}
}