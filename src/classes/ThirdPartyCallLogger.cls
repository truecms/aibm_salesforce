/**
 * Controller: Third Party Call Logger 
 *
 * Imports a CSV of calls logged with a 3rd party that we want to record within SF 
 *
 * Author: tailbyr
 * Copyright: EurekaReport.com.au
 */
public with sharing class ThirdPartyCallLogger {

	public Map<String, Task[]> createTasks { get; set; }

	public boolean uploadFile { get; set; }
	public transient string nameFile { get; set; }
    public transient Blob contentFile { get; set; }
    
	private transient String[] filelines = new String[]{};
    
    public Map<String, integer> invalidEmails { get; set; }
    public boolean invalidEmail { get; set; }
    public integer totalCallsAdded { get; set; }
    
    public ThirdPartyCallLogger(){
    	if (createTasks == null || createTasks.size()<1){
    		uploadFile = true;
    	}
    	this.totalCallsAdded = 0;
    }	
	
	public PageReference readFile() {
    	if (nameFile != null) {
            nameFile=contentFile.toString();
            
            if (!nameFile.contains('\n') && !nameFile.contains('\r')) {
                ApexPages.Message msgErr = new ApexPages.Message(ApexPages.Severity.ERROR, 'Import file does not contain any line breaks! Please check your file format and ensure that each Call entry is given on a new line and columns are separated by commas.');
                ApexPages.addmessage(msgErr);
                return null;
            }
            
            invalidEmails = new Map<String, integer>(); //Initialise the invalid addresses
            createTasks = new Map<String, Task[]>(); //Initialise the map of emails and task arrays
            
            if (nameFile.contains('\r'))
				filelines = nameFile.split('\r');
			else	        
	        	filelines = nameFile.split('\n');
            
            //Get the System Admin user who will be the assignee for all tasks
            User sysAdmin = getSysAdminUser();
            if (sysAdmin == null) {
            	ApexPages.Message msgErr = new ApexPages.Message(ApexPages.Severity.ERROR, 'Could not find the System Admin user who will be the assignee of these tasks. Please contact your Salesforce Administrator');
            	ApexPages.addmessage(msgErr);
            	return null;
            }
            
            for (Integer i=0;i<filelines.size();i++)
            {
            	String[] inputvalues = new String[]{};
                inputvalues = filelines[i].split(',');
                
                //Check the first row is our header row - if so, skip
                if (i == 0 && inputvalues[0].trim() == 'Email Address')
                	continue;
                
                //If the row is totally blank, just ignore it and carry on
                if (filelines[i].length() < 2 || inputvalues.size()<1) {
                	continue;
                }
                
                String emailValue = inputvalues[0].trim();
                Task tempTask = null;
                
                //Also continue if the email address is blank
                if (emailValue.length()<1) {
                	continue;
                }
                else {
                
	                //Create the task per row here, then assign to our Map to check valid, existing accounts
			        tempTask = new Task();
			        tempTask.Subject = 'OneVue Call';
			        tempTask.Status = 'Completed';
			        tempTask.OwnerId = sysAdmin.Id;
			                
			        if (inputvalues.size()>1)
			        	tempTask.ActivityDate = date.parse(inputvalues[1].trim());
					if (inputvalues.size()>2)
			        	tempTask.Subject += ': Agent: ' + inputvalues[2].trim();
			        if (inputvalues.size()>3)
			        	tempTask.Call_Result__c = inputValues[3].trim();
			        if (inputvalues.size()>4)
			        	tempTask.Description = inputValues[4].trim();
			                	
			        //Add our new task to the lookup map used later
			        if (createTasks.containsKey(emailValue)) {
			        	Task[] taskArr = createTasks.get(emailValue);
			        	taskArr.add(tempTask);
			        	createTasks.put(emailValue, taskArr);
			        }
			        else {
			        	createTasks.put(emailValue, new Task[]{tempTask});
			        }
			        
                }
            }
            
            if (createTasks.size() > 0) {
            	
            	//Grab the Accounts from the emails - these are the Accounts that we're gonna create tasks against
            	List<Account> accs = [SELECT Id, PersonContactId, PersonEmail FROM Account WHERE PersonEmail in :createTasks.keySet()];
            	
            	//Now we need to check if there were any accounts in the csv that did not retrieve Accounts from SF
            	Set<String> setCustomers = new Set<String>();
            	setCustomers.addAll(createTasks.keySet());
            	for (Account a : accs) {
            		if (setCustomers.contains(a.PersonEmail))
            			setCustomers.remove(a.PersonEmail);
            				
            		//We also need to set the matching tasks with the account id
            		if (createTasks.containsKey(a.PersonEmail)){ //@TODO: THIS IS THE SAME AS THE 'IF' ABOVE - CAN WE REMOVE IT?
            			Task[] updateTasks = createTasks.get(a.PersonEmail);
            			for (Task t : updateTasks){
            				t.WhatId = a.Id;
            			}
            		}
            	}
            		
            	//Whatever is left in the setCustomers object has not been returned, therefore add to invalidEmails list
            	for (String invalidCustomer : setCustomers){
            		invalidEmails.put(invalidCustomer, createTasks.get(invalidCustomer).size());
            	}
            		
            	//If we weren't able to find a matching account for the email address, remove it!
            	for (String invEmail : invalidEmails.keySet()){
            		createTasks.remove(invEmail);
            	}
            	
            	//Hide the upload section and show the allocator!
            	uploadFile = false;
            }
            
            //Show the invalid email section if applicable
            if (invalidEmails.size() > 0)
            	invalidEmail = true;
              
        }
        
        //Find the total amount of Calls that will be inserted when the user confirms
        for (Task[] tasks : createTasks.values()){
        	this.totalCallsAdded += tasks.size();
        }
        
        return null;
    }
    
    /* Method to save all the created tasks to DB
     * Called from the VF page on click of 'Create Calls' button
     */
    public PageReference persistTasks(){
    	
    	List<Task> allTasks = new List<Task>();
    	
    	for (Task[] taskArr : this.createTasks.values()){
    		allTasks.addAll(taskArr);
    	}
    	
    	insert allTasks;
    	
    	this.invalidEmail = false;
    	this.totalCallsAdded = allTasks.size();
    	
    	//Redirect the user to the completion page so that they cannot resubmit and create duplicate calls!
    	PageReference pr = new PageReference('/apex/third_party_call_logger_result');
    	pr.getParameters().put('totalCallsAdded', String.valueOf(this.totalCallsAdded));
    	pr.setRedirect(true);
    	
    	return pr;
    }
    
    public User getSysAdminUser(){
    	//We want to create all of these call tasks under System Admin
    	List<User> sysAdmin = [SELECT Id, Name FROM User WHERE Alias = 'sadm' AND IsActive = true];
    	if (sysAdmin.size()>0)
    		return sysAdmin[0];
    	else
    		return null;
    }
	
}