/**
 * TestAccountHelper 
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
@isTest(SeeAllData=true)
private class TestGo2Debit {
	
	// Test for PaymentController
	public static testMethod void testPaymentOptionsController() {
		Go2Debit_PaymentController controller = new Go2Debit_PaymentController();
		
		List<SelectOption> options = controller.getPaymentGateways();
		//System.assertEquals(1, options.size());
		
		Account account = TestAccountHelper.createAccountOnly();
		controller.account = account;
		
		System.assertEquals(true, controller.getPaymentAccountsEmpty());
		
     	go1__CreditCardAccount__c creditcard = go1.PaymentHelper.savedTestCreditCard(account);
     	
     	//Because Go1 are shite and hardcoded a now-EXPIRED expiry date into their Managed Package, we have to update this in our code
     	Date expiry = Date.today().addMonths(6);
     	creditcard.go1__Expiry__c = String.valueOf(expiry.month()) + '/' + String.valueOf(expiry.year());
     	update creditcard;
     	
		List<SelectOption> accounts = controller.getPaymentAccounts();
		System.assertEquals(1, accounts.size());
	}
	
	// Test for CreditCardController
	public static testMethod void testPaymentCreditCardController() {
		Go2Debit_CreditCardController controller = new Go2Debit_CreditCardController();
		
		//Create a new Credit Card to test the expiry methods
		go1__Payment_Gateway__c testGateway = go1.PaymentHelper.savedTestPaymentGateway();
		go1__CreditCardAccount__c creditCard = new go1__CreditCardAccount__c(name='Test Card', 
																			go1__Card_Type__c='VISA', 
																			go1__Expiry__c='4/2015',
																			go1__Merchant_ID__c='1',
																			go1__Payment_Gateway__c=testGateway.Id,
																			go1__Token__c='12345');
		insert creditCard;
		
		controller.creditcard = [SELECT Id, Expiry_Month__c, Expiry_Year__c, go1__Expiry__c FROM go1__CreditCardAccount__c WHERE Id = :creditCard.Id LIMIT 1];
		
		// Test that expiry values
		List<SelectOption> months = controller.getMonthOptions();
		System.assertEquals(months.size(), 12);
		
		List<SelectOption> years = controller.getYearOptions();
		System.assert(years.size()>5); //Check we have at least 5 entries!
		
		String monthVal = controller.getMonthValue();		
		System.assertEquals(monthVal, '4');
		controller.setMonthValue('10');
		System.assertEquals(controller.getMonthValue(), '10');
		
		String yearVal = controller.getYearValue();
		System.assertEquals(yearVal, '2015');
		controller.setYearValue('10');
		System.assertEquals(controller.getYearValue(), '10');
	}
	
	
	// Test for CreditCardController cardtypes - uses the existing system data
	@IsTest(SeeAllData=true)
	public static void testCreditCardControllerCardTypes() {
		Go2Debit_CreditCardController controller = new Go2Debit_CreditCardController();
		
		// Test we get the correct card types
		List<SelectOption> cardTypes = controller.getCardTypes();
		Set<String> cardTypeNames = new Set<String>();
		for (SelectOption ct : cardTypes) {
			cardTypeNames.add(ct.getValue());
		}
		if (cardTypeNames.contains('MASTERCARD') &&
				cardTypeNames.contains('VISA') && 
				cardTypeNames.contains('AMEX') &&
				cardTypeNames.contains('DINERS'))
			System.assert(true);
		else
			System.assert(false);
	}
}