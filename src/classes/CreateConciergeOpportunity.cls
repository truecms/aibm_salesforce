/**
 *
 * Class to create Concierge opportunities using Email Service realtime.
 * Created on 17-11-2015
 * Author: Mithun Roy
 * Copyright: EurekaReport.com.au
 */
global class CreateConciergeOpportunity implements Messaging.InboundEmailHandler {
    //Method to parse the inbound email received by Email Service.
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        try {
            String subToCompare = 'Create Concierge Opportunity';
            if (email.subject.equalsIgnoreCase(subToCompare)) {
                String fullname = null;
                String emailaddress = null;
                String phonenumber = null;
                String datetocall = null;
                String timetocall = null;
                String type = 'Existing Business';
                
                // Retrieves content from the email and splits each line by the terminating newline character.
                String[] emailBody = email.plainTextBody.split('\n', 0);
                for (integer i=0 ; i < emailBody.size(); i++) {
                    String FieldName = emailBody[i].substringBefore(':').trim();
                    //Get each field values.
                    if (FieldName == 'name') {
                        fullname = emailBody[i].substringAfter(':').trim();
                    }
                    if (FieldName == 'email') {
                        emailaddress = emailBody[i].substringAfter(':').trim();
                    } 
                    if (FieldName == 'phone_number') {
                        phonenumber = emailBody[i].substringAfter(':').trim();
                    } 
                    if (FieldName == 'preferred_date_of_call') {
                        datetocall = emailBody[i].substringAfter(':').trim();
                    } 
                    if (FieldName == 'preferred_time_of_call') {
                        timetocall = emailBody[i].substringAfter(':').trim();
                    }                                                 
                }
                
                //Check if Account exists, if not create new account.
                Account[] a = [select Id from Account WHERE PersonEmail = :emailaddress limit 1];
                
                if (a.size() == 0) {
                    Account acct =  new Account();
                    acct.LastName = fullname;
                    acct.PersonEmail = emailaddress;
                    acct.Phone = phonenumber;                    
                    insert acct;
                    type = 'New Business';
                }
                Account[] b = [select Id, Name from Account WHERE PersonEmail = :emailaddress limit 1];
                
               //Query the custom setting 'UnbounceRouting' and get the agent name from custom setting. This gives flexibility to change agents as required.
                List<UnbounceRouting__c> Ag  = [select Name, RowId__c from UnbounceRouting__c order by name asc];   
                                  
                // FInd the least loaded agent
                Integer maxcnt = 0;
                Integer nxtcnt = 0;
                Integer value = 0;
                String agentId = null;
                
                //Find the least loaded agent by iterating though all agents setup in custom setting 'UnbounceRouting'.
                for (UnbounceRouting__c c : Ag) { 
                    List<AggregateResult> results = [select count(Id) cnt from Opportunity where OwnerId = :c.RowId__c and StageName not in('Funds Received','Closed Won', 'Closed Lost', 'Closed No Contact', 'Closed Invalid Contact')];
                    value = (Integer)results[0].get('cnt');
                    if (c.Name == 'Agent1') {
                        maxcnt = value;
                        nxtcnt = value;
                    }
                    else if ( maxcnt < value ) {
                        maxcnt = value;
                    }
                    else if ( value < nxtcnt ) {
                        nxtcnt = value;
                        agentId = c.RowId__c;
                    }
                }
                
                //Convert the preferred date from String to date
                String yy=datetocall.subString(0,4);
                String mm=datetocall.subString(5,7);
                String dd=datetocall.subString(8,10);
                Date prefDate=Date.newInstance(integer.valueOf(yy),integer.valueOf(mm),integer.valueOf(dd));
                System.debug('prefDate :='+prefDate);

                // Create Opportunity.
                if (maxcnt > 0 && (b[0].Name != null)) {
                    Opportunity o = new Opportunity();
                    o.Name = b[0].Name+' : Hot Phone Call Opt In (Concierge Form)';
                    o.AccountID = b[0].Id;
                    o.CloseDate= System.Today() + 30; // Set this to current date + 30 days.
                    o.StageName= '30 Day Trial';  
                    o.Type = type; 
                    o.Description = email.plainTextBody;
                    o.Preferred_Date_to_Call__c = prefDate;
                    o.Best_time_to_call__c = timetocall;                   
                    if (agentId == null) {
                       o.OwnerId = '00590000004DqaY'; 
                    }
                    else {
                        o.OwnerId = agentId;
                    }
                                           
                    insert o;                        
               }
            }
        
        } catch(DmlException e) {
              throw e;
        }
        result.success = true;
        return result;
    }
}