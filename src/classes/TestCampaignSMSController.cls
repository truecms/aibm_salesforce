@IsTest
public class TestCampaignSMSController 
{
	public static FINAL String FIRSTNAME = 'Test';
	public static FINAL String LASTNAME = 'User';
	public static FINAL String CAMPAIGNNAME = 'Unit Test Campaign';
	
	private static RecordType getAccountRecordType()
	{
		// Find any person account record type
	    RecordType recordType = [ select Id, Name, DeveloperName from RecordType where SObjectType = 'Account' and IsPersonType = true limit 1 ];
	    return recordType;
	}
	
	private static Campaign setupCampaign(Integer noOfMembers)
	{
		Campaign testCampaign = new Campaign();
		testCampaign.Name = 'Unit Test Campaign';
		testCampaign.IsActive = true;
		testCampaign.StartDate = Date.today();
		testCampaign.EndDate = Date.today().addDays(10);
		testCampaign.Status = 'Planned';
		insert testCampaign;
		
		//Get the Account RecordType
		RecordType accRecord = getAccountRecordType();
		
		List<Account> accounts = new List<Account>();
		for (Integer i = 0; i < noOfMembers; i++) {
			Account a = new Account();
			a.RecordTypeId = accRecord.Id;
			a.FirstName = 'Test'+i;
			a.LastName = 'User'+i;
			a.Personemail = 'test.user@test.com.test'+i;
			a.PersonMobilePhone = '040012345'+i;
			a.PersonTitle = 'Mr';
			accounts.add(a);
		}
		insert accounts;
		
		List<Account> newAccs = [SELECT Id, PersonContactId FROM Account WHERE PersonEmail like 'test.user@test.com.test%'];
		
		List<CampaignMember> members = new List<CampaignMember>();
		for (Integer i = 0; i  < noOfMembers; i++) {
			CampaignMember cm = new CampaignMember();
			cm.CampaignId = testCampaign.Id;
			cm.ContactId = newAccs[i].PersonContactId;
			cm.Status = 'Sent';
			members.add(cm);
		}
		insert members;
		
		return testCampaign;
	}
	
	static TestMethod void testCountCampaignMembers()
	{
		Integer noOfMembers = 5;
		Campaign testCampaign = setupCampaign(noOfMembers);
		
		Test.setCurrentPageReference(new PageReference('Page.CampaignSMSSend'));
		System.currentPageReference().getParameters().put('recordIds', testCampaign.Id);
		
		CampaignSMSController cont = new CampaignSMSController();
		
		//Setup the status filters
		cont.populateStatusFieldDropDown();
		
		//Iterate round all status options and select them all (only using Send for test)
		for (CampaignSMSController.StatusVar sv : cont.statusFieldDropDown) {
			sv.selected = true;
		}
		
		//Test the campaign name is retrieved
		System.assertEquals(cont.campaignName, testCampaign.Name);
		
		//Check the right number of valid Campaign members are included
		cont.getTotalCount();
		System.assertEquals(cont.totalContacts, noOfMembers);
		
		//Check that the invalid campaign members have been excluded
		Contact c = new Contact();
		c.FirstName = 'invalid';
		c.LastName = 'TestUser';
		c.email = 'invalidtest.user@test.com.test';
		c.MobilePhone = '0200123451';
		c.Title = 'Mr';
		insert c;
		CampaignMember cm = new CampaignMember();
		cm.CampaignId = testCampaign.Id;
		cm.ContactId = c.Id;
		cm.Status = 'Sent';
		insert cm;
		
		cont.getTotalCount();
		System.assertEquals(cont.invalidReceivers, 1);
	}	
	
	static TestMethod void testFilterStatusLoad()
	{
		Campaign testCampaign = setupCampaign(1);
		
		Test.setCurrentPageReference(new PageReference('Page.CampaignSMSSend'));
		System.currentPageReference().getParameters().put('recordIds', testCampaign.Id);
				
		CampaignSMSController cont = new CampaignSMSController();
		System.assert(cont.statusFieldDropDown.size()>0);
	}
	
	
	static TestMethod void testRunCampaign()
	{
		Integer noOfMembers = 5;
		String templateName = 'TestTemplate';
		STring templateText = 'This is a test template. {!Contact.Name} and {!Campaign.Name}.';
		Campaign testCampaign = setupCampaign(noOfMembers);
		
		//Also populate a template for use in the Campaign send
		smagicinteract__SMS_Template__c template = new smagicinteract__SMS_Template__c();
		template.smagicinteract__Text__c = templateText;
		template.smagicinteract__Name__c = templateName;
		template.smagicinteract__ObjectName__c = 'Contact';
		insert template;
		
		Test.setCurrentPageReference(new PageReference('Page.CampaignSMSSend'));
		System.currentPageReference().getParameters().put('recordIds', testCampaign.Id);
		
		CampaignSMSController cont = new CampaignSMSController();
		
		//Iterate round all status options and select them all (only using Send for test)
		for (CampaignSMSController.StatusVar sv : cont.statusFieldDropDown) {
			sv.selected = true;
		}
		
		//Get the right number of valid Campaign members are included
		cont.getTotalCount();
		
		//Set the text template
		cont.contactTemplateName = templateText;
		cont.changeTemplateText();
		
		System.assertEquals(cont.contactTextContent, templateText);
		
		//Run the campaign template renderer
		cont.runCampaign();
		
		//We should have noOfMembers sms entries to send
		System.assertEquals(cont.smsToSend.size(), noOfMembers);
		
		//Check the rendered text message has the fields populated with the right contact details
		for (Integer i = 0; i < cont.smsToSend.size(); i++) {
			System.assertEquals(cont.smsToSend[i].smagicinteract__SMSText__c, 'This is a test template. ' + FIRSTNAME+i + ' ' + LASTNAME+i + ' and ' + CAMPAIGNNAME + '.');
		}
		
		//After all of this, call the method to actually send the SMS messages (although they won't send if called from Unit Test)
		cont.sendSMS();
		
		System.debug('DEBUG: Result Text:' + cont.resultText);
		System.assertEquals(cont.resultText, noOfMembers + ' SMS Messages sent successfully!');
		System.assert(cont.displayFinish, true);
	}
	
	static TestMethod void testShowHidePopup()
	{
		Campaign testCampaign = setupCampaign(1);
		Test.setCurrentPageReference(new PageReference('Page.CampaignSMSSend'));
		System.currentPageReference().getParameters().put('recordIds', testCampaign.Id);
		
		CampaignSMSController cont = new CampaignSMSController();
		cont.showPopup();
		System.assertEquals(cont.displayPopup, true);
		cont.closePopup();
		System.assertEquals(cont.displayPopup, false);
	}
	
	static TestMethod void testCancelSMS()
	{
		Campaign testCampaign = setupCampaign(1);
		Test.setCurrentPageReference(new PageReference('Page.CampaignSMSSend'));
		System.currentPageReference().getParameters().put('recordIds', testCampaign.Id);
		System.currentPageReference().getParameters().put('retURL', '/' + testCampaign.Id);
		
		CampaignSMSController cont = new CampaignSMSController();
		PageReference ref = cont.cancelSMS();
		
		System.assertEquals(ref.getURL(), '/' + testCampaign.Id);
		System.assert(ref.getRedirect(), true);		
	}
	
}