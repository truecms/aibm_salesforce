/**
* Batch job that runs after the Go1 recurring "monthly" payments processor to find all failed monthly transactions and creates a task allocated to an Account Manager for it
* Author: tailbyr
* Copyright: EurekaReport.com.au
*/
global with sharing class FailedRecurringPaymentsTasks implements Schedulable, Database.Batchable<Product_Instance__c>, Database.Stateful {

	public static String lastAccountManager { get; set; }
	
	public TaskManager tm {get; set;}
	
	public String failedRecurringQueue {get; set;}
	public Boolean failedRecurringAllocationRules {get; set;}
	
	//Constructor - get the lastAccountManager given one of the 'Failed Recurring Payments' tasks
	public FailedRecurringPaymentsTasks()
	{
		// Get the Account Manager/Owner of the last No Credit Card task so that we can assign the tasks equally
		List<Task> lastFailedRecurringPaymentTask = [SELECT Id, ownerId FROM Task WHERE Subject = :TaskManager.FAILED_MONTHLY_PAYMENT_SUBJECT ORDER BY createdDate desc LIMIT 1];
		if (lastFailedRecurringPaymentTask.size()>0)
			lastAccountManager = lastFailedRecurringPaymentTask[0].ownerId;
			
		//Get the customer settings allocation rule and queue for this Expired FT automated task creator
		Allocation_Rules__c allocationRules = Allocation_Rules__c.getInstance('Default');
		failedRecurringAllocationRules = allocationRules.Not_Renewed_Allocation_Rule__c;
		failedRecurringQueue = allocationRules.Not_Renewed_Queue__c;
		
		tm = new TaskManager(failedRecurringQueue); //Instigate the TaskManager and tell it which User Queue to use from Custom Settings
	}
	
	//Schedulable override method: execute - called when the scheduled job is kicked off
	global void execute(SchedulableContext ctx)
	{
		Database.executeBatch(new FailedRecurringPaymentsTasks(), 1);
	}
	
	//Batchable override method: start - queries the records to iterate over
	global Iterable<Product_Instance__c> start(Database.batchableContext BC) {
		
		List<go1__Transaction__c> transList = [SELECT Subscription_Instance__c FROM go1__Transaction__c
										WHERE LastModifiedDate >= TODAY
										AND go1__Recurring_Instance__r.go1__Next_Attempt__c <= :Datetime.now()
										AND go1__Recurring_Instance__r.go1__Expired__c = true
										AND go1__Recurring_Instance__r.go1__Cancelled__c = true
										AND go1__Code__c > 10]; //All Product Instances with Recurring elements and failed transactions
		
		List<Id> subs = new List<Id>();
		for (go1__Transaction__c trans : transList) {
			subs.add(trans.Subscription_Instance__c);
		}
		
		List<Product_Instance__c> piList = [SELECT Id, End_Date__c, Person_Account__c FROM Product_Instance__c 
										WHERE Id IN : subs];
		
		//We also need to get the Monthly Recurring Charges that are due and do not have Credit Cards! (Ideally these should have been picked up already! Cancel/Expire the Recurring instance afterwards)
		//NB: If the Product Instance has already expired, no task will be created (handled in the pih method - not current sub!)
		List<Product_Instance__c> noCCList = [SELECT Id, End_Date__c, Person_Account__c  FROM Product_Instance__c 
										WHERE Recurring_Instance__r.go1__Next_Attempt__c <= :Datetime.now()
										AND Recurring_Instance__r.go1__Expired__c = false 
										AND Recurring_Instance__r.go1__Cancelled__c = false 
										AND Recurring_Instance__r.go1__Credit_Card_Account__c = null
										AND Recurring_Instance__r.go1__Payment_Account_Type__c = 'Credit Card'];
		
		piList.addAll(noCCList);
		
		return piList;
	}
	
	global void execute(Database.BatchableContext BC, List<Product_Instance__c> instances)
	{
		ProductInstanceHelper piHelper = new ProductInstanceHelper(instances);
		piHelper.createFailedRecurringPaymentTasks(TaskManager.FAILED_MONTHLY_PAYMENT_SUBJECT, tm, failedRecurringAllocationRules);
	}
	
	//Batchable override method: finish - called when all batches are processed; allocates all fair tasks created in a fair manner across Acc mgrs
	global void finish(Database.BatchableContext BC)
	{
		//Now all the tasks have been created, grab all created tasks and reallocate fairly - CC expiry should ALWAYS be allocated fairly
		List<Task> allFailedRecurringTasksToday = [SELECT Id, ownerId FROM Task 
				where Subject = :TaskManager.FAILED_MONTHLY_PAYMENT_SUBJECT 
				AND createdDate = TODAY
				AND CallDisposition = :TaskManager.ALLOCATION_DISPOSITION];
				
		System.debug('DEBUG: allFailedMonthlyPaymentTasksToday:' + allFailedRecurringTasksToday.size());		
		
		for (Task t : allFailedRecurringTasksToday) 
		{
			String nextAccountManager = tm.getNextSalesTeamMember(FailedRecurringPaymentsTasks.lastAccountManager);
			t.OwnerId = nextAccountManager;
			FailedRecurringPaymentsTasks.lastAccountManager = nextAccountManager;	
		}
		
		update allFailedRecurringTasksToday;
	}
	
	/*
		Method used to create the scheduling of this Batch Job. This is used by the 'TestScheduledJobs' function which enables the scheduled jobs to be 
		setup and removed programmatically (also via API for Jenkins build job)
	*/
	global static void setupSchedule(String jobName, String scheduledTime) { 
		FailedRecurringPaymentsTasks scheduled_task = new FailedRecurringPaymentsTasks();
		String payment_id = System.schedule(jobName, scheduledTime, scheduled_task);
	}
}