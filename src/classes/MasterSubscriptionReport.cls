/**
 * MasterSubscriptionReport 
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
 public class MasterSubscriptionReport {
	
	// table data
	public List<DataRow> table { get; set; }
	
	// date range variables
	public List<Date> dates { get; set; }
	public List<String> dateLabels { get; set; }
	public String interval { get; set; }
	public List<Integer> intervals { get; set; }
	public Integer numberOfIntervals { get; set; }
	
	public Integer startYear { get; set; }
	public Integer startMonth { get; set; }
	public Integer startDay { get; set; }
	
	/**
	 *
	 */
	public MasterSubscriptionReport() {
		this.interval = 'weekly';
		numberOfIntervals = 4;
		
		// load data
		initData();
	}
	
	/**
	 * load and sort data to be displayed on the report
	 */
	public void initData() {
		table = new List<DataRow>();
		
		getDates();
		currentStartSubs();
		newSubs();
		attritionSubs();
		renewEarlySubs();
		renewedSubs();
		renewLateSubs();
		discountedSubs();
		discountAmounts();
		discountMonths();
		currentEndSubs();
	}
	
	/**
	 *
	 */
	public List<SelectOption> getIntervalOptions() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('weekly', 'weekly'));
		options.add(new SelectOption('monthly', 'monthly'));
		options.add(new SelectOption('yearly', 'yearly'));
		return options;
	}
	
	/**
	 * Creates three lists:
	 * one to store the month number 
	 * second to store the starting date for each month
	 * third to store the date labels to display on the report
	 */
	private void getDates() {
		if (this.interval == 'weekly') {
			numberOfIntervals = 4;
			
			Date start = Date.today().addDays(-21);
			this.startYear = start.year();
			this.startMonth = start.month();
			this.startDay = start.day();
		} else if (this.interval == 'monthly') {
			numberOfIntervals = 4;
			
			Date start = Date.today().addMonths(-3);
			this.startYear = start.year();
			this.startMonth = start.month();
			this.startDay = 1;
		} else if (this.interval == 'yearly') {
			numberOfIntervals = 2;
			
			Date start = Date.today().addYears(-1);
			this.startYear = start.year();
			this.startMonth = 1;
			this.startDay = 1;
		}
		Date startDate = Date.newInstance(this.startYear, this.startMonth, this.startDay);
		intervals = new List<Integer>();
		dates = new List<Date>();
		dateLabels = new List<String>();
		
		for (Integer i = 0; i <= numberOfIntervals; i++) {
			if (i != numberOfIntervals) {
				this.intervals.add(i);
			}
			
			if (this.interval == 'weekly') {
				Date d = startDate.toStartOfWeek().addDays(i * 7);
				this.dates.add(d);
				
				d = startDate.toStartOfWeek().addDays((i * 7)+7);
				Datetime dtime = datetime.newInstance(d.year(), d.month(), d.day());
				this.dateLabels.add('we ' + dtime.format('dd MMM'));
			} else if (this.interval == 'monthly') {
				Date d = startDate.addMonths(i);
				this.dates.add(d);
				
				Datetime dtime = datetime.newInstance(d.year(), d.month(), d.day());
				this.dateLabels.add(dtime.format('MMM yyyy'));
			} else if (this.interval == 'yearly') {
				Date d = startDate.addYears(i);
				this.dates.add(d);
				
				Datetime dtime = datetime.newInstance(d.year(), d.month(), d.day());
				this.dateLabels.add(dtime.format('yyyy'));
			}
		}
	}
	
	/**
	 *
	 */
	private DataRow queryByIntervals(String originalQuery, DataRow row) {
		for (Integer i=0; i < this.numberOfIntervals; i++) {
			String query = originalQuery;
	   		try {
	   			// insert the correct dates into the query
	   			query = query.replace('#d1', String.valueOf(dates[i]));
	   			query = query.replace('#d2', String.valueOf(dates[i+1]));
	   			
	   			if (row.queryType == 'count') {
	   				// return the number of rows
	   				Integer result = Database.countQuery(query);
	   				row.values.add(result);
	   			} else if (row.queryType == 'sum') {
	   				SObject result = (SObject)Database.query(query);
	   				if (result.get('Sum') != null) {
	   					row.values.add((Decimal)result.get('Sum'));
	   				} else {
	   					row.values.add(0);
	   				}
	   			}
	   		} catch (Exception e) {
	   			System.debug(e);
	   			row.values.add(0);
	   		}
		}
		return row;
	}
	
	/**
	 *
	 */
	private void currentStartSubs() {
		String query = 'SELECT count() FROM Product_Instance__c WHERE Start_Date__c <= #d1 AND End_Date__c > #d1 AND Subscription_Stream__c = \'Eureka Report\' AND Subscription_Type__c != \'f\' AND Status__c <> \'Cancelled\'';
		DataRow row = new DataRow('# Subs (start of month)', 'count', 'number');
		this.table.add(queryByIntervals(query, row));
	}
	
	/**
	 *
	 */
	private void newSubs() {
		String query = 'SELECT count() FROM Product_Instance__c WHERE Start_Date__c >= #d1 AND Start_Date__c < #d2 AND Is_Renewal__c = false AND Subscription_Stream__c = \'Eureka Report\' AND Subscription_Type__c != \'f\' AND Status__c <> \'Cancelled\'';
		DataRow row = new DataRow('New Subs', 'count', 'number');
		this.table.add(queryByIntervals(query, row));
	}
	
	/**
	 *
	 */
	private void attritionSubs() {
		String query = 'SELECT count() FROM Product_Instance__c WHERE Subscription_Renewal_Date__c = null AND Auto_Renewal__c = false AND End_Date__c >= #d1 AND End_Date__c < #d2 AND Subscription_Stream__c = \'Eureka Report\' AND Subscription_Type__c != \'f\' AND Status__c <> \'Cancelled\'';
		DataRow row = new DataRow('Attrition', 'count', 'number');
		this.table.add(queryByIntervals(query, row));
	}
	
	/**
	 *
	 */
	private void renewEarlySubs() {
		String query = 'SELECT count() FROM Product_Instance__c WHERE Subscription_Renewal_Date__c < #d1 AND End_Date__c >= #d1 AND End_Date__c < #d2 AND Subscription_Stream__c = \'Eureka Report\' AND Subscription_Type__c != \'f\' AND Status__c <> \'Cancelled\'';
		DataRow row = new DataRow('Renew Early', 'count', 'number');
		this.table.add(queryByIntervals(query, row));
	}
	
	/**
	 *
	 */
	private void renewedSubs() {
		//String query = 'SELECT count() FROM Product_Instance__c WHERE Subscription_Renewal_Date__c >= #d1 AND Subscription_Renewal_Date__c < #d2 AND Subscription_Stream__c = \'Eureka Report\' AND Subscription_Type__c != \'f\' AND Status__c <> \'Cancelled\'';
		String query = 'SELECT count() FROM Product_Instance__c WHERE Start_Date__c >= #d1 AND Start_Date__c < #d2 AND Is_Renewal__c = true AND Subscription_Stream__c = \'Eureka Report\' AND Subscription_Type__c != \'f\' AND Status__c <> \'Cancelled\'';
		DataRow row = new DataRow('Renewed', 'count', 'number');
		this.table.add(queryByIntervals(query, row));
	}
	
	/**
	 *
	 */
	private void renewLateSubs() {
		String query = 'SELECT count() FROM Product_Instance__c WHERE Subscription_Renewal_Date__c >= #d2 AND End_Date__c >= #d1 AND End_Date__c < #d2 AND Subscription_Stream__c = \'Eureka Report\' AND Subscription_Type__c != \'f\' AND Status__c <> \'Cancelled\'';
		DataRow row = new DataRow('Renew Late', 'count', 'number');
		this.table.add(queryByIntervals(query, row));
	}
	
	/**
	 *
	 */
	private void discountedSubs() {
		String query = 'SELECT count() FROM Product_Instance__c WHERE Bonus_Months__c != 0 AND Start_Date__c >= #d1 AND Start_Date__c < #d2 AND Subscription_Stream__c = \'Eureka Report\' AND Subscription_Type__c != \'f\' AND Status__c <> \'Cancelled\'';
		DataRow row = new DataRow('Discount # Subs', 'count', 'number');
		this.table.add(queryByIntervals(query, row));
	}
	
	/**
	 *
	 */
	private void discountAmounts() {
		String query = 'SELECT SUM(Discount_Amount__c) Sum FROM Product_Instance__c WHERE Start_Date__c >= #d1 AND Start_Date__c < #d2 AND Subscription_Stream__c = \'Eureka Report\' AND Subscription_Type__c != \'f\' AND Status__c <> \'Cancelled\'';
		DataRow row = new DataRow('Discount $ Given', 'sum', 'currency');
		this.table.add(queryByIntervals(query, row));
		System.debug(table);
	}
	
	/**
	 *
	 */
	private void discountMonths() {
		String query = 'SELECT SUM(Bonus_Months__c) Sum FROM Product_Instance__c WHERE Start_Date__c >= #d1 AND Start_Date__c < #d2 AND Subscription_Stream__c = \'Eureka Report\' AND Subscription_Type__c != \'f\' AND Status__c <> \'Cancelled\'';
		DataRow row = new DataRow('Discount Months Given', 'sum', 'number');
		this.table.add(queryByIntervals(query, row));
	}
	
	/**
	 *
	 */
	private void currentEndSubs() {
		String query = 'SELECT count() FROM Product_Instance__c WHERE Start_Date__c <= #d2 AND End_Date__c > #d2 AND Subscription_Stream__c = \'Eureka Report\' AND Subscription_Type__c != \'f\' AND Status__c <> \'Cancelled\'';
		DataRow row = new DataRow('# Subs (end of month)', 'count', 'number');
		this.table.add(queryByIntervals(query, row));
	}
	
	/***********************************************/
	/**
	 *
	 */
	public class DataRow {
		
		public String label { get; set; }
		public List<Decimal> values { get; set; }
		public String format { get; set; }
		public String queryType { get; set; }
		
		public dataRow(String label, String queryType, String format) {
			this.label = label;
			this.queryType = queryType;
			this.format = format;
			this.values = new List<Decimal>();
		}
	}
}