public class MergeDuplicateAccounts implements Database.Batchable<Account> {
	
	public class MergedAccount {
		public String accountId { get; set; }
		public String email { get; set; }
		public Boolean merged { get; set; }
		public Integer count { get; set; }
		
		public MergedAccount() {
			this.count = 0;
		}
		
		public MergedAccount(String accountId, String email, Integer count, Boolean merged) {
			this.accountId = accountId;
			this.email = email;
			this.merged = merged;
			this.count = count;
		}
	}
	
	public string nameFile { get; set; }
    public Blob contentFile { get; set; }
    public String[] filelines = new String[]{};
    public List<String> ids;
    public List<Account> accounts;
	
	/**
	 *
	 */
	public MergeDuplicateAccounts() {
		
	}
	
	/**
   	 * Parse the csv file into a list of acccounts
   	 */
    public Pagereference ReadFile() {	
    	if (nameFile != null) {
	        nameFile=contentFile.toString();
	        filelines = nameFile.split('\n');
	        ids = new List<String>();
	        accounts = new List<Account>();
	        for (Integer i=0;i<filelines.size();i++) {
	        	
	        	String[] inputvalues = new String[]{};
	            inputvalues = filelines[i].split(',');
	            
	            if (i > 0) {
		            ids.add(inputvalues[0].trim());
	            }
	        }
	        
	        accounts = [SELECT Id, PersonEmail FROM Account WHERE Id IN :ids];
	        
	        ID batchprocessid = Database.executeBatch(this, 1); // execute one at a time
    	}
        return null;
    }
	
	/**
	 * The logic behind merging duplicate accounts
	 */
	public void mergeDuplicates(Account account) {
		AccountHelper helper = new AccountHelper(account);
		helper.mergeDuplicateAccounts();
	}
	
	/**
	 *
	 */
	public List<MergedAccount> getAccountIds() {
		if (ids != null) {
			Map<String, MergedAccount> mergedAccounts = new Map<String, MergedAccount>();
			
			List<Account> existingAccounts = [SELECT Id, PersonEmail FROM Account WHERE Id IN :ids];
			List<String> emails = new List<String>();
			for (Account account : existingAccounts) {
				emails.add(account.PersonEmail);
			}
			
			List<sObject> multipleAccounts = [SELECT PersonEmail, COUNT(Id) NumberOfAccounts FROM Account WHERE PersonEmail IN :emails GROUP BY PersonEmail];
			for (sObject account : multipleAccounts) {
				MergedAccount merged = new MergedAccount();
				merged.count = integer.valueOf(account.get('NumberOfAccounts'));
				mergedAccounts.put(string.valueOf(account.get('PersonEmail')), merged);
			}
			
			for (Account account : existingAccounts) {
				if (mergedAccounts.get(account.PersonEmail) != null && mergedAccounts.get(account.PersonEmail).count > 1) {
					mergedAccounts.put(account.PersonEmail, new MergedAccount(account.Id, account.PersonEmail, mergedAccounts.get(account.PersonEmail).count, true));
				} else if (mergedAccounts.get(account.PersonEmail) != null && mergedAccounts.get(account.PersonEmail).count > 0) {
					mergedAccounts.put(account.PersonEmail, new MergedAccount(account.Id, account.PersonEmail, mergedAccounts.get(account.PersonEmail).count, false));
				} else {
					mergedAccounts.put(account.PersonEmail, new MergedAccount(account.Id, account.PersonEmail, 0, false));
				}
			}
			
			List<MergedAccount> displayAccounts = mergedAccounts.values();
			return displayAccounts;
		} else {
			return null;
		}
	}
	
	/**
	 * The BATCHABLE interface means we can parse a bunch of objects and run any updates on them.
	 */
	public Iterable<Account> start(Database.BatchableContext BC){
		return accounts;
	}
	
	public void execute(Database.BatchableContext BC, Account[] accounts) {
		for (Account account : accounts) {
			mergeDuplicates(account);
		}
	}
	
	public void finish(Database.BatchableContext BC){
		AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
	    	TotalJobItems, CreatedBy.Email
	    	FROM AsyncApexJob WHERE Id =
	    	:BC.getJobId()];
	}
	
	/*************************************************************************/
	
	@isTest 
	static void testUploadCSV() {
		// enable duplicates so that the test account objects can be created
		AIBM_Settings__c defaults = new AIBM_Settings__c(Name='defaults', Allow_Duplicates_Test_Mode__c=true);
		insert defaults;
		
		Account account1 = new Account(FirstName='Test1', LastName='Tester', PersonEmail='test@tester.com');
		insert account1;
		Account account2 = new Account(FirstName='Test2', LastName='Tester', PersonEmail='test@tester.com');
		insert account2;
		
		List<Account> accounts = [SELECT Id FROM Account WHERE PersonEmail = :account1.PersonEmail];
		System.assertEquals(2, accounts.size());
		
		String csv = 'ID\n'+account1.Id+'\n';
		
		MergeDuplicateAccounts controller = new MergeDuplicateAccounts();
		controller.nameFile = '';
		controller.contentFile = Blob.valueOf(csv);
		
		Test.startTest();
		controller.ReadFile();
		Test.stopTest();
		
		// test that 1 account id were retrieved from csv file
		System.assertEquals(1, controller.ids.size());
		System.assertEquals(account1.Id, controller.ids[0]);
		
		accounts = [SELECT Id FROM Account WHERE PersonEmail = :account1.PersonEmail];
		System.assertEquals(1, accounts.size());
	}
	
	@isTest 
	static void testMergeDuplicateAccounts() {
		// enable duplicates so that the test account objects can be created
		AIBM_Settings__c defaults = new AIBM_Settings__c(Name='defaults', Allow_Duplicates_Test_Mode__c=true);
		insert defaults;
		
		Subscription_Stream__c stream = new Subscription_Stream__c(Name='Test Stream');
		insert stream;
		Subscription_Product__c product = new Subscription_Product__c(Name='Test Product', Subscription_Stream__c=stream.Id, Price__c=100);
		insert product;
		
		// set up first account
		Account account = new Account(FirstName='Test1', LastName='Tester', PersonEmail='test@tester.com');
		insert account;
		Product_Instance__c instance = new Product_Instance__c(Subscription_Product__c=product.Id, Person_Account__c=account.Id);
		insert instance;
		
		// set up duplicate of first account
		account = new Account(FirstName='Test2', LastName='Tester', PersonEmail='test@tester.com');
		insert account;
		instance = new Product_Instance__c(Subscription_Product__c=product.Id, Person_Account__c=account.Id);
		insert instance;
		
		List<Account> accounts = [SELECT Id FROM Account WHERE PersonEmail = 'test@tester.com'];
		System.assertEquals(2, accounts.size());
		
		Test.startTest();
		MergeDuplicateAccounts controller = new MergeDuplicateAccounts();
		controller.mergeDuplicates(account);
		Test.stopTest();
		
		// check that the given account was the target object to be merged into
		accounts = [SELECT Id, Name FROM Account WHERE PersonEmail = 'test@tester.com'];
		System.assertEquals(1, accounts.size());
		System.assertEquals('Test2 Tester', accounts[0].Name);
		
		// check that the merged account has all subs attached to it
		List<Product_Instance__c> instances = [SELECT Id FROM Product_Instance__c WHERE Person_Account__c = :accounts[0].Id];
		System.assertEquals(2, instances.size());
	}
}