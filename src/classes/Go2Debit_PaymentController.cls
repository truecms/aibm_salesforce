/**
 * Go2Debit_PaymentController - Controller for payment form. 
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
global with sharing class Go2Debit_PaymentController {
	
	global Account account { get; set; }
	
	// payment details
	global go1.PaymentWrapper payment {
		get;
		set {
			payment = value;
		}
	}
	
	global String accountType {
		get;
		set {
			accountType = value;
			setExistingGateway();
		}
	}
	
	global Go2Debit_PaymentController() {
	}
	
	
	// List of active payment gateways 
	public String paymentGateway {
		get;
		set {
			paymentGateway = value;
			setExistingGateway();
		}
	}
	
	// List of active payment gateways 
	public String paymentAccount {
		get;
		set {
			paymentAccount = value;
			setExistingGateway();
		}
	}
	
	public List<SelectOption> getPaymentGateways() {
		// load active gateways
		List<SelectOption> options = new List<SelectOption>();
		List<go1__Payment_Gateway__c> gateways = go1.Paymethod.loadAllGateways(true);
		//options.add(new SelectOption('', '-Select Payment Method-'));
		for (go1__Payment_Gateway__c gateway : gateways) {
			if (gateway.go1__Payment_Type__c != null) {
				options.add(new SelectOption(gateway.Id, gateway.Name));
			}
		}
		return options;
	}
	
	// @todo - this should return list of SF ID's
	public List<SelectOption> getPaymentAccounts() {
		List<SelectOption> options = new List<SelectOption>();
		if (this.account.Id != null) {
			List<go1__CreditCardAccount__c> creditcards = [SELECT Id, Name, Preferred_Active_Card__c, Expiry_Year__c, Expiry_Month__c FROM go1__CreditCardAccount__c WHERE go1__Account__c = :this.account.Id];
			
			//Get the current month and year to check credit cards have not expired
			Date todayVal = Date.today();
			Integer monthVal = todayVal.Month();
			Integer yearVal = todayVal.Year(); 
			
			for (go1__CreditCardAccount__c value : creditcards) {
				//Check that the credit card has not expired
				if (Integer.valueOf(value.Expiry_Year__c) < yearVal 
						|| (Integer.valueOf(value.Expiry_Year__c) == yearVal && Integer.valueOf(value.Expiry_Month__c) < monthVal))
					System.debug('DEBUG: Credit Card ' + value.Name + ' has expired.');
				else
				{
					String name = value.name;
					if (value.Preferred_Active_Card__c == true) {
						name += ' [Preferred]';
					}
					options.add(new SelectOption(value.id, name));
				}
			}
						
			/*
			List<go1__Bank_Account__c> bankaccounts = [SELECT id, name FROM go1__Bank_Account__c WHERE go1__Account__c = :this.account.Id];
			for (go1__Bank_Account__c value : bankaccounts) {
				options.add(new SelectOption('bank_account:'+value.id, value.name));
			}
			*/
		}
		return options;
	}
	public Boolean getPaymentAccountsEmpty() {
		List<SelectOption> items = getPaymentAccounts();
		return items.isEmpty();
	}
	
	private void setExistingGateway() {
		if (accountType == 'existing') {
			payment.existing_creditcard = payment.getCreditCardById(paymentAccount);
		}
		else {
			payment.existing_creditcard = null;
			go1__Payment_Gateway__c gateway = go1.Paymethod.loadGateway(paymentGateway);
			payment.creditcard.go1__Payment_Gateway__c = gateway.Id;
		}
		System.debug(payment.creditcard);
	}
}