/**
 * OpportunityHelper is a wrapper class for the Opportunity object in Salesforce containing
 * all the Opportunity related functions and field manipulation in one place
 *
 * @author: Ross Tailby
 */
global with sharing class OpportunityHelper {

	public Opportunity opportunity {get; set;}
	public BrightDay_Settings__c settings {get; set;}

	private OpportunityHelper(){
		settings = BrightDay_Settings__c.getInstance();
		opportunity = new Opportunity();
		setDefaultOpeningStage();
	}

	public OpportunityHelper(Id accId){
		this();
		
		opportunity.AccountId = accId;
	}

	public OpportunityHelper(String opportunityName){
		this();
		
		setOpportunityName(null, opportunityName);
	}
	
	public OpportunityHelper(Account acct, String opportunityName){
		this(acct.Id);
				
		setOpportunityName(acct, opportunityName);
	}
	
	public void setOpportunityValues(Id owner, String oppType, Date closeDate, String stage){
		opportunity.OwnerId = owner;
		opportunity.CloseDate = closeDate;
		opportunity.Type = oppType;
		
		if (stage!=null && stage!='')
			opportunity.StageName = stage;	
	}
	
	public void setOpportunityName(Account a, String opportunityName) {
		String name = '';
		if (a != null && a.FirstName != null)
			name+= a.FirstName;
		
		if (a != null && a.LastName != null) {
			if (name.length()>0)
				name+= ' ';
			name+= a.LastName;
		}
		
		if (name.length() > 0)
			opportunity.Name = name + ' : ' + opportunityName;
		else
			opportunity.Name = opportunityName;
	}
	
	public void setDefaultOpeningStage() {
		if (settings == null || settings.Opportunity_Default_Stage__c == null)
			opportunity.StageName = 'Prospecting'; //Default no Stage value - generic to all SF instances
		else
			opportunity.StageName = settings.Opportunity_Default_Stage__c;
	}

}