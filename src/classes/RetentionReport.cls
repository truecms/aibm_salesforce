/**
 * RetentionReport 
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
 public class RetentionReport {
	
	private final Integer NUM_YEARS = 4;
	
	public List<DataYear> table { get; set; }
	public List<DataYear> total { get; set; }
	public List<String> years { get; set; }
	public List<String> dateLabels { get; set; }
	public List<Integer> yearsInt { get; set; }
	
	/**
	 *
	 */
	public RetentionReport() {
		this.table = new List<DataYear>();
		getYears();
		this.dateLabels = getDateLabels();
		
		for (String year : years) {
			buildDataTable(Integer.valueOf(year));
		}
		
		getTotalColumnData();
		getTotalData();
	}
	
	/**
	 *
	 */
	public void buildDataTable(Integer year) {
		Date startDate = Date.newInstance(year, 1, 1);
		Date endStartDate = Date.newInstance(year+1, 1, 1);
		Date endDate = Date.newInstance(Date.today().year(), 1, 1);
		Date endEndDate = Date.newInstance(Date.today().year()+1, 1, 1);
		Integer numberOfYears = Date.today().year()-year;
		
		List<SObject> dueData = getDueData(startDate, endStartDate, endDate, endEndDate, numberOfYears);
		List<SObject> lostData = getLostData(startDate, endStartDate, endDate, endEndDate, numberOfYears);
		
		DataYear column = new DataYear(String.valueOf(year));
		Integer dueNum = 0;
		Integer lostNum = 0;
		List<DataRow> rows = new List<DataRow>();
		for (Integer i=1; i<dateLabels.size(); i++) {
			DataRow data = new DataRow(dateLabels[i]);
			
			if (dueNum < dueData.size() && (Integer)dueData[dueNum].get('end_date') == i) {
				data.due = (Integer)dueData[dueNum].get('due');
				dueNum++;
			}
			if (lostNum < lostData.size() && (Integer)lostData[lostNum].get('end_date') == i) {
				data.lost = (Integer)lostData[lostNum].get('lost');
				lostNum++;
			}
			data.calculatePercent();
			rows.add(data);
			System.debug(data);
		}
		column.setRow(rows);
		this.table.add(column);
	}
	
	/**
	 * subscribers ending in that week
	 */
	public List<SObject> getDueData(Date startDate, Date endStartDate, Date endDate, Date endEndDate, Integer numberOfYears) {
		List<SObject> results = [SELECT WEEK_IN_YEAR(End_Date__c) end_date, COUNT(Id) due FROM Product_Instance__c WHERE Subscription_Product__r.Subscription_Stream__r.Name = 'Eureka Report' AND Subscription_Type__c != 'f' AND Person_Account__r.ER_Reporting_First_Payment__c >= :startDate AND Person_Account__r.ER_Reporting_First_Payment__c < :endStartDate AND End_Date__c > :endDate AND End_Date__c < :endEndDate AND Person_Account__r.Eureka_Status__c >= :numberOfYears GROUP BY WEEK_IN_YEAR(End_Date__c) ORDER BY WEEK_IN_YEAR(End_Date__c) ASC];
		return results;
	}
	
	/**
	 * subscribers ending in that week and not selected to auto renew
	 */
	public List<SObject> getLostData(Date startDate, Date endStartDate, Date endDate, Date endEndDate, Integer numberOfYears) {
		List<SObject> results = [SELECT WEEK_IN_YEAR(End_Date__c) end_date, COUNT(Id) lost FROM Product_Instance__c WHERE Subscription_Product__r.Subscription_Stream__r.Name = 'Eureka Report' AND Subscription_Type__c != 'f' AND Person_Account__r.ER_Reporting_First_Payment__c >= :startDate AND Person_Account__r.ER_Reporting_First_Payment__c < :endStartDate AND End_Date__c > :endDate AND End_Date__c < :endEndDate AND Person_Account__r.Eureka_Status__c >= :numberOfYears AND Auto_Renewal__c = false AND Subscription_Renewal_Date__c = null GROUP BY WEEK_IN_YEAR(End_Date__c) ORDER BY WEEK_IN_YEAR(End_Date__c) ASC];
		return results;
	}
	
	/**
	 * calculates the total for each year
	 */
	public void getTotalData() {
		total = new List<DataYear>();
		
		for (DataYear year : table) {
			DataYear totalYear = new DataYear(year.label);
			System.debug(totalYear);
			DataRow totalRow = new DataRow('YTD');
			System.debug(totalRow);
			
			for (DataRow row : year.rows) {
				totalRow.due += row.due;
				totalRow.lost += row.lost;
			}
			
			totalRow.calculatePercent();
			System.debug(totalRow);
			totalYear.setRow(new List<DataRow>{totalRow});
			total.add(totalYear);
		}
	}
	
	/**
	 *
	 */
	public void getTotalColumnData() {
		DataYear column = new DataYear('Total');
		List<DataRow> rows = new List<DataRow>();
		for (Integer i=0; i<dateLabels.size()-1; i++) {
			DataRow data = new DataRow(dateLabels[i]);
			for (DataYear year : table) {
				data.due += year.rows[i].due;
				data.lost += year.rows[i].lost;
			}
			data.calculatePercent();
			rows.add(data);
		}
		column.setRow(rows);
		this.table.add(column);
		this.yearsInt.add(4);
		this.years.add('Total');
	}
	
	/**
	 *
	 */
	public List<String> getDateLabels() {
		List<String> dateLabels = new List<String>();
		
		Date startOfYear = Date.newInstance(Date.today().year(), 1, 1);
		Date startOfWeek = startOfYear.toStartOfWeek();
		dateLabels.add(startOfWeek.format());
		
		// repeat the following statement for each week in a year
		for (Integer i=0; i<52; i++) {
			// add a week to the current end date
			startOfWeek = startOfWeek.addDays(7);
			// add string label to list
			dateLabels.add(startOfWeek.format());
		}
		return dateLabels;
	}
	
	/**
	 *
	 */
	public void getYears() {
		this.years = new List<String>();
		this.yearsInt = new List<Integer>();
		
		for (Integer i=0; i < NUM_YEARS; i++) {
			this.yearsInt.add(i);
			this.years.add(String.valueOf(Date.today().addYears(i-1 - (NUM_YEARS-1)).year()));
		}
	}
	
	
	/****************************************************************/
	/**
	 * Represents each row in the table for a single year
	 */
	public class DataRow {
		
		public String label { get; set; }
		public Integer due { get; set; }
		public Integer lost { get; set; }
		public Decimal percent { get; set; }
		
		public dataRow(String label) {
			this.label = label;
			this.due = 0;
			this.lost = 0;
			this.percent = 0;
		}
		
		public void calculatePercent() {
			if (this.due != 0) {
				this.percent = Integer.valueOf((Decimal.valueOf(this.lost)/Decimal.valueOf(this.due)) * 100);
			}
		}
	}
	
	/**
	 *
	 */
	public class DataYear {
		
		public String label { get; set; }
		public List<DataRow> rows { get; set; }
		
		public dataYear(String label) {
			List<DataRow> rows = new List<DataRow>();
			this.label = label;
		}
		
		public void setRow(List<DataRow> rows) {
			this.rows = rows;
		}
	}
}