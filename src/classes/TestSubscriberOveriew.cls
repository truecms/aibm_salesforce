/**
 * TestSubscriberOveriew 
 *
 * Unit Testing the subscription based reports originally designed by Go1 (some rework has been done here...) 
 *
 * @author: GO1 Pty Ltd
 * @updated: Ross Tailby
 * @license: http://www.eurekareport.com.au
 */
@isTest
private class TestSubscriberOveriew {
	
	private static Account initialiseDatabase(Integer numSubs) {
		Account account = TestAccountHelper.createAccountOnly();
		
		Subscription_Stream__c stream = new Subscription_Stream__c();
		stream.Name = 'Eureka Report';
		insert(stream);
		
		Subscription_Product__c prod = new Subscription_Product__c();
		prod.Name = 'product'; 
		prod.Subscription_Stream__c = stream.Id;
		prod.Duration__c = 2;
		prod.Duration_units__c = 'monthly';
		insert(prod);
		
		List<Product_Instance__c> insts = new List<Product_Instance__c>();
		for (Integer i = 0; i < numSubs; i++) {
			insts.add(createProductInstance(account, prod));
		}
		insert insts;
		
		return account;
	}
	
	public static Product_Instance__c createProductInstance(Account a, Subscription_Product__c prod) {
		Product_Instance__c sub = new Product_Instance__c();
		sub.Person_Account__c = a.Id;
		sub.Subscription_Product__c = prod.Id;
		sub.Start_Date__c = Date.today();
		sub.End_Date__c = Date.today();
		sub.Subscription_Type__c = 'y';
		return sub;
	}
	
    static testMethod void subscriberOverviewTest() {
    	initialiseDatabase(1);
        SubscriberOverview report = new SubscriberOverview();
    }
    
    /**
     *
     */
    static testMethod void masterSubscriptionTest() {
		initialiseDatabase(1);
        MasterSubscriptionReport report = new MasterSubscriptionReport();
        
        // view monthly period
        report.interval = 'yearly';
        report.initData();
    }
    
    /**
    * Tests for the Current Subscriptions Graphs/Report
    */
    static testMethod void currentSubscriptionTest() {
    	Account a = initialiseDatabase(5);
    	
    	//Adjust the start and end dates of these PIs
    	List<Product_Instance__c> insts = [SELECT Id, Start_Date__c, End_Date__c FROM Product_Instance__c WHERE Person_Account__c = :a.Id];
    	
    	for (Product_Instance__c inst : insts) {
    		inst.Start_Date__c = Date.today().addDays(-30);
    		inst.End_Date__c = Date.today().addDays(30);
    	}
    	update insts;
    	
    	
    	Test.startTest();
    	
        CurrentSubscriptionsGraph report = new CurrentSubscriptionsGraph();
        //Adjust date range to cover above test data
        report.dateRange.Start_Date__c = Date.today().addDays(-7);
        report.dateRange.End_Date__c = Date.today().addDays(7); //3 week entries
        report.adjustDateRange();
        
        System.assertEquals(report.dates.size(), 3);
        for (CurrentSubscriptionsGraph.DataRow dr : report.chart) {
        	System.assertEquals(dr.total, 5);
        	System.assertEquals(dr.value1, 0);
        	System.assertEquals(dr.value2, 5);
        }
        
        //Change one of the Product Instances to be monthly
        insts[0].Subscription_Type__c = 'm';
        update insts;
        
        report.adjustDateRange();
        
        Test.stopTest();
        
        //Check that the correct result statistics are loaded into the graph
        System.assertEquals(report.dates.size(), 3);
        
        for (CurrentSubscriptionsGraph.DataRow dr : report.chart) {
        	System.assertEquals(dr.total, 5);
        	System.assertEquals(dr.value1, 1);
        	System.assertEquals(dr.value2, 4);
        }
    }
    
    static testMethod void currentSubsDateRangeTest() {
    	Test.startTest();
    	CurrentSubscriptionsGraph graph = new CurrentSubscriptionsGraph();
    	graph.dateRange.Start_Date__c = Date.newInstance(2014,7,2);
    	graph.dateRange.End_Date__c = Date.newInstance(2014,8,6); //Should equate to 6 weeks (to interval date after end date)
    	graph.adjustDateRange();
    	List<Date> weeklyDateRange = graph.dates;
    	
    	System.assertEquals(weeklyDateRange.size(), 6);
    	//Check our 6 dates
    	Date d1 = Date.newInstance(2014,7,2);
    	Date d2 = Date.newInstance(2014,7,9);
    	Date d3 = Date.newInstance(2014,7,16);
    	Date d4 = Date.newInstance(2014,7,23);
    	Date d5 = Date.newInstance(2014,7,30);
    	Date d6 = Date.newInstance(2014,8,6);
    	System.assertEquals(weeklyDateRange[0], d1);
    	System.assertEquals(weeklyDateRange[1], d2);
    	System.assertEquals(weeklyDateRange[2], d3);
    	System.assertEquals(weeklyDateRange[3], d4);
    	System.assertEquals(weeklyDateRange[4], d5);
    	System.assertEquals(weeklyDateRange[5], d6);
    	
    	//Switch to monthly
    	graph.selectedGraphList = 'Monthly';
    	graph.dateRange.Start_Date__c = Date.newInstance(2014,6,6);
    	graph.dateRange.End_Date__c = Date.newInstance(2014,8,6); //3 months
    	graph.adjustDateRange();
    	System.assertEquals(graph.dates.size(), 3);
    	
    	//Switch to yearly
    	graph.selectedGraphList = 'Yearly';
    	graph.dateRange.Start_Date__c = Date.newInstance(2013,6,6);
    	graph.dateRange.End_Date__c = Date.newInstance(2014,6,6); //2 years
    	graph.adjustDateRange();
    	System.assertEquals(graph.dates.size(), 2);
    	
    	//Switch to daily and test this range
    	graph.selectedGraphList = 'Daily';
    	graph.dateRange.Start_Date__c = Date.newInstance(2014,8,6);
    	graph.dateRange.End_Date__c = Date.newInstance(2014,8,20); //15 days
    	graph.adjustDateRange();
    	List<Date> dailyDateRange = graph.dates; 
    	
    	Test.stopTest();
    	
    	System.assertEquals(dailyDateRange.size(), 15);
    	
    	//Spot check some dates in the daily date range
    	Date daily3 = Date.newInstance(2014,8,8);
    	Date daily5 = Date.newInstance(2014,8,10);
    	Date daily8 = Date.newInstance(2014,8,13);
    	Date daily10 = Date.newInstance(2014,8,15);
    	Date daily13 = Date.newInstance(2014,8,18);
    	System.assertEquals(dailyDateRange[2], daily3);
    	System.assertEquals(dailyDateRange[4], daily5);
    	System.assertEquals(dailyDateRange[7], daily8);
    	System.assertEquals(dailyDateRange[9], daily10);
    	System.assertEquals(dailyDateRange[12], daily13);
    }
    
    static testMethod void currentSubsInterfaceTest() {
    	Test.startTest();
    	CurrentSubscriptionsGraph graph = new CurrentSubscriptionsGraph();
    	System.assertEquals(graph.selectedGraphList, 'Weekly');
    	
    	graph.selectedGraphList = 'Monthly';
    	
    	Test.stopTest();
    	
    	//Check that the correct granularity options are available
    	System.assertEquals(graph.graphList.size(), 4);
    	System.assertEquals(graph.selectedGraphList, 'Monthly');
    }
    
    static TestMethod void currentSubsTooWideRangeTest() {
    	PageReference pr = new PageReference('/apex/current_subscriptions');
        Test.setCurrentPage(pr);
    	
    	Test.startTest();
    	CurrentSubscriptionsGraph graph = new CurrentSubscriptionsGraph();
    	graph.selectedGraphList = 'Daily';
    	graph.dateRange.Start_Date__c = Date.newInstance(2014,5,6);
    	graph.dateRange.End_Date__c = Date.newInstance(2014,8,20); //too many days!
    	graph.adjustDateRange();
    	
    	Test.stopTest();
    	
    	System.assert(ApexPages.getMessages().size() == 1);
        System.assertEquals(ApexPages.getMessages().get(0).getDetail(), 'This date range to too big for daily graph granularity! Please narrow the date range.');
    }
}