/**
 * UnitTest: 
 *
 * Tests the functionality of the trg_bi_OppsPerUser trigger 
 *
 * Author: tailbyr
 * Copyright: EurekaReport.com.au
 */
@IsTest
public with sharing class TestOppsPerUserBatch {

	/* Utility Methods
	*/
	public static Opportunity createOpportunity(Account a){
		Opportunity o = new Opportunity();
		o.Name = 'Test Opp';
		o.StageName = '30 Day Trial';
		o.Account = a;
		o.CloseDate = Date.Today().addDays(30);
		
		return o;
	}
	
	public static Opps_Per_User__c createOppsPerUser(User u, Integer i, String stage, Integer assignedOps){
		Opps_Per_User__c opu = new Opps_Per_User__c();
		opu.Name = 'Test OPU' + i;
		opu.Assigned_Opps__c = assignedOps;
		opu.Snapshot_Date__c = Date.today().addDays(-1);
		opu.Stage__c = stage;
		opu.User__c = u.Id;
		return opu;
	}

	/* Unit Tests
	*/
	public TestMethod static void testInsertOppsPerUser(){
		
		//Create test data
		Account a = TestAccountHelper.createAccountOnly();	
		Opportunity o = createOpportunity(a);
		
		//Find a suitable user to act as the Opportunity Owner
		User u = [SELECT Id FROM User WHERE IsActive = TRUE AND UserType = 'Standard' AND UserRoleId != '' AND alias NOT IN ('sadm', 'it') ORDER BY alias DESC LIMIT 1];
		
		System.runAs(u){
			insert o;
		}
		
		//Insert some new Opps_Per_User__c instances
		List<Opps_Per_User__c> oppsPerUser = new List<Opps_Per_User__c>();
		
		Opps_Per_User__c opu1 = createOppsPerUser(u, 1, '30 Day Trial', 10);
		oppsPerUser.add(opu1);
		
		Opps_Per_User__c opu2 = createOppsPerUser(u, 2, 'Identify Persona', 5);
		oppsPerUser.add(opu2);
		
		Opps_Per_User__c opu3 = createOppsPerUser(u, 3, 'Needs Analysis', 2);
		oppsPerUser.add(opu3);
		
		Opps_Per_User__c opu4 = createOppsPerUser(u, 4, 'Proposed Product', 1);
		oppsPerUser.add(opu4);
		
		Opps_Per_User__c opu5 = createOppsPerUser(u, 5, 'Closed Lost', 15);
		oppsPerUser.add(opu5);
		
		insert oppsPerUser;
		
		Test.startTest();
		Database.executeBatch(new OppsPerUserBatch(Date.today().addDays(-1)), 5);
		Test.stopTest();
		
		//Check that we have 5 OppsPerUser
		List<Opps_Per_User__c> resultOpps = [SELECT Id FROM Opps_Per_User__c];
		System.assertEquals(resultOpps.size(), 5);
		
		//Check that the New and Progressed Opportunities values have been populated based on the isRUnningTest entries made in the trigger code
		Opps_Per_User__c dayTrial = [SELECT Id, Stage__c, User__c, New_Opportunities__c, Progressed_Opportunities__c, Opps_Not_Changed__c, Assigned_Opps__c
											FROM Opps_Per_User__c WHERE Stage__c = '30 Day Trial' LIMIT 1];
		System.assertEquals(dayTrial.Assigned_Opps__c, 10);
		System.assertEquals(dayTrial.New_Opportunities__c, 2);
		System.assertEquals(dayTrial.Progressed_Opportunities__c, 1);
		
		Opps_Per_User__c idPersona = [SELECT Id, Stage__c, User__c, New_Opportunities__c, Progressed_Opportunities__c, Opps_Not_Changed__c, Assigned_Opps__c
											FROM Opps_Per_User__c WHERE Stage__c = 'Identify Persona' LIMIT 1];
		System.assertEquals(idPersona.Assigned_Opps__c, 5);
		System.assertEquals(idPersona.New_Opportunities__c, 1);
		System.assertEquals(idPersona.Progressed_Opportunities__c, 2);
		
		Opps_Per_User__c needAnalysis = [SELECT Id, Stage__c, User__c, New_Opportunities__c, Progressed_Opportunities__c, Opps_Not_Changed__c, Assigned_Opps__c
											FROM Opps_Per_User__c WHERE Stage__c = 'Needs Analysis' LIMIT 1];
		System.assertEquals(needAnalysis.Assigned_Opps__c, 2);
		System.assertEquals(needAnalysis.New_Opportunities__c, 1);
		System.assertEquals(needAnalysis.Progressed_Opportunities__c, 0);
		
		Opps_Per_User__c proposeProduct = [SELECT Id, Stage__c, User__c, New_Opportunities__c, Progressed_Opportunities__c, Opps_Not_Changed__c, Assigned_Opps__c
											FROM Opps_Per_User__c WHERE Stage__c = 'Proposed Product' LIMIT 1];
		System.assertEquals(proposeProduct.Assigned_Opps__c, 1);
		System.assertEquals(proposeProduct.New_Opportunities__c, 0);
		System.assertEquals(proposeProduct.Progressed_Opportunities__c, 0);
		
		Opps_Per_User__c closedLost = [SELECT Id, Stage__c, User__c, New_Opportunities__c, Progressed_Opportunities__c, Opps_Not_Changed__c, Assigned_Opps__c
											FROM Opps_Per_User__c WHERE Stage__c = 'Closed Lost' LIMIT 1];
		System.assertEquals(closedLost.Assigned_Opps__c, 15);
		System.assertEquals(closedLost.New_Opportunities__c, 1);
		System.assertEquals(closedLost.Progressed_Opportunities__c, 0);
	}

}