@IsTest
global class TestRefreshBrightdayData implements HTTPCalloutMock {

	Boolean ack {get; set;} //ack
	String httpStatus {get; set;} //e.g. OK
	Integer httpStatusCode {get; set;} //e.g. 200

	TestRefreshBrightdayData(Boolean ack, String httpStatus, Integer code){
		this.ack = ack;
		this.httpStatus = httpStatus;
		this.httpStatusCode = code;
	}

	global HttpResponse respond(HttpRequest req) {
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
    	res.setStatus(this.httpStatus);
    	res.setStatusCode(this.httpStatusCode);
    	
    	//The final response object which will be serialized and sent back as the repsonse body
    	Map<String, Object> httpResponse = new Map<String, Object>();
    	
    	//Testing the callout method ack functionality
    	httpResponse.put('ack', this.ack);
    	res.setBody(JSON.serialize(httpResponse));
    	
    	return res;
	}
	
	public static void populateCustomSettings(){
		//Populate the Org Defaults for the Custom Setting required
		TestLatePaperworkTasks.createBrightDaySettings();
	}
	
	public static TestMethod void testSuccessfulRefresh() {
		//Populate the custom setting required to trigger the callout
		populateCustomSettings();
		
		//Insert new Account and ensure there is no value in Last Refresh Datetime
		Account a = TestAccountHelper.createAccountOnly(1);
		
		Account confirmAcc = [SELECT Id, Last_Refresh_Datetime__c FROM Account WHERE Id = :a.Id];
		System.assertEquals(confirmAcc.Last_Refresh_Datetime__c, null);
		
		Test.startTest();
		
		//Callout to refresh the account
		Test.setMock(HTTPCalloutMock.class, new TestRefreshBrightdayData(true, 'OK', 200));
		ApexPages.Standardcontroller sc = new ApexPages.StandardController(a);
		RefreshBrightDayData rbdd = new RefreshBrightDayData(sc);
		rbdd.calloutDataRefresh();
		
		Test.stopTest();
		
		//Check that a true ack was returned and the Last Refresh Datetime is updated
		System.assert(rbdd.ack);
		Account refreshedAcc = [SELECT Id, Last_Refresh_Datetime__c FROM Account WHERE Id = :a.Id];
		System.assertEquals(refreshedAcc.Last_Refresh_Datetime__c.date(), Date.today());
	}
	
	public static TestMethod void testFailedRefresh() {
		//Populate the custom setting required to trigger the callout
		populateCustomSettings();
		
		//Insert new Account and ensure there is no value in Last Refresh Datetime
		Account a = TestAccountHelper.createAccountOnly(1);
		
		Account confirmAcc = [SELECT Id, Last_Refresh_Datetime__c FROM Account WHERE Id = :a.Id];
		System.assertEquals(confirmAcc.Last_Refresh_Datetime__c, null);
		
		Test.startTest();
		
		//Callout simulating a failed acknowledgement
		Test.setMock(HTTPCalloutMock.class, new TestRefreshBrightdayData(false, 'Not Found', 404));
		ApexPages.Standardcontroller sc = new ApexPages.StandardController(a);
		RefreshBrightDayData rbdd = new RefreshBrightDayData(sc);
		rbdd.calloutDataRefresh();
		
		Test.stopTest();
		
		//Check that a false ack was returned and the Last Refresh Datetime is not updated
		Account refreshedAcc = [SELECT Id, Last_Refresh_Datetime__c FROM Account WHERE Id = :a.Id];
		System.assert(!rbdd.ack);
		System.assertEquals(refreshedAcc.Last_Refresh_Datetime__c, null);
	}

}