/**
 * TestNewBrightdaySub 
 *
 *
 * @author: Ross.Tailby
 * @license: http://www.eurekareport.com.au
 */
 @isTest
public with sharing class TestNewBrightdaySub {

	/**
	* Utility Methods
	*/
	public static AIBM_Settings__c createSettings(Subscription_Product__c brightdayRenewal, Campaign renewalCampaign) {
		AIBM_Settings__c settings = new AIBM_Settings__c();
		settings.Name = 'Default';
		
		settings.Brightday_Renewal_Product__c = brightdayRenewal.Id;
		settings.Brightday_Renewal_Campaign__c = renewalCampaign.Id;
		
		upsert settings;
		return settings;
	}
	
	public static Subscription_Stream__c createStream(){
		Subscription_Stream__c stream = new Subscription_Stream__c(Name='Eureka Report');
		insert stream;
		return stream;
	} 

	public static Subscription_Product__c createSub(Subscription_Stream__c stream, String subType)
	{
		Subscription_Product__c product = new Subscription_Product__c();
	  	product.Subscription_Stream__c = stream.Id;
	  	
	  	if (subType == 'month') {
	  		product.Duration_units__c = 'month';
	  		product.Subscription_Type__c = 'm';
	  		product.Duration__c = 12;
	  	}
	  	else {
	  		product.Duration_units__c = 'year';
	  		product.Subscription_Type__c = 'y';
	  		product.Duration__c = 1;
	  	}
	  	product.Price__c = 0;
	  	insert product;
	  	return product;
	}
	
	public static Subscription_Product__c createRenewalProduct(Subscription_Stream__c stream){
		Subscription_Product__c renewal = new Subscription_Product__c();
		renewal.Subscription_Stream__c = stream.Id;
		renewal.Price__c = 0.00;
		renewal.Subscription_Type__c = 'h';
		renewal.Duration_units__c = 'year';
		renewal.Duration__c = 30;
		insert renewal;
		
		return renewal;
	}
	
	public static Campaign createCampaign(){
		Campaign campaign = new Campaign(Name='Test Campaign',StartDate=Date.today().addDays(-7),EndDate=Date.today().addDays(7),IsActive=true);
		insert campaign;
		
		return campaign;
	}
	
	public static void setupRenewalProduct(Subscription_Stream__c stream){
		Subscription_Product__c renew = createRenewalProduct(stream);
		Campaign renewCam = createCampaign();
		
		createSettings(renew, renewCam);
	}

	
	/**
	* Test Methods
	*/
	public static TestMethod void testPremiumClientYearlyAutoRenew() {
		User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
		
		TestProductInstanceHelper.createAllocationSettings();
		
		System.runAs(drupalUser){
			//Create renewal subscription product
			Subscription_Stream__c stream = createStream();
			setupRenewalProduct(stream);
			
			//Create 3 Accounts (Premium Client, Platinum and Black) and current product instance ending today, autorenewal
			List<Account> accs = TestAccountHelper.createAccounts(3);
			accs[0].BD_Current_Role__c = 'Premium Client';
			accs[1].BD_Current_Role__c = 'Platinum Client';
			accs[2].BD_Current_Role__c = 'Black';
			update accs;
			
			Subscription_Product__c currentSub = createSub(stream, 'year');
			List<Product_Instance__c> instances = new List<Product_Instance__c>();
			
			for (Account a : accs) {
				Product_Instance__c instance = new Product_Instance__c();
		  		instance.Subscription_Product__c = currentSub.Id;
		  		instance.Person_Account__c = a.Id;
		  		instance.Auto_Renewal__c = true;
		  		instances.add(instance);
			}
	  		insert instances;
	  		
	  		for (Product_Instance__c pi : instances) {
	  			pi.Start_Date__c = date.today().addDays(-365);
	  			pi.End_Date__c = date.today();
	  		}
	  		update instances;
			
			//Before we run the AutoRenew process, check what type and status our Accounts are in
			List<Account> preResults = [SELECT Id, PersonEmail, BD_Current_Role__c, ER_Reporting_Sub_Type__c, ER_Reporting_Sub_Status__c, ER_Reporting_Sub_End_Date__c
										FROM Account];
			for (Account a : preResults) {
				System.assertEquals(a.ER_Reporting_Sub_Type__c, 'y');
				System.assertEquals(a.ER_Reporting_Sub_Status__c, '4');
			}
			
			//Run auto renew process
			Test.startTest();
			
			SchedulableContext ctx;
			AutoRenewalSubscription ars = new AutoRenewalSubscription();
			ars.execute(ctx);
			
			Test.stopTest();
		}
		
		//Test that each Account has a new Heritage sub, type of 'h', status of 5 and end date of 30 years
		List<Account> results = [SELECT Id, PersonEmail, BD_Current_Role__c, ER_Reporting_Sub_Type__c, ER_Reporting_Sub_Status__c, ER_Reporting_Sub_End_Date__c
								FROM Account];
		
		System.assertEquals(results.size(), 3);
		for (Account a : results) {
			System.assertEquals(a.ER_Reporting_Sub_Type__c, 'h');
			System.assertEquals(a.ER_Reporting_Sub_Status__c, '5');
			
			//@TODO: test ER Reporting Sub End Date as well
		}
	}
	
	public static TestMethod void testPremiumClientMonthlyRenew() {
		//Monthlies should also renew!
		User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
		
		TestProductInstanceHelper.createAllocationSettings();
		
		Account account = null;
		
		System.runAs(drupalUser){
			//Create renewal subscription product
			Subscription_Stream__c stream = createStream();
			setupRenewalProduct(stream);
			
			//Create an Account
			List<Account> accounts = TestAccountHelper.createAccounts(1);
			account = accounts[0];
			account.BD_Current_Role__c = 'Premium Client';
			update account;
			
			Subscription_Product__c currentSub = createSub(stream, 'month');
			
			Product_Instance__c instance = new Product_Instance__c();
		  	instance.Subscription_Product__c = currentSub.Id;
		  	instance.Person_Account__c = account.Id;
		  	instance.Auto_Renewal__c = true;
	  		insert instance;
	  		
	  		instance.Start_Date__c = date.today().addDays(-365);
	  		instance.End_Date__c = date.today();
	  		update instance;
			
			//Before we run the AutoRenew process, check what type and status our Accounts are in
			List<Account> preResults = [SELECT Id, PersonEmail, BD_Current_Role__c, ER_Reporting_Sub_Type__c, ER_Reporting_Sub_Status__c, ER_Reporting_Sub_End_Date__c
										FROM Account];
			for (Account a : preResults) {
				System.assertEquals(a.ER_Reporting_Sub_Type__c, 'm');
				System.assertEquals(a.ER_Reporting_Sub_Status__c, '4');
			}
			
			//Run auto renew process
			Test.startTest();
			
			SchedulableContext ctx;
			AutoRenewalSubscription ars = new AutoRenewalSubscription();
			ars.execute(ctx);
			
			Test.stopTest();
		}
		
		//Test that each Account has a new Heritage sub, type of 'h', status of 5 and end date of 30 years
		List<Account> results = [SELECT Id, PersonEmail, BD_Current_Role__c, ER_Reporting_Sub_Type__c, ER_Reporting_Sub_Status__c, ER_Reporting_Sub_End_Date__c
								FROM Account];
		
		System.assertEquals(results.size(), 1);
		for (Account a : results) {
			System.assertEquals(a.ER_Reporting_Sub_Type__c, 'h');
			System.assertEquals(a.ER_Reporting_Sub_Status__c, '5');
		}
		
		//Check that the existing Product Instance has been marked as EXPIRED
		List<Product_Instance__c> instances = [SELECT Id, Status__c, Subscription_Type__c FROM Product_Instance__c WHERE Person_Account__c = :account.Id];
		System.assertEquals(instances.size(), 2);
		
		for (Product_Instance__c pi : instances) {
			if (pi.Subscription_Type__c == 'h')
				System.assertEquals(pi.Status__c, '4');
			else if (pi.Subscription_Type__c == 'm')
				System.assertEquals(pi.Status__c, '8');
			else
				System.assert(false); //There shouldn't be any Product Instances of any other type against this Account
		}
	}
	
	public static TestMethod void testMemberAutoRenew() {
		//brightday member statuses should not renew - although will hit the existing autorenew process
		User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
		
		TestProductInstanceHelper.createAllocationSettings();
		
		Account account = null;
		
		System.runAs(drupalUser){
			//Create renewal subscription product
			Subscription_Stream__c stream = createStream();
			setupRenewalProduct(stream);
			
			//Create a Premium Member Account
			List<Account> accounts = TestAccountHelper.createAccounts(1);
			account = accounts[0];
			account.BD_Current_Role__c = 'Premium Member';
			update account;
			
			Subscription_Product__c currentSub = createSub(stream, 'year');
			
			//Add the default renewal product to the AIBM Settings
			AIBM_Settings__c settings = [SELECT Id, Default_Renewal_Product__c FROM AIBM_Settings__c WHERE Name = 'Default' LIMIT 1];
			settings.Default_Renewal_Product__c = currentSub.Id;
			update settings;
			
			Product_Instance__c instance = new Product_Instance__c();
		  	instance.Subscription_Product__c = currentSub.Id;
		  	instance.Person_Account__c = account.Id;
		  	instance.Auto_Renewal__c = true;
	  		insert instance;
	  		
	  		instance.Start_Date__c = date.today().addDays(-365);
	  		instance.End_Date__c = date.today();
	  		update instance;
			
			//Before we run the AutoRenew process, check what type and status our Accounts are in
			List<Account> preResults = [SELECT Id, PersonEmail, BD_Current_Role__c, ER_Reporting_Sub_Type__c, ER_Reporting_Sub_Status__c, ER_Reporting_Sub_End_Date__c
										FROM Account];
			for (Account a : preResults) {
				System.assertEquals(a.ER_Reporting_Sub_Type__c, 'y');
				System.assertEquals(a.ER_Reporting_Sub_Status__c, '4');
			}
			
			//Run auto renew process
			Test.startTest();
			
			SchedulableContext ctx;
			AutoRenewalSubscription ars = new AutoRenewalSubscription();
			ars.execute(ctx);
			
			Test.stopTest();
		}
		
		//Test that the Accounts do not have heritage subs
		List<Account> results = [SELECT Id, PersonEmail, BD_Current_Role__c, ER_Reporting_Sub_Type__c, ER_Reporting_Sub_Status__c, ER_Reporting_Sub_End_Date__c
								FROM Account];
		
		System.assertEquals(results.size(), 1);
		for (Account a : results) {
			System.assertEquals(a.ER_Reporting_Sub_Type__c, 'y');
			System.assertEquals(a.ER_Reporting_Sub_Status__c, '4');
		}
		
		//Check that the existing Product Instance has been marked as EXPIRED (no new product instance as no credit card against Account)
		List<Product_Instance__c> instances = [SELECT Id, Status__c, Subscription_Type__c FROM Product_Instance__c WHERE Person_Account__c = :account.Id];
		System.assertEquals(instances.size(), 1);
		
		System.assertEquals(instances[0].Subscription_Type__c, 'y');
		System.assertEquals(instances[0].Status__c, '8');
	}
	
	
	
	
	
	
	
	public static TestMethod void testPremiumClientAlreadyRenewed() {
		//Scenario where the account already has a future sub purchased
		
		//brightday member statuses should not renew - although will hit the existing autorenew process
		User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
		
		TestProductInstanceHelper.createAllocationSettings();
		
		Account account = null;
		
		System.runAs(drupalUser){
			//Create renewal subscription product
			Subscription_Stream__c stream = createStream();
			setupRenewalProduct(stream);
			
			//Create a Premium Member Account
			List<Account> accounts = TestAccountHelper.createAccounts(1);
			account = accounts[0];
			account.BD_Current_Role__c = 'Premium Client';
			update account;
			
			Subscription_Product__c currentSub = createSub(stream, 'year');
			
			//Add the default renewal product to the AIBM Settings
			AIBM_Settings__c settings = [SELECT Id, Default_Renewal_Product__c FROM AIBM_Settings__c WHERE Name = 'Default' LIMIT 1];
			settings.Default_Renewal_Product__c = currentSub.Id;
			update settings;
			
			//Add two Product Instances, one just about to expire and a future one!
			List<Product_Instance__c> instances = new List<Product_Instance__c>();
			
			Product_Instance__c instance1 = new Product_Instance__c();
		  	instance1.Subscription_Product__c = currentSub.Id;
		  	instance1.Person_Account__c = account.Id;
		  	instance1.Auto_Renewal__c = true;
		  	instances.add(instance1);
		  	
		  	Product_Instance__c instance2 = new Product_Instance__c();
		  	instance2.Subscription_Product__c = currentSub.Id;
		  	instance2.Person_Account__c = account.Id;
		  	instance2.Auto_Renewal__c = true;
		  	instances.add(instance2);
		  	
	  		insert instances;
	  		
	  		instances[0].Start_Date__c = date.today().addDays(-365);
	  		instances[0].End_Date__c = date.today();
	  		instances[1].Start_Date__c = date.today().addDays(1);
	  		instances[1].End_Date__c = date.today().addDays(366);
	  		
	  		update instances;
			
			//Before we run the AutoRenew process, check what type and status our Accounts are in
			List<Account> preResults = [SELECT Id, PersonEmail, BD_Current_Role__c, ER_Reporting_Sub_Type__c, ER_Reporting_Sub_Status__c, ER_Reporting_Sub_End_Date__c
										FROM Account];
			for (Account a : preResults) {
				System.assertEquals(a.ER_Reporting_Sub_Type__c, 'y');
				System.assertEquals(a.ER_Reporting_Sub_Status__c, '4');
			}
			
			//Run auto renew process
			Test.startTest();
			
			SchedulableContext ctx;
			AutoRenewalSubscription ars = new AutoRenewalSubscription();
			ars.execute(ctx);
			
			Test.stopTest();
		}
		
		//Test that the Accounts do not have heritage subs
		List<Account> results = [SELECT Id, PersonEmail, BD_Current_Role__c, ER_Reporting_Sub_Type__c, ER_Reporting_Sub_Status__c, ER_Reporting_Sub_End_Date__c
								FROM Account];
		
		System.assertEquals(results.size(), 1);
		System.assertEquals(results[0].ER_Reporting_Sub_Type__c, 'y');
		System.assertEquals(results[0].ER_Reporting_Sub_Status__c, '4');
		
		//Check that there are only 2 product instances still against the account and they are both yearly sub types
		List<Product_Instance__c> instances = [SELECT Id, Status__c, Subscription_Type__c FROM Product_Instance__c WHERE Person_Account__c = :account.Id];
		System.assertEquals(instances.size(), 2);
		
		for (Product_Instance__c pi : instances) {
			System.assertEquals(pi.Subscription_Type__c, 'y');
		}
	}
	
	public static TestMethod void testNoRenewalProductAvailable() {
		//No renewal product specified in settings, therefore error and complete
		User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
		
		TestProductInstanceHelper.createAllocationSettings();
		
		Account account = null;
		
		System.runAs(drupalUser){
			//Create the stream but DO NOT create the renewal product - force the error			
			Subscription_Stream__c stream = createStream();
			
			//Create an Account
			List<Account> accounts = TestAccountHelper.createAccounts(1);
			account = accounts[0];
			account.BD_Current_Role__c = 'Premium Client';
			update account;
			
			Subscription_Product__c currentSub = createSub(stream, 'month');
			
			Product_Instance__c instance = new Product_Instance__c();
		  	instance.Subscription_Product__c = currentSub.Id;
		  	instance.Person_Account__c = account.Id;
		  	instance.Auto_Renewal__c = true;
		  	instance.Status__c = '4';
	  		insert instance;
	  		
	  		instance.Start_Date__c = date.today().addDays(-365);
	  		instance.End_Date__c = date.today();
	  		update instance;
			
			//Run auto renew process
			Test.startTest();
			
			SchedulableContext ctx;
			AutoRenewalSubscription ars = new AutoRenewalSubscription();
			ars.execute(ctx);
			
			Test.stopTest();
		}
		
		//Test that the account has not been changed at all
		List<Account> results = [SELECT Id, PersonEmail, BD_Current_Role__c, ER_Reporting_Sub_Type__c, ER_Reporting_Sub_Status__c, ER_Reporting_Sub_End_Date__c
								FROM Account];
		
		System.assertEquals(results.size(), 1);
		System.assertEquals(results[0].ER_Reporting_Sub_Type__c, 'm');
		System.assertEquals(results[0].ER_Reporting_Sub_Status__c, '4');
		
		//Check that the existing Product Instance has not ben changed either!
		List<Product_Instance__c> instances = [SELECT Id, Status__c, Subscription_Type__c FROM Product_Instance__c WHERE Person_Account__c = :account.Id];
		System.assertEquals(instances.size(), 1);
		System.assertEquals(instances[0].Subscription_Type__c, 'm');
		System.assertEquals(instances[0].Status__c, '4');
	}

}