/**
 * SubscriberOverview 
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
 public class SubscriberOverview {
	
	// table data
	public List<DataRow> currentSubsTable { get; set; }
	public List<DataRow> newSubsTable { get; set; }
	public List<DataRow> dueSubsTable { get; set; }
	public List<DataRow> renewalSubsTable { get; set; }
	public List<DataRow> toRenewSubsTable { get; set; }
	public List<DataRow> lapsedSubsTable { get; set; }
	
	// chart data
	public List<DataRow> kpi { get; set; }
	private Integer yearlyRenewed = 0;
	private Integer yearlyDue = 0;
	private Integer totalLapsed = 0;
	private Integer totalDue = 0;
	
	// class variables
	private Date startOfMonth = Date.today().toStartOfMonth();
	private Date endOfMonth = Date.today().toStartOfMonth().addMonths(1);
	
	private String optionalStartDate { get; set; }
	private String optionalEndDate { get; set; }
	public Product_Instance__c sub { get; set; }
	
	/**
	 *
	 */
	public SubscriberOverview() {
		// functionality to change the default start and end dates
		optionalStartDate = ApexPages.currentPage().getParameters().get('startdate');
		optionalEndDate = ApexPages.currentPage().getParameters().get('enddate');
		
		if (optionalStartDate != null && optionalStartDate != '') {
			startOfMonth = Date.parse(optionalStartDate);
		}
		if (optionalEndDate != null && optionalEndDate != '') {
			endOfMonth = Date.parse(optionalEndDate);
		}
		
		sub = new Product_Instance__c(Start_Date__c=startOfMonth,End_Date__c=endOfMonth);
		currentSubsTable = new List<DataRow>();
		newSubsTable = new List<DataRow>();
		dueSubsTable = new List<DataRow>();
		renewalSubsTable = new List<DataRow>();
		toRenewSubsTable = new List<DataRow>();
		lapsedSubsTable = new List<DataRow>();
		
		// calculate all current subscriptions this month
		currentSubs();
		// calculate new subscriptions
		newSubs();
		// calculate ending subscriptions
		endingSubs();
		// calculate renewal subscriptions
		renewalSubs();
		// calculate to be renewed subscriptions
		toRenewSubs();
		// calculate lapsed subscriptions
		lapsedSubs();
		
		// collect the chart data
		kpi = new List<DataRow>();
		kpiData();
	}
	
	public PageReference filterDateRange() {
		PageReference page = new PageReference('/apex/subscriber_overview?startdate='+sub.Start_Date__c.format()+'&enddate='+sub.End_Date__c.format());
		page.setRedirect(true);
		return page;
	}
	
	/**
	 *
	 */
	private void currentSubs() {
		Integer monthStartList = [SELECT count() FROM Product_Instance__c WHERE Start_Date__c <= :this.startOfMonth AND End_Date__c > :this.startOfMonth AND Subscription_Type__c != 'f' AND Status__c != 'Cancelled' AND Subscription_Product__r.Subscription_Stream__r.Name = 'Eureka Report'];
		Integer currentSubList = [SELECT count() FROM Product_Instance__c WHERE Start_Date__c <= :Date.today() AND End_Date__c > :Date.today() AND Subscription_Type__c != 'f' AND Status__c != 'Cancelled' AND Subscription_Product__r.Subscription_Stream__r.Name = 'Eureka Report'];
		
		this.currentSubsTable.add(new DataRow('Month Start', monthStartList));
		this.currentSubsTable.add(new DataRow('Current Subscribers', currentSubList));
		this.currentSubsTable.add(new DataRow('MTD Difference', currentSubList - monthStartList));
	}
	
	/**
	 *
	 */
	private void newSubs() {
		Integer yearlySubList = [SELECT count() FROM Product_Instance__c WHERE Start_Date__c >= :this.startOfMonth AND Start_Date__c <= :Date.today() AND Subscription_Type__c = 'y' AND Is_Renewal__c = false AND Status__c != 'Cancelled' AND Subscription_Product__r.Subscription_Stream__r.Name = 'Eureka Report'];
		Integer monthlySubList = [SELECT count() FROM Product_Instance__c WHERE Start_Date__c >= :this.startOfMonth AND Start_Date__c <= :Date.today() AND Subscription_Type__c = 'm' AND Is_Renewal__c = false AND Status__c != 'Cancelled' AND Subscription_Product__r.Subscription_Stream__r.Name = 'Eureka Report'];
		
		this.newSubsTable.add(new DataRow('New Subscribers Total', yearlySubList + monthlySubList));
		this.newSubsTable.add(new DataRow('New Yearly Subscribers', yearlySubList));
		this.newSubsTable.add(new DataRow('New Monthly Subscribers', monthlySubList));
	}
	
	/**
	 *
	 */
	private void endingSubs() {
		Integer yearlySubList = [SELECT count() FROM Product_Instance__c WHERE End_Date__c >= :this.startOfMonth AND End_Date__c < :this.endOfMonth AND Auto_Renewal__c = false AND Subscription_Renewal_Date__c = null AND Subscription_Type__c = 'y' AND Status__c != 'Cancelled' AND Subscription_Product__r.Subscription_Stream__r.Name = 'Eureka Report'];
		Integer monthlySubList = [SELECT count() FROM Product_Instance__c WHERE End_Date__c >= :this.startOfMonth AND End_Date__c < :this.endOfMonth AND Auto_Renewal__c = false AND Subscription_Renewal_Date__c = null AND Subscription_Type__c = 'm' AND Status__c != 'Cancelled' AND Subscription_Product__r.Subscription_Stream__r.Name = 'Eureka Report'];
		
		// store for chart data
		yearlyDue = yearlySubList;
		totalDue = yearlySubList + monthlySubList;
		
		this.dueSubsTable.add(new DataRow('Total Due This Month', totalDue));
		this.dueSubsTable.add(new DataRow('Yearly Due This Month', yearlySubList));
		this.dueSubsTable.add(new DataRow('Monthly Due This Month', monthlySubList));
	}
	
	/**
	 *
	 */
	private void renewalSubs() {
		Integer yearlySubList = [SELECT count() FROM Product_Instance__c WHERE Start_Date__c >= :this.startOfMonth AND Start_Date__c <= :Date.today() AND Is_Renewal__c = true AND Subscription_Type__c = 'y' AND Status__c != 'Cancelled' AND Subscription_Product__r.Subscription_Stream__r.Name = 'Eureka Report'];
		Integer monthlySubList = [SELECT count() FROM Product_Instance__c WHERE Start_Date__c >= :this.startOfMonth AND Start_Date__c <= :Date.today() AND Is_Renewal__c = true AND Subscription_Type__c = 'm' AND Status__c != 'Cancelled' AND Subscription_Product__r.Subscription_Stream__r.Name = 'Eureka Report'];
		
		// store for chart data
		yearlyRenewed = yearlySubList;
		
		this.renewalSubsTable.add(new DataRow('Total Renewals', yearlySubList + monthlySubList));
		this.renewalSubsTable.add(new DataRow('Yearly Renewed', yearlySubList));
		this.renewalSubsTable.add(new DataRow('Monthly Renewed', monthlySubList));
	}
	
	/**
	 *
	 */
	private void toRenewSubs() {
		Integer yearlySubList = [SELECT count() FROM Product_Instance__c WHERE End_Date__c < :this.endOfMonth AND End_Date__c > :Date.today() AND Auto_Renewal__c = true AND Subscription_Type__c = 'y' AND Status__c != 'Cancelled' AND Subscription_Product__r.Subscription_Stream__r.Name = 'Eureka Report'];
		Integer monthlySubList = [SELECT count() FROM Product_Instance__c WHERE End_Date__c < :this.endOfMonth AND End_Date__c > :Date.today() AND Auto_Renewal__c = true AND Subscription_Type__c = 'm' AND Status__c != 'Cancelled' AND Subscription_Product__r.Subscription_Stream__r.Name = 'Eureka Report'];
		
		this.toRenewSubsTable.add(new DataRow('Total Still To Renew', yearlySubList + monthlySubList));
		this.toRenewSubsTable.add(new DataRow('Yearly Still To Renew', yearlySubList));
		this.toRenewSubsTable.add(new DataRow('Monthly Still To Renew', monthlySubList));
	}
	
	/**
	 *
	 */
	private void lapsedSubs() {
		Integer yearlySubList = [SELECT count() FROM Product_Instance__c WHERE End_Date__c >= :this.startOfMonth AND End_Date__c < :Date.today() AND Subscription_Type__c = 'y' AND Auto_Renewal__c = false AND Status__c != 'Cancelled' AND Subscription_Product__r.Subscription_Stream__r.Name = 'Eureka Report'];
		Integer monthlySubList = [SELECT count() FROM Product_Instance__c WHERE End_Date__c >= :this.startOfMonth AND End_Date__c < :Date.today() AND Subscription_Type__c = 'm' AND Auto_Renewal__c = false AND Status__c != 'Cancelled' AND Subscription_Product__r.Subscription_Stream__r.Name = 'Eureka Report'];
		
		totalLapsed = yearlySubList + monthlySubList;
		
		this.lapsedSubsTable.add(new DataRow('Total Lapsed', totalLapsed));
		this.lapsedSubsTable.add(new DataRow('Yearly Lapsed', yearlySubList));
		this.lapsedSubsTable.add(new DataRow('Monthly Lapsed', monthlySubList));
	}
	
	/**
	 *
	 */
	private void kpiData() {
		Decimal yearlyRenewals = 0;
		if (this.yearlyDue != 0) {
			yearlyRenewals = Decimal.valueOf(this.yearlyRenewed)/Decimal.valueOf(this.yearlyDue);
		}
		this.kpi.add(new DataRow('Yearly Renewals', yearlyRenewals));
		
		Decimal retention = 0;
		if (this.totalDue != 0) {
			retention = Decimal.valueOf(this.totalLapsed)/Decimal.valueOf(this.totalDue);
		}
		this.kpi.add(new DataRow('Retention', retention));
	}
	
	/*********************************************************************************************/
	/**
	 *
	 */
	public class DataRow {
		
		public String label { get; set; }
		public Integer metric { get; set; }
		public Decimal percentage { get; set; }
		
		public dataRow(String label, Decimal metric) {
			this.label = label;
			this.metric = Integer.valueOf(metric);
			this.percentage = metric * 100;
		}
	}
}