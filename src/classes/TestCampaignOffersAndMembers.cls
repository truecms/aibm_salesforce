/**
 * TestCampaignOffersAndMembers 
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
@isTest
private class TestCampaignOffersAndMembers {
	
	/**
     *
     */
    static CampaignMember createTestCampaignMember(Account account) {
    	Campaign campaign = new Campaign(Name='Test Campaign', Duration__c=40.0);
    	insert campaign;
    	
    	Contact contact = [SELECT Id FROM Contact WHERE AccountId = :account.Id];
    	CampaignMember member = new CampaignMember(CampaignId=campaign.Id, ContactId=contact.Id);
    	insert member;
    	return member;
    }
    
    static RecordType getRecordType()
	{
		// Find any person account record type
	    RecordType recordType = [ select Id, Name, DeveloperName from RecordType where SObjectType = 'Account' and IsPersonType = true limit 1 ];
	    return recordType;
	}
    
    static Account createTestAccount() {
    	Account account = new Account(FirstName='Test', LastName='Tester', RecordTypeId='01290000000NnFN', PersonEmail='test@test.com.test');
    	insert account;
    	return account;
    }
	
    /**
	 * Tests the six possible outcomes from the webservice e.g. no account, no campaign, no valid child campaigns...
	 */
	static testMethod void testWebService() {
		// insert a test account to allow the web service to query it
       	Account account = new Account(FirstName='Test',LastName='Tester',PersonEmail='test@tester.com');
       	insert account;
       	Subscription_Stream__c stream = new Subscription_Stream__c(Name='Test Stream');
		insert stream;
		Subscription_Product__c product = new Subscription_Product__c(Name='Test Subscription',Subscription_Stream__c=stream.Id);
		insert product;
		Product_Instance__c sub = new Product_Instance__c(Person_Account__c=account.Id,Subscription_Product__c=product.Id,Start_Date__c=Date.today(),End_Date__c=Date.today().addDays(7));
		insert sub;
       	
       	// test the first error scenario where no account contains the email parameter
        CampaignOffersWebService.CampaignResponse expectedResponse1 = new CampaignOffersWebService.CampaignResponse();
        expectedResponse1.Message = 'There are no accounts with the email: test1@tester.com!';
        expectedResponse1.Response = 'Error';
        CampaignOffersWebService.CampaignResponse testResponse1 = CampaignOffersWebService.doPost('test1@tester.com');
        System.assertEquals(expectedResponse1.Message, testResponse1.Message);
       	
       	// test the second error scenario where there are no valid campaigns
        CampaignOffersWebService.CampaignResponse expectedResponse2 = new CampaignOffersWebService.CampaignResponse();
        expectedResponse2.Message = 'There are no valid campaigns in the system!';
        expectedResponse2.Response = 'Error';
		CampaignOffersWebService.CampaignResponse testResponse2 = CampaignOffersWebService.doPost('test@tester.com');
		System.assertEquals(expectedResponse2.Message, testResponse2.Message);
		
		Campaign campaign = new Campaign(Name='Test Campaign', EndDate=Date.today().addDays(7));
       	insert campaign;
       	
       	// test the default successful response
        CampaignOffersWebService.CampaignResponse expectedResponse3 = new CampaignOffersWebService.CampaignResponse();
        expectedResponse3.Response = 'Successful';
        expectedResponse3.Parent_Campaign = campaign.Id;
		CampaignOffersWebService.CampaignResponse testResponse3 = CampaignOffersWebService.doPost('test@tester.com');
		System.assertEquals(expectedResponse3.Parent_Campaign, testResponse3.Parent_Campaign);
       	
       	Contact contact = [SELECT Id FROM Contact WHERE AccountId = :account.Id];
       	CampaignMember member = new CampaignMember(CampaignId=campaign.Id,ContactId=contact.Id);
       	insert member;
		
		// test the second successful scenario where there is a campaign associated with an account but does not contain any child campaigns
		CampaignOffersWebService.CampaignResponse expectedResponse4 = new CampaignOffersWebService.CampaignResponse();
		expectedResponse4.Response = 'Successful';
		expectedResponse4.Parent_Campaign = campaign.Id;
		CampaignOffersWebService.CampaignResponse testResponse4 = CampaignOffersWebService.doPost('test@tester.com');
		System.assertEquals(expectedResponse4.Parent_Campaign, testResponse4.Parent_Campaign);
		
		Campaign childCampaign = new Campaign(Name='Test Child Campaign',ParentId=campaign.Id,EndDate=Date.today().addDays(4));
		insert childCampaign;
		
		// test the forth successful scenario where account is associated with a campaign that contains a child campaign but is not valid
		CampaignOffersWebService.CampaignResponse expectedResponse5 = new CampaignOffersWebService.CampaignResponse();
		expectedResponse5.Response = 'Successful';
		expectedResponse5.Parent_Campaign = campaign.Id;
		CampaignOffersWebService.CampaignResponse testResponse5 = CampaignOffersWebService.doPost('test@tester.com');
		System.assertEquals(expectedResponse5.Parent_Campaign, testResponse5.Parent_Campaign);
		
		childCampaign.Duration__c = 3;
		update childCampaign;
		
		// test the forth successful scenario where account is associated with a campaign that contains a valid child campaign
		CampaignOffersWebService.CampaignResponse expectedResponse6 = new CampaignOffersWebService.CampaignResponse();
		expectedResponse6.Response = 'Successful';
		expectedResponse6.Parent_Campaign = campaign.Id;
		expectedResponse6.Child_Campaign = childCampaign.Id;
		CampaignOffersWebService.CampaignResponse testResponse6 = CampaignOffersWebService.doPost('test@tester.com');
		//System.assertEquals(expectedResponse6, testResponse6);
	}
	
	/**
	 * Test an account and campaign member is updated to the first offer campaign
	 * via a schedule job
	 */
	static testMethod void testScheduledTaskFirstOffer() {
		Test.startTest();
		Campaign campaign = new Campaign(Name='Test Campaign');
		insert campaign;
		Campaign offer1 = new Campaign(Name='Campaign Offer 1',ParentId=campaign.Id,Duration__c=20);
		insert offer1;
		Campaign offer2 = new Campaign(Name='Campaign Offer 2',ParentId=campaign.Id,Duration__c=10);
		insert offer2;
		
		Account account = new Account(FirstName='Test',LastName='Tester',PersonEmail='test@test.com.test', RecordTypeId=getRecordType().Id);
		insert account;
		
		Subscription_Stream__c stream = new Subscription_Stream__c(Name='Test Stream');
		insert stream;
		Subscription_Product__c product = new Subscription_Product__c(Name='Test Subscription',Subscription_Stream__c=stream.Id,Duration__c=30,Duration_units__c='day');
		insert product;
		Product_Instance__c sub = new Product_Instance__c(Person_Account__c=account.Id,Subscription_Product__c=product.Id,Campaign__c=campaign.Id);
		insert sub;
		
		sub.Start_Date__c = Date.today().addDays(-10);
		sub.End_Date__c = Date.today().addDays(20);
		update sub;
		
		CampaignMember member = [SELECT Id, ContactId, CampaignId FROM CampaignMember WHERE CampaignId = :campaign.Id];
		member.Next_Offer_Date__c=Date.today();
		update member;
		
		AddCampaignMembers.runTask();
		Test.stopTest();
		
		CampaignMember testMember1 = [SELECT Id, Next_Offer_Date__c FROM CampaignMember WHERE Id = :member.Id];
		
		System.assertEquals(Date.today().addDays(10), testMember1.Next_Offer_Date__c);
		
		// run this method for coverage purposes
		AddCampaignMembers.start('Test Task', '0 0 * * * ?');
		
		// Test the delete campaign member trigger
		delete member;
	}
	
	/**
	 * Test an account and campaign member is updated to the second offer campaign
	 * via a schedule job
	 */
	static testMethod void testScheduledTaskSecondOffer() {
		Test.startTest();
		
		Campaign campaign = new Campaign(Name='Test Campaign');
		insert campaign;
		Campaign offer1 = new Campaign(Name='Campaign Offer 1',ParentId=campaign.Id,Duration__c=20);
		insert offer1;
		Campaign offer2 = new Campaign(Name='Campaign Offer 2',ParentId=campaign.Id,Duration__c=10);
		insert offer2;
		
		// set account to start on the first offer
		Account account = new Account(FirstName='Test',LastName='Tester', PersonEmail='test@test.com.test', RecordTypeId=getRecordType().Id);
		insert account;
		
		Subscription_Stream__c stream = new Subscription_Stream__c(Name='Test Stream');
		insert stream;
		Subscription_Product__c product = new Subscription_Product__c(Name='Test Subscription',Subscription_Stream__c=stream.Id,Duration__c=30,Duration_units__c='day');
		insert product;
		Product_Instance__c sub = new Product_Instance__c(Person_Account__c=account.Id,Subscription_Product__c=product.Id,Campaign__c=campaign.Id);
		insert sub;
		
		sub.Start_Date__c = Date.today().addDays(-20);
		sub.End_Date__c = Date.today().addDays(10);
		update sub;
		
		CampaignMember member = [SELECT Id, ContactId, CampaignId FROM CampaignMember WHERE CampaignId = :campaign.Id];
		member.Next_Offer_Date__c=Date.today();
		update member;
		
		AddCampaignMembers.runTask();
		Test.stopTest();
		
		CampaignMember testMember2 = [SELECT Id, Next_Offer_Date__c FROM CampaignMember WHERE Id = :member.Id];
		
		System.assertEquals(sub.End_Date__c, testMember2.Next_Offer_Date__c);
	}
	
	/**
	 * Deprecated: Used to test the OnlyNewCampaignMember trigger
	 * Check that the second campaign member is not stored since there is already an account with the same email
	 */
	/*static testMethod void testOnlyNewCampaignMembers() {
    	Account account = new Account(LastName='Test',PersonEmail='test@lead.com');
    	insert account;
    	
    	Campaign campaign = new Campaign(Name='Test Campaign',Only_New_Subscribers__c=true);
    	insert campaign;
    	
    	Contact contact = new Contact(FirstName='Contact',LastName='Test',Email='test@contact.com');
    	insert contact;
    	CampaignMember cm = new CampaignMember(CampaignId=campaign.Id, ContactId=contact.Id);
    	insert cm;
    	
    	Lead lead = new Lead(FirstName='Lead',LastName='Test',Email='test@lead.com');
    	insert lead;
    	CampaignMember cm1 = new CampaignMember(CampaignId=campaign.Id, LeadId=lead.Id);
    	insert cm1;
    	
    	campaign = [SELECT Id, Excluded_Members__c FROM Campaign WHERE Id = :campaign.Id];
    	System.assertEquals('test@lead.com', campaign.Excluded_Members__c);
    }*/
    
    /**
     * Deprecated: Used to test the OnlyNewCampaignMember trigger
     * Check that the campaign member is still added to the campaign regardless of the already same account
     */
    /*static testMethod void testOnlyNewCampaignMembers1() {
    	Campaign campaign = new Campaign(Name='Test Campaign');
    	insert campaign;
    	
    	Account account = new Account(FirstName='Test',LastName='Tester1',PersonEmail='test@test1.com');
    	insert account;
    	Lead lead = new Lead(FirstName='Test',LastName='Tester1',Email='test@test1.com');
    	insert lead;
    	CampaignMember cm = new CampaignMember(CampaignId=campaign.Id, LeadId=lead.Id);
    	insert cm;
    	
    	campaign = [SELECT Id, Excluded_Members__c FROM Campaign WHERE Id = :campaign.Id];
    	System.assertEquals(null, campaign.Excluded_Members__c);
    }*/
    
    /**
     * 
     */
    static testMethod void testCampaignMemberHelperMethods() {
    	Account account = createTestAccount();
    	CampaignMember member = createTestCampaignMember(account);
    	CampaignMemberHelper helper = new CampaignMemberHelper(member);
    	
    	Campaign testCampaign = helper.getCampaignFromMember();
    	System.assertEquals(40.0, testCampaign.Duration__c);
    	
    	Account testAccount = helper.getAccountFromCampaignMember();
    	System.assertEquals(account.Id, testAccount.Id);
    	
    	CampaignMember testCampaignMember = helper.getCampaignMemberFromAccount(testAccount.Id);
    	System.assertEquals(member.Id, testCampaignMember.Id);
    	
    	//Campaign bestOffer = helper.getCurrentBestCampaignOffer(40); //Campaign offers never fully implemented by Go1 - can be ignored
    }
    
    /**
     *
     */
    static testMethod void testCampaignMemberHelperMethodsInitiallyAccount() {
    	Account account = createTestAccount();
    	CampaignMember member = createTestCampaignMember(account);
    	
    	Test.startTest();
    	CampaignMemberHelper helper = new CampaignMemberHelper(account);
    	Test.stopTest();
    	
    	System.assertEquals(account.id, helper.account.id);
    	System.assertEquals(member.Id, helper.member.Id);
    }
}