/**
 * Testing the NoCreditCardBatch class
 * Author: tailbyr
 * Copyright: eurekareport.com.au
 */
@isTest
private class TestNoCreditCardBatch 
{
	static Subscription_Product__c createSubs()
	{
		//Create subscription stream
    	Subscription_Stream__c stream = new Subscription_Stream__c(Name='Eureka Report');
	  	insert stream;
	  	Subscription_Product__c product = new Subscription_Product__c();
	  	product.Subscription_Stream__c = stream.Id;
	  	product.Duration__c = 1;
	  	product.Duration_units__c = 'month';
	  	product.Price__c = 100;
	  	insert product;
	  	return product;
	}
	
	static RecordType getRecordType()
	{
		// Find any person account record type
	    RecordType recordType = [ select Id, Name, DeveloperName from RecordType where SObjectType = 'Account' and IsPersonType = true limit 1 ];
	    return recordType;
	}
	
	static List<Account> createAccounts(Integer numAccounts, RecordType recordType) 
	{
		//Create the accounts for test
    	List<Account> accs = new List<Account>(); 
    	List<Id> accIds = new List<Id>();
    	
    	for (integer i = 0; i < numAccounts; i++) {
    		Account account = new Account(FirstName='Test' +i, LastName='Tester' +i, PersonEmail='test'+i+'@test.com.test');
	  		account.RecordTypeId = recordType.Id;
	  		account.Sub_Status__c = '4';  // paid subscription
	  		accs.add(account);
    	}
    	insert accs;
    	return accs;
	}


	// Check that the No credit card against account task has been created for that scenario, and no other tasks!  
    static testMethod void testNoCreditCardTaskCreated() 
    {
     	//Set up the custom settings for queue to use and whether to utilise assignment rules or not
		TestProductInstanceHelper.createAllocationSettings();
    	
     	Subscription_Product__c product = createSubs();
     	RecordType recordType = getRecordType();
     
     	//Create just 1 account
    	List<Account> accountList = createAccounts(1, recordType);
    	Account account = accountList[0];
    	
    	
    	//Create Product Instance 
    	Product_Instance__c instance = new Product_Instance__c();
	  	instance.Person_Account__c = account.Id;
	  	instance.Subscription_Product__c = product.Id;
	  	instance.Subscription_Type__c = 'y';
	  	instance.Amount__c = 100;
	  	instance.Auto_Renewal__c = true;
	  	instance.End_Date__c = Date.today().addDays(7);
    	
    	insert instance;    	
	  		
	  	//Update the start and end dates of the PI so it will be picked up by the expiry process
	  	List<Product_Instance__c> insts = [SELECT Id, Start_Date__c, End_Date__c FROM Product_Instance__c WHERE Person_Account__c = :account.Id];
	  	for (Product_Instance__c inst : insts) {
	  		inst.Start_Date__c = Date.today().addDays(-358);
	  		inst.End_Date__c = Date.today().addDays(7);
	  	}
	  	update insts;
        	
        
        Test.startTest();
        //Call the NoCreditCardsBatch to initiate the process
        Database.executeBatch(new NoCreditCardBatch(), 1);
        Test.stopTest();
        
        //Check we have a task
        List<Task> noCreditCardTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
				where Subject = :TaskManager.NO_CREDIT_CARD_SUBJECT 
				AND createdDate = TODAY
				ORDER BY createdDate desc];
		System.assertEquals(noCreditCardTasks.size(), 1);
		System.assertEquals(noCreditCardTasks[0].WhatId, instance.Id);
		
		//Check we don't have any other tasks!
		List<Task> anyOtherTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
				where Subject != :TaskManager.NO_CREDIT_CARD_SUBJECT 
				AND createdDate = TODAY
				ORDER BY createdDate desc];
		System.assertEquals(anyOtherTasks.size(), 0);
    }
    
    
    // Create a number of the noCreditCard scenarios and check that all tasks are shared out equally across Account Managers
    static testMethod void testNoCreditCardTaskCreatedFairly()
    {
    	//Set up the custom settings for queue to use and whether to utilise assignment rules or not
		TestProductInstanceHelper.createAllocationSettings();
		
    	Subscription_Product__c product = createSubs();
     	RecordType recordType = getRecordType();
     
     	//Create 6 test accounts
    	List<Account> accountList = createAccounts(6, recordType);
    	List<Product_Instance__c> instances = new List<Product_Instance__c>();
    	
    	for (Account acc : accountList)
    	{
    		Product_Instance__c instance = new Product_Instance__c();
	  		instance.Person_Account__c = acc.Id;
	  		instance.Subscription_Product__c = product.Id;
	  		instance.Subscription_Type__c = 'y';
	  		instance.Amount__c = 100;
	  		instance.Auto_Renewal__c = true;
	  		instance.End_Date__c = Date.today().addDays(7);
	  		instances.add(instance);
    	}
    	insert instances;
    
    	Test.startTest();
    	Database.executeBatch(new NoCreditCardBatch(), instances.size());
    	Test.stopTest();
    	
    	//Check we have the right number of tasks
        List<Task> noCreditCardTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
				where Subject = :TaskManager.NO_CREDIT_CARD_SUBJECT 
				AND createdDate = TODAY
				ORDER BY createdDate desc];
		System.assertEquals(noCreditCardTasks.size(), instances.size());
		
		//Check that each account manager has the same number of these tasks
		checkAllocationOfTasks(noCreditCardTasks, instances.size());
    }
    
    
    // Create a number of the noCreditCard scenarios and check that if a task is initially allocated to a given acc mgr, new ones are allocated to the same person
    @IsTest(seealldata=true)
    static void testNoCreditCardTaskSameAccMgr()
    {
    	//Pick the first Account Manager for setting all the task ownership
    	GroupMember aMember = [SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = 'AccountManagers' ORDER BY UserOrGroupId LIMIT 1];
    	
    	Subscription_Product__c product = createSubs();
     	RecordType recordType = getRecordType();
     
     	//Create 6 test accounts
    	List<Account> accountList = createAccounts(6, recordType);
    	List<Product_Instance__c> instances = new List<Product_Instance__c>();
    	List<Task> tasks = new List<Task>();
    	
    	List<Id> accIds = new List<Id>();
    	
    	for (Account acc : accountList)
    	{
    		accIds.add(acc.Id);
    		
    		//Create a task against the account for a given acc mgr
    		Task newTask = new Task();
    		newTask.WhatId = acc.Id;
			newTask.ActivityDate = Date.today();
			newTask.Subject = 'Test task';
			newTask.OwnerId = aMember.UserOrGroupId;
			tasks.add(newTask);
    		
    		Product_Instance__c instance = new Product_Instance__c();
	  		instance.Person_Account__c = acc.Id;
	  		instance.Subscription_Product__c = product.Id;
	  		instance.Subscription_Type__c = 'y';
	  		instance.Amount__c = 100;
	  		instance.Auto_Renewal__c = true;
	  		instance.End_Date__c = Date.today().addDays(7);
	  		instance.status__c = 'test product instance';
	  		instances.add(instance);
    	}
    	insert instances;
    	insert tasks;
    	
    	//Create a TaskManager instance with the same user group
    	TaskManager taskMgr = new TaskManager('AccountManagers');
    	
    	//Also set the allocation rules so they are used
    	Boolean allocationRules = true;
    	    	
    	//Get the PersonContactIds from the Accounts
    	List<Id> personAccs = new List<Id>();
    	List<Account> personAccDetails = [SELECT Id, PersonContactId FROM Account WHERE Id IN :accIds];
    	for (Account pc : personAccDetails){
    		personAccs.add(pc.PersonContactId);
    	}
    	
    	Test.startTest();
    	
    	//Do not call the batch execution as it fails due to the problem that Unit Tests treat all Task Owners as Inactive!
		List<Product_Instance__c> piList = [SELECT Id, End_Date__c, Person_Account__c FROM Product_Instance__c WHERE Auto_Renewal__c = true
				  AND End_Date__c = :Date.today().addDays(7)
				  AND Status__c = 'test product instance'];
		
    	ProductInstanceHelper pih = new ProductInstanceHelper(piList);
    	pih.createNoCreditCardTasks(taskMgr, allocationRules);
    	
    	Test.stopTest();
    	
    	//Check we have the right number of tasks
        List<Task> noCreditCardTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
				where Subject = :TaskManager.NO_CREDIT_CARD_SUBJECT 
				AND createdDate = TODAY
				AND WhoId IN :personAccs
				ORDER BY createdDate desc];
		System.assertEquals(noCreditCardTasks.size(), instances.size());
		
		//Check each task created belongs to aMember
		for (Task t : noCreditCardTasks)
		{
			System.assertEquals(t.ownerId, aMember.UserOrGroupId);
		}
    }
    
    
    
    // Check that the No credit card against a monthly sub account task has been created for that scenario, and no other tasks!  
    static testMethod void testNoCreditCardMonthlyTaskCreated() 
    {
     	//Set up the custom settings for queue to use and whether to utilise assignment rules or not
		TestProductInstanceHelper.createAllocationSettings();
		    	
    	Subscription_Product__c product = createSubs();
     	RecordType recordType = getRecordType();
     
     	//Create just 1 account
    	List<Account> accountList = createAccounts(1, recordType);
    	Account account = accountList[0];
    	
    	
    	//Create Recurring and Product Instance
    	go1__Recurring_Instance__c recurringInst = new go1__Recurring_Instance__c();
    	recurringInst.go1__Account__c = account.Id;
    	recurringInst.go1__remaining_attempts__c = 11;
    	recurringInst.go1__next_attempt__c = Date.today().addDays(7);
    	recurringInst.go1__next_renewal__c = Date.today().addDays(7);
    	recurringInst.go1__payment_account_type__c = 'Credit Card';
    	
    	insert recurringInst;
    	 
    	Product_Instance__c instance = new Product_Instance__c();
	  	instance.Person_Account__c = account.Id;
	  	instance.Subscription_Product__c = product.Id;
	  	instance.Subscription_Type__c = 'm';
	  	instance.Amount__c = 100;
	  	instance.End_Date__c = Date.today().addDays(365);
	  	instance.Recurring_Instance__c = recurringInst.Id;
    	
    	insert instance;
        	
        
        Test.startTest();
        //Call the NoCreditCardsBatch to initiate the process
        Database.executeBatch(new NoCreditCardBatch(), 1);
        Test.stopTest();
        
        //Check we have a task
        List<Task> noCreditCardTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
				where Subject = :TaskManager.NO_CREDIT_CARD_SUBJECT_MONTHLY 
				AND createdDate = TODAY
				ORDER BY createdDate desc];
		System.assertEquals(noCreditCardTasks.size(), 1);
		System.assertEquals(noCreditCardTasks[0].WhatId, instance.Id);
		
		//Check we don't have any other tasks!
		List<Task> anyOtherTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
				where Subject != :TaskManager.NO_CREDIT_CARD_SUBJECT_MONTHLY 
				AND createdDate = TODAY
				ORDER BY createdDate desc];
		System.assertEquals(anyOtherTasks.size(), 0);
    }
    
    
    
    static void checkAllocationOfTasks(List<Task> noCreditCardTasks, Integer noOfProductInstances)
    {
    	List<GroupMember> aMembers = [SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = 'AccountManagers' ORDER BY UserOrGroupId];
    	
    	//Ensure that these tasks were allocated equally
		if (noCreditCardTasks!=null && noCreditCardTasks.size()>0){
			Map<Id, Integer> tasksPerManager = new Map<Id, Integer>();
			for (Task t : noCreditCardTasks) {
					
				//Count tasks per Account Manager
				if (tasksPerManager.containsKey(t.ownerId)){
					Integer count = tasksPerManager.get(t.ownerId);
					count++;
					tasksPerManager.put(t.ownerId, count);
				}
				else {
					tasksPerManager.put(t.ownerId, 1);
				}
			}
			
			//Work out how many tasks each Account Manager must have (not counting remainder)
			Integer noOfTasks = Math.floor(noOfProductInstances / aMembers.size()).intValue();
			List<Integer> counts = tasksPerManager.values();
			for (Integer val : counts){
				if (val == noOfTasks || val == noOfTasks+1) //+1 to account for the remainder where noOfTasks%noOfAccountManagers != 0
					System.assert(true);
				else
					System.assert(false);
			}
		}
		else
			System.assert(false);
    }
    
}