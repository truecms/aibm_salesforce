/**
 * DuplicateSubsReport
 *
 * Created to find issues where people have paid or been given multiple subs when they only should have one
 *
 * @author: Ross Tailby
 * @license: http://www.eurekareport.com.au
 */
global with sharing class DuplicateSubsReport implements Schedulable
{
	public List<DataRow> table {get; set;}
	
	global DuplicateSubsReport()
	{
		table = new List<DataRow>();
		initData();
	}
	
	public void initData()
	{
		List<AggregateResult> aggRes = [SELECT count(createdDate) numCreated, day_only(convertTimezone(createdDate)) createdonDate, Person_Account__c, Person_Account__r.PersonEmail PersonEmail, Person_Account__r.Name PersonName, Campaign__c, Campaign__r.Name CampaignName, Subscription_Product__c, Subscription_Product__r.Name ProductName, status__c 
										FROM Product_Instance__c 
										WHERE (Subscription_Type__c = 'y' or Subscription_Type__c = 'm')
										AND day_only(convertTimezone(createdDate)) > 2013-12-01
										AND CreatedBy.Name = 'System Admin'
										AND Status__c != 'Cancelled' AND Status__c != '8'
										GROUP BY Person_Account__c, Person_Account__r.PersonEmail, Person_Account__r.Name, Campaign__r.Name, Subscription_Product__r.Name, day_only(convertTimezone(createdDate)), Status__c, Subscription_Product__c, Campaign__c
										HAVING COUNT(createdDate) > 1
										ORDER BY day_only(convertTimezone(createdDate))];
										
										
		for (AggregateResult ar : aggRes)
		{
			DataRow dr = new DataRow((Id)ar.get('Person_Account__c'),
									 (String)ar.get('PersonEmail'),
									 (String)ar.get('PersonName'),
									 (Id)ar.get('Campaign__c'),
									 (String)ar.get('CampaignName'),
									 (Id)ar.get('Subscription_Product__c'),
									 (String)ar.get('ProductName'),
									 (Integer)ar.get('numCreated'),
									 (Date)ar.get('createdonDate'));
									 
			table.add(dr);
		}
	}
	
	global void execute(SchedulableContext sc)
	{
		DuplicateSubsReport dsr = new DuplicateSubsReport();
		
		try {
			boolean html = true; 
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
            //String[] toAddresses = new String[] {a.CreatedBy.Email}; 
            String[] toAddresses = new String[] {'ross.tailby@eurekareport.com.au'}; //TEST USE ONLY
            mail.setToAddresses(toAddresses); 
            mail.setSubject('Duplicate Subs Report for ' + Datetime.now().format('dd/MM/yyyy')); 
            
            string s = resultsToHtml(dsr.table); 
            mail.setHtmlBody(s); 
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		}
		catch (Exception e) {
			System.debug(e);
		}
	}
	
	private String resultsToHtml(List<DataRow> table)
	{
		String rtn = 'Number of Accounts in SF Production with duplicated subscriptions:' + table.size();
		rtn += '<br /><br /><table border="1px"><tbody><tr style="background:gray; color:white; font-weight:bold;><th width="17%">Created Date</th><th width="17%">Name</th><th width="17%">Email</th><th width="17%">Campaign</th><th width="17%">Product</th><th width="15%">Count</th></tr>';
		Integer i = 0;
		for (DataRow dr : table)
		{
			if (math.mod(i,2)==0)
                rtn+='<tr style="background:LightGrey;"><td>';
            else
                rtn+='<tr style="background:DarkGrey;"><td>';
            	rtn+=String.valueOf(dr.createdDate)
                +'</td><td>'+String.valueOf(dr.customerName)
                +'</td><td>'+String.valueOf(dr.email)
                +'</td><td>'+String.valueOf(dr.campaign)
                +'</td><td>'+String.valueOf(dr.product)
                +'</td><td>'+String.valueOf(dr.subCount)
                +'</td></tr>';
            i++;
		}
		rtn+='<tr></tr></tbody></table>'; 
        return rtn;
	}
	
	/*
		Method used to create the scheduling of this Batch Job. This is used by the 'TestScheduledJobs' function which enables the scheduled jobs to be 
		setup and removed programmatically (also via API for Jenkins build job)
	*/
	global static void setupSchedule(String jobName, String scheduledTime) { 
		DuplicateSubsReport scheduled_task = new DuplicateSubsReport();
		String payment_id = System.schedule(jobName, scheduledTime, scheduled_task);
	}
	
	
	//******* Custom Objects
	public class DataRow
	{
		public Id accountId {get; set;}
		public String email {get; set;}
		public String customerName {get; set;}
		public String campaign {get; set;}
		public Id campaignId {get; set;}
		public String product {get; set;}
		public Id productId {get; set;}
		public Integer subCount {get; set;}
		public Date createdDate {get; set;}
		
		public DataRow(Id accountId, String email, String customerName, Id campaignId, String campaign, Id productId, String product, Integer subCount, Date createdDate)
		{
			this.accountId = accountId; 
			this.email = email;
			this.customerName = customerName; 
			this.campaignId = campaignId;
			this.campaign = campaign;
			this.productId = productId;
			this.product = product; 
			this.subCount = subCount;
			this.createdDate = createdDate;
		}
		
		public DataRow(Id accountId)
		{
			this.accountId = accountId;
		}
	}
	
	
	/*
	* Unit Tests
	*/
	public TestMethod static void testDuplicateSubReport()
	{
		Profile p = [select id from profile where name='System Administrator']; 
		User u = new User(alias = 'admins', email='system.admin@eurekareport.com.au.unittest', 
            				emailencodingkey='UTF-8', lastname='Admin', firstname='System', languagelocalekey='en_US', 
            				localesidkey='en_US', profileid = p.Id, 
            				timezonesidkey='America/Los_Angeles', username='system.admin@eurekareport.com.au.unittest');
		
		System.runAs(u) {
			//Create Account
			Account account = new Account(FirstName='Test', LastName='User', PersonEmail='test.user@test.com.test');
			insert account;
			
			//Create Two product instances that are the same
			Campaign campaign = new Campaign(Name='Test Campaign');
	  		insert campaign;
	  		Subscription_Stream__c stream = new Subscription_Stream__c(Name='Eureka Report');
	  		insert stream;
	  		Subscription_Product__c product = new Subscription_Product__c();
	  		product.Subscription_Stream__c = stream.Id;
	  		product.Duration__c = 1;
	  		product.Duration_units__c = 'month';
	  		product.Price__c = 100;
	  		insert product;
	  		
	  		List<Product_Instance__c> instances = new List<Product_Instance__c>();
	  		for (Integer i = 0; i < 2; i++)
	  		{
		  		Product_Instance__c instance = new Product_Instance__c();
		  		instance.Person_Account__c = account.Id;
		  		instance.Subscription_Product__c = product.Id;
		  		instance.Subscription_Type__c = 'y';
		  		instance.Amount__c = 100;
		  		instance.Auto_Renewal__c = true;
		  		instance.Campaign__c = campaign.Id;
		  		instances.add(instance);
	  		}
	  		insert instances;
		}
  				
		//Run the report
		Test.startTest();
		DuplicateSubsReport dsr = new DuplicateSubsReport();
		Test.stopTest();
		
		//Test one record output
		System.assertEquals(dsr.table.size(), 1);
	}
	
}