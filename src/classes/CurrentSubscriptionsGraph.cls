/**
 * CurrentSubscriptionsGraph
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
public class CurrentSubscriptionsGraph {
	
	//
	public List<DataRow> chart { get; set; }
	public List<String> dateLabels { get; set; }
	public List<Date> dates { get; set; }
	
	public List<SelectOption> graphList {
		get{
			graphList = new List<SelectOption>();
			graphList.add(new SelectOption('Daily', 'Daily'));
			graphList.add(new SelectOption('Weekly', 'Weekly'));
			graphList.add(new SelectOption('Monthly', 'Monthly'));
			graphList.add(new SelectOption('Yearly', 'Yearly'));
			return graphList;
		} 
		set;	
	}
	
	//Hold the selection of the above graphList
	public String selectedGraphList 
	{ 
		get {
			if (selectedGraphList==null || selectedGraphList=='') {
				selectedGraphList = 'Weekly';
			}
			return selectedGraphList;
		}
		set; 
	}
	
	public Integer numberOfIntervals = 1;
	
	//Set the from and to dates for defining the range of the graphing report
	public Product_Instance__c dateRange { get; set; }
	
	/**
	 *
	 */
	public CurrentSubscriptionsGraph() {
		chart = new List<DataRow>();
		selectedGraphList = '';
		
		//Initialise the from and to dates
		dateRange = new Product_Instance__c();
		
		//Set the initial Start and End dates for the graphs
		dateRange.End_Date__c = Date.today().toStartOfWeek();
		dateRange.Start_Date__c = Date.today().toStartOfWeek().addDays((numberOfIntervals-1) * -7); 
		
		// load data
		adjustDateRange();
		queryByDateRangeIntervals();
	}
	
	/**
	* Get the selected dates and adjust the graph to use them
	*/
	public void adjustDateRange() {
		
		/****************** 
		*/
		dates = new List<Date>();
		dateLabels = new List<String>();
		
		if (selectedGraphList == 'Daily') {
			//Check that the date range is two weeks at most - avoids too many SOQL queries
			Integer dateDiff = dateRange.Start_Date__c.daysBetween(dateRange.End_Date__c);
			if (dateDiff > 30) {
				//ERROR: too wide range for daily date selector
				ApexPages.Message msgErr = new ApexPages.Message(ApexPages.Severity.WARNING, 'This date range to too big for daily graph granularity! Please narrow the date range.');
           		ApexPages.addmessage(msgErr);
			} 
			else {
				for (Integer i = 0; i < dateDiff+1; i++)
				{
					Date d = dateRange.Start_Date__c.addDays(i);
					this.dates.add(d);
					
					Datetime dtime = datetime.newInstance(d.year(), d.month(), d.day());
					this.dateLabels.add(dtime.format('dd MMM yy'));
				}
				
			}		
		}
		else
		{
			Date initialDate = dateRange.Start_Date__c; //Get the first date in the range
			Date graphEndDate = shiftDateForward(dateRange.End_Date__c); //Shift the end date forward so we include the interval date just after the end date in the graph
			while (initialDate < graphEndDate)
			{
				this.dates.add(initialDate);
				
				Datetime dtime = datetime.newInstance(initialDate.year(), initialDate.month(), initialDate.day());
				this.dateLabels.add(dtime.format('dd MMM yy'));
				
				Date d = shiftDateForward(initialDate);
				initialDate = d;
			}
		}
		
		//Now call the queryByIntervals to populate the graph data
		queryByDateRangeIntervals();
	}
	
	/**
	* Method to add the required amount of date/time onto the given input date parameter value
	*/
	private Date shiftDateForward(Date dValue)
	{
		Date d;
		if (selectedGraphList == 'Monthly'){
			d = dValue.addMonths(1);
		}
		else if (selectedGraphList == 'Yearly') {
			d = dValue.addYears(1);
		}
		else {
			d = dValue.addDays(7);
		}
		return d;
	}
	
	/**
	 * Use the selected dates to find the data points for the graph
	 */
	private void queryByDateRangeIntervals() {
		
		//Clear the current chart plotting contents
		this.chart.clear();
		
		for (Integer i = 0; i < this.dates.size(); i++) {
			String query = currentSubs();
			
	   		try {
	   			
	   			// insert the correct dates into the query
	   			Integer result1 = 0;
	   			Integer result2 = 0;
	   			
	   			query = query.replace('#d1', String.valueOf(dates[i]));
	   			List<AggregateResult> ars = Database.query(query);
	   			for (AggregateResult ar : ars){
	   				if ((String)ar.get('Subscription_Type__c') == 'y')
	   					result2 = (Integer)ar.get('counter');
	   				else if ((String)ar.get('Subscription_Type__c') == 'm')
	   					result1 = (Integer)ar.get('counter');
	   			}
	   			
	   			DataRow row = new DataRow(this.dateLabels[i],result1,result2);
	   			this.chart.add(row);
	   			
	   		} catch (Exception e) {
	   			System.debug(e);
	   		}
		}
	}
	
	private String currentSubs() {
		String query = 'SELECT count(Id) counter, Subscription_Type__c FROM Product_Instance__c WHERE Start_Date__c <= #d1 AND End_Date__c > #d1 AND Subscription_Stream__c = \'Eureka Report\' AND (NOT Person_Account__r.PersonEmail LIKE \'%testbucket%\') AND Person_Account__r.Is_Test__c = false GROUP BY Subscription_Type__c';
		return query;
	}
	
	/***********************************************/
	/**
	 *
	 */
	public class DataRow {
		
		public String label { get; set; }
		
		public Decimal value1 { get; set; }
		public Decimal value2 { get; set; }
		public Decimal total { get; set; }
		
		public dataRow(String label, Decimal value1, Decimal value2) {
			this.label = label;
			this.value1 = value1;
			this.value2 = value2;
			this.total = value1 + value2;
		}
	}
	
}