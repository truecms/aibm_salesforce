/**
* TestSalesPerformanceReport
* Tests the functionality of the Sales Performance Report and also the SalesResultAggregator, which facilitates this report by
* providing the data. 
*
* @CreatedBy: Ross Tailby
*/
@IsTest
public with sharing class TestSalesPerformanceReport {

	private static List<User> getSalesUsers(Integer numUsers){
		List<User> salesUsers = [SELECT Id, Name FROM User WHERE IsActive = true AND Alias NOT IN ('sadm', 'it') and UserType = 'Standard' and UserRoleId != '' ORDER BY Name desc LIMIT :numUsers];
		return salesUsers;
	}
	
	private static AIBM_Settings__c createAIBMSettings(Decimal target) {
		AIBM_Settings__c settings = new AIBM_Settings__c();
		settings.Name = 'Defaults';
		settings.Weekly_Target__c = target;
		return settings;
	}
	
	private static Sales_Result__c createSalesResult(User owner, Date weekEnding, Integer num, String callStats) {
		Sales_Result__c sr = new Sales_Result__c();
		sr.Actual_Days_Worked__c = 5;
		sr.OwnerId = owner.Id;
		sr.Week_Ending__c = weekEnding;
		sr.New__c = num;
		sr.Late_Renewal__c = num*2;
		sr.Winback__c = num*3;
		sr.Call_Result__c = callStats;
		return sr;
	}

	public static TestMethod void testSalesResultAggregator() {
		
		//Create some accounts to assign transactions to
		List<Account> accounts = TestAccountHelper.createAccounts(4);
		
		//Get 2 users to create transactions as (not sys admin or IT)
		List<User> salesUsers = getSalesUsers(2);
		User u1 = salesUsers[0];
		User u2 = salesUsers[1];
		
		//Create a new transaction against each account using these users
		List<go1__Transaction__c> u1TransList = new List<go1__Transaction__c>();
		List<go1__Transaction__c> u2TransList = new List<go1__Transaction__c>();
		
		Integer numTrans = accounts.size();
		for (Integer i = 0; i < numTrans; i++) {
			go1__Transaction__c trans = new go1__Transaction__c();
			trans.go1__Account__c = accounts[i].Id;
			trans.go1__Account_ID__c = accounts[i].Id; 
			trans.go1__Amount__c = 100;
			trans.go1__Status__c = 'Success';
			trans.go1__Payment_Name__c = '123';
			trans.Send_Invoice__c = false;
			trans.go1__Code__c = 8;
			
			if (Math.Mod(i,2) == 0) {
				u1TransList.add(trans);
			}
			else {
				u2TransList.add(trans);
			}
		}
		System.runAs(u1) {
			//Just sanity check that we have 2 transactions in here!
			System.assertEquals(u1TransList.size(), 2);
			insert u1TransList;
			
			//Insert some calls here too 
			List<Task> callTasks = new List<Task>();
			Task t = new Task();
			t.Call_Result__c = 'No Answer';
			t.Subject = 'Test task1';
			t.Status = 'Completed';
			callTasks.add(t);
			Task t2 = new Task();
			t2.Call_Result__c = 'Sale';
			t2.Subject = 'Test task2';
			t2.Status = 'Completed';
			callTasks.add(t2);
			Task t3 = new Task();
			t3.Call_Result__c = 'Callback';
			t3.Subject = 'Test task3';
			t3.Status = 'Completed';
			callTasks.add(t3);
			Task t4 = new Task();
			t4.Call_Result__c = null;
			t4.Subject = 'Test task4';
			t4.Status = 'Completed';
			callTasks.add(t4);
			insert callTasks;
		}
		System.runAs(u2) {
			//..and here too
			System.assertEquals(u2TransList.size(), 2);
			insert u2TransList;
			
			//Plus the calls
			List<Task> callTasks = new List<Task>();
			Task t = new Task();
			t.Call_Result__c = 'Sale';
			t.Subject = 'Test task1';
			t.Status = 'Completed';
			callTasks.add(t);
			Task t2 = new Task();
			t2.Call_Result__c = 'Sale';
			t2.Subject = 'Test task2';
			t2.Status = 'Completed';
			callTasks.add(t2);
			Task t3 = new Task();
			t3.Call_Result__c = 'Sale';
			t3.Subject = 'Test task3';
			t3.Status = 'Completed';
			callTasks.add(t3);
			Task t4 = new Task();
			t4.Call_Result__c = 'Sale';
			t4.Subject = 'Test task4';
			t4.Status = 'Completed';
			callTasks.add(t4);
			insert callTasks;
		}
		
		
		//Run the aggregator
		Test.startTest();
		SchedulableContext ctx;
		SalesResultAggregator sra = new SalesResultAggregator();
		sra.execute(ctx);
		Test.stopTest();
		
		//Check that we have the right number of Sales_Result__c rows and that they hold the correct values
		List<Sales_Result__c> results = [SELECT Id, Owner.Name, Owner.Id, Week_Ending__c, New__c, Late_Renewal__c, Winback__c, Total__c, Call_Result__c
										FROM Sales_Result__c];
									
		System.assertEquals(results.size(), 2);
		System.assertNotEquals(results[0].OwnerId, results[1].OwnerId); //Check both users have an entry
		for (Sales_Result__c sr : results) {
			System.assertEquals(sr.Week_Ending__c, Date.today());
			if (sr.OwnerId == u1.Id || sr.OwnerId == u2.Id)
				System.assert(true);
			else
				System.assert(false);
			System.assertEquals(sr.New__c, 2);
			System.assertEquals(sr.Total__c, 2);
			
			//Check the Call Results in the records have the right values
			System.assert(sr.Call_Result__c.length() > 0);
			Map<String, Object> callMap = (Map<String, Object>) JSON.deserializeUntyped(sr.Call_Result__c);
		}
		
	}
	
	public static TestMethod void testSalesPerformanceData() {
		
		//Initially create the AIBM Settings custom settings for weekly targets
		AIBM_Settings__c aibm = createAIBMSettings(16.0);
		insert aibm;
		
		//Get the 'sales' users to create reports for
		List<User> salesUsers = getSalesUsers(4);
		
		//Create some Sales_Result__c test data for a few possible users
		List<Sales_Result__c> salesResults = new List<Sales_Result__c>();
		for (User u : salesUsers) {
			Sales_Result__c sr = createSalesResult(u, Date.Today(), 1, '');
			salesResults.add(sr);
		}
		
		//Also add records for the first 3 users for the previous week
		Date weekAgo = Date.Today().addDays(-7);
		for (Integer i = 0; i < salesUsers.size()-1; i++) {
			Sales_Result__c sr = createSalesResult(salesUsers[i], Date.Today().addDays(-7), 2, '');
			salesResults.add(sr);
		}
		
		//Finally, for one user, add a record for 2 weeks ago
		Date twoWeeksAgo = Date.Today().addDays(-14);
		Sales_Result__c srAdd = createSalesResult(salesUsers[0], Date.Today().addDays(-14), 3, '');
		salesResults.add(srAdd);
		
		insert salesResults;
		
		//Run the report
		Test.startTest();
		SalesPerformanceReport perfReport = new SalesPerformanceReport();
		Id salesResultId = perfReport.salesWrapperList[0].result.Id; 
		perfReport.salesWrapperList[0].daysWorked = '10';
		perfReport.saveActualDaysWorked();
		Test.stopTest();
		
		//Test we have the correct number of data rows
		if (twoWeeksAgo.year() < Date.Today().year()) {
			if (weekAgo.year() < Date.Today().year())
				System.assertEquals(perfReport.salesWrapperList.size(), 4);
			else
				System.assertEquals(perfReport.salesWrapperList.size(), 7);
		}
		else
			System.assertEquals(perfReport.salesWrapperList.size(), 8);
		System.assertEquals(SalesPerformanceReport.weeklyTargets, 16.0);
		
		List<SalesPerformanceReport.SalesResultWrapper> u1Res = new List<SalesPerformanceReport.SalesResultWrapper>();
		List<SalesPerformanceReport.SalesResultWrapper> u2Res = new List<SalesPerformanceReport.SalesResultWrapper>();
		List<SalesPerformanceReport.SalesResultWrapper> u3Res = new List<SalesPerformanceReport.SalesResultWrapper>();
		List<SalesPerformanceReport.SalesResultWrapper> u4Res = new List<SalesPerformanceReport.SalesResultWrapper>();
		
		//And that those rows have the right values and have the correct display parameters
		for (SalesPerformanceReport.SalesResultWrapper wrap : perfReport.salesWrapperList){
			
			if (wrap.result.Id == salesResultId) {
				System.assertEquals(wrap.result.Actual_Days_Worked__c, 10.0);
				System.assertEquals(wrap.weeklyTarget, 32.0);
			}
			else {
				System.assertEquals(wrap.weeklyTarget, 16.0);
			}

			if (wrap.result.OwnerId == salesUsers[0].Id)
				u1Res.add(wrap);
			else if (wrap.result.OwnerId == salesUsers[1].Id)
				u2Res.add(wrap);
			else if (wrap.result.OwnerId == salesUsers[2].Id)
				u3Res.add(wrap);
			else if (wrap.result.OwnerId == salesUsers[3].Id)
				u4Res.add(wrap);
		}
		
		if (twoWeeksAgo.year() < Date.Today().year()) {
			if (weekAgo.year() < Date.Today().year()) {
				System.assertEquals(u1Res.size(), 1);
				System.assertEquals(u1Res[0].result.Week_Ending__c, Date.today());
				System.assertEquals(u1Res[0].result.Total__c, 4);
				System.assertEquals(u1Res[0].percentToTarget, 25.00);
				
				System.assertEquals(u2Res.size(), 1);
				System.assertEquals(u2Res[0].result.Week_Ending__c, Date.today());
				System.assertEquals(u2Res[0].result.Total__c, 4);
				
				System.assertEquals(u3Res.size(), 1);
				System.assertEquals(u3Res[0].result.Week_Ending__c, Date.today());
				System.assertEquals(u3Res[0].result.Total__c, 4);
			}
			else {
				System.assertEquals(u1Res.size(), 2);
				System.assertEquals(u1Res[0].result.Week_Ending__c, Date.today());
				System.assertEquals(u1Res[0].result.Total__c, 4);
				System.assertEquals(u1Res[0].percentToTarget, 25.00);
					
				System.assertEquals(u1Res[1].result.Week_Ending__c, Date.today().addDays(-7));
				System.assertEquals(u1Res[1].result.Total__c, 8);
				if (u1Res[1].result.Actual_Days_Worked__c == 10.0)
					System.assertEquals(u1Res[1].percentToTarget, 25.00);
				else
					System.assertEquals(u1Res[1].percentToTarget, 50.00);
					
				System.assertEquals(u2Res.size(), 2);
				System.assertEquals(u2Res[0].result.Week_Ending__c, Date.today());
				System.assertEquals(u2Res[0].result.Total__c, 4);
				System.assertEquals(u2Res[1].result.Week_Ending__c, Date.today().addDays(-7));
				System.assertEquals(u2Res[1].result.Total__c, 8);
				
				System.assertEquals(u3Res.size(), 2);
				System.assertEquals(u3Res[0].result.Week_Ending__c, Date.today());
				System.assertEquals(u3Res[0].result.Total__c, 4);
				System.assertEquals(u3Res[1].result.Week_Ending__c, Date.today().addDays(-7));
				System.assertEquals(u3Res[1].result.Total__c, 8);
			}
		}
		else {
			System.assertEquals(u1Res.size(), 3);
			System.assertEquals(u1Res[0].result.Week_Ending__c, Date.today());
			System.assertEquals(u1Res[0].result.Total__c, 4);
			System.assertEquals(u1Res[0].percentToTarget, 25.00);
					
			System.assertEquals(u1Res[1].result.Week_Ending__c, Date.today().addDays(-7));
			System.assertEquals(u1Res[1].result.Total__c, 8);
			if (u1Res[1].result.Actual_Days_Worked__c == 10.0)
				System.assertEquals(u1Res[1].percentToTarget, 25.00);
			else
				System.assertEquals(u1Res[1].percentToTarget, 50.00);
					
			System.assertEquals(u1Res[2].result.Week_Ending__c, Date.today().addDays(-14));
			System.assertEquals(u1Res[2].result.Total__c, 12);
			if (u1Res[2].result.Actual_Days_Worked__c == 10.0)
				System.assertEquals(u1Res[2].percentToTarget, 37.50);
			else
				System.assertEquals(u1Res[2].percentToTarget, 75.00);
				
			System.assertEquals(u2Res.size(), 2);
			System.assertEquals(u2Res[0].result.Week_Ending__c, Date.today());
			System.assertEquals(u2Res[0].result.Total__c, 4);
			System.assertEquals(u2Res[1].result.Week_Ending__c, Date.today().addDays(-7));
			System.assertEquals(u2Res[1].result.Total__c, 8);
			
			System.assertEquals(u3Res.size(), 2);
			System.assertEquals(u3Res[0].result.Week_Ending__c, Date.today());
			System.assertEquals(u3Res[0].result.Total__c, 4);
			System.assertEquals(u3Res[1].result.Week_Ending__c, Date.today().addDays(-7));
			System.assertEquals(u3Res[1].result.Total__c, 8);
		}
		
		System.assertEquals(u4Res.size(), 1);
		System.assertEquals(u4Res[0].result.Week_Ending__c, Date.today());
		System.assertEquals(u4Res[0].result.Total__c, 4);
		
	}
	
	public static TestMethod void testNoSalesResults() {
		//Initially create the AIBM Settings custom settings for weekly targets
		AIBM_Settings__c aibm = createAIBMSettings(16.0);
		insert aibm;
		
		//Run the report
		Test.startTest();
		SalesPerformanceReport perfReport = new SalesPerformanceReport();
		Test.stopTest();
		
		System.assertEquals(perfReport.result, false);
		System.assertEquals(perfReport.salesWrapperList.size(), 0);
	}
	
	public static TestMethod void testShowHideSaveTargets() {
		//Initially create the AIBM Settings custom settings for weekly targets
		AIBM_Settings__c aibm = createAIBMSettings(16.0);
		insert aibm;
		
		//Run the report
		Test.startTest();
		SalesPerformanceReport perfReport = new SalesPerformanceReport();
		
		//Test the target section is undefined
		System.assertEquals(perfReport.displayTargets, null);
		
		//Show the target section
		perfReport.showTargets();
		System.assertEquals(perfReport.displayTargets, true);
		
		//Change the target and save
		SalesPerformanceReport.weeklyTargets = 10.0;
		perfReport.saveTargets();
		
		perfReport.hideTargets();
		System.assertEquals(perfReport.displayTargets, false);
		
		Test.stopTest();
		
		//Test that the new target is applied
		AIBM_Settings__c newSetting = [SELECT Id, Weekly_Target__c FROM AIBM_Settings__c WHERE Name = 'Defaults' LIMIT 1];
		System.assertEquals(newSetting.Weekly_Target__c, 10.0);
	}
	
	public static TestMethod void testGetYearOptions() {
		//Initially create the AIBM Settings custom settings for weekly targets
		AIBM_Settings__c aibm = createAIBMSettings(16.0);
		insert aibm;
		
		//Run the report
		Test.startTest();
		SalesPerformanceReport perfReport = new SalesPerformanceReport();
		
		List<SelectOption> options = perfReport.getYearOptions();
		Test.stopTest();
		
		//Check we have 5 years given
		System.assertEquals(options.size(), 5);
		
		//Check that the last entry is this year
		System.assertEquals(options[4].getValue(), String.valueOf(Date.Today().year()));
		
		//Check that the first entry is 5 years ago
		System.assertEquals(options[0].getValue(), String.valueOf(Date.Today().addYears(-4).year()));
	}
	
	public static TestMethod void testDownloadReport() {
		//Initially create the AIBM Settings custom settings for weekly targets
		AIBM_Settings__c aibm = createAIBMSettings(16.0);
		insert aibm;
		
		//Run the report
		Test.startTest();
		SalesPerformanceReport perfReport = new SalesPerformanceReport();
		
		PageReference prExport = perfReport.downloadReport();
		Test.stopTest();
		
		//Test that our Page Reference is going to the right page
		System.assertEquals(prExport.getUrl(), '/apex/Sales_Performance_Report_Export');
	}
	
	public static TestMethod void testInvalidDaysWorkedEntry() {
		//Initially create the AIBM Settings custom settings for weekly targets
		AIBM_Settings__c aibm = createAIBMSettings(16.0);
		insert aibm;
		
		//Get the 'sales' users to create reports for
		List<User> salesUsers = getSalesUsers(1);
		
		//Create some Sales_Result__c test data for a few possible users
		List<Sales_Result__c> salesResults = new List<Sales_Result__c>();
		for (User u : salesUsers) {
			Sales_Result__c sr = createSalesResult(u, Date.Today(), 1, '');
			salesResults.add(sr);
		}
		insert salesResults;
		
		PageReference pr = new PageReference('/apex/Sales_Performance_Report');
        Test.setCurrentPage(pr);
		
		//Run the report
		Test.startTest();
		SalesPerformanceReport perfReport = new SalesPerformanceReport();
		Id salesResultId = perfReport.salesWrapperList[0].result.Id; 
		perfReport.salesWrapperList[0].daysWorked = 'invalid';
		perfReport.saveActualDaysWorked();
		Test.stopTest();
		
		//Check that our page has an error message
        System.assert(ApexPages.getMessages().size() == 1);
        System.assertEquals(ApexPages.getMessages().get(0).getDetail(), 'Entries could not be saved. Invalid value in Actual Days Worked. Please scroll down to view invalid rows.');
	}
}