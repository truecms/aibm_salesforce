/**
 * ConversionReport
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
public class ConversionReport {
	
	// table data
	public List<DataRow> table { get; set; }
	
	// date range variables
	public List<Date> dates { get; set; }
	public List<String> dateLabels { get; set; }
	public List<Integer> months { get; set; }
	public Integer numberOfMonths { get; set; }
	public Integer startYear { get; set; }
	public Integer startMonth { get; set; }
	
	/**
	 *
	 */
	public ConversionReport() {
		numberOfMonths = 4;
		startYear = Date.today().year();
		startMonth = Date.today().month();
		
		// load data
		initData();
	}
	
	/**
	 * load and sort data to be displayed on the report
	 */
	public void initData() {
		table = new List<DataRow>();
		
		getDates();
		freeTrialsSubs();
		renewEarlySubs();
		renewedSubs();
		renewLateSubs();
	}
	
	/**
	 *
	 */
	public List<SelectOption> getIntervalOptions() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('weekly', 'weekly'));
		options.add(new SelectOption('monthly', 'monthly'));
		options.add(new SelectOption('yearly', 'yearly'));
		return options;
	}
	
	/***************************************************************************/
	
	// return a list of months
	public List<SelectOption> getMonthOptions() {
	    List<String> months = new List<String> {'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'};
	    List<SelectOption> options = new List<SelectOption>();
	    for (Integer i = 1; i <= 12; i++) {
	      options.add(new SelectOption(i.format(), months[i-1]));
	    }
	    return options;
	}
	
	// return a list of years
	public List<SelectOption> getYearOptions() {
		DateTime d = System.now();
		Integer year = (Integer)d.year();
		List<SelectOption> options = new List<SelectOption>();
		for (Integer i = -6; i < 6; i++) {
			String year_str = string.valueOf(year + i);
			options.add(new SelectOption(year_str, year_str));
		}
		return options;
	}
	
	// return a list of 10 years into the future
	public List<SelectOption> getNumberOfMonthsOptions() {
		List<SelectOption> options = new List<SelectOption>();
		for (Integer i = 1; i < 13; i++) {
			String option = string.valueOf(i);
			options.add(new SelectOption(option, option));
		}
		return options;
	}
	
	/***************************************************************************/
	
	/**
	 *
	 */
	private void getDates() {
		Date startDate = Date.newInstance(this.startYear, this.startMonth, 1);
		months = new List<Integer>();
		dates = new List<Date>();
		dateLabels = new List<String>();
		
		for (Integer i = 0; i <= numberOfMonths; i++) {
			if (i != numberOfMonths) {
				this.months.add(i);
			}
			
			Date d = startDate.addMonths(i);
			this.dates.add(d);
			
			Datetime dtime = datetime.newInstance(d.year(), d.month(), d.day());
			this.dateLabels.add(dtime.format('MMM yyyy'));
		}
	}
	
	/**
	 *
	 */
	private DataRow queryByIntervals(String originalQuery, DataRow row) {
		for (Integer i=0; i < this.numberOfMonths; i++) {
			String query = originalQuery;
	   		try {
	   			// insert the correct dates into the query
	   			query = query.replace('#d1', String.valueOf(dates[i]));
	   			query = query.replace('#d2', String.valueOf(dates[i+1]));
	   			
	   			if (row.queryType == 'count') {
	   				// return the number of rows
	   				Integer result = Database.countQuery(query);
	   				row.values.add(result);
	   			} else if (row.queryType == 'sum') {
	   				SObject result = (SObject)Database.query(query);
	   				if (result.get('Sum') != null) {
	   					row.values.add((Decimal)result.get('Sum'));
	   				} else {
	   					row.values.add(0);
	   				}
	   			}
	   		} catch (Exception e) {
	   			System.debug(e);
	   			row.values.add(0);
	   		}
		}
		return row;
	}
	
	private void freeTrialsSubs() {
		//String query = 'SELECT count() FROM Product_Instance__c WHERE Subscription_Renewal_Date__c < #d1 AND End_Date__c >= #d1 AND End_Date__c < #d2 AND Status__c <> \'Cancelled\'';
		DataRow row = new DataRow('Free Trials Converted', 'count', 'number');
		
		for (Integer i=0; i < this.numberOfMonths; i++) {
			// list all free subscriptions ending this month
			List<Product_Instance__c> subs = [SELECT Person_Account__c FROM Product_Instance__c WHERE Subscription_Type__c != 'f' AND Start_Date__c >= :this.dates[i] AND Start_Date__c < :this.dates[i+1]];
			List<Id> ids = new List<Id>();
			for (Product_Instance__c sub: subs) {
			    ids.add(sub.Person_Account__c);
			}
			List<Product_Instance__c> freeTrials = [SELECT Id, Person_Account__c, Strean__c FROM Product_Instance__c WHERE Subscription_Type__c = 'f' AND End_Date__c >= :this.dates[i] AND End_Date__c < :this.dates[i+1] AND Person_Account__c IN :ids];

			row.values.add(freeTrials.size());
		}
		this.table.add(row);
	}
	
	/**
	 *
	 */
	private void renewEarlySubs() {
		String query = 'SELECT count() FROM Product_Instance__c WHERE Subscription_Renewal_Date__c < #d1 AND End_Date__c >= #d1 AND End_Date__c < #d2 AND Status__c <> \'Cancelled\'';
		DataRow row = new DataRow('Early Renewal', 'count', 'number');
		this.table.add(queryByIntervals(query, row));
	}
	
	/**
	 *
	 */
	private void renewedSubs() {
		String query = 'SELECT count() FROM Product_Instance__c WHERE Subscription_Renewal_Date__c >= #d1 AND Subscription_Renewal_Date__c < #d2 AND Status__c <> \'Cancelled\'';
		DataRow row = new DataRow('Renewals', 'count', 'number');
		this.table.add(queryByIntervals(query, row));
	}
	
	/**
	 *
	 */
	private void renewLateSubs() {
		String query = 'SELECT count() FROM Product_Instance__c WHERE Subscription_Renewal_Date__c >= #d2 AND End_Date__c >= #d1 AND End_Date__c < #d2 AND Status__c <> \'Cancelled\'';
		DataRow row = new DataRow('Expired Resub', 'count', 'number');
		this.table.add(queryByIntervals(query, row));
	}
	
	/***********************************************/
	/**
	 *
	 */
	public class DataRow {
		
		public String label { get; set; }
		public List<Decimal> values { get; set; }
		public String format { get; set; }
		public String queryType { get; set; }
		
		public dataRow(String label, String queryType, String format) {
			this.label = label;
			this.queryType = queryType;
			this.format = format;
			this.values = new List<Decimal>();
		}
	}
}