/**
 * Batch Class
 *
 * Queries and populates the number of new and progressed opportunities through each stage per user per day
 * This queries the Reporting Snapshot which inserts a record per User per Stage early each morning with the previous days stats
 * This will then populate the other fields that the report could not provide.  
 *
 * Author: tailbyr
 * Copyright: EurekaReport.com.au
 */
global class OppsPerUserBatch implements Database.Batchable<Opps_Per_User__c>, Database.Stateful {

	public Date historyDate {get; set;}
	public BrightDay_Settings__c settings {get; set;}
	
	global OppsPerUserBatch(Date queryDate){
		this.historyDate = queryDate;
		this.settings = BrightDay_Settings__c.getInstance();
	}

	public Iterable<Opps_Per_User__c> start(Database.batchableContext BC) {
		List<Opps_Per_User__c> opps = [SELECT Id, User__c, Stage__c, New_Opportunities__c, Progressed_Opportunities__c
										FROM Opps_Per_User__c
										WHERE Snapshot_Date__c = :this.historyDate];
										
		return opps;
	}
	
	//Batchable override method: execute
    public void execute(Database.BatchableContext BC, List<Opps_Per_User__c> opps) {
        
        //Map<OwnerId <StageName, Count>>
		Map<Id, Map<String, Integer>> newStates = new Map<Id, Map<String, Integer>>();
		Map<Id, Map<String, Integer>> progressedStates = new Map<Id, Map<String, Integer>>();
	
		//Get the "New" (o.NewValue) and "Progressed" (o.OldValue) values for the given date - i.e. to stage
		List<OpportunityFieldHistory> newOppStage = [Select NewValue, OldValue, Opportunity.OwnerId
													From OpportunityFieldHistory 
													where Field = 'StageName'
													and DAY_ONLY(convertTimezone(CreatedDate)) = :this.historyDate];
													
		//The OpportunityFieldHistory does not give the number of Opps entering the pipeline at the 1st stage - get this separately from OpportunityHistory
		List<OpportunityHistory> oppsEntering = [SELECT Id, StageName, Opportunity.OwnerId
                                				FROM OpportunityHistory
                                				WHERE DAY_ONLY(convertTimezone(CreatedDate)) = :historyDate
                                				AND StageName = :settings.Opportunity_Default_Stage__c];
													
		//Feels like a hack, but seems to be the only way to Unit Test code based on Field History data
		List<OppFieldHistory> customFieldHistory = new List<OppFieldHistory>();
		if (Test.isRunningTest()){
			//Get a valid User for the Opportunity Owner - should match the one used in Unit Tests
			User u = [SELECT Id FROM User WHERE IsActive = TRUE AND UserType = 'Standard' AND UserRoleId != '' AND alias NOT IN ('sadm', 'it') ORDER BY alias DESC LIMIT 1];
			customFieldHistory.add(new OppFieldHistory(u.Id, '30 Day Trial', ''));
			customFieldHistory.add(new OppFieldHistory(u.Id, '30 Day Trial', ''));
			customFieldHistory.add(new OppFieldHistory(u.Id, 'Identify Persona', '30 Day Trial'));
			customFieldHistory.add(new OppFieldHistory(u.Id, 'Needs Analysis', 'Identify Persona'));
			customFieldHistory.add(new OppFieldHistory(u.Id, 'Closed Lost', 'Identify Persona'));
		}
		else {
			for (OpportunityFieldHistory ofh : newOppStage) {
				customFieldHistory.add(new OppFieldHistory(ofh.Opportunity.OwnerId, (String)ofh.NewValue, (String)ofh.OldValue));
			}
			
			for (OpportunityHistory oh : oppsEntering) {
				customFieldHistory.add(new OppFieldHistory(oh.Opportunity.OwnerId, (String)oh.StageName, ''));
			}
		}
													
		for (OppFieldHistory ofh : customFieldHistory) {
			Id ownerId = ofh.ownerId;
			String newToStage = ofh.newValue;
			String progressedFromStage = ofh.oldValue;
			
			//For the Opportunities achieving this new status
			if (newStates.containsKey(ownerId)){
				Map<String, Integer> addNewStage = newStates.get(ownerId);
				if (addNewStage.containsKey(newToStage)){
					Integer val = addNewStage.get(newToStage);
					val++;
					addNewStage.put(newToStage, val);
				}
				else {
					addNewStage.put(newToStage, 1);
				}
				newStates.put(ownerId, addNewStage);
			}
			else {
				Map<String, Integer> newMap = new Map<String, Integer>();
				newMap.put(newToStage, 1);
				newStates.put(ownerId, newMap);
			}
			
			//For Opportunities leaving the status and 'progressing' on
			if (progressedStates.containsKey(ownerId)){
				Map<String, Integer> addOldStage = progressedStates.get(ownerId);
				if (addOldStage.containsKey(progressedFromStage)){
					Integer val = addOldStage.get(progressedFromStage);
					val++;
					addOldStage.put(progressedFromStage, val);
				}
				else {
					addOldStage.put(progressedFromStage, 1);
				}
				progressedStates.put(ownerId, addOldStage);
			}
			else {
				Map<String, Integer> oldMap = new Map<String, Integer>();
				oldMap.put(progressedFromStage, 1);
				progressedStates.put(ownerId, oldMap);
			}
		}
		
		for (Opps_Per_User__c oppPerUser : opps) {
			
			//For this Opp_Per_User, get the User and Stage represented and find the New and Progressed values from our Maps
			Id owner = oppPerUser.User__c;
			String stage = oppPerUser.Stage__c;
			
			Integer newOpps = 0;
			Integer progressedOpps = 0;
			
			if (newStates.containsKey(owner)){
				if (newStates.get(owner).containsKey(stage)){
					newOpps = newStates.get(owner).get(stage);
				}
			}
			
			if (progressedStates.containsKey(owner)){
				if (progressedStates.get(owner).containsKey(stage)){
					progressedOpps = progressedStates.get(owner).get(stage);
				}
			}
			
			oppPerUser.New_Opportunities__c = newOpps;
			oppPerUser.Progressed_Opportunities__c = progressedOpps;
		}
		
		update opps;
    }
    
    //Batchable override method: finish - called when all batches are processed; allocates all fair tasks created in a fair manner across Acc mgrs
    public void finish(Database.BatchableContext BC) {
        //Nothing to do in the finish method
    }

	/*
	Custom Opportunity Field History class created so that we can actually Unit Test this code!
	*/
	public class OppFieldHistory{
		Id ownerId {get; set;}
		String newValue {get; set;}
		String oldValue {get; set;}
		
		public OppFieldHistory(Id owner, String newVal, String oldVal){
			this.ownerId = owner;
			this.newValue = newVal;
			this.oldValue = oldVal;
		}
	}

}