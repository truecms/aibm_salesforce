/**
 * DeferredPaymentsReportBase
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * DEPRECATED: This report is no longer required - superceeded by the new DeferredPaymentsReport implementation
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
public abstract class DeferredPaymentsReportBase {
	
	// table data
	public Map<String, DataRow> table { get; set; }
	public List<Decimal> totalsData { get; set; }
	public List<Decimal> accrualData { get; set; }
	public List<Integer> months { get; set; }
	
	// date range variables
	public List<Date> dates { get; set; }
	public List<String> dateLabels { get; set; }
	public Integer numberOfMonths { get; set; }
	public Integer startYear { get; set; }
	public Integer startMonth { get; set; }
	
	public String streamName { get; set; }
	public String productName { get; set; }
	public Integer productDate { get; set; }
	
	/**
	 * Initialises the initial variables
	 */
	public DeferredPaymentsReportBase() {
		setDefaultParamters();
		initData();
	}
	
	/**
	 * set the correct variables depending if there are parameters in the url
	 */
	public void setDefaultParamters() {
		// add a feature to allow the number of initial month range to be set in the url
		if (ApexPages.currentPage().getParameters().get('months') != null) {
			numberOfMonths = Integer.valueOf(ApexPages.currentPage().getParameters().get('months'));
		} else {
			numberOfMonths = 3;
		}
		// set the stream in the url
		if (ApexPages.currentPage().getParameters().get('stream') != null) {
			streamName = String.valueOf(ApexPages.currentPage().getParameters().get('stream'));
		} else {
			streamName = '';
		}
		// set starting date in the url
		if (ApexPages.currentPage().getParameters().get('startdate') != null) {
			Date startDate = Date.parse(ApexPages.currentPage().getParameters().get('startdate'));
			this.startYear = startDate.year();
			this.startMonth = startDate.month();
		} else {
			// start the previous month
			Date startDate = Date.today().addMonths(-1);
			
			this.startYear = startDate.year();
			this.startMonth = startDate.month();
		}
		productName = '';
	}
	
	/**
	 * 
	 */
	public void initData() {
		// store the dates for each month that needs to be reported on
		getDates();
		
		if (this.streamName == null || this.streamName == '') {
			this.productName = '';
		}
		
		if (this.streamName == null || this.streamName == '') {
			getStreamData();
		} else {
			getSubscriptionsData();
		}
		
		getTotals();
		getAccruals();
	}
	
	/**
	 *
	 */
	public PageReference refreshData() {
		initData();
		return null;
	}
	
	/*******************************************************************************/
	
	// return a list of stream options
	public List<SelectOption> getStreamOptions() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('', 'All'));
		
		List<SObject> streams = [SELECT Name FROM Subscription_Stream__c GROUP BY Name ORDER By Name ASC];
		if(!streams.isEmpty()) {
			for (SObject stream : streams) {
				String name = String.valueOf(stream.get('Name'));
				options.add(new SelectOption(name, name));
			}
		}
  		return options;
	}
	
	// return a list of months
	public List<SelectOption> getMonthOptions() {
	    List<String> months = new List<String> {'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'};
	    List<SelectOption> options = new List<SelectOption>();
	    for (Integer i = 1; i <= 12; i++) {
	      options.add(new SelectOption(i.format(), months[i-1]));
	    }
	    return options;
	}
	
	// return a list of years
	public List<SelectOption> getYearOptions() {
		DateTime d = System.now();
		Integer year = (Integer)d.year();
		List<SelectOption> options = new List<SelectOption>();
		for (Integer i = -6; i < 6; i++) {
			String year_str = string.valueOf(year + i);
			options.add(new SelectOption(year_str, year_str));
		}
		return options;
	}
	
	// return a list of 10 years into the future
	public List<SelectOption> getNumberOfMonthsOptions() {
		List<SelectOption> options = new List<SelectOption>();
		for (Integer i = 1; i < 25; i++) {
			String option = string.valueOf(i);
			options.add(new SelectOption(option, option));
		}
		return options;
	}
	
	/*******************************************************************************/
	
	/**
	 * Creates three lists:
	 * one to store the month number 
	 * second to store the starting date for each month
	 * third to store the date labels to display on the report
	 */
	private void getDates() {
		Date startDate = Date.newInstance(this.startYear, this.startMonth, 1);
		months = new List<Integer>();
		dates = new List<Date>();
		dateLabels = new List<String>();
		
		for (Integer i = 0; i <= numberOfMonths; i++) {
			if (i != numberOfMonths) {
				this.months.add(i);
			}
			
			Date d = startDate.addMonths(i);
			this.dates.add(d);
			
			Datetime dtime = datetime.newInstance(d.year(), d.month(), d.day());
			this.dateLabels.add(dtime.format('MMM yyyy'));
		}
	}
	
	/**
	 * Loops through all the subscription products and retrieves the deferred payment amounts
	 * over the time specified, grouped by subscriptions
	 */
	private void getSubscriptionsData() {
		table = new Map<String, DataRow>();
		
		List<Subscription_Product__c> products = new List<Subscription_Product__c>();
		if (this.streamName == '' || this.streamName == null) {
			products = [SELECT Name FROM Subscription_Product__c];
		} else {
			products = [SELECT Name FROM Subscription_Product__c WHERE Subscription_Stream__r.Name = :this.streamName];
		}
		
		if (!products.isEmpty()) {
			for (Subscription_Product__c product : products) {
				this.table.put(product.Name, new DataRow(product.Name));
			}
			deferredPaymentsData();
		}
	}
	
	/**
	 * Loops through all the subscription products and retrieves the deferred payment amounts
	 * over the time specified, grouped by streams
	 */
	private void getStreamData() {
		table = new Map<String, DataRow>();
		
		List<Subscription_Stream__c> streams = new List<Subscription_Stream__c>();
		streams = [SELECT Name FROM Subscription_Stream__c];
		
		if (!streams.isEmpty()) {
			for (Subscription_Stream__c stream : streams) {
				this.table.put(stream.Name, new DataRow(stream.Name));
			}
			deferredPaymentsData();
		}
	}
	
	/**
	 * Query deferred payments for all subscriptions for one monthly time period
	 */
	private void deferredPaymentsData() {
		Integer months = 0;
		for (Date d : this.dates) {
			if (d > Date.today().toStartOfMonth()) {
				break;
			} else {
				months++;
			}
		}
		
		// query the historical data, the first month to this month (excluding)
		historicalData(months);
		// query the future data, this month (including) up to the last month
		futureData(months);
	}
	
	public abstract void historicalData(Integer pastMonths);
	
	public abstract void futureData(Integer futureMonths);
	
	public abstract void getAccruals();
	
	/**
	 * Loop through the table data and calculate the totals for each column
	 */
	private void getTotals() {
		totalsData = new List<Decimal>();
		for (Integer i=0; i<numberOfMonths; i++) {
			Decimal total = 0;
			for (DataRow product : this.table.values()) {
				total += product.values[i];
			}
			this.totalsData.add(total);
		}
	}
	
	/****************************************************************/
	/**
	 *
	 */
	public class DataRow {
		
		public String label { get; set; }
		public List<String> dateLinks { get; set; }
		public List<Decimal> values { get; set; }
		
		public dataRow(String label) {
			this.label = label;
			this.values = new List<Decimal>();
			this.dateLinks = new List<String>();
		}
	}
}