/**
 * AccountHelper is a wrapper class for the Account object in Salesforce with
 * custom fields developered for Eureka Report/Business Spectator.
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
global with sharing class AccountHelper implements Database.Batchable<Account> {
	
	public final String ER_STREAM = 'Eureka Report';
	//public final String LM_STREAM = 'Eureka Live Markets';
	
	public List<Account> accounts { get; set; }
	
	public Account account { get; set; }

	public AccountHelper() {
	}

	public AccountHelper(Account account) {
		this.account = account;
	}

	public AccountHelper(List<Account> accounts) {
		if (accounts.size() > 0) {
			this.account = accounts[0];
		}
		this.accounts = accounts;
	}
	
	public Boolean isNewAccount() {
		if (this.account.Id == null) {
			return true;
		}
		return false;
	}
	
	public Boolean mergeDuplicateAccounts() {
		List<Account> duplicates = [SELECT Id FROM Account WHERE PersonEmail = :this.account.PersonEmail AND Id != :this.account.Id LIMIT 2];
		if (!duplicates.isEmpty()) {
			merge this.account duplicates;
			return true;
		}
		return false;
	}
	
	public Boolean hasCurrentEurekaFT() {
		getCurrentSubscriptions();
		if (!currentSubs.isEmpty()) {
			if (currentSubs[0].Subscription_Type__c == 'f') {
				return true;
			}
		}
		return false;
	}
	
	public Boolean hasCurrentEurekaPaid() {
		getCurrentSubscriptions();
		if (!currentSubs.isEmpty()) {
			if (currentSubs[0].Subscription_Type__c != 'f') {
				return true;
			}
		}
		return false;
	}
	
	public Boolean hasPreviousEurekaFT() {
		getPastSubscriptions(); 
		if (!pastSubs.isEmpty()) {
			if (pastSubs[0].Subscription_Type__c == 'f') {
				return true;
			}
		}
		return false;
	}
	
	public Boolean hasPreviousEurekaPaid() {
		getPastSubscriptions();
		if (!pastSubs.isEmpty()) {
			if (pastSubs[0].Subscription_Type__c != 'f') {
				return true;
			}
		}
		return false;
	}

	private List<Product_Instance__c> currentSubs;
	private List<Product_Instance__c> pastSubs;
	
	private void getCurrentSubscriptions() {
		if (currentSubs == null) {
			currentSubs = [SELECT Id, Subscription_Type__c FROM Product_Instance__c WHERE Person_Account__c =:account.Id AND Start_Date__c <= :Date.today() AND End_Date__c >= :Date.today() LIMIT 1];
		}
	}
	
	private void getPastSubscriptions() {
		if (pastSubs == null) {
			pastSubs = [SELECT Id, Subscription_Type__c FROM Product_Instance__c WHERE Person_Account__c =:account.Id AND End_Date__c < :Date.today() LIMIT 1];
		}
	}
		
	/**
	 *
	 */
	public void LoadTransactionsInfo() {
		List<Id> accIds = new List<Id>();
		for (Account account : this.accounts) {
			accIds.add(account.Id);
			account.ER_Reporting_First_Payment__c = null;
			account.ER_Reporting_Last_Payment__c = null;
		}
		List<go1__Transaction__c> transactions = [Select Id, Name, go1__Date__c, go1__Account__c FROM go1__Transaction__c WHERE go1__Account__c in :accIds ORDER BY go1__Date__c];
		for (go1__Transaction__c trans : transactions) {
			for (Account acc : this.accounts) {
				if (trans.go1__Account__c == acc.Id) {
					if (acc.ER_Reporting_First_Payment__c == null) {
						acc.ER_Reporting_First_Payment__c = Date.valueOf(trans.go1__Date__c); 
					}
					if (acc.ER_Reporting_Last_Payment__c == null) {
						acc.ER_Reporting_Last_Payment__c = Date.valueOf(trans.go1__Date__c);
					}
					
					if (trans.go1__Date__c < acc.ER_Reporting_First_Payment__c) {
						acc.ER_Reporting_First_Payment__c = Date.valueOf(trans.go1__Date__c);
					}
					
					if (trans.go1__Date__c > acc.ER_Reporting_First_Payment__c) {
						acc.ER_Reporting_Last_Payment__c = Date.valueOf(trans.go1__Date__c);
					}
				}
			}
		}
	}
	
	/**
	 *
	 */
	public void LoadSubscriptionInfo() {
		//String stream = 'Eureka Report';
		//Extend the function to be able to pull back multiple subscription streams and populate Account fields accordingly
		List<String> streams = new List<String>{this.ER_STREAM}; //, this.LM_STREAM};
		
		List<Id> accIds = new List<Id>();
		for (Account account : this.accounts) {
			accIds.add(account.Id);
			account.ER_Current_Active_Subscription__c = null;
			account.Sub_Campaign__c = null;
			account.ER_Entry_Free_Trial__c = null;
			/*account.Is_Active__c = false; //SF-818 Removing the unnecessary Is_Active__c field 
			account.LM_Is_Active__c = false;
			account.LM_Current_Active_Subscription__c = null; */
		}
		List<Product_Instance__c> subs = [Select Id, Name, Status__c, Start_Date__c, End_Date__c, Strean__c, Subscription_Stream__c, Subscription_Type__c, Person_Account__c, Campaign__c FROM Product_Instance__c WHERE Person_Account__c IN :accIds AND Subscription_Stream__c IN :streams ORDER BY End_Date__c];
		Date now = date.today();
		for (Product_Instance__c instance : subs) {
			for (Account acc : this.accounts) {
				if (instance.Person_Account__c == acc.Id) {
					if (acc.Sub_Campaign__c == null && instance.Subscription_Type__c != 'f' && instance.Subscription_Stream__c == this.ER_STREAM) {
						acc.Sub_Campaign__c = instance.Campaign__c;
					}
					if (instance.Subscription_Type__c == 'f' && instance.Subscription_Stream__c == this.ER_STREAM) {
						//if (acc.ER_Entry_Free_Trial__c == null) { //Removed as per BAU-666 - latest FT not earliest
							acc.ER_Entry_Free_Trial__c = instance.Id;
						//}
					}
					// first paid product instance
					if (acc.Eureka_Subscribed_Campaign__c == null 
							&& instance.Subscription_Type__c != null
							&& instance.Subscription_Type__c != 'f'
							&& instance.Subscription_Stream__c == this.ER_STREAM) {
						acc.Eureka_Subscribed_Campaign__c = instance.Id;
					}
					// last paid product instance
					if (instance.Subscription_Type__c != null
							&& instance.Subscription_Type__c != 'f'
							&& instance.Subscription_Stream__c == this.ER_STREAM) {
						acc.ER_Last_Paid_Subscription__c = instance.Id;
					}
					// current campaign
					if (instance.Start_Date__c <= Date.today() && instance.End_Date__c >= Date.today() && instance.Status__c != 'Cancelled' && instance.Subscription_Stream__c == this.ER_STREAM){
						acc.ER_Current_Active_Subscription__c = instance.Id;
						/*acc.Eureka_Current_Campaign__c = instance.Campaign__c; //SF-864 cleaning up unused lookup fields */
						/*acc.Is_Active__c = true; //SF-818 removing unnecessary field */
					}
					//Find Subscription type of the associated Product Instance with the latest end date (may be more than 1 PI if they have current free and future paid subs)
					if (instance.End_Date__c == acc.ER_Reporting_Sub_End_Date__c && instance.Subscription_Stream__c == this.ER_STREAM){
						acc.Sub_Type__c = instance.Subscription_Type__c;
					} 
					
					//See if the user has an active LM product subscription
					/*if (instance.Subscription_Stream__c == this.LM_STREAM 
							&& instance.Start_Date__c <= Date.today() 
							&& instance.End_Date__c >= Date.today()
							&& instance.Status__c != 'Cancelled') {
						acc.LM_Is_Active__c = true;
						acc.LM_Current_Active_Subscription__c = instance.Id;
					}*/
				}
				
				// look at the active subscription to work out what the substatus should be
				if (acc.ER_Current_Active_Subscription__c != null && acc.ER_Current_Active_Subscription__c == instance.Id && instance.Subscription_Stream__c == this.ER_STREAM) {
					if (acc.Sub_Status__c == null || acc.Sub_Status__c == '' || instance.Status__c == null || instance.Status__c == '') {
						this.setSubStatus(acc, instance);
					}
					else {
						//acc.Sub_Status__c = instance.Status__c;
					}
				}
			}
		}
	}

    /**
     *
     */
	public void setSubStatus(Account acc, Product_Instance__c instance) {
		// 2 is comp, 3 is ft, 4 is paid, 8 is expired, 7 is payment failed
		if (instance.Subscription_Type__c == 'c') {
			acc.Sub_Status__c = '2';
		}
		else if (instance.Subscription_Type__c == 'f') {
			acc.Sub_Status__c = '3';
		}
		else {
			acc.Sub_Status__c = '4';
		}
			
		if (instance.End_Date__c < date.today()) {
			//acc.Sub_Status__c = '8';
		}
	}
	
	/**
	 * @todo move the the ProductInstanceHelper class.
	 */
	public List<Product_Instance__c> getSubscriptions(String stream) {
		List<Product_Instance__c> subs = [Select Id, Name, Start_Date__c, End_Date__c, Strean__c, Subscription_Stream__c, Subscription_Type__c, Person_Account__c FROM Product_Instance__c WHERE Person_Account__c = :this.account.Id AND Subscription_Stream__c = :stream ORDER BY Start_Date__c];
		return subs;
	}
	
	/**
	 *
	 */
	public List<go1__Transaction__c > getTransactions() {
		List<go1__Transaction__c> subs = [Select Id, Name, go1__Date__c FROM go1__Transaction__c WHERE go1__Account__c = :this.account.Id];
		return subs;
	}
	
	/**
	 *
	 */
	public List<go1__Recurring_Instance__c> getRecurringInstances() {
		List<go1__Recurring_Instance__c> instances = [Select Id, Name, go1__Amount__c, go1__Credit_Card_Account__c, Credit_Card_Surcharge__c FROM go1__Recurring_Instance__c WHERE go1__Account__c = :this.account.Id];
		return instances;
	}
	
	/**
	 *
	 */
	public static AccountHelper loadAccountHelper(String Id) {
		AccountHelper helper = new AccountHelper();
		helper.account = helper.loadAccount(Id);
		return helper;
	}
	
	public Account loadAccount() {
		return loadAccount(account.Id);
	}
	
	/**
	 *
	 */
	public Account loadAccount(String Id) {
		List<Account> accounts = [SELECT Id,
					   PersonContactId,
					   Name,
					   FirstName,
					   LastName,
					   PersonEmail,
					   Phone,
					   BillingStreet,
					   BillingCity,
					   BillingState,
					   BillingCountry,
					   BillingPostalCode,
					   Birth_Year__c,
					   IsPersonAccount,
					   RecordTypeId,
					   ER_Reporting_Sub_End_Date__c, 
					   Sub_Status__c,
					   ER_Entry_Free_Trial__c,
					   ER_Reporting_First_Payment__c,
					   ER_Reporting_Last_Payment__c,
					   ER_Current_Active_Subscription__c,
					   Eureka_Subscribed_Campaign__c,
					   OwnerId,
					   GST_Exempt__c
				FROM Account
				WHERE Id = :Id LIMIT 1];
		if (!accounts.isEmpty()) {
			account = accounts[0];
			return account;
		}
		return null;
	}

    /**
     * What if there is a two accounts with the same email.
     */
	public Account loadAccountByEmail(String email) {
		List<Account> accounts = [SELECT Id, ER_Reporting_Sub_End_Date__c FROM Account WHERE PersonEmail = :email LIMIT 1];
		if (!accounts.isEmpty()) {
			return accounts[0];
		}
		return null;		
	}
	
	/**
     *
     */
	public void loadAccountsByEmail(List<String> emails) {
		List<Account> accounts = [SELECT Id, ER_Reporting_Sub_End_Date__c FROM Account WHERE PersonEmail IN :emails];
		if (!accounts.isEmpty()) {
			this.accounts = accounts;
		}
	}
	
	/**
     *
     */
    public Integer deleteAccounts() {
    	Integer deleteCount = 0;
    	
    	// initialise empty lists
    	List<Product_Instance__c> subscriptions = new List<Product_Instance__c>();
    	List<go1__Recurring_Instance__c> recurring = new List<go1__Recurring_Instance__c>();
    	List<go1__Transaction__c> transactions = new List<go1__Transaction__c>();
    	List<go1__CreditCardAccount__c> creditcards = new List<go1__CreditCardAccount__c>();
    	
    	// retrieve accounts
    	if (this.accounts != null && !this.accounts.isEmpty()) {
	    	List<Id> ids = new List<Id>();
	    	for (Account account : this.accounts) {
	    		ids.add(account.Id);
	    	}
	    	
	    	// delete product instances
	    	subscriptions = [SELECT Id FROM Product_Instance__c WHERE Person_Account__c IN :ids];
	    	delete subscriptions;
	    	
	    	// delete recurring instances
	    	recurring = [SELECT Id FROM go1__Recurring_Instance__c WHERE go1__Account__c IN :ids];
	    	delete recurring;
	    	
	    	// delete transactions
	    	transactions = [SELECT Id FROM go1__Transaction__c WHERE go1__Account__c IN :ids];
	    	delete transactions;
	    	
	    	// delete credit cards
	    	creditcards = [SELECT Id FROM go1__CreditCardAccount__c WHERE go1__Account__c IN :ids];
	    	delete creditcards;
	    	
	    	// delete accounts
	    	delete this.accounts;
	    	deleteCount = this.accounts.size();
	    	System.debug('Accounts deleted: '+deleteCount);
	    	this.accounts.clear();
    	}
    	
    	System.debug('Product Instances deleted: '+subscriptions.size());
    	System.debug('Recurring Instances deleted: '+recurring.size());
    	System.debug('Transactions deleted: '+transactions.size());
    	System.debug('Credit Cards deleted: '+creditcards.size());
    	
    	return deleteCount;
    }

	public Campaign getCurrentOffer() {
		/* SF-864 Removed as Campaign_Offer__c is always NULL and therefore is not used
		if (this.account.Campaign_Offer__c != null) {
			List<Campaign> currentOffers = [SELECT Id, ParentId, Yearly_Product__c, Monthly_Product__c, Misc_Product__c FROM Campaign WHERE Id = :account.Campaign_Offer__c AND EndDate >= :Date.today()];
			if (!currentOffers.isEmpty()) {
				return currentOffers[0];
			}
		}*/
		return null;
	}
	
	/**
	 * Set the default email preferences on an account based on the product
	 */
	public void setDefaultEmailPreferences(Subscription_Product__c subscription) {
		List<Subscription_Stream__c> streams = [SELECT Id, Name FROM Subscription_Stream__c WHERE Id = :subscription.Subscription_Stream__c LIMIT 1];
        if (!streams.isEmpty()) {
        	// initialise all checkboxes as false 
            this.account.Webinar_Email__c = false;
            this.account.WE_Briefing__c = false;
            this.account.Publication__c = false;
            //this.account.Morning_Briefing__c = false;
            //this.account.KGB_Dossier__c = false;
            //this.account.Afternoon_Update__c = false;
            
            if (streams[0].Name == 'Eureka Report') {
                this.account.Webinar_Email__c = true;
                this.account.WE_Briefing__c = true;
                this.account.Publication__c = true;
            }/* else if(streams[0].Name == 'Business Spectator') {
                this.account.Morning_Briefing__c = true;
                this.account.KGB_Dossier__c = true;
                this.account.Afternoon_Update__c = true;
            }*/
        }
	}
	
	/**
	 *
	 */
	public void updateRecurringInstanceCreditCards() {
		go1__CreditCardAccount__c preferred = this.getPreferredCreditCard();
		if (preferred != null) {
			Decimal surcharge = AIBMHelper.getCreditCardSurcharge(preferred.go1__Card_Type__c);
			List<go1__Recurring_Instance__c> instances = this.getRecurringInstances();
			for (go1__Recurring_Instance__c instance : instances) {
				instance.go1__Credit_Card_Account__c = preferred.Id;
				
				if (instance.Credit_Card_Surcharge__c == null) {
					instance.Credit_Card_Surcharge__c = 0;
				}
				// set the amount to the default price before recalculating the new amount i.e. remove any surcharges
				instance.go1__Amount__c = AIBMHelper.getAmountWithoutSurcharge(instance.go1__Amount__c, instance.Credit_Card_Surcharge__c);
				// set the new surcharge and calculate the new amount with surcharge
				instance.Credit_Card_Surcharge__c = surcharge;
				instance.go1__Amount__c = AIBMHelper.getAmountWithSurcharge(instance.go1__Amount__c, instance.Credit_Card_Surcharge__c);
			}
			update instances;
		}
	}
	
	/**
	 *
	 */
	public void removePreferredCreditCard() {
		// set all the cc's preferred checkbox as false
		go1__CreditCardAccount__c preferred = this.getPreferredCreditCard();
		// untick the old preferred cc
		if (preferred != null) {
			preferred.Preferred_Active_Card__c = false;
			update preferred;
		}
	}
	
	/**
	 *
	 */
	public go1__CreditCardAccount__c getPreferredCreditCard() {
		List<go1__CreditCardAccount__c> creditcards = [SELECT Id, Name, go1__Card_Type__c, go1__Token__c, go1__Expiry__c, go1__Payment_Gateway__c, go1__Account__c, Preferred_Active_Card__c FROM go1__CreditCardAccount__c WHERE go1__Account__c = :this.account.Id AND Preferred_Active_Card__c = true ORDER BY Expiry_Year__c DESC, Expiry_Month__c DESC LIMIT 1];
		if (!creditcards.isEmpty()) {
			return creditcards[0];
		}
		return null;
	}
	
	/**
     * Go2Debit: This might be able to go back in the go2debit package.
     */
	public go1__CreditCardAccount__c getDefaultCreditCard() {
		List<go1__CreditCardAccount__c> creditcards = [SELECT Id, Name, go1__Card_Type__c, go1__Token__c, go1__Expiry__c, go1__Payment_Gateway__c, go1__Account__c, Preferred_Active_Card__c FROM go1__CreditCardAccount__c WHERE go1__Account__c = :this.account.Id ORDER BY Expiry_Year__c DESC, Expiry_Month__c];
		if (!creditcards.isEmpty()) {
			return creditcards[0]; 
		}
		return null;
	}
	
	/**
     * Return all cards associated to the Account. Use of this avoids the need for both query methods above
     */
	public List<go1__CreditCardAccount__c> getAllAccountCreditCards() {
		List<go1__CreditCardAccount__c> creditcards = [SELECT Id, Name, go1__Card_Type__c, go1__Token__c, go1__Expiry__c, go1__Payment_Gateway__c, go1__Account__c, Preferred_Active_Card__c FROM go1__CreditCardAccount__c WHERE go1__Account__c = :this.account.Id ORDER BY Expiry_Year__c DESC, Expiry_Month__c];
		return creditcards;
	}
	
	/**
	 * This queries all the accounts with the current subscriptions but 'is_active' = false
	 */
	public List<Account> getActiveAccounts() {
		List<Account> accounts = [
			SELECT Id
			FROM Account
			WHERE Id IN (
				SELECT Person_Account__c
				FROM Product_Instance__c
				WHERE End_Date__c >= :Date.today() AND
					Start_Date__c <= :Date.today() AND
					Subscription_Stream__c IN (:this.ER_STREAM) //, :this.LM_STREAM)
				) AND ER_Current_Active_Subscription__c = NULL
			LIMIT 1000
		];
		return accounts;
	}
	
	/**
	 * This queries all the accounts that does not have a current subscription but 'is_active' is set as true
	 * NOTE: not sure if the inner query finds every single current subscription
	 */
	public List<Account> getInactiveAccounts() {
		List<Account> accounts = [
			SELECT Id
			FROM Account
			WHERE Id NOT IN (
				SELECT Person_Account__c
				FROM Product_Instance__c
				WHERE End_Date__c >= :Date.today() AND
					Start_Date__c <= :Date.today()
				) AND ER_Current_Active_Subscription__c != NULL
			LIMIT 1000
		];
		return accounts;
	}
	
	
	/**
	* Grabs the list of Accounts with FTs expired x (currently 7) days ago without having subscribed and creates a follow up task against them
	*/
	public void createExpiredFTTasks(Boolean exFTAllocationRule, TaskManager tm) {
		List<Task> aNewTasks = new List<Task>();
		
		//Query for any tasks associated to the accounts being considered. If task associated, assign Acc Mgr to same as this prev task
		List<Id> accs = new List<Id>();
		for (Account account : this.accounts)
		{
			accs.add(account.Id);
		}
		List<Task> preExistingTasks = [SELECT Id, OwnerId, AccountId FROM Task where AccountId IN :accs AND Owner.IsActive=true ORDER BY createdDate ASC];
		Map<Id, Task> prevAccountTasks = new Map<Id, Task>();
		for (Task t : preExistingTasks)
		{
			prevAccountTasks.put(t.AccountId, t);
		}
		
		for (Account account : this.accounts)
		{
			this.account = account;
			
			Task newTask = null;
			
			//For each allocation rule, check that we want to apply the rule through the exFTAllocationRule boolean		
			//First task allocation rule - try to assign to the Account Owner if it is a User in the Account Management list
			Id accountOwner = this.account.OwnerId;
			if (tm.salesTeamMemberMap.containsKey(accountOwner) && exFTAllocationRule)
			{
				//Create task assigned to the Account Manager
				newTask = tm.generateExpiredFreeTrialTask(this.account, accountOwner);
			}
			else if (prevAccountTasks.containsKey(this.account.Id) && exFTAllocationRule) //Check if the account already has tasks against it. If so, assign new task to their owner
			{
				//Create the task against the owner of the existing tasks against this account
				newTask = tm.generateExpiredFreeTrialTask(this.account, prevAccountTasks.get(this.account.Id).OwnerId);
			}
			else 
			{
				//Just get the task created with the fairly allocation i.e. next Acc Mgr in line
				newTask = tm.generateExpiredFreeTrialTask(this.account);
			}
			aNewTasks.add(newTask);
		}
		insert aNewTasks;
	}
	
	/**
	* Grabs the list of Accounts with paid subs that expired x (currently 10) days ago that have not renewed i.e. not on autorenew or are monthly subs
	*/
	public void createLateRenewalTasks(Boolean lateRenewalAllocationRule, TaskManager tm) {
		List<Task> aNewTasks = new List<Task>();
		
		//Query for any tasks associated to the accounts being considered. If task associated, assign Acc Mgr to same as this prev task
		List<Id> accs = new List<Id>();
		for (Account account : this.accounts)
		{
			accs.add(account.Id);
		}
		List<Task> preExistingTasks = [SELECT Id, OwnerId, AccountId FROM Task where AccountId IN :accs AND Owner.IsActive=true ORDER BY createdDate ASC];
		Map<Id, Task> prevAccountTasks = new Map<Id, Task>();
		for (Task t : preExistingTasks)
		{
			prevAccountTasks.put(t.AccountId, t);
		}
		
		for (Account account : this.accounts)
		{
			this.account = account;
			
			Task newTask = null;
					
			//First task allocation rule - try to assign to the Account Owner if it is a User in the Account Management list
			Id accountOwner = this.account.OwnerId;
			if (tm.salesTeamMemberMap.containsKey(accountOwner) && lateRenewalAllocationRule) //We can use SalesTeamMemberMap as its being dynamically constructed from the Custom setting retrived in the initator
			{
				//Create task assigned to the Account Manager
				newTask = tm.generateLateRenewalTask(this.account, accountOwner);
			}
			else if (prevAccountTasks.containsKey(this.account.Id) && lateRenewalAllocationRule) //Check if the account already has tasks against it. If so, assign new task to their owner
			{
				//Create the task against the owner of the existing tasks against this account
				newTask = tm.generateLateRenewalTask(this.account, prevAccountTasks.get(this.account.Id).OwnerId);
			}
			else 
			{
				//Just get the task created with the fairly allocation i.e. next Acc Mgr in line
				newTask = tm.generateLateRenewalTask(this.account);
			}
			aNewTasks.add(newTask);
		}
		insert aNewTasks;
	}
	
	/**
	 * The BATCHABLE interface means we can parse a bunch of objects and run any updates on them.
	
	global final String Query;
	global Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator(query);
	}
	*/
	
	global Iterable<Account> start(Database.batchableContext BC){
		List<Account> batchInstances = this.accounts;
		return batchInstances;
	}
	
	global void execute(Database.BatchableContext BC, Account[] accounts) {
		AccountHelper helper = new AccountHelper(accounts);
		helper.LoadTransactionsInfo();
		helper.LoadSubscriptionInfo();
		upsert accounts;
	}
	
	global void finish(Database.BatchableContext BC){
		AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
	    	TotalJobItems, CreatedBy.Email
	    	FROM AsyncApexJob WHERE Id =
	    	:BC.getJobId()];
	}
}