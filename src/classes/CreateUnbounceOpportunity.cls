/**
 *
 * Class to create Unbounce opportunities using Email Service realtime.
 * Created on 6-11-2015
 * Author: Mithun Roy
 * Copyright: EurekaReport.com.au
 */
global class CreateUnbounceOpportunity implements Messaging.InboundEmailHandler {
    //Method to parse the inbound email received by Email Service.
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        try {
            String subToCompare = 'Create Unbounce Opportunity';
            if (email.subject.equalsIgnoreCase(subToCompare)) {
                String pagename = null;
                String url = null;
                String fullname = null;
                String emailaddress = null;
                String phonenumber = null;
                String utmsource = null;
                String utmmedium = null;
                String utmcontent = null;
                String utmcampaign = null;
                String sfcampaign = null;
                String timetocall = null;
                String utmipaddress = null;
                String type = 'Existing Business';
                
                // Retrieves content from the email and splits each line by the terminating newline character.
                String[] emailBody = email.plainTextBody.split('\n', 0);
                for (integer i=0 ; i < emailBody.size(); i++) {
                    String FieldName = emailBody[i].substringBefore(': ').trim();
                    if (FieldName == 'Page Name') {
                        pagename = emailBody[i].substringAfter(': ').trim();
                    }
                    if (FieldName == 'Page URL') {
                       url = emailBody[i].substringAfter(': ').trim();
                    } 
                    if (FieldName == 'name') {
                        fullname = emailBody[i].substringAfter(': ').trim();
                    }
                    if (FieldName == 'email') {
                        emailaddress = emailBody[i].substringAfter(': ').trim();
                    } 
                    if (FieldName == 'phone_number') {
                        phonenumber = emailBody[i].substringAfter(': ').trim();
                    } 
                    if (FieldName == 'utm_source') {
                        utmsource = emailBody[i].substringAfter(': ').trim();
                    } 
                    if (FieldName == 'utm_medium') {
                        utmmedium = emailBody[i].substringAfter(': ').trim();
                    } 
                    if (FieldName == 'utm_content') {
                        utmcontent = emailBody[i].substringAfter(': ').trim();
                    } 
                    if (FieldName == 'utm_campaign') {
                        utmcampaign = emailBody[i].substringAfter(': ').trim();
                    }                        
                    if (FieldName == 'sf_campaign') {
                        sfcampaign = emailBody[i].substringAfter(': ').trim();
                    } 
                    if (FieldName == 'preferred_time_of_call') {
                        timetocall = emailBody[i].substringAfter(': ').trim();
                    } 
                    if (FieldName == 'ip_address') {
                        utmipaddress = emailBody[i].substringAfter(': ').trim();
                    }                             
                }
                
                //Check if Account exists, if not create new account.
                Account[] a = [select Id from Account WHERE PersonEmail = :emailaddress limit 1];
                
                if (a.size() == 0) {
                    Account acct =  new Account();
                    acct.LastName = fullname;
                    acct.PersonEmail = emailaddress;
                    acct.Phone = phonenumber;                    
                    insert acct;
                    type = 'New Business';
                }
                Account[] b = [select Id, Name from Account WHERE PersonEmail = :emailaddress limit 1];
                
               //Query the custom setting 'UnbounceRouting' and get the agent name from custom setting. This gives flexibility to change agents as required.
                List<UnbounceRouting__c> Ag  = [select Name, RowId__c from UnbounceRouting__c order by name asc];   
                                  
                // FInd the least loaded agent
                Integer maxcnt = 0;
                Integer nxtcnt = 0;
                Integer value = 0;
                String agentId = null;
                
                //Find the least loaded agent by iterating though all agents setup in custom setting 'UnbounceRouting'.
                for (UnbounceRouting__c c : Ag) { 
                    List<AggregateResult> results = [select count(Id) cnt from Opportunity where OwnerId = :c.RowId__c and StageName not in('Funds Received','Closed Won', 'Closed Lost', 'Closed No Contact', 'Closed Invalid Contact')];
                    value = (Integer)results[0].get('cnt');
                    if (c.Name == 'Agent1') {
                        maxcnt = value;
                        nxtcnt = value;
                    }
                    else if ( maxcnt < value ) {
                        maxcnt = value;
                    }
                    else if ( value < nxtcnt ) {
                        nxtcnt = value;
                        agentId = c.RowId__c;
                    }
                }
                
                // Create Opportunity.
                if (maxcnt > 0 && (b[0].Name != null)) {
                    Opportunity o = new Opportunity();
                    o.Name = b[0].Name+' : Hot Phone Call Opt In ('+pagename+')';
                    o.AccountID = b[0].Id;
                    o.CloseDate= System.Today() + 30; // Set this to current date + 30 days.
                    o.StageName= '30 Day Trial';  
                    o.Type = type; 
                    o.Description = email.plainTextBody;
                    o.Best_time_to_call__c = timetocall;
                    o.UTM_Source__c = utmsource;
                    o.UTM_Medium__c = utmmedium;
                    o.UTM_Content__c = utmcontent;
                    o.UTM_Campaign__c = utmcampaign;
                    o.CampaignId = sfcampaign;
                    o.UTM_URL__c = url;
                    if (agentId == null) {
                       o.OwnerId = '00590000004DqaY'; 
                    }
                    else {
                        o.OwnerId = agentId;
                    }
                                           
                    insert o;                        
               }
            }
        
        } catch(DmlException e) {
              throw e;
        }
        result.success = true;
        return result;
    }
}