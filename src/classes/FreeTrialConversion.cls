/**
 * FreeTrialConversion
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
public class FreeTrialConversion {
	
	public List<DataRow> table { get; set; }
	public DataRow total { get; set; }
	public List<String> dateLabels { get; set; }
	public List<Date> dateValues { get; set; }
	public Integer numberOfRows { get; set; }
	
	/**
	 *
	 */
	public FreeTrialConversion() {
		this.numberOfRows = 25;
		this.table = new List<DataRow>();
		getDateLabels(this.numberOfRows);
		
		buildDataTable(this.numberOfRows);
		getTotalData();
	}
	
	/**
	 *
	 */
	public void buildDataTable(Integer numberOfRows) {
		for (Integer i=0; i<numberOfRows-1; i++) {
			DataRow row = getRowData(i);
			this.table.add(row);
		}
	}
	
	private DataRow getRowData(Integer weekNumber) {
		SObject fts = [SELECT COUNT(Id) NumberOfRecords FROM Product_Instance__c WHERE Subscription_Type__c = 'f' AND Start_Date__c >= :dateValues[weekNumber+1] AND Start_Date__c < :dateValues[weekNumber]];
		List<Product_Instance__c> subs = [SELECT Person_Account__c, End_Date__c FROM Product_Instance__c WHERE Subscription_Type__c = 'f' AND Start_Date__c >= :dateValues[weekNumber] AND Start_Date__c < :dateValues[weekNumber]];
		List<Id> ids = new List<Id>();
		for (Product_Instance__c sub: subs) {
			ids.add(sub.Person_Account__c);
		}
		SObject freeTrials = [SELECT COUNT(Id) NumberOfRecords FROM Product_Instance__c WHERE Subscription_Type__c != 'f' AND Start_Date__c >= :dateValues[weekNumber] AND Person_Account__c IN :ids];
		DataRow row = new DataRow(dateLabels[weekNumber], (Integer)fts.get('NumberOfRecords'), (Integer)freeTrials.get('NumberOfRecords'));
		return row;
	}
	
	/**
	 * 
	 */
	 /*
	public DataRow getRowData(Integer weekNumber) {
		Integer fts = 0;
		Integer subs = 0;
		List<SObject> results = [SELECT Auto_Renewal__c, COUNT(Id) NumberOfRecords FROM Product_Instance__c WHERE Start_Date__c >= :dateValues[weekNumber+1] AND Start_Date__c < :dateValues[weekNumber] GROUP BY Auto_Renewal__c];
		for (SObject result : results) {
			System.debug(result);
			if (Boolean.valueOf(result.get('Auto_Renewal__c')) == true) {
				subs += Integer.valueOf(result.get('NumberOfRecords'));
			} else {
				fts += Integer.valueOf(result.get('NumberOfRecords'));
			}
		}
		fts += subs;
		DataRow row = new DataRow(dateLabels[weekNumber], fts, subs);
		return row;
	}
	*/
	
	/**
	 * calculates the total for each year
	 */
	public void getTotalData() {
		total = new DataRow('Total', 0, 0);
		
		for (DataRow row : table) {
			total.fts += row.fts;
			total.subs += row.subs;
		}
		// calculate the free trial to subs conversion value
		total.calculateConversion();
	}
	
	/**
	 *
	 */
	public void getDateLabels(Integer numberOfRows) {
		dateLabels = new List<String>();
		dateValues = new List<Date>();
		
		Date startOfWeek = Date.today().toStartOfWeek();
		dateLabels.add(startOfWeek.format());
		
		// repeat the following statement for each week in a year
		for (Integer i=0; i<numberOfRows; i++) {
			// subtract a week from the current start of the week
			startOfWeek = startOfWeek.addDays(-7);
			dateValues.add(startOfWeek);
			// add string label to list
			dateLabels.add(startOfWeek.format());
		}
	}
	
	
	/****************************************************************/
	/**
	 * Repres
	 */
	public class DataRow {
		
		public String label { get; set; }
		public Integer fts { get; set; }
		public Integer subs { get; set; }
		public Decimal conversion { get; set; }
		
		public dataRow(String label, Integer fts, Integer subs) {
			this.label = label;
			this.fts = fts;
			this.subs = subs;
			
			calculateConversion();
		}
		
		public void calculateConversion() {
			if (this.fts != 0) {
				this.conversion = Integer.valueOf((Decimal.valueOf(this.subs)/Decimal.valueOf(this.fts)) * 100);
			} else {
				this.conversion = 0;
			}
		}
	}
}