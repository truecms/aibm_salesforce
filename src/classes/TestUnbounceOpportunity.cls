/**
 *
 * Test Class for Unbounce Integration with Salesforce for creating opportunities using Email Services.
 * Created on 6-11-2015
 * Author: Mithun Roy
 * Copyright: EurekaReport.com.au
 */
@isTest(SeeAllData=true)
public class TestUnbounceOpportunity {
@isTest
static void testEmail() {
    // create a new email and envelope object
    Messaging.InboundEmail email = new Messaging.InboundEmail() ;
    Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

    // setup the data for the email
    email.subject = 'Create Unbounce Opportunity';
    email.fromname = 'Xyz Abc';
    env.fromAddress = 'notifications@unbounce.com';
    email.plainTextBody = 'A new lead has been captured from your Unbounce page...'+'\n'+'\n'+
                                 'Page Name: Mithun test'+'\n'+
                                 'Page URL: http://er.eurekareport.com.au/mithun-test'+'\n'+
                                 'Variant: A'+'\n'+'\n'+
                                 '======================================================'+'\n'+'\n'+            
                                 'Here is the data submitted from your form:'+'\n'+'\n'+
                                 'name : MITHUN ROY'+'\n'+
                                 'email : abc@eurekareport.com.au'+'\n'+
                                 'phone_number:'+'\n'+ 
                                 'utm_source : test-source'+'\n'+
                                 'utm_medium : test-medium'+'\n'+
                                 'utm_content : test-content'+'\n'+
                                 'utm_campaign : test campaign'+'\n'+
                                 'preferred_time_of_call : Morning'+'\n'+
                                 'ip_address : 125.7.8.194'+'\n'+'\n';    

    // call the email service class and test it with the data in the testMethod
    CreateUnbounceOpportunity emailProcess = new CreateUnbounceOpportunity();
    emailProcess.handleInboundEmail(email, env);

    // query for the contact the email service created
    Opportunity Opp = [select id, Name,UTM_Source__c,UTM_Medium__c,UTM_Content__c,UTM_Campaign__c,Best_time_to_call__c,UTM_URL__c from Opportunity
    where CreatedDate = TODAY and StageName= '30 Day Trial' and Name = 'Mithun Roy : Hot Phone Call Opt In (Mithun test)'];
    //Assert the values
    System.assertEquals(Opp.UTM_Source__c,'test-source');
    System.assertEquals(Opp.UTM_Medium__c,'test-medium');
    System.assertEquals(Opp.UTM_Content__c,'test-content');
    System.assertEquals(Opp.UTM_Campaign__c,'test campaign');
    System.assertEquals(Opp.Best_time_to_call__c,'Morning');
    System.assertEquals(Opp.UTM_URL__c,'http://er.eurekareport.com.au/mithun-test');
  
  }
}