/**
* Test Case: unit testing the Task after insert trigger
* Created By: tailbyr
* Copyright: Eureka Report
*/
@IsTest
public with sharing class TestTaskTrigger 
{
	public static Insurance_Quote__c createInsuranceQuote(Account account){
		Insurance_Quote__c insQuote = new Insurance_Quote__c();
		
		insQuote.Account__c = account.Id;
		insQuote.Name = 'Test001';
		insQuote.Status__c = 'In Progress';
		
		return insQuote;
	}
	
	public static TestMethod void testInsertInboundEmailTaskOnQuote(){
		List<Account> accounts = TestAccountHelper.createAccounts(1);
		Account acc = accounts[0];
		
		Insurance_Quote__c quote = createInsuranceQuote(acc);
		insert quote;
		
		//Initially check that there is no activity direction
		Insurance_Quote__c preTask = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id = :quote.Id LIMIT 1];
		System.assertEquals(preTask.Last_Activity_Direction__c, null);
		
		Test.startTest();
		Task t = new Task();
		t.Subject = 'Email: Unit Test 1';
		t.Status = 'Completed';
		t.Description = 'this is test content from emailtosalesforce@1234-4321.com';
		t.WhatId = quote.Id;
		insert t;
		Test.stopTest();
		
		Insurance_Quote__c result = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id = :quote.Id LIMIT 1];
		System.assertEquals(result.Last_Activity_Direction__c, 'inbound');
	}
	
	public static TestMethod void testUpdateInboundEmailAssignmentTaskOnQuote(){
		List<Account> accounts = TestAccountHelper.createAccounts(1);
		Account acc = accounts[0];
		
		Insurance_Quote__c quote = createInsuranceQuote(acc);
		insert quote;

		//Create the task but do not assign it to the Quote - this is the test
		Task t = new Task();
		t.Subject = 'Email: Unit Test 2';
		t.Status = 'Completed';
		t.Description = 'this is test content from emailtosalesforce@1234-4321.com';
		insert t;
		
		//Initially check that there is no activity direction
		Insurance_Quote__c preTask = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id = :quote.Id LIMIT 1];
		System.assertEquals(preTask.Last_Activity_Direction__c, null);
		
		Test.startTest();
		//Now update the task and assign it to the Quote
		t.WhatId = quote.Id;
		update t;
		Test.stopTest();
		
		Insurance_Quote__c result = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id = :quote.Id LIMIT 1];
		System.assertEquals(result.Last_Activity_Direction__c, 'inbound');
	}
	
	public static TestMethod void testUpdateInboundEmailNotAssignTask(){
		List<Account> accounts = TestAccountHelper.createAccounts(1);
		Account acc = accounts[0];
		
		Insurance_Quote__c quote = createInsuranceQuote(acc);
		insert quote;

		//Create the task but do not assign it to the Quote - this is the test
		Task t = new Task();
		t.Subject = 'Email: Unit Test 3';
		t.Status = 'Completed';
		t.Description = 'this is test content from emailtosalesforce@1234-4321.com';
		insert t;
		
		//Initially check that there is no activity direction
		Insurance_Quote__c preTask = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id = :quote.Id LIMIT 1];
		System.assertEquals(preTask.Last_Activity_Direction__c, null);
		
		Test.startTest();
		//Update the Quote, but do not change the assignment
		t.Subject = 'Email: updating unit test 3';
		update t;
		Test.stopTest();
		
		Insurance_Quote__c result = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id = :quote.Id LIMIT 1];
		System.assertEquals(result.Last_Activity_Direction__c, null);
	}
	
	public static TestMethod void testInsertOutboundEmailTask(){
		List<Account> accounts = TestAccountHelper.createAccounts(1);
		Account acc = accounts[0];
		
		Insurance_Quote__c quote = createInsuranceQuote(acc);
		insert quote;
		
		//Initially check that there is no activity direction
		Insurance_Quote__c preTask = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id = :quote.Id LIMIT 1];
		System.assertEquals(preTask.Last_Activity_Direction__c, null);
		
		Test.startTest();
		Task t = new Task();
		t.Subject = 'Email: Unit Test 4';
		t.Status = 'Completed';
		t.Description = 'Sending email from Salesforce Sales User out to Customer';
		t.WhatId = quote.Id;
		insert t;
		Test.stopTest();
		
		Insurance_Quote__c result = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id = :quote.Id LIMIT 1];
		System.assertEquals(result.Last_Activity_Direction__c, 'outbound');
	}
	
	public static TestMethod void testUpdateOutboundEmailTask(){
		List<Account> accounts = TestAccountHelper.createAccounts(1);
		Account acc = accounts[0];
		
		Insurance_Quote__c quote = createInsuranceQuote(acc);
		insert quote;

		//Create the task but do not assign it to the Quote - this is the test
		Task t = new Task();
		t.Subject = 'Email: Unit Test 5';
		t.Status = 'Completed';
		t.Description = 'Sending email from Salesforce Sales User out to Customer';
		insert t;
		
		//Initially check that there is no activity direction
		Insurance_Quote__c preTask = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id = :quote.Id LIMIT 1];
		System.assertEquals(preTask.Last_Activity_Direction__c, null);
		
		Test.startTest();
		//Now update the task and assign it to the Quote
		t.WhatId = quote.Id;
		update t;
		Test.stopTest();
		
		Insurance_Quote__c result = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id = :quote.Id LIMIT 1];
		System.assertEquals(result.Last_Activity_Direction__c, 'outbound');
	}
	
	public static TestMethod void testInsertNonEmailTask(){
		List<Account> accounts = TestAccountHelper.createAccounts(1);
		Account acc = accounts[0];
		
		Insurance_Quote__c quote = createInsuranceQuote(acc);
		insert quote;
		
		//Initially check that there is no activity direction
		Insurance_Quote__c preTask = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id = :quote.Id LIMIT 1];
		System.assertEquals(preTask.Last_Activity_Direction__c, null);
		
		Test.startTest();
		//Create a new Task against the Quote that is not labelled as being an email
		Task t = new Task();
		t.Subject = 'Standard Sales Activity';
		t.Status = 'Completed';
		t.Description = 'Looks like an email, but is not...';
		t.WhatId = quote.Id;
		insert t;
		Test.stopTest();
		
		Insurance_Quote__c result = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id = :quote.Id LIMIT 1];
		System.assertEquals(result.Last_Activity_Direction__c, null);
	}
	
	public static TestMethod void testInsertTaskNotAssociatedToQuote(){
		List<Account> accounts = TestAccountHelper.createAccounts(1);
		Account acc = accounts[0];
		
		Insurance_Quote__c quote = createInsuranceQuote(acc);
		insert quote;
		
		//Initially check that there is no activity direction
		Insurance_Quote__c preTask = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id = :quote.Id LIMIT 1];
		System.assertEquals(preTask.Last_Activity_Direction__c, null);
		
		Test.startTest();
		//Create a task but attach it to the Account rather than the Quote - check there is no activity direction to that Quote
		Task t = new Task();
		t.Subject = 'Email: Unit Test 6';
		t.Status = 'Completed';
		t.Description = 'this is test content from emailtosalesforce@1234-4321.com';
		t.WhatId = acc.Id;
		insert t;
		Test.stopTest();
		
		Insurance_Quote__c result = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id = :quote.Id LIMIT 1];
		System.assertEquals(result.Last_Activity_Direction__c, null);
	}
	
	public static TestMethod void testInsertTaskOnCompleteQuote(){
		List<Account> accounts = TestAccountHelper.createAccounts(1);
		Account acc = accounts[0];
		
		Insurance_Quote__c quote = createInsuranceQuote(acc);
		quote.Status__c = 'Quote Accepted by Client'; //Update the Quote to set its status to Accepted
		insert quote;
		
		//Initially check that there is no activity direction
		Insurance_Quote__c preTask = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id = :quote.Id LIMIT 1];
		System.assertEquals(preTask.Last_Activity_Direction__c, null);
		
		Test.startTest();
		Task t = new Task();
		t.Subject = 'Email: Unit Test 7';
		t.Status = 'Completed';
		t.Description = 'this is test content from emailtosalesforce@1234-4321.com';
		t.WhatId = quote.Id;
		insert t;
		Test.stopTest();
		
		//The direction should still be updated on a complete quote, however the workflow will not trigger
		Insurance_Quote__c result = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id = :quote.Id LIMIT 1];
		System.assertEquals(result.Last_Activity_Direction__c, 'inbound');
	}
	
	public static TestMethod void testMultipleInsertsOnOneQuote(){
		List<Account> accounts = TestAccountHelper.createAccounts(1);
		Account acc = accounts[0];
		
		Insurance_Quote__c quote = createInsuranceQuote(acc);
		insert quote;
		
		//Initially check that there is no activity direction
		Insurance_Quote__c preTask = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id = :quote.Id LIMIT 1];
		System.assertEquals(preTask.Last_Activity_Direction__c, null);
		
		Test.startTest();
		List<Task> tasks = new List<Task>();
		
		Task t = new Task();
		t.Subject = 'Email: Unit Test 8';
		t.Status = 'Completed';
		t.Description = 'this is test content from emailtosalesforce@1234-4321.com';
		t.WhatId = quote.Id;
		tasks.add(t);
		
		Task t2 = new Task();
		t2.Subject = 'Email: Unit Test 8 pt 2';
		t2.Status = 'Completed';
		t2.Description = 'this is additional test content from emailtosalesforce@1234-4321.com';
		t2.WhatId = quote.Id;
		tasks.add(t2);
		
		insert tasks;
		Test.stopTest();
		
		Insurance_Quote__c result = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id = :quote.Id LIMIT 1];
		System.assertEquals(result.Last_Activity_Direction__c, 'inbound');
	}
	
	public static TestMethod void testMultipleInsertsOnMultipleQuotes(){
		List<Account> accounts = TestAccountHelper.createAccounts(1);
		Account acc = accounts[0];
		
		List<Insurance_Quote__c> quotes = new List<Insurance_Quote__c>();
		Insurance_Quote__c quote1 = createInsuranceQuote(acc);
		quotes.add(quote1);
		
		Insurance_Quote__c quote2 = createInsuranceQuote(acc);
		quotes.add(quote2);
		insert quotes;
		
		//Initially check that there is no activity direction
		List<Insurance_Quote__c> preTasks = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c];
		for (Insurance_Quote__c quote : preTasks)
			System.assertEquals(quote.Last_Activity_Direction__c, null);
		
		Test.startTest();
		List<Task> tasks = new List<Task>();
		
		Task t = new Task();
		t.Subject = 'Email: Unit Test 9';
		t.Status = 'Completed';
		t.Description = 'this is test content from emailtosalesforce@1234-4321.com';
		t.WhatId = quote1.Id;
		tasks.add(t);
		
		Task t2 = new Task();
		t2.Subject = 'Email: Unit Test 9 pt 2';
		t2.Status = 'Completed';
		t2.Description = 'Outbound message to the client Account2';
		t2.WhatId = quote2.Id;
		tasks.add(t2);
		
		insert tasks;
		Test.stopTest();
		
		Insurance_Quote__c result1 = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id = :quote1.Id LIMIT 1];
		System.assertEquals(result1.Last_Activity_Direction__c, 'inbound');
		
		Insurance_Quote__c result2 = [SELECT Last_Activity_Direction__c FROM Insurance_Quote__c WHERE Id = :quote2.Id LIMIT 1];
		System.assertEquals(result2.Last_Activity_Direction__c, 'outbound');
	}
}