/**
* Batch job populated and triggered from the after insert/update trigger on Account, where the Account no longer fulfils the criteria for 
* being eligable for Premium Client status. 
* A task for the Sales Team to follow up will be created for each of these customers where the salesperson can decide whether the customer
* gets to keep their status or will be downgraded
* 
* Author: tailbyr
* Copyright: EurekaReport.com.au
*/
public with sharing class BDRoleChangeTasks  implements Database.Batchable<Account>, Database.Stateful {
	
	public static String lastSalesTeamMember { get; set; }
	public TaskManager tm {get; set;}
	public String subject {get; set;}
	
	public String roleChangeFollowUpQueue {get; set;}
    public Boolean roleChangeFollowUpRules {get; set;}
    
    public List<Account> changedAccounts {get; set;}
	
	public BDRoleChangeTasks(List<Account> accs, String subject){
		this.changedAccounts = accs;
		this.subject = subject;
		
		lastSalesTeamMember = AccountBDHelper.getLastTaskOwner(this.subject);
		
		Allocation_Rules__c allocationRules = Allocation_Rules__c.getInstance('Default');
		this.roleChangeFollowUpQueue = allocationRules.Role_Change_Queue__c;
		this.roleChangeFollowUpRules = allocationRules.Role_Change_Allocation_Rule__c;
		
		tm = new TaskManager(this.roleChangeFollowUpQueue);
	}
	
	//Batchable override method: start - queries the records to iterate over
    public Iterable<Account> start(Database.batchableContext BC) {
    	return this.changedAccounts;
    }
		
	//Batchable override method: execute
    public void execute(Database.BatchableContext BC, List<Account> instances) {
        AccountBDHelper accHelper = new AccountBDHelper(instances);
        accHelper.createBrightDayTasks(roleChangeFollowUpRules, tm, this.subject);
    }
    
    //Batchable override method: finish - called when all batches are processed; allocates all fair tasks created in a fair manner across Acc mgrs
    public void finish(Database.BatchableContext BC) {
        
        AccountBDHelper.fairlyAllocateTasks(this.subject, this.tm, lastSalesTeamMember);
    }
}