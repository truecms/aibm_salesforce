public class MonitorBatchJobs {

	//Holds the list of currently active batch jobs running
	public List<AsyncApexJob> jobs { get; set; }
    
    //Holds the ID of the batch job that the user wants to abort
    public String abortJob { get; set; }

	public MonitorBatchJobs(){
		this.refresh();	
	}

    public PageReference abortRun() {
    	System.debug('DEBUG: We are aborting job with ID:' + abortJob);
    	System.abortJob(abortJob);
    	this.refresh();
        return null;
    }

	//Load the AsyncApexJob collection
    public PageReference refresh() {
    	List<AsyncApexJob> batchJobs = [SELECT Id, CreatedDate, ApexClass.Name, status, 
    				TotalJobItems, JobItemsProcessed, NumberOfErrors, CreatedBy.Name, CompletedDate 
    				FROM AsyncApexJob 
    				WHERE ApexClass.Name IN ('ImportNewCustomersController', 'CreditCardExpiresBatch', 'AutoRenewalSubscription')
    				ORDER BY CreatedDate DESC];
    	this.jobs = batchJobs;
    	System.debug('DEBUG: Number of jobs returned:' + batchJobs.size());
        return null;
    }

}