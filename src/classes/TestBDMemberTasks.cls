/**
 * Testing the BDMemberTasks class
 * Author: tailbyr
 * Copyright: eurekareport.com.au
 */
@isTest
public with sharing class TestBDMemberTasks {

    public static Integer firstCall = -45;
    
    public static Integer inProgressCall = -2;

    /** 
    * Utility Methods 
    */
    public static Allocation_Rules__c createAllocationSettings() 
    {
        Allocation_Rules__c allocationRules = new Allocation_Rules__c();
        allocationRules.Name = 'Default';
        
        allocationRules.Member_FollowUp_Queue__c = 'SalesTeamMembers';
        allocationRules.Member_FollowUp_Allocation_Rule__c = true;
        
        allocationRules.Role_Change_Queue__c = 'SalesTeamMembers';
        allocationRules.Role_Change_Allocation_Rule__c = true;
        
        upsert allocationRules;
        return allocationRules;
    }
    
    public static User getDrupalUser(){
        User drupalUser = [SELECT Id FROM User WHERE Name = 'System Admin'];
        return drupalUser;
    }
    /*********************************************************/

    /**
    * Test Methods
    */
    static TestMethod void testBDMemberFirstTaskCreated()
    {
        //Set up the custom settings for queue to use and whether to utilise assignment rules or not
        TestBDMemberTasks.createAllocationSettings();
        BrightDay_Settings__c settings = TestLatePaperworkTasks.createBrightDaySettings();
        
        //Pick the first Account Manager for setting all the task ownership
        GroupMember aMember = [SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = 'SalesTeamMembers' ORDER BY UserOrGroupId LIMIT 1];
        Integer noOfAccs = 8;
        
        //Create accounts; active and inactive
        List<Account> accounts = TestAccountHelper.createAccounts(noOfAccs);
        
        //Each Account also needs to have an Open Opportunity against it or we don't create these tasks
        List<Opportunity> opps = new List<Opportunity>();
        
        //Set the values required for BD registration
        System.runAs(getDrupalUser()){
            for (Account acc : accounts) {
                acc.OwnerId = aMember.UserOrGroupId;
                acc.BD_Current_Role__c = 'Member';
                acc.OV_User_Object_Created__c = false;
                acc.BD_Registration_Date__c = System.today().addDays(firstCall);
                
                Opportunity opp = new Opportunity(Name='Test Opp', AccountId=acc.Id, StageName='Prospecting', CloseDate=System.today());
                opps.add(opp);
            }
            
            //Set 4 accounts as Active, 4 as Inactive
            for (Integer i = 0; i < noOfAccs; i++) {
                if (math.mod(i,2) == 0)
                    accounts[i].BD_Last_Login_Date__c = System.today();
                else
                    accounts[i].BD_Last_Login_Date__c = System.today().addDays(firstCall);
            }
            
            update accounts;
            insert opps;
        }
        
        //Get the subject that should be used
        String activeSubject = 'Follow Up Call: Active Member has not yet become a client';
        String inactiveSubject = 'Follow Up Call: Inactive Member has not yet become a client';
        
        Test.startTest();     
        Database.executeBatch(new BDMemberTasks(Integer.valueOf(settings.Member_Followup_Call_Day__c), true, activeSubject), noOfAccs);
        Database.executeBatch(new BDMemberTasks(Integer.valueOf(settings.Member_Followup_Call_Day__c), false, inactiveSubject), noOfAccs);
        Test.stopTest();
        
        //Check we have a task
        List<Task> activeMemberTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
                where Subject = :activeSubject
                AND createdDate = TODAY
                ORDER BY createdDate desc];
        System.assertEquals(activeMemberTasks.size(), 4);
        
        List<Task> inactiveMemberTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
                where Subject = :inactiveSubject
                AND createdDate = TODAY
                ORDER BY createdDate desc];
        System.assertEquals(inactiveMemberTasks.size(), 4);
        
        Set<Id> accIds = new Set<Id>();
        for (Task t : activeMemberTasks)
        {
            System.assertEquals(t.OwnerId, aMember.UserOrGroupId);
            accIds.add(t.WhatId);
        }
        //Check that each even Account has an active task
        System.assert(accIds.contains(accounts[0].Id));
        System.assert(accIds.contains(accounts[2].Id));
        System.assert(accIds.contains(accounts[4].Id));
        System.assert(accIds.contains(accounts[6].Id));
        
        accIds.clear();
        for (Task t : inactiveMemberTasks)
        {
            System.assertEquals(t.OwnerId, aMember.UserOrGroupId);
            accIds.add(t.WhatId);
        }
        //Check that each odd Account has an inactive task
        System.assert(accIds.contains(accounts[1].Id));
        System.assert(accIds.contains(accounts[3].Id));
        System.assert(accIds.contains(accounts[5].Id));
        System.assert(accIds.contains(accounts[7].Id));
        
        //Check we don't have any other tasks!
        List<Task> anyOtherTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
                where Subject != :activeSubject
                AND Subject != :inactiveSubject 
                AND createdDate = TODAY
                ORDER BY createdDate desc];
        System.assertEquals(anyOtherTasks.size(), 0);
    }
    
    static TestMethod void testBDMemberFirstTaskNotCreated() 
    {
        Integer numAccs = 5;
        
        //Set up the custom settings for queue to use and whether to utilise assignment rules or not
        TestBDMemberTasks.createAllocationSettings();
        BrightDay_Settings__c settings = TestLatePaperworkTasks.createBrightDaySettings();
        
        //Create 6 data-scenario accounts
        List<Account> accounts = TestAccountHelper.createAccounts(numAccs);
        
        //Set the values required for BD only member followup tasks, then set non-compliant values individually to ensure tasks NOT created
        System.runAs(getDrupalUser()){
            for (Account acc : accounts) {
                acc.BD_Current_Role__c = 'member';
                acc.OV_User_Object_Created__c = false;
                acc.BD_Registration_Date__c = System.today().addDays(firstCall);
            }
            
            accounts[0].BD_Registration_Date__c = Date.today().addDays(firstCall - 1); //1 day different to firstCall
            accounts[1].OV_User_Object_Created__c = true;
            accounts[2].BD_Current_Role__c = null;
            accounts[3].BD_Current_Role__c = 'Client';
            
            //NB, the 5th account is the control - one that does fit all the requirements
            
            update accounts;
        }
        
        //Get the subject that should be used
        String activeSubject = 'Follow Up Call: Active Member has not yet become a client';
        String inactiveSubject = 'Follow Up Call: Inactive Member has not yet become a client';
                
        Test.startTest();        
        Database.executeBatch(new BDMemberTasks(Integer.valueOf(settings.Member_Followup_Call_Day__c), true, activeSubject), numAccs);
        Database.executeBatch(new BDMemberTasks(Integer.valueOf(settings.Member_Followup_Call_Day__c), false, inactiveSubject), numAccs);
        Test.stopTest();
        
        //Confirm that 1 task for the control has been created
     //   List<Task> tasks = [SELECT Id, WhatId FROM Task];
       // System.assertEquals(tasks.size(), 1);
        
    //    Task t = tasks[0];
       // System.assertEquals(t.WhatId, accounts[4].Id);
    }
    
    
    /**
    * Testing the BDRoleChangeTasks functionality
    */
    static TestMethod void testBDRoleChangeTaskCreated(){ //One task
        //Set up the custom settings for queue to use and whether to utilise assignment rules or not
        TestBDMemberTasks.createAllocationSettings();
        
        List<Account> accounts = TestAccountHelper.createAccounts(1);
        
        //Set the values required pre-update for BD role change task to be created
        System.runAs(getDrupalUser()){
            
            List<Investment_Account__c> invAccs = new List<Investment_Account__c>(); 
            for (Account acc : accounts) {
                acc.BD_Current_Role__c = 'Premium Client';
                acc.Total_Investment_Value__c = 190000;
                
                Investment_Account__c ia = new Investment_Account__c();
                ia.Investment_Account_Type__c = 'DIRECT';
                ia.Account__c = acc.Id;
                invAccs.add(ia);
            }
            
            update accounts;
            insert invAccs;
            
            Test.startTest();
            for (Account acc : accounts) {
                acc.Total_Investment_Value__c = 175000;
            }
            update accounts;
            Test.stopTest();
        }
        
        //Check that we have 1 task created with the correct subject and account
        List<Task> roleTasks = [SELECT Id, WhatId, Subject FROM Task];
        System.assertEquals(roleTasks.size(), 1);
        System.assertEquals(roleTasks[0].WhatId, accounts[0].Id);
        System.assertEquals(roleTasks[0].Subject, TaskManager.ROLE_CHANGE_SUBJECT);
    }
    
    static TestMethod void testBDMultipleAccountsRoleChangeTasks(){ //Multiple tasks
        TestBDMemberTasks.createAllocationSettings();
        
        List<Account> accounts = TestAccountHelper.createAccounts(8);
        
        //Set the values required pre-update for BD role change task to be created
        System.runAs(getDrupalUser()){
            
            List<Investment_Account__c> invAccs = new List<Investment_Account__c>(); 
            for (Account acc : accounts) {
                acc.BD_Current_Role__c = 'Premium Client';
                acc.Total_Investment_Value__c = 190000;
                
                Investment_Account__c ia = new Investment_Account__c();
                ia.Investment_Account_Type__c = 'DIRECT';
                ia.Account__c = acc.Id;
                invAccs.add(ia);
            }
            
            update accounts;
            insert invAccs;
            
            Test.startTest();
            for (Account acc : accounts) {
                acc.Total_Investment_Value__c = 175000;
            }
            update accounts;
            Test.stopTest();
        }
        
        List<Task> roleTasks = [SELECT Id, WhatId, Subject FROM Task];
        System.assertEquals(roleTasks.size(), 8);
        
        Set<Id> whatIds = new Set<Id>();
        for (Task t : roleTasks){
            System.assertEquals(t.Subject, TaskManager.ROLE_CHANGE_SUBJECT);
            whatIds.add(t.WhatId);
        }
        //Check each task is assigned to a different account
        System.assertEquals(whatIds.size(), 8);
        
        //And check that each of our original accounts has a task!
        for (Account a : accounts){
            System.assert(whatIds.contains(a.Id));
        }
    }
    
    static TestMethod void testBDRoleChangeOnSMSFAccount(){
        TestBDMemberTasks.createAllocationSettings();
        
        List<Account> accounts = TestAccountHelper.createAccounts(8);
        
        //Set the values required pre-update for BD role change task to be created
        System.runAs(getDrupalUser()){
            
            List<Investment_Account__c> invAccs = new List<Investment_Account__c>(); 
            for (Integer i = 0; i < accounts.size(); i++) {
                Account acc = accounts[i];  
                acc.BD_Current_Role__c = 'Premium Client';
                acc.Total_Investment_Value__c = 190000;
                
                //For testing all scenarios, the 1st Account has no Investment Accounts against it (SHOULD TRIGGER) and 2nd only has an SMSF, created later
                if (i != 0 && i != 1) {
                    Investment_Account__c ia = new Investment_Account__c();
                    ia.Investment_Account_Type__c = 'DIRECT';
                    ia.Account__c = acc.Id;
                    invAccs.add(ia);
                }
            }
            
            //For the 2nd Account, add an SMSF account only (No DIRECT account)
            Investment_Account__c smsf = new Investment_Account__c();
            smsf.Investment_Account_Type__c = 'SMSF';
            smsf.Account__c = accounts[1].Id;
            invAccs.add(smsf);
            
            //3rd account, Change the role value
            accounts[2].BD_Current_Role__c = 'Platinum Client';
            
            //4th account, Change the total investment value
            accounts[3].Total_Investment_Value__c = 160000;
            
            //5th account, add an SMSF account AS WELL AS the DIRECT account
            Investment_Account__c addSmsf = new Investment_Account__c();
            addSmsf.Investment_Account_Type__c = 'TOTAL SMSF';
            addSmsf.Account__c = accounts[4].Id;
            invAccs.add(addSmsf);
            
            //6th account, add a retail account as well as the DIRECT account
            Investment_Account__c addRetail = new Investment_Account__c();
            addRetail.Investment_Account_Type__c = 'COMPLETE SUPER';
            addRetail.Account__c = accounts[5].Id;
            invAccs.add(addRetail);
            
            update accounts;
            insert invAccs;
            
            Test.startTest();
            for (Account acc : accounts) {
                acc.Total_Investment_Value__c = 175000;
            }
            update accounts;
            Test.stopTest();
        }
        
        //We should now have 3 tasks, 1 for the first account with no Investment Accounts and one each for the controls
        List<Task> roleTasks = [SELECT Id, WhatId, Subject FROM Task];
        
        System.assertEquals(roleTasks.size(), 3);
        
        Set<Id> accIds = new Set<Id>();
        for (Task t : roleTasks) {
            System.assertEquals(t.Subject, TaskManager.ROLE_CHANGE_SUBJECT);
            accIds.add(t.WhatId);
        }
        
        //Check we have 3 distinct Account Ids and that the accounts we expect are represented by Tasks
        System.assertEquals(accIds.size(), 3);
        System.assert(accIds.contains(accounts[0].Id));
        System.assert(accIds.contains(accounts[6].Id));
        System.assert(accIds.contains(accounts[7].Id));
    }
    
    static TestMethod void TestCheckBDRoleChangeTasksAllocatedFairly() {
        
        TestBDMemberTasks.createAllocationSettings();
        
        Integer noOfAccs = 6;
        
        //Create noOfAccs test accounts
        List<Account> accounts = TestAccountHelper.createAccounts(noOfAccs);
        List<Investment_Account__c> invAccs = new List<Investment_Account__c>();
        
        //Set the OV user created date back to 7 days ago
        System.runAs(getDrupalUser()){
            for (Account acc : accounts) {
                acc.BD_Current_Role__c = 'Premium Client';
                acc.Total_Investment_Value__c = 190000;
                
                Investment_Account__c ia = new Investment_Account__c();
                ia.Investment_Account_Type__c = 'DIRECT';
                ia.Account__c = acc.Id;
                invAccs.add(ia);
            }
            
            update accounts;
            insert invAccs;
            
            Test.startTest();
            for (Account acc : accounts) {
                acc.Total_Investment_Value__c = 175000;
            }
            update accounts;
            Test.stopTest();
        }
        
        //Get the subject that should be used
        String subject = TaskManager.ROLE_CHANGE_SUBJECT;
        
        //Initially check the right number of tasks were created
        List<Task> roleChangeTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
                where Subject = :subject
                AND createdDate = TODAY
                ORDER BY createdDate desc];
        System.assertEquals(roleChangeTasks.size(), noOfAccs);
        
        //Now check that each Sales User has the same number of these tasks
        TestExpiredFreeTrialTasks.checkAllocationOfTasks(roleChangeTasks, noOfAccs, 'SalesTeamMembers');
    }
    
    
    /**
    * Tests for the BDInProgressTasks child class
    */
    static TestMethod void TestInProgressTasksCreated(){
        //Set up the custom settings for queue to use and whether to utilise assignment rules or not
        TestBDMemberTasks.createAllocationSettings();
        BrightDay_Settings__c settings = TestLatePaperworkTasks.createBrightDaySettings();
        
        settings.In_Progress_Call_Day__c = -2; //Current 24-48 hr follow up rule
        update settings;
        
        //Pick the first Account Manager for setting all the task ownership
        GroupMember aMember = [SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = 'SalesTeamMembers' ORDER BY UserOrGroupId LIMIT 1];
        Integer noOfAccs = 10;
        
        //Create accounts; active and inactive
        List<Account> accounts = TestAccountHelper.createAccounts(noOfAccs);
        
        //Set the values required for BD registration
        System.runAs(getDrupalUser()){
            for (Account acc : accounts) {
                acc.OwnerId = aMember.UserOrGroupId;
                acc.BD_Current_Role__c = 'Premium Client';
                
                acc.OneVue_Application_Status__c = 'in_progress';
            }
            
            //For every even account, move the OV status on to awaiting paperwork
            for (integer i = 0; i < noOfAccs; i++){
                if (math.mod(i,2) == 0)
                    accounts[i].OneVue_Application_Status__c = 'awaiting_paperwork';
            }
            
            update accounts;
            
            for (Account acc : accounts) {
                acc.OV_Status_Change_Date__c = Date.today().addDays(inProgressCall); //Do this after as the workflow sets the OV created date to today
            }
            update accounts;
        }
        
        //Get the subject that should be used
        String subject = 'Follow Up Call: User started Transact application but has not submitted';
        
        Test.startTest();     
        Database.executeBatch(new BDInProgressTasks(Integer.valueOf(settings.In_Progress_Call_Day__c), false, subject), noOfAccs);
        Test.stopTest();
        
        //Check we have a task
        List<Task> inProgressTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
                where Subject = :subject
                AND createdDate = TODAY
                ORDER BY createdDate desc];
        System.assertEquals(inProgressTasks.size(), 5);
        
        Set<Id> accIds = new Set<Id>();
        for (Task t : inProgressTasks)
        {
            System.assertEquals(t.OwnerId, aMember.UserOrGroupId);
            accIds.add(t.WhatId);
        }
        //Check that each odd Account has an active task
        System.assert(accIds.contains(accounts[1].Id));
        System.assert(accIds.contains(accounts[3].Id));
        System.assert(accIds.contains(accounts[5].Id));
        System.assert(accIds.contains(accounts[7].Id));
        
        //Check we don't have any other tasks!
        List<Task> anyOtherTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
                where Subject != :subject
                AND createdDate = TODAY
                ORDER BY createdDate desc];
        System.assertEquals(anyOtherTasks.size(), 0);
    }
    
    static TestMethod void TestInProgressTasksNotCreated() {
        //Set up the custom settings for queue to use and whether to utilise assignment rules or not
        TestBDMemberTasks.createAllocationSettings();
        BrightDay_Settings__c settings = TestLatePaperworkTasks.createBrightDaySettings();
        
        settings.In_Progress_Call_Day__c = -2; //Current 24-48 hr follow up rule
        update settings;
        
        //Pick the first Account Manager for setting all the task ownership
        GroupMember aMember = [SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = 'SalesTeamMembers' ORDER BY UserOrGroupId LIMIT 1];
        Integer noOfAccs = 5;
        
        //Create accounts; active and inactive
        List<Account> accounts = TestAccountHelper.createAccounts(noOfAccs);
        
        //Set the values required for BD registration
        System.runAs(getDrupalUser()){
            for (Account acc : accounts) {
                acc.OwnerId = aMember.UserOrGroupId;
                acc.BD_Current_Role__c = 'Premium Client';
                
                acc.OneVue_Application_Status__c = 'in_progress';
            }
            
            update accounts;
            
            //Update the accounts OV created date, avoiding -2 as thats when tasks ARE created. Last two accounts should have date = today
            accounts[0].OV_Status_Change_Date__c = Date.today().addDays(-1);
            accounts[1].OV_Status_Change_Date__c = Date.today().addDays(-3);
            accounts[2].OV_Status_Change_Date__c = Date.today().addDays(-4);
            update accounts;
        }
        
        //Get the subject that should be used
        String subject = 'Follow Up Call: User started Transact application but has not submitted';
        
        Test.startTest();     
        Database.executeBatch(new BDInProgressTasks(Integer.valueOf(settings.In_Progress_Call_Day__c), false, subject), noOfAccs);
        Test.stopTest();
        
        //Check we have no 'in progress' task
        List<Task> inProgressTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
                where Subject = :subject
                AND createdDate = TODAY
                ORDER BY createdDate desc];
        System.assertEquals(inProgressTasks.size(), 0);
        
        //Check we don't have any other tasks!
        List<Task> anyOtherTasks = [SELECT Id, ownerId, CallDisposition, createdDate, WhatId FROM Task 
                where Subject != :subject
                AND createdDate = TODAY
                ORDER BY createdDate desc];
        System.assertEquals(anyOtherTasks.size(), 0);
    }
}