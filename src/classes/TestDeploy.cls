/**
* TestDeploy
* Tests the functionality of the scheduled job deployment code 
*
* @CreatedBy: Ross Tailby
*/
@IsTest
public with sharing class TestDeploy {

	static TestMethod void testCreateAndRemoveAllSchedules() {
		
		//Initially set up the Custom Setting required for this process
		TestProductInstanceHelper.createAllocationSettings();
		
		Set<String> schedules = new Set<String>{'0 15 3 * * ?', 
												'0 0 4 * * ?', 
												'0 0 7 * * ?', 
												'0 0 2 * * ?'};
		
		//Use the Depoy 'start' method to clear out all WAITING cron jobs and check each of those jobs is not scheduled and WAITING
		Deploy.start();
		list<CronTrigger> preCron = new list<CronTrigger>([select Id, State, OwnerId, CronExpression from CronTrigger WHERE State = 'WAITING']);
		for (CronTrigger ct : preCron) {
			if (schedules.contains(ct.CronExpression))
				System.assert(false);
		}
		
		Test.startTest();
		
		//Call the Deploy 'schedule all' method and check they are all deployed
		Deploy testDeploy = new Deploy();
		testDeploy.setup();
		
		//Check what jobs have been scheduled after the execution - all 4 should be there
		list<CronTrigger> postCron = new list<CronTrigger>([select Id, State, OwnerId, CronExpression from CronTrigger WHERE State = 'WAITING']);
		integer numberOfSchedulesFound = 0; //We need this to be the same length at the schedules collection
		for (CronTrigger ct : postCron) {
			if (schedules.contains(ct.CronExpression))
				numberOfSchedulesFound++;
		}
		System.assertEquals(schedules.size(), numberOfSchedulesFound);
		
		//Finally test the removal of all job schedules as detailed above
		testDeploy.remove();
		list<CronTrigger> deletedCron = new list<CronTrigger>([select Id, State, OwnerId, CronExpression from CronTrigger WHERE State = 'WAITING']);
		for (CronTrigger ct : deletedCron) {
			if (schedules.contains(ct.CronExpression))
				System.assert(false);
		}
		
		Test.stopTest();
	}
	
	static TestMethod void testRunIndividualJobs() {
		//Initially set up the Custom Setting required for this process
		TestProductInstanceHelper.createAllocationSettings();
		
		//NB: do not need to test the actual output of these called processes as these should be handled by the unit tests for these methods 
		Test.startTest();
		
		Deploy testDeploy = new Deploy();
		testDeploy.runJob = Deploy.AutoRenewal; //AutoRenewal
		testDeploy.runScheduler(); //Run the scheduler process
		
		System.assert(ApexPages.getMessages().size() == 1);
        System.assert(ApexPages.getMessages().get(0).getDetail() == 'Schedule \'' + Deploy.AutoRenewal + '\' has run successfully!');
		
		
		testDeploy.runJob = Deploy.MonthlyPayments; //Monthly Payments
		testDeploy.runScheduler(); //Run the scheduler process
		
		System.assert(ApexPages.getMessages().size() == 2);
        System.assert(ApexPages.getMessages().get(1).getDetail() == 'Schedule \'' + Deploy.MonthlyPayments + '\' has run successfully!');

		
		testDeploy.runJob = Deploy.CreditCardExpiry; //Credit Card Expiries
		testDeploy.runScheduler(); //Run the scheduler process
		
		System.assert(ApexPages.getMessages().size() == 3);
        System.assert(ApexPages.getMessages().get(2).getDetail() == 'Schedule \'' + Deploy.CreditCardExpiry + '\' has run successfully!');
        
        
        testDeploy.runJob = Deploy.AccountActiveStatus; //Account activations/deactivations
		testDeploy.runScheduler(); //Run the scheduler process
		
		System.assert(ApexPages.getMessages().size() == 4);
        System.assert(ApexPages.getMessages().get(3).getDetail() == 'Schedule \'' + Deploy.AccountActiveStatus + '\' has run successfully!');		
		
		Test.stopTest();
		
	}
	
	static TestMethod void testRunIndividualJobsTwo() {
		//Initially set up the Custom Settings required for this process
		TestProductInstanceHelper.createAllocationSettings();
		
		//NB: do not need to test the actual output of these called processes as these should be handled by the unit tests for these methods 
		Test.startTest();
		
		Deploy testDeploy = new Deploy();
		testDeploy.runJob = Deploy.failedRecurringPayment; //Failed Recurring payments task
		testDeploy.runScheduler();
		
		System.assert(ApexPages.getMessages().size() == 1);
        System.assert(ApexPages.getMessages().get(0).getDetail() == 'Schedule \'' + Deploy.failedRecurringPayment + '\' has run successfully!');
		
		testDeploy.runJob = Deploy.hourlyRecurringNoCard; //Recurring instance/payment set up without credit card hourly task creator
		testDeploy.runScheduler();
		
		System.assert(ApexPages.getMessages().size() == 2);
        System.assert(ApexPages.getMessages().get(1).getDetail() == 'Schedule \'' + Deploy.hourlyRecurringNoCard + '\' has run successfully!');
		
		//testDeploy.runJob = Deploy.duplicateSubsReportSchedule; //Emails a duplicate subscription report daily
		//testDeploy.runScheduler();
		
		//System.assert(ApexPages.getMessages().size() == 3);
        //System.assert(ApexPages.getMessages().get(2).getDetail() == 'Schedule \'' + Deploy.duplicateSubsReportSchedule + '\' has run successfully!');
		
		testDeploy.runJob = Deploy.expiredFTs; //Creates tasks for sales x days after a FT expires
		testDeploy.runScheduler();
		
		System.assert(ApexPages.getMessages().size() == 3);
        System.assert(ApexPages.getMessages().get(2).getDetail() == 'Schedule \'' + Deploy.expiredFTs + '\' has run successfully!');
        
        testDeploy.runJob = Deploy.lateRenewals; //Creates tasks for Customer Service 10 days after nonAR yearly subs or Monthly subs expire
		testDeploy.runScheduler();
		
		System.assert(ApexPages.getMessages().size() == 4);
        System.assert(ApexPages.getMessages().get(3).getDetail() == 'Schedule \'' + Deploy.lateRenewals + '\' has run successfully!');
		
		Test.stopTest();
		
	}
	
	static TestMethod void testRunIndividualJobsThree() {
		//Initially set up the Custom Settings required for this process
		TestProductInstanceHelper.createAllocationSettings();
		TestLatePaperworkTasks.createBrightDaySettings();
		
		//NB: do not need to test the actual output of these called processes as these should be handled by the unit tests for these methods 
		Test.startTest();
		
		Deploy testDeploy = new Deploy();
		
		testDeploy.runJob = Deploy.latePaperworkFirstCall; //Creates tasks for Sales to follow up OV customers not returning their paperwork after 7 days
		testDeploy.runScheduler();
		
		System.assert(ApexPages.getMessages().size() == 1);
        System.assert(ApexPages.getMessages().get(0).getDetail() == 'Schedule \'' + Deploy.latePaperworkFirstCall + '\' has run successfully!');
        
        testDeploy.runJob = Deploy.memberFollowUpCall; //Creates tasks for Sales to follow up site members who have not registered for OV
		testDeploy.runScheduler();
		
		System.assert(ApexPages.getMessages().size() == 2);
        System.assert(ApexPages.getMessages().get(1).getDetail() == 'Schedule \'' + Deploy.memberFollowUpCall + '\' has run successfully!');
        
        testDeploy.runJob = Deploy.noCreditCard; //No Credit Card tasks
		testDeploy.runScheduler();
		
		System.assert(ApexPages.getMessages().size() == 3);
        System.assert(ApexPages.getMessages().get(2).getDetail() == 'Schedule \'' + Deploy.noCreditCard + '\' has run successfully!');
        
        Test.stopTest();
	}
	
	static TestMethod void testRunIndividualJobsFour() {
		Test.startTest();
		
		Deploy testDeploy = new Deploy();
		
		testDeploy.runJob = Deploy.oppsPerUserBatch; //OppsPerUser additional batch
		testDeploy.runScheduler();
		
		System.assert(ApexPages.getMessages().size() == 1);
        System.assert(ApexPages.getMessages().get(0).getDetail() == 'Schedule \'' + Deploy.oppsPerUserBatch + '\' has run successfully!');
        
        Test.stopTest();
	}

}