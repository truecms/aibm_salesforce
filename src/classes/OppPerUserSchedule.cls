global class OppPerUserSchedule implements Schedulable {

	public static void setupSchedule(String jobName, String scheduledTime) { 
        OppPerUserSchedule scheduled_task = new OppPerUserSchedule();
        String payment_id = System.schedule(jobName, scheduledTime, scheduled_task);
    }

    //Schedulable override method: execute - called when the scheduled job is kicked off
    public void execute(SchedulableContext ctx) {
        
        Database.executeBatch(new OppsPerUserBatch(Date.today().addDays(-1)), 5);
        
    }

}