/**
 * TestAccountHelper 
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
@isTest
private class TestReports {

	/**
	 * Helper function for setting up the required objects before testing
	 */
    private static void initialiseDatabase(Integer numRecords, String subType) {
		RecordType recordType = [ select Id, Name, DeveloperName from RecordType where SObjectType = 'Account' and IsPersonType = true limit 1 ];
		
		List<Account> accounts = new List<Account>();
		for (Integer i = 0; i < numRecords; i++) {
			Account account = new Account();
			account.FirstName = 'test'+i;
			account.LastName = 'tester'+i;
			account.PersonEmail = 'test'+i+'@testbucket.net';
			account.Phone = '12345678';
			account.Eureka_ID__c = 'ER_1234567890'+i;
			account.Sub_Type__c = subType;
			accounts.add(account);
		}
		insert(accounts);
		
		Subscription_Stream__c stream = new Subscription_Stream__c();
		stream.Name = 'Eureka Report';
		insert(stream);
		
		Subscription_Product__c prod = new Subscription_Product__c();
		prod.Name = 'product'; 
		prod.Subscription_Stream__c = stream.Id;
		prod.Duration__c = 2;
		if (subType == 'm')
			prod.Duration_units__c = 'month';
		else
			prod.Duration_units__c = 'year';
		insert(prod);
		
		List<Product_Instance__c> subs = new List<Product_Instance__c>();
		for (Integer i = 0; i < numRecords; i++) {
			Product_Instance__c sub = new Product_Instance__c();
			sub.Person_Account__c = accounts[i].Id;
			sub.Subscription_Product__c = prod.Id;
			sub.End_Date__c = Date.today().addMonths(24);
			sub.Subscription_Type__c = subType;
			subs.add(sub);
		}
		insert(subs);
		
		List<go1__Transaction__c> transList = new List<go1__Transaction__c>();
		for (Integer i = 0; i < numRecords; i++) {
			go1__Transaction__c trans = new go1__Transaction__c();
			trans.go1__Account__c = accounts[i].Id;
			trans.go1__Account_ID__c = accounts[i].Id;
			trans.go1__Payment_Name__c = 'Test payment name '+i;
			trans.Subscription_Instance__c = subs[i].Id;
			if (subType == 'm')
				trans.go1__Amount__c = 38.00;
			else
				trans.go1__Amount__c = 385.00;
			trans.go1__Status__c = 'Approved';
			trans.go1__Reference__c = '123456'+i;
			trans.go1__Date__c = Date.today();
			trans.Send_Invoice__c = false; //Set to false so that emails do not get sent out
			transList.add(trans);
		}
		insert(transList);
	}
	
	/**
     * Test the deferred revenue report
     */
    static testMethod void deferredPaymentsTest() {
    	initialiseDatabase(1, 'm');
    	
    	Test.startTest();
        DeferredPaymentsReport report = new DeferredPaymentsReport();
        report.dateRange.Start_Date__c = Date.today().toStartOfMonth();
        report.dateRange.End_Date__c = report.dateRange.Start_Date__c.addDays(30);
        report.initData();
        Test.stopTest();
        
        System.assertEquals(report.results.size(), 1);
        DeferredPaymentsReport.DataRow drTest = report.results[0];
        System.assertEquals(drTest.subId, 'ER_12345678900');
        System.assertEquals(drTest.subStatus, 'active sub');
        System.assertEquals(drTest.sub_type, 'Monthly');
        System.assertEquals(report.totalMonthly, 38.00);
        System.assertEquals(report.totalYearly, 0.00);
        System.assertEquals(drTest.validation, 0.00);
    }
    
    /**
     * Test the deferred revenue report for yearly subs
     */
    static testMethod void deferredPaymentsYearlyTest() {
    	initialiseDatabase(1, 'y');
    	
    	Test.startTest();
        DeferredPaymentsReport report = new DeferredPaymentsReport();
        report.dateRange.Start_Date__c = Date.today().toStartOfMonth();
        report.dateRange.End_Date__c = report.dateRange.Start_Date__c.addDays(30);
        report.initData();
        Test.stopTest();
        
        System.assertEquals(report.results.size(), 1);
        DeferredPaymentsReport.DataRow drTest = report.results[0];
        System.assertEquals(drTest.subId, 'ER_12345678900');
        System.assertEquals(drTest.subStatus, 'active sub');
        System.assertEquals(drTest.sub_type, 'Yearly');
        System.assertEquals(report.totalMonthly, 0.00);
        System.assertEquals(report.totalYearly, 385.00);
        System.assertEquals(drTest.validation, 0.00);
        
        //Check deferred values
        System.assertEquals(drTest.monthlyDeferred.size(), 36); //default 36 months
        System.assertEquals(drTest.monthlyDeferred[0], String.valueOf((385.00/24).setScale(2, RoundingMode.HALF_UP)));
    }
    
    /**
    * Test an invalid entry in the deferred revenue report
    */
    static testMethod void deferredPaymentsInvalidTest() {
    	initialiseDatabase(3, 'y');
    	
    	//Edit one of the Product instances so that its dates are incorrect
    	List<Product_Instance__c> insts = [SELECT Id, Start_Date__c, End_Date__c FROM Product_Instance__c];
    	insts[0].End_Date__c = insts[0].Start_Date__c.addDays(-10);
    	update insts[0];
    	
    	Test.startTest();
    	DeferredPaymentsReport report = new DeferredPaymentsReport();
    	report.dateRange.Start_Date__c = Date.today().toStartOfMonth();
        report.dateRange.End_Date__c = report.dateRange.Start_Date__c.addDays(30);
        report.initData();
    	System.assertEquals(report.results.size(), 3);
    	
    	//Show only invalid rows
    	report.showInvalidRowsOnly();
    	System.assertEquals(report.results.size(), 1);
    	
    	Test.stopTest();
    }
    
    /**
    * Test downloading the deferred revenue report
    */
    static testMethod void deferredPaymentsDownloadTest() {
    	initialiseDatabase(1, 'm');
    	
    	Test.startTest();
    	DeferredPaymentsReport report = new DeferredPaymentsReport();
    	report.dateRange.Start_Date__c = Date.today().toStartOfMonth();
        report.dateRange.End_Date__c = report.dateRange.Start_Date__c.addDays(30);
        report.initData();
    	report.downloadReport();
    	Test.stopTest();
    	
    	System.assertEquals(report.displayPopUp, true);
    	
    	//Check that there is a batch in the queue for the Deferred Payment Report download
    	List<AsyncApexJob> a = [SELECT Id, Status, ApexClass.Name FROM AsyncApexJob];
    	System.assertEquals(a.size(), 1);
    	System.assertEquals(a[0].ApexClass.Name, 'DeferredPaymentsReport');
    	System.assertEquals(a[0].Status, 'Completed');
    }
    
    /**
    * Test showing only invalid rows in the Deferred payments report
    */
    static testMethod void deferredPaymentsInvalidRowsTest() {
    	initialiseDatabase(1, 'm');
    	
    	Test.startTest();
    	DeferredPaymentsReport report = new DeferredPaymentsReport();
    	report.dateRange.Start_Date__c = Date.today().toStartOfMonth();
        report.dateRange.End_Date__c = report.dateRange.Start_Date__c.addDays(30);
        report.initData();
    	report.showOnlyInvalid = true;
    	report.showInvalidRowsOnly();
    	Test.stopTest();
    	
    	System.assertEquals(report.results.size(),0);
    }
    
    /**
    * Test for loading the interface options
    */
    static testMethod void deferredPaymentsInterfaceLoadTest() {
    	initialiseDatabase(1, 'm');
    	
    	Test.startTest();
    	DeferredPaymentsReport report = new DeferredPaymentsReport();
    	List<SelectOption> numMonthOptions = report.getNumberOfMonthsOptions();
    	Test.stopTest();
    	
    	System.assertEquals(numMonthOptions.size(), 50);
    }
    
    /**
     * Test the campaign report
     */
    static testMethod void campaignResults() {
    	CampaignResults results = new CampaignResults();
    }
    
    /**
     * Test the retention report
     */
    static testMethod void retentionReport() {
    	RetentionReport report = new RetentionReport();
    }
    
    /**
     * Test the aquisition and retention report
     */
    static testMethod void aquisitionAndRetentionReport() {
    	AquisitionAndRetentionReport report = new AquisitionAndRetentionReport();
    }
    
    /**
     * Test the conversion report
     */
    static testMethod void conversionReport() {
    	ConversionReport report = new ConversionReport();
    }
    
    /**
     * Test the subscriber list report
     */
    static testMethod void subscriberList() {
    	Subscription_Stream__c stream = new Subscription_Stream__c(Name='Test Stream');
    	insert stream;
    	Subscription_Product__c sub = new Subscription_Product__c(Name='Test Sub', Subscription_Stream__c=stream.Id);
    	insert sub;
    	
    	SubscriberList report = new SubscriberList();
    	report.productName = sub;
    	report.productDate = Date.today().toStartOfMonth();
    	report.initData();
    	
    	report.changePageSize();
    	report.nextBtnClick();
    	report.previousBtnClick();
    	
    	report.getStartPageSize();
    	report.getEndPageSize();
    	report.getTotalSize();
    }
    
    /**
     * Test the free trial conversion report
     */
    static testMethod void freeTrialConversion() {
    	FreeTrialConversion report = new FreeTrialConversion();
    }
    
    /**
    * Tests the PreviousLaunchDeferredPayments report
    */
    static TestMethod void testPreviousDeferredPayments() {
    	PreviousLaunchDeferredPayments testDeferredPayments = new PreviousLaunchDeferredPayments();
    	
    	testDeferredPayments.historicalData(1);
    	
    	testDeferredPayments.futureData(1);
    	
    	testDeferredPayments.getAccruals();
    }
    
    /**
    * Test - the pagination buttons
    */     
	public TestMethod static void testPaginationButtons()
	{
		//Add a large number of test data so we can paginate between them
		initialiseDatabase(8, 'm');
		
		Test.startTest();
		
		DeferredPaymentsReport dpr = new DeferredPaymentsReport();
		dpr.dateRange.Start_Date__c = Date.today().toStartOfMonth();
        dpr.dateRange.End_Date__c = dpr.dateRange.Start_Date__c.addDays(30);
        dpr.initData();
		
		//Set the number of records per page then regenerate the results
		dpr.recordsPerPage = 2; 
		dpr.queryTransactions();		
		
		//Activate all our pagination
		dpr.getMyCommandButtons();
		dpr.refreshGrid();
		
		//Check default counter value
		System.assertEquals(dpr.getCounter(), 0);
		
		dpr.Next();
		System.assertEquals(dpr.getCounter(), 2);
		
		dpr.Next();
		System.assertEquals(dpr.getCounter(), 4);
		
		dpr.Previous();
		System.assertEquals(dpr.getCounter(), 2);
		
		dpr.End();
		System.assertEquals(dpr.getCounter(), 8);
		
		System.assertEquals(dpr.getDisableNext(), true);
		System.assertEquals(dpr.getDisablePrevious(), false);
		System.assertEquals(dpr.totalResults, 8);
		System.assertEquals(dpr.getPageNumber(), 5);
		System.assertEquals(dpr.getTotalPages(), 4);
		
		Test.stopTest();
	}
    
    /**
    * Tests for Monthly Due Report
    */
    static TestMethod void testMonthlyDueNotEarlyRenew() {
    	
    	initialiseDatabase(1, 'y');
    	
    	//Update the Product Instance created so it ends today and appears in the report
    	List<Product_Instance__c> piList = [SELECT Id, Start_Date__c, End_Date__c FROM Product_Instance__c];
    	for (Product_Instance__c pi : piList) {
    		pi.Start_Date__c = Date.today().addMonths(-12);
    		pi.End_Date__c = Date.today();
    	}
    	update piList;
    	
    	String formattedChosenDate = Date.today().month() + '-' + Date.today().year();
    	
    	Test.startTest();
    	MonthlyDueReport mdr = new MonthlyDueReport();
    	Test.stopTest();
    	
    	//Check that the report has 12 result rows and that there is only a count of 1 in the relevant box
    	System.assertEquals(mdr.results.size(), 12);
    	
    	//Iterate round those result rows and check that the only one to contain non-zero results is the one for this month
    	for (MonthlyDueReport.DataRow dr : mdr.results){
    		
    		System.assertEquals(dr.monthlyDue, 0);
    		System.assertEquals(dr.earlyRenew, 0);
    		
    		if (dr.monthDueFormatted == formattedChosenDate){
    			System.assertEquals(dr.yearlyDue, 1);
    			System.assertEquals(dr.totalDue, 1);
    		}
    		else {
    			System.assertEquals(dr.yearlyDue, 0);
    			System.assertEquals(dr.totalDue, 0);
    		}
    		
    	}
    }
    
    static TestMethod void testMonthlyDueMonthlyNotEarlyRenew() {
    	
    	initialiseDatabase(1, 'm');
    	
    	//Update the Product Instance created so it ends today and appears in the report
    	List<Product_Instance__c> piList = [SELECT Id, Start_Date__c, End_Date__c FROM Product_Instance__c];
    	for (Product_Instance__c pi : piList) {
    		pi.Start_Date__c = Date.today().addMonths(-12);
    		pi.End_Date__c = Date.today();
    	}
    	update piList;
    	
    	String formattedChosenDate = Date.today().month() + '-' + Date.today().year();
    	
    	Test.startTest();
    	MonthlyDueReport mdr = new MonthlyDueReport();
    	Test.stopTest();
    	
    	//Check that the report has 12 result rows and that there is only a count of 1 in the relevant box
    	System.assertEquals(mdr.results.size(), 12);
    	
    	//Iterate round those result rows and check that the only one to contain non-zero results is the one for this month
    	for (MonthlyDueReport.DataRow dr : mdr.results){
    		
    		System.assertEquals(dr.yearlyDue, 0);
    		System.assertEquals(dr.earlyRenew, 0);
    		
    		if (dr.monthDueFormatted == formattedChosenDate){
    			System.assertEquals(dr.monthlyDue, 1);
    			System.assertEquals(dr.totalDue, 1);
    		}
    		else {
    			System.assertEquals(dr.monthlyDue, 0);
    			System.assertEquals(dr.totalDue, 0);
    		}
    		
    	}
    }
    
    static TestMethod void testMonthlyDueEarlyRenew() {
    	initialiseDatabase(1, 'y');
    	
    	//Update the Product Instance created so it ends today and appears in the report
    	List<Product_Instance__c> piList = [SELECT Id, Start_Date__c, End_Date__c, Person_Account__c, Subscription_Product__c FROM Product_Instance__c];
    	for (Product_Instance__c pi : piList) {
    		pi.Start_Date__c = Date.today().addMonths(-12);
    		pi.End_Date__c = Date.today();
    	}
    	
    	//Now add a 'renewed' PI with a slightly later Start Date to simulate Early Renewal
    	Product_Instance__c sub = new Product_Instance__c();
		sub.Person_Account__c = piList[0].Person_Account__c;
		sub.Subscription_Product__c = piList[0].Subscription_Product__c;
		sub.Start_Date__c = Date.today().addMonths(1);
		sub.End_Date__c = Date.today().addMonths(13);
		sub.Subscription_Type__c = 'y';
		piList.add(sub);
		
		upsert piList; //Inserting our renewal and updating the original PI
		
		String formattedChosenDate = Date.today().month() + '-' + Date.today().year();
		
		Test.startTest();
		MonthlyDueReport mdr = new MonthlyDueReport();
		Test.stopTest();
		
		System.assertEquals(mdr.results.size(), 12);
		
		for (MonthlyDueReport.DataRow dr : mdr.results){
    		
    		//Only interested in our current end date month
    		if (dr.monthDueFormatted == formattedChosenDate){
    			System.assertEquals(dr.yearlyDue,1);
    			System.assertEquals(dr.monthlyDue,0);
    			System.assertEquals(dr.totalDue,1);
    			System.assertEquals(dr.earlyRenew,1);
    		}
    		
    	}
    }
    
    static TestMethod void testMonthlyDueSwitchFY() {
    	Test.startTest();
    	MonthlyDueReport mdr = new MonthlyDueReport();
    	Test.stopTest();
    	
    	//There should be 6 financial year options in the drop down
    	System.assertEquals(mdr.getFinancialYearOptions().size(), 6);
    	
    	//Get the first option and "select" it 
    	SelectOption firstFY = mdr.getFinancialYearOptions()[0];
    	String firstFYString = firstFY.getValue();
    	mdr.fyYearSelection = firstFYString;
    	mdr.selectFinancialYear();
    	
    	//Test the FY start date used is correct
    	System.assertEquals(String.valueOf(mdr.financialYearStart.year()), firstFYString);
    }
    
    /**
    * Tests for New Subs By Source Report
    */
    static TestMethod void testNewSubsBySourceBusSpec() {
    	initialiseDatabase(1, 'y');
    	
    	//Also create the Bus Spec campaign and FT that will make this appear in the report
    	Campaign c = new Campaign();
    	c.Name = 'Test Campaign';
    	c.Type = 'Business Spectator';
    	insert c;
    	
    	List<Account> accounts = [SELECT Id FROM Account];
    	
    	Subscription_Stream__c stream = [SELECT Id FROM Subscription_Stream__c WHERE Name = 'Eureka Report' LIMIT 1];
    	
    	Subscription_Product__c prod = new Subscription_Product__c();
		prod.Name = 'free product'; 
		prod.Subscription_Stream__c = stream.Id;
		prod.Duration__c = 2;
		prod.Duration_units__c = 'month';
		prod.Subscription_Type__c = 'f';
		insert(prod);
    	
    	Product_Instance__c pi = new Product_Instance__c();
    	pi.Subscription_Type__c = 'f';
    	pi.Person_Account__c = accounts[0].Id;
		pi.Subscription_Product__c = prod.Id;
		pi.End_Date__c = Date.today().addMonths(24);
		pi.Campaign__c = c.Id;
		insert pi;
    	
    	//Get the end date of the period the report should be covering
    	Datetime endDate = Date.newInstance(Date.today().year(), Date.today().month(), 1);
    	
    	//Now correctly set the date of the transaction so it appears within our date range
    	List<go1__Transaction__c> trans = [SELECT Id, go1__Date__c FROM go1__Transaction__c];
    	for (go1__Transaction__c tr : trans) {
    		tr.go1__Date__c = endDate.addDays(-3);
    	}
    	update trans;
    	
    	//Also set up the dates of the Product Instance so that they correspond too
    	List<Product_Instance__c> pis = [SELECT Id, Start_Date__c, End_Date__c, Subscription_Type__c FROM Product_Instance__c WHERE Person_Account__c = :accounts[0].Id];
    	for (Product_Instance__c prodInst : pis) {
    		if (prodInst.Subscription_Type__c == 'f') {
    			Datetime startDate = endDate.addDays(-120);
    			Datetime newEnd = endDate.addDays(-90);
    			prodInst.Start_Date__c = Date.newInstance(startDate.year(), startDate.month(), startDate.day());
    			prodInst.End_Date__c = Date.newInstance(newEnd.year(), newEnd.month(), newEnd.day());
    		}
    		if (prodInst.Subscription_Type__c == 'y') {
    			Datetime startDate = endDate.addDays(-3);
    			Datetime newEnd = endDate.addDays(120);
    			prodInst.Start_Date__c = Date.newInstance(startDate.year(), startDate.month(), startDate.day());
    			prodInst.End_Date__c = Date.newInstance(newEnd.year(), newEnd.month(), newEnd.day());
    		}
    	}
    	update pis;
    	
    	//Finally update the Accounts so that all the triggers apply
    	update accounts;
    	
    	Test.startTest();
    	NewSubsBySourceReport nsr = new NewSubsBySourceReport();
    	Test.stopTest();
    	
    	//Check that the default has applied i.e. the campaign type is BUS SPEC
    	System.assertEquals(nsr.campaignType, 'Business Spectator');
    	
    	//Check the end date is the start of the current month
    	System.assertEquals(nsr.dateInstance.End_Date__c, endDate);
    	
    	//Check that we have the 1 sub appearing in our report results
    	System.assertEquals(nsr.totalSubs, 1);
    	System.assertEquals(nsr.results[0].email, 'test0@testbucket.net');
    }
    
    
    static TestMethod void testNewSubsBySourceInvTrends() {
    	initialiseDatabase(1, 'y');
    	
    	String source = 'Affiliate - Investment Trends';
    	
    	//Also create the Bus Spec campaign and FT that will make this appear in the report
    	Campaign c = new Campaign();
    	c.Name = 'Test Campaign';
    	c.Type = source;
    	insert c;
    	
    	List<Account> accounts = [SELECT Id FROM Account];
    	
    	Subscription_Stream__c stream = [SELECT Id FROM Subscription_Stream__c WHERE Name = 'Eureka Report' LIMIT 1];
    	
    	Subscription_Product__c prod = new Subscription_Product__c();
		prod.Name = 'free product'; 
		prod.Subscription_Stream__c = stream.Id;
		prod.Duration__c = 2;
		prod.Duration_units__c = 'month';
		prod.Subscription_Type__c = 'f';
		insert(prod);
    	
    	Product_Instance__c pi = new Product_Instance__c();
    	pi.Subscription_Type__c = 'f';
    	pi.Person_Account__c = accounts[0].Id;
		pi.Subscription_Product__c = prod.Id;
		pi.End_Date__c = Date.today().addMonths(24);
		pi.Campaign__c = c.Id;
		insert pi;
    	
    	//Get the end date of the period the report should be covering
    	Datetime endDate = Date.newInstance(Date.today().year(), Date.today().month(), 1);
    	
    	//Now correctly set the date of the transaction so it appears within our date range
    	List<go1__Transaction__c> trans = [SELECT Id, go1__Date__c FROM go1__Transaction__c];
    	for (go1__Transaction__c tr : trans) {
    		tr.go1__Date__c = endDate.addDays(-3);
    	}
    	update trans;
    	
    	//Also set up the dates of the Product Instance so that they correspond too
    	List<Product_Instance__c> pis = [SELECT Id, Start_Date__c, End_Date__c, Subscription_Type__c FROM Product_Instance__c WHERE Person_Account__c = :accounts[0].Id];
    	for (Product_Instance__c prodInst : pis) {
    		if (prodInst.Subscription_Type__c == 'f') {
    			Datetime startDate = endDate.addDays(-120);
    			Datetime newEnd = endDate.addDays(-90);
    			prodInst.Start_Date__c = Date.newInstance(startDate.year(), startDate.month(), startDate.day());
    			prodInst.End_Date__c = Date.newInstance(newEnd.year(), newEnd.month(), newEnd.day());
    		}
    		if (prodInst.Subscription_Type__c == 'y') {
    			Datetime startDate = endDate.addDays(-3);
    			Datetime newEnd = endDate.addDays(120);
    			prodInst.Start_Date__c = Date.newInstance(startDate.year(), startDate.month(), startDate.day());
    			prodInst.End_Date__c = Date.newInstance(newEnd.year(), newEnd.month(), newEnd.day());
    		}
    	}
    	update pis;
    	
    	//Finally update the Accounts so that all the triggers apply
    	update accounts;
    	
    	Test.startTest();
    	NewSubsBySourceReport nsr = new NewSubsBySourceReport();
    	
    	//Set our choice of source i.e. affiliate
    	nsr.campaignType = source;
    	
    	//Re-initialise the data
    	nsr.initData();
    	Test.stopTest();
    	
    	//Check that the default has applied i.e. the campaign type is BUS SPEC
    	System.assertEquals(nsr.campaignType, source);
    	
    	//Check the end date is the start of the current month
    	System.assertEquals(nsr.dateInstance.End_Date__c, endDate);
    	
    	//Check that we have the 1 sub appearing in our report results
    	System.assertEquals(nsr.totalSubs, 1);
    	System.assertEquals(nsr.results[0].email, 'test0@testbucket.net');
    }
    
    static TestMethod void testNewSubsBySourceGetSources() {
    	Test.startTest();
    	NewSubsBySourceReport nsr = new NewSubsBySourceReport();
    	Test.stopTest();
    	
    	System.assertEquals(nsr.getSources().size(), 2);
    }
    
    /**
    * Tests for Commission Report
    */
    static TestMethod void testCommissionReportNew() {
    	
    	//Get a reference to any one of the users that are involved in the report
    	List<User> users = [SELECT Id, Name FROM User WHERE Alias IN ('jwill', 'zkesh', 'lmain', 'tbull') and IsActive = true];
    	System.assert(users.size() > 0);
    	
    	//Create our test data as this user
    	System.runAs(users.get(0)) {
    		//Create the sample data, transaction can be this month if we set the start and end date of the report
    		initialiseDatabase(1, 'y');
    	}
    	
    	Datetime current = Datetime.now();
    	
    	Test.startTest();
    	CommissionReport cr = new CommissionReport();
		cr.dateInstance.Start_Date__c = Date.newInstance(current.year(), current.month(), 1);
		cr.dateInstance.End_Date__c = Date.newInstance(current.addMonths(1).year(), current.addMonths(1).month(), 1);
		cr.initData();
    	
    	Test.stopTest();
    	
    	System.assertEquals(cr.results.size(),1);
    	System.assertEquals(cr.results.get(0).transactionType, 'New');
    	
    	//Check the Summary values
    	System.assertEquals(cr.summaryResults[0].salesperson, users.get(0).Name);
    	System.assertEquals(cr.summaryResults[0].newCount, 1);
    	System.assertEquals(cr.summaryResults[0].newSumValue, 385.00);
    	System.assertEquals(cr.summaryResults[0].winbackCount, 0);
    	System.assertEquals(cr.summaryResults[0].winbackSumValue, 0);
    	System.assertEquals(cr.summaryResults[0].totalCount, 1);
    	System.assertEquals(cr.summaryResults[0].totalSumValue, 385.00);
    	
    }
    
    //Methods for winback, late renewal and renewal?
    static TestMethod void testCommissionReportWinback() {
    	//Get a reference to any one of the users that are involved in the report
    	List<User> users = [SELECT Id, Name FROM User WHERE Alias IN ('jwill', 'zkesh', 'lmain', 'tbull') and IsActive = true];
    	System.assert(users.size() > 0);
    	
    	//Create our test data as this user
    	System.runAs(users.get(0)) {
    		//Create the sample data, transaction can be this month if we set the start and end date of the report
    		initialiseDatabase(1, 'y');
    		
    		//Now add a previous Product Instance that lapsed more than 30 days earlier
    		List<Product_Instance__c> insts = [SELECT Start_Date__c, Subscription_Product__c, Person_Account__c
    											FROM Product_Instance__c];
			
			//Just add a winback for the 1st stored inst (there should only be 1!)
			Product_Instance__c newInst = new Product_Instance__c();
			newInst.Subscription_Product__c = insts[0].Subscription_Product__c;
			newInst.Person_Account__c = insts[0].Person_Account__c;
			newInst.Subscription_Type__c = 'y';
			insert newInst;
			go1__Transaction__c trans = new go1__Transaction__c();
			trans.go1__Account__c = insts[0].Person_Account__c;
			trans.go1__Account_ID__c = insts[0].Person_Account__c;
			trans.go1__Payment_Name__c = 'Test payment name 2';
			trans.Subscription_Instance__c = newInst.Id;
			trans.go1__Amount__c = 385.00;
			trans.go1__Status__c = 'Approved';
			trans.go1__Reference__c = '1234562';
			trans.go1__Date__c = Date.today().addMonths(-24);
			trans.Send_Invoice__c = false; //Set to false so that emails do not get sent out
			insert trans;
			
			List<Product_Instance__c> setDates = [SELECT Start_Date__c, End_Date__c, Id 
													FROM Product_Instance__c
													WHERE Id = :newInst.Id];
						
			Product_Instance__c updater = setDates[0];
			updater.Start_Date__c = insts[0].Start_Date__c.addMonths(-24);
			updater.End_Date__c = insts[0].Start_Date__c.addMonths(-12);
			update updater;
    	}
    	
    	Datetime current = Datetime.now();
    	
    	Test.startTest();
    	CommissionReport cr = new CommissionReport();
		cr.dateInstance.Start_Date__c = Date.newInstance(current.year(), current.month(), 1);
		cr.dateInstance.End_Date__c = Date.newInstance(current.addMonths(1).year(), current.addMonths(1).month(), 1);
		cr.initData();
    	
    	Test.stopTest();
    	
    	System.assertEquals(cr.results.size(),2); //using createdDate - both trans will be on the same day, although the Product Instance days dictate the trans type
    	for (CommissionReport.DataRow dr : cr.results) {
    		//The 2nd (later) transaction should always follow directly on and be treated as a Renewal, its the 1st transaction we're interested in testing!
    		if (dr.daysLapsed < 1)
    			System.assertEquals(dr.transactionType, 'Renewal');
    		else
    			System.assertEquals(dr.transactionType, 'Winback');
    	}
    	
    	//Check the Summary values
    	System.assertEquals(cr.summaryResults[0].salesperson, users.get(0).Name);
    	System.assertEquals(cr.summaryResults[0].newCount, 0);
    	System.assertEquals(cr.summaryResults[0].newSumValue, 0.00);
    	System.assertEquals(cr.summaryResults[0].winbackCount, 1);
    	System.assertEquals(cr.summaryResults[0].winbackSumValue, 385.00);
    	System.assertEquals(cr.summaryResults[0].renewalCount, 1);
    	System.assertEquals(cr.summaryResults[0].renewalSumValue, 385.00);
    	System.assertEquals(cr.summaryResults[0].totalCount, 2);
    	System.assertEquals(cr.summaryResults[0].totalSumValue, 770.00);
    }
    
    static TestMethod void testCommissionReportLateRenewal() {
    	//Get a reference to any one of the users that are involved in the report
    	List<User> users = [SELECT Id FROM User WHERE Alias IN ('jwill', 'zkesh', 'lmain', 'tbull') and IsActive = true];
    	System.assert(users.size() > 0);
    	
    	//Create our test data as this user
    	System.runAs(users.get(0)) {
    		//Create the sample data, transaction can be this month if we set the start and end date of the report
    		initialiseDatabase(1, 'y');
    		
    		//Now add a previous Product Instance that lapsed less than 30 and more than 1 day ago
    		List<Product_Instance__c> insts = [SELECT Start_Date__c, Subscription_Product__c, Person_Account__c
    											FROM Product_Instance__c];
			
			//Just add a winback for the 1st stored inst (there should only be 1!)
			Product_Instance__c newInst = new Product_Instance__c();
			newInst.Subscription_Product__c = insts[0].Subscription_Product__c;
			newInst.Person_Account__c = insts[0].Person_Account__c;
			newInst.Subscription_Type__c = 'y';
			insert newInst;
			go1__Transaction__c trans = new go1__Transaction__c();
			trans.go1__Account__c = insts[0].Person_Account__c;
			trans.go1__Account_ID__c = insts[0].Person_Account__c;
			trans.go1__Payment_Name__c = 'Test payment name 2';
			trans.Subscription_Instance__c = newInst.Id;
			trans.go1__Amount__c = 385.00;
			trans.go1__Status__c = 'Approved';
			trans.go1__Reference__c = '1234562';
			trans.go1__Date__c = Date.today().addMonths(-12);
			trans.Send_Invoice__c = false; //Set to false so that emails do not get sent out
			insert trans;
			
			List<Product_Instance__c> setDates = [SELECT Start_Date__c, End_Date__c, Id 
													FROM Product_Instance__c
													WHERE Id = :newInst.Id];
						
			Product_Instance__c updater = setDates[0];
			updater.Start_Date__c = insts[0].Start_Date__c.addMonths(-12);
			updater.End_Date__c = insts[0].Start_Date__c.addDays(-5);
			update updater;
    	}
    	
    	Datetime current = Datetime.now();
    	
    	Test.startTest();
    	CommissionReport cr = new CommissionReport();
		cr.dateInstance.Start_Date__c = Date.newInstance(current.year(), current.month(), 1);
		cr.dateInstance.End_Date__c = Date.newInstance(current.addMonths(1).year(), current.addMonths(1).month(), 1);
		cr.initData();
    	
    	Test.stopTest();
    	
    	System.assertEquals(cr.results.size(),2); 
    	for (CommissionReport.DataRow dr : cr.results) {
    		//The 2nd (later) transaction should always follow directly on and be treated as a Renewal, its the 1st transaction we're interested in testing!
    		if (dr.daysLapsed < 1)
    			System.assertEquals(dr.transactionType, 'Renewal');
    		else
    			System.assertEquals(dr.transactionType, 'Late Renewal');
    	}
    }
    
    static TestMethod void testCommissionReportRenewal() {
    	//Get a reference to any one of the users that are involved in the report
    	List<User> users = [SELECT Id FROM User WHERE Alias IN ('jwill', 'zkesh', 'lmain', 'tbull') and IsActive = true];
    	System.assert(users.size() > 0);
    	
    	//Create our test data as this user
    	System.runAs(users.get(0)) {
    		//Create the sample data, transaction can be this month if we set the start and end date of the report
    		initialiseDatabase(1, 'y');
    		
    		//Now add a previous Product Instance that lapsed less than 30 and more than 1 day ago
    		List<Product_Instance__c> insts = [SELECT Start_Date__c, Subscription_Product__c, Person_Account__c
    											FROM Product_Instance__c];
			
			//Just add a winback for the 1st stored inst (there should only be 1!)
			Product_Instance__c newInst = new Product_Instance__c();
			newInst.Subscription_Product__c = insts[0].Subscription_Product__c;
			newInst.Person_Account__c = insts[0].Person_Account__c;
			newInst.Subscription_Type__c = 'y';
			insert newInst;
			go1__Transaction__c trans = new go1__Transaction__c();
			trans.go1__Account__c = insts[0].Person_Account__c;
			trans.go1__Account_ID__c = insts[0].Person_Account__c;
			trans.go1__Payment_Name__c = 'Test payment name 2';
			trans.Subscription_Instance__c = newInst.Id;
			trans.go1__Amount__c = 385.00;
			trans.go1__Status__c = 'Approved';
			trans.go1__Reference__c = '1234562';
			trans.go1__Date__c = Date.today().addMonths(-12);
			trans.Send_Invoice__c = false; //Set to false so that emails do not get sent out
			insert trans;
			
			List<Product_Instance__c> setDates = [SELECT Start_Date__c, End_Date__c, Id 
													FROM Product_Instance__c
													WHERE Id = :newInst.Id];
						
			Product_Instance__c updater = setDates[0];
			updater.Start_Date__c = insts[0].Start_Date__c.addMonths(-12);
			updater.End_Date__c = insts[0].Start_Date__c;
			update updater;
    	}
    	
    	Datetime current = Datetime.now();
    	
    	Test.startTest();
    	CommissionReport cr = new CommissionReport();
		cr.dateInstance.Start_Date__c = Date.newInstance(current.year(), current.month(), 1);
		cr.dateInstance.End_Date__c = Date.newInstance(current.addMonths(1).year(), current.addMonths(1).month(), 1);
		cr.initData();
    	
    	Test.stopTest();
    	
    	System.assertEquals(cr.results.size(),2);
    	for (CommissionReport.DataRow dr : cr.results) {
    		//The 2nd (later) transaction should always follow directly on and be treated as a Renewal, its the 1st transaction we're interested in testing!
    		if (dr.daysLapsed < 1)
    			System.assertEquals(dr.transactionType, 'Renewal');
    		else
    			System.assertEquals(dr.transactionType, 'Renewal');
    	}
    }
    
    static testMethod void testCommissionReportDownload() {
    	initialiseDatabase(1, 'm');
    	
    	Test.startTest();
    	CommissionReport report = new CommissionReport();
    	PageReference pr = report.downloadReport();
    	Test.stopTest();
    	
    	System.assertEquals(pr.getUrl(), '/apex/Commission_report_export');
    }
}