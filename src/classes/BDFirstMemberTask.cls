/**
* Batch job that runs nightly to pull all Accounts that have not signed up for a Onevue account after being registered on the BD site for x days (First task)
* A task for the Sales Team to follow up will be created for each of these customers 
* Author: tailbyr
* Copyright: EurekaReport.com.au
*/
public with sharing class BDFirstMemberTask implements Schedulable {

    /*  
        Method used to create the scheduling of this Batch Job. This is used by the 'TestScheduledJobs' function which enables the scheduled jobs to be 
        setup and removed programmatically (also via API for Jenkins build job)
    */
    public static void setupSchedule(String jobName, String scheduledTime) { 
        BDFirstMemberTask scheduled_task = new BDFirstMemberTask();
        String payment_id = System.schedule(jobName, scheduledTime, scheduled_task);
    }

    //Schedulable override method: execute - called when the scheduled job is kicked off
    public void execute(SchedulableContext ctx) {
        
        BrightDay_Settings__c settings = BrightDay_Settings__c.getInstance();
        List<Integer> numDays = getNumDays(settings);
        
        for (Integer day : numDays) {
        	String activeSubject = TaskManager.ACTIVE_MEMBER_FOLLOWUP_SUBJECT;
        	activeSubject = activeSubject.replace('[xxx]', String.valueOf(Math.abs(day)));
        	String inactiveSubject = TaskManager.INACTIVE_MEMBER_FOLLOWUP_SUBJECT;
        	inactiveSubject = inactiveSubject.replace('[xxx]', String.valueOf(Math.abs(day)));
        
        	Database.executeBatch(new BDMemberTasks(day, true, activeSubject), 1);
        	Database.executeBatch(new BDMemberTasks(day, false, inactiveSubject), 1);
        }
        
        //Now also execute the In Progress follwup tasks
        if (settings.In_Progress_Call_Day__c != null) {
        	Integer inProgressDays = Integer.valueOf(settings.In_Progress_Call_Day__c);
        	Database.executeBatch(new BDInProgressTasks(inProgressDays, false, TaskManager.IN_PROGRESS_APPLICATION_FOLLOWUP_SUBJECT));
        }
    }

    public static List<Integer> getNumDays(BrightDay_Settings__c settings){
    	if (settings == null)
        	settings = BrightDay_Settings__c.getInstance();
        
        List<Integer> numDays = new List<Integer>();
        if (settings.Member_Followup_Call_Day__c != null) {
        	
        	String[] daysStrArr = settings.Member_Followup_Call_Day__c.split(',');
        	for (Integer i = 0; i < daysStrArr.size(); i++) {
        		numDays.add(Integer.valueOf(daysStrArr[i]));
        	}
        	
        }
        
        return numDays;
    }

}