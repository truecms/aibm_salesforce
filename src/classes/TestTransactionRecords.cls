/**
 * UnitTest: 
 *
 * Tests the functionality of the AIBM TransactionRecords implementation 
 *
 * Author: tailbyr
 * Copyright: EurekaReport.com.au
 */
@IsTest
public with sharing class TestTransactionRecords {

	private static Account createAccount(){
		Account a = new Account();
		a.FirstName = 'test';
		a.LastName = 'tester';
		a.PersonEmail = 'test@testbucket.net';
		a.Phone = '12345678';
		a.BD_Current_Role__c = 'Client';
		insert a;
		return a;
	}

	private static Account setupAccountTransactions(Integer numTrans, Boolean fail){
		Account a = createAccount();
		
		List<go1__Transaction__c> transList = new List<go1__Transaction__c>();
		for (Integer i = 0; i < numTrans; i++) {
			go1__Transaction__c trans = new go1__Transaction__c();
			trans.go1__Account__c = a.Id;
			trans.go1__Account_ID__c = a.Id; 
			trans.go1__Amount__c = 100;
			trans.go1__Status__c = 'Success';
			trans.go1__Payment_Name__c = '123';
			trans.Send_Invoice__c = false;
			trans.go1__Code__c = 8;
			transList.add(trans);
		}
		
		//Failed transactions too?
		if (fail == true) {
			for (Integer i = 0; i < numTrans; i++) {
				go1__Transaction__c trans = new go1__Transaction__c();
				trans.go1__Account__c = a.Id;
				trans.go1__Account_ID__c = a.Id; 
				trans.go1__Amount__c = 100;
				trans.go1__Status__c = 'Fail';
				trans.go1__Payment_Name__c = '123';
				trans.Send_Invoice__c = false;
				trans.go1__Code__c = 11;
				transList.add(trans);
			}
		}
		
		insert transList;
		return a;
	}
	
	public static TestMethod void testDisplaySuccessfulTransactions(){
		Account a = setupAccountTransactions(5, false);
		
		Test.startTest();
		ApexPages.Standardcontroller sc = new ApexPages.StandardController(a);
		TransactionRecords tr = new TransactionRecords(sc);
		Test.stopTest();
		
		//Get the Transactions collection to check that has been populated
		List<go1__Transaction__c> transactions = tr.getTransactions();
		System.assertEquals(transactions.size(), 5);
		
		System.assertEquals(tr.getTotalPageNumber(), 1);
		System.assertEquals(tr.getPageNumber(), 1);
		System.assertEquals(tr.getTotal(), 500.00);
	}
	
	public static TestMethod void testDisplayFailedTransactionsNotTotalled(){
		//Check that the total amount value of the transactions does not include the failed transactions
		Account a = setupAccountTransactions(5, true);
		
		Test.startTest();
		ApexPages.Standardcontroller sc = new ApexPages.StandardController(a);
		TransactionRecords tr = new TransactionRecords(sc);
		Test.stopTest();
		
		System.assertEquals(tr.getTotalPageNumber(), 1);
		System.assertEquals(tr.getPageNumber(), 1);
		System.assertEquals(tr.getTotal(), 500.00);
		
		System.assert(tr.getPreviousButtonDisabled());
		System.assert(tr.getNextButtonDisabled());
	}
	
	public static TestMethod void testNoTransactions(){
		//Create the Account only
		Account a = createAccount();
		
		Test.startTest();
		ApexPages.Standardcontroller sc = new ApexPages.StandardController(a);
		TransactionRecords tr = new TransactionRecords(sc);
		Test.stopTest();
		
		//Test that the collection size is 0 and the buttons are both disabled
		System.assertEquals(tr.getTotalSize(), 0);
		System.assertEquals(tr.getTotal(), 0);
		System.assert(tr.getNextButtonDisabled());
		System.assert(tr.getPreviousButtonDisabled());
	}
	
	public static TestMethod void testPagination(){
		Account a = setupAccountTransactions(32, false);
		
		Test.startTest();
		ApexPages.Standardcontroller sc = new ApexPages.StandardController(a);
		TransactionRecords tr = new TransactionRecords(sc);
		Test.stopTest();
		
		System.assertEquals(tr.getTotalPageNumber(), 4);
		System.assertEquals(tr.getTotal(), 3200.00);
		
		//Page Start Number and End Number
		System.assertEquals(tr.getStartPageNumber(), 1);
		System.assertEquals(tr.getEndPageNumber(), 10);
		
		//Click 'Next'
		System.assert(!tr.getNextButtonDisabled());
		tr.nextBtnClick();
		System.assertEquals(tr.getPageNumber(), 2);
		System.assertEquals(tr.getStartPageNumber(), 11);
		System.assertEquals(tr.getEndPageNumber(), 20);
		tr.nextBtnClick();
		System.assertEquals(tr.getPageNumber(), 3);
		System.assertEquals(tr.getStartPageNumber(), 21);
		System.assertEquals(tr.getEndPageNumber(), 30);
		tr.nextBtnClick();
		System.assertEquals(tr.getPageNumber(), 4);
		System.assert(tr.getNextButtonDisabled());
		System.assertEquals(tr.getStartPageNumber(), 31);
		System.assertEquals(tr.getEndPageNumber(), 32);
				
		//Click 'Previous'
		System.assert(!tr.getPreviousButtonDisabled());
		tr.previousBtnClick();
		System.assertEquals(tr.getPageNumber(), 3);
		tr.previousBtnClick();
		System.assertEquals(tr.getPageNumber(), 2);
		tr.previousBtnClick();
		System.assert(tr.getPreviousButtonDisabled());
		
		//Page Size and Total Size
		System.assertEquals(tr.getPageSize(), 10);
		System.assertEquals(tr.getTotalSize(), 32);
	}
	
	public static TestMethod void testRefundPageReference(){
		Account a = setupAccountTransactions(1, false);
		
		//Get the Id of the 1 transactions we inserted - it will be our refund
		go1__Transaction__c trans = [SELECT Id FROM go1__Transaction__c LIMIT 1];
		
		Test.startTest();
		ApexPages.Standardcontroller sc = new ApexPages.StandardController(a);
		TransactionRecords tr = new TransactionRecords(sc);
		tr.refundId = trans.Id;
		PageReference refund = tr.refund();
		Test.stopTest();
		
		System.assertEquals(refund.getURL(), '/apex/go1__refund?id=' + trans.Id);
	}

}