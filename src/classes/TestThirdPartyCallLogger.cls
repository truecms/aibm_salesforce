/**
 * Testing the ThirdPartyCallLogger class
 * Author: tailbyr
 * Copyright: eurekareport.com.au
 */
@isTest
public with sharing class TestThirdPartyCallLogger {

	public static TestMethod void uploadCallLog(){
		//Create sample accounts
		List<Account> accounts = TestAccountHelper.createAccounts(3);
		
		//Define the content of our CSV
		String csv = 'Email Address,DateTime logged,Agent Name,Call Result,Comments\n';
		for (Account a : accounts){
        	csv += a.PersonEmail + ',01/01/2014,TestAgent,Sale,Unit Test Comment\n';
		}
		
		//Also add an additional call to the 1st account to ensure that is possible too
		csv += accounts[0].PersonEmail + ',01/01/2014,TestAgent2,Complaint,Unit Test Comment2\n';
		
		Test.startTest();
		
		//Replicate the CSV upload
		ThirdPartyCallLogger tpc = new ThirdPartyCallLogger();
		tpc.nameFile = '';
		tpc.contentFile = Blob.valueOf(csv);
		tpc.readFile();
		
		//Check the variables hold the right values
		System.assertEquals(tpc.totalCallsAdded, 4);
		System.assertEquals(tpc.invalidEmails.size(), 0);
		System.assertEquals(tpc.invalidEmail, null); //TRUE or NULL
		System.assertEquals(tpc.createTasks.size(), 3);
		
		//Persist changes
		tpc.persistTasks();
		
		Test.stopTest();
		
		System.assertEquals(tpc.totalCallsAdded, 4);
		
		//Test our Accounts have the correct calls logged against them
		List<Task> callTasks = [SELECT WhatId, Call_Result__c, Subject, Description FROM Task];
		System.assertEquals(callTasks.size(), 4);
		
		List<Task> account0Tasks = [SELECT WhatId, Call_Result__c, Subject, Description FROM Task WHERE WhatId = :accounts[0].Id ORDER BY Subject ASC];
		System.assertEquals(account0Tasks.size(), 2);
		System.assertEquals(account0Tasks[0].Call_Result__c, 'Sale');
		System.assertEquals(account0Tasks[0].Subject, 'OneVue Call: Agent: TestAgent');
		System.assertEquals(account0Tasks[1].Subject, 'OneVue Call: Agent: TestAgent2');
		System.assertEquals(account0Tasks[1].Call_Result__c, 'Complaint');
		
		List<Task> account1Tasks = [SELECT WhatId, Call_Result__c, Subject, Description FROM Task WHERE WhatId = :accounts[1].Id ORDER BY Subject ASC];
		System.assertEquals(account1Tasks.size(), 1);
		System.assertEquals(account1Tasks[0].Call_Result__c, 'Sale');
		System.assertEquals(account1Tasks[0].Subject, 'OneVue Call: Agent: TestAgent');
		
		List<Task> account2Tasks = [SELECT WhatId, Call_Result__c, Subject, Description FROM Task WHERE WhatId = :accounts[2].Id ORDER BY Subject ASC];
		System.assertEquals(account2Tasks.size(), 1);
		System.assertEquals(account2Tasks[0].Call_Result__c, 'Sale');
		System.assertEquals(account2Tasks[0].Subject, 'OneVue Call: Agent: TestAgent');
	}
	
	public static TestMethod void uploadCallsInvalidAccounts(){
		//Define CSV content, using invalid emails addresses and valid ones with no accounts
		String csv = 'Email Address,DateTime logged,Agent Name,Call Result,Comments\n';
		csv += 'invalid.option1@testbucket.com,01/01/2014,TestAgent,Sale,Unit Test Comment\n';
		csv += 'invalid.option2@testbucket.net,01/01/2014,TestAgent2,Complaint,Unit Test Comment2\n';
		
		Test.startTest();
		
		//Replicate the CSV upload
		ThirdPartyCallLogger tpc = new ThirdPartyCallLogger();
		tpc.nameFile = '';
		tpc.contentFile = Blob.valueOf(csv);
		tpc.readFile();
		
		//Check the variables hold the right values
		System.assertEquals(tpc.totalCallsAdded, 0);
		System.assertEquals(tpc.invalidEmails.size(), 2);
		System.assertEquals(tpc.invalidEmail, true);
		System.assertEquals(tpc.createTasks.size(), 0);
		
		//Test we have the correct error rows
		System.assert(tpc.invalidEmails.containsKey('invalid.option1@testbucket.com'));
		System.assertEquals(tpc.invalidEmails.get('invalid.option1@testbucket.com'), 1);
		System.assert(tpc.invalidEmails.containsKey('invalid.option2@testbucket.net'));
		System.assertEquals(tpc.invalidEmails.get('invalid.option2@testbucket.net'), 1);
		
		//Persist changes, should be nothing saved
		tpc.persistTasks();
		
		Test.stopTest();
		
		System.assertEquals(tpc.totalCallsAdded, 0);
		
		List<Task> noTasks = [SELECT Id FROM Task];
		System.assertEquals(noTasks.size(), 0);		
	}
	
	public static TestMethod void uploadFileWithEmptyCSVRow() {
		List<Account> accounts = TestAccountHelper.createAccounts(1);
		
        String csv = 'Email Address,DateTime logged,Agent Name,Call Result,Comments\n';
        csv += ',,,,\n'; // Blank row
        csv += accounts[0].PersonEmail + ',01/01/2014,TestAgent,Sale,Unit Test Comment\n';
         
       	Test.startTest();
		
		//Replicate the CSV upload
		ThirdPartyCallLogger tpc = new ThirdPartyCallLogger();
		tpc.nameFile = '';
		tpc.contentFile = Blob.valueOf(csv);
		tpc.readFile();
        Test.stopTest();
        
        //Test that it doesn't fall over on an empty row
        System.assertEquals(tpc.totalCallsAdded, 1);
    }

	public static TestMethod void uploadFileWithBlankEmail() {
		List<Account> accounts = TestAccountHelper.createAccounts(1);
		
        String csv = 'Email Address,DateTime logged,Agent Name,Call Result,Comments\n';
        csv += ',01/01/2014,TestAgent,Sale,Unit Test Comment\n'; //No email address
        csv += accounts[0].PersonEmail + ',01/01/2014,TestAgent,Sale,Unit Test Comment\n';
         
       	Test.startTest();
		
		//Replicate the CSV upload
		ThirdPartyCallLogger tpc = new ThirdPartyCallLogger();
		tpc.nameFile = '';
		tpc.contentFile = Blob.valueOf(csv);
		tpc.readFile();
        Test.stopTest();
        
        //Test that it doesn't fall over on an empty row
        System.assertEquals(tpc.totalCallsAdded, 1);
    }
    
    public static TestMethod void uploadFileWithNoLineBreaks() {
        String csv = 'Email Address,DateTime logged,Agent Name,Call Result,Comments';
        csv += 'valid.email@testbucket.net,01/01/2014,TestAgent,Sale,Unit Test Comment';
        
        PageReference pr = new PageReference('/apex/third_party_call_logger');
        Test.setCurrentPage(pr);
        
        Test.startTest();
        
        ThirdPartyCallLogger tpc = new ThirdPartyCallLogger();
		tpc.nameFile = '';
		tpc.contentFile = Blob.valueOf(csv);
		tpc.readFile();
        Test.stopTest();
        
        System.assert(ApexPages.getMessages().size() == 1);
        System.assertEquals(ApexPages.getMessages().get(0).getDetail(), 'Import file does not contain any line breaks! Please check your file format and ensure that each Call entry is given on a new line and columns are separated by commas.');
    }

	public static TestMethod void uploadFileWithRLineBreaks() {
		List<Account> accounts = TestAccountHelper.createAccounts(1);
		
        String csv = 'Email Address,DateTime logged,Agent Name,Call Result,Comments\r';
        csv += accounts[0].PersonEmail + ',01/01/2014,TestAgent,Sale,Unit Test Comment\r';
         
       	Test.startTest();
		
		//Replicate the CSV upload
		ThirdPartyCallLogger tpc = new ThirdPartyCallLogger();
		tpc.nameFile = '';
		tpc.contentFile = Blob.valueOf(csv);
		tpc.readFile();
        Test.stopTest();
        
        //Test that it doesn't fall over on an empty row
        System.assertEquals(tpc.totalCallsAdded, 1);
    }

}