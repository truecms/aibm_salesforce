@isTest
public with sharing class TestAccountSchedule {

	public static TestMethod void testScheduleJob() {
				
		Test.startTest();
		AccountSchedule.startDefaultTime();
		Test.stopTest();
		
		List<CronTrigger> ctList = [SELECT Id, CronExpression, TimesTriggered, NextFireTime, PreviousFireTime, State FROM CronTrigger];
		boolean activeAccount = false;
		for (CronTrigger ct : ctList) {
			if (ct.TimesTriggered == 0 && ct.CronExpression == '0 0 1 * * ?')
				activeAccount = true;
		}
		
		System.assert(activeAccount);
	}

}