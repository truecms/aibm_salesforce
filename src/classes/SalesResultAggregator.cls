/**
* Schedulable job that inserts a record per sales user for their weekly sales stats
* I.e. the number of winbacks, new subs and late renewals for that week
* 
* Author: tailbyr
* Copyright: EurekaReport.com.au
*/
global class SalesResultAggregator implements Schedulable {

    List<Sales_Result__c> salesResults { get; set; }
    Map<String, Map<String, Integer>> callResultStructure { get; set; }

    global void execute(SchedulableContext ctx) {
        
        //Populate the Sales User lookup object
        Map<String, User> salesUsers = getSalesUser();
        
        //Run the commission report for specific date range based on execution day
        CommissionReport cr = new CommissionReport();
        cr.dateInstance.Start_Date__c = Date.today().addDays(-6);
        cr.dateInstance.End_Date__c = Date.today();
        cr.initData(); //@TODO: create a way to constructing the CommissionReport without running initData initially for wrong dates! 
        
        if (cr.summaryResults.size() > 0)
            salesResults = new List<Sales_Result__c>();
        
        //Populate the call summary stats as well - stored as JSON on the Sales Result obj
        getCallStats(cr.dateInstance.Start_Date__c, cr.dateInstance.End_Date__c);
        
        //Use the summary data to insert into Sales_Result__c per user
        for (CommissionReport.SummaryRow summRow : cr.summaryResults){
            if (salesUsers.containsKey(summRow.salesperson)){
            	
            	String jsonCalls = null;
            	if (callResultStructure.containsKey(summRow.salesperson))
            		jsonCalls = JSON.serialize(callResultStructure.get(summRow.salesperson));
                
                createSalesResult(summRow, salesUsers.get(summRow.salesperson), jsonCalls);
            }
        }
        
        //Finally save the Sales Result and exit
        if (salesResults != null && salesResults.size() > 0)
            saveSalesResults();
    }
    
    //Creates the sales stats of that week for that user to db
    private void createSalesResult(CommissionReport.SummaryRow weeklySales, User salesAgent, String jsonCalls) {
        
        Sales_Result__c sr = new Sales_Result__c();
        sr.OwnerId = salesAgent.Id;
        sr.Actual_Days_Worked__c = 5; //Default value, can be changed in the report interface
        sr.Week_Ending__c = Date.today();
        sr.New__c = weeklySales.newCount;
        sr.Late_Renewal__c = weeklySales.lateRenewalCount;
        sr.Winback__c = weeklySales.winbackCount;
        sr.Average_Value__c = (weeklySales.totalSumValue / weeklySales.totalCount).setScale(2);
        
        sr.Call_Result__c = jsonCalls;
        
        salesResults.add(sr);
    }
    
    private void saveSalesResults(){
        insert salesResults;
    }
    
    private Map<String,User> getSalesUser(){
        Map<String, User> salesUserMap = new Map<String, User>();
        
        List<User> users = [SELECT Id, Name FROM User WHERE IsActive = true];
        for(User u : users) {
            salesUserMap.put(u.Name, u); //Keyed on Name as this is what's used in the CommissionReport SummaryRow
        }
        
        return salesUserMap;
    }
    
    
    
    //Get all the call stats, grouped by sales user first and then by week ending. One stat per possible call result value
	public void getCallStats(Date startDate, Date endDate){
		
		//Our data structure does not need dates - the Sales Result record is for one given week only
		this.callResultStructure = new Map<String, Map<String, Integer>>();  //Map<Salesperson, Map<Call Result, COUNT>>
		
		//Start with finding all the possible Call Results
		Schema.Describefieldresult callResultField = Task.Call_Result__c.getDescribe();
		List<Schema.PicklistEntry> callResultVals = callResultField.getPicklistValues();
		
		//Create the Call Result template - Map with all possible call results and zero values
		Map<String, Integer> callResultMapTemplate = new Map<String, Integer>();
		
		for (Integer i = 0; i < callResultVals.size(); i++) {
			String callResult = callResultVals[i].getValue();
			
			callResultMapTemplate.put(callResult, 0);
		}
		
		//Add the "No Call Result" option
		callResultMapTemplate.put('No Call Result', 0);
		
		
		//Create dateTime objects for the start and end dates
		Datetime startDatetime = Datetime.newInstance(startDate.year(), startDate.month(), startDate.day(), 0, 0, 0);
		Datetime endDatetime = Datetime.newInstance(endDate.year(), endDate.month(), endDate.day(), 23, 59, 59);
		
		List<AggregateResult> callSummary = [SELECT OwnerId, Owner.Name ownerName, Call_Result__c, 
												COUNT(Id) callNum
												FROM Task 
												WHERE LastModifiedDate > :startDateTime
												AND LastModifiedDate < :endDateTime
												AND Status != 'Not Started'
												GROUP BY OwnerId, Owner.Name, Call_Result__c
												ORDER BY Call_Result__c];
												
												

												
												
		for (AggregateResult ar : callSummary) {
			
			String owner = (String)ar.get('ownerName');
			
			String callResult = (String)ar.get('Call_Result__c'); 
			if (callResult == null)
				callResult = 'No Call Result';
			
			
			if (callResultStructure.containsKey(owner)){
				
				Map<String, Integer> callResultMap = callResultStructure.get(owner);
				
				if (callResultMap.containsKey(callResult)){ //If its not there, we're not interested in it!
					callResultMap.put(callResult, (Integer)ar.get('callNum'));
				}
				
				//Put the updated callResultMap back in our structure 
				callResultStructure.put(owner, callResultMap);
			}
			else {
				
				//Clone our template so we can apply values to the call results for this owner (should be where Owner does not already have a map)
				Map<String, Integer> callResultMap = callResultMapTemplate.clone();
				
				if (callResultMap.containsKey(callResult)){
					callResultMap.put(callResult, (Integer)ar.get('callNum'));
				}
				
				//Put the updated callResultMap back in our structure
				this.callResultStructure.put(owner, callResultMap);
			}
		}
	}
    
    
    
    /*
        Method used to create the scheduling of this Batch Job. This is used by the 'TestScheduledJobs' function which enables the scheduled jobs to be 
        setup and removed programmatically (also via API for Jenkins build job)
    */
    public static void setupSchedule(String jobName, String scheduledTime) { 
        SalesResultAggregator scheduled_task = new SalesResultAggregator();
        String payment_id = System.schedule(jobName, scheduledTime, scheduled_task);
    }

}