public with sharing class MergeAndLinkAccount 
{
	public Account masterAccount {get; set; }
	public Contact masterContact {get; set; }
	
	//Master Account linked objects
	public List<Product_Instance__c> masterProducts {get; set; }
	public List<go1__CreditCardAccount__c> masterCreditCards {get; set; } 
	public List<go1__Recurring_Instance__c> masterRecurring {get; set; }
	public List<go1__Transaction__c> masterTransactions {get; set; }
	public List<Note> masterNotes {get; set; }
	
	//Child Account linked objects
	public List<Product_Instance__c> childProducts {get; set; }
	public List<go1__CreditCardAccount__c> childCreditCards {get; set; }
	public List<go1__Recurring_Instance__c> childRecurring {get; set; }
	public List<go1__Transaction__c> childTransactions {get; set; }
	public List<Note> childNotes {get; set; }
	
	public Account childAccount {get; set; }
	public Contact childContact {get; set; }
	
	public Note masterReasonNote {get; set; } //Default text - explains that record xyz has been imported in
	public Note childReasonNote {get; set; }
	
	public Integer chosenAccount {get; set;}
	public String chosenEmail {get; set;}
	
	public boolean showForm {get; set; }
	
	
	public MergeAndLinkAccount(ApexPages.StandardController stdController)
	{
  		stdController.addFields(new List<String>{'PersonEmail'});	
		this.masterAccount = (Account)stdController.getRecord();
		this.masterContact = [SELECT Id, AccountId FROM Contact WHERE AccountId = :this.masterAccount.Id];
		
		//Initiate the child objects so that they persist when AJAX submits occur 
		this.childAccount = new Account();
		this.childContact = new Contact();
		
		//Load the Master Account records on construction if pre-selected
		if (this.masterContact.AccountId!=null)
		{
			loadMasterAccount();
		}
		
		// Create the notes against both accounts so that both accounts will have an explanation as to why they were linked/merged	
		this.masterReasonNote = new Note(IsPrivate=false);
		this.childReasonNote = new Note(Title='Account Merged into ' + this.masterAccount.Name + ' Account', IsPrivate=false);
		
		//Set default values for the radio buttons
		this.chosenAccount = 1;
		this.chosenEmail = this.masterAccount.PersonEmail;
		
		this.showForm = true;
	}
	
	public void loadMasterAccount()
	{
		this.masterAccount = [SELECT Id, PersonEmail, Name FROM Account WHERE Id = :this.masterContact.AccountId];
		
		this.masterProducts = Database.query(getCreatableFieldsSOQL('Product_Instance__c', 'Person_Account__c=\'' + this.masterAccount.Id + '\''));
		
		//this.masterProducts = [SELECT Id, Name, Subscription_Product__c, Start_Date__c, End_Date__c, Amount__c FROM Product_Instance__c WHERE Person_Account__c = :this.masterAccount.Id];
		this.masterCreditCards = [SELECT Id, Name, Preferred_Active_Card__c, go1__Expiry__c, Expiry_Date__c FROM go1__CreditCardAccount__c WHERE go1__Account__c = :this.masterAccount.Id];
		this.masterRecurring = [SELECT Id, Name, go1__Amount__c, go1__Remaining_Attempts__c, go1__Next_Renewal__c FROM go1__Recurring_Instance__c WHERE go1__Account__c = :this.masterAccount.Id];
		this.masterTransactions = [SELECT Id, Name, go1__Description__c, go1__Date__c, go1__Amount__c FROM go1__Transaction__c WHERE go1__Account__c = :this.masterAccount.Id];
		this.masterNotes = [SELECT Id, Title, IsPrivate, CreatedBy.Name, Body FROM Note WHERE ParentId = :this.masterAccount.Id];
	}
	
	public void loadChildAccount()
	{
		this.childAccount = [SELECT Id, PersonEmail, Name FROM Account WHERE Id = :this.childContact.AccountId];
		
		this.childProducts = Database.query(getCreatableFieldsSOQL('Product_Instance__c', 'Person_Account__c=\'' + this.childAccount.Id + '\''));
		
		//this.childProducts = [SELECT Id, Name, Subscription_Product__c, Start_Date__c, End_Date__c, Amount__c FROM Product_Instance__c WHERE Person_Account__c = :this.childAccount.Id];
		this.childCreditCards = [SELECT Id, Name, Preferred_Active_Card__c, go1__Expiry__c, Expiry_Date__c FROM go1__CreditCardAccount__c WHERE go1__Account__c = :this.childAccount.Id];
		this.childRecurring = [SELECT Id, Name, go1__Amount__c, go1__Remaining_Attempts__c, go1__Next_Renewal__c FROM go1__Recurring_Instance__c WHERE go1__Account__c = :this.childAccount.Id];
		this.childTransactions = [SELECT Id, Name, go1__Description__c, go1__Date__c, go1__Amount__c FROM go1__Transaction__c WHERE go1__Account__c = :this.childAccount.Id];
		this.childNotes = [SELECT Id, Title, IsPrivate, CreatedBy.Name, Body FROM Note WHERE ParentId = :this.childAccount.Id];
	}
	
	public void submitForm()
	{
		String chosenId = null;
		String chosenAccountName = null;
		
		//Figure out which account should remain and which should be cancelled
		if (chosenAccount == 1) {
			chosenId = this.masterAccount.Id;
			chosenAccountName = this.masterAccount.Name;
			
			//Also transfer over the records from child to master
			transferProducts(this.childProducts, this.masterAccount.Id); 
			
			transferCreditCards(this.childCreditCards, this.masterAccount.Id, this.masterCreditCards);
			transferRecurring(this.childRecurring, this.masterAccount.Id);
			transferTransactions(this.childTransactions, this.masterAccount.Id);
			transferNotes(this.childNotes, this.masterAccount.Id);
			
			operateOnTwoAccounts(this.masterAccount, this.childAccount);
		}
		else {
			chosenId = this.childAccount.Id;
			chosenAccountName = this.childAccount.Name;
			
			//Also transfer over the records from master to child
			transferProducts(this.masterProducts, this.childAccount.Id);
			transferCreditCards(this.masterCreditCards, this.childAccount.Id, this.childCreditCards);
			transferRecurring(this.masterRecurring, this.childAccount.Id);
			transferTransactions(this.masterTransactions, this.childAccount.Id);
			transferNotes(this.masterNotes, this.childAccount.Id);
			
			operateOnTwoAccounts(this.childAccount, this.masterAccount);
		}
					
		//Finally set the 'showDone' boolean to true so that the window gets updated
		String status = 'The Accounts have been merged sucessfully! Click <a href="/'+ chosenId +'">here</a> to view ' + chosenAccountName;
		ApexPages.Message msgErr = new ApexPages.Message(ApexPages.Severity.CONFIRM, status);
		ApexPages.addmessage(msgErr);
		
		this.showForm = false; //And hide the form as we're all done here
	}
	
	
	private void operateOnTwoAccounts(Account chosenAcc, Account mergedAcc)
	{
		//Save the notes objects against the relevant accounts
		List<Note> notes = new List<Note>();
		
		this.masterReasonNote.Title = mergedAcc.Name + ' account has been merged and linked to this Account';
		this.masterReasonNote.ParentId = chosenAcc.Id;
		notes.add(this.masterReasonNote); 
		
		this.childReasonNote.ParentId = mergedAcc.Id;
		notes.add(this.childReasonNote);
		
		insert notes;
		
		//Deactivate the account NOT selected to be 'chosenAccount'
		mergedAcc = DeactivateAccount.cancelAccount(mergedAcc);
		
		//Set the 'Merged Into Account' field on the mergedAcc
		mergedAcc.Merged_Into_Account__c = chosenAcc.Id;
		update mergedAcc;
		
		//Set the desired email address of the chosen account (if necessary)
		List<Account> accs = new List<Account>();
		
		Account updateChosenAcc = [SELECT Id, PersonEmail FROM Account WHERE Id = :chosenAcc.Id LIMIT 1];
		updateChosenAcc.PersonEmail = this.chosenEmail;
		
		//Can we wait 1 second between these DML operations??? No sleep implementation available - below is the best option, although could lead to max script statement limit. 
		//TODO: replace with better implementation or remove when integration solution is in place
		final Long timeAfterInsertingAccount = Datetime.now().getTime();
 		while (timeAfterInsertingAccount > Datetime.now().getTime() - 1000) {
		}
		
		update updateChosenAcc;
		 
	}
	
	private void transferProducts(List<Product_Instance__c> products, Id chosenAccountId)
	{
		List<Product_Instance__c> newProds = new List<Product_Instance__c>();
		
		//If there is only 1 product being transfered, we need to capture the original start and end dates as the trigger will change them
		Date startDate = null;
		Date endDate = null;
		if (products.size()==1) {
			startDate = products[0].Start_Date__c;
			endDate = products[0].End_Date__c;	
		}
		
		for (Product_Instance__c prod : products)
		{
			//prod.Person_Account__c = chosenAccountId; //Do not want to transfer, we want to clone and then suspend
			Product_Instance__c newProd = prod.clone(false, true);
			newProd.Person_Account__c = chosenAccountId;
			newProd.Notes__c = 'Subscription migrated from account \'' + this.childAccount.Name + '\'';
			newProds.add(newProd);
			
			//Now cancel the original
			prod.End_Date__c = Date.today().addDays(-1);
			prod.Status__c = 'Cancelled';
			prod.Notes__c = 'Subscription migrated over to account \'' + this.masterAccount.Name + '\'';
		}
		
		insert newProds;
		update products;
				
		//Now we need to update the inserted Product Instances with the original dates so subscriptions are not chained together
		if (startDate!=null && endDate!=null && newProds.size()==1) {
			newProds[0].Start_Date__c = startDate;
			newProds[0].End_Date__c = endDate;
			update newProds;
		}
	}
	
	private void transferCreditCards(List<go1__CreditCardAccount__c> cards, Id chosenAccountId, List<go1__CreditCardAccount__c> chosenAccCards)
	{
		//We need to check the dates of the preferred card on the chosen account! 
		go1__CreditCardAccount__c chosenCard = null;
		boolean updateChosenCard = false;
		
		for (go1__CreditCardAccount__c card : chosenAccCards)
		{
			if (card.Preferred_Active_Card__c == true){
				chosenCard = card;
				break;
			}
		}
		
		for (go1__CreditCardAccount__c card : cards)
		{
			if (chosenCard!=null && chosenCard.Expiry_Date__c < card.Expiry_Date__c && card.Preferred_Active_Card__c == true) {
				//Set chosenCreditCardAccount as the non preferred now
				chosenCard.Preferred_Active_Card__c = false;
				updateChosenCard = true;
			}
			else
				card.Preferred_Active_Card__c = false;
				
			card.go1__Account__c = chosenAccountId;
		}
		
		update cards;
		
		if (updateChosenCard==true)
			update chosenCard;
	}
	
	private void transferRecurring(List<go1__Recurring_Instance__c> recurringProds, Id chosenAccountId)
	{
		for (go1__Recurring_Instance__c recur : recurringProds)
		{
			recur.go1__Account__c = chosenAccountId;
		}
		
		update recurringProds;
	}
	
	private void transferTransactions(List<go1__Transaction__c> transactions, Id chosenAccountId)
	{
		for (go1__Transaction__c trans : transactions)
		{
			trans.go1__Account__c = chosenAccountId;
		}
		
		update transactions;
	}
	
	private void transferNotes(List<Note> notes, Id chosenAccountId)
	{
		//Notes cannot be moved from one ParentId to another after insert - have to clone and delete
		List<Note> newNotes = new List<Note>();
		for (Note note : notes)
		{
			Note newNote = new Note(Title = note.Title, Body = note.Body, IsPrivate = note.IsPrivate, ParentId = chosenAccountId);
			newNotes.add(newNote);
		}
		
		insert newNotes;
		delete notes;
	}
	
	
	//Method to dynamically query all the fields on an object so that we can clone it sucessfully using the default method 
	public static string getCreatableFieldsSOQL(String objectName, String whereClause)
	{
        String selects = '';
         
        if (whereClause == null || whereClause == ''){ return null; }
         
        // Get a map of field name and field token
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
         
        if (fMap != null)
        {
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                if (fd.isCreateable()){ // field is creatable
                    selectFields.add(fd.getName());
                }
            }
        }
         
        if (!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
             
        }
         
        return 'SELECT Id, Name, ' + selects + ' FROM ' + objectName + ' WHERE ' + whereClause;    
    }
	
}