/**
 * SubscriberList 
 *
 * LICENSE: This source code files is subject to the terms of services as
 * outlined at http://www.go1.com.au/tos
 *
 * @author: GO1 Pty Ltd
 * @license: http://www.go1.com.au/tos
 */
public class SubscriberList {
	
	public List<DataRow> pageTable { get; set; }
	public Decimal pageTotal { get; set; }
	public Decimal total { get; set; }
	public Boolean tableIsNull { get; set; }
	
	public String subName { get; set; }
	public Subscription_Product__c productName { get; set; }
	public Date productDate { get; set; }
	public String dateLabel { get; set; }
	public String pDate { get; set; } 
	public String preLaunch { get; set; }
	public String launchDateLabel { get; set; }
	public Date launchDate { get; set; }
	public Boolean isError { get; set; }
	public String format { get; set; }
	
	public Integer PageNumber { get; set; }
	public Integer PageSize { get; set; }
	public Integer TotalSize { get; set; }
	private Integer TotalPageNumber { get; set; }
	
	public Boolean nextDisabled { get; set; }
	public Boolean prevEnabled { get; set; }
	
	/*******************************************************/
	
	public Integer getStartPageSize() {
  		return (pageNumber - 1) * pageSize + 1;
  	}
  	public Integer getEndPageSize() {
  		return (pageNumber - 1) * pageSize + pageTable.size();
  	}
  	public Integer getTotalSize() {
  		return TotalSize;
  	}
  	public Boolean getPreviousButtonEnabled(){
    	return !(pageNumber > 1);
  	}
  	public Boolean getNextButtonDisabled(){
	    if (this.tableIsNull) return true;
	    else
	    return ((pageNumber * pageSize) >= TotalSize);
  	}
  	
  	/**
  	 *
  	 */
  	public Integer getTotalPageNumber(){
	    if (totalPageNumber == 0 && !this.tableIsNull){
	    	totalPageNumber = TotalSize / pageSize;
	   		Integer mod = TotalSize - (totalPageNumber * pageSize);
	    	if (mod > 0)
	        	totalPageNumber++;
	    }
	    return totalPageNumber;
  	}
	
	/**
	 * Select list options for changing the page size
	 */
	public List<SelectOption> getPageSizes() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('25','25'));
        options.add(new SelectOption('50','50'));
        options.add(new SelectOption('100','100'));
		options.add(new SelectOption('200','200'));
        return options;
    }
	
	/**
	 * Initialises the inital variables
	 */
	public SubscriberList() {
		tableIsNull = true;
		launchDateLabel = ApexPages.currentPage().getParameters().get('launch');
		preLaunch = ApexPages.currentPage().getParameters().get('prelaunch');
		pDate = ApexPages.currentPage().getParameters().get('date');
		String pName = ApexPages.currentPage().getParameters().get('product');
		try {
			if (pDate != null && pName != null) {
				productDate = Date.parse(pDate);
				productName = [SELECT Id, Name, Subscription_Type__c FROM Subscription_Product__c WHERE Name = :pName LIMIT 1];
				dateLabel = Datetime.newInstance(productDate, Time.newInstance(0,0,0,0)).format('MMM yyyy');
				isError = false;
				
				format = ApexPages.currentPage().getParameters().get('format');
				if (format == null) {
					format = '';
				}
				
				initData();
			} else {
				//display error message
				ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: missing date or product');
				ApexPages.addMessage(error);
				isError = true;
			}
		} catch (Exception ex) {
			ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: invalid date');
			ApexPages.addMessage(error);
			isError = true;
		}
	}
	
	/** 
	 * 
	 */
	public void initData() {
		totalPageNumber = 0;
		
		String pSize = ApexPages.currentPage().getParameters().get('size');
		if (pSize != null) {
			this.pageSize = Integer.valueOf(pSize);
		} else {
			this.pageSize = 50;
		}
		String pPage = APexPages.currentPage().getParameters().get('page');
		if (pPage != null) {
			this.pageNumber = Integer.valueOf(pPage);
		} else {
			this.pageNumber = 1;
		}
		
		getSubscriptionList(0, this.PageSize);
		updateTable(this.pageNumber);
		getPageTotals();
		getTotals();
	}
	
	/**
	 * retrieves the table data for a single page
	 */
	private void getSubscriptionList(Integer min, Integer max) {
		pageTable = new List<DataRow>();
		tableIsNull = false;
		
		Date endDate = this.productDate.addMonths(1);
		if (this.productName != null) {
			// use subscription data
			List<Product_Instance__c> subscriptions = new List<Product_Instance__c>();
			if (launchDateLabel != null && launchDateLabel != '') {
			  launchDate = Date.parse(launchDateLabel);
			  if (this.productName.Subscription_Type__c == 'm') {
			    if (preLaunch != null && preLaunch != '') {
			      subscriptions = [SELECT Id, Person_Account__r.Name, Person_Account__r.PersonEmail, Deferred_Payment__c FROM Product_Instance__c WHERE Start_Date__c < :launchDate AND Start_Date__c <= :Date.today() AND ((Start_Date__c < :this.productDate.addMonths(1) AND End_Date__c >= :this.productDate.addMonths(1)) OR (Start_Date__c >= :this.productDate AND End_Date__c < :this.productDate.addMonths(1))) AND Subscription_Product__c = :this.productName.Id AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :this.productDate AND go1__Date__c < :this.productDate.addMonths(1) AND go1__Refunded__c = true) ORDER BY Person_Account__r.Name];
			    } else {
			      subscriptions = [SELECT Id, Person_Account__r.Name, Person_Account__r.PersonEmail, Deferred_Payment__c FROM Product_Instance__c WHERE Start_Date__c >= :launchDate AND Start_Date__c <= :Date.today() AND ((Start_Date__c < :this.productDate.addMonths(1) AND End_Date__c >= :this.productDate.addMonths(1)) OR (Start_Date__c >= :this.productDate AND End_Date__c < :this.productDate.addMonths(1))) AND Subscription_Product__c = :this.productName.Id AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :this.productDate AND go1__Date__c < :this.productDate.addMonths(1) AND go1__Refunded__c = true) ORDER BY Person_Account__r.Name];
			  	}
			  } else {
			  	if (preLaunch != null && preLaunch != '') {
			      subscriptions = [SELECT Id, Person_Account__r.Name, Person_Account__r.PersonEmail, Deferred_Payment__c FROM Product_Instance__c WHERE Start_Date__c < :launchDate AND ((Start_Date__c < :this.productDate.addMonths(1) AND End_Date__c >= :this.productDate.addMonths(1)) OR (Start_Date__c >= :this.productDate AND End_Date__c < :this.productDate.addMonths(1))) AND Subscription_Product__c = :this.productName.Id AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :this.productDate AND go1__Date__c < :this.productDate.addMonths(1) AND go1__Refunded__c = true) ORDER BY Person_Account__r.Name];
			  	} else {
			  	  subscriptions = [SELECT Id, Person_Account__r.Name, Person_Account__r.PersonEmail, Deferred_Payment__c FROM Product_Instance__c WHERE Start_Date__c >= :launchDate AND ((Start_Date__c < :this.productDate.addMonths(1) AND End_Date__c >= :this.productDate.addMonths(1)) OR (Start_Date__c >= :this.productDate AND End_Date__c < :this.productDate.addMonths(1))) AND Subscription_Product__c = :this.productName.Id AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :this.productDate AND go1__Date__c < :this.productDate.addMonths(1) AND go1__Refunded__c = true) ORDER BY Person_Account__r.Name];
			  	}
			  }
			} else {
			  if (this.productName.Subscription_Type__c == 'm') {
			    subscriptions = [SELECT Id, Person_Account__r.Name, Person_Account__r.PersonEmail, Deferred_Payment__c FROM Product_Instance__c WHERE Start_Date__c <= :Date.today() AND ((Start_Date__c < :this.productDate.addMonths(1) AND End_Date__c >= :this.productDate.addMonths(1)) OR (Start_Date__c >= :this.productDate AND End_Date__c < :this.productDate.addMonths(1))) AND Subscription_Product__c = :this.productName.Id AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :this.productDate AND go1__Date__c < :this.productDate.addMonths(1) AND go1__Refunded__c = true) ORDER BY Person_Account__r.Name];
			  } else {
			    subscriptions = [SELECT Id, Person_Account__r.Name, Person_Account__r.PersonEmail, Deferred_Payment__c FROM Product_Instance__c WHERE ((Start_Date__c < :this.productDate.addMonths(1) AND End_Date__c >= :this.productDate.addMonths(1)) OR (Start_Date__c >= :this.productDate AND End_Date__c < :this.productDate.addMonths(1))) AND Subscription_Product__c = :this.productName.Id AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :this.productDate AND go1__Date__c < :this.productDate.addMonths(1) AND go1__Refunded__c = true) ORDER BY Person_Account__r.Name];
			  }
			}
			this.TotalSize = subscriptions.size();
			Integer recordCount = 0;
			for (Product_Instance__c sub : subscriptions) {
				recordCount++;
				if (recordCount > min && recordCount <= max) {
					pageTable.add(new DataRow(sub.Person_Account__r.Name,String.valueOf(sub.Id),sub.Person_Account__r.PersonEmail,sub.Deferred_Payment__c));
				}
			}
		}
	}
	
	/**
	 * method to update the table variables when going to prev/next page
	 */
	public void updateTable(Integer newPageIndex) {
		totalPageNumber = this.TotalSize / this.pageSize;
		Integer mod = this.TotalSize - (this.totalPageNumber * this.pageSize);
    	if (mod > 0)
        	this.totalPageNumber++;
		
		if (newPageIndex > this.totalPageNumber) {
			newPageIndex = this.totalPageNumber;
		}
		
		pageTable = new List<DataRow>();
	  	Integer counter = 0;
	  	Integer min = 0;
  		Integer max = 0;
     	if (newPageIndex > pageNumber){
        	min = pageNumber * pageSize;
        	max = newPageIndex * pageSize;
      	} else {
			max = newPageIndex * pageSize;
			min = max - pageSize;
     	}
     	getSubscriptionList(min, max);
		pageNumber = newPageIndex;
		
		this.nextDisabled = getNextButtonDisabled();
		this.prevEnabled = getPreviousButtonEnabled();
	}
	
	/**
	 * loop through the table data and calculate the totals for each column
	 */
	private void getPageTotals() {
		pageTotal = 0;
		for (DataRow product : this.pageTable) {
			pageTotal += product.value;
		}
	}
	
	/**
	 * retrieve the full total amount for the given month
	 */
	private void getTotals() {
		total = 0;
		
		Date endDate = this.productDate.addMonths(1);
		SObject subscriptions;
		if (launchDateLabel != null && launchDateLabel != '') {
			launchDate = Date.parse(launchDateLabel);
			if (this.productName.Subscription_Type__c == 'm') {
				if (preLaunch != null && preLaunch != null) {
					subscriptions = [SELECT SUM(Deferred_Payment__c) TotalAmount FROM Product_Instance__c WHERE Start_Date__c < :launchDate AND Start_Date__c <= :Date.today() AND ((Start_Date__c < :this.productDate.addMonths(1) AND End_Date__c >= :this.productDate.addMonths(1)) OR (Start_Date__c >= :this.productDate AND End_Date__c < :this.productDate.addMonths(1))) AND Subscription_Product__c = :this.productName.Id AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :this.productDate AND go1__Date__c < :this.productDate.addMonths(1) AND go1__Refunded__c = true)];
				} else {
					subscriptions = [SELECT SUM(Deferred_Payment__c) TotalAmount FROM Product_Instance__c WHERE Start_Date__c >= :launchDate AND Start_Date__c <= :Date.today() AND ((Start_Date__c < :this.productDate.addMonths(1) AND End_Date__c >= :this.productDate.addMonths(1)) OR (Start_Date__c >= :this.productDate AND End_Date__c < :this.productDate.addMonths(1))) AND Subscription_Product__c = :this.productName.Id AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :this.productDate AND go1__Date__c < :this.productDate.addMonths(1) AND go1__Refunded__c = true)];
				}
			} else {
				if (preLaunch != null && preLaunch != null) {
					subscriptions = [SELECT SUM(Deferred_Payment__c) TotalAmount FROM Product_Instance__c WHERE Start_Date__c < :launchDate AND ((Start_Date__c < :this.productDate.addMonths(1) AND End_Date__c >= :this.productDate.addMonths(1)) OR (Start_Date__c >= :this.productDate AND End_Date__c < :this.productDate.addMonths(1))) AND Subscription_Product__c = :this.productName.Id AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :this.productDate AND go1__Date__c < :this.productDate.addMonths(1) AND go1__Refunded__c = true)];
				}else {
					subscriptions = [SELECT SUM(Deferred_Payment__c) TotalAmount FROM Product_Instance__c WHERE Start_Date__c >= :launchDate AND ((Start_Date__c < :this.productDate.addMonths(1) AND End_Date__c >= :this.productDate.addMonths(1)) OR (Start_Date__c >= :this.productDate AND End_Date__c < :this.productDate.addMonths(1))) AND Subscription_Product__c = :this.productName.Id AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :this.productDate AND go1__Date__c < :this.productDate.addMonths(1) AND go1__Refunded__c = true)];
				}
			}
		} else {
			if (this.productName.Subscription_Type__c == 'm') {
				subscriptions = [SELECT SUM(Deferred_Payment__c) TotalAmount FROM Product_Instance__c WHERE Start_Date__c <= :Date.today() AND ((Start_Date__c < :this.productDate.addMonths(1) AND End_Date__c >= :this.productDate.addMonths(1)) OR (Start_Date__c >= :this.productDate AND End_Date__c < :this.productDate.addMonths(1))) AND Subscription_Product__c = :this.productName.Id AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :this.productDate AND go1__Date__c < :this.productDate.addMonths(1) AND go1__Refunded__c = true)];
			} else {
				subscriptions = [SELECT SUM(Deferred_Payment__c) TotalAmount FROM Product_Instance__c WHERE ((Start_Date__c < :this.productDate.addMonths(1) AND End_Date__c >= :this.productDate.addMonths(1)) OR (Start_Date__c >= :this.productDate AND End_Date__c < :this.productDate.addMonths(1))) AND Subscription_Product__c = :this.productName.Id AND Status__c != 'Cancelled' AND Id NOT IN (SELECT Subscription_Instance__c FROM go1__Transaction__c WHERE go1__Date__c >= :this.productDate AND go1__Date__c < :this.productDate.addMonths(1) AND go1__Refunded__c = true)];
			}
		}
		total = (Decimal)subscriptions.get('TotalAmount');
	}
  	
  	/**
   	 * ajax call to request all transactions that belong to the next page
   	 */
  	public PageReference changePageSize() {
		updateTable(pageNumber);
		getPageTotals();
		return null;
  	}
  	
  	/**
   	 * ajax call to request all transactions that belong to the next page
   	 */
  	public PageReference nextBtnClick() {		
		updateTable(pageNumber + 1);
		getPageTotals();
		return null;
  	}
  
  	/**
   	 * ajax call to request all transactions that belong to the previous page
	 */
  	public PageReference previousBtnClick() {
		updateTable(pageNumber - 1);
		getPageTotals();
		return null;	
	}
	
	/****************************************************************/
	/**
	 *
	 */
	public class DataRow {
		
		public String label { get; set; }
		public String id { get; set; }
		public String email { get; set; }
		public Decimal value { get; set; }
		
		public dataRow(String label, String id, String email, Decimal value) {
			this.label = label;
			this.id = id;
			this.email = email;
			this.value = value;
		}
	}
}