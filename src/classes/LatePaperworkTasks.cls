/**
* Batch job that runs nightly to pull all Accounts that have signed up to OneVue platform but not returned their paperwork
* A task for the Sales Team to follow up will be created for each of these customers 
* Author: tailbyr
* Copyright: EurekaReport.com.au
*/
public with sharing class LatePaperworkTasks implements Database.Batchable<Account>, Database.Stateful {
	
	public static String lastSalesTeamMember { get; set; }
	
	public TaskManager tm {get; set;}
	
	public String latePaperworkQueue {get; set;}
	public Boolean latePaperworkRules {get; set;}
	
	public Integer daysLate {get; set;}
	public String subject {get; set;}
		
	//Constructor - get the lastSalesTeamMember given one of the 'Late Paperwork' tasks
	public LatePaperworkTasks(Integer daysLate) {
		this.daysLate = daysLate;
		
		this.subject = 'no match';
		if (daysLate != null) this.subject = TaskManager.LATE_PAPERWORK_SUBJECT.replace('numDays', String.valueOf(math.abs(daysLate)));
		
		lastSalesTeamMember = AccountBDHelper.getLastTaskOwner(this.subject);
			
		//Get the customer settings allocation rule and queue for this Expired FT automated task creator
		Allocation_Rules__c allocationRules = Allocation_Rules__c.getInstance('Default');
		latePaperworkRules = allocationRules.Late_Paperwork_Allocation_Rule__c;
		latePaperworkQueue = allocationRules.Late_Paperwork_Queue__c;
		
		tm = new TaskManager(latePaperworkQueue);
	}
	
	//Batchable override method: start - queries the records to iterate over
	public Iterable<Account> start(Database.batchableContext BC) {
		
		Date OVRegisteredDate = System.Today().addDays(daysLate); //Not received paperwork after x number of days
		List<Account> latePaperworkAccounts = [SELECT id, OwnerId, PersonContactId
											,(SELECT Id FROM Investment_Accounts__r)
											FROM Account 
											WHERE BD_Current_Role__c != null
											AND OV_User_Object_Created__c = True
											AND OV_Status_Change_Date__c = :OVRegisteredDate
											AND OneVue_Application_Status__c = 'awaiting_paperwork'];
										
		//If the Account has related Investment Accounts, the paperwork has definitely been received! Not sure if the status updates at this stage until funds are received, this ensures we don't follow up when we don't need to 
		for (Integer i = 0; i < latePaperworkAccounts.size(); i++) {
			if (latePaperworkAccounts[i].Investment_Accounts__r.size() > 0){
				latePaperworkAccounts.remove(i);
				i--;
			}
		}
		
		return latePaperworkAccounts;
	}
	
	//Batchable override method: execute
	public void execute(Database.BatchableContext BC, List<Account> instances) {
		AccountBDHelper accHelper = new AccountBDHelper(instances);
		accHelper.createBrightDayTasks(latePaperworkRules, tm, this.subject);
	}
	
	//Batchable override method: finish - called when all batches are processed; allocates all fair tasks created in a fair manner across Acc mgrs
	public void finish(Database.BatchableContext BC) {
		
		AccountBDHelper.fairlyAllocateTasks(this.subject, this.tm, lastSalesTeamMember);
	}
}