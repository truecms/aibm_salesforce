public without sharing class MonthlyDueReport {
	
	public static final Integer orgFiscalMonth = [SELECT FiscalYearStartMonth FROM Organization].FiscalYearStartMonth;
	
	public Date financialYearStart {get; set;}
	public Date financialYearEnd {get; set;}
	
	public String fyYearSelection {get; set;}
	public String financialYear {get; set;}
	
	public List<DataRow> results {get; set;}
	
	//TEST OBJECT COLLECTION - Use if you need to see output of the subs considered per month
	/*public transient List<TestObject> testObjs {get; set;}*/
	
	public MonthlyDueReport() {
		//Find the current financial year
		this.financialYearStart = this.findFinancialYear();
		this.financialYearEnd = this.getFinancialYearEnd(this.financialYearStart);
		
		//Set the default SelectOption for the financial year drop-down
		this.fyYearSelection = String.valueOf(this.financialYearStart.year());
		
		/*testObjs = new List<TestObject>();*/
		
		initData();
	}
	
	//Find the start date of the financial year based on the Orgs chosen fiscal year start month
	private Date findFinancialYear(){
		Integer currentMonth = system.today().month();
		Integer currentFiscalYear = system.today().year();
		
		if (currentMonth < orgFiscalMonth){
		    currentFiscalYear--;
		}
		
		return findSelectedFinancialYear(currentFiscalYear);
	}
	
	private Date findSelectedFinancialYear(Integer fyStart){
		Date orgFiscalYear = Date.newinstance(fyStart, orgFiscalMonth, 1);
		return orgFiscalYear;
	}
	
	
	//Get the last day of the financial year
	private Date getFinancialYearEnd(Date financialYearStart){
		return financialYearStart.addYears(1).addDays(-1);
	}
	
	//Generate the formatted financial year string for display on the page and use in the year selector
	private String formatFinancialYear(Date financialYearStart, Date financialYearEnd){
		return financialYearStart.year() + '/' + financialYearEnd.year();
	}
	
	public void initData() {
		this.results = new List<DataRow>();
		
		//Initialise the chosen (or default on first load) financial year to display
		this.financialYear = this.formatFinancialYear(this.financialYearStart, this.financialYearEnd);
		
		Integer numMonths = 12; //Think this will be static - not report more than 1 financial year at a time
		for (Integer i = 0; i < numMonths; i++) {
			
			Date startDate = financialYearStart.addMonths(i);
			Date endDate = financialYearStart.addMonths(i+1);
		
			//Even though SOQL queries in loops are BAD, we are limiting this to 12 recursions guaranteed only - workaround for aggregation limitations of SOQL!	
			queryResults(startDate, endDate);
		}
		
	}
	
	public void queryResults(Date startDate, Date endDate) {
		
		try {
			//TODO: we can remove the incorrect max and min date values from this query
			List<AggregateResult> trans = [SELECT 
												go1__Account__c,
												Subscription_Instance__r.Subscription_Type__c sub_type,
												MIN(go1__Date__c) first_payment_date,
												MAX(go1__Date__c) last_payment_date,
												MAX(Subscription_Instance__r.Start_Date__c) max_start,
												MAX(Subscription_Instance__r.End_Date__c) max_due
												FROM
												go1__Transaction__c
												WHERE ((go1__Amount__c > 0 AND Subscription_Instance__r.Subscription_Type__c = 'y') 
														OR (go1__Amount__c > 0 AND Subscription_Instance__r.Subscription_Type__c = 'm'))
												AND go1__Refunded__c != TRUE
												AND Subscription_Instance__r.Status__c != 'Cancelled'
												AND go1__Status__c IN ('Approved','Success','Successful Transaction','Successfully added manual transaction') 
												AND Subscription_Instance__r.End_Date__c >= :startDate
												AND Subscription_Instance__r.End_Date__c < :endDate
												GROUP BY go1__Account__c, Subscription_Instance__r.Subscription_Type__c, go1__Account__r.Eureka_ID__c];
			
			//One new DataRow instantiation per month for a financial year
			DataRow dr = new DataRow(startDate);
			
			//We need a List of Account Ids for the correct end date collections
			List<Id> accIds = new List<Id>();
			
			for (AggregateResult ar : trans) {
													
				accIds.add((String)ar.get('go1__Account__c')); //Grabbing this for next query
				
				if (ar.get('sub_type') == 'y') {
					dr.yearlyDue++;
				}
				else if (ar.get('sub_type') == 'm') {
					dr.monthlyDue++;
				}
				
			}
			
			List<AggregateResult> aggResSubs = [SELECT
												Person_Account__c,
												//Person_Account__r.Eureka_ID__c eurekaId,
												MAX(CreatedDate) max_create,
												MAX(Start_Date__c) max_start,
												MAX(End_Date__c) max_due
												FROM Product_Instance__c
												WHERE Person_Account__c in :accIds
												AND Subscription_Type__c in ('y', 'm')
												AND Status__c != 'Cancelled'
												AND End_Date__c > :this.financialYearEnd.addDays(1)
												GROUP BY Person_Account__c, Person_Account__r.Eureka_ID__c];
							
												
			//Map<String, Date[]> subRes = new Map<String, Date[]>();
			for (AggregateResult ar : aggResSubs) {
					
					Date piCreatedDate = ((Datetime)ar.get('max_create')).date();
					Date maxStart = (Date)ar.get('max_start');
					Date maxDue = (Date)ar.get('max_due');
					
					if (piCreatedDate < maxStart) {
						dr.earlyRenew++;
						
						/*TestObject testObj = new TestObject((String)ar.get('Person_Account__c'), (String)ar.get('eurekaId'));
						testObj.maxStart = maxStart;
						testObj.maxDue = maxDue;
						testObj.createdDate = piCreatedDate;
						testObj.earlyRenew = true;
						testObjs.add(testObj);*/
					}
					
			}
			
			results.add(dr);
		
		}
		catch(Exception e) {
			System.debug('DEBUG: Exception thrown:' + e.getMessage() + '. StackTrace:' + e.getStackTraceString());
		}
	}
	
	//Populate the dropdown list for financial year data
	public List<SelectOption> getFinancialYearOptions() {
	    List<SelectOption> options = new List<SelectOption>();
	    Date todayFy = this.findFinancialYear();
	    for (Integer i = -2; i < 4; i++) {
	    	Date fyStart = todayFy.addYears(i);
	    	Date fyEnd = getFinancialYearEnd(fyStart);
	    	options.add(new SelectOption(String.valueOf(fyStart.year()), formatFinancialYear(fyStart, fyEnd)));
	    }
	    return options;
	}
	
	
	public void selectFinancialYear(){
		this.financialYearStart = findSelectedFinancialYear(Integer.valueOf(fyYearSelection));
		this.financialYearEnd = getFinancialYearEnd(this.financialYearStart);
		this.initData();
	}
	
	
	//Test Class - used for displaying the data rows applicable to any particular month
	/*public class TestObject {
		public String subId {get; set;}
		public String subType {get; set;}
		public Datetime firstPaymentDate {get; set;}
		public Datetime lastPaymentDate {get; set;}
		public Date firstPayment {get; set;}
		public Date lastPayment {get; set;}
		public Date maxStart {get; set;}
		public Date maxDue {get; set;}
		public Date createdDate {get; set;}
		public Boolean earlyRenew {get; set;}
		
		public TestObject(){}
		
		public TestObject(String accId, String eurekaId) {
			if (eurekaId != null)
				this.subId = eurekaId;
			else
				this.subId = accId;
		}
		
		public TestObject(String accId, String eurekaId, String subType, Datetime firstPaymentDate, Datetime lastPaymentDate, Date maxStart, Date maxDue) {
			this(accId, eurekaId);
			this.subType = subtype;
			this.firstPaymentDate = firstPaymentDate;
			this.lastPaymentDate = lastPaymentDate;
			this.maxStart = maxStart;
			this.maxDue = maxDue;
			this.earlyRenew = false;
		}
		
	}*/
	
	
	/**
	* Object class to hold values of each data row in the results (max 12!)
	*/
	public class DataRow {
		public String monthDueFormatted {get; set;}
		public Integer yearlyDue {get; set;}
		public Integer monthlyDue {get; set;}
		public Integer totalDue {
			get{
				return this.yearlyDue + this.monthlyDue;
			} 
			
			set;}
		public Integer earlyRenew {get; set;}
		
		public DataRow(Date monthDue){
			this.monthDueFormatted = monthDue.month() + '-' + monthDue.year();
			this.yearlyDue = 0;
			this.monthlyDue = 0;
			this.totalDue = 0;
			this.earlyRenew = 0;
		}
	}
}