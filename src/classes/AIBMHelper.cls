/**
 * AIBMHelper is a list of functions used throughout the codebase specific to AIBM business rules
 * or providing common utility functionality that can be referenced by any other classes.
 *
 * @author: GO1 Pty Ltd
 * @updatedBy: Ross Tailby
 * @license: http://www.eurekareport.com.au
 */
public with sharing class AIBMHelper {
	
	public static final String PRODUCTION_ORG = '00D90000000czlOEAQ';
	
    /**
     * Get the Credit Card surcharge
     */
	public static Decimal getCreditCardSurcharge(String cardtype) {
		Decimal surcharge = 0; // no surcharge
		if (cardtype == null) {
			surcharge = 0;
		} else if (cardtype.toLowerCase() == 'amex') {
    		surcharge = 0.022; // add 2.2% fee
    	} else if (cardtype.toLowerCase() == 'diners' || cardtype.toLowerCase() == 'dc') {
    		surcharge = 0.011; // add 1.1% fee
    	}
    	return surcharge;
	}
	
	/**
	 * Calculates the amount and surcharge together
	 */
	public static Decimal getAmountWithSurcharge(Decimal amount, Decimal surcharge) {
		if (surcharge != null) {
			Decimal totalAmount = amount * (1 + surcharge);
			return totalAmount;
		} else {
			return amount;
		}
	}
	
	/**
	 * Removes the surcharge from the total amount
	 */
	public static Decimal getAmountWithoutSurcharge(Decimal totalAmount, Decimal surcharge) {
		if (surcharge != null) {
			Decimal amount = totalAmount / (1 + surcharge);
			return amount;
		} else {
			return totalAmount;
		}
	}
	
    /**
     * Calculate the next renewal date
     */
	public static DateTime evaluateNextRenewal(Datetime startDate, String interval, Boolean payNow) {
        // change the default duration
        Datetime nextRenewal = startDate;
        if (payNow) {
            if (interval == 'm' || interval.toLowerCase() == 'monthly') {
                nextRenewal = startDate.addMonths(1);
            } else if (interval == 'y' || interval.toLowerCase() == 'yearly') {
                nextRenewal = startDate.addYears(1);
            }
        }
        return nextRenewal;
    }
    
    /**
     * Calculate the next renewal date
     */
	public static DateTime evaluateNextRenewalFromPayments(Datetime startDate, String interval, Integer numberOfPayments) {
        // change the default duration
        Datetime nextRenewal = startDate;
        if (interval != null) {
        	if (interval == 'm' || interval.toLowerCase() == 'monthly') {
            	nextRenewal = startDate.addMonths(numberOfPayments);
	        } else if (interval == 'y' || interval.toLowerCase() == 'yearly') {
    	        nextRenewal = startDate.addYears(numberOfPayments);
        	}
        }
        return nextRenewal;
    }
	
	
	/**
     * Converts the num and period values to a number of months
     * Uses the interval to divide the number of months into a number of attempts
     */
    public static Decimal evaluateNumberAttempts(Decimal num, String period, String interval, Boolean payNow) {
        // change the default duration
        Decimal months = 0;
        Decimal attempts = 0;
        
        // Calculate number of months
        if (period == 'day') {
            months = Math.floor(num/30);
        } else if (period == 'week') {
            months = Math.floor(num/4);
        } else if (period == 'month') {
            months = Math.floor(num);
        } else if (period == 'year') {
            months = Math.floor(num * 12);
        }
        
        // calculate number of attempts
        if (interval == 'm' || interval == 'monthly') {
            attempts = Math.floor(months);
        } else if (interval == 'y' || interval == 'yearly') {
            attempts = Math.floor(months/12);
        } else {
            // interval defaults to 'once'
            attempts = 1;
        }
        
        // remove first payment if paid immediately
        if (payNow) {
        	// Only subtract 1 if attempts is greater than 0 (this will occur if duration is less than a month)
        	if (attempts > 0) {
        		attempts = attempts - 1;
        	}
        } else {
        	if (attempts <= 0) {
        		attempts = 1;
        	}
        }
        return attempts;
    }
    
    /**
     *
     */
    public static void removeAccounts(List<String> accountEmails) {
    	AccountHelper helper = new AccountHelper();
		helper.loadAccountsByEmail(accountEmails);
		helper.deleteAccounts();
    }

    
    	
	public static Boolean inWhiteListEmail(String email) {
		// we can't sent if not a real email address.
		if (email == null) {
			return false;
		}
		
		// send if in production
		if (Userinfo.getOrganizationId() == AIBMHelper.PRODUCTION_ORG) {
			return true;
		}
		else {
			List<String> whiteList = new List<String>{'@businessspectator.com.au','@eurekareport.com.au','@mailinator.com','@testbucket.net'};
			for (String white : whiteList) {
				if (email.contains(white)) {
					return true;
				}
			}
		}
		return false;
	}
	
	/* 
	 * Send email function - to be used internally for notifications and call to action
	 */
	public static Messaging.SendEmailResult[] sendEmailNotification(String toAddress, String subject, String emailBody){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
	    String[] toAddresses = new String[] {toAddress};
	    mail.setToAddresses(toAddresses); 
	    mail.setSubject(subject); 
	            
	    string s = emailBody; 
	    mail.setHtmlBody(s); 
	    Messaging.SendEmailResult[] mailResults = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	    return mailResults; 
	}

	/******* Unit Tests *****/
	static TestMethod void testSendEmailNotification(){
		String toAddress = 'AIBM_testing@testbucket.net';
		String subject = 'TESTING: test subject';
		String emailBody = 'UNIT TESTING: email body';
		
		Test.startTest();
		Messaging.SendEmailResult[] res = AIBMHelper.sendEmailNotification(toAddress, subject, emailBody);
		Test.stopTest();
		
		System.assert(res[0].isSuccess());
	}
	
	static TestMethod void testRemoveAccounts(){
		List<Account> accs = new List<Account>();
		List<String> emails = new List<String>();
		
		String email1 = 'test@test.com.test1';
		Account account1 = new Account(FirstName='Test'+1, LastName='Tester'+1, PersonEmail=email1);
		accs.add(account1);
		emails.add(email1);
		
		String email2 = 'test@test.com.test2';
		Account account2 = new Account(FirstName='Test'+2, LastName='Tester'+2, PersonEmail=email2);
		accs.add(account2);
		emails.add(email2);
		
		String email3 = 'test@test.com.test3';
		Account account3 = new Account(FirstName='Test'+3, LastName='Tester'+3, PersonEmail=email3);
		accs.add(account3);
		emails.add(email3);
		
		insert accs;
		
		List<Account> beforeRem = [SELECT Id, PersonEmail FROM Account WHERE PersonEmail like 'test@test%'];
		System.assertEquals(beforeRem.size(), 3);
		
		Test.startTest();
		AIBMHelper.removeAccounts(emails);
		Test.stopTest();
		
		List<Account> afterRem = [SELECT Id, PersonEmail FROM Account WHERE PersonEmail like 'test@test%'];
		System.assertEquals(afterRem.size(), 0);
	}
}