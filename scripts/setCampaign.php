<?php

session_start();

define("CLIENT_ID", "3MVG9Nvmjd9lcjRmxKHt1XpMgMbTFGhfwC9elIKrORYkT1OYINKcXDGfOiAD4v5zTkTar0SASkzIBYYg34jAC");
define("CLIENT_SECRET", "2250350097313131606");
define("REDIRECT_URI", "http://localhost/resttest/oauth_callback.php");
define("LOGIN_URI", "https://test.salesforce.com");
define("USERNAME", "eureka%40instantcloud.com.au.uat");
define("PASSWORD", "Cloud874t6vgJaZYJmQFJTIZbn7bRYTY");

$token_url = LOGIN_URI . "/services/oauth2/token";

$params = "username=" . USERNAME
	. "&password=" . PASSWORD
    . "&grant_type=password"
    . "&client_id=" . CLIENT_ID
    . "&client_secret=" . CLIENT_SECRET;
	

$curl = curl_init($token_url);
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

echo "Logging into Salesforce with username " . USERNAME;
$json_response = curl_exec($curl);

$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

if ( $status != 200 ) {
    die("\r\nError: call to token URL $token_url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
}
else {
	echo "\r\nLogin Successful!";
}

curl_close($curl);

$response = json_decode($json_response, true);

$access_token = $response['access_token'];
$instance_url = $response['instance_url'];

if (!isset($access_token) || $access_token == "") {
    die("Error - access token missing from response!");
}

if (!isset($instance_url) || $instance_url == "") {
    die("Error - instance URL missing from response!");
}

$rest_url = $instance_url . "/services/apexrest/setCampaign";


//Get CSV file of Accounts and Sources and Iterate round contents
$row = 1;
if (($handle = fopen("setCampaign_input.csv", "r")) !== FALSE) {
    while (!feof($handle)) {
	
		$data = fgetcsv($handle);
        $num = count($data);
        
		if ($num < 2) {
			echo("Error - not enough elements in row $row! \r\n");	
		}
		else {
			$email = trim($data[0]);
			$campaignID = urlencode(trim($data[1]));
			echo "\r\nRow $row -> Email:" . $email . " | CampaignID:" . $campaignID;
			
			$rest_params = "&emailAddress=" . "$email"
				. "&campaignId=" . "$campaignID";

			$rest_full_URL = $rest_url . '?' . $rest_params;
							
			$restCurl = curl_init($rest_full_URL);
			curl_setopt($restCurl, CURLOPT_HEADER, false);
			curl_setopt($restCurl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($restCurl, CURLOPT_HTTPHEADER, array("Authorization: OAuth $access_token"));
			curl_setopt($restCurl, CURLOPT_POST, false);
			curl_setopt($restCurl, CURLOPT_SSL_VERIFYPEER, false);

			$new_json_response = curl_exec($restCurl);
			curl_close($restCurl);
			
			echo " | Result:" . $new_json_response;
		}
		$row++;
    }
    fclose($handle);
}

?>